package com.caletbak.samplecode;

import java.io.*;

import android.os.Bundle;
import android.content.Context;
import android.media.AudioManager;
import android.view.KeyEvent;
import android.content.Intent;

import org.bakengine.BeDebug;
import org.bakengine.BeAppStartUpNativeActivity;
import org.bakengine.BeSplashScreenActivity;

public class BeSampleCodeTestActivity extends BeAppStartUpNativeActivity
{
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private static final String TAG = "BeSampleCodeTestActivity";

    private static Bundle s_pCreationBundle = null;

    private static boolean s_bIsFirstTimeSplashScreen = true;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private static native void onNativeCreate (Class<?> thisClass, Bundle savedInstanceState);

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected void onCreate (Bundle savedInstanceState)
    {
        // 
        // Called when the activity is starting.
        // 

        BeDebug.v (TAG, "onCreate");
        super.onCreate (savedInstanceState);

        s_pCreationBundle = savedInstanceState;

        if (s_bIsFirstTimeSplashScreen)
        {
            // launching the splash screen
            Intent i = new Intent(this, BeSplashScreenActivity.class);
            startActivity(i);

            s_bIsFirstTimeSplashScreen = false;
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public BeSampleCodeTestActivity()
    {
        super();

        BeDebug.i(TAG, "Creating SampleCodeNativeActivity");
    }

    @Override
    public void onStart()
    {
        BeDebug.i(TAG, " --onStart-- ");

        super.onStart();
    }

    @Override
    public void onStop()
    {
        BeDebug.i(TAG, " --onStop-- ");

        super.onStop();
    }

    @Override
    public void onDestroy()
    {
        BeDebug.i(TAG, " --onDestroy-- ");

        super.onDestroy();
    }

    @Override
    public void onPause()
    {
        BeDebug.i(TAG, " --onPause-- ");

        super.onPause();
    }

    @Override
    public void onResume()
    {
        BeDebug.i(TAG, " --onResume-- ");

        super.onResume();
    }
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        boolean result = true;

        if (keyCode == KeyEvent.KEYCODE_VOLUME_UP)
        {
            BeDebug.i(TAG, "volume up");
        }
        else if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)
        {
            BeDebug.i(TAG, "volume down");
        }
        else if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            BeDebug.i(TAG, "Back key pressed");
            return true;
        }
         
        return false;
    }
        
    @Override
    public void onBackPressed()
    {
        BeDebug.i(TAG, "onBackPressed Called");
    }

    @Override protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        super.onRestoreInstanceState(savedInstanceState);

        BeDebug.i(TAG, "onRestore: Bundle is = "+savedInstanceState);
    }

    @Override
    public void onWindowFocusChanged (boolean focus)
    {
        BeDebug.i(TAG, " -- onWindowFocusChanged -- focus = "+focus);

        super.onWindowFocusChanged(focus);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState)
    {
        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);

        BeDebug.i(TAG, "Activity: Bundle is: " + savedInstanceState);
    }

}
