/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeSampleCodeTestActivity.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <BeSampleCodeTestActivity.h>

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
static std::string s_kLanguagesDebug[8] = { "en", "es", "fr", "it", "de", "ja", "zh", "ko" };
static int s_currentIndexLanguage = 0;
static ndk_helper::PinchDetector pinch_detector;
static ndk_helper::TapCamera camera_input;
*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const char* g_rawNativeActivityClassName = "com/caletbak/samplecode/BeSampleCodeTestActivity";

jclass g_pBeSampleCodeTestActivityClass = 0;

jobject g_pBeSampleCodeTestActivityObj = 0;

jobject g_pBeSampleCodeTestActivityClassLoader = 0;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

JNIEXPORT void Java_com_caletbak_samplecode_BeSampleCodeTestActivity_onNativeCreate(JNIEnv *env, jobject obj, jclass thisClass, jobject savedInstanceState)
{
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

bool BeSampleCodeTestActivity::onAppDidLaunch()
{
    /*
	ISTAR_DEBUG("lifecycle  iStarNativeActivity::appInitFunc    UbiNativeActivity::s_GLContextWasDestroyed = %d", UbiNativeActivity::s_GLContextWasDestroyed);

	//ISTAR_DEBUG("Initializing Networking ...");
	//if (!iStar::NetworkInterface::init())
	//{
	//	return false;
	//}

	//if (engine->state.runtimeInit)
	if (bcn::display::getRoot() != NULL)
	{
		if (BeNativeActivity::s_GLContextWasDestroyed)
		{
			ISTAR_DEBUG("lifecycle iStarNativeActivity::appInitFunc : RESTORED");

			
			//bcn::renderer::instance->restore();

			//resourcemgr->reloadAll(); // enqueue all resources for reload

			//FreeTypeFontManager::getInstance()->reloadAll();

			bcn::events::GameRestored ev(0);
			bcn::display::getRoot()->dispatchCustomEvent(&ev);

            BeNativeActivity::s_GLContextWasDestroyed = false;
		}
	}
	else
	{
		ISTAR_DEBUG("lifecycle iStarNativeActivity::appInitFunc : first time initialization");

		srand(bcn::DeltaTimer::getMachineTimeMS());

		UbiAppConfig &appConfig = UbiAppStartUpRuntime::GetConfig();

    float scaleFactorX = (float)appConfig.m_graphicsConfig.m_screenWidth / (float)bcn::screen::designedWidth;
    float scaleFactorY = (float)appConfig.m_graphicsConfig.m_screenHeight / (float)bcn::screen::designedHeight;
    screen::scaleFactor = scaleFactorX;
    if (scaleFactorY < scaleFactorX)
        screen::scaleFactor = scaleFactorY;

		bcn::screen::SetScreenResolution(appConfig.m_graphicsConfig.m_screenWidth, appConfig.m_graphicsConfig.m_screenHeight);

#if USE_RENDER_TARGET
		bcn::screen::SetScreenResolution((appConfig.m_graphicsConfig.m_screenWidth * screen::designedHeight) / appConfig.m_graphicsConfig.m_screenHeight, screen::designedHeight);
#else
    appConfig.m_graphicsConfig.m_screenWidth /= screen::scaleFactor;
    appConfig.m_graphicsConfig.m_screenHeight /= screen::scaleFactor;

		bcn::screen::SetScreenResolution(appConfig.m_graphicsConfig.m_screenWidth, appConfig.m_graphicsConfig.m_screenHeight);
#endif

		bcn::UbiGraphics::Initialise();

		if (bcn::UbiGraphics::EnterCriticalSection("iStarNativeActivity::onAppDidLaunch"))
		{
			GameApp::InitCommonResources();

#if USE_RENDER_TARGET
			bcn::screen::SetScreenResolution((appConfig.m_graphicsConfig.m_screenWidth * screen::designedHeight) / appConfig.m_graphicsConfig.m_screenHeight, screen::designedHeight);

			bcn::screen::realWidth = appConfig.m_graphicsConfig.m_screenWidth;
			bcn::screen::realHeight = appConfig.m_graphicsConfig.m_screenHeight;

			ISTAR_DEBUG("Screen resolution is %d x %d", bcn::screen::realWidth, bcn::screen::realHeight);

			renderer::instance->createRenderQuad();
#else
			bcn::screen::SetScreenResolution(appConfig.m_graphicsConfig.m_screenWidth, appConfig.m_graphicsConfig.m_screenHeight);
#endif

			GameApp::CreateEntryPoint();

			KeyboardManager::init(NULL);

			bcn::UbiGraphics::ExitCriticalSection("iStarNativeActivity::onAppDidLaunch");
		}
	}

    camera_input.SetFlip( 1.f, -1.f, -1.f );
    camera_input.SetPinchTransformFactor( 4.f, 4.f, 8.f );
    
    */

	return BE_TRUE;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BeSampleCodeTestActivity::onAppWillTerminate()
{
    BE_DEBUG("iStarNativeActivity::onAppWillTerminate");
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BeSampleCodeTestActivity::onAppUpdate()
{
	//ISTAR_DEBUG("iStarNativeActivity::appUpdateFunc");

    /*
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	bcn::display::deletePendingObjects();
	
	if (UbiGraphics::EnterCriticalSection ("iStarNativeActivity::onDrawFrame 1"))
	{
        resourcemgr->update();

		UbiGraphics::ExitCriticalSection("iStarNativeActivity::onDrawFrame 1");
    }
    
	if (UbiGraphics::EnterCriticalSection("iStarNativeActivity::onDrawFrame 2"))
    {
        bcn::InputManager::Instance()->refreshDisplayList(display::getRoot());

		UbiGraphics::ExitCriticalSection("iStarNativeActivity::onDrawFrame 2");
    }
    
  if (UbiGraphics::EnterCriticalSection("iStarNativeActivity::onDrawFrame 3"))
  {
      NetworkInterface::processNetworkEvents();

      UbiGraphics::ExitCriticalSection("iStarNativeActivity::onDrawFrame 3");
  }

	static bcn::Chrono chrono;
	int delta = chrono.getTimeMS();
	chrono.start();

	if (UbiGraphics::EnterCriticalSection("iStarNativeActivity::onDrawFrame 3"))
	{
	
		//if (!UbiNativeActivity::s_stopUpdate)
		{
			bcn::display::getRoot()->logicTraversal(delta);
		}
		
		bcn::renderer::instance->render(bcn::screen::width, bcn::screen::height);

		UbiGraphics::ExitCriticalSection("iStarNativeActivity::onDrawFrame 3");
    }

	//bcn::FTTextLabel::sForceReload = false;
    */
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//bool BeSampleCodeTestActivity::onHandleInput(AInputEvent *inputEvent)
bool BeSampleCodeTestActivity::onHandleInput(BeVoidP pInputEvent)
{
    /*
	ISTAR_DEBUG("iStarNativeActivity::onHandleInput type:%d", AInputEvent_getType(inputEvent));

	if (AInputEvent_getType(inputEvent) == AINPUT_EVENT_TYPE_MOTION)
	{
        ndk_helper::GESTURE_STATE pinchState = pinch_detector.Detect(inputEvent);
        
        //Handle pinch state
        if( pinchState & ndk_helper::GESTURE_STATE_START )
        {
            //Start new pinch
            ndk_helper::Vec2 v1;
            ndk_helper::Vec2 v2;
            pinch_detector.GetPointers( v1, v2 );
            camera_input.BeginPinch( v1, v2 );
        }
        else if( pinchState & ndk_helper::GESTURE_STATE_MOVE )
        {
            //Multi touch
            //Start new pinch
            ndk_helper::Vec2 v1;
            ndk_helper::Vec2 v2;
            pinch_detector.GetPointers( v1, v2 );
            camera_input.Pinch( v1, v2 );
            float scale = camera_input.pinchFactor - 1.0;
            
            if (scale < 0.0f)
            {
                scale *= 2.0f;
            }
            else
            {
                scale *= 0.5f;
            }

            onHandlePinchGesture(1.0, scale);
        }
        
        
		float inputX = AMotionEvent_getX(inputEvent, 0);
		float inputY = AMotionEvent_getY(inputEvent, 0);
#if USE_RENDER_TARGET
		inputX *= (float)bcn::screen::width / (float)bcn::screen::realWidth;
		inputY *= (float)bcn::screen::height / (float)bcn::screen::realHeight;
#endif

		int32_t action = AMotionEvent_getAction(inputEvent);
		switch (action)
		{
			case AMOTION_EVENT_ACTION_UP:
			{
				//ISTAR_DEBUG("EGLView::handleInput: action=AMOTION_EVENT_ACTION_UP, x=%f, y=%f", inputX, inputY);
				bcn::NativeInputEvent ev(bcn::NativeInputEvent::E_INPUT_UP, inputX, inputY);
				bcn::InputManager::Instance()->processInputEvent(ev);
				break;
			}

			case AMOTION_EVENT_ACTION_DOWN:
			{
				//ISTAR_DEBUG("EGLView::handleInput: action=AMOTION_EVENT_ACTION_DOWN, x=%f, y=%f", inputX, inputY);
				bcn::NativeInputEvent ev(bcn::NativeInputEvent::E_INPUT_DOWN, inputX, inputY);
				bcn::InputManager::Instance()->processInputEvent(ev);
				break;
			}

			case AMOTION_EVENT_ACTION_MOVE:
			{
				//ISTAR_DEBUG("EGLView::handleInput: action=AMOTION_EVENT_ACTION_MOVE, x=%f, y=%f", inputX, inputY);
				bcn::NativeInputEvent ev(bcn::NativeInputEvent::E_INPUT_MOVE, inputX, inputY);
				bcn::InputManager::Instance()->processInputEvent(ev);
				break;
			}
		}

		return true;
	}
	else if (AInputEvent_getType(inputEvent) == AINPUT_EVENT_TYPE_KEY)
	{
		int32_t keyPressed = AKeyEvent_getKeyCode(inputEvent);
		int32_t action = AMotionEvent_getAction(inputEvent);
        ISTAR_DEBUG("EGLView::handleInput: action=AINPUT_EVENT_TYPE_KEY, keyPressed=%d, action=%d", keyPressed, action);

		switch (keyPressed)
		{
            case AKEYCODE_VOLUME_UP:
            {
                //SoundUtils::setMusicVolume();
                break;
            }
            case AKEYCODE_VOLUME_DOWN:
            {
                break;
            }
			case AKEYCODE_BACK:
			{
				if (action == AKEY_EVENT_ACTION_UP)
				{
            events::BackButtonPressed ev;
            display::getRoot()->dispatchCustomEvent(&ev);

            return true;
				}
				break;
			}
            default:
            {
				if (action == AKEY_EVENT_ACTION_DOWN)
				{
                    char buf[1];
					bool process = true;
                    if(keyPressed >= AKEYCODE_A && keyPressed <= AKEYCODE_Z){
                        sprintf(buf, "%c", (AKeyEvent_getKeyCode(inputEvent) + 'a' - AKEYCODE_A));
                    }
                    else if(keyPressed >= AKEYCODE_0 && keyPressed <= AKEYCODE_9){
                        sprintf(buf, "%c", (AKeyEvent_getKeyCode(inputEvent) + '0' - AKEYCODE_0));
                    }
                    else if(keyPressed == AKEYCODE_SPACE){
                        sprintf(buf, " ");
                    }
                    else if(keyPressed == AKEYCODE_DEL){
                        KeyboardManager::Instance()->backspace();
                        process = false;
                    }
					else{
						process = false;
                    }
					if (process)
					{
						KeyboardManager::Instance()->addText(stringUtils::ConvertString8To16(buf));
					}
                    break;
                }
            }
		}
	}

	return false;
    */

    return false;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BeSampleCodeTestActivity::onHandlePinchGesture(float factor, float velocity)
{
    /*
    NativeInputEvent ev(NativeInputEvent::E_INPUT_PINCH,-10000,-10000);
    ev.velocity = velocity;
    ev.factor = factor;
    InputManager::Instance()->processInputEvent(ev);
    */
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BeSampleCodeTestActivity::onHandleRotationGesture(float deltaDegrees, bool isEnded)
{
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

