@REM ##########################################################
@REM ##
@REM ## BAK ENGINE 3
@REM ## ANDROID KEYS GENERATOR
@REM ##
@REM ##########################################################

@echo off

setlocal EnableDelayedExpansion

cls

@echo.
@echo BAK ENGINE 3 BUILDING SYSTEM - Generating Android Keys...
@echo.

@SET strPrefixToFind=
@SET strPathToBe3SDK=
@SET strPathToSearch=
@SET maxTries=
@SET Be3Toolkit=

@SET strPrefixToFind=/Be3Toolkit
@SET strPathToBe3SDK=.
@SET strPathToSearch=!strPathToBe3SDK!!strPrefixToFind!
@SET maxTries=10

@for /l %%x in (1, 1, !maxTries!) do (
	@SET strPathToBe3SDK=!strPathToBe3SDK!/..
	@SET strPathToSearch=!strPathToBe3SDK!!strPrefixToFind!
	
	if exist !strPathToSearch! (
		@SET Be3Toolkit=!strPathToSearch!
		goto found
	)
	
	@IF %%x == !maxTries! (goto notFound)
)

:found
@echo.
@echo DEBUG Key data...
@echo.
"!Be3Toolkit!/../ExternalTools/Java/windows/bin/keytool.exe" -genkey -v -keystore debugKey.keystore -alias androidDebugKey -keyalg RSA -keysize 2048 -validity 10000
@echo.
@echo RELEASE Key data...
@echo.
"!Be3Toolkit!/../ExternalTools/Java/windows/bin/keytool.exe" -genkey -v -keystore releaseKey.keystore -alias androidReleaseKey -keyalg RSA -keysize 2048 -validity 10000
goto finish

:notFound
@echo No Be3Toolkit found. Checkout BE3 Toolkit and be sure the folder is called Be3Toolkit.

:finish

