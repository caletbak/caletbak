/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeSampleCodeTestPredef.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BESAMPLECODETESTPREDEF_H
#define BE_BESAMPLECODETESTPREDEF_H

// Global BE3 include
#include <BakEngine3Libs.h>

#ifdef _DEBUG
#  define BE_DEBUG(x, ...) _OutputDebugString("[  DEBUG]: ", true, x, ##__VA_ARGS__)
#  define BE_WARNING(x, ...) _OutputDebugString("[! WARNING]: ", true, x, ##__VA_ARGS__)
#  define BE_ERROR(x, ...) _OutputDebugString("[* ERROR]: ", true, x, ##__VA_ARGS__)
#  define BE_PRINT(x, ...) _OutputDebugString("", false, x, ##__VA_ARGS__)
#else
#  define BE_DEBUG(x, ...) x
#  define BE_WARNING(x, ...) x
#  define BE_ERROR(x, ...) x
#  define BE_PRINT(x, ...) x
#endif

// MODULES IDs
#define NO_MODULE             0
#define GAME_TITLE_MODULE     1
#define GAME_PLAY_MODULE      2

#endif // #ifndef BE_BEKINGTESTPREDEF_H

