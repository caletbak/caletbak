//--------------------------------------------------------------------------------------
// File: material_2d_VS.hlsl
//
// The vertex shader file for the material 2D 
//--------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------
// Globals
//--------------------------------------------------------------------------------------
cbuffer cbPerObject : register( b0 )
{
    matrix        g_mWorldViewProjection    : packoffset( c0 );
	float4        g_vColor	                : packoffset( c4 );
};

//--------------------------------------------------------------------------------------
// Input / Output structures
//--------------------------------------------------------------------------------------
struct VS_INPUT
{
    float4 vPosition    : POSITION;
    float2 vTexcoord    : TEXCOORD0;
};

struct VS_OUTPUT
{
    float4 vColor       : COLOR;
    float2 vTexcoord    : TEXCOORD0;  
	float4 vPosition    : SV_POSITION;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUTPUT VSMain( VS_INPUT Input )
{
    VS_OUTPUT Output;
    
    Output.vPosition = mul( g_mWorldViewProjection, Input.vPosition );
	Output.vColor = g_vColor;
    Output.vTexcoord = Input.vTexcoord;
	
    return Output;
}
