####################################################################
##
## BAK ENGINE 3
## BUILD SYSTEM
##
## RegenProjectConfig.rb
## This ruby prepares the main project properties
##
## Javier Calet Toledo - 2013
##
###################################################################

#!/bin/sh

# SAMPLE CODE TEST CONFIG
kProjectPropertiesConfig = Hash.new
$g_bForceDebugInReleaseBuild = false

# GAME Project related
kProjectPropertiesConfig["gameName"] = "SampleCode Test"
kProjectPropertiesConfig["forcePackageName"] = "com.caletbak.samplecode"		# Force Main project bundle package
kProjectPropertiesConfig["companyName"] = "bakengine"			# Only 1 word (used by Java packages)

# VERSIONS - GPLAY and AMAZON
kProjectPropertiesConfig["versionCode"] = "00000001"
kProjectPropertiesConfig["versionName"] = "1.0.0"
kProjectPropertiesConfig["outputFilename"] = "samplecode"		# Internal Output files generated (ie. shared lib), and main project package (com.be3.sampleCodeTest)

# VERSIONS - IOS
#kProjectPropertiesConfig["versionName"] = "1.0"
#kProjectPropertiesConfig["versionCode"] = "1"
#kProjectPropertiesConfig["outputFilename"] = "sampleCode"		# Internal Output files generated (ie. shared lib), and main project package (com.be.sampleCodeTest)

# COMMON Project stuff
kProjectPropertiesConfig["isMainProject"] = true
kProjectPropertiesConfig["isLibrary"] = false

# IOS APP generation related

# ANDROID APK generation related
kProjectPropertiesConfig["androidGPlayABIs"] = "armeabi-v7a" # use "armeabi-v7a x86" for adding x86 platform libraries
kProjectPropertiesConfig["androidAmazonABIs"] = "armeabi-v7a"
kProjectPropertiesConfig["androidTarget"] = $g_strAndroidTarget
kProjectPropertiesConfig["androidMinTarget"] = $g_strAndroidMinTarget
kProjectPropertiesConfig["androidActivity"] = ".BeSampleCodeTestActivity"
kProjectPropertiesConfig["androidIconName"] = "ic_launcher"

kProjectPropertiesConfig["androidDebugKeyStoreFilePath"] = "./AndroidKeys/debugKey.keystore"
kProjectPropertiesConfig["androidDebugKeyAlias"] = "androidDebugKey"
kProjectPropertiesConfig["androidDebugKeyStorePass"] = "android"
kProjectPropertiesConfig["androidReleaseKeyStoreFilePath"] = "./AndroidKeys/releaseKey.keystore"
kProjectPropertiesConfig["androidReleaseKeyAlias"] = "androidReleaseKey"
kProjectPropertiesConfig["androidReleaseKeyStorePass"] = "android"

# Proxy settings for APK time-stamp signing (leave empty or remove it if not needed)
$g_bAndroidSignProxyServer = "bcn-net-proxy.ubisoft.org"
$g_bAndroidSignProxyPort = "3128"

# ANDROID MANIFEST related

# If TRUE add the necessary stuff for showing a splash screen
$g_bAndroidUsingSplashScreen = true

$g_kProjectPropertiesConfig.push kProjectPropertiesConfig
