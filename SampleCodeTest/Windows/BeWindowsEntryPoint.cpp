/////////////////////////////////////////////////////////////////////////////
//
// KING TEST
// by Javier Calet Toledo
//
// BeTestMain.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <stdafx.h>

#include <BeSampleCodeTestMain.h>
#include <Modules/BeGameTitleModule.h>
#include <Modules/BeGamePlayModule.h>

BeApplicationConfig s_appConfig;

BeInt APIENTRY WinMain (HINSTANCE hAppInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, BeInt nCmdShow)
{
    BE_UNUSED (hAppInstance);
    BE_UNUSED (hPrevInstance);
    BE_UNUSED (lpCmdLine);
    BE_UNUSED (nCmdShow);

    BE_APPLICATION_DEBUG("lifecycle BeAppStartUpRuntime: onCreate");

    if (!s_appConfig.m_bIsAppInitialised)
    {
        s_appConfig.SetAppName("SampleCodeTest");

        // 
        // Populate initial platform and configuration defaults.
        // 

        ApplicationSetConfiguration(s_appConfig);
    }

    if (s_appConfig.m_onAppDidLaunch)
    {
        BeApplicationContext* pAppContext = BeApplicationContext::GetInstance();

        BeBoolean bAppInitialised = s_appConfig.m_onAppDidLaunch();
        if (bAppInitialised)
        {
            BeAccTableHandle hAccel = NULL;
            BeWindowHandle hWnd = pAppContext->GetAppWindow();

            // Now we're ready to receive and process Windows messages.
            BeBoolean bGotMsg;

            MSG msg;
            msg.message = WM_NULL;

            while (WM_QUIT != msg.message)
            {
                // Use PeekMessage() so we can use idle time to render the scene. 
                bGotMsg = (PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE) != 0);

                if (bGotMsg)
                {
                    // Translate and dispatch the message
                    if (hAccel == NULL || hWnd == NULL || TranslateAccelerator(hWnd, hAccel, &msg) == 0)
                    {
                        TranslateMessage(&msg);
                        DispatchMessage(&msg);
                    }
                }
                else
                {
                    if (s_appConfig.m_onAppUpdate)
                    {
                        s_appConfig.m_onAppUpdate();
                    }
                }
            }
        }
    }

    if (s_appConfig.m_onAppWillTerminate)
    {
        s_appConfig.m_onAppWillTerminate();
    }

    return 0;
}

