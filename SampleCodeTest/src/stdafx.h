// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#if TARGET_WINDOWS

#include <targetver.h>
#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers

// Windows Header Files:
#include <windows.h>
#include <tchar.h>

#endif

// C RunTime Header Files
#include <stdlib.h>
#if TARGET_IOS
#   include <malloc/malloc.h>
#else
#   include <malloc.h>
#endif
#include <memory.h>

// TODO: reference additional headers your program requires here
