/////////////////////////////////////////////////////////////////////////////
//
// KING TEST
// by Javier Calet Toledo
//
// BeTestMain.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <BeSampleCodeTestMain.h>

#include <Modules/BeGameTitleModule.h>
#include <Modules/BeGamePlayModule.h>

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

static BeGameTitleModulePtr s_spGameTitleModule = BeNew BeGameTitleModule();
static BeGamePlayModulePtr s_spGamePlayModule = BeNew BeGamePlayModule();

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

BeBoolean BeSampleCodeTestMain::onAppDidLaunch()
{
    BeBoolean bRenderDeviceIsOK = BE_FALSE;

    BeApplicationContext* pAppContext = BeApplicationContext::GetInstance();

#if TARGET_WINDOWS
    BeApplicationContext::CheckForCurrentDirectory(L"assets", 3);

    if (pAppContext->ConfigureRenderDevice(L"assets/XML/Configuration/DeviceConfig.xml") != NULL)
    {
        bRenderDeviceIsOK = BE_TRUE;
    }
#endif

    if (bRenderDeviceIsOK)
    {
        s_spGameTitleModule = BeNew BeGameTitleModule();
        s_spGamePlayModule = BeNew BeGamePlayModule();

        pAppContext->GetModulesManager()->RegistryModule(s_spGameTitleModule);
        pAppContext->GetModulesManager()->RegistryModule(s_spGamePlayModule);

        pAppContext->GetModulesManager()->ChangeModule(GAME_TITLE_MODULE);

        return BE_TRUE;
    }

    return BE_FALSE;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BeSampleCodeTestMain::onAppUpdate()
{
    BeApplicationContext* pAppContext = BeApplicationContext::GetInstance();

    pAppContext->Update();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BeSampleCodeTestMain::onAppWillTerminate(void)
{
    BeApplicationContext* pAppContext = BeApplicationContext::GetInstance();

    pAppContext->GetModulesManager()->ChangeModule(NO_MODULE);

    BeApplicationContext::DestroyStaticResources();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

BeBoolean BeSampleCodeTestMain::onHandleInput(BeVoidP pInputEvent)
{
    return BE_FALSE;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BeSampleCodeTestMain::onHandlePinchGesture(BeFloat fFactor, BeFloat fVelocity)
{
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BeSampleCodeTestMain::onHandleRotationGesture(BeFloat fDeltaDegrees, BeBoolean bIsEnded)
{
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void ApplicationSetConfiguration(BeApplicationConfig& kConfig)
{
    kConfig.m_onAppDidLaunch = BeSampleCodeTestMain::onAppDidLaunch;
    kConfig.m_onAppUpdate = BeSampleCodeTestMain::onAppUpdate;
    kConfig.m_onAppWillTerminate = BeSampleCodeTestMain::onAppWillTerminate;
    kConfig.m_onHandleInput = BeSampleCodeTestMain::onHandleInput;
    kConfig.m_onHandlePinchGesture = BeSampleCodeTestMain::onHandlePinchGesture;
    kConfig.m_onHandleRotationGesture = BeSampleCodeTestMain::onHandleRotationGesture;

#if TARGET_ANDROID
    kConfig.m_kGraphicsConfig.m_uEGLRenderableType = EGL_OPENGL_ES2_BIT;
    kConfig.m_kGraphicsConfig.m_uEGLSurfaceType = EGL_WINDOW_BIT;
    kConfig.m_kGraphicsConfig.m_uEGLRedSize = 8;
    kConfig.m_kGraphicsConfig.m_uEGLGreenSize = 8;
    kConfig.m_kGraphicsConfig.m_uEGLBlueSize = 8;
    kConfig.m_kGraphicsConfig.m_uEGLDepthSize = 24;
    kConfig.m_kGraphicsConfig.m_uEGLStencilSize = 8;
#endif

    kConfig.m_bIsAppInitialised = false;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

