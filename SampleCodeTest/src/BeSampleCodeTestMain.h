/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeSampleCodeTestMain.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BESAMPLECODETESTMAIN_H
#define BE_BESAMPLECODETESTMAIN_H

// Global BE3 include
#include <Pch/BeSampleCodeTestPredef.h>

class BeSampleCodeTestMain
{
public:

    static BeBoolean onAppDidLaunch(void);

    static void onAppUpdate(void);

    static void onAppWillTerminate(void);

    static BeBoolean onHandleInput(BeVoidP pInputEvent);

    static void onHandlePinchGesture(BeFloat fFactor, BeFloat fVelocity);

    static void onHandleRotationGesture(BeFloat fDeltaDegrees, BeBoolean bIsEnded);

    static void SetDataPath(const BeString8& strPath);

    static void SetCachePath(const BeString8& strPath);

};

#endif // #ifndef BE_BESAMPLECODETESTMAIN_H

