/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeGamePlayModule.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEGAMEPLAYMODULE_H
#define BE_BEGAMEPLAYMODULE_H

#include <Pch/BeSampleCodeTestPredef.h>

// Forward declarations
class BeRenderDevice;

/////////////////////////////////////////////////
/// \class BeGamePlayModule
/// \brief Game Play module
/////////////////////////////////////////////////
class BeGamePlayModule : public BeModule
{
  public:

    /// \brief  Constructor
    BeGamePlayModule();

    /// \brief  Destructor
    ~BeGamePlayModule();


    // Getters
    // {

    // }


    // Methods
    // {

      /// \brief  This is called first when the module has been loaded
      virtual void LoadModule(void);

      /// \brief  Performs the update process
      virtual BeInt Update(const BeApplicationTypes::BeContextUpdate& kContextUpdate);

      /// \brief  Performs the rendering process
      virtual void Render(BeRenderDevice* pRenderDevice);

      /// \brief  This is called when exiting the module
      virtual void UnLoadModule(void);

    // }

  private:

    /// \brief  Callback for mouse events
    static void MouseListener(BeVoidP pObjectListener, BeVoidP pMouseData);

};

// Smart pointer BeModelPackerModule
typedef BeSmartPointer<BeGamePlayModule> BeGamePlayModulePtr;

#endif // #ifndef BE_BEGAMEPLAYMODULE_H
