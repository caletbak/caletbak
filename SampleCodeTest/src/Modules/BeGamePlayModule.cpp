/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeGamePlayModule.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <Modules/BeGamePlayModule.h>



/////////////////////////////////////////////////////////////////////////////
static BeSprite2DPtr m_spMousePointerIcon = NULL;
static BeInt m_iSoundID = -1;
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeGamePlayModule::BeGamePlayModule ()
{
}
/////////////////////////////////////////////////////////////////////////////
BeGamePlayModule::~BeGamePlayModule ()
{
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeGamePlayModule::LoadModule(void)
{
  BeApplicationContext* pAppContext = BeApplicationContext::GetInstance();

  m_spMousePointerIcon = BeNew BeSprite2D(pAppContext->GetRenderManager(), L"assets\\Textures\\mouse_icon.png", BeSprite2D::PIVOT_V_TOP | BeSprite2D::PIVOT_H_LEFT);

  //m_iSoundID = pAppContext->GetAudioSystem ()->GetSoundManager ()->PrepareSound (L"assets\\Sounds\\block_metal.ogg", BeAudioSystem::SOUND_2D, 10);
  //pAppContext->GetAudioSystem ()->GetSoundManager ()->SetVolume (m_iSoundID, 0.25f);

  pAppContext->GetInputManager ()->RegistryInputListener (this, &MouseListener);

  pAppContext->GetSceneManager()->SetCurrentCamera(BeSceneManager::CAMERA_DEFAULT_UI);
}
/////////////////////////////////////////////////////////////////////////////
void BeGamePlayModule::UnLoadModule(void)
{
  m_spMousePointerIcon = NULL;

  BeApplicationContext* pAppContext = BeApplicationContext::GetInstance();

  //pAppContext->GetAudioSystem ()->GetSoundManager ()->ReleaseSound (m_iSoundID);

  pAppContext->GetInputManager ()->UnRegistryInputListener (this, &MouseListener);
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeInt BeGamePlayModule::Update (const BeApplicationTypes::BeContextUpdate& kContextUpdate)
{
  BE_UNUSED(kContextUpdate);

#if TARGET_WINDOWS
  BeInputMouse::MouseData* mouseState = BeInputMouse::GetMouseCurrentState ();
  m_spMousePointerIcon->SetPosition ((BeFloat)mouseState->m_coordX, (BeFloat)mouseState->m_coordY);
#endif

  return m_nextModule;
}
/////////////////////////////////////////////////////////////////////////////
void BeGamePlayModule::Render (BeRenderDevice* pRenderDevice)
{
  BE_UNUSED(pRenderDevice);

  BeApplicationContext* pAppContext = BeApplicationContext::GetInstance();

  pRenderDevice->BeginScene(pAppContext->GetSceneManager()->GetCurrentCamera()->GetViewProjectionMatrix());

  BeVector4D clearColour(0.0f, 0.0f, 1.0f, 1.0f);
  pRenderDevice->Clear (BeRenderTypes::CLEAR_TARGET, clearColour, 1.0f, 0);

  m_spMousePointerIcon->Render ();

  pRenderDevice->EndScene ();
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeGamePlayModule::MouseListener(BeVoidP pObjectListener, BeVoidP pMouseData)
{
#if TARGET_WINDOWS
    BeInputMouse::MouseData* pMouseDataImpl = reinterpret_cast<BeInputMouse::MouseData*>(pMouseData);

    if (pObjectListener && pMouseDataImpl)
    {
        BeGamePlayModule* module = static_cast<BeGamePlayModule*> (pObjectListener);
        BE_UNUSED (module);

        switch (pMouseDataImpl->m_buttonDidEvent)
        {
            case BeInputMouse::MOUSE_BUTTON_LEFT:
            {
                BE_INPUT_DEBUG("Mouse MOUSE_BUTTON_LEFT = %d   (%d,%d)", pMouseDataImpl->m_leftButton, pMouseDataImpl->m_coordX, pMouseDataImpl->m_coordY);
                break;
            }
            case BeInputMouse::MOUSE_BUTTON_RIGHT:
            {
                BE_INPUT_DEBUG("Mouse MOUSE_BUTTON_RIGHT = %d   (%d,%d)", pMouseDataImpl->m_rightButton, pMouseDataImpl->m_coordX, pMouseDataImpl->m_coordY);
                break;
            }
            case BeInputMouse::MOUSE_BUTTON_MEDIUM:
            {
                if (pMouseDataImpl->m_mediumButton == BUTTON_DOWN)
                {
                    module->m_nextModule = GAME_TITLE_MODULE;
                }
                break;
            }
        }
    }
#endif
}
/////////////////////////////////////////////////////////////////////////////


