/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeGameTitleModule.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <Modules/BeGameTitleModule.h>

/////////////////////////////////////////////////////////////////////////////
static BeSprite2DPtr m_spMousePointerIcon = NULL;
static BeSprite2DPtr m_spTitle = NULL;
static BeTimerPtr m_spTimer = NULL;
static BeInt m_iTitleMusicID = -1;
static BeInt m_iStartSoundID = -1;
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeGameTitleModule::BeGameTitleModule ()
{
}
/////////////////////////////////////////////////////////////////////////////
BeGameTitleModule::~BeGameTitleModule ()
{
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeGameTitleModule::LoadModule(void)
{
  BeApplicationContext* pAppContext = BeApplicationContext::GetInstance();

  m_spMousePointerIcon = BeNew BeSprite2D(pAppContext->GetRenderManager(), L"assets\\Textures\\mouse_icon.png", BeSprite2D::PIVOT_V_TOP | BeSprite2D::PIVOT_H_LEFT);

  m_spTitle = BeNew BeSprite2D(pAppContext->GetRenderManager(), L"assets\\Textures\\title.png", BeSprite2D::PIVOT_V_TOP | BeSprite2D::PIVOT_H_LEFT);
  m_spTitle->SetScale (1.25f, 0.703125f);

  m_spTimer = BeNew BeTimer ();
  m_spTimer->Start (1000, BE_TRUE);

  //m_iTitleMusicID = pAppContext->GetAudioSystem ()->GetMusicManager ()->PrepareMusic (L"assets\\Music\\navigation.ogg");
  //pAppContext->GetAudioSystem ()->GetMusicManager ()->FadeIn (m_iTitleMusicID, 3000, BE_FALSE, 0.0f);
  //pAppContext->GetAudioSystem ()->GetMusicManager ()->PlayMusic (m_iTitleMusicID);

  //m_iStartSoundID = pAppContext->GetAudioSystem ()->GetSoundManager ()->PrepareSound (L"assets\\Sounds\\start.ogg", BeAudioSystem::SOUND_2D, 10);

  pAppContext->GetInputManager ()->RegistryInputListener (this, &MouseListener);

  pAppContext->GetSceneManager()->SetCurrentCamera (BeSceneManager::CAMERA_DEFAULT_UI);
}
/////////////////////////////////////////////////////////////////////////////
void BeGameTitleModule::UnLoadModule(void)
{
  m_spMousePointerIcon = NULL;
  m_spTitle = NULL;
  m_spTimer = NULL;

  BeApplicationContext* pAppContext = BeApplicationContext::GetInstance();
  pAppContext->GetInputManager ()->UnRegistryInputListener (this, &MouseListener);
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeInt BeGameTitleModule::Update (const BeApplicationTypes::BeContextUpdate& kContextUpdate)
{
  BE_UNUSED(kContextUpdate);

#if TARGET_WINDOWS
  BeInputMouse::MouseData* mouseState = BeInputMouse::GetMouseCurrentState ();
  m_spMousePointerIcon->SetPosition ((BeFloat)mouseState->m_coordX, (BeFloat)mouseState->m_coordY);
#endif

  return m_nextModule;
}
/////////////////////////////////////////////////////////////////////////////
void BeGameTitleModule::Render (BeRenderDevice* pRenderDevice)
{
  BE_UNUSED(pRenderDevice);

  BeApplicationContext* pAppContext = BeApplicationContext::GetInstance();

  BeVector4D clearColour(0.5f, 0.5f, 0.5f, 1.0f);
  pRenderDevice->Clear(BeRenderTypes::CLEAR_TARGET, clearColour, 1.0f, 0);

  pRenderDevice->BeginScene(pAppContext->GetSceneManager()->GetCurrentCamera()->GetViewProjectionMatrix());

  m_spTitle->Render ();
  m_spMousePointerIcon->Render ();

  pRenderDevice->EndScene ();
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeGameTitleModule::MouseListener(BeVoidP pObjectListener, BeVoidP pMouseData)
{
#if TARGET_WINDOWS
    BeInputMouse::MouseData* pMouseDataImpl = reinterpret_cast<BeInputMouse::MouseData*>(pMouseData);
  
    if (pObjectListener && pMouseDataImpl)
    {
        BeGameTitleModule* module = static_cast<BeGameTitleModule*> (pObjectListener);

        switch (pMouseDataImpl->m_buttonDidEvent)
        {
            case BeInputMouse::MOUSE_BUTTON_LEFT:
            {
                if (pMouseDataImpl->m_leftButton == BUTTON_DOWN)
                {
                    //BeApplicationContext* pAppContext = BeApplicationContext::GetInstance();
                    //pAppContext->GetAudioSystem ()->GetSoundManager ()->PlaySoundAndRelease (m_iStartSoundID);
                    //pAppContext->GetAudioSystem ()->GetMusicManager ()->FadeOut (m_iTitleMusicID, 2000, BE_TRUE);

                    module->m_nextModule = GAME_PLAY_MODULE;
                }
                break;
            }
        }
    }
#endif
}
/////////////////////////////////////////////////////////////////////////////

