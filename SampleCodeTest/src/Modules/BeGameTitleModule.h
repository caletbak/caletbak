/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeGameTitleModule.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEGAMETITLEMODULE_H
#define BE_BEGAMETITLEMODULE_H

#include <Pch/BeSampleCodeTestPredef.h>

// Forward declarations
class BeRenderDevice;

/////////////////////////////////////////////////
/// \class BeGameTitleModule
/// \brief Game Title module
/////////////////////////////////////////////////
class BeGameTitleModule : public BeModule
{
  public:

    /// \brief  Constructor
    BeGameTitleModule();

    /// \brief  Destructor
    ~BeGameTitleModule();


    // Getters
    // {

    // }


    // Methods
    // {

      /// \brief  This is called first when the module has been loaded
      virtual void LoadModule(void);

      /// \brief  Performs the update process
      virtual BeInt Update(const BeApplicationTypes::BeContextUpdate& kContextUpdate);

      /// \brief  Performs the rendering process
      virtual void Render(BeRenderDevice* pRenderDevice);

      /// \brief  This is called when exiting the module
      virtual void UnLoadModule(void);

    // }

  private:

    /// \brief  Callback for mouse events
    static void MouseListener(BeVoidP pObjectListener, BeVoidP pMouseData);

};

// Smart pointer BeModelPackerModule
typedef BeSmartPointer<BeGameTitleModule> BeGameTitleModulePtr;

#endif // #ifndef BE_BEGAMETITLEMODULE_H
