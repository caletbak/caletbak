@REM ##########################################################
@REM ##
@REM ## BAK ENGINE 3
@REM ## SAMPLE CODE TEST
@REM ##
@REM ##########################################################

@echo off

setlocal EnableDelayedExpansion

@echo.
@echo BAK ENGINE 3 BUILDING SYSTEM - RegenProject.bat
@echo Building the project files automatically for...
@echo.

@SET strPrefixToFind=
@SET strPathToBe3SDK=
@SET strPathToSearch=
@SET maxTries=
@SET Be3Toolkit=

@SET strPrefixToFind=/Be3Toolkit
@SET strPathToBe3SDK=.
@SET strPathToSearch=!strPathToBe3SDK!!strPrefixToFind!
@SET maxTries=10

@for /l %%x in (1, 1, !maxTries!) do (
	@SET strPathToBe3SDK=!strPathToBe3SDK!/..
	@SET strPathToSearch=!strPathToBe3SDK!!strPrefixToFind!
	
	if exist !strPathToSearch! (
		@SET Be3Toolkit=!strPathToSearch!
		goto found
	)
	
	@IF %%x == !maxTries! (goto notFound)
)

:found
@"ruby" "!Be3Toolkit!/BuildTools/ExecBuildingSystem.rb" "REGEN_PROJECT" clean
goto finish

:notFound
@echo No Be3Toolkit found. Be sure the toolkit folder is called Be3Toolkit.

:finish
