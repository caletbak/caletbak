@REM ##########################################################
@REM ##
@REM ## BAK ENGINE 3 MOBILE BARCELONA
@REM ## DX UTILS LIB
@REM ##
@REM ##########################################################

@echo off

setlocal EnableDelayedExpansion

@echo.
@echo BAK ENGINE 3 BUILDING SYSTEM - RegenProjects.bat
@echo Building the project files automatically for...
@echo.

@SET strPrefixToFind=
@SET strPathToBE3SDK=
@SET strPathToSearch=
@SET maxTries=
@SET Be3Toolkit=

@SET strPrefixToFind=/Be3Toolkit
@SET strPathToBE3SDK=.
@SET strPathToSearch=!strPathToBE3SDK!!strPrefixToFind!
@SET maxTries=10

@for /l %%x in (1, 1, !maxTries!) do (
	@SET strPathToBE3SDK=!strPathToBE3SDK!/..
	@SET strPathToSearch=!strPathToBE3SDK!!strPrefixToFind!
	
	if exist !strPathToSearch! (
		@SET Be3Toolkit=!strPathToSearch!
		goto found
	)
	
	@IF %%x == !maxTries! (goto notFound)
)

:found
@"ruby" "!Be3Toolkit!/BeBuildTools/ExecBuildingSystem.rb" "REGEN_PROJECT_EXTERNAL" %1
goto finish

:notFound
@echo No Be3Toolkit found. Checkout BAK Engine 3 Toolkit and be sure the folder is called Be3Toolkit.

:finish
