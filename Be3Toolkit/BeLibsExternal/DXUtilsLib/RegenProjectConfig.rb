####################################################################
##
## BAK ENGINE 3
## BUILD SYSTEM
## 
## RegenProjectConfig.rb
## This ruby prepares the main project properties
##
###################################################################

#!/bin/sh

# DX UTILS LIB CONFIG
kProjectPropertiesConfig = Hash.new
$g_bForceDebugInReleaseBuild = false

# COMMON Project stuff
kProjectPropertiesConfig["projectName"] = "DXUtilsLib"
kProjectPropertiesConfig["projectPath"] = "BeLibsExternal/"
kProjectPropertiesConfig["projectExports"] = ["BUILDING_DXUTILS"]
kProjectPropertiesConfig["extraIncludeDirs"] = ["include"]

kProjectPropertiesConfig["isMainProject"] = false
kProjectPropertiesConfig["isLibrary"] = false
kProjectPropertiesConfig["isExternalLibrary"] = true

# VS generation related
kProjectPropertiesConfig["VSDisableWarnings"] = "4273;"

# ANDROID APK generation related
kProjectPropertiesConfig["androidTarget"] = $g_strAndroidTarget
kProjectPropertiesConfig["androidMinTarget"] = $g_strAndroidMinTarget

$g_kProjectPropertiesConfig.push kProjectPropertiesConfig
