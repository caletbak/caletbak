####################################################################
##
## BAK ENGINE 3
## BUILD SYSTEM
## 
## RegenProjectConfig.rb
## This ruby prepares the main project properties
##
###################################################################

#!/bin/sh

# ZLIB LIB CONFIG
kProjectPropertiesConfig = Hash.new
$g_bForceDebugInReleaseBuild = false

# COMMON Project stuff
kProjectPropertiesConfig["projectName"] = "ZLibLib"
kProjectPropertiesConfig["projectPath"] = "BeLibsExternal/"
kProjectPropertiesConfig["projectExports"] = ["BUILDING_ZLIB", "IOAPI_NO_64"]
kProjectPropertiesConfig["extraIncludeDirs"] = ["include"]

kProjectPropertiesConfig["isMainProject"] = false
kProjectPropertiesConfig["isLibrary"] = false
kProjectPropertiesConfig["isExternalLibrary"] = true

# VS generation related
kProjectPropertiesConfig["VSDisableWarnings"] = "4100;4127;4131;4189;4244;4701;4703;4996"

# ANDROID APK generation related
kProjectPropertiesConfig["androidTarget"] = $g_strAndroidTarget
kProjectPropertiesConfig["androidMinTarget"] = $g_strAndroidMinTarget

$g_kProjectPropertiesConfig.push kProjectPropertiesConfig
