####################################################################
##
## BAK ENGINE 3
## BUILD SYSTEM
## 
## RegenProjectConfig.rb
## This ruby prepares the main project properties
##
###################################################################

#!/bin/sh

# ICU LIB CONFIG
kProjectPropertiesConfig = Hash.new
$g_bForceDebugInReleaseBuild = false

# COMMON Project stuff
kProjectPropertiesConfig["projectName"] = "ICULib"
kProjectPropertiesConfig["projectPath"] = "BeLibsExternal/"
kProjectPropertiesConfig["projectExternalDependencies"] = ["ICUDataLib"]
kProjectPropertiesConfig["projectExports"] = ["U_COMMON_IMPLEMENTATION", "U_STATIC_IMPLEMENTATION", "BUILDING_LIBRARY"]
kProjectPropertiesConfig["extraIncludeDirs"] = ["include"]

kProjectPropertiesConfig["isMainProject"] = false
kProjectPropertiesConfig["isLibrary"] = false
kProjectPropertiesConfig["isExternalLibrary"] = true

# VS generation related
kProjectPropertiesConfig["VSDisableWarnings"] = "4100;4127;4132;4133;4244;4245;4273;4310;4389;4456;4457;4458;4505;4510;4512;4610;4701;4703;4706;4996;"

# ANDROID APK generation related
kProjectPropertiesConfig["androidTarget"] = $g_strAndroidTarget
kProjectPropertiesConfig["androidMinTarget"] = $g_strAndroidMinTarget

$g_kProjectPropertiesConfig.push kProjectPropertiesConfig
