####################################################################
##
## BAK ENGINE 3
## BUILD SYSTEM
## 
## RegenProjectConfig.rb
## This ruby prepares the main project properties
##
###################################################################

#!/bin/sh

# FREETYPE LIB CONFIG
kProjectPropertiesConfig = Hash.new
$g_bForceDebugInReleaseBuild = false

# COMMON Project stuff
kProjectPropertiesConfig["projectName"] = "FreetypeLib"
kProjectPropertiesConfig["projectPath"] = "BeLibsExternal/"
kProjectPropertiesConfig["projectExports"] = ["BUILDING_FREETYPE", "FT2_BUILD_LIBRARY", "_CRT_SECURE_NO_WARNINGS", "FT_DEBUG_LEVEL_ERROR", "FT_DEBUG_LEVEL_TRACE", "_CRT_SECURE_NO_DEPRECATE"]
kProjectPropertiesConfig["extraIncludeDirs"] = ["include", "includeCode"]

kProjectPropertiesConfig["isMainProject"] = false
kProjectPropertiesConfig["isLibrary"] = false
kProjectPropertiesConfig["isExternalLibrary"] = true

kProjectPropertiesConfig["OnlyCompileCodeFiles"] = ["autofit.c", "ftbase.c", "bdf.c", "ftbzip2.c", "ftcache.c", "cff.c", "type1cid.c", "gxvalid.c", "ftgzip.c", "ftlzw.c", "otvalid.c", "pcf.c", "pfr.c", "psaux.c", "pshinter.c", "psnames.c", "raster.c", "sfnt.c", "smooth.c", "truetype.c", "type1.c", "type42.c", "winfnt.c"]

# VS generation related
kProjectPropertiesConfig["VSDisableWarnings"] = "4018;4244;4701;4702;4703;"

# ANDROID APK generation related
kProjectPropertiesConfig["androidTarget"] = $g_strAndroidTarget
kProjectPropertiesConfig["androidMinTarget"] = $g_strAndroidMinTarget

$g_kProjectPropertiesConfig.push kProjectPropertiesConfig
