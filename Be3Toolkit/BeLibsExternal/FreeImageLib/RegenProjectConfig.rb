####################################################################
##
## BAK ENGINE 3
## BUILD SYSTEM
## 
## RegenProjectConfig.rb
## This ruby prepares the main project properties
##
###################################################################

#!/bin/sh

# FREE IMAGE LIB CONFIG
kProjectPropertiesConfig = Hash.new
$g_bForceDebugInReleaseBuild = false

# COMMON Project stuff
kProjectPropertiesConfig["projectName"] = "FreeImageLib"
kProjectPropertiesConfig["projectPath"] = "BeLibsExternal/"
kProjectPropertiesConfig["projectExternalDependencies"] = ["ZLibLib"]
kProjectPropertiesConfig["projectExports"] = ["BUILDING_FREEIMAGE", "FREEIMAGE_LIB", "LIBRAW_NODLL", "LIBRAW_LIBRARY_BUILD", "OPJ_STATIC", "USE_JPIP", "IGNOREALL"]
kProjectPropertiesConfig["extraIncludeDirs"] = ["include", "includeCode"]

kProjectPropertiesConfig["isMainProject"] = false
kProjectPropertiesConfig["isLibrary"] = false
kProjectPropertiesConfig["isExternalLibrary"] = true

kProjectPropertiesConfig["AvoidCompileInclusiveCodeFiles"] = ["dcb_demosaicing.c", "aahd_demosaic.cpp", "dht_demosaic.cpp", "wf_filtering.cpp"]

# VS generation related
kProjectPropertiesConfig["VSDisableWarnings"] = "4005;4013;4018;4024;4047;4055;4057;4099;4100;4101;4102;4127;4130;4131;4132;4133;4146;4189;4204;4206;4218;4221;4244;4245;4251;4258;4267;4297;4309;4310;4389;4431;4456;4457;4458;4459;4477;4505;4510;4512;4515;4554;4610;4611;4701;4702;4703;4706;4715;4800;4804;4838;4996;"
kProjectPropertiesConfig["VSDisableLinkWarnings"] = ["4006", "4221"]
kProjectPropertiesConfig["VSAvoidCompile"] = ["jmemdos.c", "jmemmac.c", "jmemnobs.c", "jmemname.c", "tif_wince.c", "tif_vms.c"]

# ANDROID APK generation related
kProjectPropertiesConfig["androidTarget"] = $g_strAndroidTarget
kProjectPropertiesConfig["androidMinTarget"] = $g_strAndroidMinTarget

$g_kProjectPropertiesConfig.push kProjectPropertiesConfig
