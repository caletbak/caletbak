##########################################################
##
## BAK ENGINE 3
## FREE IMAGE LIB
##
##########################################################

#!/bin/bash

echo
echo "BAK ENGINE 3 BUILDING SYSTEM - RegenProjects.sh"
echo "Building the project files automatically for..."
echo

executeRubyScript () {
    "ruby" "${1}/BuildTools/ExecBuildingSystem.rb" "REGEN_PROJECT_EXTERNAL" $2
}

strMainBashParam=$1

strPrefixToFind="/Be3Toolkit"
strPathToBE3SDK="."
strPathToSearch="${strPathToBE3SDK}${strPrefixToFind}"
maxTries=10

for ((x=0; x<=maxTries; x++)); do
    strPathToBE3SDK="${strPathToBE3SDK}/.."
    strPathToSearch="${strPathToBE3SDK}${strPrefixToFind}"

    if [ -e "${strPathToSearch}" ]
    then
        executeRubyScript $strPathToSearch $strMainBashParam
        break
    fi

    if [ $x = $maxTries ]
    then
        echo "No Be3Toolkit found. Checkout BAK Engine 3 Toolkit and be sure the folder is called Be3Toolkit."
        echo
    fi
done
