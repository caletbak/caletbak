####################################################################
##
## BAK ENGINE 3
## BUILD SYSTEM
## 
## RegenProjectConfig.rb
## This ruby prepares the main project properties
##
###################################################################

#!/bin/sh

# ASSIMP LIB CONFIG
kProjectPropertiesConfig = Hash.new
$g_bForceDebugInReleaseBuild = false

# COMMON Project stuff
kProjectPropertiesConfig["projectName"] = "AssimpLib"
kProjectPropertiesConfig["projectPath"] = "BeLibsExternal/"
kProjectPropertiesConfig["projectExternalDependencies"] = ["ZLibLib"]
kProjectPropertiesConfig["projectExports"] = ["BUILDING_ASSIMP", "ASSIMP_BUILD_NO_OWN_ZLIB", "ASSIMP_BUILD_NO_C4D_IMPORTER", "OPENDDLPARSER_BUILD", "ASSIMP_BUILD_NO_GLTF_IMPORTER", "ASSIMP_BUILD_NO_GLTF_EXPORTER", "DISABLE_GLTF"]
kProjectPropertiesConfig["extraIncludeDirs"] = ["include", "includeCode"]

kProjectPropertiesConfig["isMainProject"] = false
kProjectPropertiesConfig["isLibrary"] = false
kProjectPropertiesConfig["isExternalLibrary"] = true

# VS generation related
kProjectPropertiesConfig["VSDisableWarnings"] = "4057;4100;4127;4131;4132;4133;4189;4201;4244;4245;4267;4273;4310;4389;4456;4457;4458;4459;4505;4510;4512;4610;4701;4702;4703;4706;4709;4714;4800;4996;"

# ANDROID APK generation related
kProjectPropertiesConfig["androidTarget"] = $g_strAndroidTarget
kProjectPropertiesConfig["androidMinTarget"] = $g_strAndroidMinTarget

$g_kProjectPropertiesConfig.push kProjectPropertiesConfig
