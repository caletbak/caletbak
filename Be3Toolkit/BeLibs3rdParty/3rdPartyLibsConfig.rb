####################################################################
##
## BAK ENGINE 3
## BUILD SYSTEM
## 
## 3rdPartyLibsConfig.rb
## This ruby prepares the 3rdParty libraries configuration
##
## Javier Calet Toledo - 2013
##
###################################################################

#!/bin/sh

# 3rd PARTY LIBS CONFIG

# Android APP GLUE LIB
kProjectPropertiesConfig = Hash.new
kProjectPropertiesConfig["projectName"] = "AndroidAppGlueLib"
kProjectPropertiesConfig["projectPath"] = "3rdParty/"
kProjectPropertiesConfig["projectDependencies"] = []
kProjectPropertiesConfig["projectVSAvoidLibraries"] = []
kProjectPropertiesConfig["is3rdPartyLib"] = true
kProjectPropertiesConfig["libraryType"] = ProjectProperties::LIBRARY_TYPE_STATIC
kProjectPropertiesConfig["libraryLinkWhole"] = true
kProjectPropertiesConfig["projectPlatformTargets"] = [PLATFORM_ANDROID]
kProjectPropertiesConfig["androidTarget"] = $g_strAndroidTarget
$g_k3rdPartyLibsPropertiesConfig.push kProjectPropertiesConfig
