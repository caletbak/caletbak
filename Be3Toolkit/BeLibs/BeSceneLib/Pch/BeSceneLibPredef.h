/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeSceneLibPredef.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_SCENELIBPREDEF_H
#define BE_SCENELIBPREDEF_H

// Global BE3 include
#include <BeCommon/BakEngine3Common.h>

#if TARGET_WINDOWS
#ifdef SCENE_IMPORTS
#  define RENDER_API __declspec(dllimport)
#  pragma message("automatic link to BeSceneLib.lib")
#  pragma comment(lib, "BeSceneLib.lib")
#else
#  define SCENE_API __declspec(dllexport)
#endif
#else
#  define SCENE_API
#endif

#ifdef _DEBUG
#  define BE_SCENE_DEBUG(x, ...) _OutputDebugString("[  SCENE DEBUG]: ", true, x, ##__VA_ARGS__)
#  define BE_SCENE_WARNING(x, ...) _OutputDebugString("[! SCENE WARNING]: ", true, x, ##__VA_ARGS__)
#  define BE_SCENE_ERROR(x, ...) _OutputDebugString("[* SCENE ERROR]: ", true, x, ##__VA_ARGS__)
#  define BE_SCENE_PRINT(x, ...) _OutputDebugString("", false, x, ##__VA_ARGS__)
#else
#  define BE_SCENE_DEBUG(x, ...) x
#  define BE_SCENE_WARNING(x, ...) x
#  define BE_SCENE_ERROR(x, ...) x
#  define BE_SCENE_PRINT(x, ...) x
#endif

#if defined(AS400) || defined(OS400)
  typedef pthread_id_np_t ThreadID;
#elif defined(VMS) 
  typedef pthread_t ThreadID;
#else
#  ifdef USE_BEGIN_THREAD
  typedef BeUInt ThreadID;
#  else
  typedef BeULong ThreadID;
#  endif
#endif

// External includes
#include <BeCoreLib/Pch/BeCoreLib.h>
#include <BeMathLib/Pch/BeMathLib.h>
#include <BeInputLib/Pch/BeInputLib.h>
#include <BeUtilityLib/Pch/BeUtilityLib.h>
#include <BeRenderLib/Pch/BeRenderLib.h>
#include <BeFontLib/Pch/BeFontLib.h>

#endif // #ifndef BE_SCENELIBPREDEF_H

