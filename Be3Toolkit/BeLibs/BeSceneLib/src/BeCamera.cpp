/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeCamera.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <BeCamera.h>

/////////////////////////////////////////////////////////////////////////////
BeCamera::BeCamera(const BeString8& strCameraName)
:
    m_strCameraName(strCameraName),
    m_eProjectionType(PROJECTION_ORTHOGRAPHIC),
    m_fFOV(45.0f)
{
    m_mViewMatrix.Identity();
    m_mProjectionMatrix.Identity();
    m_mViewProjectionMatrix.Identity();
}
/////////////////////////////////////////////////////////////////////////////
BeCamera::~BeCamera()
{

}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeCamera::UpdateMatrices(void)
{
    if (m_eProjectionType == PROJECTION_ORTHOGRAPHIC)
    {
        MakeOrtho(m_kScreenRect.m_fX, m_kScreenRect.m_fX + m_kScreenRect.m_fZ, m_kScreenRect.m_fY, m_kScreenRect.m_fY + m_kScreenRect.m_fW, -1, 1, m_mProjectionMatrix);
    }
    else if (m_eProjectionType == PROJECTION_PERSPECTIVE)
    {
    }

    m_mViewProjectionMatrix = m_mViewMatrix * m_mProjectionMatrix;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
const BeMatrix& BeCamera::GetViewMatrix(void)
{
    return m_mViewMatrix;
}
/////////////////////////////////////////////////////////////////////////////
const BeMatrix& BeCamera::GetProjectionMatrix(void)
{
    return m_mProjectionMatrix;
}
/////////////////////////////////////////////////////////////////////////////
const BeMatrix& BeCamera::GetViewProjectionMatrix(void)
{
    return m_mViewProjectionMatrix;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeCamera::SetScreenRect(const BeVector4D& kScreenRect)
{
    m_kScreenRect = kScreenRect;

    UpdateMatrices ();
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeCamera::MakeOrtho (BeFloat fLeft, BeFloat fRight, BeFloat fTop, BeFloat fBottom, BeFloat fNearZ, BeFloat fFarZ, BeMatrix& m)
{
    BeFloat ral = fRight + fLeft;
    BeFloat rsl = fRight - fLeft;
    BeFloat tab = fTop + fBottom;
    BeFloat tsb = fTop - fBottom;
    BeFloat fan = fFarZ + fNearZ;
    BeFloat fsn = fFarZ - fNearZ;

    m.m_fM11 = 2.0f / rsl;
    m.m_fM12 = 0.0f;
    m.m_fM13 = 0.0f;
    m.m_fM14 = 0.0f;

    m.m_fM21 = 0.0f;
    m.m_fM22 = 2.0f / tsb;
    m.m_fM23 = 0.0f;
    m.m_fM24 = 0.0f;

    m.m_fM31 = 0.0f;
    m.m_fM32 = 0.0f;
    m.m_fM33 = -2.0f / fsn;
    m.m_fM34 = 0.0f;

    m.m_fTx = -ral / rsl;
    m.m_fTy = -tab / tsb;
    m.m_fTz = -fan / fsn;
    m.m_fTw = 1.0f;
}
/////////////////////////////////////////////////////////////////////////////

