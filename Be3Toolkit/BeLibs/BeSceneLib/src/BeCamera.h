/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeCamera.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BECAMERA_H
#define BE_BECAMERA_H

#include <BeSceneLib/Pch/BeSceneLibPredef.h>

/////////////////////////////////////////////////
/// \class BeCamera
/// \brief High-level material object
/////////////////////////////////////////////////
class SCENE_API BeCamera : public BeMemoryObject
{

public:

    enum ECameraProjection
    {
        PROJECTION_PERSPECTIVE = 0,
        PROJECTION_ORTHOGRAPHIC
    };

    // Methods
    // {

        /// \brief  Constructor
        BeCamera (const BeString8& strCameraName);

        /// \brief  Destructor
        ~BeCamera ();

        /// \brief  Updates matrices
        void UpdateMatrices(void);

        /// \brief  Creates an orthographic matrix depending on input
        static void MakeOrtho (BeFloat fLeft, BeFloat fRight, BeFloat fTop, BeFloat fBottom, BeFloat fNearZ, BeFloat fFarZ, BeMatrix& m);

    // }



    // Getters
    // {

        /// \brief  Updates and returns the current view matrix
        const BeMatrix& GetViewMatrix(void);

        /// \brief  Updates and returns the current projection matrix
        const BeMatrix& GetProjectionMatrix (void);

        /// \brief  Returns the view*proj matrix
        const BeMatrix& GetViewProjectionMatrix(void);

    // }



    // Setters
    // {

        /// \brief  Sets the screen rect (useful for 2D orthographic camera) - X, Y, W, H
        void SetScreenRect (const BeVector4D& kScreenRect);

    // }

private:

    /// \brief  Camera name
    BeString8 m_strCameraName;

    /// \brief  Camera projection type
    ECameraProjection m_eProjectionType;

    /// \brief  Camera FOV
    BeFloat m_fFOV;

    /// \brief  Camera screen rect (useful for 2D orthographic camera) - X, Y, W, H
    BeVector4D m_kScreenRect;

    /// \brief  Camera current view matrix
    BeMatrix m_mViewMatrix;

    /// \brief  Camera current projection matrix
    BeMatrix m_mProjectionMatrix;

    /// \brief  Camera current projection matrix
    BeMatrix m_mViewProjectionMatrix;

};

#endif // #ifndef BE_BECAMERA_H
