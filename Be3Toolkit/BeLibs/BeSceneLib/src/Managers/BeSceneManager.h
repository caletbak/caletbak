/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeSceneManager.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BESCENEMANAGER_H
#define BE_BESCENEMANAGER_H

#include <BeSceneLib/Pch/BeSceneLibPredef.h>

// Forward delacations
class BeRenderDevice;
class BeCamera;

/////////////////////////////////////////////////
/// \class BeSceneManager
/// \brief Manager that controls the current scene
/////////////////////////////////////////////////
class SCENE_API BeSceneManager : public BeMemoryObject
{

public:

        /// \brief  Some default cameras being automatically created
        static BeString8 CAMERA_DEFAULT_UI;



        /// \brief  Constructor
        BeSceneManager();
        /// \brief  Destructor
        ~BeSceneManager();



    // Methods
    // {

        /// \brief  Initialises the scene with common resources
        void Initialise (void);

        /// \brief  Releases current defined cameras
        void ReleaseCameras (void);

        /// \brief  Adds and returns a new scene camera
        BeCamera* AddCamera (const BeString8& strCameraName);

    // }



    // Getters
    // {

        /// \brief  Returns the current camera being used
        BeCamera* GetCurrentCamera (void);

        /// \brief  Returns the desired camera
        BeCamera* GetCamera (const BeString8& strCameraName);

    // }



    // Setters
    // {

        /// \brief  Sets the current render device to be used for rendering
        void SetRenderDevice(BeRenderDevice* pRenderDevice);

        /// \brief  Sets the current camera to use
        void SetCurrentCamera (const BeString8& strCameraName);

    // }

  private:

      /// \brief  The Render device pointer
      BeRenderDevice* m_pRenderDevice;

      /// \brief  The defined scene cameras
      std::map<BeString8, BeCamera*> m_kCameras;

      /// \brief  The current scene camera being used
      BeCamera* m_pCurrentCamera;

};



#endif // #ifndef BE_BESCENEMANAGER_H

