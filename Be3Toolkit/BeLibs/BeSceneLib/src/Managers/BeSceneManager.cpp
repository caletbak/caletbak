/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeCameraManager.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <Managers/BeSceneManager.h>
#include <Render/BeRenderDevice.h>
#include <BeCamera.h>



/////////////////////////////////////////////////////////////////////////////
BeString8 BeSceneManager::CAMERA_DEFAULT_UI = "CAMERA_DEFAULT_UI";
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeSceneManager::BeSceneManager()
:
    m_pRenderDevice(NULL),
    m_pCurrentCamera(NULL)
{
}
/////////////////////////////////////////////////////////////////////////////
BeSceneManager::~BeSceneManager()
{
    m_pCurrentCamera = NULL;

    ReleaseCameras ();
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeSceneManager::Initialise(void)
{
    ReleaseCameras();

    // We add a default camera for UI
    AddCamera(CAMERA_DEFAULT_UI);
}
/////////////////////////////////////////////////////////////////////////////
void BeSceneManager::ReleaseCameras()
{
    std::map<BeString8, BeCamera*>::iterator it = m_kCameras.begin();
    while (it != m_kCameras.end())
    {
        BeDelete it->second;

        it++;
    }
    m_kCameras.clear();
}
/////////////////////////////////////////////////////////////////////////////
BeCamera* BeSceneManager::AddCamera(const BeString8& strCameraName)
{
    std::map<BeString8, BeCamera*>::const_iterator it = m_kCameras.find(strCameraName);
    if (it == m_kCameras.end())
    {
        BeCamera* newCamera = BeNew BeCamera (strCameraName);

        const BeRenderTypes::BeRenderDeviceSettings& kRenderSettings = m_pRenderDevice->GetRenderDeviceSettings();

        newCamera->SetScreenRect(BeVector4D(0.0f, 0.0f, (BeFloat)kRenderSettings.m_uBufferWidth, (BeFloat)kRenderSettings.m_uBufferHeight));

        m_kCameras[strCameraName] = newCamera;

        return newCamera;
    }

    BE_RENDER_DEBUG ("The camera with name %s already exists.", strCameraName.ToRaw());

    return NULL;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeCamera* BeSceneManager::GetCurrentCamera(void)
{
    return m_pCurrentCamera;
}
/////////////////////////////////////////////////////////////////////////////
BeCamera* BeSceneManager::GetCamera(const BeString8& strCameraName)
{
    std::map<BeString8, BeCamera*>::const_iterator it = m_kCameras.find(strCameraName);
    if (it != m_kCameras.end())
    {
        return it->second;
    }

    return NULL;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeSceneManager::SetRenderDevice(BeRenderDevice* pRenderDevice)
{
    m_pRenderDevice = pRenderDevice;

    Initialise ();
}
/////////////////////////////////////////////////////////////////////////////
void BeSceneManager::SetCurrentCamera(const BeString8& strCameraName)
{
    std::map<BeString8, BeCamera*>::const_iterator it = m_kCameras.find(strCameraName);
    if (it != m_kCameras.end())
    {
        m_pCurrentCamera = it->second;
    }
}
/////////////////////////////////////////////////////////////////////////////

