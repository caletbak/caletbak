/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BakEngine3.h
//
// Include this file into your project
/////////////////////////////////////////////////////////////////////////////

#ifndef BAKENGINE3_H
#define BAKENGINE3_H

#include <BeCoreLib/Pch/BeCoreLib.h>
#include <BeMathLib/Pch/BeMathLib.h>
#include <BeUtilityLib/Pch/BeUtilityLib.h>
#include <BeRenderLib/Pch/BeRenderLib.h>
#include <BeSceneLib/Pch/BeSceneLib.h>
#include <BeInputLib/Pch/BeInputLib.h>
#include <BeApplicationLib/Pch/BeApplicationLib.h>

#endif // #ifndef BAKENGINE3_H

