/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeApplicationConfig.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_APPLICATIONCONFIG_H
#define BE_APPLICATIONCONFIG_H

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <BeApplicationLib/Pch/BeApplicationLibPredef.h>

//#include <BeGraphics.h>

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class BeApplicationConfig
{
public:

    BeApplicationConfig()
    :
        m_bIsAppInitialised(false),
        m_onAppDidLaunch(NULL),
        m_onAppWillTerminate(NULL),
        m_onAppUpdate(NULL),
        m_onHandleInput(NULL),
        m_onHandlePinchGesture(NULL),
        m_onHandleRotationGesture(NULL),
        m_setDataPath(NULL),
        m_setCachePath(NULL)
    {
    }
    BeApplicationConfig(const BeApplicationConfig &appConfig)
    {
        m_onAppDidLaunch                = appConfig.m_onAppDidLaunch;
        m_onAppWillTerminate            = appConfig.m_onAppWillTerminate;
        m_onAppUpdate                   = appConfig.m_onAppUpdate;
        m_onHandleInput                 = appConfig.m_onHandleInput;
        m_onHandlePinchGesture          = appConfig.m_onHandlePinchGesture;
        m_onHandleRotationGesture       = appConfig.m_onHandleRotationGesture;
        m_setDataPath                   = appConfig.m_setDataPath;
        m_setCachePath                  = appConfig.m_setCachePath;

        m_strAppName                    = appConfig.m_strAppName;
        m_strAppVersion                 = appConfig.m_strAppVersion;
        m_uMaximumUpdateRate            = appConfig.m_uMaximumUpdateRate;
        m_bIsAppInitialised             = appConfig.m_bIsAppInitialised;
        //m_graphicsConfig              = appConfig.m_graphicsConfig;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    bool(*m_onAppDidLaunch) (void);
    void(*m_onAppWillTerminate) (void);
    void(*m_onAppUpdate) (void);
    bool(*m_onHandleInput) (BeVoidP pInputEvent); // AInputEvent
    void(*m_onHandlePinchGesture) (BeFloat, BeFloat);
    void(*m_onHandleRotationGesture) (BeFloat, BeBoolean);
    void(*m_setDataPath) (const BeString8&);
    void(*m_setCachePath) (const BeString8&);

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    void SetAppName(const BeString8& strName)
    {
        m_strAppName = strName;
    }

    void SetAppVersion(const BeString8& strVersion)
    {
        m_strAppVersion = strVersion;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    BeString8 m_strAppName;
    BeString8 m_strAppVersion;

    BeUInt m_uMaximumUpdateRate; // Number of updates per second

    bool m_bIsAppInitialised;

#if TARGET_ANDROID
    BeGraphics::BeEGLGraphicsConfig m_kGraphicsConfig;
#endif
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

extern void ApplicationSetConfiguration(BeApplicationConfig &config);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#endif // BE_APPLICATIONCONFIG_H

