/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeModule.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEMODULE_H
#define BE_BEMODULE_H

#include <BeApplicationLib/Pch/BeApplicationLibPredef.h>

#include <BeApplicationTypes.h>

// Forward declarations
class BeRenderDevice;

/////////////////////////////////////////////////
/// \class BeModule
/// \brief Logic and rendering application module
/////////////////////////////////////////////////
class APPLICATION_API BeModule : public BeMemoryObject
{
      
  public:
    
    /// \brief  Constructor
    BeModule();

    /// \brief  Destructor
    ~BeModule();


    // Getters
    // {

      ///  \brief  Returns the module ID
      BeInt GetModuleID(void);

    // }


    // Methods
    // {

      /// \brief  This is called first when the module has been loaded
      virtual void LoadModule(void);



      /// \brief  Performs a pre update process
      virtual void PreUpdate(const BeApplicationTypes::BeContextUpdate& kContextUpdate);

      /// \brief  Performs the update process
      virtual BeInt Update(const BeApplicationTypes::BeContextUpdate& kContextUpdate) = 0;

      /// \brief  Performs a post update process
      virtual void PostUpdate(const BeApplicationTypes::BeContextUpdate& kContextUpdate);



      /// \brief  Performs a pre rendering process
      virtual void PreRender(BeRenderDevice* pRenderDevice);

      /// \brief  Performs the rendering process
      virtual void Render(BeRenderDevice* pRenderDevice) = 0;

      /// \brief  Performs a post rendering process
      virtual void PostRender(BeRenderDevice* pRenderDevice);



      /// \brief  This is called when exiting the module
      virtual void UnLoadModule(void);
          
    // }
  
  private:

    /// \brief  The next module ID to be assigned
    static BeInt m_iNextModuleID;

    /// \brief  The module ID defined when it is added to the logic modules manager
    BeInt m_iModuleID;

  protected:

    /// \brief  The next module ID can be used for changing to a different module when it is returned in the module Update
    BeInt m_nextModule;



  friend class BeModulesManager;

};

// Smart pointer BeModulePtr
typedef BeSmartPointer<BeModule> BeModulePtr;

#endif // #ifndef BE_BEMODULE_H
