/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeModule.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <Logic/BeModule.h>

/////////////////////////////////////////////////////////////////////////////
BeInt BeModule::m_iNextModuleID = 1;
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeModule::BeModule()
{
  m_iModuleID = m_iNextModuleID++;
  m_nextModule = 0;
}
/////////////////////////////////////////////////////////////////////////////
BeModule::~BeModule()
{
}
/////////////////////////////////////////////////////////////////////////////
void BeModule::LoadModule(void)
{
}
/////////////////////////////////////////////////////////////////////////////
void BeModule::PreUpdate(const BeApplicationTypes::BeContextUpdate& kContextUpdate)
{
  BE_UNUSED(kContextUpdate);
}
/////////////////////////////////////////////////////////////////////////////
void BeModule::PostUpdate(const BeApplicationTypes::BeContextUpdate& kContextUpdate)
{
  BE_UNUSED(kContextUpdate);
}
/////////////////////////////////////////////////////////////////////////////
void BeModule::PreRender(BeRenderDevice* pRenderDevice)
{
  BE_UNUSED(pRenderDevice);
}
/////////////////////////////////////////////////////////////////////////////
void BeModule::PostRender(BeRenderDevice* pRenderDevice)
{
  BE_UNUSED(pRenderDevice);
}
/////////////////////////////////////////////////////////////////////////////
void BeModule::UnLoadModule(void)
{
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeInt BeModule::GetModuleID(void)
{
  return m_iModuleID;
}
/////////////////////////////////////////////////////////////////////////////

