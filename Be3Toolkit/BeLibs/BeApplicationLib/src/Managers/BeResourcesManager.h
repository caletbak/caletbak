/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeResourcesManager.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BERESOURCESMANAGER_H
#define BE_BERESOURCESMANAGER_H

#include <BeApplicationLib/Pch/BeApplicationLibPredef.h>

// Forward declarations

/////////////////////////////////////////////////
/// \class BeResourcesManager
/// \brief Manager that loads disk content as thread
/////////////////////////////////////////////////
class APPLICATION_API BeResourcesManager : public BeThread
{
      
  public:
    
    /// \brief  Constructor
    BeResourcesManager(void);
    /// \brief  Destructor
    ~BeResourcesManager();

    // Setters
    // {
                        
    // }



    // Methods
    // {

      /// \brief  Called when an event occurs
      virtual BeBoolean OnTask(BeVoidP lpvData);

      /// \brief  Called when a time interval has elapsed
      virtual BeBoolean OnTask(void);

      /// \brief  Adds a new resource to load
      void AddLoadEvent( BeCoreTypes::ELoadResourceEvents eResourceType, BeString16 strFileName, BeCoreTypes::ResourcesInterface* pCallBackFnc );
                  
    // }

  private:
    
    /// \class BeResourceLoadEvent
    /// \brief An object containing the data for a loaded resource
    class APPLICATION_API BeResourceLoadEvent : public BeMemoryObject
    {
      
      public:

        /// \brief  Constructor
        BeResourceLoadEvent(BeString16 strFileName, BeCoreTypes::ResourcesInterface* pCallBackFnc);
        /// \brief  Copy Constructor
        BeResourceLoadEvent(const BeResourceLoadEvent& kResourceLoadEvent);
        /// \brief  Destructor
        ~BeResourceLoadEvent();



        /// \brief  The resource filename to be loaded
        BeString16 m_strFileName;

        /// \brief  The resource filename to be loaded
        BeCoreTypes::ResourcesInterface* m_pCallBackFnc;

        /// \brief  The resource loaded buffer
        BeVoidP m_pLoadedBuffer;

        /// \brief  The resource loaded buffer size
        BeInt m_pLoadedBufferSize;

        /// \brief  Extra params for resource
        int m_iExtraParam1;

    };
    typedef BeSmartPointer<BeResourceLoadEvent> BeResourceLoadEventPtr;



    // Methods
    // {

      /// \brief  Adds a new resource to be finished
      void AddResourceFinishEvent( BeCoreTypes::EResourceFinishEvents eResourceType, BeResourceLoadEventPtr spResourceEvent );

    // }


    
    /// \brief  The resources array to be loaded array type
    typedef std::vector<BeResourceLoadEventPtr> BeResourcesToLoadArray;
    
    /// \brief  The resources array to be loaded in rendering process
    BeResourcesToLoadArray m_kResourcesToLoad[BeCoreTypes::LOAD_RESOURCE_TYPE_COUNT];

    /// \brief  The resources array to be finished in update process
    BeResourcesToLoadArray m_kResourceFinishEvents[BeCoreTypes::RESOURCE_FINISH_EVENTS_COUNT];

};

// Smart pointer BeResourcesManager
typedef BeSmartPointer<BeResourcesManager> BeResourcesManagerPtr;

#endif // #ifndef BE_BERESOURCESMANAGER_H
