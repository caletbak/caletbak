/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeResourcesManager.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <Managers/BeResourcesManager.h>
#include <BeApplicationTypes.h>

/////////////////////////////////////////////////////////////////////////////
BeResourcesManager::BeResourceLoadEvent::BeResourceLoadEvent(BeString16 strFileName, BeCoreTypes::ResourcesInterface* pCallBackFnc)
:
  m_strFileName(strFileName),
  m_pCallBackFnc(pCallBackFnc),
  m_pLoadedBuffer(NULL),
  m_pLoadedBufferSize(0)
{
}
/////////////////////////////////////////////////////////////////////////////
BeResourcesManager::BeResourceLoadEvent::BeResourceLoadEvent(const BeResourceLoadEvent& kResourceLoadEvent)
:
  m_strFileName(kResourceLoadEvent.m_strFileName),
  m_pCallBackFnc(kResourceLoadEvent.m_pCallBackFnc),
  m_pLoadedBuffer(kResourceLoadEvent.m_pLoadedBuffer),
  m_pLoadedBufferSize(kResourceLoadEvent.m_pLoadedBufferSize)
{
}
/////////////////////////////////////////////////////////////////////////////
BeResourcesManager::BeResourceLoadEvent::~BeResourceLoadEvent()
{
  if ( m_pLoadedBuffer != NULL )
  {
    BeFreeMemory(m_pLoadedBuffer);
    m_pLoadedBuffer = NULL;
  }
  m_pCallBackFnc = NULL;
  m_pLoadedBufferSize = 0;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeResourcesManager::BeResourcesManager(void)
{
  // We set the thread type and interval
  SetThreadType( BeThread::THREAD_TYPE_SPECIALIZED, 0 );

  m_kMutex.Lock();

  for ( BeInt i=0; i < BeCoreTypes::RESOURCE_FINISH_EVENTS_COUNT; ++i )
    m_kResourceFinishEvents[i].clear();
  
  for ( BeInt i=0; i < BeCoreTypes::LOAD_RESOURCE_TYPE_COUNT; ++i )
    m_kResourcesToLoad[i].clear();

  m_kMutex.UnLock();
}
/////////////////////////////////////////////////////////////////////////////
BeResourcesManager::~BeResourcesManager()
{
  m_kMutex.Lock();

  for ( BeInt i=0; i < BeCoreTypes::RESOURCE_FINISH_EVENTS_COUNT; ++i )
    m_kResourceFinishEvents[i].clear();
  
  for ( BeInt i=0; i < BeCoreTypes::LOAD_RESOURCE_TYPE_COUNT; ++i )
    m_kResourcesToLoad[i].clear();

  m_kMutex.UnLock();
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeBoolean BeResourcesManager::OnTask(BeVoidP lpvData)
{
  BE_UNUSED(lpvData);

  BeApplicationTypes::BeResourceOnTaskEvent* pOnTaskResourceEvent = static_cast<BeApplicationTypes::BeResourceOnTaskEvent*>(lpvData);

  //BE_ENGINE_DEBUG("BeResourcesManager::OnTask(BeVoidP lpvData)     type: %d    time: %d", pOnTaskResourceEvent->m_eTaskType, pOnTaskResourceEvent->m_iTimeToProcessTask);

  switch(pOnTaskResourceEvent->m_eTaskType)
  {

    case BeApplicationTypes::BeResourceOnTaskEvent::TASK_ON_UPDATE_PROCESS:
    {
      
      for ( BeInt i = 0; i < BeCoreTypes::RESOURCE_FINISH_EVENTS_COUNT; ++i )
      {
        while ( m_kResourceFinishEvents[i].size() > 0 )
        {
          BeResourceLoadEventPtr spResourceToFinish( m_kResourceFinishEvents[i].at( m_kResourceFinishEvents[i].size()-1 ) );
          spResourceToFinish->m_pCallBackFnc->ProcessLoadedEvent(spResourceToFinish->m_strFileName, spResourceToFinish->m_pLoadedBuffer, spResourceToFinish->m_pLoadedBufferSize, spResourceToFinish->m_iExtraParam1);
          m_kResourceFinishEvents[i].pop_back();
        }
      }

      break;
    }
    case BeApplicationTypes::BeResourceOnTaskEvent::TASK_ON_RENDERING_PROCESS:
    {

      for ( BeInt i = 0; i < BeCoreTypes::LOAD_RESOURCE_TYPE_COUNT; ++i )
      {

        while ( m_kResourcesToLoad[i].size() > 0 )
        {

          BeResourceLoadEventPtr spResourceToLoad( m_kResourcesToLoad[i].at( m_kResourcesToLoad[i].size()-1 ) );
          spResourceToLoad->m_pLoadedBuffer = BeFileTool::ReadEntireFile( spResourceToLoad->m_strFileName, &spResourceToLoad->m_pLoadedBufferSize );

          if ( spResourceToLoad->m_pLoadedBuffer != NULL )
          {

            switch (i)
            {
              case BeCoreTypes::LOAD_RESOURCE_TEXTURE_DATA:
              {
                AddResourceFinishEvent( BeCoreTypes::DATA_TEXTURE_TO_DEVICE, spResourceToLoad );
                break;
              }
            }

          }
          else
          {
            BE_APPLICATION_DEBUG( "The File %s does not exist.", ConvertString16To8(spResourceToLoad->m_strFileName).ToRaw() );
          }

          m_kResourcesToLoad[i].pop_back();

        }

      }

      break;
    }

  }

  pOnTaskResourceEvent = NULL;

  return BE_TRUE;
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeResourcesManager::OnTask(void)
{

  //BE_ENGINE_DEBUG("BeResourcesManager::OnTask");

  return BE_TRUE;
}
/////////////////////////////////////////////////////////////////////////////
void BeResourcesManager::AddLoadEvent( BeCoreTypes::ELoadResourceEvents eResourceType, BeString16 strFileName, BeCoreTypes::ResourcesInterface* pCallBackFnc )
{
  BeResourcesManager::BeResourceLoadEventPtr spLoadEvent( BeNew BeResourcesManager::BeResourceLoadEvent( strFileName, pCallBackFnc ) );
  m_kMutex.Lock();
  m_kResourcesToLoad[(BeInt)eResourceType].push_back( spLoadEvent );
  m_kMutex.UnLock();
}
/////////////////////////////////////////////////////////////////////////////
void BeResourcesManager::AddResourceFinishEvent( BeCoreTypes::EResourceFinishEvents eResourceType, BeResourceLoadEventPtr spResourceEvent )
{
  m_kMutex.Lock();
  m_kResourceFinishEvents[(BeInt)eResourceType].push_back( spResourceEvent );
  m_kMutex.UnLock();
}
/////////////////////////////////////////////////////////////////////////////

