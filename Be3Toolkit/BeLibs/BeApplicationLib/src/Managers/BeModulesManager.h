/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeModulesManager.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEMODULESMANAGER_H
#define BE_BEMODULESMANAGER_H

#include <BeApplicationLib/Pch/BeApplicationLibPredef.h>

#include <Logic/BeModule.h>

// Forward declarations
class BeRenderManager;

/////////////////////////////////////////////////
/// \class BeModulesManager
/// \brief Manager that controls the current module running
/////////////////////////////////////////////////
class APPLICATION_API BeModulesManager : public BeMemoryObject
{
  public:
    
    /// \brief  Constructor
    BeModulesManager(void);
    /// \brief  Destructor
    ~BeModulesManager();

    // Setters
    // {

    // }



    // Methods
    // {

      /// \brief  Registry a new module to the state machine
      void RegistryModule (BeModule* pModule);

      /// \brief  Changes the current module running
      void ChangeModule (BeInt iModuleID);

      /// \brief  Updates the state machine
      void Update (void);

      /// \brief  Performs the update calling to PreUpdate, Update and PostUpdate methods of the current module
      BeInt PerformUpdateProcess (void);

      /// \brief  Performs the render calling to PreRender, Render and PostRender methods of the current module
      BeInt PerformRenderProcess (BeRenderManager* pRenderManager);

    // }

  private:

    /// \brief  The context update
    BeApplicationTypes::BeContextUpdate m_kContextUpdate;

    /// \brief  The current logic module to be updated
    BeModule* m_pCurrentModule;

    /// \brief  The next module to be loaded in the state machine
    BeInt m_nextModule;

    /// \brief  The modules array type
    typedef std::vector<BeModule*> ModulesArray;

    /// \brief  The processed string tokens
    ModulesArray m_kRegisteredModules;
};

// Smart pointer BeModulesManagerPtr
typedef BeSmartPointer<BeModulesManager> BeModulesManagerPtr;

#endif // #ifndef BE_BEMODULESMANAGER_H
