/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeModulesManager.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <Managers/BeRenderManager.h>
#include <Managers/BeModulesManager.h>

/////////////////////////////////////////////////////////////////////////////
BeModulesManager::BeModulesManager(void)
:
  m_pCurrentModule(NULL),
  m_nextModule(0)
{
  m_kRegisteredModules.push_back (NULL);
}
/////////////////////////////////////////////////////////////////////////////
BeModulesManager::~BeModulesManager()
{
  m_pCurrentModule = NULL;
  m_nextModule = 0;
  m_kRegisteredModules.clear ();
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeInt BeModulesManager::PerformRenderProcess (BeRenderManager* pRenderManager)
{
  BeInt fInitTime = GetCurrentTimeMS();

  if ( m_pCurrentModule != NULL )
  {
    BeRenderDevice* pRenderDevice = pRenderManager->GetRenderDevice();
    if ( pRenderDevice != NULL )
    {
      // Performs the pre render pass  
      m_pCurrentModule->PreRender(pRenderDevice);

      // Performs the render pass  
      m_pCurrentModule->Render(pRenderDevice);

      // Performs the post render pass  
      m_pCurrentModule->PostRender(pRenderDevice);
    }
  }

  return GetCurrentTimeMS() - fInitTime;
}
/////////////////////////////////////////////////////////////////////////////
BeInt BeModulesManager::PerformUpdateProcess (void)
{
  BeInt fInitTime = GetCurrentTimeMS();

  if ( m_pCurrentModule != NULL )
  {
    // Performs the pre update pass  
    m_pCurrentModule->PreUpdate (m_kContextUpdate);

    // Performs the update pass  
    m_nextModule = m_pCurrentModule->Update (m_kContextUpdate);

    // Performs the post update pass  
    m_pCurrentModule->PostUpdate (m_kContextUpdate);
  }

  return GetCurrentTimeMS() - fInitTime;
}
/////////////////////////////////////////////////////////////////////////////
void BeModulesManager::RegistryModule (BeModule* pModule)
{
  m_kRegisteredModules.push_back (pModule);
}
/////////////////////////////////////////////////////////////////////////////
void BeModulesManager::ChangeModule (BeInt iModuleID)
{
  if (m_pCurrentModule)
  {
    m_pCurrentModule->UnLoadModule ();
    m_pCurrentModule->m_nextModule = 0;
  }

  if (iModuleID > 0)
  {
    m_pCurrentModule = m_kRegisteredModules.at (iModuleID);

    if (m_pCurrentModule)
    {
      m_pCurrentModule->LoadModule ();
    }
  }
}
/////////////////////////////////////////////////////////////////////////////
void BeModulesManager::Update (void)
{
  if (m_nextModule != 0)
  {
    // We need to unload the current module for loading the next one
    ChangeModule (m_nextModule);
    m_nextModule = 0;
  }
}
/////////////////////////////////////////////////////////////////////////////
