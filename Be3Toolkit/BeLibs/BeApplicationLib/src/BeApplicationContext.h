/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeApplicationContext.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEAPPLICATIONCONTEXT_H
#define BE_BEAPPLICATIONCONTEXT_H

#include <BeApplicationLib/Pch/BeApplicationLibPredef.h>

// Forward declarations
class BeRenderDevice;
class BeDevicesManager;
class BeModulesManager;
class BeRenderManager;
class BeResourcesManager;
class BeInputManager;
class BeSceneManager;

/////////////////////////////////////////////////
/// \class BeApplicationContext
/// \brief This is the Engine context in which all the necessary resources are created
/////////////////////////////////////////////////
class APPLICATION_API BeApplicationContext : public BeMemoryObject
{

  public:

    /// \brief Constructor
    BeApplicationContext(void);
    
    /// \brief Destructor
    ~BeApplicationContext();



    // \brief Getters
    // {

      /// \brief  Returns a singleton instance
      static BeApplicationContext* GetInstance(void);
      
      /// \brief  Returns the Devices Manager
      BeDevicesManager* GetDevicesManager(void);

      /// \brief  Returns the Logic Modules Manager
      BeModulesManager* GetModulesManager(void);

      /// \brief  Returns the Rendering Manager
      BeRenderManager* GetRenderManager(void);

      /// \brief  Returns the Resources thread Manager
      BeResourcesManager* GetResourcesManager(void);

      /// \brief  Returns the Input Manager
      BeInputManager* GetInputManager (void);

      /// \brief  Returns the scene manager
      BeSceneManager* GetSceneManager(void);

      /// \brief  Returns the Audio System
      BeAudioSystem* GetAudioSystem (void);

      /// \brief  Returns the application window
      BeWindowHandle GetAppWindow(void);

    // }



    // \brief Setters
    // {

      /// \brief  Sets the Frame rate
      void SetFrameRate(BeInt iFrameRate);

    // }



    // \brief Methods
    // {

      /// \brief This is a hack function to make work the Bin .exe files using the same relative path base as when you are debugging from the IDE
      /// It searches for a directory name (only name, not path) in previous directories to the current one
      /// The algorithm will go uRecursiveCount times back in directories searching for the directory name
      /// The idea is to have the Data folder as a previous Directory to Bin and Project folders
      static void CheckForCurrentDirectory(BeString16 strDirectoryName, BeUInt uRecursiveCount);
      
      /// \brief Creates and configures the render device using settings struct
      BeRenderDevice* ConfigureRenderDevice(const BeRenderTypes::BeRenderDeviceSettings& kRenderDeviceSettings);
      /// \brief Creates and configures the render device using XML config file
      BeRenderDevice* ConfigureRenderDevice(const BeString16& strConfigFilePath);



      /// \brief Performs the engine update process
      void Update(void);

      /// \brief  Init the static engine resources
      static void InitStaticResources(void);

      /// \brief  Destroy the static engine resources
      static void DestroyStaticResources(void);
    
    // }

  private:

    /// \brief  The Application Instance
    BeInstance m_kAppInstance;

    /// \brief  The Devices Manager
    BeDevicesManager* m_pDevicesManager;

    /// \brief  The Logic Modules Manager
    BeModulesManager* m_pModulesManager;

    /// \brief  The Rendering Manager
    BeRenderManager* m_pRenderManager;

    /// \brief  The Resources thread manager
    BeResourcesManager* m_pResourcesManager;

    /// \brief  The Input manager
    BeInputManager* m_pInputManager;

    /// \brief  The scene manager
    BeSceneManager* m_pSceneManager;

    /// \brief  The Audio system
    BeAudioSystem* m_pAudioSystem;



    /// \brief  The Frame Rate
    BeInt m_iFrameRate;

    /// \brief  The Frame Interval time in MS calculated using the frame rate
    BeInt m_iFrameIntervalMs;

    /// \brief  The Half Frame Interval time in MS calculated using the frame rate
    BeInt m_iFrameHalfIntervalMs;

    /// \brief  The last update time in MS
    BeInt m_iLastUpdateTimeMS;
    /// \brief  The last rendering time in MS
    BeInt m_iLastRenderingTimeMS;



    /// \brief  The static instance for singleton instantiation
    static BeApplicationContext* m_pApplicationContextInstance;

};

#include <BeApplicationContext.inl>

#endif // #ifndef BE_BEAPPLICATIONCONTEXT_H
