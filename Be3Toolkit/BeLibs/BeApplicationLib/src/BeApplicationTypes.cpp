/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeApplicationTypes.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <BeApplicationTypes.h>



/////////////////////////////////////////////////////////////////////////////
BeApplicationTypes::BeContextUpdate::BeContextUpdate()
:
  m_uCurrentTime(0),
  m_uLastTime(0),
  m_uDeltaTimeMs(0),
  m_fDeltaTime(0.0f)
{
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeApplicationTypes::BeResourceOnTaskEvent::BeResourceOnTaskEvent()
:
  m_eTaskType(TASK_ON_UNKNOWN),
  m_iTimeToProcessTask(0)
{
}
/////////////////////////////////////////////////////////////////////////////
BeApplicationTypes::BeResourceOnTaskEvent::BeResourceOnTaskEvent(EOnTaskEventType eTaskType, BeInt iTimeToProcessTask)
:
  m_eTaskType(eTaskType),
  m_iTimeToProcessTask(iTimeToProcessTask)
{
}
/////////////////////////////////////////////////////////////////////////////
BeApplicationTypes::BeResourceOnTaskEvent::~BeResourceOnTaskEvent()
{
}
/////////////////////////////////////////////////////////////////////////////


