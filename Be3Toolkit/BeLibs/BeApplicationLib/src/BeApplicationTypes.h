/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeApplicationTypes.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_APPLICATIONTYPES_H
#define BE_APPLICATIONTYPES_H

#include <BeApplicationLib/Pch/BeApplicationLibPredef.h>

/////////////////////////////////////////////////
/// \class BeApplicationTypes
/// \brief Application project types
/////////////////////////////////////////////////
class APPLICATION_API BeApplicationTypes
{
      
  public:
    
    /// \brief  Update context to be used in every update process
    class APPLICATION_API BeContextUpdate
    {

      public:

        /// \brief  Constructor
        BeContextUpdate();
        
        /// \brief  Current update application time (ms)
        BeUInt m_uCurrentTime;
        /// \brief  Last update application time (ms)
        BeUInt m_uLastTime;
        /// \brief  Delta time in ms
        BeUInt m_uDeltaTimeMs;
        /// \brief  Delta time divided by 1 second
        BeFloat m_fDeltaTime;

    };

    /// \brief  An object for identifying the OnTask process to be done in the resources manager
    class APPLICATION_API BeResourceOnTaskEvent : public BeMemoryObject
    {
      
      public:

        enum EOnTaskEventType
        {
          TASK_ON_UPDATE_PROCESS,
          TASK_ON_RENDERING_PROCESS,

          TASK_ON_UNKNOWN
        };

        /// \brief  Constructor
        BeResourceOnTaskEvent();
        /// \brief  Constructor
        BeResourceOnTaskEvent(EOnTaskEventType eTaskType, BeInt iTimeToProcessTask);
        /// \brief  Destructor
        ~BeResourceOnTaskEvent();



        /// \brief  The task type
        EOnTaskEventType m_eTaskType;

        /// \brief  The max time for processing the thread task
        BeInt m_iTimeToProcessTask;

    };
            
};

#endif // #ifndef BE_APPLICATIONTYPES_H
