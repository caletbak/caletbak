/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeEngineContext.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <BeApplicationContext.h>

#include <Managers/BeModulesManager.h>
#include <Managers/BeResourcesManager.h>
#include <Managers/BeInputManager.h>

#if TARGET_WINDOWS
#pragma comment (lib, "Shlwapi.lib") 
#include "Shlwapi.h"
#endif

/////////////////////////////////////////////////////////////////////////////
APPLICATION_API BeApplicationContext* BeApplicationContext::m_pApplicationContextInstance = NULL;
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
BeApplicationContext::BeApplicationContext(void)
:
  m_kAppInstance(NULL),
  m_pDevicesManager(NULL),
  m_pModulesManager(NULL),
  m_pRenderManager(NULL),
  m_pResourcesManager(NULL),
  m_pInputManager(NULL),
  m_pSceneManager(NULL),
  m_iFrameRate(0),
  m_iFrameIntervalMs(0),
  m_iFrameHalfIntervalMs(0),
  m_iLastUpdateTimeMS(0),
  m_iLastRenderingTimeMS(0)
{  
#if TARGET_WINDOWS
  m_kAppInstance = (BeInstance)GetModuleHandle(NULL);
#endif

  BeApplicationContext::InitStaticResources();

  m_pDevicesManager = BeNew BeDevicesManager();
  m_pModulesManager = BeNew BeModulesManager();
  m_pRenderManager = BeNew BeRenderManager();
  m_pResourcesManager = BeNew BeResourcesManager();
  m_pInputManager = BeNew BeInputManager ();
  m_pSceneManager = BeNew BeSceneManager();
  m_pAudioSystem = BeNew BeAudioSystem ();

  SetFrameRate(60);
}
/////////////////////////////////////////////////////////////////////////////
BeApplicationContext::~BeApplicationContext()
{
  m_kAppInstance = NULL;
  if ( m_pDevicesManager != NULL )
  {
    BeDelete m_pDevicesManager;
    m_pDevicesManager = NULL;
  }
  if ( m_pModulesManager != NULL )
  {
    BeDelete m_pModulesManager;
    m_pModulesManager = NULL;
  }
  if ( m_pRenderManager != NULL )
  {
    BeDelete m_pRenderManager;
    m_pRenderManager = NULL;
  }
  if ( m_pResourcesManager != NULL )
  {
    BeDelete m_pResourcesManager;
    m_pResourcesManager = NULL;
  }
  if ( m_pInputManager != NULL )
  {
    BeDelete m_pInputManager;
    m_pInputManager = NULL;
  }
  if (m_pSceneManager != NULL)
  {
      BeDelete m_pSceneManager;
      m_pSceneManager = NULL;
  }
  if ( m_pAudioSystem != NULL )
  {
    BeDelete m_pAudioSystem;
    m_pAudioSystem = NULL;
  }
}
/////////////////////////////////////////////////////////////////////////////
BeApplicationContext* BeApplicationContext::GetInstance(void)
{
  if ( m_pApplicationContextInstance == NULL )
  {
    m_pApplicationContextInstance = BeNew BeApplicationContext();
  }
  
  return m_pApplicationContextInstance;
}
/////////////////////////////////////////////////////////////////////////////
void BeApplicationContext::InitStaticResources(void)
{
    BeMaterialConstant::InitStaticResources ();
    BeMaterialVertexAttribute::InitStaticResources();
}
/////////////////////////////////////////////////////////////////////////////
void BeApplicationContext::DestroyStaticResources(void)
{
    BeMaterialConstant::DestroyStaticResources ();
    BeMaterialVertexAttribute::DestroyStaticResources();

    if ( m_pApplicationContextInstance != NULL )
    {
        BeDelete m_pApplicationContextInstance;
        m_pApplicationContextInstance = NULL;
    }
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeDevicesManager* BeApplicationContext::GetDevicesManager(void)
{
  return m_pDevicesManager;
}
/////////////////////////////////////////////////////////////////////////////
BeModulesManager* BeApplicationContext::GetModulesManager(void)
{
  return m_pModulesManager;
}
/////////////////////////////////////////////////////////////////////////////
BeRenderManager* BeApplicationContext::GetRenderManager(void)
{
  return m_pRenderManager;
}
/////////////////////////////////////////////////////////////////////////////
BeResourcesManager* BeApplicationContext::GetResourcesManager(void)
{
  return m_pResourcesManager;
}
/////////////////////////////////////////////////////////////////////////////
BeInputManager* BeApplicationContext::GetInputManager (void)
{
  return m_pInputManager;
}
/////////////////////////////////////////////////////////////////////////////
BeSceneManager* BeApplicationContext::GetSceneManager(void)
{
  return m_pSceneManager;
}
/////////////////////////////////////////////////////////////////////////////
BeAudioSystem* BeApplicationContext::GetAudioSystem (void)
{
  return m_pAudioSystem;
}
/////////////////////////////////////////////////////////////////////////////
BeWindowHandle BeApplicationContext::GetAppWindow(void)
{
  return NULL;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeApplicationContext::SetFrameRate(BeInt iFrameRate)
{
  m_iFrameRate = iFrameRate;
  m_iFrameIntervalMs = 1000 / m_iFrameRate;
  m_iFrameHalfIntervalMs = m_iFrameIntervalMs / 2;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeApplicationContext::CheckForCurrentDirectory(BeString16 strDirectoryName, BeUInt uRecursiveCount)
{  
#if TARGET_WINDOWS
  BeChar16 kCurrentOrigDir[MAX_PATH];
  GetCurrentDirectory(MAX_PATH, kCurrentOrigDir);

  while (uRecursiveCount >= 0)
  {
    if ( PathFileExists(strDirectoryName.ToRaw()) )
    {
      return;
    }
    if ( uRecursiveCount == 0 ) break;

    _chdir("..");
    uRecursiveCount--;
  }

  SetCurrentDirectory(kCurrentOrigDir);
#endif
}
/////////////////////////////////////////////////////////////////////////////
BeRenderDevice* BeApplicationContext::ConfigureRenderDevice(const BeRenderTypes::BeRenderDeviceSettings& kRenderDeviceSettings)
{
  BeRenderDevice* pRenderDevice = m_pDevicesManager->CreateRenderDevice(kRenderDeviceSettings);
  if (pRenderDevice != NULL)
  {
      m_pRenderManager->SetRenderDevice(pRenderDevice);
      m_pSceneManager->SetRenderDevice(pRenderDevice);
  }
  
  return pRenderDevice;
}
/////////////////////////////////////////////////////////////////////////////
BeRenderDevice* BeApplicationContext::ConfigureRenderDevice(const BeString16& strConfigFilePath)
{
  BE_UNUSED(strConfigFilePath);

  BeXMLReader kXMLParser(strConfigFilePath);
  BeRenderTypes::BeRenderDeviceSettings kRenderDeviceSettings;

  if (kXMLParser.SearchXMLNode("./Configuration/DeviceConfig/WindowCaption", "/"))
      kRenderDeviceSettings.m_strWindowName = ConvertString8To16(kXMLParser.ParseNodeValueString());
  if (kXMLParser.SearchXMLNode("./Configuration/DeviceConfig/IsWindowed", "/"))
      kRenderDeviceSettings.m_bWindowed = kXMLParser.ParseNodeValueBoolean();
  if (kXMLParser.SearchXMLNode("./Configuration/DeviceConfig/IsForcingVSync", "/"))
      kRenderDeviceSettings.m_bVerticalSync = kXMLParser.ParseNodeValueBoolean();
  if (kXMLParser.SearchXMLNode("./Configuration/DeviceConfig/BufferResolution", "/"))
  {
      BeVector2D m_vResolution(kXMLParser.ParseNodeValueVector2D());
      kRenderDeviceSettings.m_uBufferWidth = (BeInt)m_vResolution.m_fX;
      kRenderDeviceSettings.m_uBufferHeight = (BeInt)m_vResolution.m_fY;
  }
  if (kXMLParser.SearchXMLNode("./Configuration/DeviceConfig/RenderAPI", "/"))
  {
      BeInt iRenderAPI = kXMLParser.ParseNodeValueInt();

      switch (iRenderAPI)
      {
      case 0:
          kRenderDeviceSettings.m_eRenderDeviceAPI = BeRenderTypes::RENDER_DEVICE_API_DX11;
          break;
      case 1:
          kRenderDeviceSettings.m_eRenderDeviceAPI = BeRenderTypes::RENDER_DEVICE_API_GL;
          break;
      default:
          BE_APPLICATION_ERROR("Wrong Render API setting: %d", iRenderAPI);
          return NULL;
      }
  }

  BeRenderDevice* pRenderDevice = m_pDevicesManager->CreateRenderDevice(kRenderDeviceSettings);
  if (pRenderDevice != NULL)
  {
      m_pRenderManager->SetRenderDevice(pRenderDevice);
      m_pSceneManager->SetRenderDevice(pRenderDevice);

      BeViewport kViewport;
      kViewport.m_fWidth = (BeFloat) kRenderDeviceSettings.m_uBufferWidth;
      kViewport.m_fHeight = (BeFloat) kRenderDeviceSettings.m_uBufferHeight;

      pRenderDevice->SetViewport(kViewport);
  }

  return pRenderDevice;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeApplicationContext::Update(void)
{
  // We update the modules state machine
  m_pModulesManager->Update ();

  // We update the input manager
  m_pInputManager->Update ();

  // We update the sound system
  m_pAudioSystem->Update ();

  /*
  BeApplicationTypes::BeResourceOnTaskEvent kOnTaskResourceEvent;

  //BeInt fInitTime = GetCurrentTimeMS();
  
  BeInt fTimeToProcessResources = m_iLastUpdateTimeMS;
  if ( fTimeToProcessResources < m_iFrameHalfIntervalMs )
    fTimeToProcessResources = m_iFrameHalfIntervalMs;

  // The resource manager thread is executed for loading API device dependant data during the update
  BeInt iTimeToProcess = m_iFrameHalfIntervalMs;
  kOnTaskResourceEvent.m_eTaskType = BeApplicationTypes::BeResourceOnTaskEvent::TASK_ON_UPDATE_PROCESS;
  kOnTaskResourceEvent.m_iTimeToProcessTask = iTimeToProcess;
  m_pResourcesManager->Event(&kOnTaskResourceEvent);
  */

  m_iLastUpdateTimeMS = m_pModulesManager->PerformUpdateProcess();

  /*
  // We Sleep the main thread until next rendering frame
  BeInt fTimeToSleep = m_iFrameHalfIntervalMs - m_iLastUpdateTimeMS;
#if TARGET_WINDOWS
  Sleep(fTimeToSleep);
#else
  sleep(fTimeToSleep);
#endif

  fTimeToProcessResources = m_iLastRenderingTimeMS;
  if ( fTimeToProcessResources < m_iFrameHalfIntervalMs )
    fTimeToProcessResources = m_iFrameHalfIntervalMs;
  
  // The resource manager thread is executed for loading non API device dependant data during the rendering
  iTimeToProcess = m_iFrameHalfIntervalMs;
  kOnTaskResourceEvent.m_eTaskType = BeApplicationTypes::BeResourceOnTaskEvent::TASK_ON_RENDERING_PROCESS;
  kOnTaskResourceEvent.m_iTimeToProcessTask = iTimeToProcess;
  m_pResourcesManager->Event(&kOnTaskResourceEvent);
  */

  m_iLastRenderingTimeMS = m_pModulesManager->PerformRenderProcess(m_pRenderManager);

  /*
  // We Sleep the main thread until next frame
  fTimeToSleep = m_iFrameHalfIntervalMs - m_iLastRenderingTimeMS;
#if TARGET_WINDOWS
  Sleep(fTimeToSleep);
#else
  sleep(fTimeToSleep);
#endif
  */

  if ( m_pRenderManager != NULL )
  {
    m_pRenderManager->PresentFrame();
  }

  //BE_ENGINE_DEBUG("total time : %d", (GetCurrentTimeMS() - fInitTime) );
}
/////////////////////////////////////////////////////////////////////////////

