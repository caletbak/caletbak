/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeApplicationLibPredef.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_APPLICATIONLIBPREDEF_H
#define BE_APPLICATIONLIBPREDEF_H

// Global BE3 include
#include <BeCommon/BakEngine3Common.h>

#if TARGET_WINDOWS
#ifdef APPLICATION_IMPORTS
#  define APPLICATION_API __declspec(dllimport)
#  pragma message("automatic link to BakEngine3.lib")
#  pragma comment(lib, "BakEngine3.lib")
#else
#  define APPLICATION_API __declspec(dllexport)
#endif
#else
#  define APPLICATION_API
#endif

#ifdef _DEBUG
#  define BE_APPLICATION_DEBUG(x, ...) _OutputDebugString("[  APPLICATION DEBUG]: ", true, x, ##__VA_ARGS__)
#  define BE_APPLICATION_WARNING(x, ...) _OutputDebugString("[! APPLICATION WARNING]: ", true, x, ##__VA_ARGS__)
#  define BE_APPLICATION_ERROR(x, ...) _OutputDebugString("[* APPLICATION ERROR]: ", true, x, ##__VA_ARGS__)
#  define BE_APPLICATION_PRINT(x, ...) _OutputDebugString("", false, x, ##__VA_ARGS__)
#else
#  define BE_APPLICATION_DEBUG(x, ...) x
#  define BE_APPLICATION_WARNING(x, ...) x
#  define BE_APPLICATION_ERROR(x, ...) x
#  define BE_APPLICATION_PRINT(x, ...) x
#endif

// External includes
#include <BeCoreLib/Pch/BeCoreLib.h>
#include <BeMathLib/Pch/BeMathLib.h>
#include <BeRenderLib/Pch/BeRenderLib.h>
#include <BeSceneLib/Pch/BeSceneLib.h>
#include <BeInputLib/Pch/BeInputLib.h>
#include <BeSoundLib/Pch/BeSoundLib.h>
#include <BeUtilityLib/Pch/BeUtilityLib.h>

#endif // #ifndef BE_APPLICATIONLIBPREDEF_H

