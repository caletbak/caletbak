/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeAppStartUpNativeActivity.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_APPSTARTUPNATIVEACTIVITY_H
#define BE_APPSTARTUPNATIVEACTIVITY_H

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <BeApplicationLib/Pch/BeApplicationLibPredef.h>

#include <BeNativeActivity.h>

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class APPLICATION_API BeAppStartUpNativeActivity
{
public:

    static void onCreate (struct android_app *androidApp);

    static void onStart (struct android_app *androidApp);

    static void onPause (struct android_app *androidApp);

    static void onStop (struct android_app *androidApp);

    static void onInitView (struct android_app *androidApp);

    static void onReleaseView (struct android_app *androidApp);

    static void onGainFocus (struct android_app *androidApp);

    static void onLoseFocus (struct android_app *androidApp);

    static void onDestroy (struct android_app *androidApp);

    static void onConfigChanged (struct android_app *androidApp);

    static void onLowMemoryWarning (struct android_app *androidApp);

    static void onUpdate (struct android_app *androidApp);

    static int onInputEvent(struct android_app *androidApp, BeVoidP inputEvent);
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BeAppStartUpNativeActivity_dummy();

extern "C" JNIEXPORT void Java_org_bakengine_BeAppStartUpNativeActivity_onNativeCreate (JNIEnv *env, jobject obj, jclass thisClass, jobject savedInstanceState);

extern "C" JNIEXPORT void JNICALL Java_org_bakengine_BeAppStartUpNativeActivity_nativeWindowFocusGot(JNIEnv* env, jobject obj);

extern "C" JNIEXPORT void JNICALL Java_org_bakengine_BeAppStartUpNativeActivity_nativeWindowFocusLost(JNIEnv* env, jobject obj);

extern "C" JNIEXPORT void JNICALL Java_org_bakengine_BeAppStartUpNativeActivity_nativeHandlePinchGesture(JNIEnv* env, jobject thiz, jfloat factor, jfloat velocity);

extern "C" JNIEXPORT void JNICALL Java_org_bakenginesoft_BeAppStartUpNativeActivity_nativeHandleRotationGesture(JNIEnv* env, jobject thiz, jfloat factor, jfloat velocity);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#endif // BE_APPSTARTUPNATIVEACTIVITY_H
