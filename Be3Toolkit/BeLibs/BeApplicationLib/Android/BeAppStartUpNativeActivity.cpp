/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeAppStartUpNativeActivity.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <BeAppStartUpNativeActivity.h>
#include <BeAppStartUpRuntime.h>
#include <BeApplicationConfig.h>

#include <BeEGLView.h>
#include <BeGraphics.h>

#include <BeJNI.h>
#include <Threading/BeThreadMutex.h>
#include <Common/BeDeviceUtils.h>

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define DEBUG_ACTIVITY_STATES (!RELEASE && 1)

#define DEBUG_INPUT_OUTPUT (!RELEASE && 0)

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

extern JavaVM* g_javaVM;

extern bool g_isWindowDestroyed;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct NativeTouchPosition
{
    float x, y;
};

static NativeTouchPosition g_prevTouchPositions [10];

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BeAppStartUpNativeActivity_dummy()
{
    // Stub function to prevent code-stripping.

    BeNativeActivity_dummy();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BeAppStartUpNativeActivity::onCreate (struct android_app *androidApp)
{
    BE_APPLICATION_DEBUG("lifecycle BeAppStartUpNativeActivity::onCreate");

    BeAppStartUpRuntime::onCreate(NULL);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BeAppStartUpNativeActivity::onStart (struct android_app *androidApp)
{
    BE_APPLICATION_DEBUG("lifecycle BeAppStartUpNativeActivity::onStart");

    BeAppStartUpRuntime::onStart ();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BeAppStartUpNativeActivity::onPause (struct android_app *androidApp)
{
    BE_APPLICATION_DEBUG("lifecycle BeAppStartUpNativeActivity::onPause");

    BeAppStartUpRuntime::onPause ();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BeAppStartUpNativeActivity::onStop (struct android_app *androidApp)
{
    BE_APPLICATION_DEBUG("lifecycle BeAppStartUpNativeActivity::onStop");

    BeAppStartUpRuntime::onStop ();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BeAppStartUpNativeActivity::onInitView (struct android_app *androidApp)
{
#if TARGET_ANDROID
    BE_APPLICATION_DEBUG("lifecycle BeAppStartUpNativeActivity::onInitView : %d", gettid());
#endif

    BE_APPLICATION_DEBUG("lifecycle BeAppStartUpNativeActivity::onInitView");

    // 
    // Grab the critical section to ensure any EGL modifications are atomic.
    // 

    /* this is OK
    if (EGLView::s_EGLDisplay == EGL_NO_DISPLAY)
    {
        BE_APPLICATION_DEBUG("lifecycle onInitView EGL_NO_DISPLAY");

        EGLView::s_EGLDisplay = EGLView::GetNativeDisplay(EGL_DEFAULT_DISPLAY);

        eglInitialize(EGLView::s_EGLDisplay, 0, 0);

        EGLView::AssertNoEGLErrors();
    }

    if (EGLView::s_EGLDisplay != EGL_NO_DISPLAY)
    {
        //if (EGLView::s_EGLConfig == NULL)
        {
            BE_APPLICATION_DEBUG("lifecycle onInitView s_EGLConfig");

            const BeAppConfig &appConfig = BeAppStartUpRuntime::GetConfig();

            EGLView::s_EGLConfig = EGLView::SelectBestFitConfig(EGLView::s_EGLDisplay, appConfig.m_graphicsConfig, androidApp);

            EGLView::AssertNoEGLErrors();

            ASSERT_PTR(EGLView::s_EGLConfig);
        }

        //	if (EGLView::s_EGLConfig)
        {
            if (EGLView::s_EGLContext == EGL_NO_CONTEXT)
            {
                BE_APPLICATION_DEBUG("lifecycle onInitView EGL_NO_CONTEXT");

                EGLView::s_EGLContext = EGLView::CreateContext(EGLView::s_EGLDisplay, EGLView::s_EGLConfig, 2);

                EGLView::AssertNoEGLErrors();

                ASSERT(EGLView::s_EGLContext != EGL_NO_CONTEXT);
            }

            if (EGLView::s_EGLContext != EGL_NO_CONTEXT)
            {
                if (EGLView::s_EGLWindow == EGL_NO_SURFACE)
                {
                    BE_APPLICATION_DEBUG("lifecycle onInitView s_EGLWindow");

                    EGLView::s_EGLWindow = EGLView::CreateNativeWindowSurface(EGLView::s_EGLDisplay, EGLView::s_EGLConfig, androidApp->window);

                    //EGLUtils::PrintStatistics(EGLView::s_EGLDisplay, EGLView::s_EGLConfig, EGLView::s_EGLWindow);

                    EGLView::AssertNoEGLErrors();

                    ASSERT(EGLView::s_EGLWindow != EGL_NO_SURFACE);

                    // 
                    // Evaluate new native surface's dimensions.
                    // 

                    EGLint backbufferWidth, backbufferHeight;

                    eglQuerySurface(EGLView::s_EGLDisplay, EGLView::s_EGLWindow, EGL_WIDTH, &backbufferWidth);

                    eglQuerySurface(EGLView::s_EGLDisplay, EGLView::s_EGLWindow, EGL_HEIGHT, &backbufferHeight);

                    BeAppConfig viewChangedConfig = BeAppStartUpRuntime::GetConfig();

                    viewChangedConfig.m_graphicsConfig.m_screenWidth = (unsigned int)backbufferWidth;

                    viewChangedConfig.m_graphicsConfig.m_screenHeight = (unsigned int)backbufferHeight;

                    BeAppStartUpRuntime::SetConfig(viewChangedConfig);
                }

                if (EGLView::s_EGLWindow != EGL_NO_SURFACE)
                {
                    BE_APPLICATION_DEBUG("lifecycle EGLView::MakeCurrent");

                    // 
                    // Bind the new surface (ensures any old surfaces are destroyed), and release if it's the first launch.
                    // - BeGraphics::Initialise expects an invalid EGL context so the release satisfies this constraint.
                    // 

                    EGLView::MakeCurrent(EGLView::s_EGLDisplay, EGLView::s_EGLWindow, EGLView::s_EGLWindow, EGLView::s_EGLContext);
                }
            }
        }
    }
    */

    BeAppStartUpRuntime::onSurfaceCreated();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BeAppStartUpNativeActivity::onReleaseView (struct android_app *androidApp)
{
    BE_APPLICATION_DEBUG("lifecycle BeAppStartUpNativeActivity::onReleaseView");

    // 
    // Bind and maintain the graphics critical section until the view has been recreated.
    // 

    BeAppStartUpRuntime::onSurfaceLost ();

    /* this is OK
    EGLView::MakeCurrent(EGLView::s_EGLDisplay, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);

    EGLView::DestroySurface(EGLView::s_EGLDisplay, EGLView::s_EGLWindow);

    EGLView::s_EGLWindow = EGL_NO_SURFACE;
    */

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BeAppStartUpNativeActivity::onGainFocus (struct android_app *androidApp)
{
    BeAppStartUpRuntime::onVisibilityChanged (true);

    // 
    // Evaluate new native surface's dimensions.
    // 

    /* this is OK
    EGLint backbufferWidth, backbufferHeight;

    eglQuerySurface(EGLView::s_EGLDisplay, EGLView::s_EGLWindow, EGL_WIDTH, &backbufferWidth);

    eglQuerySurface(EGLView::s_EGLDisplay, EGLView::s_EGLWindow, EGL_HEIGHT, &backbufferHeight);

    BeApplicationConfig viewChangedConfig = BeAppStartUpRuntime::GetConfig();
    if (backbufferWidth > backbufferHeight)
    {
        viewChangedConfig.m_graphicsConfig.m_screenWidth = (unsigned int)backbufferWidth;
        viewChangedConfig.m_graphicsConfig.m_screenHeight = (unsigned int)backbufferHeight;
        BE_APPLICATION_DEBUG("Device Resolution onGainFocus: %d, %d", backbufferWidth, backbufferHeight);
    }
    else
    {
        viewChangedConfig.m_graphicsConfig.m_screenWidth = (unsigned int)backbufferHeight;
        viewChangedConfig.m_graphicsConfig.m_screenHeight = (unsigned int)backbufferWidth;
        BE_APPLICATION_DEBUG("Device swapppppppppppppppppp Resolution onGainFocus: %d, %d", backbufferHeight, backbufferWidth);
    }

    BeAppStartUpRuntime::SetConfig(viewChangedConfig);
    */

    BeAppStartUpRuntime::onSurfaceChanged();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BeAppStartUpNativeActivity::onLoseFocus (struct android_app *androidApp)
{
	BeAppStartUpRuntime::onVisibilityChanged (false);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BeAppStartUpNativeActivity::onDestroy (struct android_app *androidApp)
{
#if TARGET_ANDROID
    BE_APPLICATION_DEBUG("lifecycle BeAppStartUpNativeActivity::onDestroy : %d", gettid());
#endif

    BE_APPLICATION_DEBUG("lifecycle BeNativeActivity::s_GLContextWasDestroyed BeAppStartUpNativeActivity::onDestroy");

    /* this is OK
    EGLView::MakeCurrent(EGLView::s_EGLDisplay, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);

    BeNativeActivity::s_GLContextWasDestroyed = true;

    BeAppStartUpRuntime::onDestroy();
    */
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BeAppStartUpNativeActivity::onConfigChanged(struct android_app *androidApp)
{
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BeAppStartUpNativeActivity::onLowMemoryWarning(struct android_app *androidApp)
{
    BeAppStartUpRuntime::onLowMemory();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BeAppStartUpNativeActivity::onUpdate(struct android_app *androidApp)
{
    //BE_APPLICATION_DEBUG("BeAppStartUpNativeActivity::onUpdate");

    BeAppStartUpRuntime::onDrawFrame();

    bool needToTakeCriticalSection = (BeGraphics::s_pCriticalSection != NULL);

    if (needToTakeCriticalSection)
    {
        if (BeGraphics::EnterCriticalSection("BeAppStartUpNativeActivity::onUpdate"))
        {
            if (!g_isWindowDestroyed)
            {
                BeEGLView::SwapBuffers();
            }

            BeGraphics::ExitCriticalSection("BeAppStartUpNativeActivity::onUpdate");
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int BeAppStartUpNativeActivity::onInputEvent(struct android_app *androidApp, BeVoidP inputEvent)
{
    //BE_APPLICATION_DEBUG("BeAppStartUpNativeActivity::onInputEvent");

    return BeAppStartUpRuntime::onInputEvent(inputEvent);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

JNIEXPORT void Java_org_bakengine_BeAppStartUpNativeActivity_onNativeCreate (JNIEnv *env, jobject obj, jclass thisClass, jobject savedInstanceState)
{
    BE_APPLICATION_DEBUG("BeAppStartUpNativeActivity onNativeCreate");

    BE_ASSERT_PTR(BeJNI::GetVM());

    BeAppStartUpNativeActivity_dummy();

    BeJNIThreadEnv jniThread;

    Java_org_bakengine_BeNativeActivity_onNativeCreate(env, obj, thisClass, savedInstanceState);

    BeJNI::CheckExceptions(jniThread);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void android_main (struct android_app *androidApp)
{
    BeThreadMutex::s_uInitialNativeThread = BeThreadMutex::GetCurrentThreadIdentifier();

    BE_APPLICATION_DEBUG("lifecycle android_main: %d", BeThreadMutex::s_uInitialNativeThread);

    app_dummy();

    BE_ASSERT_PTR(androidApp);

    g_app = androidApp;

    BE_ASSERT_PTR(androidApp->activity->vm);

    g_javaVM = androidApp->activity->vm;

    BeJNI::SetVM(g_javaVM);

    // 
    // Setup the initial device state (before the config is created)
    // 

    BeNativeActivity::RegisteredCallbacks nativeActivityCallbacks;

    memset(&nativeActivityCallbacks, 0, sizeof (BeNativeActivity::RegisteredCallbacks));

    nativeActivityCallbacks.activityOnStart = BeAppStartUpNativeActivity::onStart;

    nativeActivityCallbacks.activityOnStop = BeAppStartUpNativeActivity::onStop;

    nativeActivityCallbacks.activityOnPause = BeAppStartUpNativeActivity::onPause;

    nativeActivityCallbacks.activityOnUpdate = BeAppStartUpNativeActivity::onUpdate;

    nativeActivityCallbacks.activityOnDestroy = BeAppStartUpNativeActivity::onDestroy;

    nativeActivityCallbacks.activityOnInitView = BeAppStartUpNativeActivity::onInitView;

    nativeActivityCallbacks.activityOnReleaseView = BeAppStartUpNativeActivity::onReleaseView;

    nativeActivityCallbacks.activityOnGainFocus = BeAppStartUpNativeActivity::onGainFocus;

    nativeActivityCallbacks.activityOnLoseFocus = BeAppStartUpNativeActivity::onLoseFocus;

    nativeActivityCallbacks.activityOnConfigChanged = BeAppStartUpNativeActivity::onConfigChanged;

    nativeActivityCallbacks.activityOnLowMemoryWarning = BeAppStartUpNativeActivity::onLowMemoryWarning;

    nativeActivityCallbacks.activityOnInputEvent = BeAppStartUpNativeActivity::onInputEvent;

    BeNativeActivity::Initialise(androidApp, nativeActivityCallbacks);

    BeAppStartUpNativeActivity::onCreate(androidApp);

    while (true)
    {
        const BeUInt uRequestedEventFlags = BeNativeActivity::Update();

        if ((BeDeviceUtils::s_iGameFinishRequested & BeDeviceUtils::GAME_REQUEST_FINISH_FLAG) == 0)
        {
            const BeUInt uRequestedEventFlags = BeNativeActivity::Update();

            if (BeNativeActivity::s_destroyPreviousThread == gettid())
            {
                //BE_APPLICATION_DEBUG ("onDestroy Calling ANativeActivity_finish ...");

                //ANativeActivity_finish(androidApp->activity);

                break;
            }
        }
        else
        {
            //BeNativeActivity::PollOSLooper(10);
        }
      
        if (BeDeviceUtils::s_iGameFinishRequested == BeDeviceUtils::GAME_REQUEST_FINISH_FLAG)
        {
            break;
        }
    }

    BeNativeActivity::s_destroyPreviousThread = 0;

    BE_APPLICATION_DEBUG("lifecycle onDestroy main thread: %d", gettid());

    //BeNativeActivity::Deinitialise ();

    if (BeDeviceUtils::s_iGameFinishRequested == BeDeviceUtils::GAME_REQUEST_FINISH_FLAG)
    {
        //BeNativeActivity::PollOSLooper (-1);

        BE_APPLICATION_DEBUG("BeDeviceUtils::s_gameFinishRequested: EXIT 0 !!");

        BeDeviceUtils::SendFinishSignal();

        //exit(0);
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

JNIEXPORT void Java_org_bakengine_BeAppStartUpNativeActivity_nativeWindowFocusGot(JNIEnv* env, jobject thiz)
{
    BE_APPLICATION_DEBUG("BeAppStartUpRuntime_nativeWindowFocusGot");
}

JNIEXPORT void Java_org_bakengine_BeAppStartUpNativeActivity_nativeWindowFocusLost(JNIEnv* env, jobject thiz)
{
    BE_APPLICATION_DEBUG("BeAppStartUpRuntime_nativeWindowFocusLost");
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

JNIEXPORT void Java_org_bakengine_BeAppStartUpNativeActivity_nativeHandlePinchGesture(JNIEnv* env, jobject thiz, jfloat factor, jfloat velocity)
{
    BeApplicationConfig& appConfig = BeAppStartUpRuntime::GetConfig();

    if (appConfig.m_onHandlePinchGesture)
    {
        appConfig.m_onHandlePinchGesture(factor, velocity);
    }
}

JNIEXPORT void Java_org_bakengine_BeAppStartUpNativeActivity_nativeHandleRotationGesture(JNIEnv* env, jobject thiz, jfloat deltaDegrees, jboolean isEnded)
{
    BeApplicationConfig& appConfig = BeAppStartUpRuntime::GetConfig();

    if (appConfig.m_onHandleRotationGesture)
    {
        appConfig.m_onHandleRotationGesture(deltaDegrees, isEnded);
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
