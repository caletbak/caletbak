
package org.bakengine;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

import android.os.Bundle;
import android.view.ScaleGestureDetector;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

public class BeAppStartUpNativeActivity extends BeNativeActivity
{
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	private static final String TAG = "BeAppStartUpNativeActivity";

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	private static native void onNativeCreate (Class<?> thisClass, Bundle savedInstanceState);

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public static native void nativeWindowFocusGot();

	public static native void nativeWindowFocusLost();

	public static native void nativeHandlePinchGesture(float factor, float velocity);

	public static native void nativeHandleRotationGesture(float deltaDegrees, boolean isEnded);

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	private float m_deltaRotationDegrees;
	private BaseGestureDetector.GestureProgress m_rotateGestureProgress;
	private ScaleGestureDetector m_scaleDetector;
	private RotateGestureDetector m_rotateDetector;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener
	{
		@Override
		public boolean onScale(ScaleGestureDetector detector) 
		{
			float scaleVelocity = detector.getCurrentSpan() - detector.getPreviousSpan();
			scaleVelocity *= 0.2f;
			
			nativeHandlePinchGesture(detector.getScaleFactor(),scaleVelocity);
			return true;
		}
	}
	
	private class RotateListener extends RotateGestureDetector.SimpleOnRotateGestureListener 
	{
		@Override
		public boolean onRotate(RotateGestureDetector detector) 
		{
			m_rotateGestureProgress = BaseGestureDetector.GestureProgress.GESTURE_INPROGRESS;
			m_deltaRotationDegrees = detector.getRotationDegreesDelta();
			return true;
		}
		
		@Override
		public boolean onRotateBegin(RotateGestureDetector detector)
		{
			m_rotateGestureProgress = BaseGestureDetector.GestureProgress.GESTURE_STARTED;
			return true;
		}
		
		@Override
		public void onRotateEnd(RotateGestureDetector detector)
		{
			m_rotateGestureProgress = BaseGestureDetector.GestureProgress.GESTURE_ENDED;
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	@Override
	protected void onCreate (Bundle savedInstanceState)
	{
		// 
		// Called when the activity is starting.
		// 

		BeDebug.v (TAG, "onCreate");

		if (!BeNativeActivity.s_gameAlreadyStarted)
		{
			BeDebug.v (TAG, "lifecycle Dynamically link required native libraries before continuing with startup");

			// 
			// Dynamically link required native libraries before continuing with startup.
			// 
    
			try
			{
				BeAppStartUpRuntime.onCreate (this, savedInstanceState);
      
				onNativeCreate (BeAppStartUpNativeActivity.class, savedInstanceState);
			}
			catch (UnsatisfiedLinkError e)
			{
				BeDebug.e (TAG, "Native implementation of BeAppStartUpNativeActivity.onNativeCreate not found.", e);
			}
		
			m_deltaRotationDegrees = 0.0f;
			m_scaleDetector = new ScaleGestureDetector(this, new ScaleListener());
			m_rotateDetector = new RotateGestureDetector(this, new RotateListener());
			m_rotateGestureProgress = BaseGestureDetector.GestureProgress.GESTURE_NOTSTARTED;

			BeNativeActivity.s_gameAlreadyStarted = true;
		}
    
		super.onCreate (savedInstanceState);
	}
  
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	@Override
	protected void onDestroy ()
	{
		super.onDestroy ();
    
		BeDebug.v (TAG, "onDestroy");
    
		BeAppStartUpRuntime.onDestroy (this);
	}
  
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
	@Override
	protected void onStart ()
	{
		super.onStart ();
    
		BeDebug.v (TAG, "onStart");
    
		BeAppStartUpRuntime.onStart (this);
		
		//BeNotification.PreInitialise ();
	}
  
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
	@Override
	protected void onStop ()
	{
		super.onStop ();
    
		BeDebug.v (TAG, "onStop");
    
		BeAppStartUpRuntime.onStop (this);
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	@Override
	public void onWindowFocusChanged (boolean focus)
	{
		super.onWindowFocusChanged (focus);
    
		BeDebug.v (TAG, "onWindowFocusChanged");

		if (focus)
		{
			nativeWindowFocusGot();
		}
		else
		{
			nativeWindowFocusLost();
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
