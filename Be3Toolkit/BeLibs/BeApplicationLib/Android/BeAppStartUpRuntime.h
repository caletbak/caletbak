/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeAppStartUpRuntime.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_APPSTARTUPRUNTIME_H
#define BE_APPSTARTUPRUNTIME_H

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <BeApplicationLib/Pch/BeApplicationLibPredef.h>

#include <BeApplicationConfig.h>

#include <android/configuration.h>

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class APPLICATION_API BeAppStartUpRuntime
{
public:

    static void onCreate (jobject savedInstanceState);

    static void onStart ();

    static void onNewIntent (jobject intent);

    static void onActivityResult (int requestCode, int resultCode, jobject data);

    static void onResume ();

    static void onPause ();

    static void onStop ();

    static void onDestroy ();

    static void onVisibilityChanged (bool visible);

    static void onConfigurationChanged (AConfiguration *configuration);

    static void onLowMemory ();

    static void onSurfaceCreated ();

    static void onSurfaceChanged ();

    static void onSurfaceLost ();

    static void onDrawFrame ();

    static bool onInputEvent(BeVoidP inputEvent);

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    static BeApplicationConfig& GetConfig();

    static void SetConfig(BeApplicationConfig& config);

private:

    static BeApplicationConfig s_appConfig;

};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#endif // BE_APPSTARTUPRUNTIME_H

