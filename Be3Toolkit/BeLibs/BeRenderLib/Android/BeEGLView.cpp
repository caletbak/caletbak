/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeEGLView.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <BeEGLView.h>

#include <EGL/egl.h>

/*
#include <GLES/gl.h>
#include <GLES2/gl2.h>
*/

#define LOG_TAG "BeEGLView"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

static EGLDisplay  s_pEGLDisplay = EGL_NO_DISPLAY;
static EGLContext  s_pEGLContext = EGL_NO_CONTEXT;
static EGLSurface  s_pEGLWindow = EGL_NO_SURFACE;
static EGLConfig   s_pEGLConfig = NULL;
static EGLint      m_iFormat = -1;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

BeVoidP BeEGLView::GetNativeDisplay(BeVoidP pDisplayType)
{
    EGLDisplay display = EGL_NO_DISPLAY;

    if ((display = eglGetDisplay(pDisplayType)) == EGL_NO_DISPLAY)
    {
        BE_RENDER_ERROR("eglGetDisplay() returned error %d", eglGetError());
    }

    return display;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

BeVoidP BeEGLView::SelectBestFitConfig(BeVoidP pDisplay, const BeGraphics::BeEGLGraphicsConfig& graphicsConfig, BeVoidP pAndroidApp)
{
    EGLDisplay display = reinterpret_cast<EGLDisplay>(pDisplay);
    struct android_app* androidApp = reinterpret_cast<struct android_app*>(pAndroidApp);

    EGLConfig eglConfig;
    EGLint numConfigs;

    if (s_pEGLContext == EGL_NO_CONTEXT)
    {
        m_iFormat = ANativeWindow_getFormat(androidApp->window);
    }
    else
    {
        ANativeWindow_setBuffersGeometry(androidApp->window, 0, 0, m_iFormat);

        return s_pEGLConfig;
    }

    const EGLint attribs565[] =
    {
        EGL_SURFACE_TYPE, (EGLint)graphicsConfig.m_uEGLSurfaceType,
        EGL_BLUE_SIZE, 5,
        EGL_GREEN_SIZE, 6,
        EGL_RED_SIZE, 5,
        EGL_RENDERABLE_TYPE, (EGLint)graphicsConfig.m_uEGLRenderableType,
        EGL_NONE
    };
    const EGLint attribs888[] =
    {
        EGL_SURFACE_TYPE, (EGLint)graphicsConfig.m_uEGLSurfaceType,
        EGL_BLUE_SIZE, 8,
        EGL_GREEN_SIZE, 8,
        EGL_RED_SIZE, 8,
        EGL_RENDERABLE_TYPE, (EGLint)graphicsConfig.m_uEGLRenderableType,
        EGL_NONE
    };
    const EGLint attribs8888[] =
    {
        EGL_SURFACE_TYPE, (EGLint)graphicsConfig.m_uEGLSurfaceType,
        EGL_BLUE_SIZE, 8,
        EGL_GREEN_SIZE, 8,
        EGL_RED_SIZE, 8,
        EGL_ALPHA_SIZE, 8,
        EGL_RENDERABLE_TYPE, (EGLint)graphicsConfig.m_uEGLRenderableType,
        EGL_NONE
    };

    if (m_iFormat == WINDOW_FORMAT_RGB_565)
    {
        BE_RENDER_DEBUG("EGL WINDOW_FORMAT_RGB_565");
        if (!eglChooseConfig(display, attribs565, &eglConfig, 1, &numConfigs))
        {
            BE_RENDER_ERROR("eglChooseConfig() returned error %d", eglGetError());

            return NULL;
        }
    }
    else if (m_iFormat == WINDOW_FORMAT_RGBX_8888)
    {
        BE_RENDER_DEBUG("EGL WINDOW_FORMAT_RGBX_8888");

        if (!eglChooseConfig(display, attribs888, &eglConfig, 1, &numConfigs))
        {
            BE_RENDER_ERROR("eglChooseConfig() returned error %d", eglGetError());

            return NULL;
        }
    }
    else if (m_iFormat == WINDOW_FORMAT_RGBA_8888)
    {
        BE_RENDER_DEBUG("EGL WINDOW_FORMAT_RGBA_8888");

        if (!eglChooseConfig(display, attribs8888, &eglConfig, 1, &numConfigs))
        {
            BE_RENDER_ERROR("eglChooseConfig() returned error %d", eglGetError());

            return NULL;
        }
    }

    return eglConfig;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

BeVoidP BeEGLView::CreateContext(BeVoidP pDisplay, BeVoidP pConfig, int contextClientVersion)
{
    EGLContext context = EGL_NO_CONTEXT;

    EGLDisplay display = reinterpret_cast<EGLDisplay>(pDisplay);
    EGLConfig config = reinterpret_cast<EGLConfig>(pConfig);

    static const EGLint ctx_attribs[] =
    {
        EGL_CONTEXT_CLIENT_VERSION, contextClientVersion,
        EGL_NONE
    };

    if (!(context = eglCreateContext(display, config, EGL_NO_CONTEXT, ctx_attribs)))
    {
        BE_RENDER_ERROR("eglCreateContext() returned error %d", eglGetError());

        return EGL_NO_CONTEXT;
    }

    return context;
}

void BeEGLView::DestroyContext(BeVoidP pdisplay, BeVoidP pContext)
{
    EGLDisplay display = reinterpret_cast<EGLDisplay>(pdisplay);
    EGLContext context = reinterpret_cast<EGLContext>(pContext);

    if (!eglDestroyContext(display, context))
    {
        BE_RENDER_ERROR("eglDestroyContext() returned error %d", eglGetError());
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

BeVoidP BeEGLView::CreateNativeWindowSurface(BeVoidP pDisplay, BeVoidP pConfig, BeVoidP pWindow)
{ 
    EGLSurface surface = EGL_NO_SURFACE;

    EGLDisplay display = reinterpret_cast<EGLDisplay>(pDisplay);
    EGLConfig config = reinterpret_cast<EGLConfig>(pConfig);
    ANativeWindow* window = reinterpret_cast<ANativeWindow*>(pWindow);

    if (!(surface = eglCreateWindowSurface(display, config, window, 0)))
    {
        BE_RENDER_ERROR("eglCreateWindowSurface() returned error %d", eglGetError());

        return EGL_NO_SURFACE;
    }

    return surface;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

BeBoolean BeEGLView::MakeCurrent(BeVoidP pDisplay, BeVoidP pDrawSurface, BeVoidP pReadSurface, BeVoidP pContext)
{
    EGLDisplay display = reinterpret_cast<EGLDisplay>(pDisplay);
    EGLSurface drawSurface = reinterpret_cast<EGLSurface>(pDrawSurface);
    EGLSurface readSurface = reinterpret_cast<EGLSurface>(pReadSurface);
    EGLContext context = reinterpret_cast<EGLContext>(pContext);

    if (!eglMakeCurrent(display, drawSurface, readSurface, context))
    {
        BE_RENDER_ERROR("eglMakeCurrent() returned error %d", eglGetError());

        return BE_FALSE;
    }

    return BE_TRUE;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

BeBoolean BeEGLView::DestroySurface(BeVoidP pDisplay, BeVoidP pWindow)
{
    EGLDisplay display = reinterpret_cast<EGLDisplay>(pDisplay);
    ANativeWindow* window = reinterpret_cast<ANativeWindow*>(pWindow);

    if (!eglDestroySurface(display, window))
    {
        BE_RENDER_ERROR("eglDestroySurface() returned error %d", eglGetError());

        return BE_FALSE;
    }

    return BE_TRUE;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

bool BeEGLView::SwapBuffers()
{
    if (!eglSwapBuffers(s_pEGLDisplay, s_pEGLWindow))
    {
        BE_RENDER_ERROR("eglSwapBuffers() returned error %d", eglGetError());

        return BE_FALSE;
    }

    return BE_TRUE;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

bool BeEGLView::AssertNoEGLErrors()
{
    EGLint eglError = eglGetError();
    if (eglError != EGL_SUCCESS)
    {
        BE_RENDER_ERROR("EGLView::AssertNoEGLErrors: %d", eglError);

        return BE_FALSE;
    }

    return BE_TRUE;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
