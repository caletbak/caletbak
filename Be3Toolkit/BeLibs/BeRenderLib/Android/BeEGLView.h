/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeEGLView.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_EGLVIEW_H
#define BE_EGLVIEW_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>

#include <BeGraphics.h>

#include <EGL/egl.h>

/*
#include <GLES/gl.h>
#include <GLES2/gl2.h>

#include <android/input.h>
*/

/////////////////////////////////////////////////
/// \class BeEGLView
/// \brief EGL View for creating GL contexts from it
/////////////////////////////////////////////////
class RENDER_API BeEGLView
{

public:

    static BeVoidP GetNativeDisplay(BeVoidP pDisplayType);

    static BeVoidP SelectBestFitConfig(BeVoidP pDisplay, const BeGraphics::BeEGLGraphicsConfig& graphicsConfig, BeVoidP pAndroidApp);

    static BeVoidP CreateContext(BeVoidP pDisplay, BeVoidP pConfig, BeInt iContextClientVersion);

    static void DestroyContext(BeVoidP display, BeVoidP context);

    static BeVoidP CreateNativeWindowSurface(BeVoidP display, BeVoidP config, BeVoidP pWindow);

    static BeBoolean MakeCurrent(BeVoidP display, BeVoidP drawSurface, BeVoidP readSurface, BeVoidP context);

    static BeBoolean DestroySurface(BeVoidP display, BeVoidP window);

    static BeBoolean SwapBuffers();

    static BeBoolean AssertNoEGLErrors();

};

#endif // BE_EGLVIEW_H
