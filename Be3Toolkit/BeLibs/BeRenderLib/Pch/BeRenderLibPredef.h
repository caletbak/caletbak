/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeRenderLibPredef.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_RENDERLIBPREDEF_H
#define BE_RENDERLIBPREDEF_H

// Global BE3 include
#include <BeCommon/BakEngine3Common.h>

#if TARGET_WINDOWS
#ifdef RENDER_IMPORTS
#  define RENDER_API __declspec(dllimport)
#  pragma message("automatic link to BeRenderLib.lib")
#  pragma comment(lib, "BeRenderLib.lib")
#else
#  define RENDER_API __declspec(dllexport)
#endif
#else
#  define RENDER_API
#endif

#ifdef _DEBUG
#  define BE_RENDER_DEBUG(x, ...) _OutputDebugString("[  RENDER DEBUG]: ", true, x, ##__VA_ARGS__)
#  define BE_RENDER_WARNING(x, ...) _OutputDebugString("[! RENDER WARNING]: ", true, x, ##__VA_ARGS__)
#  define BE_RENDER_ERROR(x, ...) _OutputDebugString("[* RENDER ERROR]: ", true, x, ##__VA_ARGS__)
#  define BE_RENDER_PRINT(x, ...) _OutputDebugString("", false, x, ##__VA_ARGS__)
#else
#  define BE_RENDER_DEBUG(x, ...) x
#  define BE_RENDER_WARNING(x, ...) x
#  define BE_RENDER_ERROR(x, ...) x
#  define BE_RENDER_PRINT(x, ...) x
#endif

#if defined(AS400) || defined(OS400)
  typedef pthread_id_np_t ThreadID;
#elif defined(VMS) 
  typedef pthread_t ThreadID;
#else
#  ifdef USE_BEGIN_THREAD
  typedef BeUInt ThreadID;
#  else
  typedef BeULong ThreadID;
#  endif
#endif

// External includes
#include <BeCoreLib/Pch/BeCoreLib.h>
#include <BeMathLib/Pch/BeMathLib.h>
#include <BeInputLib/Pch/BeInputLib.h>
#include <BeUtilityLib/Pch/BeUtilityLib.h>

#endif // #ifndef BE_RENDERLIBPREDEF_H

