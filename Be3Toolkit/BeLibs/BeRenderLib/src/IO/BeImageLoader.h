/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeImageLoader.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEIMAGELOADER_H
#define BE_BEIMAGELOADER_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>

/////////////////////////////////////////////////
/// \class  BeImageLoader
/// \brief  Image loader helper
/////////////////////////////////////////////////
class RENDER_API BeImageLoader : public BeMemoryObject
{
    public:

        /// \brief  Constructor
        BeImageLoader();
        /// \brief  Destructor
        virtual ~BeImageLoader();



        // \brief  Methods
        // {

            /// \brief  Loads an image if possible from a file
            virtual BeVoidP LoadFileImage(const BeString16& strFilename);

            /// \brief  Loads an image if possible from a buffer
            virtual BeVoidP LoadFileImage(BeVoidP pData, BeInt iDataSize) = 0;

            /// \brief  Generates the pixel array from a previous loaded image
            virtual BeVector4D* GeneratePixelArray(void) = 0;

            /// \brief  Releases the current loaded API image
            virtual void Release(void) = 0;

        // }



        // \brief  Getters
        // {

            /// \brief  Returns the image width after being loaded, -1 otherwise
            virtual BeInt GetImageWidth(void) = 0;

            /// \brief  Returns the image height after being loaded, -1 otherwise
            virtual BeInt GetImageHeight(void) = 0;

            /// \brief  Returns the internal graphics API image object
            BeVoidP GetImageObjectAPI(void);

            /// \brief  Returns the image pixels array after calling GeneratePixelArray
            BeVector4D* GetImagePixels(void);

        // }

    protected:

        /// \brief  Graphics API Image object
        BeVoidP     m_pImageObjectAPI;

        /// \brief  Image pixels array
        BeVector4D* m_pImagePixels;
};

#endif // #ifndef BE_BEIMAGELOADER_H
