/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeGraphics.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_GRAPHICS_H
#define BE_GRAPHICS_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>
#include <Threading/BeThreadMutex.h>

class RENDER_API BeGraphics
{
    public:

    class BeEGLGraphicsConfig
    {
        public:

            BeUInt m_uEGLRenderableType;
            BeUInt m_uEGLSurfaceType;

            BeUChar8 m_uEGLRedSize;
            BeUChar8 m_uEGLGreenSize;
            BeUChar8 m_uEGLBlueSize;

            BeUChar8 m_uEGLDepthSize;
            BeUChar8 m_uEGLStencilSize;

            BeUInt m_uScreenWidth;
            BeUInt m_uScreenHeight;

            BeEGLGraphicsConfig();
            BeEGLGraphicsConfig(const BeEGLGraphicsConfig& kGraphicsConfig);
    };

    static void Initialise ();

    static void Deinitialise ();

    static bool EnterCriticalSection (const BeString8& strCallingFrom = "");

    static bool ExitCriticalSection(const BeString8& strCallingFrom = "");

    static BeThreadMutex* s_pCriticalSection;
};

#endif // BE_GRAPHICS_H
