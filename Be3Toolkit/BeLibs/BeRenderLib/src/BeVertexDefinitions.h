/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeVertexDefinitions.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEVERTEXDEFINITIONS_H
#define BE_BEVERTEXDEFINITIONS_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>

/////////////////////////////////////////////////
/// \class BeVertexDefinitions
/// \brief Render project types
/////////////////////////////////////////////////
class RENDER_API BeVertexDefinitions
{

  public:

      /// \brief  Vertex definition for 2D sprites
      struct BeVFSprite2D
      {
          BeVector4D m_kPosition;
          BeVector2D m_kTexCoord;
      };

};

#endif // #ifndef BE_BEVERTEXDEFINITIONS_H
