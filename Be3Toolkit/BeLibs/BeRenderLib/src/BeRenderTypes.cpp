/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeRenderTypes.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <BeRenderTypes.h>
#include <Render/BeTexture.h>
#include <Render/BeMaterial.h>

/////////////////////////////////////////////////////////////////////////////
BeRenderTypes::BeRenderDeviceSettings::BeRenderDeviceSettings()
:
  m_uAdapterOrdinal(0),
  m_eRenderDeviceAPI(BeRenderTypes::RENDER_DEVICE_API_DX11),
  m_strWindowName(L"No APP name defined"),
  m_uBufferWidth(640),
  m_uBufferHeight(480),
  m_bWindowed(BE_FALSE),
  m_bVerticalSync(BE_FALSE),
  m_bStartApp(BE_TRUE)
{
}
/////////////////////////////////////////////////////////////////////////////
BeRenderTypes::BeRenderDeviceSettings::BeRenderDeviceSettings(const BeRenderDeviceSettings& kRenderDeviceSettings)
:
  m_uAdapterOrdinal(kRenderDeviceSettings.m_uAdapterOrdinal),
  m_eRenderDeviceAPI(kRenderDeviceSettings.m_eRenderDeviceAPI),
  m_strWindowName(kRenderDeviceSettings.m_strWindowName),
  m_uBufferWidth(kRenderDeviceSettings.m_uBufferWidth),
  m_uBufferHeight(kRenderDeviceSettings.m_uBufferHeight),
  m_bWindowed(kRenderDeviceSettings.m_bWindowed),
  m_bVerticalSync(kRenderDeviceSettings.m_bVerticalSync),
  m_bStartApp(kRenderDeviceSettings.m_bStartApp)
{
}
/////////////////////////////////////////////////////////////////////////////
BeRenderTypes::BeRenderDeviceSettings &BeRenderTypes::BeRenderDeviceSettings::operator =(const BeRenderDeviceSettings &kRenderDeviceSettings)
{
  m_uAdapterOrdinal = kRenderDeviceSettings.m_uAdapterOrdinal;
  m_eRenderDeviceAPI = kRenderDeviceSettings.m_eRenderDeviceAPI;
  m_strWindowName = kRenderDeviceSettings.m_strWindowName;
  m_uBufferWidth = kRenderDeviceSettings.m_uBufferWidth;
  m_uBufferHeight = kRenderDeviceSettings.m_uBufferHeight;
  m_bWindowed = kRenderDeviceSettings.m_bWindowed;
  m_bVerticalSync = kRenderDeviceSettings.m_bVerticalSync;
  m_bStartApp = kRenderDeviceSettings.m_bStartApp;
  return *this;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeRenderTypes::BeRenderDeviceDisplayMode::BeRenderDeviceDisplayMode()
:
  m_bWindowed(BE_TRUE),
  m_uResWidth(640),
  m_uResHeight(480),
  m_kFormat(-1),
  m_iRefreshRate(0),
  m_iScanlineOrdering(-1),
  m_iScaling(-1),
  m_hWindowHandle(NULL)
{
}
/////////////////////////////////////////////////////////////////////////////
BeRenderTypes::BeRenderDeviceDisplayMode::BeRenderDeviceDisplayMode(const BeRenderDeviceDisplayMode &kRenderDeviceDisplayMode)
:
  m_bWindowed(kRenderDeviceDisplayMode.m_bWindowed),
  m_uResWidth(kRenderDeviceDisplayMode.m_uResWidth),
  m_uResHeight(kRenderDeviceDisplayMode.m_uResHeight),
  m_kFormat(kRenderDeviceDisplayMode.m_kFormat),
  m_iRefreshRate(kRenderDeviceDisplayMode.m_iRefreshRate),
  m_iScanlineOrdering(kRenderDeviceDisplayMode.m_iScanlineOrdering),
  m_iScaling(kRenderDeviceDisplayMode.m_iScaling),
  m_hWindowHandle(kRenderDeviceDisplayMode.m_hWindowHandle)
{
}
/////////////////////////////////////////////////////////////////////////////
BeRenderTypes::BeRenderDeviceDisplayMode &BeRenderTypes::BeRenderDeviceDisplayMode::operator =(const BeRenderDeviceDisplayMode &kRenderDeviceDisplayMode)
{
  m_bWindowed = kRenderDeviceDisplayMode.m_bWindowed;
  m_uResWidth = kRenderDeviceDisplayMode.m_uResWidth;
  m_uResHeight = kRenderDeviceDisplayMode.m_uResHeight;
  m_kFormat = kRenderDeviceDisplayMode.m_kFormat;
  m_iRefreshRate = kRenderDeviceDisplayMode.m_iRefreshRate;
  m_iScanlineOrdering = kRenderDeviceDisplayMode.m_iScanlineOrdering;
  m_iScaling = kRenderDeviceDisplayMode.m_iScaling;
  m_hWindowHandle = kRenderDeviceDisplayMode.m_hWindowHandle;
  return *this;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeRenderTypes::BeDeviceAdapter::BeDeviceAdapter()
:
  m_strAdapterName(BeString16::EMPTY())
{
  for ( BeInt i = 0; i < BeRenderTypes::RENDER_DEVICE_UNKNOWN; ++i )
    m_kRenderAPIAvailable[i] = BE_FALSE;
}
/////////////////////////////////////////////////////////////////////////////
BeRenderTypes::BeDeviceAdapter::~BeDeviceAdapter()
{
  m_kEnumWindowedDisplayModes.clear();
  m_kEnumFullDisplayModes.clear();
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeRenderTypes::BeTextureCacheEntry::BeTextureCacheEntry()
:
  m_strTextureName(L"Empty\0"),
  m_xSize(0),
  m_ySize(0),
  m_pTexture(NULL),
  m_iSceneModelsUsingIt(0),
  m_bIsAllocated(BE_FALSE),
  m_bIsLoaded(BE_FALSE)
{
}
/////////////////////////////////////////////////////////////////////////////
BeRenderTypes::BeTextureCacheEntry::~BeTextureCacheEntry()
{
  Release();
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderTypes::BeTextureCacheEntry::Release(void)
{
    if (m_pTexture != NULL)
    {
    BeDelete m_pTexture;
    m_pTexture = NULL;
  }
    m_xSize = m_ySize = 0;
  m_strTextureName = L"Empty\0";
  m_iSceneModelsUsingIt = 0;
    m_bIsAllocated = false;
    m_bIsLoaded = false;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeRenderTypes::BeMaterialCacheEntry::BeMaterialCacheEntry()
:
m_strMaterialName(L"Empty\0"),
m_pMaterial(NULL),
m_bIsAllocated(BE_FALSE),
m_bIsLoaded(BE_FALSE)
{
}
/////////////////////////////////////////////////////////////////////////////
BeRenderTypes::BeMaterialCacheEntry::~BeMaterialCacheEntry()
{
    Release();
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderTypes::BeMaterialCacheEntry::Release(void)
{
    if (m_pMaterial != NULL)
    {
        BeDelete m_pMaterial;
        m_pMaterial = NULL;
    }
    m_bIsAllocated = false;
    m_bIsLoaded = false;
}
/////////////////////////////////////////////////////////////////////////////


