/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeMaterialProgram.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEMATERIALPROGRAM_H
#define BE_BEMATERIALPROGRAM_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>
#include <BeRenderTypes.h>

// Forward declarations
class BeRenderDevice;
class BeMaterialConstant;
class BeMaterialVertexDefinition;
class BeTextureChannel;

/////////////////////////////////////////////////
/// \class BeMaterialProgram
/// \brief High-level material program object
/////////////////////////////////////////////////
class RENDER_API BeMaterialProgram : public BeMemoryObject
{
  public:

      /// \brief  Constructor
      BeMaterialProgram();
      /// \brief  Destructor
      virtual ~BeMaterialProgram();


      // Methods
      // {

          /// \brief  Prepares the program with its constant and so
          virtual void PrepareProgram(BeRenderDevice* pRenderDevice) = 0;

          /// \brief  Sets the block constant name of the program
          void SetProgramConstantBlockName(const BeString8& strContantBlockName);

          /// \brief  Adds a new constant to the program
          void AddProgramConstant(BeRenderTypes::BeConstantConfig* pConstantConfig);

          /// \brief  Adds a new texture channel to the program
          void AddProgramTextureChannel(BeRenderTypes::BeTextureChannelConfig* pTextureChannelConfig);

          /// \brief  Make a request for updating a constant in the material
          void RequestUpdateConstant(const BeString8& strConstantName, BeVoidP pValue);

          /// \brief  Updates the program and constants all together
          virtual void UpdateProgram(BeRenderDevice* pRenderDevice) = 0;

          /// \brief  Sets the program constant buffers
          virtual void SetConstantBuffer(BeRenderDevice* pRenderDevice) = 0;

          /// \brief  Prepares the vertex definition
          virtual void PrepareVertexDefinition(BeRenderDevice* pRenderDevice);

          /// \brief  Sets the program texture samplers
          virtual void SetTextureSamplers(BeRenderDevice* pRenderDevice) = 0;

          /// \brief  Sets the program for rendering
          virtual void SetProgramForRendering(BeVoidP pRenderContext) = 0;

      // }



      // Getters
      // {

          /// \brief  Returns the material constant by name
          BeMaterialConstant* GetConstantByName(const BeString8& strConstantName);

          /// \brief  Returns the number of uniform indices the constant buffer needs
          virtual BeInt GetConstantIndicesCount(void) = 0;

          /// \brief  Returns the direct pointer to the material compiled data API impl
          virtual BeVoidP GetMaterialProgramCompiledDataPtr(void) = 0;

          /// \brief  Returns the direct pointer to the material API impl
          virtual BeVoidP GetMaterialProgramPtr(void) = 0;

          /// \brief  Returns the vertex definition
          BeMaterialVertexDefinition* GetVertexDefinition(void);

      // }



      // Setters
      // {

          /// \brief  Sets the direct pointer to the material compiled data API impl
          virtual void SetMaterialProgramCompiledDataPtr(BeVoidP pMaterialCompiledDataPtr) = 0;

          /// \brief  Sets the direct pointer to the material API impl
          virtual void SetMaterialProgramPtr(BeVoidP pMaterialPtr) = 0;

          /// \brief  Adds a new attribute to the vertex definition
          void SetVertexDefinition(BeMaterialVertexDefinition* pVertexDefinition);

      // }



  protected:

      /// \brief  Material vertex definition
      BeMaterialVertexDefinition* m_pVertexDefinition;

      /// \brief  Material Program type
      BeRenderTypes::EBeMaterialPipeline m_eProgramType;

      /// \brief  Program constant block name
      BeString8 m_strContantBlockName;

      /// \brief  Program constants
      std::vector<BeMaterialConstant*> m_kConstants;

      /// \brief  Texture channels
      std::vector<BeRenderTypes::BeTextureChannel*> m_kTextureChannels;

      /// \brief  Mutex object
      BeThreadMutex  m_kPendingConstantsMutex;

      /// \brief  Pending material constant updates
      std::map<BeString8, BeVoidP> m_kPendingConstantUpdates;

};

#endif // #ifndef BE_BEMATERIALPROGRAM_H
