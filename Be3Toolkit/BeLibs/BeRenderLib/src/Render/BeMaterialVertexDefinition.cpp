/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeMaterialVertexDefinition.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <Render/BeMaterialVertexDefinition.h>
#include <Render/BeMaterialVertexAttribute.h>

/////////////////////////////////////////////////////////////////////////////
BeMaterialVertexDefinition::~BeMaterialVertexDefinition()
{
    std::vector<BeMaterialVertexAttribute*>::iterator it = m_kAttributes.begin();
    while (it != m_kAttributes.end())
    {
        BeMaterialVertexAttribute* pAttribute = reinterpret_cast<BeMaterialVertexAttribute*>(*it);

        BeDelete pAttribute;

        it++;
    }

    m_kAttributes.clear();
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeInt BeMaterialVertexDefinition::GetAttributeCount(void)
{
    return m_kAttributes.size();
}
/////////////////////////////////////////////////////////////////////////////
BeMaterialVertexAttribute* BeMaterialVertexDefinition::GetAttributeByName(const BeString8& strAttributeName)
{
    std::vector<BeMaterialVertexAttribute*>::iterator it = m_kAttributes.begin();
    while (it != m_kAttributes.end())
    {
        BeMaterialVertexAttribute* pAttribute = reinterpret_cast<BeMaterialVertexAttribute*>(*it);
        if (pAttribute->m_strName.Compare(strAttributeName) == 0)
        {
            return pAttribute;
        }

        it++;
    }

    return NULL;
}
/////////////////////////////////////////////////////////////////////////////
BeMaterialVertexAttribute* BeMaterialVertexDefinition::GetAttributeByIndex(BeUInt iAttributeIdx)
{
    if (iAttributeIdx < m_kAttributes.size())
    {
        return m_kAttributes[iAttributeIdx];
    }

    return NULL;
}
/////////////////////////////////////////////////////////////////////////////
BeInt BeMaterialVertexDefinition::GetVertexDefinitionSize(void)
{
    BeInt iSize = 0;

    for (BeUInt i = 0; i < m_kAttributes.size(); ++i)
    {
        iSize += m_kAttributes[i]->GetTypeSize();
    }

    return iSize;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeMaterialVertexDefinition::AddAttribute(BeRenderTypes::BeAttributeConfig* pAttributeConfig)
{
    BeMaterialVertexAttribute* pAttribute = GetAttributeByName(pAttributeConfig->m_strName);
    if (!pAttribute)
    {
        BeMaterialVertexAttribute* pNewAttribute = BeNew BeMaterialVertexAttribute(pAttributeConfig->m_strName, pAttributeConfig->m_strSemantic, pAttributeConfig->m_iSemanticIndex, pAttributeConfig->m_strType, pAttributeConfig->m_bNormalised);

        m_kAttributes.push_back(pNewAttribute);
    }
}
/////////////////////////////////////////////////////////////////////////////

