/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeVertexBuffer.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEVERTEXBUFFER_H
#define BE_BEVERTEXBUFFER_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>

// Forward declarations
class BeRenderManager;

/////////////////////////////////////////////////
/// \class BeVertexBuffer
/// \brief High-level mesh object
/////////////////////////////////////////////////
class RENDER_API BeVertexBuffer : public BeMemoryObject
{

  public:

    // Getters
    // {

        /// \brief  Returns the direct pointer to the vertex buffer API impl
        virtual BeVoidP GetVertexBufferPtr (void) = 0;

        /// \brief  Returns the vertex size or stride
        BeSize_T GetVertexElementSize (void);

    // }



    // Setters
    // {

        /// \brief  Sets the direct pointer to the vertex buffer API impl
        virtual void SetVertexBufferPtr(BeVoidP pVBPtr, BeSize_T iVertexSize) = 0;

    // }

protected:

    /// \brief  Size of the vertices elements (stride)
    BeSize_T m_iVertexSize;

};

#endif // #ifndef BE_BEVERTEXBUFFER_H
