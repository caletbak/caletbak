/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeSprite2D.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <Render/2D/BeSprite2D.h>
#include <Render/BeRenderDevice.h>
#include <Managers/BeRenderManager.h>
#include <Managers/BeTextureManager.h>
#include <BeVertexDefinitions.h>

/////////////////////////////////////////////////////////////////////////////
BeSprite2D::BeSprite2DAnimation::BeSprite2DAnimation (BeInt* pAnimationSequenceTiledArray, BeUInt iTimeInterFrame)
:
  m_iCurrentFrame (0),
  m_iTimeInterFrame (iTimeInterFrame)
{
  m_iSequenceFramesNumber = 0;
  while (pAnimationSequenceTiledArray[m_iSequenceFramesNumber] != -1)
  {
    m_iSequenceFramesNumber++;
  }

  m_pAnimationSequence = BeNew BeUInt[m_iSequenceFramesNumber];
  for (BeUInt i = 0; i < m_iSequenceFramesNumber; ++i)
  {
    m_pAnimationSequence[i] = pAnimationSequenceTiledArray[i];
  }
}
/////////////////////////////////////////////////////////////////////////////
BeSprite2D::BeSprite2DAnimation::~BeSprite2DAnimation ()
{
  BeDeleteArray m_pAnimationSequence;
  m_pAnimationSequence = NULL;
}
/////////////////////////////////////////////////////////////////////////////
void BeSprite2D::BeSprite2DAnimation::Start ()
{
  m_iCurrentFrame = 0;
  m_kTimer.Start (m_iTimeInterFrame, BE_FALSE);
}
/////////////////////////////////////////////////////////////////////////////
BeInt BeSprite2D::BeSprite2DAnimation::Update ()
{
  if (m_kTimer.IsFinished ())
  {
    m_iCurrentFrame++;
    if (m_iCurrentFrame >= m_iSequenceFramesNumber)
    {
      m_iCurrentFrame = 0;
    }
    m_kTimer.Start (m_iTimeInterFrame, BE_FALSE);
  }

  return m_pAnimationSequence[m_iCurrentFrame];
}
/////////////////////////////////////////////////////////////////////////////
void BeSprite2D::BeSprite2DAnimation::Stop ()
{
  m_iCurrentFrame = 0;
  m_kTimer.Stop ();
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeSprite2D::BeSprite2D(BeRenderManager* pRenderManager, BeString16 strTextureFileName, BeInt ePivot, BeInt iTilesNumX, BeInt iTilesNumY)
:
  m_bIsContentSharing (BE_FALSE),
  m_iCurrentTileRendering (0),
  m_iCurrentAnimationSequencePlaying (-1),
  m_iSpritePivot(ePivot)
{
  m_pRenderManager = pRenderManager;

  m_iTextureSlot = m_pRenderManager->GetRenderDevice ()->LoadTextureFromFile (strTextureFileName);
  m_pTextureCacheEntry = m_pRenderManager->GetRenderDevice ()->GetTextureManager ()->GetTextureCacheEntry (m_iTextureSlot);

  m_kPosition.m_fX = 0.0f;
  m_kPosition.m_fY = 0.0f;

  m_kOrientation.m_fHeading = 0.0f;
  m_kOrientation.m_fPitch = 0.0f;
  m_kOrientation.m_fBank = 0.0f;

  m_kScale.m_fX = 1.0f;
  m_kScale.m_fY = 1.0f;

  m_kTintColour.m_fX = 1.0f;
  m_kTintColour.m_fY = 1.0f;
  m_kTintColour.m_fZ = 1.0f;
  m_kTintColour.m_fW = 1.0f;

  m_kTilesNumber.m_fX = (BeFloat) iTilesNumX;
  m_kTilesNumber.m_fY = (BeFloat) iTilesNumY;

  m_kTileSize.m_fX = (BeFloat) (m_pTextureCacheEntry->m_xSize / iTilesNumX);
  m_kTileSize.m_fY = (BeFloat) (m_pTextureCacheEntry->m_ySize / iTilesNumY);

  m_kTileMeshes = new std::vector<BeMesh*>();
  PrepareTileMeshes ();

  SetTileToRender (0);
}
/////////////////////////////////////////////////////////////////////////////
BeSprite2D::BeSprite2D (BeSprite2D* pSharedSprite2D)
:
  m_bIsContentSharing (BE_TRUE),
  m_iCurrentTileRendering (0),
  m_iCurrentAnimationSequencePlaying (-1)
{
  m_pRenderManager = pSharedSprite2D->m_pRenderManager;

  m_iSpritePivot = pSharedSprite2D->m_iSpritePivot;

  m_iTextureSlot = pSharedSprite2D->m_iTextureSlot;
  m_pTextureCacheEntry = pSharedSprite2D->m_pTextureCacheEntry;

  m_kPosition.m_fX = 0.0f;
  m_kPosition.m_fY = 0.0f;

  m_kOrientation.m_fHeading = 0.0f;
  m_kOrientation.m_fPitch = 0.0f;
  m_kOrientation.m_fBank = 0.0f;

  m_kScale.m_fX = 1.0f;
  m_kScale.m_fY = 1.0f;

  m_kTintColour.m_fX = 1.0f;
  m_kTintColour.m_fY = 1.0f;
  m_kTintColour.m_fZ = 1.0f;
  m_kTintColour.m_fW = 1.0f;

  m_kTilesNumber = pSharedSprite2D->m_kTilesNumber;
  m_kTileSize = pSharedSprite2D->m_kTileSize;

  m_kTileMeshes = pSharedSprite2D->m_kTileMeshes;

  SetTileToRender (0);
}
/////////////////////////////////////////////////////////////////////////////
BeSprite2D::~BeSprite2D ()
{
  if (!m_bIsContentSharing && m_iTextureSlot != -1)
  {
    m_pRenderManager->GetRenderDevice ()->FreeTexture (m_iTextureSlot);
  }

  if (m_kTileMeshes)
  {
      for (BeUInt i = 0; i < m_kTileMeshes->size(); ++i)
      {
          BeMesh* pMesh = reinterpret_cast<BeMesh*>(m_kTileMeshes->at(i));

          delete pMesh;
      }
      m_kTileMeshes->clear();

      delete m_kTileMeshes;
      m_kTileMeshes = NULL;
  }

  m_kAnimationSequences.clear ();

  m_pRenderManager = NULL;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeSprite2D::PrepareTileMeshes (void)
{
    BeUInt iTotalTiles = (BeUInt) (m_kTilesNumber.m_fX * m_kTilesNumber.m_fY);

    float fPositionOffsetX = -0.5f;
    float fPositionOffsetY = -0.5f;

    if (m_iSpritePivot & PIVOT_H_LEFT)
    {
        fPositionOffsetX = 0.0f;
    }
    else if (m_iSpritePivot & PIVOT_H_RIGHT)
    {
        fPositionOffsetX = 1.0f;
    }

    if (m_iSpritePivot & PIVOT_V_TOP)
    {
        fPositionOffsetY = 0.0f;
    }
    else if (m_iSpritePivot & PIVOT_V_BOTTOM)
    {
        fPositionOffsetY = 1.0f;
    }

    float fTileTextureFragmentSizeX = 1.0f / m_kTilesNumber.m_fX;
    float fTileTextureFragmentSizeY = 1.0f / m_kTilesNumber.m_fY;

    for (BeUInt i = 0; i < iTotalTiles; ++i)
    {
        BeMesh* pMesh = new BeMesh(m_pRenderManager);

        int iTileX = i % ((BeInt) m_kTilesNumber.m_fX);
        int iTileY = i / ((BeInt) m_kTilesNumber.m_fX);

        BeVertexDefinitions::BeVFSprite2D kMeshVertices[4];
        for (BeUInt j = 0; j < 4; ++j)
        {
            kMeshVertices[j].m_kPosition.m_fX = (j % 2) * 1.0f;
            kMeshVertices[j].m_kPosition.m_fY = (j / 2) * 1.0f;
            kMeshVertices[j].m_kPosition.m_fZ = kMeshVertices[j].m_kPosition.m_fW = 1.0f;

            if (iTotalTiles > 1)
            {
                kMeshVertices[j].m_kTexCoord.m_fX = (iTileX * fTileTextureFragmentSizeX) + (kMeshVertices[j].m_kPosition.m_fX * fTileTextureFragmentSizeX);
                kMeshVertices[j].m_kTexCoord.m_fY = (iTileY * fTileTextureFragmentSizeY) + (kMeshVertices[j].m_kPosition.m_fY * fTileTextureFragmentSizeY);
            }
            else
            {
                kMeshVertices[j].m_kTexCoord.m_fX = kMeshVertices[j].m_kPosition.m_fX;
                kMeshVertices[j].m_kTexCoord.m_fY = kMeshVertices[j].m_kPosition.m_fY;
            }

            kMeshVertices[j].m_kPosition.m_fX += fPositionOffsetX;
            kMeshVertices[j].m_kPosition.m_fY += fPositionOffsetY;
        }

        BeUShort kMeshIndices[] =
        {
            0, 1, 2,
            2, 1, 3
        };

        pMesh->CreateMeshBuffers(&kMeshVertices[0], &kMeshIndices[0], sizeof(BeVertexDefinitions::BeVFSprite2D), 4, 2, "material_2d_VS");

        m_kTileMeshes->push_back (pMesh);
    }
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeInt BeSprite2D::GetSpriteTextureID (void)
{
  return m_iTextureSlot;
}
/////////////////////////////////////////////////////////////////////////////
BeRenderTypes::BeTextureCacheEntry* BeSprite2D::GetSpriteTexture (void)
{
  return m_pTextureCacheEntry;
}
/////////////////////////////////////////////////////////////////////////////
void BeSprite2D::GetPosition (BeVector2D* kPosition)
{
  if (kPosition)
  {
    kPosition->m_fX = m_kPosition.m_fX;
    kPosition->m_fY = m_kPosition.m_fY;
  }
}
/////////////////////////////////////////////////////////////////////////////
void BeSprite2D::GetOrientation (BeEulerAngles* kOrientation)
{
  if (kOrientation)
  {
    kOrientation->m_fHeading = m_kOrientation.m_fHeading;
    kOrientation->m_fPitch = m_kOrientation.m_fPitch;
    kOrientation->m_fBank = m_kOrientation.m_fBank;
  }
}
/////////////////////////////////////////////////////////////////////////////
void BeSprite2D::GetScale (BeVector2D* kScale)
{
  if (kScale)
  {
    kScale->m_fX = m_kScale.m_fX;
    kScale->m_fY = m_kScale.m_fY;
  }
}
/////////////////////////////////////////////////////////////////////////////
BeVector2D* BeSprite2D::GetPosition (void)
{
  return &m_kPosition;
}
/////////////////////////////////////////////////////////////////////////////
BeEulerAngles* BeSprite2D::GetOrientation (void)
{
  return &m_kOrientation;
}
/////////////////////////////////////////////////////////////////////////////
BeVector2D* BeSprite2D::GetScale (void)
{
  return &m_kScale;
}
/////////////////////////////////////////////////////////////////////////////
BeVector4D* BeSprite2D::GetColour ()
{
  return &m_kTintColour;
}
/////////////////////////////////////////////////////////////////////////////
BeRenderTypes::BeRect2D* BeSprite2D::GetSpriteRect (void)
{
  return &m_kCurrentSpriteRect;
}
/////////////////////////////////////////////////////////////////////////////
BeVector2D* BeSprite2D::GetTileSize (void)
{
  return &m_kTileSize;
}
/////////////////////////////////////////////////////////////////////////////
BeMesh* BeSprite2D::GetCurrentTileMesh(void)
{
    return m_kTileMeshes->at(m_iCurrentTileRendering);
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeSprite2D::SetPosition (BeFloat fCoordX, BeFloat fCoordY)
{
  m_kPosition.m_fX = fCoordX;
  m_kPosition.m_fY = fCoordY;
}
/////////////////////////////////////////////////////////////////////////////
void BeSprite2D::SetOrientation (BeFloat fHeading)
{
  m_kOrientation.m_fHeading = fHeading;
}
/////////////////////////////////////////////////////////////////////////////
void BeSprite2D::SetScale (BeFloat fScaleX, BeFloat fScaleY)
{
  m_kScale.m_fX = fScaleX;
  m_kScale.m_fY = fScaleY;
}
/////////////////////////////////////////////////////////////////////////////
void BeSprite2D::SetColour (BeFloat fR, BeFloat fG, BeFloat fB, BeFloat fA)
{
  m_kTintColour.m_fX = fR;
  m_kTintColour.m_fY = fG;
  m_kTintColour.m_fZ = fB;
  m_kTintColour.m_fW = fA;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeSprite2D::Update (void)
{
  if (m_iCurrentAnimationSequencePlaying != -1)
  {
    BeInt newTile = m_kAnimationSequences[m_iCurrentAnimationSequencePlaying]->Update ();

    if (newTile != m_iCurrentTileRendering)
    {
      SetTileToRender (newTile);
    }
  }
}
/////////////////////////////////////////////////////////////////////////////
void BeSprite2D::AddAnimation (BeInt* pAnimationSequenceTiledArray, BeUInt iTimeInterFrame)
{
  BeSprite2DAnimationPtr spNewAnimationSequence = BeNew BeSprite2DAnimation (pAnimationSequenceTiledArray, iTimeInterFrame);
  m_kAnimationSequences.push_back (spNewAnimationSequence);
}
/////////////////////////////////////////////////////////////////////////////
void BeSprite2D::PlayAnimation (BeUInt iAnimationSequence)
{
  if (iAnimationSequence < m_kAnimationSequences.size ())
  {
    StopAnimation ();

    m_iCurrentAnimationSequencePlaying = iAnimationSequence;
    m_kAnimationSequences[m_iCurrentAnimationSequencePlaying]->Start ();
  }
}
/////////////////////////////////////////////////////////////////////////////
void BeSprite2D::StopAnimation ()
{
  if (m_iCurrentAnimationSequencePlaying != -1)
  {
    m_kAnimationSequences[m_iCurrentAnimationSequencePlaying]->Stop ();
  }
}
/////////////////////////////////////////////////////////////////////////////
void BeSprite2D::SetTileToRender (const BeInt iTile, const BeInt iOffsetLeft, const BeInt iOffsetTop, const BeInt iNewSizeX, const BeInt iNewSizeY)
{
  m_iCurrentTileRendering = iTile;

  BeInt iTilesNumX = (BeInt) m_kTilesNumber.m_fX;
  BeInt iTilesNumY = (BeInt) m_kTilesNumber.m_fY;

  BeInt iTotalTiles = iTilesNumX * iTilesNumY;
  if (iTile >= 0 && iTile < iTotalTiles)
  {
    BeInt iTilesSizeX = (BeInt) m_kTileSize.m_fX;
    BeInt iTilesSizeY = (BeInt) m_kTileSize.m_fY;

    m_kCurrentSpriteRect.m_iLeft = ((iTile % iTilesNumX) * iTilesSizeX) + iOffsetLeft;
    m_kCurrentSpriteRect.m_iTop = ((iTile / iTilesNumY) * iTilesSizeY) + iOffsetTop;
    if (iNewSizeX != -1)
    {
      m_kCurrentSpriteRect.m_iRight = m_kCurrentSpriteRect.m_iLeft + iNewSizeX;
    }
    else
    {
      m_kCurrentSpriteRect.m_iRight = m_kCurrentSpriteRect.m_iLeft + iTilesSizeX;
    }
    if (iNewSizeY != -1)
    {
      m_kCurrentSpriteRect.m_iBottom = m_kCurrentSpriteRect.m_iTop + iNewSizeY;
    }
    else
    {
      m_kCurrentSpriteRect.m_iBottom = m_kCurrentSpriteRect.m_iTop + iTilesSizeY;
    }
  }
}
/////////////////////////////////////////////////////////////////////////////
void BeSprite2D::Render (void)
{
    BeMatrix mWorldRotation;
    mWorldRotation.Identity();
    mWorldRotation.SetupRotate(BeEulerAngles::AXIS_X, m_kOrientation.m_fHeading);

    BeMatrix mWorldScale;
    mWorldScale.Identity();
    mWorldScale.SetupScale(BeVector3D(m_kTileSize.m_fX * m_kScale.m_fX, m_kTileSize.m_fY * m_kScale.m_fY, 1.0f));

    BeMatrix mWorldMatrix = mWorldScale * mWorldRotation;
    mWorldMatrix.SetTranslation(BeVector3D(m_kPosition.m_fX, m_kPosition.m_fY, 0.0f));

    m_pRenderManager->GetRenderDevice()->Render2D (this, mWorldMatrix);
}
/////////////////////////////////////////////////////////////////////////////


