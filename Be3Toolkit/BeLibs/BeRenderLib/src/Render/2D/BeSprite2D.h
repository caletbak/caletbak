/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeSprite2D.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BESPRITE2D_H
#define BE_BESPRITE2D_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>
#include <BeRenderTypes.h>
#include <Render/BeMesh.h>

// Forward delacations
class BeRenderManager;

/////////////////////////////////////////////////
/// \class BeSprite2D
/// \brief 2D Sprites for rendering
/////////////////////////////////////////////////
class RENDER_API BeSprite2D : public BeMemoryObject
{
  public:

        enum ESpritePivot
        {
            PIVOT_V_TOP = 0x01,
            PIVOT_V_CENTER = 0x02,
            PIVOT_V_BOTTOM = 0x04,
            PIVOT_H_LEFT = 0x08,
            PIVOT_H_CENTER = 0x10,
            PIVOT_H_RIGHT = 0x20
        };

        /// \brief  Constructor
        BeSprite2D(BeRenderManager* pRenderManager, BeString16 strTextureFileName, BeInt ePivot = (PIVOT_V_CENTER | PIVOT_H_CENTER), BeInt iTilesNumX = 1, BeInt iTilesNumY = 1);

        /// \brief  Constructor - The new Sprite will share the texture data
        BeSprite2D (BeSprite2D* pSharedSprite2D);

        /// \brief  Destructor
        virtual ~BeSprite2D ();



    // \brief Getters
    // {

      /// \brief  Get Sprite texture ID from texture manager
      BeInt GetSpriteTextureID (void);

      /// \brief  Get Sprite texture entry from texture manager
      BeRenderTypes::BeTextureCacheEntry* GetSpriteTexture (void);

      /// \brief  Get Sprite Position
      void GetPosition (BeVector2D* kPosition);

      /// \brief  Get Sprite Orientation
      void GetOrientation (BeEulerAngles* kOrientation);

      /// \brief  Get Sprite Scale
      void GetScale (BeVector2D* kScale);

      /// \brief  Get Sprite Position
      BeVector2D* GetPosition (void);

      /// \brief  Get Sprite Orientation
      BeEulerAngles* GetOrientation (void);

      /// \brief  Get Sprite Scale
      BeVector2D* GetScale (void);

      /// \brief  Get Sprite Tint Colour
      BeVector4D* GetColour (void);

      /// \brief  Get the current RECT area of the sprite to render
      BeRenderTypes::BeRect2D* GetSpriteRect (void);

      /// \brief  Get the tile Size
      BeVector2D* GetTileSize (void);

      /// \brief  Get the current tile mesh to render
      BeMesh* GetCurrentTileMesh (void);

    // }



    // \brief Setters
    // {

      /// \brief  Set Sprite Position
      void SetPosition (BeFloat fCoordX, BeFloat fCoordY);

      /// \brief  Set Sprite Orientation
      void SetOrientation (BeFloat fHeading);

      /// \brief  Set Sprite Scale
      void SetScale (BeFloat fScaleX, BeFloat fScaleY);

      /// \brief  Set Sprite Tint Colour
      void SetColour (BeFloat fR, BeFloat fG, BeFloat fB, BeFloat fA);

    // }



    // \brief Methods
    // {

      /// \brief  Updates the sprite and its animations
      void Update (void);

      /// \brief  Adds a new animation sequence to the sprite
      void AddAnimation (BeInt* pAnimationSequenceTiledArray, BeUInt iTimeInterFrame);

      /// \brief  Selects the current sequence animation to be played
      void PlayAnimation (BeUInt iAnimationSequence);

      /// \brief  Stops the current animation being played
      void StopAnimation (void);

      /// \brief  Selects the current Tile to render
      void SetTileToRender (const BeInt iTile, const BeInt iOffsetLeft = 0, const BeInt iOffsetTop = 0, const BeInt iNewSizeX = -1, const BeInt iNewSizeY = -1);

      /// \brief  Renders the Sprite
      void Render (void);

    // }

  private:

      /// \brief  Prepares the meshes buffer for all the sprite tiles
      void PrepareTileMeshes (void);



    /// \brief  Handle to render manager
    BeRenderManager* m_pRenderManager;

    /// \brief  The assigned TextureSlot ID for this Sprite texture
    BeInt m_iTextureSlot;

    /// \brief  The assigned TextureSlot for this Sprite texture
    BeRenderTypes::BeTextureCacheEntry* m_pTextureCacheEntry;

    /// \brief  The Sprite Pivot
    BeInt m_iSpritePivot;

    /// \brief  The Sprite 2D position
    BeVector2D m_kPosition;

    /// \brief  The Sprite 2D orientation
    BeEulerAngles m_kOrientation;

    /// \brief  The Sprite 2D scale
    BeVector2D m_kScale;

    /// \brief  The Sprite 2D tint colour
    BeVector4D m_kTintColour;

    /// \brief  The Alpha Blend value
    BeFloat m_fAlphaBlend;

    /// \brief  The current RECT area of the sprite to render
    BeRenderTypes::BeRect2D m_kCurrentSpriteRect;

    /// \brief  The Number of tiles horizontally and vertically for this texture
    BeVector2D m_kTilesNumber;

    /// \brief  The tileSize generated for number of tiles and texture size
    BeVector2D m_kTileSize;

    /// \brief  The current tile to be rendered
    BeInt m_iCurrentTileRendering;

    /// \brief  The render meshes foreach tile
    std::vector<BeMesh*>* m_kTileMeshes;

    /// \brief  The current animation sequence being played
    BeInt m_iCurrentAnimationSequencePlaying;

    /// \brief  Is this sprite sharing texture content from a different sprite?
    BeBoolean m_bIsContentSharing;

    /// \brief  Class for defining Sprite animation sequences
    class BeSprite2DAnimation : public BeMemoryObject
    {
      public:

        /// \brief  Constructor - Finish the pAnimationSequenceTiledArray with -1
        BeSprite2DAnimation(BeInt* pAnimationSequenceTiledArray, BeUInt iTimeInterFrame);

        /// \brief  Destructor
        ~BeSprite2DAnimation();

        // \brief Methods
        // {

          /// \brief  Starts the animation from its first frame
          void Start (void);

          /// \brief  Updates the animation
          BeInt Update (void);

          /// \brief  Stops the animation to its first frame
          void Stop (void);

        // }

      private:

        /// \brief  The animation tiled sequence
        BeUInt* m_pAnimationSequence;

        /// \brief  The animation sequence current frame to render
        BeUInt m_iCurrentFrame;

        /// \brief  The animation sequence frame number
        BeUInt m_iSequenceFramesNumber;

        /// \brief  The time between each sequence frame
        BeUInt m_iTimeInterFrame;

        /// \brief  The timer
        BeTimer m_kTimer;
    };

    // Smart pointer BeSprite2DAnimation
    typedef BeSmartPointer<BeSprite2DAnimation> BeSprite2DAnimationPtr;

    /// \brief  The animation sequences array type
    typedef std::vector<BeSprite2DAnimationPtr> AnimationSequencesArray;

    /// \brief  The animation sequences
    AnimationSequencesArray m_kAnimationSequences;

};

// Smart pointer BeSprite2D
typedef BeSmartPointer<BeSprite2D> BeSprite2DPtr;

#endif // #ifndef BE_BESPRITE2D_H
