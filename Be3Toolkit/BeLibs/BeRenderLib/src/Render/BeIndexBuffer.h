/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeIndexBuffer.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEINDEXBUFFER_H
#define BE_BEINDEXBUFFER_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>

// Forward declarations
class BeRenderManager;

/////////////////////////////////////////////////
/// \class BeIndexBuffer
/// \brief High-level mesh object
/////////////////////////////////////////////////
class RENDER_API BeIndexBuffer : public BeMemoryObject
{

  public:

    // Getters
    // {

        /// \brief  Returns the direct pointer to the index buffer API impl
        virtual BeVoidP GetIndexBufferPtr(void) = 0;

        /// \brief  Returns the number of indices in the buffer
        BeInt GetIndexBufferCount(void);

    // }



    // Setters
    // {

        /// \brief  Sets the direct pointer to the index buffer API impl
        virtual void SetIndexBufferPtr(BeVoidP pVBPtr, BeInt iIndicesCount) = 0;

    // }

protected:

    /// \brief  Number of indices in the index buffer
    BeInt m_iIndicesCount;

};

#endif // #ifndef BE_BEINDEXBUFFER_H
