/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeRenderStates.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BERENDERSTATES_H
#define BE_BERENDERSTATES_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>

// Forward declarations
class BeRenderDevice;

/////////////////////////////////////////////////
/// \class BeRenderStates
/// \brief Class for updating the Render states
/////////////////////////////////////////////////
class RENDER_API BeRenderStates : public BeMemoryObject
{

  public:

      /// \brief  Render states API Objects
      enum ERenderStatesType
      {
          RENDER_STATES_RASTERIZER,
          RENDER_STATES_BLEND
      };

      /// \brief  Render states IDs
      enum ERenderStateID
      {
          RS_FILL_MODE,
          RS_CULL_MODE,
          RS_FRONT_IS_CCW,
          RS_DEPTH_BIAS,
          RS_DEPTH_BIAS_CLAMP,
          RS_SLOPE_SCALED_DEPTH_BIAS,
          RS_DEPTH_CLIP_ENABLE,
          RS_SCISSOR_ENABLE,
          RS_MULTI_SAMPLE_ENABLE,
          RS_ANTIALIASED_LINE_ENABLE
      };

      /// \brief  Fill mode types
      enum ERS_FillMode
      {
          FILL_WIREFRAME = 0,
          FILL_SOLID
      };

      /// \brief  Cull mode types
      enum ERS_CullMode
      {
          CULL_NONE = 0,
          CULL_FRONT,
          CULL_BACK
      };

      /// \brief Destructor
      virtual ~BeRenderStates();

      // Getters
      // {

            /// \brief  Returns the direct pointer to the render states API impl
            virtual BeVoidP GetRenderStatesPtr(ERenderStatesType eRSType) = 0;

      // }



      // Setters
      // {

            /// \brief  Sets the direct pointer to the render states API impl
            virtual void SetRenderStatesPtr(BeVoidP pRSPtr, ERenderStatesType eRSType) = 0;

      // }



      // Methods
      // {

            /// \brief  Make a request for updating an state in the render states
            void RequestUpdateState(const ERenderStateID& eRenderStateID, BeVoidP pValue);

            /// \brief  Updates the material and constants all together
            virtual void UpdateRenderStates(void) = 0;

      // }

protected:

    /// \brief  The render device
    BeRenderDevice* m_pRenderDevice;

    /// \brief  Mutex object
    BeThreadMutex  m_kPendingRenderStatesMutex;

    /// \brief  Pending render states updates
    std::map<ERenderStateID, BeVoidP> m_kPendingRenderStatesUpdates;

};

#endif // #ifndef BE_BERENDERSTATES_H
