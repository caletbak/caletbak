/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeTextureSampler.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BETEXTURESAMPLER_H
#define BE_BETEXTURESAMPLER_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>

// Forward declarations
class BeRenderDevice;

/////////////////////////////////////////////////
/// \class BeTextureSampler
/// \brief High-level texture sampler object
/////////////////////////////////////////////////
class RENDER_API BeTextureSampler : public BeMemoryObject
{
      
  public:

      /// \brief  Constructor
      BeTextureSampler();
      /// \brief  Destructor
      virtual ~BeTextureSampler();



      // Methods
      // {

        /// \brief  Prepares the texture sampler internally
        virtual void PrepareSampler(BeRenderDevice* pRenderDevice) = 0;

        

      // }



      // Getters
      // {

          /// \brief  Returns the direct pointer to the texture sampler API impl
            virtual BeVoidP GetTextureSamplerPtr(void) = 0;

      // }



      // Setters
      // {

          /// \brief  Sets the direct pointer to the texture sampler API impl
          virtual void SetTextureSamplerPtr(BeVoidP pSamplerPtr) = 0;

      // }



      BeString16        m_strName;
      BeString16        m_strConstantName;
      BeString16        m_strFilterMin;
      BeString16        m_strFilterMag;
      BeString16        m_strFilterMip;
      BeString16        m_strFilterFunc;
      BeString16        m_strAddressU;
      BeString16        m_strAddressV;
      BeString16        m_strAddressW;
      BeFloat           m_MipLODBias;
      BeInt             m_MaxAnisotropy;
      BeString16        m_strComparisionFunc;
      BeVector4D        m_kBorderColor;
      BeFloat           m_MinLOD;
      BeFloat           m_MaxLOD;

private:

        /// \brief  Converts an address name to API value
        virtual BeInt ConvertAddressToAPI(const BeString16& strAddress) = 0;

        /// \brief  Converts a comparision func to API value
        virtual BeInt ConvertComparisionFuncToAPI(const BeString16& strFunc) = 0;

};

#endif // #ifndef BE_BETEXTURESAMPLER_H
