/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeViewport.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEVIEWPORT_H
#define BE_BEVIEWPORT_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>

/////////////////////////////////////////////////
/// \class BeViewport
/// \brief High-level viewport
/////////////////////////////////////////////////
class RENDER_API BeViewport : public BeMemoryObject
{

  public:

      /// \brief  Constructor
      BeViewport();

      /// \brief  Destructor
      ~BeViewport();

      BeFloat m_fTopLeftX;
      BeFloat m_fTopLeftY;

      BeFloat m_fWidth;
      BeFloat m_fHeight;

      BeFloat m_fMinDepth;
      BeFloat m_fMaxDepth;

};

#endif // #ifndef BE_BEVIEWPORT_H
