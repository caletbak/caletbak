/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeConstantBuffer.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BECONSTANTBUFFER_H
#define BE_BECONSTANTBUFFER_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>

// Forward declarations
class BeRenderManager;

/////////////////////////////////////////////////
/// \class BeIndexBuffer
/// \brief High-level constant buffer
/////////////////////////////////////////////////
class RENDER_API BeConstantBuffer : public BeMemoryObject
{

  public:

      /// \brief  Destructor
      virtual ~BeConstantBuffer();



    // Getters
    // {

        /// \brief  Returns the direct pointer to the constant buffer API impl
        virtual BeVoidP GetConstantBufferPtr(void) = 0;

    // }



    // Setters
    // {

        /// \brief  Sets the direct pointer to the constant buffer API impl
        virtual void SetConstantBufferPtr(BeVoidP pVBPtr) = 0;

    // }

};

#endif // #ifndef BE_BECONSTANTBUFFER_H
