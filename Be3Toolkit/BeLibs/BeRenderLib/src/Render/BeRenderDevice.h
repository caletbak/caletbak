/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeRenderDevice.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BERENDERDEVICE_H
#define BE_BERENDERDEVICE_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>
#include <Managers/BeMaterialsManager.h>
#include <Render/BeMaterial.h>
#include <Render/BeViewport.h>
#include <Render/BeRenderStates.h>
#include <Render/BeTexture.h>

#include <BeRenderTypes.h>

// Forward declarations
class BeTextureManager;
class BeSprite2D;
class BeVertexArrayObject;
class BeVertexBuffer;
class BeIndexBuffer;
class BeConstantBuffer;
class BeTextureSampler;

/////////////////////////////////////////////////
/// \class BeRenderDevice
/// \brief High-level rendering device
/////////////////////////////////////////////////
class RENDER_API BeRenderDevice : public BeMemoryObject
{

  /// \brief  Class friendship
  friend class BeDevicesManager;
  friend class BeTextureManager;
  friend class BeMaterialsManager;
  friend class BeRenderDeviceGLEnumerator;
      
  public:

    /// \brief  Constructor
    BeRenderDevice();
    /// \brief  Destructor
    virtual ~BeRenderDevice();
    /// \brief  Release - This must be called by derived render devices in destructor
    void Release();



    // Getters
    // {

      /// \brief  Gets the Texture Manager
      BeTextureManager* GetTextureManager (void);

      /// \brief  Gets the Materials Manager
      BeMaterialsManager* GetMaterialsManager(void);

      /// \brief  Gets the Render states
      BeRenderStates* GetRenderStates(void);

      /// \brief  Gets the current view projection matrix to use in the scene
      const BeMatrix& GetSceneViewProjectionMatrix(void);

      /// \brief  Gets the current world view projection matrix to use in the render
      const BeMatrix& GetCurrentWorldViewProjectionMatrix(void);

    // }



    // Setters
    // {

      /// \brief  Sets the viewport
      virtual void SetViewport(const BeViewport& kViewport) = 0;

    // }



    // Methods
    // {

      /// \brief  Creates the render device
      virtual BeBoolean CreateRenderDevice(const BeRenderTypes::BeRenderDeviceSettings& kRenderDeviceSettings) = 0;

      /// \brief  Return the render device settings used when starting the app
      virtual const BeRenderTypes::BeRenderDeviceSettings& GetRenderDeviceSettings(void) = 0;

      /// \brief  Begins a scene
      virtual void BeginScene(const BeMatrix& kViewProjMatrix) = 0;

      /// \brief  Ends a scene
      virtual void EndScene (void) = 0;

      /// \brief  Clears current frame buffer
      virtual void Clear (const BeRenderTypes::EClearMask clearMask, const BeVector4D& clearColour, const BeFloat clearZValue, const BeInt clearStencilValue) = 0;

      /// \brief  Presents the current frame
      virtual BeBoolean Present(void) = 0;

      /// \brief  Renders a 2D Sprite
      void Render2D(BeSprite2D* pSprite, BeMatrix& mWorldMatrix);

    // }



    // Buffer Methods
    // {

      /// \brief  Creates a vertex array object (if the used API need it)
      virtual BeVertexArrayObject* CreateVertexArrayObject(void) = 0;

      /// \brief  Sets a previously created Vertex Array object for rendering
      virtual void SetVertexArrayObject(BeVertexArrayObject* pVertexArrayObject) = 0;

      /// \brief  Frees a previously created Vertex Array Object
      virtual void FreeVertexArrayObject(BeVertexArrayObject* pVertexArrayObject) = 0;

      /// \brief  Creates a vertex buffer with the incoming buffer
      virtual BeVertexBuffer* CreateVertexBuffer(BeVoidP pMeshVertices, BeSize_T iVertexStructSize, BeUInt uNumVertices, const BeString8& strVSProgram, BeVertexArrayObject* pVAO = NULL) = 0;

      /// \brief  Sets a previously created Vertex Buffer for rendering
      virtual void SetVertexBuffer(BeVertexBuffer* pVertexBuffer) = 0;

      /// \brief  Frees a previously created Vertex Buffer
      virtual void FreeVertexBuffer(BeVertexBuffer* pVertexBuffer) = 0;

      /// \brief  Creates an index buffer with the incoming buffer
      virtual BeIndexBuffer* CreateIndexBuffer(BeVoidP pMeshIndices, BeUInt uNumTriangles) = 0;

      /// \brief  Sets a previously created Index Buffer for rendering
      virtual void SetIndexBuffer(BeIndexBuffer* pIndexBuffer) = 0;

      /// \brief  Frees a previously created Index Buffer
      virtual void FreeIndexBuffer(BeIndexBuffer* pIndexBuffer) = 0;

      /// \brief  Creates a constant buffer with the incoming buffer
      virtual BeConstantBuffer* CreateConstantBuffer(BeVoidP pConstantBuffer, BeUInt uConstantBufferSize) = 0;

      /// \brief  Updates a constant buffer with the incoming buffer
      virtual void UpdateConstantBuffer(BeConstantBuffer* pConstantBuffer, BeVoidP pBuffer, BeUInt uBufferSize) = 0;

      /// \brief  Sets a previously created constant Buffer for rendering
      virtual void SetConstantBuffer(BeConstantBuffer* pConstantBuffer, BeRenderTypes::EBeMaterialPipeline eMaterialPipeline) = 0;

      /// \brief  Frees a previously created constant Buffer
      virtual void FreeConstantBuffer(BeConstantBuffer* pConstantBuffer) = 0;

      /// \brief  Renders an indexed primitive
      virtual void RenderIndexedPrimitive(BeVertexBuffer* pVB, BeIndexBuffer* pIB, BeVertexArrayObject* pVAO) = 0;

      /// \brief  Creates a vertex definition
      virtual BeMaterialVertexDefinition* CreateVertexDefinition(void) = 0;

      /// \brief  Creates the API related objects for a vertex definition previously created
      virtual BeVoidP ResolveVertexDefinition(BeMaterialVertexDefinition* pVertexDefinition, BeVoidP pInputData, BeMaterialProgram* pMaterialProgram) = 0;

    // }



    // Render states Methods
    // {

      /// \brief  Creates a render state API object
      virtual BeVoidP CreateRenderState(BeRenderStates::ERenderStatesType eRSType, BeVoidP pData) = 0;

      /// \brief  Updates a render state API object
      virtual void UpdateRenderState(BeRenderStates::ERenderStatesType eRSType, BeVoidP pData) = 0;

      /// \brief  Creates a texture sampler
      virtual BeTextureSampler* CreateTextureSampler(void) = 0;

      /// \brief  Creates the API related objects for a texture sampler previously created
      virtual BeVoidP ResolveTextureSampler(BeVoidP pInputData) = 0;

    // }



    // Texture Methods
    // {

      /// \brief  Loads a texture. Returns the handle of the cache slot where the texture will be allocated and it can be used once the texture is loaded and ready to use.
      BeInt LoadTextureFromFile(BeString16 strFileName);

      /// \brief  Frees a previously loaded texture
      void FreeTexture(BeInt iCacheHandle);

      /// \brief  Binds the texture to the selected channel
      virtual void BindTexture(BeInt iCacheHandle, BeTexture::ETextureChannel eChannel) = 0;

      /// \brief  Sets a previously created texture sampler for rendering
      virtual void SetSampler(BeTextureSampler* pTextureSampler, BeInt iSlot, BeRenderTypes::EBeMaterialPipeline eMaterialPipeline) = 0;

    // }


    // Material Methods
    // {

      /// \brief  Sets the material for rendering
      virtual void SetMaterialForRendering(BeMaterial* pMaterial) = 0;

      /// \brief  Creates a new material program
      virtual BeMaterialProgram* CreateMaterialProgramFromBuffer(BeRenderTypes::EBeMaterialPipeline eProgramType, BeVoidP pBufferData, BeInt iBufferSize, BeString8 strFunction) = 0;

    // }
  
  private:

    /// \brief  Creates the texture manager for this device
    void CreateTextureManager();

    /// \brief  Creates the materials manager for this device
    void CreateMaterialsManager();

    /// \brief  Creates the render states for this device
    virtual void CreateRenderStates() = 0;

    /// \brief  Creates a texture from buffer
    virtual BeBoolean CreateTextureFromBuffer(BeRenderTypes::BeTextureCacheEntry* pCacheTextureEntry, BeVoidP pData, BeInt iDataSize) = 0;

    /// \brief  Creates a material from buffer
    virtual BeBoolean CreateMaterialFromConfig(BeRenderTypes::BeMaterialCacheEntry* pCacheMaterialEntry, const BeRenderTypes::BeMaterialConfig& kMaterialConfig) = 0;

  protected:

    /// \brief  The Texture Manager
    BeTextureManager* m_pTextureManager;

    /// \brief  The Materials Manager
    BeMaterialsManager* m_pMaterialsManager;

    /// \brief  Current view projection matrix to use in the scene
    BeViewport m_kCurrentViewport;

    /// \brief  Current view projection matrix to use in the scene
    BeMatrix m_mSceneViewProjectionMatrix;

    /// \brief  Current world view projection matrix to use in the render
    BeMatrix m_mCurrentWorldViewProjectionMatrix;

    /// \brief  Current vertex array object set for rendering
    BeVertexArrayObject* m_pCurrentVertexArrayObjectSet;

    /// \brief  Current vertex buffer set for rendering
    BeVertexBuffer* m_pCurrentVertexBufferSet;

    /// \brief  Current index buffer set for rendering
    BeIndexBuffer* m_pCurrentIndexBufferSet;

    /// \brief  Current GS constant buffer set for rendering
    BeConstantBuffer* m_pCurrentGSConstantBufferSet;

    /// \brief  Current VS constant buffer set for rendering
    BeConstantBuffer* m_pCurrentVSConstantBufferSet;

    /// \brief  Current PS constant buffer set for rendering
    BeConstantBuffer* m_pCurrentPSConstantBufferSet;

    /// \brief  Current GS sampler set for rendering
    BeTextureSampler* m_pCurrentGSSamplerSet;

    /// \brief  Current VS sampler buffer set for rendering
    BeTextureSampler* m_pCurrentVSSamplerSet;

    /// \brief  Current PS sampler buffer set for rendering
    BeTextureSampler* m_pCurrentPSSamplerSet;

    /// \brief  Render states
    BeRenderStates* m_pRenderStates;

#if    TARGET_WINDOWS
    /// \brief  Window process callback
    static BeLResult CALLBACK WndProc(BeWindowHandle hWnd, BeUInt uMessage, BeWParam wParam, BeLParam lParam);

    /// \brief  Creates an app window
    static BeBoolean CreateAppWindow(BeString16 strWindowTitle, BeInt iWidth, BeInt iHeight, BeChar8 iBits, BeBoolean bFullScreen, BeWindowProcess pfWndProc);

    /// \brief  Destroys the app window
    static void DestroyAppWindow(void);
#endif
    
    /// \brief  The app window handle
    static BeWindowHandle m_hWindow;

    /// \brief  The Render API being used
    static BeRenderTypes::ERenderDeviceAPI m_eRenderAPIBeingUsed;
};

#endif // #ifndef BE_BERENDERDEVICE_H
