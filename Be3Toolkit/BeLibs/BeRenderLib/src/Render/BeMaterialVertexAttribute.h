/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeMaterialVertexAttribute.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEMATERIALVERTEXATTRIBUTE_H
#define BE_BEMATERIALVERTEXATTRIBUTE_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>

/////////////////////////////////////////////////
/// \class BeMaterialVertexAttibute
/// \brief High-level material vertex attibute object
/////////////////////////////////////////////////
class RENDER_API BeMaterialVertexAttribute : public BeMemoryObject
{

public:

    /// \brief Material attribute types
    enum eBeMaterialAttributeType
    {
        ATTRIBUTE_FLOAT = 0,
        ATTRIBUTE_FLOAT2,
        ATTRIBUTE_FLOAT3,
        ATTRIBUTE_FLOAT4,
        ATTRIBUTE_MATRIX
    };

    // Methods
    // {

        /// \brief  Constructor
        BeMaterialVertexAttribute(const BeString8& strAttribName, const BeString8& strAttribSemantic, const BeInt iAttribSemanticIndex, const BeString8& strAttibTypeName, const BeBoolean bIsNormalised);

        /// \brief  Destructor
        ~BeMaterialVertexAttribute();

        /// \brief  Init the static engine resources
        static void InitStaticResources(void);

        /// \brief  Destroy the static engine resources
        static void DestroyStaticResources(void);

    // }



    // Getters
    // {

        /// \brief  Returns the attibute type size
        BeSize_T GetTypeSize(void);

        /// \brief  Returns the component count for this type
        BeInt GetComponentCount(void);

    // }

public:

    /// \brief  Attribute name
    BeString8 m_strName;

    /// \brief  Attribute semantic
    BeString8 m_strSemantic;

    /// \brief  Attribute semantic index
    BeInt m_strSemanticIndex;

    /// \brief  Attribute type
    eBeMaterialAttributeType m_eType;

    /// \brief  Attribute type size
    BeSize_T m_eTypeSize;

    /// \brief  Attribute is normalised
    BeBoolean m_bNormalised;

private:

    /// \brief  Convert attribute type name to eBeMaterialAttributeType
    eBeMaterialAttributeType GetAttributeTypeByName(const BeString8& strAttributeTypeName);

};

#endif // #ifndef BE_BEMATERIALVERTEXATTRIBUTE_H
