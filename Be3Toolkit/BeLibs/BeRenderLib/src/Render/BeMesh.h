/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeMesh.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEMESH_H
#define BE_BEMESH_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>

// Forward delacations
class BeRenderManager;
class BeVertexArrayObject;
class BeVertexBuffer;
class BeIndexBuffer;

/////////////////////////////////////////////////
/// \class BeMesh
/// \brief High-level mesh object
/////////////////////////////////////////////////
class RENDER_API BeMesh : public BeMemoryObject
{

  public:

    // Methods
    // {

        /// \brief  Constructor
        BeMesh(BeRenderManager* pRenderManager);

        /// \brief  Destructor
        virtual ~BeMesh();

        /// \brief  Creates the mesh API buffers depending on input buffers
        void CreateMeshBuffers(BeVoidP pMeshVertices, BeVoidP pMeshIndices, BeSize_T iVertexStructSize, BeUInt uNumVertices, BeUInt uNumTriangles, const BeString8& strVSProgram);

        /// \brief  Releases current buffers
        void ReleaseBuffers (void);

    // }



    // Getters
    // {

        /// \brief  Get vertex array object
        BeVertexArrayObject* GetVertexArrayObject(void);

        /// \brief  Get vertex buffer
        BeVertexBuffer* GetVertexBuffer(void);

        /// \brief  Get index buffer
        BeIndexBuffer* GetIndexBuffer(void);

    // }

  private:

      /// \brief  Handle to render manager
      BeRenderManager* m_pRenderManager;

      /// \brief  Mesh vertex array object
      BeVertexArrayObject* m_pMeshVA;

      /// \brief  Mesh vertex buffer
      BeVertexBuffer* m_pMeshVB;

      /// \brief  Mesh index buffer
      BeIndexBuffer* m_pMeshIB;

};

#endif // #ifndef BE_BEMESH_H
