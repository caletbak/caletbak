/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeMaterial.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEMATERIAL_H
#define BE_BEMATERIAL_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>
#include <Managers/BeMaterialsManager.h>

#include <BeRenderTypes.h>

// Forward declarations
class BeRenderDevice;
class BeMaterialConstant;
class BeConstantBuffer;
class BeMaterialVertexDefinition;
class BeTextureSampler;
class BeMaterialProgram;

/////////////////////////////////////////////////
/// \class BeMaterial
/// \brief High-level material object
/////////////////////////////////////////////////
class RENDER_API BeMaterial : public BeMemoryObject
{

public:

    

    // Methods
    // {

        /// \brief  Constructor
        BeMaterial();

        /// \brief  Destructor
        virtual ~BeMaterial();



        /// \brief  Prepares the material with its constant and so
        virtual void PrepareMaterial(BeRenderDevice* pRenderDevice) = 0;

        /// \brief  Make a request for updating a constant in the material
        virtual void RequestUpdateConstant(const BeString8& strConstantName, BeVoidP pValue) = 0;

        /// \brief  Updates the material and constants all together
        virtual void UpdateMaterial(BeRenderDevice* pRenderDevice) = 0;

        /// \brief  Sets the material constant buffers into the render context
        virtual void SetConstantBuffers(BeRenderDevice* pRenderDevice) = 0;

        /// \brief  Sets the material texture samplers into the render context
        virtual void SetTextureSamplers(BeRenderDevice* pRenderDevice) = 0;

    // }



    // Getters
    // {

        /// \brief  Returns the direct pointer to the material API impl
        virtual BeMaterialProgram* GetMaterialProgram(BeRenderTypes::EBeMaterialPipeline eMaterialStep) = 0;

    // }



    // Setters
    // {

        /// \brief  Sets the current material for rendering
        virtual void SetMaterialForRendering (BeVoidP pRenderContext) = 0;

        /// \brief  Sets the direct pointer to the material API impl
        virtual void SetMaterialProgram(BeMaterialProgram* pMaterialProgram, BeRenderTypes::EBeMaterialPipeline eMaterialStep) = 0;

    // }

};

#endif // #ifndef BE_BEMATERIAL_H
