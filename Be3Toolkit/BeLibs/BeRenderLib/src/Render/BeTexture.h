/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeTexture.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BETEXTURE_H
#define BE_BETEXTURE_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>

/////////////////////////////////////////////////
/// \class BeTexture
/// \brief High-level texture object
/////////////////////////////////////////////////
class RENDER_API BeTexture : public BeMemoryObject
{
      
  public:

      enum ETextureChannel
      {
          TEXTURE_CHANNEL_0 = 0,
          TEXTURE_CHANNEL_1,
          TEXTURE_CHANNEL_2,
          TEXTURE_CHANNEL_3,
          TEXTURE_CHANNEL_4,
          TEXTURE_CHANNEL_5,
          TEXTURE_CHANNEL_6,
          TEXTURE_CHANNEL_7,
          TEXTURE_CHANNEL_8,
          TEXTURE_CHANNEL_9,
          TEXTURE_CHANNEL_10,
          TEXTURE_CHANNEL_11,
          TEXTURE_CHANNEL_12,
          TEXTURE_CHANNEL_13,
          TEXTURE_CHANNEL_14,
          TEXTURE_CHANNEL_15
      };

      /// \brief  Destructor
      virtual ~BeTexture();



      // Getters
      // {

        /// \brief  Returns the direct pointer to the texture API impl
        virtual BeVoidP GetTexturePtr(void) = 0;

      // }



      // Methods
      // {

        /// \brief  Sets the direct pointer to the texture API impl
        virtual void SetTexturePtr(BeVoidP pTexturePtr) = 0;

      // }

};

#endif // #ifndef BE_BETEXTURE_H
