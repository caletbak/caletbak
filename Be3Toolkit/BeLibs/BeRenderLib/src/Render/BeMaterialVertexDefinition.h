/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeMaterialVertexDefinition.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEMATERIALVERTEXDEFINITION_H
#define BE_BEMATERIALVERTEXDEFINITION_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>
#include <Render/BeMaterialVertexAttribute.h>
#include <BeRenderTypes.h>

// Forward declarations
class BeRenderDevice;
class BeMaterial;

/////////////////////////////////////////////////
/// \class BeMaterialVertexDefinition
/// \brief High-level material vertex definition object
/////////////////////////////////////////////////
class RENDER_API BeMaterialVertexDefinition : public BeMemoryObject
{

    public:

        /// \brief  Destructor
        virtual ~BeMaterialVertexDefinition();

        // Methods
        // {

            /// \brief  Adds an attribute to the vertex definition
            void AddAttribute(BeRenderTypes::BeAttributeConfig* pAttributeConfig);

            /// \brief  Prepares the API vertex definition with the added attributes
            virtual void PrepareVertexDefinition(BeRenderDevice* pRenderDevice, BeMaterialProgram* pMaterialProgram) = 0;

        // }

        

        // Getters
        // {

            /// \brief  Returns a vertex attribute by name
            BeMaterialVertexAttribute* GetAttributeByName(const BeString8& strAttributeName);

            /// \brief  Returns a vertex attribute by index
            BeMaterialVertexAttribute* GetAttributeByIndex(BeUInt iAttributeIdx);

            /// \brief  Returns the vertex definition structure size
            BeInt GetVertexDefinitionSize(void);

            /// \brief  Returns the number of attributes
            BeInt GetAttributeCount(void);

            /// \brief  Returns the direct pointer to the vertex definition API impl
            virtual BeVoidP GetVertexDefinitionPtr(void) = 0;

        // }



        // Setters
        // {

            /// \brief  Sets the direct pointer to the vertex definition API impl
            virtual void SetVertexDefinitionPtr(BeVoidP pVDefPtr) = 0;

        // }

    protected:

        /// \brief  Converts the attribute type to API format
        virtual BeInt ConvertAttributeTypeToAPIFormat(BeMaterialVertexAttribute::eBeMaterialAttributeType eType) = 0;

        /// \brief  Vertex definition attributes
        std::vector<BeMaterialVertexAttribute*> m_kAttributes;

};

#endif // #ifndef BE_BEMATERIALVERTEXDEFINITION_H
