/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeMaterialProgram.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <Render/BeMaterialProgram.h>
#include <Render/BeMaterialConstant.h>
#include <Render/BeMaterialVertexDefinition.h>

/////////////////////////////////////////////////////////////////////////////
BeMaterialProgram::BeMaterialProgram()
:
    m_pVertexDefinition(NULL)
{
    m_kPendingConstantUpdates.clear();
    m_strContantBlockName = BeString8::EMPTY();
}
/////////////////////////////////////////////////////////////////////////////
BeMaterialProgram::~BeMaterialProgram()
{
    std::vector<BeMaterialConstant*>::iterator it = m_kConstants.begin();
    while (it != m_kConstants.end())
    {
        BeMaterialConstant* pMaterialConstant = reinterpret_cast<BeMaterialConstant*>(*it);

        BeDelete pMaterialConstant;

        it++;
    }
    m_kConstants.clear();

    std::vector<BeRenderTypes::BeTextureChannel*>::iterator itTexChannel = m_kTextureChannels.begin();
    while (itTexChannel != m_kTextureChannels.end())
    {
        BeRenderTypes::BeTextureChannel* pTextureChannel = reinterpret_cast<BeRenderTypes::BeTextureChannel*>(*itTexChannel);

        BeDelete pTextureChannel;

        itTexChannel++;
    }
    m_kTextureChannels.clear();

    if (m_pVertexDefinition)
    {
        BeDelete m_pVertexDefinition;
        m_pVertexDefinition = NULL;
    }

    m_kPendingConstantUpdates.clear();
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeMaterialProgram::SetProgramConstantBlockName(const BeString8& strContantBlockName)
{
    m_strContantBlockName = strContantBlockName;
}
/////////////////////////////////////////////////////////////////////////////
void BeMaterialProgram::AddProgramConstant(BeRenderTypes::BeConstantConfig* pConstantConfig)
{
    BeMaterialConstant* pConstant = GetConstantByName(pConstantConfig->m_strName);
    if (!pConstant)
    {
        BeMaterialConstant* pNewConstant = BeNew BeMaterialConstant(pConstantConfig->m_strName, pConstantConfig->m_strType);

        m_kConstants.push_back(pNewConstant);
    }
}
/////////////////////////////////////////////////////////////////////////////
void BeMaterialProgram::AddProgramTextureChannel(BeRenderTypes::BeTextureChannelConfig* pTextureChannelConfig)
{
    BeRenderTypes::BeTextureChannel* pNewTextureChannel = BeNew BeRenderTypes::BeTextureChannel();
    pNewTextureChannel->m_strTextureConstant = pTextureChannelConfig->m_strTextureConstant;
    pNewTextureChannel->m_strSamplerConstant = pTextureChannelConfig->m_strSamplerConstant;
    pNewTextureChannel->m_pTextureSampler = pTextureChannelConfig->m_pTextureSampler;

    m_kTextureChannels.push_back(pNewTextureChannel);
}
/////////////////////////////////////////////////////////////////////////////
void BeMaterialProgram::RequestUpdateConstant(const BeString8& strConstantName, BeVoidP pValue)
{
    m_kPendingConstantsMutex.Lock();

    BeMaterialConstant* pConstant = GetConstantByName(strConstantName);
    if (pConstant)
    {
        m_kPendingConstantUpdates[strConstantName] = pValue;
    }

    m_kPendingConstantsMutex.UnLock();
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeMaterialProgram::SetVertexDefinition(BeMaterialVertexDefinition* pVertexDefinition)
{
    m_pVertexDefinition = pVertexDefinition;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeMaterialVertexDefinition* BeMaterialProgram::GetVertexDefinition(void)
{
    return m_pVertexDefinition;
}
/////////////////////////////////////////////////////////////////////////////
BeMaterialConstant* BeMaterialProgram::GetConstantByName(const BeString8& strConstantName)
{
    std::vector<BeMaterialConstant*>::iterator it = m_kConstants.begin();
    while (it != m_kConstants.end())
    {
        BeMaterialConstant* pConstant = reinterpret_cast<BeMaterialConstant*>(*it);
        if (pConstant->m_strName.Compare(strConstantName) == 0)
        {
            return pConstant;
        }

        it++;
    }

    return NULL;
}
/////////////////////////////////////////////////////////////////////////////
void BeMaterialProgram::PrepareVertexDefinition(BeRenderDevice* pRenderDevice)
{
    if (m_pVertexDefinition)
    {
        m_pVertexDefinition->PrepareVertexDefinition(pRenderDevice, this);
    }
}
/////////////////////////////////////////////////////////////////////////////

