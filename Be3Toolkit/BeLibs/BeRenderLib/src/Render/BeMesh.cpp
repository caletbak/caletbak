/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeMesh.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <Render/BeMesh.h>
#include <Render/BeRenderDevice.h>
#include <Managers/BeRenderManager.h>


/////////////////////////////////////////////////////////////////////////////

BeMesh::BeMesh(BeRenderManager* pRenderManager)
:
    m_pRenderManager(pRenderManager),
    m_pMeshVA(NULL),
    m_pMeshVB(NULL),
    m_pMeshIB(NULL)
{
}

/////////////////////////////////////////////////////////////////////////////

BeMesh::~BeMesh()
{
    ReleaseBuffers ();

    m_pRenderManager = NULL;
}

/////////////////////////////////////////////////////////////////////////////

void BeMesh::CreateMeshBuffers(BeVoidP pMeshVertices, BeVoidP pMeshIndices, BeSize_T iVertexStructSize, BeUInt uNumVertices, BeUInt uNumTriangles, const BeString8& strVSProgram)
{
    ReleaseBuffers();

    m_pMeshVA = m_pRenderManager->GetRenderDevice()->CreateVertexArrayObject();

    if (m_pMeshVA)
    {
        m_pRenderManager->GetRenderDevice()->SetVertexArrayObject(m_pMeshVA);

        m_pMeshVB = m_pRenderManager->GetRenderDevice()->CreateVertexBuffer(pMeshVertices, iVertexStructSize, uNumVertices, strVSProgram, m_pMeshVA);

        m_pMeshIB = m_pRenderManager->GetRenderDevice()->CreateIndexBuffer(pMeshIndices, uNumTriangles);

        m_pRenderManager->GetRenderDevice()->SetVertexArrayObject(NULL);

        m_pRenderManager->GetRenderDevice()->SetVertexBuffer(NULL);

        m_pRenderManager->GetRenderDevice()->SetIndexBuffer(NULL);
    }
}

/////////////////////////////////////////////////////////////////////////////

void BeMesh::ReleaseBuffers(void)
{
    if (m_pMeshVB)
    {
        m_pRenderManager->GetRenderDevice()->FreeVertexBuffer(m_pMeshVB);

        m_pMeshVB = NULL;
    }

    if (m_pMeshIB)
    {
        m_pRenderManager->GetRenderDevice()->FreeIndexBuffer(m_pMeshIB);

        m_pMeshIB = NULL;
    }

    if (m_pMeshVA)
    {
        m_pRenderManager->GetRenderDevice()->FreeVertexArrayObject(m_pMeshVA);

        m_pMeshVA = NULL;
    }
}

/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeVertexArrayObject* BeMesh::GetVertexArrayObject(void)
{
    return m_pMeshVA;
}
/////////////////////////////////////////////////////////////////////////////
BeVertexBuffer* BeMesh::GetVertexBuffer(void)
{
    return m_pMeshVB;
}
/////////////////////////////////////////////////////////////////////////////
BeIndexBuffer* BeMesh::GetIndexBuffer(void)
{
    return m_pMeshIB;
}
/////////////////////////////////////////////////////////////////////////////

