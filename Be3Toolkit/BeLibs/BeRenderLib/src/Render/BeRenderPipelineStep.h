/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeRenderPipelineStep.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BERENDERPIPELINESTEP_H
#define BE_BERENDERPIPELINESTEP_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>

// Forward declarations
class BeRenderDevice;

/////////////////////////////////////////////////
/// \class BeRenderPipelineStep
/// \brief Render pipeline step where perform part of the rendering
/////////////////////////////////////////////////
class RENDER_API BeRenderPipelineStep : public BeMemoryObject
{
      
  public:
    
    // Getters
    // {
    
      

    // }

    // Methods
    // {

      /// \brief  Performs a pre rendering process
      virtual void PreRender(BeRenderDevice* pRenderDevice);

      /// \brief  Performs the rendering stuff
      virtual void Render(BeRenderDevice* pRenderDevice) = 0;

      /// \brief  Performs a post rendering process
      virtual void PostRender(BeRenderDevice* pRenderDevice);
          
    // }
            
};

// Smart pointer BeRenderPipelineStepPtr
typedef BeSmartPointer<BeRenderPipelineStep> BeRenderPipelineStepPtr;

#endif // #ifndef BE_BERENDERPIPELINESTEP_H
