/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeMaterialVertexAttibute.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <Render/BeMaterialVertexAttribute.h>

static std::map<BeString8, BeMaterialVertexAttribute::eBeMaterialAttributeType> s_kAttributeTypesByName = std::map<BeString8, BeMaterialVertexAttribute::eBeMaterialAttributeType>();

/////////////////////////////////////////////////////////////////////////////
BeMaterialVertexAttribute::BeMaterialVertexAttribute(const BeString8& strAttribName, const BeString8& strAttribSemantic, const BeInt iAttribSemanticIndex, const BeString8& strAttibTypeName, const BeBoolean bIsNormalised)
:
    m_strName(strAttribName),
    m_strSemantic(strAttribSemantic)
{
    m_eType = GetAttributeTypeByName(strAttibTypeName);

    int iConstantSize = 0;

    switch (m_eType)
    {
        case BeMaterialVertexAttribute::ATTRIBUTE_FLOAT:
        {
            iConstantSize = 1;

            break;
        }
        case BeMaterialVertexAttribute::ATTRIBUTE_FLOAT2:
        {
            iConstantSize = 2;

            break;
        }
        case BeMaterialVertexAttribute::ATTRIBUTE_FLOAT3:
        {
            iConstantSize = 3;

            break;
        }
        case BeMaterialVertexAttribute::ATTRIBUTE_FLOAT4:
        {
            iConstantSize = 4;

            break;
        }
        case BeMaterialVertexAttribute::ATTRIBUTE_MATRIX:
        {
            iConstantSize = 4 * 4;

            break;
        }
    }

    m_eTypeSize = iConstantSize * sizeof(BeFloat);
    m_strSemanticIndex = iAttribSemanticIndex;
    m_bNormalised = bIsNormalised;
}
/////////////////////////////////////////////////////////////////////////////
BeMaterialVertexAttribute::~BeMaterialVertexAttribute()
{
}
/////////////////////////////////////////////////////////////////////////////
void BeMaterialVertexAttribute::InitStaticResources(void)
{
    s_kAttributeTypesByName.clear();

    s_kAttributeTypesByName["float"] = BeMaterialVertexAttribute::ATTRIBUTE_FLOAT;
    s_kAttributeTypesByName["float2"] = BeMaterialVertexAttribute::ATTRIBUTE_FLOAT2;
    s_kAttributeTypesByName["float3"] = BeMaterialVertexAttribute::ATTRIBUTE_FLOAT3;
    s_kAttributeTypesByName["float4"] = BeMaterialVertexAttribute::ATTRIBUTE_FLOAT4;
    s_kAttributeTypesByName["matrix"] = BeMaterialVertexAttribute::ATTRIBUTE_MATRIX;
}
/////////////////////////////////////////////////////////////////////////////
void BeMaterialVertexAttribute::DestroyStaticResources(void)
{
    s_kAttributeTypesByName.clear();
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeSize_T BeMaterialVertexAttribute::GetTypeSize(void)
{
    return m_eTypeSize;
}
/////////////////////////////////////////////////////////////////////////////
BeInt BeMaterialVertexAttribute::GetComponentCount(void)
{
    switch (m_eType)
    {
        case BeMaterialVertexAttribute::ATTRIBUTE_FLOAT:
        {
            return 1;
        }
        case BeMaterialVertexAttribute::ATTRIBUTE_FLOAT2:
        {
            return 2;
        }
        case BeMaterialVertexAttribute::ATTRIBUTE_FLOAT3:
        {
            return 3;
        }
        case BeMaterialVertexAttribute::ATTRIBUTE_FLOAT4:
        {
            return 4;
        }
        case BeMaterialVertexAttribute::ATTRIBUTE_MATRIX:
        {
            return 16;
        }
    }

    return 0;
}
/////////////////////////////////////////////////////////////////////////////
BeMaterialVertexAttribute::eBeMaterialAttributeType BeMaterialVertexAttribute::GetAttributeTypeByName(const BeString8& strAttributeTypeName)
{
    return s_kAttributeTypesByName[strAttributeTypeName];
}
/////////////////////////////////////////////////////////////////////////////


