/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeMaterialConstant.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEMATERIALCONSTANT_H
#define BE_BEMATERIALCONSTANT_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>
#include <Managers/BeMaterialsManager.h>

/////////////////////////////////////////////////
/// \class BeMaterialConstant
/// \brief High-level material constant object
/////////////////////////////////////////////////
class RENDER_API BeMaterialConstant : public BeMemoryObject
{

public:

    /// \brief Material constant types
    enum eBeMaterialConstantType
    {
        CONSTANT_FLOAT = 0,
        CONSTANT_FLOAT2,
        CONSTANT_FLOAT3,
        CONSTANT_FLOAT4,
        CONSTANT_MATRIX
    };

    static int CONSTANT_ALIGNMENT;

    // Methods
    // {

        /// \brief  Constructor
        BeMaterialConstant(const BeString8& strConstantName, const BeString8& strConstantTypeName);

        /// \brief  Destructor
        ~BeMaterialConstant();

        /// \brief  Init the static engine resources
        static void InitStaticResources(void);

        /// \brief  Destroy the static engine resources
        static void DestroyStaticResources(void);

    // }



    // Getters
    // {

        /// \brief  Returns the constant type size
        BeSize_T GetTypeSize(void);

        /// \brief  Returns the constant type size in aligned memory
        BeSize_T GetTypeSizeInMemory(void);

    // }

public:

    /// \brief  Constant name
    BeString8 m_strName;

    /// \brief  Constant type
    eBeMaterialConstantType m_eType;

    /// \brief  Constant type size
    BeSize_T m_eTypeSize;

    /// \brief  Constant type size in aligned memory
    BeSize_T m_eTypeSizeMem;

private:

    /// \brief  Convert Constant type name to eBeMaterialConstantType
    eBeMaterialConstantType GetConstantTypeByName (const BeString8& strConstantTypeName);

    /// \brief  Constant value
    BeVoidP m_pValue;

};

#endif // #ifndef BE_BEMATERIALCONSTANT_H
