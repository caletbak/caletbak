/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeRenderDeviceEnumerator.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BERENDERDEVICEENUMERATOR_H
#define BE_BERENDERDEVICEENUMERATOR_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>

#include <BeRenderTypes.h>

/////////////////////////////////////////////////
/// \class BeRenderDeviceEnumerator
/// \brief Rendering device enumerator interface
/////////////////////////////////////////////////
class RENDER_API BeRenderDeviceEnumerator : public BeMemoryObject
{
      
  public:
    
    /// \brief  Executes all the adapters, devices and display modes enumeration processes
    virtual BeBoolean ExecuteEnumerationProcess(void) = 0;
    
    /// \brief  Fills the display modes array with the enumeration results
    virtual void FillDisplayModesArray( BeUInt uAdapterOrdinal, BeRenderTypes::BeDeviceAdapter& kDeviceAdapter ) = 0;

    /// \brief  Find the best suitable display mode taking into account the input data
    virtual BeBoolean FindBestDisplayMode( BeUInt uAdapterOrdinal, const BeRenderTypes::BeRenderDeviceSettings& kRenderDeviceSettings, BeRenderTypes::BeRenderDeviceDisplayMode* pDisplayMode ) = 0;
                
};

#endif // #ifndef BE_BERENDERDEVICEENUMERATOR_H
