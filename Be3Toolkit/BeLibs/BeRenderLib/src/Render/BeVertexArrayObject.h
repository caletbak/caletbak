/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeVertexArrayObject.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEVERTEXARRAYOBJECT_H
#define BE_BEVERTEXARRAYOBJECT_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>
#include <Render/BeVertexArrayObject.h>

// Forward declarations
class BeMaterialVertexDefinition;

/////////////////////////////////////////////////
/// \class BeVertexArrayObject
/// \brief High-level VAO object
/////////////////////////////////////////////////
class RENDER_API BeVertexArrayObject : public BeMemoryObject
{

public:

    /// \brief Constructor
    BeVertexArrayObject();
    /// \brief Destructor
    virtual ~BeVertexArrayObject();

    // Getters
    // {

        /// \brief  Returns the direct pointer to the vertex array object API impl
        virtual BeVoidP GetVertexArrayObjectPtr(void) = 0;

        /// \brief  Returns if the attributes for this VAO have been set
        BeBoolean HasAttributesSet(void);

    // }



    // Setters
    // {

        /// \brief  Sets the direct pointer to the vertex array object API impl
        virtual void SetVertexArrayObjectPtr(BeVoidP pVAOPtr) = 0;

    // }



    // Methods
    // {

        /// \brief  Prepares the attributes for this VAO object
        virtual void PrepareVertexArrayObjectAttributes(BeMaterialVertexDefinition* pVertexDefinition) = 0;

        /// \brief  Enable/Disable all the attributes for this VAO object
        virtual void EnableVertexArrayObjectAttributes(BeMaterialVertexDefinition* pVertexDefinition, BeBoolean bEnable) = 0;

    // }

protected:

    /// \brief  The attributes for this VAO have been set
    BeBoolean m_bAttributesSet;

};

#endif // #ifndef BE_BEVERTEXARRAYOBJECT_H
