/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeMaterialConstant.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <Render/BeMaterialConstant.h>

static std::map<BeString8, BeMaterialConstant::eBeMaterialConstantType> s_kConstantTypesByName = std::map<BeString8, BeMaterialConstant::eBeMaterialConstantType>();

/////////////////////////////////////////////////////////////////////////////
int BeMaterialConstant::CONSTANT_ALIGNMENT = 0;
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeMaterialConstant::BeMaterialConstant(const BeString8& strConstantName, const BeString8& strConstantTypeName)
:
    m_strName(strConstantName),
    m_pValue(NULL)
{
    m_eType = GetConstantTypeByName (strConstantTypeName);

    int iConstantSize = 0;
    int iConstantSizeMem = 0;

    switch (m_eType)
    {
        case BeMaterialConstant::CONSTANT_FLOAT:
        {
            iConstantSize = 1;
            iConstantSizeMem = iConstantSize;
            if (CONSTANT_ALIGNMENT == 16)
                iConstantSize = 4;

            break;
        }
        case BeMaterialConstant::CONSTANT_FLOAT2:
        {
            iConstantSize = 2;
            iConstantSizeMem = iConstantSize;
            if (CONSTANT_ALIGNMENT == 16)
                iConstantSizeMem = 4;

            break;
        }
        case BeMaterialConstant::CONSTANT_FLOAT3:
        {
            iConstantSize = 3;
            iConstantSizeMem = iConstantSize;
            if (CONSTANT_ALIGNMENT == 16)
                iConstantSizeMem = 4;

            break;
        }
        case BeMaterialConstant::CONSTANT_FLOAT4:
        {
            iConstantSize = 4;
            iConstantSizeMem = iConstantSize;
            if (CONSTANT_ALIGNMENT == 16)
                iConstantSizeMem = 4;

            break;
        }
        case BeMaterialConstant::CONSTANT_MATRIX:
        {
            iConstantSize = 4 * 4;
            iConstantSizeMem = iConstantSize;
            if (CONSTANT_ALIGNMENT == 16)
                iConstantSizeMem = 4 * 4;

            break;
        }
    }

    m_eTypeSize = iConstantSize * sizeof(BeFloat);
    m_eTypeSizeMem = iConstantSizeMem * sizeof(BeFloat);

    m_pValue = BeNew BeFloat[iConstantSizeMem];
    memset(m_pValue, 0, m_eTypeSizeMem);
}
/////////////////////////////////////////////////////////////////////////////
BeMaterialConstant::~BeMaterialConstant()
{
    if (m_pValue != NULL)
    {
        BeFloat* pValue = reinterpret_cast<BeFloat*>(m_pValue);

        BeDeleteArray pValue;

        m_pValue = NULL;
    }
}
/////////////////////////////////////////////////////////////////////////////
void BeMaterialConstant::InitStaticResources(void)
{
    s_kConstantTypesByName.clear();

    s_kConstantTypesByName["float"] = BeMaterialConstant::CONSTANT_FLOAT;
    s_kConstantTypesByName["float2"] = BeMaterialConstant::CONSTANT_FLOAT2;
    s_kConstantTypesByName["float3"] = BeMaterialConstant::CONSTANT_FLOAT3;
    s_kConstantTypesByName["float4"] = BeMaterialConstant::CONSTANT_FLOAT4;
    s_kConstantTypesByName["matrix"] = BeMaterialConstant::CONSTANT_MATRIX;
}
/////////////////////////////////////////////////////////////////////////////
void BeMaterialConstant::DestroyStaticResources(void)
{
    s_kConstantTypesByName.clear();
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeSize_T BeMaterialConstant::GetTypeSize(void)
{
    return m_eTypeSize;
}
/////////////////////////////////////////////////////////////////////////////
BeSize_T BeMaterialConstant::GetTypeSizeInMemory(void)
{
    return m_eTypeSizeMem;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeMaterialConstant::eBeMaterialConstantType BeMaterialConstant::GetConstantTypeByName (const BeString8& strConstantTypeName)
{
    return s_kConstantTypesByName[strConstantTypeName];
}
/////////////////////////////////////////////////////////////////////////////

