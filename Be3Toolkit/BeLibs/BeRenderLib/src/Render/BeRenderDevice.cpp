/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeRenderDevice.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <Render/BeRenderDevice.h>
#include <Managers/BeTextureManager.h>
#include <Managers/BeMaterialsManager.h>
#include <Render/2D/BeSprite2D.h>
#include <Render/BeVertexBuffer.h>
#include <Render/BeIndexBuffer.h>
#include <Render/BeRenderStates.h>
#include <Render/BeTexture.h>

#if TARGET_WINDOWS
// Win32 libs
#pragma comment (lib, "Shell32.lib")
#include <Shellapi.h>
#endif



/////////////////////////////////////////////////////////////////////////////
BeRenderTypes::ERenderDeviceAPI BeRenderDevice::m_eRenderAPIBeingUsed = BeRenderTypes::RENDER_DEVICE_UNKNOWN;
BeWindowHandle BeRenderDevice::m_hWindow = NULL;
/////////////////////////////////////////////////////////////////////////////
BeRenderDevice::BeRenderDevice()
:
  m_pTextureManager(NULL),
  m_pMaterialsManager(NULL),
  m_pCurrentVertexArrayObjectSet(NULL),
  m_pCurrentVertexBufferSet(NULL),
  m_pCurrentIndexBufferSet(NULL),
  m_pCurrentGSConstantBufferSet(NULL),
  m_pCurrentVSConstantBufferSet(NULL),
  m_pCurrentPSConstantBufferSet(NULL),
  m_pCurrentGSSamplerSet(NULL),
  m_pCurrentVSSamplerSet(NULL),
  m_pCurrentPSSamplerSet(NULL),
  m_pRenderStates(NULL)
{
  m_hWindow = NULL;

  m_mSceneViewProjectionMatrix.Identity();
  m_mCurrentWorldViewProjectionMatrix.Identity();
}
/////////////////////////////////////////////////////////////////////////////
BeRenderDevice::~BeRenderDevice()
{
  m_hWindow = NULL;
  m_pCurrentVertexArrayObjectSet = NULL;
  m_pCurrentVertexBufferSet = NULL;
  m_pCurrentIndexBufferSet = NULL;
  m_pCurrentGSConstantBufferSet = NULL;
  m_pCurrentVSConstantBufferSet = NULL;
  m_pCurrentPSConstantBufferSet = NULL;
  m_pCurrentGSSamplerSet = NULL;
  m_pCurrentVSSamplerSet = NULL;
  m_pCurrentPSSamplerSet = NULL;

  Release();
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderDevice::Release()
{
  if ( m_pTextureManager != NULL )
  {
    BeDelete m_pTextureManager;
    m_pTextureManager = NULL;
  }

  if (m_pMaterialsManager != NULL)
  {
      BeDelete m_pMaterialsManager;
      m_pMaterialsManager = NULL;
  }

  if (m_pRenderStates != NULL)
  {
      BeDelete m_pRenderStates;
      m_pRenderStates = NULL;
  }
}
/////////////////////////////////////////////////////////////////////////////


#if  TARGET_WINDOWS
/////////////////////////////////////////////////////////////////////////////
LRESULT CALLBACK BeRenderDevice::WndProc (BeWindowHandle hWnd, BeUInt uMessage, WPARAM wParam, LPARAM lParam)
{
  switch (uMessage)
  {
    case WM_MOUSEMOVE:
    case WM_LBUTTONDOWN:
    case WM_LBUTTONUP:
    case WM_LBUTTONDBLCLK:
    case WM_RBUTTONDOWN:
    case WM_RBUTTONUP:
    case WM_RBUTTONDBLCLK:
    case WM_MBUTTONDOWN:
    case WM_MBUTTONUP:
    case WM_MBUTTONDBLCLK:
    {
      int coordX = LOWORD (lParam);
      int coordY = HIWORD (lParam);

      BeInputMouse::MouseEventHandle (uMessage, coordX, coordY);

      break;
    }

    case WM_DESTROY:
    {
      PostQuitMessage(0);
      break;
    }

    case WM_KEYDOWN:
    {
      switch( wParam )
      {
        case VK_ESCAPE:
        {
          SendMessage( hWnd, WM_CLOSE, 0, 0 );
          break;
        }
      }
      break;
    }

    case WM_CLOSE:
    {
      DestroyAppWindow();  
      return 0;
    }
  }

  return DefWindowProc(hWnd, uMessage, wParam, lParam);
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeRenderDevice::CreateAppWindow(BeString16 strWindowTitle, BeInt iWidth, BeInt iHeight, BeChar8 iBits, BeBoolean bFullScreen, WNDPROC pfWndProc)
{
  BeInstance hInstance;         // Holds The Instance Of The Application
  WNDCLASS   wc;                // Windows Class Structure
  BeULong    dwExStyle;         // Window Extended Style
  BeULong    dwStyle;           // Window Style
  
  RECT    WindowRect;                   // Grabs Rectangle Upper Left / Lower Right Values
  WindowRect.left = (BeLong)0;          // Set Left Value To 0
  WindowRect.right = (BeLong)iWidth;    // Set Right Value To Requested Width
  WindowRect.top = (BeLong)0;           // Set Top Value To 0
  WindowRect.bottom = (BeLong)iHeight;  // Set Bottom Value To Requested Height

  hInstance      = GetModuleHandle(NULL);        // Grab An Instance For Our Window

  wc.style      = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;  // Redraw On Size, And Own DC For Window.
  wc.lpfnWndProc    = pfWndProc;         // WndProc Handles Messages
  wc.cbClsExtra    = 0;                  // No Extra Window Data
  wc.cbWndExtra    = 0;                  // No Extra Window Data
  wc.hInstance    = hInstance;           // Set The Instance
  
  WCHAR szExePath[MAX_PATH];
  GetModuleFileName( NULL, szExePath, MAX_PATH );
  wc.hIcon = ExtractIcon( hInstance, szExePath, 0 );

  wc.hCursor      = LoadCursor(NULL, IDC_ARROW);      // Load The Arrow Pointer
  wc.hbrBackground  = NULL;                  // No Background Required For GL
  wc.lpszMenuName    = NULL;                  // We Don't Want A Menu
  wc.lpszClassName  = L"AppWindow";              // Set The Class Name

  if ( !RegisterClass(&wc) )                  // Attempt To Register The Window Class
  {
    BE_RENDER_ERROR("Failed To Register The Window Class.");
    return BE_FALSE;                    // Return FALSE
  }

  if ( bFullScreen )                      // Attempt Fullscreen Mode?
  {
    DEVMODE dmScreenSettings;                // Device Mode
    memset(&dmScreenSettings,0,sizeof(dmScreenSettings));  // Makes Sure Memory's Cleared
    dmScreenSettings.dmSize=sizeof(dmScreenSettings);    // Size Of The Devmode Structure
    dmScreenSettings.dmPelsWidth  = iWidth;        // Selected Screen Width
    dmScreenSettings.dmPelsHeight  = iHeight;        // Selected Screen Height
    dmScreenSettings.dmBitsPerPel  = iBits;        // Selected Bits Per Pixel
    dmScreenSettings.dmFields=DM_BITSPERPEL|DM_PELSWIDTH|DM_PELSHEIGHT;

    // Try To Set Selected Mode And Get Results.  NOTE: CDS_FULLSCREEN Gets Rid Of Start Bar.
    if (ChangeDisplaySettings(&dmScreenSettings,CDS_FULLSCREEN)!=DISP_CHANGE_SUCCESSFUL)
    {
    BE_RENDER_WARNING("FULL SCREEN NOT AVAILABLE !!.");
      bFullScreen = BE_FALSE;    // Windowed Mode Selected.  Fullscreen = FALSE
    }
  }

  if (bFullScreen)                      // Are We Still In Fullscreen Mode?
  {
    dwExStyle=WS_EX_APPWINDOW;                // Window Extended Style
    dwStyle=WS_POPUP | WS_CLIPSIBLINGS | WS_CLIPCHILDREN;  // Windows Style
    ShowCursor(FALSE);                    // Hide Mouse Pointer
  }
  else
  {
    dwExStyle=WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;            // Window Extended Style
    dwStyle = (WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX);  // Windows Style
  }

  AdjustWindowRectEx( &WindowRect, dwStyle, FALSE, dwExStyle );    // Adjust Window To True Requested Size

  BeInt iCenterX = (GetSystemMetrics(SM_CXSCREEN) / 2) - (iWidth / 2);
  BeInt iCenterY = (GetSystemMetrics(SM_CYSCREEN) / 2) - (iHeight / 2);

    // Create The Window
  m_hWindow = CreateWindowEx(dwExStyle,              // Extended Style For The Window
                  L"AppWindow",          // Class Name
                  strWindowTitle.ToRaw(),        // Window Title
                  dwStyle |              // Defined Window Style
                  WS_CLIPSIBLINGS |          // Required Window Style
                  WS_CLIPCHILDREN,          // Required Window Style
                  iCenterX, iCenterY,          // Window Position
                  WindowRect.right-WindowRect.left,  // Calculate Window Width
                  WindowRect.bottom-WindowRect.top,  // Calculate Window Height
                  NULL,                // No Parent Window
                  NULL,                // No Menu
                  hInstance,              // Instance
                  NULL);

  if ( !m_hWindow )      // Dont Pass Anything To WM_CREATE
  {
    BE_RENDER_ERROR("Window Creation Error.");
    return BE_FALSE;              // Return FALSE
  }

  return BE_TRUE;
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderDevice::DestroyAppWindow(void)
{
  if ( m_hWindow != NULL )
  {
    HMENU hMenu = GetMenu( m_hWindow );
    
    if( hMenu != NULL )
      DestroyMenu( hMenu );

    DestroyWindow( m_hWindow );
    m_hWindow = NULL;
    UnregisterClass( L"AppWindow", NULL );
  }
}

#endif

/////////////////////////////////////////////////////////////////////////////
void BeRenderDevice::CreateTextureManager()
{
    m_pTextureManager = BeNew BeTextureManager(this);
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderDevice::CreateMaterialsManager()
{
    m_pMaterialsManager = BeNew BeMaterialsManager(this, L"assets\\Materials\\");
}
/////////////////////////////////////////////////////////////////////////////
BeTextureManager* BeRenderDevice::GetTextureManager (void)
{
  return m_pTextureManager;
}
/////////////////////////////////////////////////////////////////////////////
BeMaterialsManager* BeRenderDevice::GetMaterialsManager(void)
{
    return m_pMaterialsManager;
}
/////////////////////////////////////////////////////////////////////////////
BeRenderStates* BeRenderDevice::GetRenderStates(void)
{
    return m_pRenderStates;
}
/////////////////////////////////////////////////////////////////////////////
const BeMatrix& BeRenderDevice::GetSceneViewProjectionMatrix(void)
{
    return m_mSceneViewProjectionMatrix;
}
/////////////////////////////////////////////////////////////////////////////
const BeMatrix& BeRenderDevice::GetCurrentWorldViewProjectionMatrix(void)
{
    return m_mCurrentWorldViewProjectionMatrix;
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderDevice::Render2D(BeSprite2D* pSprite, BeMatrix& mWorldMatrix)
{
    if (m_pMaterialsManager->SetMaterialForRendering(L"MATERIAL_2D"))
    {
        m_mCurrentWorldViewProjectionMatrix = mWorldMatrix * m_mSceneViewProjectionMatrix;

        m_pMaterialsManager->RequestUpdateConstant("g_mWorldViewProjection", (BeVoidP)&m_mCurrentWorldViewProjectionMatrix);
        m_pMaterialsManager->RequestUpdateConstant("g_vColor", (BeVoidP)pSprite->GetColour());
        m_pMaterialsManager->UpdateMaterial();

        m_pMaterialsManager->SetConstantBuffers();

        BeRenderTypes::BeTextureCacheEntry* pTextureEntry = pSprite->GetSpriteTexture();
        BindTexture(pSprite->GetSpriteTextureID(), BeTexture::TEXTURE_CHANNEL_0);

        m_pMaterialsManager->SetTextureSamplers();

        BeMesh* pMesh = pSprite->GetCurrentTileMesh();

        RenderIndexedPrimitive(pMesh->GetVertexBuffer(), pMesh->GetIndexBuffer(), pMesh->GetVertexArrayObject());

        BindTexture(-1, BeTexture::TEXTURE_CHANNEL_0);
    }
}
/////////////////////////////////////////////////////////////////////////////
BeInt BeRenderDevice::LoadTextureFromFile(BeString16 strFileName)
{
  return m_pTextureManager->LoadTextureFromFile(strFileName);
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderDevice::FreeTexture(BeInt iTextureSlot)
{
  m_pTextureManager->FreeTexture(iTextureSlot);
}
/////////////////////////////////////////////////////////////////////////////
