/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeMaterialsManager.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEMATERIALSMANAGER_H
#define BE_BEMATERIALSMANAGER_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>
#include <Render/BeMaterial.h>

#include <BeRenderTypes.h>

// Forward delacations
class BeRenderDevice;
class BeTextureSampler;
class BeMaterialProgram;

/////////////////////////////////////////////////
/// \class BeMaterialsManager
/// \brief Manager that controls the created or loaded materials/shaders
/////////////////////////////////////////////////
class RENDER_API BeMaterialsManager : public BeCoreTypes::ResourcesInterface
{

    // Class Friendship
    friend class BeResourcesManager;

    public:

        /// \brief  Constructor
        BeMaterialsManager(BeRenderDevice* pRenderDevice, const BeString16& strMaterialsConfigBasePath);
        /// \brief  Destructor
        ~BeMaterialsManager();



    // Methods
    // {

      /// \brief  Release all the cached materials
      void ReleaseAllCachedMaterials();

      /// \brief  Prepares the materials cache
      void ResetMaterialsCache();

      /// \brief  Returns the material handle if the material name has been found in the cache
      BeInt FindMaterial(const BeString16& strMaterialName);

      /// \brief  Allocates a new material cached space and returns the handle
      BeInt AllocMaterial(const BeString16& strMaterialName);

      /// \brief  Frees a cached material
      void FreeMaterial(BeInt iCacheHandle);

      /// \brief  Sets the material for rendering
      BeMaterial* SetMaterialForRendering(const BeString16& strMaterialName);

      /// \brief  Make a request for updating a constant in the current set material
      void RequestUpdateConstant(const BeString8& strConstantName, BeVoidP pValue);

      /// \brief  Updates the material and constants all together
      void UpdateMaterial(void);

      /// \brief  Sets the current material constant buffers
      void SetConstantBuffers(void);

      /// \brief  Sets the current material texture samplers
      void SetTextureSamplers(void);

    // }



    // Getters
    // {

      /// \brief  Return a Material entry previously loaded
      BeRenderTypes::BeMaterialCacheEntry* GetMaterialCacheEntry(BeInt iMaterialCacheSlot);

      /// \brief  Return the ID of the current Material being used, -1 if any
      BeInt GetCurrentMaterialIDSetForRendering(void);

      /// \brief  Return the current Material being used, NULL if any
      BeMaterial* GetCurrentMaterialSetForRendering(void);

      /// \brief  Return a previously compiled material program by name
      BeMaterialProgram* GetMaterialProgramByName(const BeString8& strProgramName);

    // }

  private:

    /// \brief  The default Material cache size
    static BeInt DEFAULT_CACHE_SIZE;



    // Methods
    // {

      /// \brief  Loads a program file for compiling later
      void LoadMaterialProgram(const BeString16& strBasePath, const BeString16& strFileName, BeVoidP* pBufferData, BeInt& iBufferSize);

      /// \brief  Callback for processing loaded Material resources
      virtual void ProcessLoadedEvent(const BeString16& strFileName, BeVoidP pBufferData, BeInt iBufferSize, int eMaterialStep);

      /// \brief  Assigns a material to a cache Material slot
      void SetMaterialIntoCacheSlot(BeInt iCacheHandle, BeMaterial* pMaterial);

    // }



    /// \brief  The Material cache
    typedef std::vector<BeRenderTypes::BeMaterialCacheEntryPtr> BeMaterialCacheArray;
    BeMaterialCacheArray m_pMaterialsCacheArray;

    /// \brief  Texture samplers
    std::map<BeString16, BeTextureSampler*> m_kTextureSamplers;

    /// \brief  Constant configs
    std::map<BeString8, BeRenderTypes::BeConstantConfig*> m_kConstantConfigs;

    /// \brief  Attribute configs
    std::map<BeString8, BeRenderTypes::BeAttributeConfig*> m_kAttributeConfigs;

    /// \brief  Texture channels configs
    std::map<BeString8, BeRenderTypes::BeTextureChannelConfig*> m_kTextureChannelConfigs;

    /// \brief  Material programs
    std::map<BeString8, BeMaterialProgram*> m_kMaterialPrograms;

    /// \brief  The Render device pointer
    BeRenderDevice* m_pRenderDevice;

    /// \brief  The current material set for rendering
    BeInt m_iCurrentMaterialSetForRendering;

};



#endif // #ifndef BE_BEMATERIALSMANAGER_H

