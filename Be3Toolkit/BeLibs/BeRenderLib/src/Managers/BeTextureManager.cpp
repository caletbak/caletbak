/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeTextureDX9.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <Managers/BeTextureManager.h>
#include <Render/BeRenderDevice.h>
#include <Render/BeTextureSampler.h>

/////////////////////////////////////////////////////////////////////////////
BeInt BeTextureManager::DEFAULT_CACHE_SIZE = 2048;
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeTextureManager::BeTextureManager(BeRenderDevice* pRenderDevice)
:
  m_pRenderDevice(pRenderDevice)
{
  for ( BeInt i = 0; i < BeRenderTypes::MAT_TEXTURE_MAPS_COUNT; ++i )
  {
    m_iLastTextureMapsIdx[i] = -1;
  }

  ResetTextureCache();
}
/////////////////////////////////////////////////////////////////////////////
BeTextureManager::~BeTextureManager()
{
  ReleaseAllCachedTextures();

  m_pRenderDevice = NULL;
}
/////////////////////////////////////////////////////////////////////////////
void BeTextureManager::ReleaseAllCachedTextures()
{
  m_pTextureCacheArray.clear();
}
/////////////////////////////////////////////////////////////////////////////
void BeTextureManager::ResetTextureCache()
{
  ReleaseAllCachedTextures();

  m_pTextureCacheArray.resize(DEFAULT_CACHE_SIZE);
  for ( BeInt i = 0; i < DEFAULT_CACHE_SIZE; ++i )
  {
    m_pTextureCacheArray[i] = BeNew BeRenderTypes::BeTextureCacheEntry();
  }

  LoadTextureFromFile(L"assets\\Textures\\whiteTexture.png");
  LoadTextureFromFile(L"assets\\Textures\\blackTexture.png");
  LoadTextureFromFile(L"assets\\Textures\\pinkTexture.png");
}
/////////////////////////////////////////////////////////////////////////////
BeInt BeTextureManager::LoadTextureFromFile(const BeString16& strFileName, BeBoolean bAsyncLoad)
{
  BeInt iTextureSlot = FindTexture(strFileName);
  if ( iTextureSlot == -1 )
  {
    iTextureSlot = AllocTexture( strFileName );
    if ( iTextureSlot != -1 )
    {
      if ( !bAsyncLoad )
      {
        BeVoidP pBufferData = NULL;
        BeInt iBufferSize = 0;

        pBufferData = BeFileTool::ReadEntireFile( strFileName, &iBufferSize );
        if ( pBufferData != NULL )
        {
          CreateDeviceTextureFromBuffer(strFileName, pBufferData, iBufferSize);
          
          BeFreeMemory(pBufferData);
          pBufferData = NULL;
        }
        else
        {
          BE_RENDER_ERROR( "Error loading RAW buffer from file: %s !!", ConvertString16To8(strFileName).ToRaw() );
        }
      }
      else
      {
        //BeResourcesManager* pResourcesManager = BeEngineContext::GetInstance()->GetResourcesManager();
        //pResourcesManager->AddLoadEvent( BeCoreTypes::LOAD_RESOURCE_TEXTURE_DATA, strFileName, this );
      }
    }
  }

  return iTextureSlot;
}
/////////////////////////////////////////////////////////////////////////////
void BeTextureManager::CreateDeviceTextureFromBuffer(const BeString16& strFileName, BeVoidP pBufferData, BeInt iBufferSize)
{
  BeInt iTextureSlot = FindTexture(strFileName);
  if ( iTextureSlot != -1 )
  {
    BeRenderTypes::BeTextureCacheEntry* pCachedTexture = m_pTextureCacheArray[iTextureSlot].GetPointer();
    if ( !m_pRenderDevice->CreateTextureFromBuffer(pCachedTexture, pBufferData, iBufferSize) )
    {
      BE_RENDER_ERROR( "The Texture %s could not be created !!.", ConvertString16To8(strFileName).ToRaw() );
    }
  }
}
/////////////////////////////////////////////////////////////////////////////
void BeTextureManager::ProcessLoadedEvent(const BeString16& strFileName, BeVoidP pBufferData, BeInt iBufferSize, int iExtraParam1)
{
    BE_UNUSED(iExtraParam1);

    CreateDeviceTextureFromBuffer(strFileName, pBufferData, iBufferSize);
}
/////////////////////////////////////////////////////////////////////////////
BeInt BeTextureManager::FindTexture(const BeString16& strTextureName)
{
  for ( BeUInt i = 0 ; i < m_pTextureCacheArray.size(); ++i )
  {
    BeRenderTypes::BeTextureCacheEntry* pCachedTexture = m_pTextureCacheArray[i].GetPointer();
    if ( pCachedTexture->m_strTextureName.Compare(strTextureName) == 0 )
      return i;
  }

  return -1;
}
/////////////////////////////////////////////////////////////////////////////
BeInt BeTextureManager::AllocTexture(const BeString16& strTextureName)
{
  BeInt iFreeCacheSlot = -1;

  for ( BeUInt i = 0 ; i < m_pTextureCacheArray.size(); ++i )
  {
    BeRenderTypes::BeTextureCacheEntry* pCachedTexture = m_pTextureCacheArray[i].GetPointer();
    if ( !pCachedTexture->m_bIsAllocated && pCachedTexture->m_pTexture == NULL )
    {
      iFreeCacheSlot = i;
      break;
    }
  }

  if ( iFreeCacheSlot == -1 )
  {
    if ( m_pTextureCacheArray.size() >= (BeUInt)DEFAULT_CACHE_SIZE )
    {
      BE_RENDER_DEBUG("CAUTION : texture cache full !!!");
      return -1;
    }

    iFreeCacheSlot = m_pTextureCacheArray.size();
  }

  FreeTexture(iFreeCacheSlot);

  BeRenderTypes::BeTextureCacheEntry* pCachedTexture = m_pTextureCacheArray[iFreeCacheSlot].GetPointer();
  pCachedTexture->m_strTextureName = strTextureName;
  pCachedTexture->m_bIsAllocated = BE_TRUE;

  return iFreeCacheSlot;
}
/////////////////////////////////////////////////////////////////////////////
void BeTextureManager::FreeTexture(BeInt iCacheHandle)
{
  if ( iCacheHandle < 0 || (BeUInt)iCacheHandle >= m_pTextureCacheArray.size() )
    return;

  m_pTextureCacheArray[iCacheHandle]->Release();
}
/////////////////////////////////////////////////////////////////////////////
void BeTextureManager::SetTextureIntoCacheSlot(BeInt iCacheHandle, BeTexture* pTexture)
{
  if ( iCacheHandle < 0 || (BeUInt)iCacheHandle >= m_pTextureCacheArray.size() )
    return;

  BeRenderTypes::BeTextureCacheEntry* pCachedTexture = m_pTextureCacheArray[iCacheHandle].GetPointer();
  pCachedTexture->m_pTexture = pTexture;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeRenderTypes::BeTextureCacheEntry* BeTextureManager::GetTextureCacheEntry(BeInt iTextureCacheSlot)
{
  if ( iTextureCacheSlot < 0 || (BeUInt)iTextureCacheSlot >= m_pTextureCacheArray.size() )
    return NULL;

  BeRenderTypes::BeTextureCacheEntry* pCachedTexture = m_pTextureCacheArray[iTextureCacheSlot].GetPointer();
  return pCachedTexture;
}
/////////////////////////////////////////////////////////////////////////////



/*
TextureCacheEntry* CTextureManager::getTextureCacheEntry( int slot ){

    if (slot < 1 || slot >= nextTextureSlot) {
        return new TextureCacheEntry();
    }
  
    return &textureCacheList[slot];

}

void CTextureManager::setTextureIsLoaded(int slot){
    textureCacheList[slot].isLoaded = true;
}

bool CTextureManager::getIsTextureLoaded(int slot) {
  if (slot < 1 || slot >= nextTextureSlot) return false;
    return textureCacheList[slot].isLoaded;
}


int CTextureManager::cacheTexture(char* filename, TextureReference* texture)
{
                        
    int  slot = findTexture(filename);
    if (slot > 0) {
        return slot;
    }

    slot = allocTexture(filename);
    textureCacheList[slot].allocated = true;
    textureCacheList[slot].isLoaded = false;
  textureCacheList[slot].numModelsUsingThis = 1;

    string newPath = getFilenameWithoutExt(filename);
  
    texture->handle = slot;
    texture->isCubeTexture = false;
    //XNA_Project_01.Game1.resourcesThread.Load2DTexture(newPath, texture);

    return slot;

}
void CTextureManager::cacheTexture(TextureReference* texture)
{

    if ((texture->handle > 0) && (texture->handle < nextTextureSlot))
    {

        TextureCacheEntry t = textureCacheList[texture->handle];

    if ( compareCharStrings(t.name, texture->name) == 0 && (t.texture != NULL || t.textureCube != NULL))
        {
      textureCacheList[texture->handle].numModelsUsingThis++;
            return;
        }
    }

    texture->handle = cacheTexture(texture->name, texture);

}

void CTextureManager::unCacheTexture(TextureReference* texture){

  if ((texture->handle > 0) && (texture->handle < nextTextureSlot))
    {

        TextureCacheEntry t = textureCacheList[texture->handle];

    //if ( compareCharStrings(t.name, texture->name) == 0 && (t.texture != NULL || t.textureCube != NULL) )
    if ( compareCharStrings(t.name, texture->name) == 0 )
        {
            
      textureCacheList[texture->handle].numModelsUsingThis--;

      #ifdef DEBUG_MSG
        debug << "textureCacheList[texture->handle].numModelsUsingThis : " << textureCacheList[texture->handle].numModelsUsingThis << " : " << t.name << "\n";
      #endif

      if ( textureCacheList[texture->handle].numModelsUsingThis <= 0 ){
        textureCacheList[texture->handle].release();
      }

        }
    }

}

bool CTextureManager::cacheTextureGroup(char* filename, TextureReference* texture)
{

    int slot = findTexture(filename);
    if (slot > 0)
    {
    texture->handle = slot;
    texture->isCubeTexture = false;
    textureCacheList[slot].numModelsUsingThis++;
        return false;
    }

    slot = allocTexture(filename);
    textureCacheList[slot].allocated = true;
    textureCacheList[slot].isLoaded = false;
  textureCacheList[slot].numModelsUsingThis = 1;

    string newPath = getFilenameWithoutExt(filename);

    texture->handle = slot;
    texture->isCubeTexture = false;
  //hw->resourcesThread->add2DTextureToGroup(texture);

    return true;

}
bool CTextureManager::cacheCubeTextureGroup(char* filename, TextureReference* texture)
{

    int slot = findTexture(filename);
    if (slot > 0)
    {
    texture->handle = slot;
    texture->isCubeTexture = true;
    textureCacheList[slot].numModelsUsingThis++;
        return false;
    }
  
    slot = allocTexture(filename);
    textureCacheList[slot].allocated = true;
    textureCacheList[slot].isLoaded = false;
  textureCacheList[slot].numModelsUsingThis = 1;
    
    string newPath = getFilenameWithoutExt(filename);
  //debug << "cacheCubeTextureGroup : " << filename.c_str() << "\n";

    texture->handle = slot;
    texture->isCubeTexture = true;
    //hw->resourcesThread->add2DTextureToGroup(texture);

    return true;

}
                        


void CTextureManager::cacheTextureGroup(TextureReference* texture)
{
                
  //if ((texture->handle > 0) && (texture->handle < nextTextureSlot)) {

    //    TextureCacheEntry t = textureCacheList[texture->handle];

  //  if ( compareCharStrings(t.name, texture->name) == 0 && t.texture != NULL ) {
  //    textureCacheList[texture->handle].numModelsUsingThis++;
  //        return;
    //    }
    //}
  

  
  //int slot = findTexture(texture->name);
    //if (slot > 0)
    //{
  //  texture->handle = slot;
  //  textureCacheList[slot].numModelsUsingThis++;
  //    return;
  //}
  
    //texture->handle = cacheTextureGroup(texture->name, texture);
  cacheTextureGroup(texture->name, texture);
              
}
void CTextureManager::cacheTextureGroup(TextureReference* texture, int format)
{
     
  //if ((texture->handle > 0) && (texture->handle < nextTextureSlot)) {

    //    TextureCacheEntry t = textureCacheList[texture->handle];
    
  //  if ( compareCharStrings(t.name, texture->name) == 0 && t.texture != NULL ) {
  //    textureCacheList[texture->handle].numModelsUsingThis++;
  //        return;
    //    }
    //}
  
  //int slot = findTexture(texture->name);
    //if (slot > 0)
    //{
  //  texture->handle = slot;
  //  textureCacheList[slot].numModelsUsingThis++;
  //    return;
  //}
  
    //texture->handle = cacheTextureGroup(texture->name, texture);
  cacheTextureGroup(texture->name, texture);
  texture->formatToLoad = format;
              
}
void CTextureManager::cacheCubeTextureGroup(TextureReference* texture)
{
             
  //if ((texture->handle > 0) && (texture->handle < nextTextureSlot)) {

    //    TextureCacheEntry t = textureCacheList[texture->handle];

    //    if ( compareCharStrings(t.name, texture->name) == 0 && t.textureCube != NULL ) {
  //    textureCacheList[texture->handle].numModelsUsingThis++;
  //        return;
    //    }
    //}
  
  //int slot = findTexture(texture->name);
    //if (slot > 0)
    //{
  //  texture->handle = slot;
  //  textureCacheList[slot].numModelsUsingThis++;
  //    return;
  //}
  
    //texture->handle = cacheCubeTextureGroup(texture->name, texture);
  cacheCubeTextureGroup(texture->name, texture);
              
}



void CTextureManager::setCubeTextureImage(int handle, LPDIRECT3DCUBETEXTURE9 image)
{

    if ((handle < 1) || (handle >= nextTextureSlot))
    {
        return;
    }

    TextureCacheEntry* t = &textureCacheList[handle];
    t->textureCube = image;
    t->isCubeTexture = true;

  D3DSURFACE_DESC* imageDesc;
  image->GetLevelDesc(0, imageDesc);

    t->xSize = imageDesc->Width;
  t->ySize = imageDesc->Height;

  //debug << "setCubeTextureImage " << t->name.c_str() << " : " << t->xSize << "," << t->ySize << "\n";

}





bool CTextureManager::selectTexture(int handle, int index) {
  
  HRESULT result;

  bool changedTexture = false;

  if (handle < 1 || handle >= nextTextureSlot) {
    return false;
  }

  //CShaderManager::shaderManager.hasToResetTextureIndexes = true;
  if ( CShaderManager::shaderManager.hasToResetTextureIndexes ){

    MAT_DIFFUSEMAP_LASTIDX = -1;
    MAT_SPECULARCOLORMAP_LASTIDX = -1;
    MAT_SPECULARLEVELMAP_LASTIDX = -1;
    MAT_GLOSSINESSMAP_LASTIDX = -1;
    MAT_SELFILLUMINATIONMAP_LASTIDX = -1;
    MAT_OPACITYMAP_LASTIDX = -1;
    MAT_FILTERCOLORMAP_LASTIDX = -1;
    MAT_BUMPMAP_LASTIDX = -1;
    MAT_REFLECTIONMAP_LASTIDX = -1;
    MAT_REFRACTIONMAP_LASTIDX = -1;
    MAT_DISPLACEMENTMAP_LASTIDX = -1;

    CShaderManager::shaderManager.hasToResetTextureIndexes = false;

  }
  
  if (handle != currentTextureHandle) {

    TextureCacheEntry *t = &textureCacheList[handle];

    currentTextureHandle = handle;

    if ( g_pd3dDevice != NULL && t->isLoaded ){

      switch( index ){
        case MAT_DIFFUSEMAP:{

          if (handle != MAT_DIFFUSEMAP_LASTIDX && CShaderManager::shaderManager.getEffect() != NULL)
          {
            D3DXHANDLE handle_param = CShaderManager::shaderManager.getEffect()->GetParameterByName( NULL, "MAT_DIFFUSEMAP" );
            if ( handle_param ){
              CShaderManager::shaderManager.getEffect()->SetTexture( handle_param, t->texture );
            }
            MAT_DIFFUSEMAP_LASTIDX = handle;

            changedTexture = true;
          }

          break;
        }
        case MAT_SELFILLUMINATIONMAP:{

          if (handle != MAT_SELFILLUMINATIONMAP_LASTIDX && CShaderManager::shaderManager.getEffect() != NULL)
          {
            D3DXHANDLE handle_param = CShaderManager::shaderManager.getEffect()->GetParameterByName( NULL, "MAT_SELFILLUMINATIONMAP" );
            if ( handle_param ){
              CShaderManager::shaderManager.getEffect()->SetTexture( handle_param, t->texture );
            }
            MAT_SELFILLUMINATIONMAP_LASTIDX = handle;

            changedTexture = true;
          }

          break;
        }
        case MAT_OPACITYMAP:{

          if (handle != MAT_OPACITYMAP_LASTIDX && CShaderManager::shaderManager.getEffect() != NULL)
          {
            D3DXHANDLE handle_param = CShaderManager::shaderManager.getEffect()->GetParameterByName( NULL, "MAT_OPACITYMAP" );
            if ( handle_param ){
              CShaderManager::shaderManager.getEffect()->SetTexture( handle_param, t->texture );
            }
            MAT_OPACITYMAP_LASTIDX = handle;

            changedTexture = true;
          }

          break;
        }
        case MAT_SPECULARCOLORMAP:{

          if (handle != MAT_SPECULARCOLORMAP_LASTIDX && CShaderManager::shaderManager.getEffect() != NULL)
          {
            D3DXHANDLE handle_param = CShaderManager::shaderManager.getEffect()->GetParameterByName( NULL, "MAT_SPECULARCOLORMAP" );
            if ( handle_param ){
              CShaderManager::shaderManager.getEffect()->SetTexture( handle_param, t->texture );
            }
            MAT_SPECULARCOLORMAP_LASTIDX = handle;

            changedTexture = true;
          }
          
          break;
        }
        case MAT_SPECULARLEVELMAP:{

          if (handle != MAT_SPECULARLEVELMAP_LASTIDX && CShaderManager::shaderManager.getEffect() != NULL)
          {
            D3DXHANDLE handle_param = CShaderManager::shaderManager.getEffect()->GetParameterByName( NULL, "MAT_SPECULARLEVELMAP" );
            if ( handle_param ){
              CShaderManager::shaderManager.getEffect()->SetTexture( handle_param, t->texture );
            }
            MAT_SPECULARLEVELMAP_LASTIDX = handle;

            changedTexture = true;
          }
          
          break;
        }
        case MAT_BUMPMAP:{

          if (handle != MAT_BUMPMAP_LASTIDX && CShaderManager::shaderManager.getEffect() != NULL)
          {
            D3DXHANDLE handle_param = CShaderManager::shaderManager.getEffect()->GetParameterByName( NULL, "MAT_BUMPMAP" );
            if ( handle_param ){
              CShaderManager::shaderManager.getEffect()->SetTexture( handle_param, t->texture );
            }
            MAT_BUMPMAP_LASTIDX = handle;

            changedTexture = true;
          }
          
          break;
        }
        case MAT_GLOSSINESSMAP:
        {

          if (handle != MAT_GLOSSINESSMAP_LASTIDX && CShaderManager::shaderManager.getEffect() != NULL)
          {
            D3DXHANDLE handle_param = CShaderManager::shaderManager.getEffect()->GetParameterByName( NULL, "MAT_GLOSSINESSMAP" );
            if ( handle_param ){
              CShaderManager::shaderManager.getEffect()->SetTexture( handle_param, t->texture );
            }
            MAT_GLOSSINESSMAP_LASTIDX = handle;

            changedTexture = true;
          }

          break;
        }
        case MAT_REFLECTIONMAP:
        {

          if (handle != MAT_REFLECTIONMAP_LASTIDX && CShaderManager::shaderManager.getEffect() != NULL)
          {
            D3DXHANDLE handle_param = CShaderManager::shaderManager.getEffect()->GetParameterByName( NULL, "MAT_REFLECTIONMAP" );
            if ( handle_param ){
              CShaderManager::shaderManager.getEffect()->SetTexture( handle_param, t->textureCube );
            }
            MAT_REFLECTIONMAP_LASTIDX = handle;

            changedTexture = true;
          }

          break;
        }
        case MAT_REFRACTIONMAP:
        {

          if (handle != MAT_REFRACTIONMAP_LASTIDX && CShaderManager::shaderManager.getEffect() != NULL)
          {
            D3DXHANDLE handle_param = CShaderManager::shaderManager.getEffect()->GetParameterByName( NULL, "MAT_REFRACTIONMAP" );
            if ( handle_param ){
              CShaderManager::shaderManager.getEffect()->SetTexture( handle_param, t->textureCube );
            }
            MAT_REFRACTIONMAP_LASTIDX = handle;

            changedTexture = true;
          }

          break;
        }
        case MAT_FILTERCOLORMAP:
        {

          if (handle != MAT_FILTERCOLORMAP_LASTIDX && CShaderManager::shaderManager.getEffect() != NULL)
          {
            D3DXHANDLE handle_param = CShaderManager::shaderManager.getEffect()->GetParameterByName( NULL, "MAT_FILTERCOLORMAP" );
            if ( handle_param ){
              CShaderManager::shaderManager.getEffect()->SetTexture( handle_param, t->texture );
            }
            MAT_FILTERCOLORMAP_LASTIDX = handle;

            changedTexture = true;
          }

          break;
        }
        case MAT_DISPLACEMENTMAP:
        {

          if (handle != MAT_DISPLACEMENTMAP_LASTIDX && CShaderManager::shaderManager.getEffect() != NULL)
          {
            D3DXHANDLE handle_param = CShaderManager::shaderManager.getEffect()->GetParameterByName( NULL, "MAT_DISPLACEMENTMAP" );
            if ( handle_param ){
              CShaderManager::shaderManager.getEffect()->SetTexture( handle_param, t->texture );
            }
            MAT_DISPLACEMENTMAP_LASTIDX = handle;

            changedTexture = true;
          }

          break;
        }
      }
    }
  }

  return changedTexture;

}

bool CTextureManager::clearTexture(int index) {

  bool textureChanged = false;
    
  switch( index ){

        case MAT_DIFFUSEMAP:{

            if (kWhiteTexture != MAT_DIFFUSEMAP_LASTIDX && CShaderManager::shaderManager.getEffect() != NULL)
            {
        D3DXHANDLE handle_param = CShaderManager::shaderManager.getEffect()->GetParameterByName( NULL, "MAT_DIFFUSEMAP" );
        if ( handle_param ){
          TextureCacheEntry t = textureCacheList[kWhiteTexture];
          CShaderManager::shaderManager.getEffect()->SetTexture( handle_param, t.texture );
        }

                MAT_DIFFUSEMAP_LASTIDX = kWhiteTexture;
        textureChanged = true;
            }

          break;
        }
        case MAT_SELFILLUMINATIONMAP:{

            if (kWhiteTexture != MAT_SELFILLUMINATIONMAP_LASTIDX && CShaderManager::shaderManager.getEffect() != NULL)
            {
        D3DXHANDLE handle_param = CShaderManager::shaderManager.getEffect()->GetParameterByName( NULL, "MAT_SELFILLUMINATIONMAP" );
        if ( handle_param ){
          TextureCacheEntry t = textureCacheList[kWhiteTexture];
          CShaderManager::shaderManager.getEffect()->SetTexture( handle_param, t.texture );
        }

                MAT_SELFILLUMINATIONMAP_LASTIDX = kWhiteTexture;
        textureChanged = true;
            }
                          
          break;
        }
        case MAT_OPACITYMAP:{

            if (kWhiteTexture != MAT_OPACITYMAP_LASTIDX && CShaderManager::shaderManager.getEffect() != NULL)
            {
        D3DXHANDLE handle_param = CShaderManager::shaderManager.getEffect()->GetParameterByName( NULL, "MAT_OPACITYMAP" );
        if ( handle_param ){
          TextureCacheEntry t = textureCacheList[kWhiteTexture];
          CShaderManager::shaderManager.getEffect()->SetTexture( handle_param, t.texture );
        }
                
                MAT_OPACITYMAP_LASTIDX = kWhiteTexture;
        textureChanged = true;
            }
          
          break;
        }
        case MAT_SPECULARCOLORMAP:{

            if (kBlackTexture != MAT_SPECULARCOLORMAP_LASTIDX && CShaderManager::shaderManager.getEffect() != NULL)
            {
                D3DXHANDLE handle_param = CShaderManager::shaderManager.getEffect()->GetParameterByName( NULL, "MAT_SPECULARCOLORMAP" );
        if ( handle_param ){
          TextureCacheEntry t = textureCacheList[kBlackTexture];
          CShaderManager::shaderManager.getEffect()->SetTexture( handle_param, t.texture );
        }

                MAT_SPECULARCOLORMAP_LASTIDX = kBlackTexture;
        textureChanged = true;
            }
                          
          break;
        }
    case MAT_SPECULARLEVELMAP:{

            if (kWhiteTexture != MAT_SPECULARLEVELMAP_LASTIDX && CShaderManager::shaderManager.getEffect() != NULL)
            {
        D3DXHANDLE handle_param = CShaderManager::shaderManager.getEffect()->GetParameterByName( NULL, "MAT_SPECULARLEVELMAP" );
        if ( handle_param ){
          TextureCacheEntry t = textureCacheList[kWhiteTexture];
          CShaderManager::shaderManager.getEffect()->SetTexture( handle_param, t.texture );
        }
                
                MAT_SPECULARLEVELMAP_LASTIDX = kWhiteTexture;
        textureChanged = true;
            }
                          
          break;
        }
        case MAT_BUMPMAP:{

            if (kPinkTexture != MAT_BUMPMAP_LASTIDX && CShaderManager::shaderManager.getEffect() != NULL)
            {
        D3DXHANDLE handle_param = CShaderManager::shaderManager.getEffect()->GetParameterByName( NULL, "MAT_BUMPMAP" );
        if ( handle_param ){
          TextureCacheEntry t = textureCacheList[kPinkTexture];
          CShaderManager::shaderManager.getEffect()->SetTexture( handle_param, t.texture );
        }
                
                MAT_BUMPMAP_LASTIDX = kPinkTexture;
        textureChanged = true;
            }
                          
          break;
        }
        case MAT_GLOSSINESSMAP:
        {

            if (kBlackTexture != MAT_GLOSSINESSMAP_LASTIDX && CShaderManager::shaderManager.getEffect() != NULL)
            {
            
        D3DXHANDLE handle_param = CShaderManager::shaderManager.getEffect()->GetParameterByName( NULL, "MAT_GLOSSINESSMAP" );
        if ( handle_param ){
          TextureCacheEntry t = textureCacheList[kBlackTexture];
          CShaderManager::shaderManager.getEffect()->SetTexture( handle_param, t.texture );
        }

                MAT_GLOSSINESSMAP_LASTIDX = kBlackTexture;
        textureChanged = true;
            }

            break;
        }
        case MAT_REFLECTIONMAP:
        {

            if (kWhiteTexture != MAT_REFLECTIONMAP_LASTIDX && CShaderManager::shaderManager.getEffect() != NULL)
            {

        D3DXHANDLE handle_param = CShaderManager::shaderManager.getEffect()->GetParameterByName( NULL, "MAT_REFLECTIONMAP" );
        if ( handle_param ){
          TextureCacheEntry t = textureCacheList[kWhiteTexture];
          CShaderManager::shaderManager.getEffect()->SetTexture( handle_param, t.textureCube );
        }

                MAT_REFLECTIONMAP_LASTIDX = kWhiteTexture;
        textureChanged = true;
            }

            break;
        }
    case MAT_REFRACTIONMAP:
        {

            if (kWhiteTexture != MAT_REFRACTIONMAP_LASTIDX && CShaderManager::shaderManager.getEffect() != NULL)
            {
                
        D3DXHANDLE handle_param = CShaderManager::shaderManager.getEffect()->GetParameterByName( NULL, "MAT_REFRACTIONMAP" );
        if ( handle_param ){
          TextureCacheEntry t = textureCacheList[kWhiteTexture];
          CShaderManager::shaderManager.getEffect()->SetTexture( handle_param, t.textureCube );
        }

                MAT_REFRACTIONMAP_LASTIDX = kWhiteTexture;
        textureChanged = true;
            }

            break;
        }
    case MAT_FILTERCOLORMAP:
        {

            if (kBlackTexture != MAT_FILTERCOLORMAP_LASTIDX && CShaderManager::shaderManager.getEffect() != NULL)
            {
                
        D3DXHANDLE handle_param = CShaderManager::shaderManager.getEffect()->GetParameterByName( NULL, "MAT_FILTERCOLORMAP" );
        if ( handle_param ){
          TextureCacheEntry t = textureCacheList[kBlackTexture];
          CShaderManager::shaderManager.getEffect()->SetTexture( handle_param, t.texture );
        }

                MAT_FILTERCOLORMAP_LASTIDX = kBlackTexture;
        textureChanged = true;
            }

            break;
        }
    case MAT_DISPLACEMENTMAP:
        {

            if (kBlackTexture != MAT_DISPLACEMENTMAP_LASTIDX && CShaderManager::shaderManager.getEffect() != NULL)
            {

        D3DXHANDLE handle_param = CShaderManager::shaderManager.getEffect()->GetParameterByName( NULL, "MAT_DISPLACEMENTMAP" );
        if ( handle_param ){
          TextureCacheEntry t = textureCacheList[kBlackTexture];
          CShaderManager::shaderManager.getEffect()->SetTexture( handle_param, t.texture );
        }

                MAT_DISPLACEMENTMAP_LASTIDX = kBlackTexture;
        textureChanged = true;
            }

            break;
        }
          
    }

  return textureChanged;

}

void CTextureManager::resetAllTextures() {
  
    clearTexture(MAT_DIFFUSEMAP);
    clearTexture(MAT_SPECULARCOLORMAP);
    clearTexture(MAT_SPECULARLEVELMAP);
    clearTexture(MAT_GLOSSINESSMAP);
    clearTexture(MAT_SELFILLUMINATIONMAP);
    clearTexture(MAT_OPACITYMAP);
    clearTexture(MAT_FILTERCOLORMAP);
  clearTexture(MAT_BUMPMAP);
  clearTexture(MAT_REFLECTIONMAP);
  clearTexture(MAT_REFRACTIONMAP);
  clearTexture(MAT_DISPLACEMENTMAP);

}

void CTextureManager::setTextureClamp(bool flag) {

  HRESULT result;

  if (g_pd3dDevice == NULL) {
    assert(false);
    return;
  }

  if (textureClamp != flag) {

    textureClamp = flag;

    D3DTEXTUREADDRESS m = flag ? D3DTADDRESS_CLAMP : D3DTADDRESS_WRAP;
    
    for (int i = 0; i < 5; i++ ){
      g_pd3dDevice->SetSamplerState(i, D3DSAMP_ADDRESSU, m ); 
      g_pd3dDevice->SetSamplerState(i, D3DSAMP_ADDRESSV, m );
    }

  }
}

*/
