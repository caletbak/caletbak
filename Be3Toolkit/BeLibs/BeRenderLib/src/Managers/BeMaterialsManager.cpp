/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeMaterialsManager.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <Managers/BeMaterialsManager.h>
#include <Render/BeRenderDevice.h>
#include <Render/BeMaterial.h>
#include <Render/BeMaterialConstant.h>
#include <Render/BeMaterialVertexDefinition.h>
#include <Render/BeTextureSampler.h>
#include <Render/BeMaterialProgram.h>

/////////////////////////////////////////////////////////////////////////////
BeInt BeMaterialsManager::DEFAULT_CACHE_SIZE = 2048;
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeMaterialsManager::BeMaterialsManager(BeRenderDevice* pRenderDevice, const BeString16& strMaterialsConfigBasePath)
:
    m_pRenderDevice(pRenderDevice),
    m_iCurrentMaterialSetForRendering(-1)
{
    ResetMaterialsCache();

    BeString16 strSamplersConfigFile = L"SamplersConfig.xml";
    BeString16 strProgramsConfigFile = L"ProgramsConfig.json";
    BeString16 strMaterialsConfigFile = L"MaterialsConfig.json";

    BeString16 strPathToSamplersConfig = strMaterialsConfigBasePath;
    strPathToSamplersConfig.Append(strSamplersConfigFile);

    BeXMLReader kXMLParserSamplers(strPathToSamplersConfig);
    if (kXMLParserSamplers.SearchXMLNode("./Samplers", "/"))
    {
        for (int mat = 0; mat < kXMLParserSamplers.GetNodeChildCount(); ++mat)
        {
            if (mat == 0)
                kXMLParserSamplers.MoveToFirstChild();  // Sampler

            int iChildCount = kXMLParserSamplers.GetNodeChildCount();

            BeTextureSampler* pTextureSampler = pRenderDevice->CreateTextureSampler();

            for (int i = 0; i < iChildCount; ++i)
            {
                if (i == 0)
                    kXMLParserSamplers.MoveToFirstChild();

                if (kXMLParserSamplers.GetNodeName().Compare("Name") == 0)
                    pTextureSampler->m_strName = ConvertString8To16(kXMLParserSamplers.ParseNodeValueString());

                if (kXMLParserSamplers.GetNodeName().Compare("Constant") == 0)
                    pTextureSampler->m_strConstantName = ConvertString8To16(kXMLParserSamplers.ParseNodeValueString());

                if (kXMLParserSamplers.GetNodeName().Compare("FilterMin") == 0)
                    pTextureSampler->m_strFilterMin = ConvertString8To16(kXMLParserSamplers.ParseNodeValueString());
                if (kXMLParserSamplers.GetNodeName().Compare("FilterMag") == 0)
                    pTextureSampler->m_strFilterMag = ConvertString8To16(kXMLParserSamplers.ParseNodeValueString());
                if (kXMLParserSamplers.GetNodeName().Compare("FilterMip") == 0)
                    pTextureSampler->m_strFilterMip = ConvertString8To16(kXMLParserSamplers.ParseNodeValueString());

                if (kXMLParserSamplers.GetNodeName().Compare("FilterFunc") == 0)
                    pTextureSampler->m_strFilterFunc = ConvertString8To16(kXMLParserSamplers.ParseNodeValueString());

                if (kXMLParserSamplers.GetNodeName().Compare("AddressU") == 0)
                    pTextureSampler->m_strAddressU = ConvertString8To16(kXMLParserSamplers.ParseNodeValueString());
                if (kXMLParserSamplers.GetNodeName().Compare("AddressV") == 0)
                    pTextureSampler->m_strAddressV = ConvertString8To16(kXMLParserSamplers.ParseNodeValueString());
                if (kXMLParserSamplers.GetNodeName().Compare("AddressW") == 0)
                    pTextureSampler->m_strAddressW = ConvertString8To16(kXMLParserSamplers.ParseNodeValueString());

                if (kXMLParserSamplers.GetNodeName().Compare("MipLODBias") == 0)
                    pTextureSampler->m_MipLODBias = kXMLParserSamplers.ParseNodeValueFloat();

                if (kXMLParserSamplers.GetNodeName().Compare("MaxAnisotropy") == 0)
                    pTextureSampler->m_MaxAnisotropy = kXMLParserSamplers.ParseNodeValueInt();

                if (kXMLParserSamplers.GetNodeName().Compare("ComparisonFunc") == 0)
                    pTextureSampler->m_strComparisionFunc = ConvertString8To16(kXMLParserSamplers.ParseNodeValueString());

                if (kXMLParserSamplers.GetNodeName().Compare("BorderColor") == 0)
                {
                    BeVector4D m_vBorderColor(kXMLParserSamplers.ParseNodeValueVector4D());
                    pTextureSampler->m_kBorderColor = m_vBorderColor;
                }

                if (kXMLParserSamplers.GetNodeName().Compare("MinLOD") == 0)
                {
                    BeString8 strTemp = kXMLParserSamplers.ParseNodeValueString();
                    if (strTemp.Compare("-FLT_MAX") == 0)
                    {
                        pTextureSampler->m_MinLOD = -FLT_MAX;
                    }
                    else if (strTemp.Compare("FLT_MAX") == 0)
                    {
                        pTextureSampler->m_MinLOD = FLT_MAX;
                    }
                    else
                    {
                        pTextureSampler->m_MinLOD = kXMLParserSamplers.ParseNodeValueFloat();
                    }
                }

                if (kXMLParserSamplers.GetNodeName().Compare("MaxLOD") == 0)
                {
                    BeString8 strTemp = kXMLParserSamplers.ParseNodeValueString();
                    if (strTemp.Compare("-FLT_MAX") == 0)
                    {
                        pTextureSampler->m_MaxLOD = -FLT_MAX;
                    }
                    else if (strTemp.Compare("FLT_MAX") == 0)
                    {
                        pTextureSampler->m_MaxLOD = FLT_MAX;
                    }
                    else
                    {
                        pTextureSampler->m_MaxLOD = kXMLParserSamplers.ParseNodeValueFloat();
                    }
                }

                kXMLParserSamplers.MoveToNextSibling();
            }
            kXMLParserSamplers.MoveToParent();

            pTextureSampler->PrepareSampler(pRenderDevice);
            m_kTextureSamplers[pTextureSampler->m_strName] = pTextureSampler;

            kXMLParserSamplers.MoveToParent();  // Sampler

            kXMLParserSamplers.MoveToNextSibling();
        }
    }

    if (m_pRenderDevice->m_eRenderAPIBeingUsed == BeRenderTypes::RENDER_DEVICE_API_DX11)
    {
        BeMaterialConstant::CONSTANT_ALIGNMENT = 16;
    }

    BeString16 strPathToProgramsConfig = strMaterialsConfigBasePath;
    strPathToProgramsConfig.Append(strProgramsConfigFile);

    BeJSONReader kJSONParserPrograms(strPathToProgramsConfig);
    if (kJSONParserPrograms.HasNode("Constants"))
    {
        BeJSONReader::BeJSONValue* pConstants = kJSONParserPrograms["Constants"];
        for (BeInt i = 0; i < pConstants->GetValueArraySize(); ++i)
        {
            BeJSONReader::BeJSONNode* pConstant = (*pConstants)[i]->GetValueAsNode();

            BeRenderTypes::BeConstantConfig* pNewConstantConfig = BeNew BeRenderTypes::BeConstantConfig();

            pNewConstantConfig->m_strName = (*pConstant)["Name"]->GetValueAsString();
            pNewConstantConfig->m_strType = (*pConstant)["Type"]->GetValueAsString();

            m_kConstantConfigs[pNewConstantConfig->m_strName] = pNewConstantConfig;
        }
    }
    if (kJSONParserPrograms.HasNode("Attributes"))
    {
        BeJSONReader::BeJSONValue* pAttributes = kJSONParserPrograms["Attributes"];
        for (BeInt i = 0; i < pAttributes->GetValueArraySize(); ++i)
        {
            BeJSONReader::BeJSONNode* pAttribute = (*pAttributes)[i]->GetValueAsNode();

            BeRenderTypes::BeAttributeConfig* pNewAttributeConfig = BeNew BeRenderTypes::BeAttributeConfig();

            pNewAttributeConfig->m_strName = (*pAttribute)["Name"]->GetValueAsString();
            pNewAttributeConfig->m_strType = (*pAttribute)["Type"]->GetValueAsString();
            pNewAttributeConfig->m_strSemantic = (*pAttribute)["Semantic"]->GetValueAsString();
            pNewAttributeConfig->m_iSemanticIndex = (*pAttribute)["SemanticIndex"]->GetValueAsInt();
            pNewAttributeConfig->m_bNormalised = (*pAttribute)["Normalised"]->GetValueAsBoolean();

            m_kAttributeConfigs[pNewAttributeConfig->m_strName] = pNewAttributeConfig;
        }
    }
    if (kJSONParserPrograms.HasNode("TextureChannels"))
    {
        BeJSONReader::BeJSONValue* pTextureChannels = kJSONParserPrograms["TextureChannels"];
        for (BeInt i = 0; i < pTextureChannels->GetValueArraySize(); ++i)
        {
            BeJSONReader::BeJSONNode* pTextureChannel = (*pTextureChannels)[i]->GetValueAsNode();

            BeRenderTypes::BeTextureChannelConfig* pNewTextureChannelConfig = BeNew BeRenderTypes::BeTextureChannelConfig();

            pNewTextureChannelConfig->m_strName = (*pTextureChannel)["Name"]->GetValueAsString();
            pNewTextureChannelConfig->m_strTextureConstant = (*pTextureChannel)["TextureConstant"]->GetValueAsString();
            pNewTextureChannelConfig->m_strSamplerConstant = (*pTextureChannel)["SamplerConstant"]->GetValueAsString();
            pNewTextureChannelConfig->m_pTextureSampler = m_kTextureSamplers[ConvertString8To16((*pTextureChannel)["SamplerID"]->GetValueAsString())];

            m_kTextureChannelConfigs[pNewTextureChannelConfig->m_strName] = pNewTextureChannelConfig;
        }
    }
    if (kJSONParserPrograms.HasNode("MaterialPrograms"))
    {
        BeJSONReader::BeJSONValue* pMaterialPrograms = kJSONParserPrograms["MaterialPrograms"];
        for (BeInt i = 0; i < pMaterialPrograms->GetValueArraySize(); ++i)
        {
            BeJSONReader::BeJSONNode* pMaterialProgram = (*pMaterialPrograms)[i]->GetValueAsNode();

            BeString8 strProgramFilename = (*pMaterialProgram)["Filename"]->GetValueAsString();
            BeString8 strProgramType = (*pMaterialProgram)["Type"]->GetValueAsString();
            BeString8 strProgramFunction = (*pMaterialProgram)["Function"]->GetValueAsString();

            BeInt iProgramType = -1;
            if (strProgramType.Compare("VERTEX_SHADER") == 0)
                iProgramType = BeRenderTypes::MATERIAL_PIPELINE_VERTEX;
            if (strProgramType.Compare("PIXEL_SHADER") == 0)
                iProgramType = BeRenderTypes::MATERIAL_PIPELINE_PIXEL;

            if (iProgramType != -1)
            {
                BeVoidP pBufferData = NULL;
                BeInt iBufferSize = 0;

                LoadMaterialProgram(strMaterialsConfigBasePath, ConvertString8To16(strProgramFilename), &pBufferData, iBufferSize);

                if (pBufferData)
                {
                    BeMaterialProgram* pNewMaterialProgram = pRenderDevice->CreateMaterialProgramFromBuffer((BeRenderTypes::EBeMaterialPipeline)iProgramType, pBufferData, iBufferSize, strProgramFunction);

                    BeFreeMemory(pBufferData);

                    if (pMaterialProgram->HasNode("Constants"))
                    {
                        BeJSONReader::BeJSONValue* pConstants = (*pMaterialProgram)["Constants"];
                        if (pConstants)
                        {
                            BeJSONReader::BeJSONNode* pConstantsNode = pConstants->GetValueAsNode();
                            std::map<BeString8, BeJSONReader::BeJSONValue*>::iterator it = pConstantsNode->m_kMembers.begin();
                            if (it != pConstantsNode->m_kMembers.end())
                            {
                                pNewMaterialProgram->SetProgramConstantBlockName(it->first);

                                for (BeInt j = 0; j < it->second->GetValueArraySize(); ++j)
                                {
                                    BeString8 strConstant = (*it->second)[j]->GetValueAsString();

                                    pNewMaterialProgram->AddProgramConstant(m_kConstantConfigs[strConstant]);
                                }
                            }
                        }
                    }
                    if (pMaterialProgram->HasNode("Attributes"))
                    {
                        BeJSONReader::BeJSONValue* pAttributes = (*pMaterialProgram)["Attributes"];
                        if (pAttributes->GetValueArraySize() > 0)
                        {
                            pNewMaterialProgram->SetVertexDefinition(pRenderDevice->CreateVertexDefinition());

                            for (BeInt j = 0; j < pAttributes->GetValueArraySize(); ++j)
                            {
                                BeString8 strAttribute = (*pAttributes)[j]->GetValueAsString();

                                pNewMaterialProgram->GetVertexDefinition()->AddAttribute(m_kAttributeConfigs[strAttribute]);
                            }

                            pNewMaterialProgram->PrepareVertexDefinition(pRenderDevice);
                        }
                    }
                    if (pMaterialProgram->HasNode("TextureChannels"))
                    {
                        BeJSONReader::BeJSONValue* pTextureChannels = (*pMaterialProgram)["TextureChannels"];
                        if (pTextureChannels->GetValueArraySize() > 0)
                        {
                            for (BeInt j = 0; j < pTextureChannels->GetValueArraySize(); ++j)
                            {
                                BeString8 strTextureChannel = (*pTextureChannels)[j]->GetValueAsString();

                                pNewMaterialProgram->AddProgramTextureChannel(m_kTextureChannelConfigs[strTextureChannel]);
                            }
                        }
                    }

                    m_kMaterialPrograms[strProgramFilename] = pNewMaterialProgram;
                }
            }
        }
    }

    BeString16 strPathToMaterialsConfig = strMaterialsConfigBasePath;
    strPathToMaterialsConfig.Append(strMaterialsConfigFile);

    BeJSONReader kJSONParserMaterials(strPathToMaterialsConfig);
    if (kJSONParserMaterials.HasNode("Materials"))
    {
        BeJSONReader::BeJSONValue* pMaterials = kJSONParserMaterials["Materials"];
        for (BeInt i = 0; i < pMaterials->GetValueArraySize(); ++i)
        {
            BeJSONReader::BeJSONNode* pMaterial = (*pMaterials)[i]->GetValueAsNode();

            BeString8 strMaterialName = (*pMaterial)["Name"]->GetValueAsString();
            BeString8 strVSProgram = (*pMaterial)["VSProgram"]->GetValueAsString();
            BeString8 strPSProgram = (*pMaterial)["PSProgram"]->GetValueAsString();

            BeRenderTypes::BeMaterialConfig kMaterialConfig;
            kMaterialConfig.m_strName = ConvertString8To16(strMaterialName);
            kMaterialConfig.m_pVSProgram = m_kMaterialPrograms[strVSProgram];
            kMaterialConfig.m_pPSProgram = m_kMaterialPrograms[strPSProgram];

            BeInt iMaterialSlot = AllocMaterial(kMaterialConfig.m_strName);
            BeRenderTypes::BeMaterialCacheEntry* pCachedMaterial = m_pMaterialsCacheArray[iMaterialSlot].GetPointer();

            m_pRenderDevice->CreateMaterialFromConfig(pCachedMaterial, kMaterialConfig);

            pCachedMaterial->m_pMaterial->PrepareMaterial(pRenderDevice);
        }
    }
}
/////////////////////////////////////////////////////////////////////////////
BeMaterialsManager::~BeMaterialsManager()
{
    ReleaseAllCachedMaterials();

    std::map<BeString8, BeRenderTypes::BeConstantConfig*>::iterator constantsIt = m_kConstantConfigs.begin();
    while (constantsIt != m_kConstantConfigs.end())
    {
        BeRenderTypes::BeConstantConfig* pContantConfig = reinterpret_cast<BeRenderTypes::BeConstantConfig*>(constantsIt->second);
        BeDelete pContantConfig;

        constantsIt++;
    }
    m_kConstantConfigs.clear();

    std::map<BeString8, BeRenderTypes::BeAttributeConfig*>::iterator attributesIt = m_kAttributeConfigs.begin();
    while (attributesIt != m_kAttributeConfigs.end())
    {
        BeRenderTypes::BeAttributeConfig* pAttributeConfig = reinterpret_cast<BeRenderTypes::BeAttributeConfig*>(attributesIt->second);
        BeDelete pAttributeConfig;

        attributesIt++;
    }
    m_kAttributeConfigs.clear();

    std::map<BeString8, BeRenderTypes::BeTextureChannelConfig*>::iterator textureChannelsIt = m_kTextureChannelConfigs.begin();
    while (textureChannelsIt != m_kTextureChannelConfigs.end())
    {
        BeRenderTypes::BeTextureChannelConfig* pAttributeConfig = reinterpret_cast<BeRenderTypes::BeTextureChannelConfig*>(textureChannelsIt->second);
        BeDelete pAttributeConfig;

        textureChannelsIt++;
    }
    m_kTextureChannelConfigs.clear();

    std::map<BeString16, BeTextureSampler*>::iterator samplersIt = m_kTextureSamplers.begin();
    while (samplersIt != m_kTextureSamplers.end())
    {
        BeTextureSampler* pTextureSampler = reinterpret_cast<BeTextureSampler*>(samplersIt->second);
        BeDelete pTextureSampler;

        samplersIt++;
    }
    m_kTextureSamplers.clear();

    m_pRenderDevice = NULL;
}
/////////////////////////////////////////////////////////////////////////////
void BeMaterialsManager::ReleaseAllCachedMaterials()
{
    m_pMaterialsCacheArray.clear();
}
/////////////////////////////////////////////////////////////////////////////
void BeMaterialsManager::ResetMaterialsCache()
{
    ReleaseAllCachedMaterials();

    m_pMaterialsCacheArray.resize(DEFAULT_CACHE_SIZE);
    for ( BeInt i = 0; i < DEFAULT_CACHE_SIZE; ++i )
    {
        m_pMaterialsCacheArray[i] = BeNew BeRenderTypes::BeMaterialCacheEntry();
    }

    BeInt iMaterialSlot = AllocMaterial(L"MATERIAL_NONE");

    BeRenderTypes::BeMaterialCacheEntry* pCachedMaterial = m_pMaterialsCacheArray[iMaterialSlot].GetPointer();
    BeRenderTypes::BeMaterialConfig kMaterialConfig;
    kMaterialConfig.m_strName = L"MATERIAL_NONE";
    m_pRenderDevice->CreateMaterialFromConfig(pCachedMaterial, kMaterialConfig);
}
/////////////////////////////////////////////////////////////////////////////
void BeMaterialsManager::LoadMaterialProgram(const BeString16& strBasePath, const BeString16& strFileName, BeVoidP* pBufferData, BeInt& iBufferSize)
{
    BeString16 strPathToMaterial = strBasePath;
    BeString16 strMaterialExtension;
    switch (m_pRenderDevice->m_eRenderAPIBeingUsed)
    {
        case BeRenderTypes::RENDER_DEVICE_API_DX11:
        {
            strPathToMaterial.Append(L"DX\\");
            strMaterialExtension = L".hlsl";
            break;
        }
        case BeRenderTypes::RENDER_DEVICE_API_GL:
        {
            strPathToMaterial.Append(L"GL\\");
            strMaterialExtension = L".glsh";
            break;
        }
    }

    BeString16 strPathToMaterialVS = strPathToMaterial;
    strPathToMaterialVS.Append(strFileName);
    strPathToMaterialVS.Append(strMaterialExtension);

    *pBufferData = BeFileTool::ReadEntireFile(strPathToMaterialVS, &iBufferSize);
}
/////////////////////////////////////////////////////////////////////////////
void BeMaterialsManager::ProcessLoadedEvent(const BeString16& strFileName, BeVoidP pBufferData, BeInt iBufferSize, int eMaterialStep)
{
    BE_UNUSED(strFileName);
    BE_UNUSED(pBufferData);
    BE_UNUSED(iBufferSize);
    BE_UNUSED(eMaterialStep);
}
/////////////////////////////////////////////////////////////////////////////
BeInt BeMaterialsManager::FindMaterial(const BeString16& strMaterialName)
{
    for (BeUInt i = 0; i < m_pMaterialsCacheArray.size(); ++i)
    {
        BeRenderTypes::BeMaterialCacheEntry* pCachedMaterial = m_pMaterialsCacheArray[i].GetPointer();
        if (pCachedMaterial->m_strMaterialName.Compare(strMaterialName) == 0)
            return i;
    }

    return -1;
}
/////////////////////////////////////////////////////////////////////////////
BeInt BeMaterialsManager::AllocMaterial(const BeString16& strMaterialName)
{
  BeInt iFreeCacheSlot = -1;

  for (BeUInt i = 0; i < m_pMaterialsCacheArray.size(); ++i)
  {
      BeRenderTypes::BeMaterialCacheEntry* pCachedMaterial = m_pMaterialsCacheArray[i].GetPointer();
      if (!pCachedMaterial->m_bIsAllocated && pCachedMaterial->m_pMaterial == NULL)
      {
        iFreeCacheSlot = i;
        break;
      }
  }

  if ( iFreeCacheSlot == -1 )
  {
      if (m_pMaterialsCacheArray.size() >= (BeUInt)DEFAULT_CACHE_SIZE)
      {
        BE_RENDER_DEBUG("CAUTION : Material cache full !!!");
        return -1;
      }

      iFreeCacheSlot = m_pMaterialsCacheArray.size();
  }

  FreeMaterial(iFreeCacheSlot);

  BeRenderTypes::BeMaterialCacheEntry* pCachedMaterial = m_pMaterialsCacheArray[iFreeCacheSlot].GetPointer();
  pCachedMaterial->m_strMaterialName = strMaterialName;
  pCachedMaterial->m_bIsAllocated = BE_TRUE;

  return iFreeCacheSlot;
}
/////////////////////////////////////////////////////////////////////////////
void BeMaterialsManager::FreeMaterial(BeInt iCacheHandle)
{
    if (iCacheHandle < 0 || (BeUInt)iCacheHandle >= m_pMaterialsCacheArray.size())
    return;

    m_pMaterialsCacheArray[iCacheHandle]->Release();
}
/////////////////////////////////////////////////////////////////////////////
BeMaterial* BeMaterialsManager::SetMaterialForRendering(const BeString16& strMaterialName)
{
    BeInt iMaterialSlot = FindMaterial(strMaterialName);
    if (iMaterialSlot != -1)
    {
        if (iMaterialSlot != m_iCurrentMaterialSetForRendering)
        {
            m_iCurrentMaterialSetForRendering = iMaterialSlot;
            m_pRenderDevice->SetMaterialForRendering(m_pMaterialsCacheArray[iMaterialSlot]->m_pMaterial);
        }

        return m_pMaterialsCacheArray[iMaterialSlot]->m_pMaterial;
    }

    return NULL;
}
/////////////////////////////////////////////////////////////////////////////
void BeMaterialsManager::RequestUpdateConstant(const BeString8& strConstantName, BeVoidP pValue)
{
    if (m_iCurrentMaterialSetForRendering != -1)
    {
        BeRenderTypes::BeMaterialCacheEntry* pCachedMaterial = m_pMaterialsCacheArray[m_iCurrentMaterialSetForRendering].GetPointer();
        pCachedMaterial->m_pMaterial->RequestUpdateConstant(strConstantName, pValue);
    }
}
/////////////////////////////////////////////////////////////////////////////
void BeMaterialsManager::UpdateMaterial(void)
{
    if (m_iCurrentMaterialSetForRendering != -1)
    {
        BeRenderTypes::BeMaterialCacheEntry* pCachedMaterial = m_pMaterialsCacheArray[m_iCurrentMaterialSetForRendering].GetPointer();
        pCachedMaterial->m_pMaterial->UpdateMaterial(m_pRenderDevice);
    }
}
/////////////////////////////////////////////////////////////////////////////
void BeMaterialsManager::SetConstantBuffers(void)
{
    if (m_iCurrentMaterialSetForRendering != -1)
    {
        BeRenderTypes::BeMaterialCacheEntry* pCachedMaterial = m_pMaterialsCacheArray[m_iCurrentMaterialSetForRendering].GetPointer();
        pCachedMaterial->m_pMaterial->SetConstantBuffers(m_pRenderDevice);
    }
}
/////////////////////////////////////////////////////////////////////////////
void BeMaterialsManager::SetTextureSamplers(void)
{
    if (m_iCurrentMaterialSetForRendering != -1)
    {
        BeRenderTypes::BeMaterialCacheEntry* pCachedMaterial = m_pMaterialsCacheArray[m_iCurrentMaterialSetForRendering].GetPointer();
        pCachedMaterial->m_pMaterial->SetTextureSamplers(m_pRenderDevice);
    }
}
/////////////////////////////////////////////////////////////////////////////
void BeMaterialsManager::SetMaterialIntoCacheSlot(BeInt iCacheHandle, BeMaterial* pMaterial)
{
    if (iCacheHandle < 0 || (BeUInt)iCacheHandle >= m_pMaterialsCacheArray.size())
    return;

    BeRenderTypes::BeMaterialCacheEntry* pCachedMaterial = m_pMaterialsCacheArray[iCacheHandle].GetPointer();
    pCachedMaterial->m_pMaterial = pMaterial;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeRenderTypes::BeMaterialCacheEntry* BeMaterialsManager::GetMaterialCacheEntry(BeInt iMaterialCacheSlot)
{
    if (iMaterialCacheSlot < 0 || (BeUInt)iMaterialCacheSlot >= m_pMaterialsCacheArray.size())
    return NULL;

    BeRenderTypes::BeMaterialCacheEntry* pCachedMaterial = m_pMaterialsCacheArray[iMaterialCacheSlot].GetPointer();
    return pCachedMaterial;
}
/////////////////////////////////////////////////////////////////////////////
BeInt BeMaterialsManager::GetCurrentMaterialIDSetForRendering(void)
{
    return m_iCurrentMaterialSetForRendering;
}
/////////////////////////////////////////////////////////////////////////////
BeMaterial* BeMaterialsManager::GetCurrentMaterialSetForRendering(void)
{
    if (m_iCurrentMaterialSetForRendering != -1)
    {
        return m_pMaterialsCacheArray[m_iCurrentMaterialSetForRendering]->m_pMaterial;
    }

    return NULL;
}
/////////////////////////////////////////////////////////////////////////////
BeMaterialProgram* BeMaterialsManager::GetMaterialProgramByName(const BeString8& strProgramName)
{
    std::map<BeString8, BeMaterialProgram*>::iterator it = m_kMaterialPrograms.find(strProgramName);
    if (it != m_kMaterialPrograms.end())
    {
        return it->second;
    }

    return NULL;
}
/////////////////////////////////////////////////////////////////////////////

