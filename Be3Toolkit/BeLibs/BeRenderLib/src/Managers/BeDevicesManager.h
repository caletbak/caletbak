/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeDevicesManager.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEDEVICESMANAGER_H
#define BE_BEDEVICESMANAGER_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>

#include <BeRenderTypes.h>

/// Forward declarations
class BeRenderDevice;

/////////////////////////////////////////////////
/// \class BeDevicesManager
/// \brief Class that controls all the hardware devices
/////////////////////////////////////////////////
class RENDER_API BeDevicesManager : public BeMemoryObject
{
    
  public:
    
    /// \brief Constructor
    BeDevicesManager();
    
    /// \brief Destructor
    ~BeDevicesManager();



    // Getters
    // {
    
      /// \brief  Returns the render device
      BeRenderDevice* GetRenderDevice(void);
            
      /// \brief  Returns the device adapter
      const BeRenderTypes::BeDeviceAdapter& GetDeviceAdapter(BeUInt uAdapter);

      /// \brief  Returns the device adapter count
      BeUInt GetDeviceAdapterCount(void);

    // }



    // Methods
    // {

      /// \brief  Creates a new Render Device
      BeRenderDevice* CreateRenderDevice(const BeRenderTypes::BeRenderDeviceSettings& kRenderDeviceSettings);

      /// \brief  Checks available graphics APIs and display modes they could present in the running system
      void CheckForRenderDevices(void);
          
    // }



  private:

    /// \brief  The render device to be used for rendering
    BeRenderDevice* m_pRenderDevice;

    /// \brief  The windowed display modes enumarated
    BeRenderTypes::BeEnumAdaptersArray m_kEnumAdapters;
    
};

// Smart pointer BeDevicesManagerPtr
typedef BeSmartPointer<BeDevicesManager> BeDevicesManagerPtr;

#endif // #ifndef BE_BEDEVICESMANAGER_H
