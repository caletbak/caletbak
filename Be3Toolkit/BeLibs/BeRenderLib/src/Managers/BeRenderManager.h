/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeRenderManager.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BERENDERMANAGER_H
#define BE_BERENDERMANAGER_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>

#include <Render/BeRenderPipelineStep.h>

// Forward declarations
class BeRenderDevice;

/////////////////////////////////////////////////
/// \class BeRenderManager
/// \brief Manager that controls the render pipelines
/////////////////////////////////////////////////
class RENDER_API BeRenderManager : public BeMemoryObject
{
      
  public:
    
    /// \brief Constructor
    BeRenderManager(void);
    /// \brief Destructor
    ~BeRenderManager();



    // Getters
    // {

      /// \brief  Gets the current render device
      BeRenderDevice* GetRenderDevice(void);

    // }



    // Setters
    // {

      /// \brief  Sets the current render device to be used for rendering
      void SetRenderDevice( BeRenderDevice* pRenderDevice );

    // }



    // Methods
    // {

      /// \brief  Registers a new render pipeline step
      void RegisterRenderPipelineStep(BeRenderPipelineStepPtr spStep, const BeString8& strStepName = BeString8::EMPTY());

      /// \brief  Unregisters a render pipeline step
      void UnRegisterRenderPipelineStep(const BeString8& strStepName);
      void UnRegisterRenderPipelineStep(const BeRenderPipelineStepPtr& spStep);

      /// \brief  Performs the rendering calling to PreRender, Render and PostRender methods of each registered RenderPipelineStep
      void PerformRenderingProcess(void);

      /// \brief  Presents the current frame
      void PresentFrame(void);

    // }

  private:

    /// \brief The render device to be used for rendering
    BeRenderDevice* m_pRenderDevice;

    /// \brief  The data to be stored by step
    struct sPipeLineStepData
    {
      BeString8 m_strStepName;
      BeRenderPipelineStepPtr m_spStep;
    };
    typedef BeSmartPointer<sPipeLineStepData> sPipeLineStepDataPtr;

    /// \brief  The render pipeline steps
    typedef std::list<sPipeLineStepDataPtr> BeRenderPipelineStepsList;
    BeRenderPipelineStepsList m_kRenderPipelineSteps;

};

// Smart pointer BeRenderManagerPtr
typedef BeSmartPointer<BeRenderManager> BeRenderManagerPtr;

#endif // #ifndef BE_BERENDERMANAGER_H
