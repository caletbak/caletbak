/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeDevicesManager.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <Managers/BeDevicesManager.h>
#include <Render/BeRenderDevice.h>

#if TARGET_WINDOWS || TARGET_MACOS
#include <GraphicsAPI/BeRenderDeviceGL.h>
#endif

#if TARGET_WINDOWS
#include <GraphicsAPI/BeRenderDeviceDX11.h>
#include <crtdbg.h>
#endif

/////////////////////////////////////////////////////////////////////////////
BeDevicesManager::BeDevicesManager()
:
  m_pRenderDevice(NULL)
{
}
/////////////////////////////////////////////////////////////////////////////
BeDevicesManager::~BeDevicesManager()
{
  if (m_pRenderDevice != NULL)
  {
    BeDelete(m_pRenderDevice);
    m_pRenderDevice = NULL;
  }
  m_kEnumAdapters.clear();
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeDevicesManager::CheckForRenderDevices(void)
{
#if TARGET_WINDOWS
  BeInt iNumAdapters = BeRenderDeviceDX11::CheckNumAdapters();
  if ( iNumAdapters == -1 )
    iNumAdapters = BeRenderDeviceGL::CheckNumAdapters();

  // Check render APIs and display modes
  for ( BeInt i = 0; i < iNumAdapters; ++i )
  {
    BeRenderTypes::BeDeviceAdapter kDeviceAdapter;

    kDeviceAdapter.m_kRenderAPIAvailable[BeRenderTypes::RENDER_DEVICE_API_DX11] = BeRenderDeviceDX11::CheckD3D11(i);
    kDeviceAdapter.m_kRenderAPIAvailable[BeRenderTypes::RENDER_DEVICE_API_GL] = BeRenderDeviceGL::CheckGL(i);

    BeBoolean bDisplayModesFilled = BE_FALSE;

    if ( !bDisplayModesFilled && kDeviceAdapter.m_kRenderAPIAvailable[BeRenderTypes::RENDER_DEVICE_API_DX11] )
      bDisplayModesFilled = BeRenderDeviceDX11::EnumerateDisplayModes(i, kDeviceAdapter);

    if ( !bDisplayModesFilled && kDeviceAdapter.m_kRenderAPIAvailable[BeRenderTypes::RENDER_DEVICE_API_GL] )
      bDisplayModesFilled = BeRenderDeviceGL::EnumerateDisplayModes(i, kDeviceAdapter);

    m_kEnumAdapters.push_back(kDeviceAdapter);
  }
#endif
}
/////////////////////////////////////////////////////////////////////////////
BeRenderDevice* BeDevicesManager::CreateRenderDevice(const BeRenderTypes::BeRenderDeviceSettings& kRenderDeviceSettings)
{
  switch(kRenderDeviceSettings.m_eRenderDeviceAPI)
  {
#if TARGET_WINDOWS
    case BeRenderTypes::RENDER_DEVICE_API_DX11:
    {
      m_pRenderDevice = BeNew BeRenderDeviceDX11();
      break;
    }
    case BeRenderTypes::RENDER_DEVICE_API_GL:
    {
      m_pRenderDevice = BeNew BeRenderDeviceGL();
      break;
    }
#endif

    case BeRenderTypes::RENDER_DEVICE_UNKNOWN:
      return NULL;
  }

  if (m_pRenderDevice)
  {
    if ( !m_pRenderDevice->CreateRenderDevice(kRenderDeviceSettings) )
    {
      BE_RENDER_ERROR("The Render Device API has not been created!");

      BeDelete(m_pRenderDevice);
      m_pRenderDevice = NULL;
    }
    else
    {
        m_pRenderDevice->CreateTextureManager();
        m_pRenderDevice->CreateMaterialsManager();
        m_pRenderDevice->CreateRenderStates();
    }
  }

  return m_pRenderDevice;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeRenderDevice* BeDevicesManager::GetRenderDevice(void)
{
  return m_pRenderDevice;
}
/////////////////////////////////////////////////////////////////////////////
const BeRenderTypes::BeDeviceAdapter& BeDevicesManager::GetDeviceAdapter(BeUInt uAdapter)
{
  return m_kEnumAdapters.at(uAdapter);
}
/////////////////////////////////////////////////////////////////////////////
BeUInt BeDevicesManager::GetDeviceAdapterCount(void)
{
  return m_kEnumAdapters.size();
}
/////////////////////////////////////////////////////////////////////////////


