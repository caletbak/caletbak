/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeRenderManager.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <Managers/BeRenderManager.h>
#include <Render/BeRenderDevice.h>


/////////////////////////////////////////////////////////////////////////////
BeRenderManager::BeRenderManager(void)
:
  m_pRenderDevice(NULL)
{
}
/////////////////////////////////////////////////////////////////////////////
BeRenderManager::~BeRenderManager()
{
    m_pRenderDevice = NULL;

    m_kRenderPipelineSteps.clear();
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeRenderDevice* BeRenderManager::GetRenderDevice(void)
{
    return m_pRenderDevice;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeRenderManager::SetRenderDevice( BeRenderDevice* pRenderDevice )
{
  m_pRenderDevice = pRenderDevice;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeRenderManager::RegisterRenderPipelineStep(BeRenderPipelineStepPtr spStep, const BeString8& strStepName)
{
  if ( strStepName != BeString8::EMPTY() )
  {
    BeRenderPipelineStepsList::iterator kPipelineBegin( m_kRenderPipelineSteps.begin() );
    BeRenderPipelineStepsList::const_iterator kPipelineEnd( m_kRenderPipelineSteps.end() );
    while( kPipelineBegin != kPipelineEnd )
    {
      // Gets the current render pipeline step
      BeRenderManager::sPipeLineStepData* pRenderPipelineStep = static_cast<BeRenderManager::sPipeLineStepData*>(*kPipelineBegin);
      if ( pRenderPipelineStep->m_strStepName == strStepName )
      {
        BE_RENDER_WARNING("A render pipeline step with name %s already exists.", strStepName.ToRaw());
        return;
      }
      ++kPipelineBegin;
    }
  }

  sPipeLineStepDataPtr spStepData = BeNew sPipeLineStepData();
  spStepData->m_spStep = spStep;
  spStepData->m_strStepName = strStepName;
  m_kRenderPipelineSteps.push_back(spStepData);
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderManager::UnRegisterRenderPipelineStep(const BeString8& strStepName)
{
  if ( strStepName != BeString8::EMPTY() )
  {
    BeRenderPipelineStepsList::iterator kPipelineBegin( m_kRenderPipelineSteps.begin() );
    BeRenderPipelineStepsList::const_iterator kPipelineEnd( m_kRenderPipelineSteps.end() );
    while( kPipelineBegin != kPipelineEnd )
    {
      // Gets the current render pipeline step
      BeRenderManager::sPipeLineStepData* pRenderPipelineStep = static_cast<BeRenderManager::sPipeLineStepData*>(*kPipelineBegin);
      if ( pRenderPipelineStep->m_strStepName == strStepName )
      {
        m_kRenderPipelineSteps.erase(kPipelineBegin);
        return;
      }
      ++kPipelineBegin;
    }
    BE_RENDER_WARNING("The render pipeline step with name %s does not exist.", strStepName.ToRaw());
  }
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderManager::UnRegisterRenderPipelineStep(const BeRenderPipelineStepPtr& spStep)
{
  if ( spStep != NULL )
  {
    BeRenderPipelineStepsList::iterator kPipelineBegin( m_kRenderPipelineSteps.begin() );
    BeRenderPipelineStepsList::const_iterator kPipelineEnd( m_kRenderPipelineSteps.end() );
    while( kPipelineBegin != kPipelineEnd )
    {
      // Gets the current render pipeline step
      BeRenderManager::sPipeLineStepData* pRenderPipelineStep = static_cast<BeRenderManager::sPipeLineStepData*>(*kPipelineBegin);
      if ( pRenderPipelineStep->m_spStep.GetPointer() == spStep.GetPointer() )
      {
        m_kRenderPipelineSteps.erase(kPipelineBegin);
        return;
      }
      ++kPipelineBegin;
    }
    BE_RENDER_WARNING("The render pipeline step does not exist.");
  }
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderManager::PerformRenderingProcess(void)
{

  BeRenderPipelineStepsList::iterator kPipelineBegin( m_kRenderPipelineSteps.begin() );
  BeRenderPipelineStepsList::const_iterator kPipelineEnd( m_kRenderPipelineSteps.end() );
  while( kPipelineBegin != kPipelineEnd )
  {
    
    // Gets the current render pipeline step
    BeRenderManager::sPipeLineStepData* pRenderPipelineStep = static_cast<BeRenderManager::sPipeLineStepData*>(*kPipelineBegin);

    // Performs the prerender pass
    pRenderPipelineStep->m_spStep->PreRender(m_pRenderDevice);

    // Performs the render pass
    pRenderPipelineStep->m_spStep->Render(m_pRenderDevice);

    // Performs the postrender pass
    pRenderPipelineStep->m_spStep->PostRender(m_pRenderDevice);
        
    ++kPipelineBegin;

  }
  
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderManager::PresentFrame(void)
{

  // Present the current frame
  m_pRenderDevice->Present();

}
/////////////////////////////////////////////////////////////////////////////



