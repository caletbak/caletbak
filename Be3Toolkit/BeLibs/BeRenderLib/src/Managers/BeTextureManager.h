/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeEngineContext.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BETEXTUREMANAGER_H
#define BE_BETEXTUREMANAGER_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>

#include <BeRenderTypes.h>

// Forward delacations
class BeRenderDevice;
class BeTextureSampler;

/////////////////////////////////////////////////
/// \class BeTextureManager
/// \brief Manager that controls the created or loaded textures
/////////////////////////////////////////////////
class RENDER_API BeTextureManager : public BeCoreTypes::ResourcesInterface
{

  // Class Friendship
  friend class BeResourcesManager;

    public:

        /// \brief  Constructor
        BeTextureManager(BeRenderDevice* pRenderDevice);
        /// \brief  Destructor
        ~BeTextureManager();



    // Methods
    // {

      /// \brief  Release all the cached textures
      void ReleaseAllCachedTextures();

      /// \brief  Prepares the texture cache and creates the default textures
      void ResetTextureCache();

      /// \brief  Creates a new texture from file. Returns the handle of the cache slot where the texture will be allocated and it can be used once the texture is loaded and ready to use.
      BeInt LoadTextureFromFile(const BeString16& strFileName, BeBoolean bAsyncLoad = BE_FALSE);

      /// \brief  Returns the texture handle if the texture name has been found in the cache
      BeInt FindTexture(const BeString16& strTextureName);

      /// \brief  Allocates a new texture cached space and returns the handle
      BeInt AllocTexture(const BeString16& strTextureName);

      /// \brief  Frees a cached texture
      void FreeTexture(BeInt iCacheHandle);

    // }



    // Getters
    // {

      /// \brief  Return a texture entry previously loaded
      BeRenderTypes::BeTextureCacheEntry* GetTextureCacheEntry(BeInt iTextureCacheSlot);

    // }



    /*
    int getActualNumCachedTextures() { return nextTextureSlot; }

    

        
    void  setTextureIsLoaded(int slot);
    bool  getIsTextureLoaded(int slot);
    int    cacheTexture(char* filename, TextureReference* texture);
    void  cacheTexture(TextureReference* texture);
    void  unCacheTexture(TextureReference* texture);
    bool  cacheTextureGroup(char* filename, TextureReference* texture);
    void  cacheTextureGroup(TextureReference* texture);
    void  cacheTextureGroup(TextureReference* texture, int format);
    bool  cacheCubeTextureGroup(char* filename, TextureReference* texture);
    void  cacheCubeTextureGroup(TextureReference* texture);
    void  resetTextureCache();

    
    
    void  freeAllTextures();

    bool  selectTexture( int handle, int index );
      
    bool  clearTexture( int index );
    void  releaseTexturesFromModel3D( CModel3D* materials );
    void  resetAllTextures();

    void  setTextureClamp(bool flag);

    static  CTextureManager    textureManager;
    */

  private:

    /// \brief  The default texture cache size
    static BeInt DEFAULT_CACHE_SIZE;



    // Methods
    // {

      /// \brief  Callback for processing loaded texture resources
      virtual void ProcessLoadedEvent(const BeString16& strFileName, BeVoidP pBufferData, BeInt iBufferSize, int iExtraParam1);

      /// \brief  Creates the device texture using the pre-loaded buffer data
      void CreateDeviceTextureFromBuffer(const BeString16& strFileName, BeVoidP pBufferData, BeInt iBufferSize);
      
      /// \brief  Assigns a texture to a cache texture slot
      void SetTextureIntoCacheSlot(BeInt iCacheHandle, BeTexture* pTexture);

    // }



    /// \brief  The texture cache
    typedef std::vector<BeRenderTypes::BeTextureCacheEntryPtr> BeTextureCacheArray;
    BeTextureCacheArray m_pTextureCacheArray;

    /*
    int              nextTextureSlot;
    int              currentTextureHandle;
    bool            textureClamp;
    int              kWhiteTexture;
    int              kBlackTexture;
    int              kPinkTexture;
    */
    
    /// \brief  The last chache index binded for each map
    BeInt m_iLastTextureMapsIdx[BeRenderTypes::MAT_TEXTURE_MAPS_COUNT];

    /// \brief  The Render device pointer
    BeRenderDevice* m_pRenderDevice;
      
};



#endif // #ifndef BE_BETEXTUREMANAGER_H

