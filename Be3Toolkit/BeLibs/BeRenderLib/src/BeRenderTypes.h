/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeRenderTypes.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BERENDERTYPES_H
#define BE_BERENDERTYPES_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>

// Forward declarations
class BeTexture;
class BeMaterial;
class BeTextureSampler;
class BeMaterialProgram;

/////////////////////////////////////////////////
/// \class BeRenderTypes
/// \brief Render project types
/////////////////////////////////////////////////
class RENDER_API BeRenderTypes
{
      
  public:

    /// \brief  Useful Render formats
    enum ERenderFormat
    {
      /// \brief  DirectX9          DirectX10              OpenGL
      RFORMAT_UNKNOWN = 0,
      
      /// \brief   D3DFMT_X8R8G8B8      DXGI_FORMAT_B8G8R8X8_UNORM_SRGB    GL_RGB8
      RFORMAT_R8G8B8 = ((8 << 24)+(8 << 16)+(8 << 8)),
      /// \brief   D3DFMT_A8R8G8B8      DXGI_FORMAT_R8G8B8A8_UNORM_SRGB    GL_RGBA8
      RFORMAT_R8G8B8A8 = ((8 << 24)+(8 << 16)+(8 << 8)+(8)),

      /// \brief   D3DFMT_X1R5G5B5      DXGI_FORMAT_B5G5R5A1_UNORM      GL_RGB5
      RFORMAT_R5G5B5 = ((5 << 24)+(5 << 16)+(5 << 8)),
      /// \brief   D3DFMT_A1R5G5B5      DXGI_FORMAT_B5G5R5A1_UNORM      GL_RGB5_A1
      RFORMAT_R5G5B5A1 = ((5 << 24)+(5 << 16)+(5 << 8)+(1)),

      /// \brief   D3DFMT_X4R4G4B4      Not Supported            GL_RGB4
      RFORMAT_R4G4B4 = ((4 << 24)+(4 << 16)+(4 << 8)),
      /// \brief   D3DFMT_A4R4G4B4      Not Supported            GL_RGBA4
      RFORMAT_R4G4B4A4 = ((4 << 24)+(4 << 16)+(4 << 8)+(4)),

      /// \brief   D3DFMT_R5G6B5        DXGI_FORMAT_B5G6R5_UNORM      Not Supported      
      RFORMAT_R5G6B5 = ((5 << 24)+(6 << 16)+(5 << 8)),

      /// \brief   D3DFMT_A2B10G10R10      DXGI_FORMAT_R10G10B10A2_TYPELESS  GL_RGB10
      RFORMAT_R10G10B10 = ((10 << 24)+(10 << 16)+(10 << 8)),
      /// \brief   D3DFMT_A2R10G10B10      DXGI_FORMAT_R10G10B10A2_UNORM    GL_RGB10_A2
      RFORMAT_R10G10B10A2 = ((10 << 24)+(10 << 16)+(10 << 8)+(2)),
            
      /// \brief   D3DFMT_A16B16G16R16F    DXGI_FORMAT_R16G16B16A16_FLOAT    GL_RGB16
      RFORMAT_R16G16B16 = ((16 << 24)+(16 << 16)+(16 << 8)),
      /// \brief   D3DFMT_A16B16G16R16    DXGI_FORMAT_R16G16B16A16_UNORM    GL_RGBA16
      RFORMAT_R16G16B16A16 = ((16 << 24)+(16 << 16)+(16 << 8)+(16))
    };

    /// \brief  Useful Depth stencil formats
    enum EDepthStencilFormat
    {
      /// \brief  DirectX9          DirectX10              OpenGL
      DSFORMAT_UNKNOWN = 0,
      
      /// \brief    D3DFMT_D32        DXGI_FORMAT_D32_FLOAT        GL_DEPTH_COMPONENT32F
      DSFORMAT_DEPTH_32 = ((32 << 8)),
      /// \brief    Not Supported        Not Supported            GL_DEPTH32F_STENCIL8
      DSFORMAT_DEPTH_32_S8 = ((32 << 8)+(8)),
      /// \brief    D3DFMT_D24X8        DXGI_FORMAT_D24_UNORM_S8_UINT    GL_DEPTH_COMPONENT24
      DSFORMAT_DEPTH_24 = ((24 << 8)),
      /// \brief    D3DFMT_D24S8        DXGI_FORMAT_D24_UNORM_S8_UINT    GL_DEPTH24_STENCIL8
      DSFORMAT_DEPTH_24_S8 = ((24 << 8)+(8)),
      /// \brief    D3DFMT_D16        Not Supported            GL_DEPTH_COMPONENT16
      DSFORMAT_DEPTH_16 = ((16 << 8)),
      /// \brief    Not Supported        Not Supported            GL_DEPTH16_STENCIL8
      DSFORMAT_DEPTH_16_S8 = ((16 << 8)+(8))
    };

    /// \brief  Render API enumerator
    enum ERenderDeviceAPI
    {
        RENDER_DEVICE_API_DX11,
        RENDER_DEVICE_API_GL,

        RENDER_DEVICE_UNKNOWN
    };

    /// \brief  Program constant config for creating
    class RENDER_API BeConstantConfig
    {
    public:

        BeString8 m_strName;
        BeString8 m_strType;

        ~BeConstantConfig(){}
    };

    /// \brief  Program attributes config for creating
    class RENDER_API BeAttributeConfig
    {
    public:

        BeString8 m_strName;
        BeString8 m_strType;
        BeString8 m_strSemantic;
        BeInt     m_iSemanticIndex;
        BeBoolean m_bNormalised;

        ~BeAttributeConfig(){}
    };

    /// \brief  Texture channel config for creating
    class RENDER_API BeTextureChannelConfig
    {
    public:

        BeString8 m_strName;
        BeString8 m_strTextureConstant;
        BeString8 m_strSamplerConstant;
        BeTextureSampler* m_pTextureSampler;

        ~BeTextureChannelConfig()
        {
            m_pTextureSampler = NULL;
        }
    };

    /// \brief  Material config for creating
    class RENDER_API BeMaterialConfig
    {
    public:

        BeString16 m_strName;

        BeMaterialProgram* m_pVSProgram;
        BeMaterialProgram* m_pPSProgram;

        BeMaterialConfig()
        :
            m_pVSProgram(NULL),
            m_pPSProgram(NULL)
        {
        }
        ~BeMaterialConfig()
        {
            m_pVSProgram = NULL;
            m_pPSProgram = NULL;
        }
    };

    /// \brief  Material texture channel
    class RENDER_API BeTextureChannel
    {
    public:

        BeString8           m_strTextureConstant;
        BeString8           m_strSamplerConstant;
        BeTextureSampler*   m_pTextureSampler;

        BeTextureChannel()
        :
            m_pTextureSampler(NULL)
        {
        }

        ~BeTextureChannel()
        {
            m_pTextureSampler = NULL;
        }
    };

    /// \brief  Material pipeline
    enum EBeMaterialPipeline
    {
        MATERIAL_PIPELINE_GEOMETRY = 0,
        MATERIAL_PIPELINE_VERTEX,
        MATERIAL_PIPELINE_PIXEL
    };

    /// \brief  Texture MAPs enumerator
    enum ETextureMaps
    {
      MAT_DIFFUSEMAP = 1,
      MAT_SPECULARCOLORMAP = 2,
      MAT_SPECULARLEVELMAP = 4,
      MAT_GLOSSINESSMAP = 8,
      MAT_SELFILLUMINATIONMAP = 16,
      MAT_OPACITYMAP = 32,
      MAT_FILTERCOLORMAP = 64,
      MAT_BUMPMAP = 128,
      MAT_REFLECTIONMAP = 256,
      MAT_REFRACTIONMAP = 512,
      MAT_DISPLACEMENTMAP = 1024
    };

    /// \brief  Texture MAPs enumerator
    enum ETextureMapsIdx
    {
      MAT_DIFFUSEMAP_IDX,
      MAT_SPECULARCOLORMAP_IDX,
      MAT_SPECULARLEVELMAP_IDX,
      MAT_GLOSSINESSMAP_IDX,
      MAT_SELFILLUMINATIONMAP_IDX,
      MAT_OPACITYMAP_IDX,
      MAT_FILTERCOLORMAP_IDX,
      MAT_BUMPMAP_IDX,
      MAT_REFLECTIONMAP_IDX,
      MAT_REFRACTIONMAP_IDX,
      MAT_DISPLACEMENTMAP_IDX,

      MAT_TEXTURE_MAPS_COUNT
    };

    /// \brief  Texture MAPs enumerator
    enum EDefaultTexturesSlot
    {
      WHITE_TEXTURE = 1,
      BLACK_TEXTURE = 2,
      PINK_TEXTURE = 3
    };
    
    /// \brief  Render Device Settings for creating the Render API Device
    class RENDER_API BeRenderDeviceSettings
    {

      public:

        /// \brief  Constructor
        BeRenderDeviceSettings();
        /// \brief  Copy Constructor
        BeRenderDeviceSettings(const BeRenderDeviceSettings& kRenderDeviceSettings);

        /// \brief Copy values
        BeRenderDeviceSettings &operator =(const BeRenderDeviceSettings &kRenderDeviceSettings);

        /// \brief  The device adapter
        BeUInt m_uAdapterOrdinal;
        /// \brief  The render device API
        ERenderDeviceAPI m_eRenderDeviceAPI;
        /// \brief  The application window name
        BeString16 m_strWindowName;

        /// \brief  Buffer Resolution Width
        BeUInt m_uBufferWidth;
        /// \brief  Buffer Resolution Height
        BeUInt m_uBufferHeight;
        /// \brief  Is Windowed?
        BeBoolean m_bWindowed;
        /// \brief  Vertical sync
        BeBoolean m_bVerticalSync;

        /// \brief  If TRUE the app starts normally. It is used only in special cases.
        BeBoolean m_bStartApp;

    };

    /// \brief  Render Device Display mode settings
    class RENDER_API BeRenderDeviceDisplayMode
    {

      public:

        /// \brief  Constructor
        BeRenderDeviceDisplayMode();
        /// \brief  Copy Constructor
        BeRenderDeviceDisplayMode(const BeRenderDeviceDisplayMode& kRenderDeviceDisplayMode);

        /// \brief Copy values
        BeRenderDeviceDisplayMode &operator =(const BeRenderDeviceDisplayMode &kRenderDeviceDisplayMode);

        /// \brief  Is Windowed?
        BeBoolean m_bWindowed;
        /// \brief  Resolution Width
        BeUInt m_uResWidth;
        /// \brief  Resolution Height
        BeUInt m_uResHeight;
        /// \brief  Format
        BeInt m_kFormat;
        /// \brief  Refresh rate
        BeShort m_iRefreshRate;
        /// \brief  Scanline ordering
        BeShort m_iScanlineOrdering;
        /// \brief  Scaling
        BeShort m_iScaling;

        /// \brief  Window app handle
        BeWindowHandle m_hWindowHandle;

    };
    typedef std::vector<BeRenderDeviceDisplayMode> BeEnumDisplayModesArray;

    /// \brief  Render Device Adapters with the available APIs and display modes
    class RENDER_API BeDeviceAdapter
    {

      public:

        /// \brief  Constructor
        BeDeviceAdapter();
        /// \brief  Destructor
        ~BeDeviceAdapter();


        /// \brief  Adapter name
        BeString16 m_strAdapterName;

        /// \brief  Contains if the render APIs are available
        BeBoolean m_kRenderAPIAvailable[BeRenderTypes::RENDER_DEVICE_UNKNOWN];

        /// \brief  The windowed display modes enumarated
        BeRenderTypes::BeEnumDisplayModesArray m_kEnumWindowedDisplayModes;

        /// \brief  The full screen display modes enumarated
        BeRenderTypes::BeEnumDisplayModesArray m_kEnumFullDisplayModes;

    };
    typedef std::vector<BeDeviceAdapter> BeEnumAdaptersArray;

    /// \brief  A texture chache entry
    class RENDER_API BeTextureCacheEntry : public BeMemoryObject
    {
      public:

        /// \brief  Constructor
        BeTextureCacheEntry();
        /// \brief  Destructor
        ~BeTextureCacheEntry();

        /// \brief  Release function
        void Release(void);



        /// \brief  Texture Name
        BeString16 m_strTextureName;
        
        /// \brief  Texture X Pixel Size
        BeInt m_xSize;
        /// \brief  Texture Y Pixel Size
        BeInt m_ySize;

        /// \brief  Texture Object
        BeTexture* m_pTexture;

        /// \brief  Texture X Pixel Size
        BeInt m_iSceneModelsUsingIt;

        /// \brief  The texture has been allocated
        BeBoolean m_bIsAllocated;
        /// \brief  The texture has been loaded
        BeBoolean m_bIsLoaded;
    };
    typedef BeSmartPointer<BeTextureCacheEntry> BeTextureCacheEntryPtr;

    /// \brief  A material chache entry
    class RENDER_API BeMaterialCacheEntry : public BeMemoryObject
    {
    public:

        /// \brief  Constructor
        BeMaterialCacheEntry();
        /// \brief  Destructor
        ~BeMaterialCacheEntry();

        /// \brief  Release function
        void Release(void);



        /// \brief  Material Name
        BeString16 m_strMaterialName;

        /// \brief  Material vertex shader filename
        BeString16 m_strVertexShaderFilename;

        /// \brief  Material pixel shader filename
        BeString16 m_strPixelShaderFilename;

        /// \brief  Material Object
        BeMaterial* m_pMaterial;

        /// \brief  The material has been allocated
        BeBoolean m_bIsAllocated;
        /// \brief  The material has been loaded and compiled
        BeBoolean m_bIsLoaded;
    };
    typedef BeSmartPointer<BeMaterialCacheEntry> BeMaterialCacheEntryPtr;

    /// \brief  A 2D Rect
    struct BeRect2D
    {
      BeInt m_iLeft;
      BeInt m_iTop;
      BeInt m_iRight;
      BeInt m_iBottom;
    };

    /// \brief  Render API enumerator
    enum EClearMask
    {
      CLEAR_TARGET = 0x01,
      CLEAR_STENCIL = 0x02,
      CLEAR_ZBUFFER = 0x04
    };

};

#endif // #ifndef BE_BERENDERTYPES_H
