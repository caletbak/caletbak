////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <BeGraphics.h>

#if TARGET_ANDROID
#include <BeEGLView.h>
#endif

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

BeThreadMutex* BeGraphics::s_pCriticalSection = NULL;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BeGraphics::Initialise()
{
    s_pCriticalSection = new BeThreadMutex();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BeGraphics::Deinitialise()
{
    if (s_pCriticalSection)
    {
        delete s_pCriticalSection;
        s_pCriticalSection = NULL;
    }
}

/////////////////////////////////////////////////////////////////////////////

bool BeGraphics::EnterCriticalSection(const BeString8& strCallingFrom)
{
    if (s_pCriticalSection)
    {
        if (strCallingFrom.GetSize() > 0)
        {
            BE_RENDER_DEBUG("UbiGraphics::EnterCriticalSection entering from: %s ...", strCallingFrom.ToRaw());
        }

        bool entered = s_pCriticalSection->Lock();

        if (entered)
        {
#           if TARGET_ANDROID
            //BE_RENDER_DEBUG("EnterCriticalSection MakeCurrent PUT TO SOMETHING");
            //BeEGLView::MakeCurrent(EGLView::s_EGLDisplay, EGLView::s_EGLWindow, EGLView::s_EGLWindow, EGLView::s_EGLContext);
#           endif

            if (strCallingFrom.GetSize() > 0)
            {
                BE_RENDER_DEBUG("UbiGraphics::EnterCriticalSection from: %s ENTERED", strCallingFrom.ToRaw());
            }
        }

        return entered;
    }

    return false;
}

/////////////////////////////////////////////////////////////////////////////

bool BeGraphics::ExitCriticalSection(const BeString8& strCallingFrom)
{
    if (s_pCriticalSection)
    {
        if (strCallingFrom.GetSize() > 0)
        {
            BE_RENDER_DEBUG("UbiGraphics::ExitCriticalSection exiting from: %s ...", strCallingFrom.ToRaw());
        }

#       if TARGET_ANDROID
            //BE_RENDER_DEBUG("ExitCriticalSection MakeCurrent TO NULL");
            //BeEGLView::MakeCurrent(EGLView::s_EGLDisplay, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
#       endif

        bool exited = s_pCriticalSection->UnLock();

        if (!exited)
        {
#           if TARGET_ANDROID
            //BE_RENDER_DEBUG("ExitCriticalSection MakeCurrent RETURN TO SOMETHING");
            //BeEGLView::MakeCurrent(EGLView::s_EGLDisplay, EGLView::s_EGLWindow, EGLView::s_EGLWindow, EGLView::s_EGLContext);
#           endif
        }
        else
        {
            if (strCallingFrom.GetSize() > 0)
            {
                BE_RENDER_DEBUG("UbiGraphics::ExitCriticalSection from: %s EXITED", strCallingFrom.ToRaw());
            }
        }

        return exited;
    }

    return false;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

BeGraphics::BeEGLGraphicsConfig::BeEGLGraphicsConfig()
{
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

BeGraphics::BeEGLGraphicsConfig::BeEGLGraphicsConfig(const BeGraphics::BeEGLGraphicsConfig &grapchisConfig)
{
    m_uEGLRenderableType = grapchisConfig.m_uEGLRenderableType;
    m_uEGLSurfaceType = grapchisConfig.m_uEGLSurfaceType;

    m_uEGLRedSize = grapchisConfig.m_uEGLRedSize;
    m_uEGLGreenSize = grapchisConfig.m_uEGLGreenSize;
    m_uEGLBlueSize = grapchisConfig.m_uEGLBlueSize;

    m_uEGLDepthSize = grapchisConfig.m_uEGLDepthSize;
    m_uEGLStencilSize = grapchisConfig.m_uEGLStencilSize;

    m_uScreenWidth = grapchisConfig.m_uScreenWidth;
    m_uScreenHeight = grapchisConfig.m_uScreenHeight;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

