/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeMaterialGL.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEMATERIALGL_H
#define BE_BEMATERIALGL_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>

#include <Managers/BeMaterialsManager.h>
#include <Render/BeMaterial.h>

// Forward declarations
class BeMaterialProgram;

/////////////////////////////////////////////////
/// \class BeMaterialGL
/// \brief GL Material object
/////////////////////////////////////////////////
class RENDER_API BeMaterialGL : public BeMaterial
{
      
  public:
    
    /// \brief Constructor
    BeMaterialGL();
    /// \brief Destructor
    ~BeMaterialGL();

    // Methods
    // {

        /// \brief  Prepares the material with its constant and so
        virtual void PrepareMaterial(BeRenderDevice* pRenderDevice);

        /// \brief  Make a request for updating a constant in the material
        virtual void RequestUpdateConstant(const BeString8& strConstantName, BeVoidP pValue);

        /// \brief  Updates the material and constants all together
        virtual void UpdateMaterial(BeRenderDevice* pRenderDevice);

        /// \brief  Sets the material constant buffers into the render context
        virtual void SetConstantBuffers(BeRenderDevice* pRenderDevice);

        /// \brief  Sets the material texture samplers into the render context
        virtual void SetTextureSamplers(BeRenderDevice* pRenderDevice);

    // }



    // Getters
    // {

        /// \brief  Returns the direct pointer to the material API impl
        virtual BeMaterialProgram* GetMaterialProgram(BeRenderTypes::EBeMaterialPipeline eMaterialStep);

        /// \brief  Returns the direct pointer to the material linked program API impl
        BeInt GetMaterialLinkedProgram(void);

    // }



    // Methods
    // {

        /// \brief  Sets the current material for rendering
        virtual void SetMaterialForRendering(BeVoidP pRenderContext);

        /// \brief  Sets the direct pointer to the material API impl
        virtual void SetMaterialProgram(BeMaterialProgram* pMaterialProgram, BeRenderTypes::EBeMaterialPipeline eMaterialStep);

        /// \brief  Sets the direct pointer to the material program linked
        void SetMaterialProgramLinked(BeUInt uProgramLinked);

    // }

  private:

    // Forward declarations
    class BeMaterialGLImpl;

    /// \brief  Render Device API Implementation
    BeMaterialGLImpl* m_pMaterialImpl;

};

#endif // #ifndef BE_BEMATERIALGL_H
