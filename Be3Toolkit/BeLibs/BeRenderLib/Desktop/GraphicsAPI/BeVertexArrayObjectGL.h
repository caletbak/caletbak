/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeVertexArrayObjectGL.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEVERTEXARRAYOBJECTGL_H
#define BE_BEVERTEXARRAYOBJECTGL_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>
#include <Render/BeVertexArrayObject.h>

/////////////////////////////////////////////////
/// \class BeVertexArrayObjectGL
/// \brief High-level VAO object
/////////////////////////////////////////////////
class RENDER_API BeVertexArrayObjectGL : public BeVertexArrayObject
{

  public:

      /// \brief Constructor
      BeVertexArrayObjectGL();
      /// \brief Destructor
      ~BeVertexArrayObjectGL();

      // Getters
      // {

          /// \brief  Returns the direct pointer to the vertex array object API impl
          virtual BeVoidP GetVertexArrayObjectPtr (void);

      // }



      // Setters
      // {

          /// \brief  Sets the direct pointer to the vertex array object API impl
          virtual void SetVertexArrayObjectPtr(BeVoidP pVAOPtr);

      // }



      // Methods
      // {

          /// \brief  Prepares the attributes for this VAO object
          virtual void PrepareVertexArrayObjectAttributes(BeMaterialVertexDefinition* pVertexDefinition);

          /// \brief  Enable/Disable all the attributes for this VAO object
          virtual void EnableVertexArrayObjectAttributes(BeMaterialVertexDefinition* pVertexDefinition, BeBoolean bEnable);

      // }

  private:

    // Forward declarations
    class BeVertexArrayObjectGLImpl;

    /// \brief  Render Device API Implementation
    BeVertexArrayObjectGLImpl* m_pVertexArrayObjectImpl;

};

#endif // #ifndef BE_BEVERTEXARRAYOBJECTGL_H
