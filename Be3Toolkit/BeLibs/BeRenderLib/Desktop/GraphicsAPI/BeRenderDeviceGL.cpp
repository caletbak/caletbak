﻿/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeRenderDeviceGL.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <GraphicsAPI/BeRenderDeviceGL.h>
#include <GraphicsAPI/BeTextureGL.h>
#include <GraphicsAPI/BeMaterialGL.h>
#include <GraphicsAPI/BeVertexBufferGL.h>
#include <GraphicsAPI/BeIndexBufferGL.h>
#include <GraphicsAPI/BeConstantBufferGL.h>
#include <GraphicsAPI/BeMaterialVertexDefinitionGL.h>
#include <GraphicsAPI/BeRenderStatesGL.h>
#include <GraphicsAPI/BeTextureSamplerGL.h>
#include <GraphicsAPI/BeMaterialProgramGL.h>
#include <GraphicsAPI/BeImageLoaderGL.h>
#include <GraphicsAPI/BeVertexArrayObjectGL.h>
#include <GraphicsAPI/BeErrorCheckGL.h>
#include <Managers/BeTextureManager.h>
#include <Managers/BeMaterialsManager.h>
#include <BeVertexDefinitions.h>



#if TARGET_WINDOWS

#include <FreeImage.h>

// OpenGL libs
#pragma comment (lib, "opengl32.lib")
#pragma comment (lib, "OpenGLEWLib.lib")

#include <windows.h>

#include <GL/glew.h>
#include <GL/wglew.h>

GLEWContext* s_kCurrentGLEWContext = NULL;
WGLEWContext* s_kCurrentWGLEWContext = NULL;

/////////////////////////////////////////////////////////////////////////////
class RENDER_API BeRenderDeviceGL::BeRenderDeviceGLImpl
{

  public:
  
    /// \brief  Constructor
    BeRenderDeviceGLImpl();
    /// \brief  Destructor
    ~BeRenderDeviceGLImpl();
    
    /// \brief  Release
    void Release(void);



    /// \brief  The render device settings
    BeRenderTypes::BeRenderDeviceSettings m_kRenderDeviceSettings;

    /// \brief  OpenGL Rendering Context 
    HGLRC m_hGLContext;

    /// \brief  Device context
    HDC m_hDeviceContext;

    /// \brief  The selected pixel format
    PIXELFORMATDESCRIPTOR m_kPixelFormatDesc;

    /// \brief  OpenGL GLEW MX Rendering Context 
    GLEWContext m_pGLGlewContext;

};
/////////////////////////////////////////////////////////////////////////////
BeRenderDeviceGL::BeRenderDeviceGLImpl::BeRenderDeviceGLImpl()
:
  m_hGLContext(NULL),
  m_hDeviceContext(NULL)
{
}
/////////////////////////////////////////////////////////////////////////////
BeRenderDeviceGL::BeRenderDeviceGLImpl::~BeRenderDeviceGLImpl()
{
  Release();
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderDeviceGL::BeRenderDeviceGLImpl::Release(void)
{
  ChangeDisplaySettings(NULL,0);             // If So Switch Back To The Desktop
  ShowCursor(BE_TRUE);                       // Show Mouse Pointer

  if ( m_hGLContext )                                // Do We Have A Rendering Context?
  {
    if (!wglMakeCurrent(NULL,NULL))                 // Are We Able To Release The DC And RC Contexts?
    {
      BE_RENDER_ERROR("Release Of DC And RC Failed.");
    }
    
    if (!wglDeleteContext(m_hGLContext))                 // Are We Able To Delete The RC?
    {
      BE_RENDER_ERROR("Release Rendering Context Failed.");
    }

    m_hGLContext = NULL;                           // Set RC To NULL
  }

  if (s_kCurrentGLEWContext)
  {
      BeDelete s_kCurrentGLEWContext;
      s_kCurrentGLEWContext = NULL;
  }
  if (s_kCurrentWGLEWContext)
  {
      BeDelete s_kCurrentWGLEWContext;
      s_kCurrentWGLEWContext = NULL;
  }

  m_hDeviceContext = NULL;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeRenderDeviceGL::BeRenderDeviceGLImpl* BeRenderDeviceGL::m_pRenderDeviceImpl = NULL;
BeRenderDeviceGLEnumerator* BeRenderDeviceGL::m_pDeviceEnumerator = NULL;
/////////////////////////////////////////////////////////////////////////////
BeRenderDeviceGL::BeRenderDeviceGL()
{
    m_eRenderAPIBeingUsed = BeRenderTypes::RENDER_DEVICE_API_GL;

    m_pRenderDeviceImpl = BeNew BeRenderDeviceGLImpl();

    BeImageLoaderGL::InitStaticResources();
}
/////////////////////////////////////////////////////////////////////////////
BeRenderDeviceGL::~BeRenderDeviceGL()
{
    BeRenderDevice::Release();

    if ( m_pRenderDeviceImpl != NULL )
    {
        BeDelete m_pRenderDeviceImpl;
        m_pRenderDeviceImpl = NULL;
    }

    if ( m_pDeviceEnumerator != NULL )
    {
        BeDelete m_pDeviceEnumerator;
        m_pDeviceEnumerator = NULL;
    }

    BeImageLoaderGL::DestroyStaticResources();
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeBoolean BeRenderDeviceGL::CreateTextureFromBuffer(BeRenderTypes::BeTextureCacheEntry* pCacheTextureEntry, BeVoidP pData, BeInt iDataSize)
{
    if ( pCacheTextureEntry->m_pTexture != NULL )
    {
        BeDelete pCacheTextureEntry->m_pTexture;
        pCacheTextureEntry->m_pTexture = NULL;
    }
    pCacheTextureEntry->m_pTexture = BeNew BeTextureGL();

    BeTextureGL* pTextureGL = static_cast<BeTextureGL*>(pCacheTextureEntry->m_pTexture);

    BeImageLoaderGL kImageLoader;
    kImageLoader.LoadFileImage(pData, iDataSize);

    if (kImageLoader.GetImageObjectAPI())
    {
        pCacheTextureEntry->m_xSize = kImageLoader.GetImageWidth();
        pCacheTextureEntry->m_ySize = kImageLoader.GetImageHeight();

        BeVector4D* pPixels = kImageLoader.GeneratePixelArray();

        BeInt iTotalBytes = pCacheTextureEntry->m_xSize * pCacheTextureEntry->m_ySize * 4;

        GLuint uNewTexture;
        glGenTextures(1, &uNewTexture);

        CheckGLError();

        glBindTexture(GL_TEXTURE_2D, uNewTexture);

        glTexImage2D(GL_TEXTURE_2D,
            GLint(0),
            GL_RGBA,
            GLsizei(pCacheTextureEntry->m_xSize),
            GLsizei(pCacheTextureEntry->m_ySize),
            0,
            GL_RGBA,
            GL_FLOAT,
            pPixels);

        CheckGLError();

        glGenerateMipmap(GL_TEXTURE_2D);

        CheckGLError();

        glBindTexture(GL_TEXTURE_2D, 0);

        pTextureGL->SetTexturePtr(&uNewTexture);
    }
    else
    {
        BeDelete pCacheTextureEntry->m_pTexture;
        pCacheTextureEntry->m_pTexture = NULL;

        BE_RENDER_ERROR("ERROR CreateTextureFromBuffer %s !!", ConvertString16To8(pCacheTextureEntry->m_strTextureName).ToRaw());
        return BE_FALSE;
    }

    return BE_TRUE;
}
/////////////////////////////////////////////////////////////////////////////
const BeRenderTypes::BeRenderDeviceSettings& BeRenderDeviceGL::GetRenderDeviceSettings(void)
{
    return m_pRenderDeviceImpl->m_kRenderDeviceSettings;
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeRenderDeviceGL::CreateMaterialFromConfig(BeRenderTypes::BeMaterialCacheEntry* pCacheMaterialEntry, const BeRenderTypes::BeMaterialConfig& kMaterialConfig)
{
    BE_UNUSED(pCacheMaterialEntry);
    BE_UNUSED(kMaterialConfig);

    if (!m_pRenderDeviceImpl->m_hGLContext)
        return BE_FALSE;

    if (pCacheMaterialEntry->m_pMaterial != NULL)
    {
        BeDelete pCacheMaterialEntry->m_pMaterial;
        pCacheMaterialEntry->m_pMaterial = NULL;
    }
    pCacheMaterialEntry->m_pMaterial = BeNew BeMaterialGL();

    BeMaterialGL* pMaterialGL = static_cast<BeMaterialGL*>(pCacheMaterialEntry->m_pMaterial);

    pMaterialGL->SetMaterialProgram(kMaterialConfig.m_pVSProgram, BeRenderTypes::MATERIAL_PIPELINE_VERTEX);
    pMaterialGL->SetMaterialProgram(kMaterialConfig.m_pPSProgram, BeRenderTypes::MATERIAL_PIPELINE_PIXEL);

    if (kMaterialConfig.m_pVSProgram && kMaterialConfig.m_pPSProgram)
    {
        GLuint pMaterialProgram = glCreateProgram();

        CheckGLError();

        GLuint uVS = *((GLuint*) kMaterialConfig.m_pVSProgram->GetMaterialProgramPtr());
        GLuint uPS = *((GLuint*) kMaterialConfig.m_pPSProgram->GetMaterialProgramPtr());

        glAttachShader(pMaterialProgram, uVS);

        CheckGLError();

        glAttachShader(pMaterialProgram, uPS);

        CheckGLError();

        glLinkProgram(pMaterialProgram);

        GLint isLinked = 0;
        glGetProgramiv(pMaterialProgram, GL_LINK_STATUS, (int *)&isLinked);
        if (isLinked == GL_FALSE)
        {
            GLint maxLength = 0;
            glGetProgramiv(pMaterialProgram, GL_INFO_LOG_LENGTH, &maxLength);

            std::vector<GLchar> infoLog(maxLength);
            glGetProgramInfoLog(pMaterialProgram, maxLength, &maxLength, &infoLog[0]);

            glDeleteProgram(pMaterialProgram);

            BE_RENDER_ERROR("Material linkage failed with log: \n\n%s", infoLog.data());

            return BE_FALSE;
        }

        glDetachShader(pMaterialProgram, uVS);
        glDetachShader(pMaterialProgram, uPS);

        CheckGLError();

        pMaterialGL->SetMaterialProgramLinked(pMaterialProgram);
    }

    return BE_TRUE;
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderDeviceGL::CreateRenderStates(void)
{
    m_pRenderStates = new BeRenderStatesGL(this);
}
/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
LRESULT CALLBACK BeRenderDeviceGL::WndProcGL(BeWindowHandle hWnd, BeUInt uMessage, WPARAM wParam, LPARAM lParam)
{
  return BeRenderDevice::WndProc(hWnd, uMessage, wParam, lParam);
}
/////////////////////////////////////////////////////////////////////////////
LRESULT CALLBACK BeRenderDeviceGL::WndProcCheckGL(BeWindowHandle hWnd, BeUInt uMessage, WPARAM wParam, LPARAM lParam)
{
  switch(uMessage)
  {
    case WM_CLOSE:
    case WM_DESTROY:
      return 0;
  }
  return BeRenderDevice::WndProc(hWnd, uMessage, wParam, lParam);
}
/////////////////////////////////////////////////////////////////////////////
BeInt BeRenderDeviceGL::ConvertRenderFormatToAPIFormat(const BeRenderTypes::ERenderFormat eRenderFormat)
{
  switch(eRenderFormat)
  {
    case BeRenderTypes::RFORMAT_UNKNOWN:
      return 0;

    case BeRenderTypes::RFORMAT_R8G8B8:
      return GL_RGB8;
    case BeRenderTypes::RFORMAT_R8G8B8A8:
      return GL_RGBA8;

    case BeRenderTypes::RFORMAT_R5G5B5:
      return GL_RGB5;
    case BeRenderTypes::RFORMAT_R5G5B5A1:
      return GL_RGB5_A1;

    case BeRenderTypes::RFORMAT_R4G4B4:
      return GL_RGB4;
    case BeRenderTypes::RFORMAT_R4G4B4A4:
      return GL_RGBA4;

    case BeRenderTypes::RFORMAT_R5G6B5:
      return 0;

    case BeRenderTypes::RFORMAT_R10G10B10:
      return GL_RGB10;
    case BeRenderTypes::RFORMAT_R10G10B10A2:
      return GL_RGB10_A2;
            
    case BeRenderTypes::RFORMAT_R16G16B16:
      return GL_RGB16;
    case BeRenderTypes::RFORMAT_R16G16B16A16:
      return GL_RGBA16;
  }

  return 0;
}
/////////////////////////////////////////////////////////////////////////////
BeInt BeRenderDeviceGL::ConvertDepthStencilFormatToAPIFormat(const BeRenderTypes::EDepthStencilFormat eDepthStencilFormat)
{
  switch(eDepthStencilFormat)
  {
    case BeRenderTypes::DSFORMAT_UNKNOWN:
      return 0;

    case BeRenderTypes::DSFORMAT_DEPTH_32:
      return GL_DEPTH_COMPONENT32F;
    case BeRenderTypes::DSFORMAT_DEPTH_32_S8:
      return GL_DEPTH_COMPONENT32F;
    case BeRenderTypes::DSFORMAT_DEPTH_24:
      return GL_DEPTH_COMPONENT24;
    case BeRenderTypes::DSFORMAT_DEPTH_24_S8:
      return GL_DEPTH24_STENCIL8;
    case BeRenderTypes::DSFORMAT_DEPTH_16:
      return GL_DEPTH_COMPONENT16;
  }

  return 0;
}
/////////////////////////////////////////////////////////////////////////////
BeRenderTypes::ERenderFormat BeRenderDeviceGL::ConvertPixelFormatDescToRenderFormat(const BeVoidP pPixelFormatDesc)
{
  const PIXELFORMATDESCRIPTOR* pPixelFormat = static_cast<const PIXELFORMATDESCRIPTOR*>(pPixelFormatDesc);

  BeInt iFinalFormat = 0;
  
  iFinalFormat += (pPixelFormat->cRedBits << 24);
  iFinalFormat += (pPixelFormat->cGreenBits << 16);
  iFinalFormat += (pPixelFormat->cBlueBits << 8);
  iFinalFormat += (pPixelFormat->cAlphaBits);

  return (BeRenderTypes::ERenderFormat)iFinalFormat;
}
/////////////////////////////////////////////////////////////////////////////
BeRenderTypes::EDepthStencilFormat BeRenderDeviceGL::ConvertPixelFormatDescToDepthStencilFormat(const BeVoidP pPixelFormatDesc)
{
  const PIXELFORMATDESCRIPTOR* pPixelFormat = static_cast<const PIXELFORMATDESCRIPTOR*>(pPixelFormatDesc);

  BeInt iFinalFormat = 0;
  
  iFinalFormat += (pPixelFormat->cDepthBits << 8);
  iFinalFormat += (pPixelFormat->cStencilBits);
  
  return (BeRenderTypes::EDepthStencilFormat)iFinalFormat;
}
/////////////////////////////////////////////////////////////////////////////
BeInt BeRenderDeviceGL::CheckNumAdapters(void)
{

  DISPLAY_DEVICE  dd;
  dd.cb = sizeof(DISPLAY_DEVICE);
  
  BeUInt uNumAdapters = 0;
  BeInt iDevice = 0;
  while ( EnumDisplayDevices(0, iDevice, &dd, 0) )
  {
    DISPLAY_DEVICE ddMon;
    ZeroMemory(&ddMon, sizeof(ddMon));
    ddMon.cb = sizeof(ddMon);
    DWORD devMon = 0;

    if ( EnumDisplayDevices(dd.DeviceName, devMon, &ddMon, 0) )
      uNumAdapters++;
  
    ZeroMemory(&dd, sizeof(dd));
    dd.cb = sizeof(dd);
    iDevice++; 
  }
    
  return uNumAdapters;
  
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeRenderDeviceGL::CheckGL(BeUInt uAdapterOrdinal)
{

  BE_UNUSED(uAdapterOrdinal);
  HDC  hDC = NULL;

  if ( !BeRenderDevice::CreateAppWindow(L"CheckGL", 640, 480, 32, BE_FALSE, WndProcCheckGL) )
    return BE_FALSE;

  /* What is the highest pixel format?... */
  hDC = GetDC(BeRenderDevice::m_hWindow);
  if ( !hDC )
  {
    BeRenderDevice::DestroyAppWindow();
    return BE_FALSE;
  }

  BeUInt uPixelFormat;
  static PIXELFORMATDESCRIPTOR pfd =        // pfd Tells Windows How We Want Things To Be
    {
      sizeof(PIXELFORMATDESCRIPTOR),        // Size Of This Pixel Format Descriptor
      1,                      // Version Number
      PFD_DRAW_TO_WINDOW |            // Format Must Support Window
      PFD_SUPPORT_OPENGL |            // Format Must Support OpenGL
      PFD_DOUBLEBUFFER,              // Must Support Double Buffering
      PFD_TYPE_RGBA,                // Request An RGBA Format
      32,                      // Select Our Color Depth
      0, 0, 0, 0, 0, 0,              // Color Bits Ignored
      0,                      // No Alpha Buffer
      0,                      // Shift Bit Ignored
      0,                      // No Accumulation Buffer
      0, 0, 0, 0,                  // Accumulation Bits Ignored
      32,                      // 16Bit Z-Buffer (Depth Buffer)  
      0,                      // No Stencil Buffer
      0,                      // No Auxiliary Buffer
      PFD_MAIN_PLANE,                // Main Drawing Layer
      0,                      // Reserved
      0, 0, 0                    // Layer Masks Ignored
    };
  
  uPixelFormat = ChoosePixelFormat( hDC, &pfd );
    if ( !uPixelFormat )  // Did Windows Find A Matching Pixel Format?
  {
    BeRenderDevice::DestroyAppWindow();
      return BE_FALSE;                // Return FALSE
  }

    if( !SetPixelFormat( hDC, uPixelFormat, &pfd ) )    // Are We Able To Set The Pixel Format?
  {
    BeRenderDevice::DestroyAppWindow();
      return BE_FALSE;                // Return FALSE
  }

  HGLRC tempOpenGLContext = wglCreateContext(hDC); // Create an OpenGL 2.1 context for our device context
  if ( tempOpenGLContext == NULL )
  {
    BeRenderDevice::DestroyAppWindow();
      return BE_FALSE;
  }
  wglDeleteContext(tempOpenGLContext); // Delete the temporary OpenGL 2.1 context

  ReleaseDC(NULL, hDC);
  BeRenderDevice::DestroyAppWindow();
  return BE_TRUE;
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeRenderDeviceGL::EnumerateDisplayModes(BeUInt uAdapterOrdinal, BeRenderTypes::BeDeviceAdapter& kDeviceAdapter)
{

  if ( m_pDeviceEnumerator != NULL )
  {
    BeDelete m_pDeviceEnumerator;
    m_pDeviceEnumerator = NULL;
  }

  m_pDeviceEnumerator = BeNew BeRenderDeviceGLEnumerator();
  if ( m_pDeviceEnumerator->ExecuteEnumerationProcess() )
  {
    m_pDeviceEnumerator->FillDisplayModesArray(uAdapterOrdinal, kDeviceAdapter);
  }
  else
  {
    BeDelete m_pDeviceEnumerator;
    m_pDeviceEnumerator = NULL;
    return BE_FALSE;
  }

  if ( m_pDeviceEnumerator != NULL )
  {
    BeDelete m_pDeviceEnumerator;
    m_pDeviceEnumerator = NULL;
  }

  return BE_TRUE;
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeRenderDeviceGL::CreateGLContext(void)
{

  BeInt    iPixelFormat;      // Holds The Results After Searching For A Match
  
  //Set up DX Present Parameters
  //--------------------------------------------------------------
  ZeroMemory(&m_pRenderDeviceImpl->m_kPixelFormatDesc, sizeof(m_pRenderDeviceImpl->m_kPixelFormatDesc));
  m_pRenderDeviceImpl->m_kPixelFormatDesc.nSize = sizeof(PIXELFORMATDESCRIPTOR);

  BeRenderTypes::BeRenderDeviceDisplayMode kRenderDeviceDisplayMode;
  kRenderDeviceDisplayMode.m_hWindowHandle = m_hWindow;
  if ( m_pDeviceEnumerator == NULL )
  {
    m_pDeviceEnumerator = BeNew BeRenderDeviceGLEnumerator();
    if ( !m_pDeviceEnumerator->ExecuteEnumerationProcess() )
      return BE_FALSE;
  }
  m_pDeviceEnumerator->FindBestDisplayMode( m_pRenderDeviceImpl->m_kRenderDeviceSettings.m_uAdapterOrdinal, m_pRenderDeviceImpl->m_kRenderDeviceSettings, &kRenderDeviceDisplayMode );
  if ( m_pRenderDeviceImpl->m_kRenderDeviceSettings.m_uBufferWidth != kRenderDeviceDisplayMode.m_uResWidth || m_pRenderDeviceImpl->m_kRenderDeviceSettings.m_uBufferHeight != kRenderDeviceDisplayMode.m_uResHeight )
  {
    SetWindowLongPtr(m_hWindow, GWL_WNDPROC, (LONG_PTR)WndProcCheckGL );
    DestroyAppWindow();
    if ( !CreateAppWindow(m_pRenderDeviceImpl->m_kRenderDeviceSettings.m_strWindowName, kRenderDeviceDisplayMode.m_uResWidth, kRenderDeviceDisplayMode.m_uResHeight, 32, !m_pRenderDeviceImpl->m_kRenderDeviceSettings.m_bWindowed, WndProcGL) )
    {
      m_pRenderDeviceImpl->Release();          // Reset The Display
        BE_RENDER_ERROR("Can't Create A new resized window.");
        return BE_FALSE;                // Return FALSE
    }
  }



  m_pRenderDeviceImpl->m_hDeviceContext = GetDC(m_hWindow);
    if ( !m_pRenderDeviceImpl->m_hDeviceContext )    // Did We Get A Device Context?
    {
      m_pRenderDeviceImpl->Release();          // Reset The Display
      BE_RENDER_ERROR("Can't Create A GL Device Context.");
      return BE_FALSE;                // Return FALSE
    }
  
  iPixelFormat = m_pDeviceEnumerator->FillPixelFormat( m_pRenderDeviceImpl->m_hDeviceContext, &m_pRenderDeviceImpl->m_kPixelFormatDesc );
  if ( iPixelFormat == -1 )
  {
    m_pRenderDeviceImpl->Release();          // Reset The Display
      BE_RENDER_ERROR("Can't choose a PixelFormat.");
      return BE_FALSE;    
  }

    if( !SetPixelFormat( m_pRenderDeviceImpl->m_hDeviceContext, iPixelFormat, &m_pRenderDeviceImpl->m_kPixelFormatDesc ) )    // Are We Able To Set The Pixel Format?
    {
      m_pRenderDeviceImpl->Release();          // Reset The Display
      BE_RENDER_ERROR("Can't Set The PixelFormat.");
      return BE_FALSE;                // Return FALSE
    }

  HGLRC tempOpenGLContext = wglCreateContext(m_pRenderDeviceImpl->m_hDeviceContext); // Create an OpenGL 2.1 context for our device context
  wglMakeCurrent(m_pRenderDeviceImpl->m_hDeviceContext, tempOpenGLContext); // Make the OpenGL 2.1 context current and active

  s_kCurrentGLEWContext = BeNew GLEWContext();
  s_kCurrentWGLEWContext = BeNew WGLEWContext();

  glewExperimental = true;

  GLenum error = glewInit(); // Enable GLEW
  if (error != GLEW_OK) // If GLEW fails
      return BE_FALSE;

  error = wglewInit(); // Enable GLEW
  if (error != GLEW_OK) // If GLEW fails
      return BE_FALSE;

  CheckGLError();

  BeInt attributes[] =
  {
    WGL_CONTEXT_MAJOR_VERSION_ARB, 4, // Set the MAJOR version of OpenGL to 4
    WGL_CONTEXT_MINOR_VERSION_ARB, 2, // Set the MINOR version of OpenGL to 2
    //WGL_CONTEXT_FLAGS_ARB, WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB, // Set our OpenGL context to be forward compatible
    0
  };
  
  if (wglewIsSupported("WGL_ARB_create_context") == 1) // If the OpenGL 4.x context creation extension is available
  { 
    m_pRenderDeviceImpl->m_hGLContext = wglCreateContextAttribsARB(m_pRenderDeviceImpl->m_hDeviceContext, NULL, attributes); // Create and OpenGL 3.x context based on the given attributes
    
    if ( m_pRenderDeviceImpl->m_hGLContext != NULL )
    {
        wglMakeCurrent(NULL, NULL); // Remove the temporary context from being active
        wglDeleteContext(tempOpenGLContext); // Delete the temporary OpenGL 2.1 context

        BeDelete s_kCurrentGLEWContext; s_kCurrentGLEWContext = NULL;
        BeDelete s_kCurrentWGLEWContext; s_kCurrentWGLEWContext = NULL;

      wglMakeCurrent(m_pRenderDeviceImpl->m_hDeviceContext, m_pRenderDeviceImpl->m_hGLContext); // Make our OpenGL 3.0 context current

      s_kCurrentGLEWContext = BeNew GLEWContext();
      s_kCurrentWGLEWContext = BeNew WGLEWContext();

      glewExperimental = true;

      error = glewInit(); // Enable GLEW
      if (error != GLEW_OK) // If GLEW fails
          return BE_FALSE;

      error = wglewInit(); // Enable GLEW
      if (error != GLEW_OK) // If GLEW fails
          return BE_FALSE;

      CheckGLError();
    }
    else
      m_pRenderDeviceImpl->m_hGLContext = tempOpenGLContext; // If we didn't have support for OpenGL 3.x and up, use the OpenGL 2.1 context
  }
  else
    m_pRenderDeviceImpl->m_hGLContext = tempOpenGLContext; // If we didn't have support for OpenGL 3.x and up, use the OpenGL 2.1 context


  // VSync
  wglSwapIntervalEXT(m_pRenderDeviceImpl->m_kRenderDeviceSettings.m_bVerticalSync ? 1 : 0);

  BeInt glVersion[2] = {-1, -1}; // Set some default values for the version
  glGetIntegerv(GL_MAJOR_VERSION, &glVersion[0]); // Get back the OpenGL MAJOR version we are using
  glGetIntegerv(GL_MINOR_VERSION, &glVersion[1]); // Get back the OpenGL MAJOR version we are using
  
  BE_RENDER_DEBUG("Using OpenGL: %d.%d", glVersion[0], glVersion[1]); // Output which version of OpenGL we are using
    
  return BE_TRUE; // We have successfully created a context, return true
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeRenderDeviceGL::IsGLExtensionAvailable(BeString8 strGLExtension)
{
    GLint iExtensionsCount;
    glGetIntegerv(GL_NUM_EXTENSIONS, &iExtensionsCount);

    for (int i = 0; i < iExtensionsCount; i++)
    {
        const char* pExtensionName = reinterpret_cast<const char*>(glGetStringi(GL_EXTENSIONS, i));
        if (strGLExtension.Compare(pExtensionName) == 0)
        {
            return BE_TRUE;
        }
    }

    return BE_FALSE;
}
/////////////////////////////////////////////////////////////////////////////
GLEWContext* BeRenderDeviceGL::GetGLEWContext(void)
{
    return s_kCurrentGLEWContext;
}
/////////////////////////////////////////////////////////////////////////////
WGLEWContext* BeRenderDeviceGL::GetWGLEWContext(void)
{
    return s_kCurrentWGLEWContext;
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeRenderDeviceGL::CreateRenderDevice(const BeRenderTypes::BeRenderDeviceSettings& kRenderDeviceSettings)
{
  m_pRenderDeviceImpl->m_kRenderDeviceSettings = kRenderDeviceSettings;

  // Create our App window
  if ( CreateAppWindow(m_pRenderDeviceImpl->m_kRenderDeviceSettings.m_strWindowName, m_pRenderDeviceImpl->m_kRenderDeviceSettings.m_uBufferWidth, m_pRenderDeviceImpl->m_kRenderDeviceSettings.m_uBufferHeight, 32, !m_pRenderDeviceImpl->m_kRenderDeviceSettings.m_bWindowed, WndProcGL) )
  {

    // Create our OpenGL rendering context
    CreateGLContext();

    ShowWindow(m_hWindow,SW_SHOW);            // Show The Window
    SetForegroundWindow(m_hWindow);            // Slightly Higher Priority
    SetFocus(m_hWindow);                // Sets Keyboard Focus To The Window

    //is WGL_EXT_swap_control in the string? VSync switch possible?
    if (IsGLExtensionAvailable("WGL_EXT_swap_control"))
    {
      //get address's of both functions and save them
      PFNWGLSWAPINTERVALEXTPROC pfGLSwapIntervalEXT = (PFNWGLSWAPINTERVALEXTPROC) wglGetProcAddress("wglSwapIntervalEXT");
      if ( kRenderDeviceSettings.m_bVerticalSync )
        pfGLSwapIntervalEXT(BE_TRUE);
      else
        pfGLSwapIntervalEXT(BE_FALSE);

    }
      
    return BE_TRUE;

  }

  return BE_FALSE;
  
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderDeviceGL::BeginScene (const BeMatrix& kViewProjMatrix)
{
    BE_UNUSED(kViewProjMatrix);

    m_mSceneViewProjectionMatrix = kViewProjMatrix;
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderDeviceGL::EndScene (void)
{
  //
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderDeviceGL::Clear (const BeRenderTypes::EClearMask clearMask, const BeVector4D& clearColour, const BeFloat clearZValue, const BeInt clearStencilValue)
{
  BeInt glFlags = 0;
  if (clearMask & BeRenderTypes::CLEAR_TARGET)
  {
    glFlags |= GL_COLOR_BUFFER_BIT;
    glClearColor (clearColour.m_fX, clearColour.m_fY, clearColour.m_fZ, clearColour.m_fW);
  }
  if (clearMask & BeRenderTypes::CLEAR_STENCIL)
  {
    glFlags |= GL_STENCIL_BUFFER_BIT;
    glClearStencil (clearStencilValue);
  }
  if (clearMask & BeRenderTypes::CLEAR_ZBUFFER)
  {
    glFlags |= GL_DEPTH_BUFFER_BIT;
    glClearDepthf (clearZValue);
  }

  glClear(glFlags);
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeRenderDeviceGL::Present(void)
{
  BeInt iFailed = SwapBuffers(m_pRenderDeviceImpl->m_hDeviceContext); // Swap buffers so we can see our rendering
  BeBoolean bFailed( iFailed == 0 );
    
  if ( bFailed )
    BE_RENDER_WARNING("Render Device Present Failed !");
      
  return bFailed;

}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeRenderDeviceGL::SetViewport(const BeViewport& kViewport)
{
    m_kCurrentViewport = kViewport;

    glViewport((BeInt)kViewport.m_fTopLeftX, (BeInt)kViewport.m_fTopLeftY, (BeInt)kViewport.m_fWidth, (BeInt)kViewport.m_fHeight);
    glDepthRangef(kViewport.m_fMinDepth, kViewport.m_fMaxDepth);

    CheckGLError();
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeVertexArrayObject* BeRenderDeviceGL::CreateVertexArrayObject(void)
{
    CheckGLError();

    GLuint uNewVAO;

    glGenVertexArrays(1, &uNewVAO);

    if (CheckGLError())
    {
        BE_RENDER_WARNING("Render Device could not create the vertex array object!");
        return NULL;
    }

    BeVertexArrayObjectGL* pNewVertexArrayObject = BeNew BeVertexArrayObjectGL();
    pNewVertexArrayObject->SetVertexArrayObjectPtr(&uNewVAO);

    return pNewVertexArrayObject;
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderDeviceGL::SetVertexArrayObject(BeVertexArrayObject* pVertexArrayObject)
{
    if (pVertexArrayObject)
    {
        if (m_pCurrentVertexArrayObjectSet != pVertexArrayObject)
        {
            GLuint pVertexArrayObjectAPI = *(reinterpret_cast<GLuint*>(pVertexArrayObject->GetVertexArrayObjectPtr()));

            glBindVertexArray(pVertexArrayObjectAPI);

            m_pCurrentVertexArrayObjectSet = pVertexArrayObject;
        }
    }
    else
    {
        glBindVertexArray(0);

        m_pCurrentVertexArrayObjectSet = NULL;
    }
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderDeviceGL::FreeVertexArrayObject(BeVertexArrayObject* pVertexArrayObject)
{
    BeVertexArrayObjectGL* pVertexArrayObjectAPI = reinterpret_cast<BeVertexArrayObjectGL*>(pVertexArrayObject);

    BeDelete pVertexArrayObjectAPI;
}
/////////////////////////////////////////////////////////////////////////////
BeVertexBuffer* BeRenderDeviceGL::CreateVertexBuffer(BeVoidP pMeshVertices, BeSize_T iVertexStructSize, BeUInt uNumVertices, const BeString8& strVSProgram, BeVertexArrayObject* pVAO)
{
    CheckGLError();

    GLuint uNewVBO;

    glGenBuffers(1, &uNewVBO);

    glBindBuffer(GL_ARRAY_BUFFER, uNewVBO);

    glBufferData(GL_ARRAY_BUFFER, iVertexStructSize * uNumVertices, pMeshVertices, GL_STATIC_DRAW);

    if (pVAO)
    {
        BeMaterialProgram* pUsedVSProgram = m_pMaterialsManager->GetMaterialProgramByName(strVSProgram);
        if (pUsedVSProgram)
        {
            BeMaterialVertexDefinition* pUsedVertexDefinition = pUsedVSProgram->GetVertexDefinition();

            if (!pVAO->HasAttributesSet())
            {
                pVAO->PrepareVertexArrayObjectAttributes(pUsedVertexDefinition);
            }

            pVAO->EnableVertexArrayObjectAttributes(pUsedVertexDefinition, true);
        }
    }

    if (CheckGLError())
    {
        glDeleteBuffers(1, &uNewVBO);

        BE_RENDER_WARNING("Render Device could not create the vertex buffer!");
        return NULL;
    }

    BeVertexBufferGL* pNewVertexBuffer = BeNew BeVertexBufferGL();
    pNewVertexBuffer->SetVertexBufferPtr(&uNewVBO, iVertexStructSize);

    return pNewVertexBuffer;
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderDeviceGL::SetVertexBuffer(BeVertexBuffer* pVertexBuffer)
{
    if (pVertexBuffer)
    {
        if (m_pCurrentVertexBufferSet != pVertexBuffer)
        {
            GLuint pVertexBufferAPI = *(reinterpret_cast<GLuint*>(pVertexBuffer->GetVertexBufferPtr()));

            glBindBuffer(GL_ARRAY_BUFFER, pVertexBufferAPI);

            m_pCurrentVertexBufferSet = pVertexBuffer;
        }
    }
    else
    {
        m_pCurrentVertexBufferSet = NULL;

        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderDeviceGL::FreeVertexBuffer(BeVertexBuffer* pVertexBuffer)
{
    BeVertexBufferGL* pVertexBufferAPI = reinterpret_cast<BeVertexBufferGL*>(pVertexBuffer);

    if (m_pCurrentVertexBufferSet == pVertexBuffer)
    {
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }

    BeDelete pVertexBufferAPI;
}
/////////////////////////////////////////////////////////////////////////////
BeIndexBuffer* BeRenderDeviceGL::CreateIndexBuffer(BeVoidP pMeshIndices, BeUInt uNumTriangles)
{
    CheckGLError();

    GLuint uNewIBO;

    glGenBuffers(1, &uNewIBO);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, uNewIBO);

    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(BeUShort) * (uNumTriangles * 3), pMeshIndices, GL_STATIC_DRAW);

    if (CheckGLError())
    {
        glDeleteBuffers(1, &uNewIBO);

        BE_RENDER_WARNING("Render Device could not create the index buffer!");
        return NULL;
    }

    BeIndexBufferGL* pNewIndexBuffer = BeNew BeIndexBufferGL();
    pNewIndexBuffer->SetIndexBufferPtr(&uNewIBO, uNumTriangles * 3);

    return pNewIndexBuffer;
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderDeviceGL::SetIndexBuffer(BeIndexBuffer* pIndexBuffer)
{
    if (pIndexBuffer)
    {
        if (m_pCurrentIndexBufferSet != pIndexBuffer)
        {
            GLuint pIndexBufferAPI = *(reinterpret_cast<GLuint*>(pIndexBuffer->GetIndexBufferPtr()));

            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, pIndexBufferAPI);

            m_pCurrentIndexBufferSet = pIndexBuffer;
        }
    }
    else
    {
        m_pCurrentIndexBufferSet = NULL;

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    }
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderDeviceGL::FreeIndexBuffer(BeIndexBuffer* pIndexBuffer)
{
    BeIndexBufferGL* pIndexBufferAPI = reinterpret_cast<BeIndexBufferGL*>(pIndexBuffer);

    if (m_pCurrentIndexBufferSet == pIndexBuffer)
    {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    }

    BeDelete pIndexBufferAPI;
}
/////////////////////////////////////////////////////////////////////////////
BeConstantBuffer* BeRenderDeviceGL::CreateConstantBuffer(BeVoidP pConstantBuffer, BeUInt uConstantBufferSize)
{
    CheckGLError();

    GLuint uNewUBO;

    glGenBuffers(1, &uNewUBO);

    glBindBuffer(GL_UNIFORM_BUFFER, uNewUBO);

    glBufferData(GL_UNIFORM_BUFFER, uConstantBufferSize, pConstantBuffer, GL_DYNAMIC_DRAW);

    glBindBuffer(GL_UNIFORM_BUFFER, 0);

    if (CheckGLError())
    {
        glDeleteBuffers(1, &uNewUBO);

        BE_RENDER_WARNING("Render Device could not create the constant buffer!");
        return NULL;
    }

    BeConstantBufferGL* pNewConstantBuffer = BeNew BeConstantBufferGL();
    pNewConstantBuffer->SetConstantBufferPtr(&uNewUBO);

    return pNewConstantBuffer;
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderDeviceGL::UpdateConstantBuffer(BeConstantBuffer* pConstantBuffer, BeVoidP pBuffer, BeUInt uBufferSize)
{
    BeConstantBufferGL* pConstantBufferGL = reinterpret_cast<BeConstantBufferGL*>(pConstantBuffer);

    GLuint pConstantBufferAPI = *(reinterpret_cast<GLuint*>(pConstantBufferGL->GetConstantBufferPtr()));

    glBindBuffer(GL_UNIFORM_BUFFER, pConstantBufferAPI);

    GLvoid* p = glMapBuffer(GL_UNIFORM_BUFFER, GL_WRITE_ONLY);

    if (p && !CheckGLError())
    {
        memcpy(p, pBuffer, uBufferSize);

        glUnmapBuffer(GL_UNIFORM_BUFFER);
    }
    else
    {
        BE_RENDER_DEBUG("GL device could not map the material constant buffer!");
    }

    glBindBuffer(GL_UNIFORM_BUFFER, 0);
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderDeviceGL::SetConstantBuffer(BeConstantBuffer* pConstantBuffer, BeRenderTypes::EBeMaterialPipeline eMaterialPipeline)
{
    if (pConstantBuffer)
    {
        GLuint pConstantBufferAPI = *(reinterpret_cast<GLuint*>(pConstantBuffer->GetConstantBufferPtr()));

        switch (eMaterialPipeline)
        {
            case BeRenderTypes::MATERIAL_PIPELINE_GEOMETRY:
            {
                if (m_pCurrentGSConstantBufferSet != pConstantBuffer)
                {
                    glBindBuffer(GL_UNIFORM_BUFFER, pConstantBufferAPI);
                    m_pCurrentGSConstantBufferSet = pConstantBuffer;
                }
                break;
            }
            case BeRenderTypes::MATERIAL_PIPELINE_VERTEX:
            {
                if (m_pCurrentVSConstantBufferSet != pConstantBuffer)
                {
                    glBindBuffer(GL_UNIFORM_BUFFER, pConstantBufferAPI);
                    m_pCurrentVSConstantBufferSet = pConstantBuffer;
                }
                break;
            }
            case BeRenderTypes::MATERIAL_PIPELINE_PIXEL:
            {
                if (m_pCurrentPSConstantBufferSet != pConstantBuffer)
                {
                    glBindBuffer(GL_UNIFORM_BUFFER, pConstantBufferAPI);
                    m_pCurrentPSConstantBufferSet = pConstantBuffer;
                }
                break;
            }
        }
    }
    else
    {
        switch (eMaterialPipeline)
        {
            case BeRenderTypes::MATERIAL_PIPELINE_GEOMETRY:
            {
                m_pCurrentGSConstantBufferSet = NULL;
                glBindBuffer(GL_UNIFORM_BUFFER, 0);

                break;
            }
            case BeRenderTypes::MATERIAL_PIPELINE_VERTEX:
            {
                m_pCurrentVSConstantBufferSet = NULL;
                glBindBuffer(GL_UNIFORM_BUFFER, 0);

                break;
            }
            case BeRenderTypes::MATERIAL_PIPELINE_PIXEL:
            {
                m_pCurrentPSConstantBufferSet = NULL;
                glBindBuffer(GL_UNIFORM_BUFFER, 0);

                break;
            }
        }
    }
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderDeviceGL::FreeConstantBuffer(BeConstantBuffer* pConstantBuffer)
{
    BeConstantBufferGL* pConstantBufferAPI = reinterpret_cast<BeConstantBufferGL*>(pConstantBuffer);

    if (m_pCurrentGSConstantBufferSet == pConstantBuffer || m_pCurrentVSConstantBufferSet == pConstantBuffer || m_pCurrentPSConstantBufferSet == pConstantBuffer)
    {
        glBindBuffer(GL_UNIFORM_BUFFER, 0);
    }

    BeDelete pConstantBufferAPI;
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderDeviceGL::RenderIndexedPrimitive(BeVertexBuffer* pVB, BeIndexBuffer* pIB, BeVertexArrayObject* pVAO)
{
    BeMaterial* pUsedMaterial = NULL;
    BeMaterialProgram* pUsedVSProgram = NULL;
    BeMaterialVertexDefinition* pUsedVertexDefinition = NULL;

    if (pVAO)
    {
        SetVertexArrayObject(pVAO);
    }
    else
    {
        SetVertexBuffer(pVB);

        CheckGLError();

        SetIndexBuffer(pIB);

        CheckGLError();
    }

    glDrawElements(GL_TRIANGLES, pIB->GetIndexBufferCount(), GL_UNSIGNED_SHORT, 0);

    CheckGLError();

    if (pVAO)
    {
        SetVertexArrayObject(NULL);

        CheckGLError();
    }
}
/////////////////////////////////////////////////////////////////////////////
BeMaterialVertexDefinition* BeRenderDeviceGL::CreateVertexDefinition(void)
{
    BeMaterialVertexDefinitionGL* pVertexDefinition = BeNew BeMaterialVertexDefinitionGL();

    return pVertexDefinition;
}
/////////////////////////////////////////////////////////////////////////////
BeVoidP BeRenderDeviceGL::ResolveVertexDefinition(BeMaterialVertexDefinition* pVertexDefinition, BeVoidP pInputData, BeMaterialProgram* pMaterialProgram)
{
    BE_UNUSED(pVertexDefinition);
    BE_UNUSED(pInputData);
    BE_UNUSED(pMaterialProgram);

    return NULL;
}
/////////////////////////////////////////////////////////////////////////////
BeVoidP BeRenderDeviceGL::ResolveTextureSampler(BeVoidP pInputData)
{
    BE_UNUSED(pInputData);

    return NULL;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeVoidP BeRenderDeviceGL::CreateRenderState(BeRenderStates::ERenderStatesType eRSType, BeVoidP pData)
{
    return NULL;
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderDeviceGL::UpdateRenderState(BeRenderStates::ERenderStatesType eRSType, BeVoidP pData)
{
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeRenderDeviceGL::BindTexture(BeInt iCacheHandle, BeTexture::ETextureChannel eChannel)
{
    if (iCacheHandle != -1)
    {
        BeRenderTypes::BeTextureCacheEntry* pTextureCached = GetTextureManager()->GetTextureCacheEntry(iCacheHandle);

        if (pTextureCached)
        {
            BeTextureGL* pTexture = reinterpret_cast<BeTextureGL*>(pTextureCached->m_pTexture);

            GLuint pTextureAPI = *(reinterpret_cast<GLuint*>(pTexture->GetTexturePtr()));

            glActiveTexture(GL_TEXTURE0 + (BeInt)eChannel);

            glBindTexture(GL_TEXTURE_2D, pTextureAPI);

            CheckGLError();
        }
    }
    else
    {
        glBindTexture(GL_TEXTURE_2D, 0);
    }
}
/////////////////////////////////////////////////////////////////////////////
BeTextureSampler* BeRenderDeviceGL::CreateTextureSampler(void)
{
    BeTextureSamplerGL* pNewTextureSampler = BeNew BeTextureSamplerGL();

    return pNewTextureSampler;
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderDeviceGL::SetSampler(BeTextureSampler* pTextureSampler, BeInt iSlot, BeRenderTypes::EBeMaterialPipeline eMaterialPipeline)
{
    if (pTextureSampler)
    {
        GLuint pSamplerAPI = *(reinterpret_cast<GLuint*>(pTextureSampler->GetTextureSamplerPtr()));

        switch (eMaterialPipeline)
        {
            case BeRenderTypes::MATERIAL_PIPELINE_GEOMETRY:
            {
                if (m_pCurrentGSSamplerSet != pTextureSampler)
                {
                    glBindSampler(iSlot, pSamplerAPI);
                    m_pCurrentGSSamplerSet = pTextureSampler;
                }
                break;
            }
            case BeRenderTypes::MATERIAL_PIPELINE_VERTEX:
            {
                if (m_pCurrentVSSamplerSet != pTextureSampler)
                {
                    glBindSampler(iSlot, pSamplerAPI);
                    m_pCurrentVSSamplerSet = pTextureSampler;
                }
                break;
            }
            case BeRenderTypes::MATERIAL_PIPELINE_PIXEL:
            {
                //if (m_pCurrentPSSamplerSet != pTextureSampler)
                {


                    glBindSampler(iSlot, pSamplerAPI);
                    m_pCurrentPSSamplerSet = pTextureSampler;
                }
                break;
            }
        }
    }
    else
    {
        switch (eMaterialPipeline)
        {
            case BeRenderTypes::MATERIAL_PIPELINE_GEOMETRY:
            {
                glBindSampler(iSlot, 0);
                m_pCurrentGSSamplerSet = NULL;

                break;
            }
            case BeRenderTypes::MATERIAL_PIPELINE_VERTEX:
            {
                glBindSampler(iSlot, 0);
                m_pCurrentVSSamplerSet = NULL;

                break;
            }
            case BeRenderTypes::MATERIAL_PIPELINE_PIXEL:
            {
                glBindSampler(iSlot, 0);
                m_pCurrentPSSamplerSet = NULL;

                break;
            }
        }
    }

    CheckGLError();
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeRenderDeviceGL::SetMaterialForRendering(BeMaterial* pMaterial)
{
    pMaterial->SetMaterialForRendering(NULL);
}
/////////////////////////////////////////////////////////////////////////////
BeMaterialProgram* BeRenderDeviceGL::CreateMaterialProgramFromBuffer(BeRenderTypes::EBeMaterialPipeline eProgramType, BeVoidP pBufferData, BeInt iBufferSize, BeString8 strFunction)
{
    if (pBufferData != NULL)
    {
        const GLchar* sourceProgram = (const GLchar *)pBufferData;

        GLuint pMaterialProgram = 0;

        switch (eProgramType)
        {
            case BeRenderTypes::MATERIAL_PIPELINE_VERTEX:
            {
                pMaterialProgram = glCreateShader(GL_VERTEX_SHADER);
                break;
            }
            case BeRenderTypes::MATERIAL_PIPELINE_PIXEL:
            {
                pMaterialProgram = glCreateShader(GL_FRAGMENT_SHADER);
                break;
            }
        }

        glShaderSource(pMaterialProgram, 1, &sourceProgram, 0);
        glCompileShader(pMaterialProgram);

        GLint isCompiled = 0;
        glGetShaderiv(pMaterialProgram, GL_COMPILE_STATUS, &isCompiled);
        if (isCompiled == GL_FALSE)
        {
            GLint maxLength = 0;
            glGetShaderiv(pMaterialProgram, GL_INFO_LOG_LENGTH, &maxLength);

            std::vector<GLchar> infoLog(maxLength);
            glGetShaderInfoLog(pMaterialProgram, maxLength, &maxLength, &infoLog[0]);

            glDeleteShader(pMaterialProgram);

            BE_RENDER_ERROR("Program %s Compilation failed with log: \n\n%s", strFunction.ToRaw(), infoLog.data());

            return NULL;
        }

        BeMaterialProgramGL* pNewMaterialProgram = BeNew BeMaterialProgramGL(eProgramType);

        switch (eProgramType)
        {
            case BeRenderTypes::MATERIAL_PIPELINE_VERTEX:
            {
                pNewMaterialProgram->SetMaterialProgramPtr(&pMaterialProgram);
                break;
            }
            case BeRenderTypes::MATERIAL_PIPELINE_PIXEL:
            {
                pNewMaterialProgram->SetMaterialProgramPtr(&pMaterialProgram);
                break;
            }
        }

        return pNewMaterialProgram;
    }

    return NULL;
}
/////////////////////////////////////////////////////////////////////////////

#endif

