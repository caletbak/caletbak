/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeRenderDeviceGLEnumerator.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BERENDERDEVICEGLENUMERATOR_H
#define BE_BERENDERDEVICEGLENUMERATOR_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>

#include <Render/BeRenderDeviceEnumerator.h>

/////////////////////////////////////////////////
/// \class BeRenderDeviceGLEnumerator
/// \brief GL rendering device enumerator
/////////////////////////////////////////////////
class RENDER_API BeRenderDeviceGLEnumerator : public BeRenderDeviceEnumerator
{
      
  public:

    /// \brief  GL Device types
    enum EGLDeviceType
    {
      DEVICE_ICD_ACCELERATED,    // ICD Accelerated Hardware
      DEVICE_MCD_ACCELERATED,    // MCD Accelerated Hardware
      DEVICE_NOT_ACCELERATED,    // Software

      DEVICE_TYPE_COUNT
    };


        
    /// \brief  Constructor
    BeRenderDeviceGLEnumerator();
    /// \brief  Destructor
    ~BeRenderDeviceGLEnumerator();
    


    // Overload
    // {

      /// \brief  Executes all the adapters, devices and display modes enumeration processes
      virtual BeBoolean ExecuteEnumerationProcess(void);

      /// \brief  Fills the display modes array with the enumeration results
      virtual void FillDisplayModesArray( BeUInt uAdapterOrdinal, BeRenderTypes::BeDeviceAdapter& kDeviceAdapter );

      /// \brief  Find the best suitable display mode taking into account the input data
      virtual BeBoolean FindBestDisplayMode( BeUInt uAdapterOrdinal, const BeRenderTypes::BeRenderDeviceSettings& kRenderDeviceSettings, BeRenderTypes::BeRenderDeviceDisplayMode* pDisplayMode );

    // }



    // Methods
    // {

      /// \brief  Fill the pixel format with the found display mode desc
      BeInt FillPixelFormat( BeVoidP pHDC, BeVoidP pPixelFormat );

    // }



  private:

    /// \brief  Callback for sorting the enumerated display modes
    static BeInt SortModesCallback( const BeVoidP pArg1, const BeVoidP pArg2 );

    // Forward declarations
    class BeGLEnumAdapterInfo;
    class BeGLEnumDeviceInfo;
    class BeGLEnumDSMSConflict;
    class BeGLEnumDeviceSettingsCombo;
    
    // Methods
    // {

      /// \brief  Enumerates and builds the device info
      BeInt EnumerateDevices( BeGLEnumAdapterInfo* pAdapterInfo, BeVoidP pAdapterFormatList );

      /// \brief  Enumerate and builds the devices combo settings info
      BeInt EnumerateDeviceCombos( BeVoidP pHDC, BeGLEnumAdapterInfo* pAdapterInfo, BeGLEnumDeviceInfo* pDeviceInfo );

      /// \brief  Enumerate the available display modes
      void EnumerateDisplayModes( BeStringBase16 strAdapterName, BeVoidP pDisplayModesList );

      /// \brief  Checks an OpenGL extension
      BeBoolean WGLisExtensionSupported(const BeChar8* strExtension);

      /// \brief  Builds the available depth stencil formats list
      void BuildDepthStencilFormatList( BeGLEnumDeviceSettingsCombo* pDeviceCombo );

      /// \brief  Builds the available MultiSample types list
      void BuildMultiSampleTypeList( BeVoidP pHDC, BeGLEnumDeviceSettingsCombo* pDeviceCombo );

      /*
      /// \brief  Builds the available depth stencil formats list
      void BuildDepthStencilFormatList( BeGLEnumDeviceSettingsCombo* pDeviceCombo );
      
      /// \brief  Builds the conflict DSMS list
      void BuildDSMSConflictList( BeGLEnumDeviceSettingsCombo* pDeviceCombo );

      /// \brief  Builds the available present intervals list
      void BuildPresentIntervalList( BeGLEnumDeviceInfo* pDeviceInfo, BeGLEnumDeviceSettingsCombo* pDeviceCombo );
      */
                
    // }


    
    /// \brief  The adapters info list
    std::vector<BeGLEnumAdapterInfo*> m_kAdapterInfoList;

    /// \brief  The current selected settings combo
    BeGLEnumDeviceSettingsCombo* m_pCurrentSelectedSettingsCombo;
                    
};
// Smart pointer BeRenderDeviceGLEnumerator
typedef BeSmartPointer<BeRenderDeviceGLEnumerator> BeRenderDeviceGLEnumeratorPtr;

#endif // #ifndef BE_BERENDERDEVICEGLENUMERATOR_H
