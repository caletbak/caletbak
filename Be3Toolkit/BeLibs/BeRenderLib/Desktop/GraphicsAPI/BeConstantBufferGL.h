/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeConstantBufferGL.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BECONSTANTBUFFERGL_H
#define BE_BECONSTANTBUFFERGL_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>
#include <Render/BeConstantBuffer.h>

// Forward declarations
class BeRenderDevice;

/////////////////////////////////////////////////
/// \class BeConstantBufferGL
/// \brief High-level API constant buffer object
/////////////////////////////////////////////////
class RENDER_API BeConstantBufferGL : public BeConstantBuffer
{

public:

    // Methods
    // {

        /// \brief  Constructor
        BeConstantBufferGL();

        /// \brief  Destructor
        ~BeConstantBufferGL();

    // }



    // Getters
    // {

        /// \brief  Returns the direct pointer to the constant buffer API impl
        virtual BeVoidP GetConstantBufferPtr(void);

        /// \brief  Returns the number of program uniform indices this contant buffer uses
        BeInt GetConstantIndicesCount(void);

    // }



    // Setters
    // {

        /// \brief  Sets the direct pointer to the constant buffer API impl
        virtual void SetConstantBufferPtr(BeVoidP pConstantBufferPtr);

    // }

private:

    // Forward declarations
    class BeConstantBufferGLImpl;

    /// \brief  API Implementation
    BeConstantBufferGLImpl* m_pConstantBufferImpl;

};

#endif // #ifndef BE_BECONSTANTBUFFERGL_H
