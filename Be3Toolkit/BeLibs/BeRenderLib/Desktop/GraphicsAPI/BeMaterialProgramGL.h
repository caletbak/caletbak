/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeMaterialProgramGL.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEMATERIALPROGRAMGL_H
#define BE_BEMATERIALPROGRAMGL_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>
#include <Render/BeMaterialProgram.h>
#include <BeRenderTypes.h>

/////////////////////////////////////////////////
/// \class BeMaterialProgramGL
/// \brief High-level GL material program object
/////////////////////////////////////////////////
class RENDER_API BeMaterialProgramGL : public BeMaterialProgram
{
public:

    /// \brief Constructor
    BeMaterialProgramGL(BeRenderTypes::EBeMaterialPipeline eProgramType);
    /// \brief Destructor
    ~BeMaterialProgramGL();



    // Methods
    // {

        /// \brief  Prepares the program with its constant and so
        virtual void PrepareProgram(BeRenderDevice* pRenderDevice);

        /// \brief  Updates the program and constants all together
        virtual void UpdateProgram(BeRenderDevice* pRenderDevice);

        /// \brief  Sets the program constant buffers
        virtual void SetConstantBuffer(BeRenderDevice* pRenderDevice);

        /// \brief  Sets the program texture samplers
        virtual void SetTextureSamplers(BeRenderDevice* pRenderDevice);

        /// \brief  Sets the program for rendering
        virtual void SetProgramForRendering(BeVoidP pRenderContext);

        /// \brief  Links the UBO with the linked program
        void LinkUBOWithProgram(BeUInt uMaterialLinkedProgram, BeUInt uUBOBindingPointIndex);

    // }



    // Getters
    // {

        /// \brief  Returns the direct pointer to the material compiled data API impl
        virtual BeVoidP GetMaterialProgramCompiledDataPtr(void);

        /// \brief  Returns the number of uniform indices the constant buffer needs
        virtual BeInt GetConstantIndicesCount(void);

        /// \brief  Returns the direct pointer to the material API impl
        virtual BeVoidP GetMaterialProgramPtr(void);

    // }



    // Setters
    // {

        /// \brief  Sets the direct pointer to the material compiled data API impl
        virtual void SetMaterialProgramCompiledDataPtr(BeVoidP pMaterialCompiledDataPtr);

        /// \brief  Sets the direct pointer to the material API impl
        virtual void SetMaterialProgramPtr(BeVoidP pMaterialPtr);

    // }

private:

    /// \brief  Material Program type
    BeRenderTypes::EBeMaterialPipeline m_eProgramType;

    // Forward declarations
    class BeMaterialProgramGLImpl;

    /// \brief  Render Device API Implementation
    BeMaterialProgramGLImpl* m_pMaterialProgramImpl;

};

#endif // #ifndef BE_BEMATERIALPROGRAMGL_H
