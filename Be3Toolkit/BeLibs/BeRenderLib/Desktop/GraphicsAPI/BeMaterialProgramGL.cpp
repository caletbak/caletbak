/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeMaterialProgramGL.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <GraphicsAPI/BeMaterialProgramGL.h>
#include <GraphicsAPI/BeConstantBufferGL.h>
#include <GraphicsAPI/BeRenderDeviceGL.h>
#include <GraphicsAPI/BeTextureSamplerGL.h>
#include <GraphicsAPI/BeErrorCheckGL.h>
#include <Render/BeMaterialConstant.h>

#if TARGET_WINDOWS

// OpenGL libs
#pragma comment (lib, "opengl32.lib")
#pragma comment (lib, "OpenGLEWLib.lib")
#pragma comment (lib, "glu32.lib")

#include <GL/glew.h>

/////////////////////////////////////////////////////////////////////////////
class RENDER_API BeMaterialProgramGL::BeMaterialProgramGLImpl
{

public:

    /// \brief  Constructor
    BeMaterialProgramGLImpl();
    /// \brief  Destructor
    ~BeMaterialProgramGLImpl();

    /// \brief  API Material VS object
    BeInt m_iGLMaterialVS;

    /// \brief  API Material PS object
    BeInt m_iGLMaterialPS;

    /// \brief  API Material constants buffer
    BeConstantBufferGL* m_pConstantBufferAPI;
    /// \brief  Material constants buffer
    BeUChar8* m_pConstantBuffer;
    /// \brief  Material constants buffer size
    BeInt m_iConstantBufferSize;
    /// \brief  Material constants buffer offsets
    std::map<BeString8, BeInt> m_kConstantBufferOffsets;
};
/////////////////////////////////////////////////////////////////////////////
BeMaterialProgramGL::BeMaterialProgramGLImpl::BeMaterialProgramGLImpl()
    :
    m_pConstantBufferAPI(NULL),
    m_iGLMaterialVS(-1),
    m_iGLMaterialPS(-1),
    m_pConstantBuffer(NULL),
    m_iConstantBufferSize(0)
{
}
/////////////////////////////////////////////////////////////////////////////
BeMaterialProgramGL::BeMaterialProgramGLImpl::~BeMaterialProgramGLImpl()
{
    if (m_pConstantBufferAPI != NULL)
    {
        BeDelete m_pConstantBufferAPI;
        m_pConstantBufferAPI = NULL;
    }

    if (m_iGLMaterialVS >= 0)
    {
        glDeleteShader(m_iGLMaterialVS);
        m_iGLMaterialVS = -1;
    }

    if (m_iGLMaterialPS >= 0)
    {
        glDeleteShader(m_iGLMaterialPS);
        m_iGLMaterialPS = -1;
    }

    if (m_pConstantBuffer != NULL)
    {
        BeDelete[] m_pConstantBuffer;
        m_pConstantBuffer = NULL;
    }

    m_kConstantBufferOffsets.clear();
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeMaterialProgramGL::BeMaterialProgramGL(BeRenderTypes::EBeMaterialPipeline eProgramType)
:
    m_pMaterialProgramImpl(NULL)
{
    m_eProgramType = eProgramType;

    m_pMaterialProgramImpl = BeNew BeMaterialProgramGLImpl();
}
/////////////////////////////////////////////////////////////////////////////
BeMaterialProgramGL::~BeMaterialProgramGL()
{
    if (m_pMaterialProgramImpl != NULL)
    {
        BeDelete m_pMaterialProgramImpl;
        m_pMaterialProgramImpl = NULL;
    }
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeMaterialProgramGL::PrepareProgram(BeRenderDevice* pRenderDevice)
{
    // Creating the constant buffer struct in memory
    BeInt iConstantBufferSize = 0;

    std::vector<BeMaterialConstant*>::iterator it = m_kConstants.begin();
    while (it != m_kConstants.end())
    {
        BeMaterialConstant* pConstant = reinterpret_cast<BeMaterialConstant*>(*it);

        m_pMaterialProgramImpl->m_kConstantBufferOffsets[pConstant->m_strName] = iConstantBufferSize;

        iConstantBufferSize += pConstant->GetTypeSizeInMemory();

        it++;
    }

    if (iConstantBufferSize > 0)
    {
        m_pMaterialProgramImpl->m_iConstantBufferSize = iConstantBufferSize;
        m_pMaterialProgramImpl->m_pConstantBuffer = BeNew BeUChar8[iConstantBufferSize];
        memset(m_pMaterialProgramImpl->m_pConstantBuffer, 0, iConstantBufferSize);

        BeConstantBuffer* pConstantBufferAPI = pRenderDevice->CreateConstantBuffer(m_pMaterialProgramImpl->m_pConstantBuffer, m_pMaterialProgramImpl->m_iConstantBufferSize);
        if (pConstantBufferAPI != NULL)
        {
            m_pMaterialProgramImpl->m_pConstantBufferAPI = reinterpret_cast<BeConstantBufferGL*>(pConstantBufferAPI);
        }
    }
}
/////////////////////////////////////////////////////////////////////////////
void BeMaterialProgramGL::UpdateProgram(BeRenderDevice* pRenderDevice)
{
    m_kPendingConstantsMutex.Lock();

    std::map<BeString8, BeVoidP>::iterator it = m_kPendingConstantUpdates.begin();
    while (it != m_kPendingConstantUpdates.end())
    {
        BeMaterialConstant* pConstant = GetConstantByName(it->first);

        memcpy(&m_pMaterialProgramImpl->m_pConstantBuffer[m_pMaterialProgramImpl->m_kConstantBufferOffsets[pConstant->m_strName]], it->second, pConstant->GetTypeSize());

        it++;
    }
    m_kPendingConstantUpdates.clear();

    m_kPendingConstantsMutex.UnLock();

    if (m_pMaterialProgramImpl->m_pConstantBufferAPI)
    {
        pRenderDevice->UpdateConstantBuffer(m_pMaterialProgramImpl->m_pConstantBufferAPI, m_pMaterialProgramImpl->m_pConstantBuffer, m_pMaterialProgramImpl->m_iConstantBufferSize);
    }
}
/////////////////////////////////////////////////////////////////////////////
void BeMaterialProgramGL::SetConstantBuffer(BeRenderDevice* pRenderDevice)
{
    if (m_pMaterialProgramImpl->m_pConstantBufferAPI)
    {
        pRenderDevice->SetConstantBuffer(m_pMaterialProgramImpl->m_pConstantBufferAPI, m_eProgramType);
    }
}
/////////////////////////////////////////////////////////////////////////////
void BeMaterialProgramGL::SetTextureSamplers(BeRenderDevice* pRenderDevice)
{
    for (BeUInt i = 0; i < m_kTextureChannels.size(); ++i)
    {
        BeRenderTypes::BeTextureChannel* pTextureChannel = m_kTextureChannels[i];

        BeTextureSamplerGL* pTextureSampler = reinterpret_cast<BeTextureSamplerGL*>(pTextureChannel->m_pTextureSampler);

        pRenderDevice->SetSampler(pTextureSampler, i, m_eProgramType);
    }
}
/////////////////////////////////////////////////////////////////////////////
void BeMaterialProgramGL::SetProgramForRendering(BeVoidP pRenderContext)
{
    // Not needed in GL, using linked program
}
/////////////////////////////////////////////////////////////////////////////
void BeMaterialProgramGL::LinkUBOWithProgram(BeUInt uMaterialLinkedProgram, BeUInt uUBOBindingPointIndex)
{
    if (m_pMaterialProgramImpl->m_pConstantBufferAPI)
    {
        GLuint uUBOObject = *((GLuint*) reinterpret_cast<GLuint*>(m_pMaterialProgramImpl->m_pConstantBufferAPI->GetConstantBufferPtr()));

        glBindBuffer(GL_UNIFORM_BUFFER, uUBOObject);

        BeUInt uUBOBlockIndex = glGetUniformBlockIndex(uMaterialLinkedProgram, m_strContantBlockName.ToRaw());

        glBindBufferBase(GL_UNIFORM_BUFFER, uUBOBindingPointIndex, uUBOObject);

        glUniformBlockBinding(uMaterialLinkedProgram, uUBOBlockIndex, uUBOBindingPointIndex);

        glBindBuffer(GL_UNIFORM_BUFFER, 0);

        CheckGLError();
    }
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeVoidP BeMaterialProgramGL::GetMaterialProgramCompiledDataPtr(void)
{
    // Not needed in GL

    return NULL;
}
/////////////////////////////////////////////////////////////////////////////
BeInt BeMaterialProgramGL::GetConstantIndicesCount(void)
{
    BeInt iIndicesCount = 0;

    for (BeUInt i = 0; i < m_kConstants.size(); ++i)
    {
        BeMaterialConstant* pConstant = m_kConstants[i];

        if (pConstant->m_eType == BeMaterialConstant::CONSTANT_MATRIX)
        {
            iIndicesCount += 4;
        }
        else
        {
            iIndicesCount++;
        }
    }

    return iIndicesCount;
}
/////////////////////////////////////////////////////////////////////////////
BeVoidP BeMaterialProgramGL::GetMaterialProgramPtr(void)
{
    switch (m_eProgramType)
    {
        case BeRenderTypes::MATERIAL_PIPELINE_VERTEX:
        {
            return &m_pMaterialProgramImpl->m_iGLMaterialVS;
        }
        case BeRenderTypes::MATERIAL_PIPELINE_PIXEL:
        {
            return &m_pMaterialProgramImpl->m_iGLMaterialPS;
        }
    }

    return NULL;
}
/////////////////////////////////////////////////////////////////////////////
void BeMaterialProgramGL::SetMaterialProgramCompiledDataPtr(BeVoidP pMaterialCompiledDataPtr)
{
    // Not needed in GL
}
/////////////////////////////////////////////////////////////////////////////
void BeMaterialProgramGL::SetMaterialProgramPtr(BeVoidP pMaterialProgramPtr)
{
    switch (m_eProgramType)
    {
        case BeRenderTypes::MATERIAL_PIPELINE_VERTEX:
        {
            m_pMaterialProgramImpl->m_iGLMaterialVS = *((BeInt*)pMaterialProgramPtr);
            break;
        }
        case BeRenderTypes::MATERIAL_PIPELINE_PIXEL:
        {
            m_pMaterialProgramImpl->m_iGLMaterialPS = *((BeInt*)pMaterialProgramPtr);
            break;
        }
    }
}
/////////////////////////////////////////////////////////////////////////////

#endif

