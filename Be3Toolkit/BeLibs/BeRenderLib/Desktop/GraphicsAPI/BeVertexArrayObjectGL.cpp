/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeVertexArrayObjectGL.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <GraphicsAPI/BeVertexArrayObjectGL.h>
#include <GraphicsAPI/BeRenderDeviceGL.h>
#include <GraphicsAPI/BeErrorCheckGL.h>
#include <Render/BeMaterialVertexDefinition.h>

#if TARGET_WINDOWS

// OpenGL libs
#pragma comment (lib, "opengl32.lib")
#pragma comment (lib, "OpenGLEWLib.lib")
#pragma comment (lib, "glu32.lib")

#include <GL/glew.h>

/////////////////////////////////////////////////////////////////////////////
class RENDER_API BeVertexArrayObjectGL::BeVertexArrayObjectGLImpl
{

public:

    /// \brief  Constructor
    BeVertexArrayObjectGLImpl();
    /// \brief  Destructor
    ~BeVertexArrayObjectGLImpl();

    /// \brief  VAO API object
    GLuint m_uVAOHandle;
};
/////////////////////////////////////////////////////////////////////////////
BeVertexArrayObjectGL::BeVertexArrayObjectGLImpl::BeVertexArrayObjectGLImpl()
:
    m_uVAOHandle(0)
{
}
/////////////////////////////////////////////////////////////////////////////
BeVertexArrayObjectGL::BeVertexArrayObjectGLImpl::~BeVertexArrayObjectGLImpl()
{
    if (m_uVAOHandle > 0)
    {
        glDeleteVertexArrays(1, &m_uVAOHandle);
        m_uVAOHandle = 0;
    }
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeVertexArrayObjectGL::BeVertexArrayObjectGL()
:
    m_pVertexArrayObjectImpl(NULL)
{
    m_pVertexArrayObjectImpl = BeNew BeVertexArrayObjectGLImpl();
}
/////////////////////////////////////////////////////////////////////////////
BeVertexArrayObjectGL::~BeVertexArrayObjectGL()
{
    if (m_pVertexArrayObjectImpl != NULL)
    {
        BeDelete m_pVertexArrayObjectImpl;
        m_pVertexArrayObjectImpl = NULL;
    }
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeVoidP BeVertexArrayObjectGL::GetVertexArrayObjectPtr(void)
{
    return &m_pVertexArrayObjectImpl->m_uVAOHandle;
}
/////////////////////////////////////////////////////////////////////////////
void BeVertexArrayObjectGL::SetVertexArrayObjectPtr(BeVoidP pVAOPtr)
{
    GLuint* pVAOAPIObject = reinterpret_cast<GLuint*>(pVAOPtr);

    m_pVertexArrayObjectImpl->m_uVAOHandle = *pVAOAPIObject;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeVertexArrayObjectGL::PrepareVertexArrayObjectAttributes(BeMaterialVertexDefinition* pVertexDefinition)
{
    BeInt iCurrentAttribLayout = 0;
    for (BeInt i = 0; i < pVertexDefinition->GetAttributeCount(); ++i)
    {
        BeMaterialVertexAttribute* pAttribute = pVertexDefinition->GetAttributeByIndex(i);

        if (pAttribute->m_eType == BeMaterialVertexAttribute::ATTRIBUTE_MATRIX)
        {
            glVertexAttribPointer(iCurrentAttribLayout, 4, GL_FLOAT, pAttribute->m_bNormalised, pVertexDefinition->GetVertexDefinitionSize(), 0);
            iCurrentAttribLayout++;
            glVertexAttribPointer(iCurrentAttribLayout, 4, GL_FLOAT, pAttribute->m_bNormalised, pVertexDefinition->GetVertexDefinitionSize(), 0);
            iCurrentAttribLayout++;
            glVertexAttribPointer(iCurrentAttribLayout, 4, GL_FLOAT, pAttribute->m_bNormalised, pVertexDefinition->GetVertexDefinitionSize(), 0);
            iCurrentAttribLayout++;
            glVertexAttribPointer(iCurrentAttribLayout, 4, GL_FLOAT, pAttribute->m_bNormalised, pVertexDefinition->GetVertexDefinitionSize(), 0);
            iCurrentAttribLayout++;
        }
        else
        {
            glVertexAttribPointer(iCurrentAttribLayout, pAttribute->GetComponentCount(), GL_FLOAT, pAttribute->m_bNormalised, pVertexDefinition->GetVertexDefinitionSize(), 0);
            iCurrentAttribLayout++;
        }
    }

    m_bAttributesSet = true;
}
/////////////////////////////////////////////////////////////////////////////
void BeVertexArrayObjectGL::EnableVertexArrayObjectAttributes(BeMaterialVertexDefinition* pVertexDefinition, BeBoolean bEnable)
{
    BeInt iCurrentAttribLayout = 0;
    for (BeInt i = 0; i < pVertexDefinition->GetAttributeCount(); ++i)
    {
        BeMaterialVertexAttribute* pAttribute = pVertexDefinition->GetAttributeByIndex(i);
        if (bEnable)
        {
            if (pAttribute->m_eType == BeMaterialVertexAttribute::ATTRIBUTE_MATRIX)
            {
                glEnableVertexAttribArray(iCurrentAttribLayout++);
                glEnableVertexAttribArray(iCurrentAttribLayout++);
                glEnableVertexAttribArray(iCurrentAttribLayout++);
                glEnableVertexAttribArray(iCurrentAttribLayout++);
            }
            else
            {
                glEnableVertexAttribArray(iCurrentAttribLayout++);
            }
        }
        else
        {
            if (pAttribute->m_eType == BeMaterialVertexAttribute::ATTRIBUTE_MATRIX)
            {
                glDisableVertexAttribArray(iCurrentAttribLayout++);
                glDisableVertexAttribArray(iCurrentAttribLayout++);
                glDisableVertexAttribArray(iCurrentAttribLayout++);
                glDisableVertexAttribArray(iCurrentAttribLayout++);
            }
            else
            {
                glDisableVertexAttribArray(iCurrentAttribLayout++);
            }
        }
    }
}
/////////////////////////////////////////////////////////////////////////////


#endif

