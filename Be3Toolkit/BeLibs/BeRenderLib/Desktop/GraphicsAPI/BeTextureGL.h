/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeRenderDeviceGL.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BETEXTUREGL_H
#define BE_BETEXTUREGL_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>

#include <Render/BeTexture.h>

/////////////////////////////////////////////////
/// \class BeTextureGL
/// \brief GL Texture object
/////////////////////////////////////////////////
class RENDER_API BeTextureGL : public BeTexture
{
      
  public:
    
    /// \brief Constructor
    BeTextureGL();
    /// \brief Destructor
    ~BeTextureGL();

    // Getters
    // {

        /// \brief  Returns the direct pointer to the texture API impl
        virtual BeVoidP GetTexturePtr(void);

    // }



    // Methods
    // {

        /// \brief  Sets the direct pointer to the texture API impl
        virtual void SetTexturePtr(BeVoidP pTexturePtr);

    // }

  private:
    
    // Forward declarations
    class BeTextureGLImpl;

    /// \brief  Render Device API Implementation
    BeTextureGLImpl* m_pTextureImpl;
            
};

#endif // #ifndef BE_BETEXTUREGL_H
