/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeRenderStatesGL.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BERENDERSTATESGL_H
#define BE_BERENDERSTATESGL_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>
#include <Render/BeRenderStates.h>

/////////////////////////////////////////////////
/// \class BeRenderStatesGL
/// \brief Class for updating the Render states
/////////////////////////////////////////////////
class RENDER_API BeRenderStatesGL : public BeRenderStates
{

    public:

        /// \brief Constructor
        BeRenderStatesGL(BeRenderDevice* pRenderDevice);
        /// \brief Destructor
        ~BeRenderStatesGL();

        // Getters
        // {

            /// \brief  Returns the direct pointer to the render states API impl
            virtual BeVoidP GetRenderStatesPtr(ERenderStatesType eRSType);

        // }



        // Setters
        // {

            /// \brief  Sets the direct pointer to the render states API impl
            virtual void SetRenderStatesPtr(BeVoidP pRSPtr, ERenderStatesType eRSType);

        // }



        // Methods
        // {

            /// \brief  Updates the material and constants all together
            virtual void UpdateRenderStates(void);

        // }

    private:

        // Forward declarations
        class BeRenderStatesGLImpl;

        /// \brief  API Implementation
        BeRenderStatesGLImpl* m_pRenderStatesImpl;

};

#endif // #ifndef BE_BERENDERSTATESGL_H
