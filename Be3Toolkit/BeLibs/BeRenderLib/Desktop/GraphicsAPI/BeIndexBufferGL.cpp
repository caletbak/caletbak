/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeIndexBufferGL.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <GraphicsAPI/BeIndexBufferGL.h>
#include <GraphicsAPI/BeRenderDeviceGL.h>
#include <Managers/BeRenderManager.h>

#if TARGET_WINDOWS

// OpenGL libs
#pragma comment (lib, "opengl32.lib")
#pragma comment (lib, "OpenGLEWLib.lib")

#include <windows.h>

#include <GL/glew.h>

/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
class RENDER_API BeIndexBufferGL::BeIndexBufferGLImpl
{

public:

    /// \brief  Constructor
    BeIndexBufferGLImpl();
    /// \brief  Destructor
    ~BeIndexBufferGLImpl();

    /// \brief  API Index buffer object
    GLuint m_uIndexBufferAPI;

};
/////////////////////////////////////////////////////////////////////////////
BeIndexBufferGL::BeIndexBufferGLImpl::BeIndexBufferGLImpl()
:
    m_uIndexBufferAPI(0)
{
}
/////////////////////////////////////////////////////////////////////////////
BeIndexBufferGL::BeIndexBufferGLImpl::~BeIndexBufferGLImpl()
{
    if (m_uIndexBufferAPI > 0)
    {
        glDeleteBuffers(1, &m_uIndexBufferAPI);
        m_uIndexBufferAPI = 0;
    }
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////

BeIndexBufferGL::BeIndexBufferGL()
:
    m_pIndexBufferImpl(NULL)
{
    m_pIndexBufferImpl = BeNew BeIndexBufferGLImpl();
}

/////////////////////////////////////////////////////////////////////////////

BeIndexBufferGL::~BeIndexBufferGL()
{
    if (m_pIndexBufferImpl != NULL)
    {
        BeDelete m_pIndexBufferImpl;
        m_pIndexBufferImpl = NULL;
    }
}

/////////////////////////////////////////////////////////////////////////////

BeVoidP BeIndexBufferGL::GetIndexBufferPtr(void)
{
    return &m_pIndexBufferImpl->m_uIndexBufferAPI;
}

/////////////////////////////////////////////////////////////////////////////

void BeIndexBufferGL::SetIndexBufferPtr(BeVoidP pIndexBufferPtr, BeInt iIndicesCount)
{
    GLuint uIndexBufferAPI = *(reinterpret_cast<GLuint*>(pIndexBufferPtr));

    m_pIndexBufferImpl->m_uIndexBufferAPI = uIndexBufferAPI;

    m_iIndicesCount = iIndicesCount;
}

/////////////////////////////////////////////////////////////////////////////

#endif

