/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeRenderDeviceGLEnumerator.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <GraphicsAPI/BeRenderDeviceGLEnumerator.h>

#include <Render/BeRenderDevice.h>
#include <GraphicsAPI/BeRenderDeviceGL.h>
#include <GraphicsAPI/BeErrorCheckGL.h>

#if TARGET_WINDOWS

#include <GL/glew.h>
#include <GL/wglew.h>

/////////////////////////////////////////////////////////////////////////////
class BeGLDisplayMode
{
  public:

    /// \brief  Constructor
    BeGLDisplayMode();
    /// \brief  Copy Constructor
    BeGLDisplayMode(const BeGLDisplayMode& kDisplayMode);

    /// \brief Copy values
    BeGLDisplayMode &operator =(const BeGLDisplayMode &kDisplayMode);

    /// \brief  Resolution Width
    BeUInt m_uWidth;
    /// \brief  Resolution Height
    UINT m_uHeight;
    /// \brief  DM refresh rate
    UINT m_uRefreshRate;
    /// \brief  DM flags
    BeInt m_iFlags;
    /// \brief  Bit depth
    BeShort m_iBits;
    /// \brief  DM Format
    BeInt m_iFormat;
    
};
/////////////////////////////////////////////////////////////////////////////
BeGLDisplayMode::BeGLDisplayMode()
:
  m_uWidth(0),
  m_uHeight(0),
  m_uRefreshRate(0),
  m_iFlags(0),
  m_iBits(0),
  m_iFormat(0)
{
}
/////////////////////////////////////////////////////////////////////////////
BeGLDisplayMode::BeGLDisplayMode(const BeGLDisplayMode& kDisplayMode)
:
  m_uWidth(kDisplayMode.m_uWidth),
  m_uHeight(kDisplayMode.m_uHeight),
  m_uRefreshRate(kDisplayMode.m_uRefreshRate),
  m_iFlags(kDisplayMode.m_iFlags),
  m_iBits(kDisplayMode.m_iBits),
  m_iFormat(kDisplayMode.m_iFormat)
{
}
/////////////////////////////////////////////////////////////////////////////
BeGLDisplayMode& BeGLDisplayMode::operator =(const BeGLDisplayMode &kDisplayMode)
{
  m_uWidth = kDisplayMode.m_uWidth;
  m_uHeight = kDisplayMode.m_uHeight;
  m_uRefreshRate = kDisplayMode.m_uRefreshRate;
  m_iFlags = kDisplayMode.m_iFlags;
  m_iBits = kDisplayMode.m_iBits;
  m_iFormat = kDisplayMode.m_iFormat;
  return *this;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
class RENDER_API BeRenderDeviceGLEnumerator::BeGLEnumDeviceSettingsCombo
{
  public:
    
    /// \brief  Destructor
    ~BeGLEnumDeviceSettingsCombo();


    /// \brief  The adapter ordinal index
    BeUInt m_uAdapterOrdinal;
    /// \brief  The device type
    EGLDeviceType m_kDeviceType;
    /// \brief  The backbuffer format
    BeRenderTypes::ERenderFormat m_kBackBufferFormat;
          

    /// \brief  The available depth stencil list
    std::vector<BeRenderTypes::EDepthStencilFormat> m_kDepthStencilFormatList;
    /// \brief  The available MultiSample type list
    std::vector<BeInt> m_kMultiSampleTypeList;
    /// \brief  The available MultiSample Quality list for each MS type
    std::vector<BeInt> m_kMultiSampleQualityList;
    

    /// \brief  The adapter info
    BeRenderDeviceGLEnumerator::BeGLEnumAdapterInfo* m_pAdapterInfo;
    /// \brief  The device info
    BeRenderDeviceGLEnumerator::BeGLEnumDeviceInfo* m_pDeviceInfo;

};
/////////////////////////////////////////////////////////////////////////////
BeRenderDeviceGLEnumerator::BeGLEnumDeviceSettingsCombo::~BeGLEnumDeviceSettingsCombo()
{
  m_kDepthStencilFormatList.clear();
  m_kMultiSampleTypeList.clear();
  m_kMultiSampleQualityList.clear();
  
  m_pAdapterInfo = NULL;
    m_pDeviceInfo = NULL;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
class RENDER_API BeRenderDeviceGLEnumerator::BeGLEnumDeviceInfo
{
  private:

    /// \brief  Disable equal operator
    const BeGLEnumDeviceInfo& operator =( const BeGLEnumDeviceInfo& rhs );

  public:

    /// \brief  Destructor
    ~BeGLEnumDeviceInfo();


    /// \brief  The adapter ordinal index
    BeUInt m_uAdapterOrdinal;
    /// \brief  The device type
    EGLDeviceType m_kDeviceType;
    /// \brief  The available device pixel formats
    std::vector<PIXELFORMATDESCRIPTOR> m_kDevicePixelFormatsList;


    /// \brief  List of BeDX9EnumDeviceSettingsCombo* with a unique set of AdapterFormat, BackBufferFormat, and Windowed
    std::vector<BeRenderDeviceGLEnumerator::BeGLEnumDeviceSettingsCombo*> m_kDeviceSettingsComboList;

};
/////////////////////////////////////////////////////////////////////////////
BeRenderDeviceGLEnumerator::BeGLEnumDeviceInfo::~BeGLEnumDeviceInfo()
{
  for ( BeUInt i = 0; i < m_kDeviceSettingsComboList.size(); ++i )
  {
    BeRenderDeviceGLEnumerator::BeGLEnumDeviceSettingsCombo* pSettingsCombo = m_kDeviceSettingsComboList.at(i);
    BeDelete pSettingsCombo;
    pSettingsCombo = NULL;
  }
  m_kDeviceSettingsComboList.clear();
  m_kDevicePixelFormatsList.clear();
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
class RENDER_API BeRenderDeviceGLEnumerator::BeGLEnumAdapterInfo
{
  private:

    /// \brief  Disable equal operator
    const BeGLEnumAdapterInfo& operator =( const BeGLEnumAdapterInfo& rhs );

  public:
      
    /// \brief  Destructor
    ~BeGLEnumAdapterInfo();

    /// \brief  The Adapter ordinal index
    BeUInt m_uAdapterOrdinal;
    /// \brief  The adapter identifier
    DISPLAY_DEVICE m_kAdapterIdentifier;
    /// \brief  The adapter unique name
    BeString16 m_strUniqueDescription;
        

    /// \brief  The array of supported display modes
    std::vector<BeGLDisplayMode> m_kDisplayModeList;
    /// \brief  The array of devices info with unique DeviceTypes
    std::vector<BeGLEnumDeviceInfo*> m_kDeviceInfoList;
    /// \brief  The Adapter format list
    std::vector<BeInt> m_kAdapterFormatList;
    
};
/////////////////////////////////////////////////////////////////////////////
BeRenderDeviceGLEnumerator::BeGLEnumAdapterInfo::~BeGLEnumAdapterInfo()
{
  
  m_kDisplayModeList.clear();
  m_kAdapterFormatList.clear();

  for ( BeUInt i = 0; i < m_kDeviceInfoList.size(); ++i )
  {
    BeRenderDeviceGLEnumerator::BeGLEnumDeviceInfo* pDeviceInfo = m_kDeviceInfoList.at(i);
    BeDelete pDeviceInfo;
    pDeviceInfo = NULL;
  }
  m_kDeviceInfoList.clear();
  
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeRenderDeviceGLEnumerator::BeRenderDeviceGLEnumerator()
:
  m_pCurrentSelectedSettingsCombo(NULL)
{
}
/////////////////////////////////////////////////////////////////////////////
BeRenderDeviceGLEnumerator::~BeRenderDeviceGLEnumerator()
{
  for ( BeUInt i = 0; i < m_kAdapterInfoList.size(); ++i )
  {
    BeGLEnumAdapterInfo* pAdapterInfo = m_kAdapterInfoList.at(i);
    BeDelete pAdapterInfo;
    pAdapterInfo = NULL;
  }
  m_kAdapterInfoList.clear();
}
/////////////////////////////////////////////////////////////////////////////
BeInt BeRenderDeviceGLEnumerator::SortModesCallback( const BeVoidP pArg1, const BeVoidP pArg2 )
{
    BeGLDisplayMode* pDm1 = (BeGLDisplayMode*)pArg1;
    BeGLDisplayMode* pDm2 = (BeGLDisplayMode*)pArg2;

    if( pDm1->m_uWidth > pDm2->m_uWidth )
        return 1;
    if( pDm1->m_uWidth < pDm2->m_uWidth )
        return -1;
    if( pDm1->m_uHeight > pDm2->m_uHeight )
        return 1;
    if( pDm1->m_uHeight < pDm2->m_uHeight )
        return -1;
    if( pDm1->m_iFormat > pDm2->m_iFormat )
        return 1;
    if( pDm1->m_iFormat < pDm2->m_iFormat )
        return -1;
    if( pDm1->m_uRefreshRate > pDm2->m_uRefreshRate )
        return 1;
    if( pDm1->m_uRefreshRate < pDm2->m_uRefreshRate )
        return -1;

    return 0;
}
////////////////////////////////////////////////////////////////////////////
BeBoolean BeRenderDeviceGLEnumerator::WGLisExtensionSupported(const BeChar8* strExtension)
{
    const BeSize_T iExtlen = strlen(strExtension);
    const BeChar8 *supported = NULL;
 
    // Try To Use wglGetExtensionStringARB On Current DC, If Possible
    PROC wglGetExtString = wglGetProcAddress("wglGetExtensionsStringARB");
 
    if (wglGetExtString)
        supported = ((char*(__stdcall*)(HDC))wglGetExtString)(wglGetCurrentDC());
 
    // If That Failed, Try Standard Opengl Extensions String
    if (supported == NULL)
        supported = (char*)glGetString(GL_EXTENSIONS);
 
    // If That Failed Too, Must Be No Extensions Supported
    if (supported == NULL)
        return BE_FALSE;
 
    // Begin Examination At Start Of String, Increment By 1 On False Match
    for (const char* p = supported; ; p++)
    {
        // Advance p Up To The Next Possible Match
        p = strstr(p, strExtension);
 
        if (p == NULL)
            return BE_FALSE;                       // No Match
 
        // Make Sure That Match Is At The Start Of The String Or That
        // The Previous Char Is A Space, Or Else We Could Accidentally
        // Match "wglFunkywglExtension" With "wglExtension"
 
        // Also, Make Sure That The Following Character Is Space Or NULL
        // Or Else "wglExtensionTwo" Might Match "wglExtension"
        if ((p==supported || p[-1]==' ') && (p[iExtlen]=='\0' || p[iExtlen]==' '))
            return BE_TRUE;                        // Match
    }
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderDeviceGLEnumerator::BuildDepthStencilFormatList( BeGLEnumDeviceSettingsCombo* pDeviceCombo )
{
  
    for( BeUInt i = 0; i < pDeviceCombo->m_pDeviceInfo->m_kDevicePixelFormatsList.size(); ++i )
    {
    PIXELFORMATDESCRIPTOR kPixelFormat = pDeviceCombo->m_pDeviceInfo->m_kDevicePixelFormatsList[i];
    BeRenderTypes::EDepthStencilFormat kPixelDepthStencilFormat = BeRenderDeviceGL::ConvertPixelFormatDescToDepthStencilFormat(&kPixelFormat);

    BeBoolean bContains = BE_FALSE;
    for( BeUInt j = 0; j < pDeviceCombo->m_kDepthStencilFormatList.size(); ++j )
    {
      if ( kPixelDepthStencilFormat == pDeviceCombo->m_kDepthStencilFormatList[j] )
      {
        bContains = BE_TRUE;
        break;
      }
    }
    if ( !bContains )
      pDeviceCombo->m_kDepthStencilFormatList.push_back( kPixelDepthStencilFormat );
    }

}
/////////////////////////////////////////////////////////////////////////////
void BeRenderDeviceGLEnumerator::BuildMultiSampleTypeList( BeVoidP pHDC, BeGLEnumDeviceSettingsCombo* pDeviceCombo )
{

  BE_UNUSED(pHDC);

  // See If The String Exists In WGL!
    if (!WGLisExtensionSupported("WGL_ARB_multisample"))
    return;
     
    /*
    // Get Our Pixel Format
    PFNWGLGETPIXELFORMATATTRIBIVARBPROC wglGetPixelFormatAttribivARB = (PFNWGLGETPIXELFORMATATTRIBIVARBPROC)wglGetProcAddress("wglGetPixelFormatAttribivEXT");
    PFNWGLGETPIXELFORMATATTRIBFVARBPROC wglGetPixelFormatAttribfvARB = (PFNWGLGETPIXELFORMATATTRIBFVARBPROC)wglGetProcAddress("wglGetPixelFormatAttribfvEXT");

    if (!wglGetPixelFormatAttribivARB || !wglGetPixelFormatAttribfvARB)
        return;
        */

  //BeRenderTypes::ERenderFormat m_kBackBufferFormat;
  //std::vector<BeRenderTypes::EDepthStencilFormat> m_kDepthStencilFormatList;

  BeInt iFoundPixelFormat = -1;
  for( BeUInt i = 0; i < pDeviceCombo->m_kDepthStencilFormatList.size(); ++i )
    {

    for( BeUInt j = 0; j < pDeviceCombo->m_pDeviceInfo->m_kDevicePixelFormatsList.size(); ++j )
    {
      PIXELFORMATDESCRIPTOR kPixelFormat = pDeviceCombo->m_pDeviceInfo->m_kDevicePixelFormatsList[j];
      BeRenderTypes::ERenderFormat kPixelRenderFormat = BeRenderDeviceGL::ConvertPixelFormatDescToRenderFormat(&kPixelFormat);
      BeRenderTypes::EDepthStencilFormat kPixelDepthStencilFormat = BeRenderDeviceGL::ConvertPixelFormatDescToDepthStencilFormat(&kPixelFormat);

      if ( kPixelRenderFormat == pDeviceCombo->m_kBackBufferFormat && kPixelDepthStencilFormat == pDeviceCombo->m_kDepthStencilFormatList[i] )
      {
        iFoundPixelFormat = j+1;
        break;
      }
    }

    if ( iFoundPixelFormat != -1 )
      break;

  }

  if ( iFoundPixelFormat != -1 )
  {

    for( BeUInt i = 0; i < 16; ++i )
    {

      // These Attributes Are The Bits We Want To Test For In Our Sample
      // Everything Is Pretty Standard, The Only One We Want To
      // Really Focus On Is The SAMPLE BUFFERS ARB And WGL SAMPLES
      // These Two Are Going To Do The Main Testing For Whether Or Not
      // We Support Multisampling On This Hardware
      /*
      BeInt iAttributes[] = { 
        WGL_DRAW_TO_WINDOW_ARB,GL_TRUE,
        WGL_SUPPORT_OPENGL_ARB,GL_TRUE,
        WGL_ACCELERATION_ARB,WGL_FULL_ACCELERATION_ARB,
        WGL_COLOR_BITS_ARB,24,
        WGL_ALPHA_BITS_ARB,8,
        WGL_DEPTH_BITS_ARB,16,
        WGL_STENCIL_BITS_ARB,0,
        WGL_DOUBLE_BUFFER_ARB,GL_TRUE,
        WGL_SAMPLE_BUFFERS_ARB,GL_TRUE,
        WGL_SAMPLES_ARB, i,
        0,0};
        */
      BeInt iAttributes[] = { 
        WGL_DRAW_TO_WINDOW_ARB,
        WGL_SUPPORT_OPENGL_ARB,
        WGL_ACCELERATION_ARB,
        WGL_COLOR_BITS_ARB,
        WGL_ALPHA_BITS_ARB,
        WGL_DEPTH_BITS_ARB,
        WGL_STENCIL_BITS_ARB,
        WGL_DOUBLE_BUFFER_ARB,
        WGL_SAMPLE_BUFFERS_ARB,
        WGL_SAMPLES_ARB, 
        0};
      BeInt iAttributesResult[10];
      //BeFloat fAttributes[] = {0,0};

      BeBoolean bValid = (wglGetPixelFormatAttribivARB( (HDC)pHDC, iFoundPixelFormat, 0, 10, iAttributes, &iAttributesResult[0] ) != 0);
      if ( bValid && iAttributes[8] != 0 )
      {
        pDeviceCombo->m_kMultiSampleTypeList.push_back( i );
      }
      
      /*
      BeInt iPixelFormat;
      BeUInt uNumFormats;

      // First We Check To See If We Can Get A Pixel Format For 4 Samples
      BeBoolean bValid = (wglChoosePixelFormatARB( (HDC)pHDC, iAttributes, fAttributes, 1, &iPixelFormat, &uNumFormats ) != 0);
      if ( bValid && uNumFormats >= 1 )
      {
        pDeviceCombo->m_kMultiSampleTypeList.push_back( i );
      }
      */

      /*
      if( SUCCEEDED( pDeviceCombo->m_pAdapterInfo->m_pD3D->CheckDeviceMultiSampleType( pDeviceCombo->m_uAdapterOrdinal,
                                 pDeviceCombo->m_kDeviceType, pDeviceCombo->m_kBackBufferFormat,
                                 pDeviceCombo->m_bIsWindowed, kMSType, &uMSQuality ) ) )
      {
        pDeviceCombo->m_kMultiSampleTypeList.push_back( kMSType );
        if( uMSQuality > uMultisampleQualityMax + 1 )
          uMSQuality = uMultisampleQualityMax + 1;
        pDeviceCombo->m_kMultiSampleQualityList.push_back( uMSQuality );
      }
      */
    }

  }

}
/*
/////////////////////////////////////////////////////////////////////////////
void BeRenderDeviceGLEnumerator::BuildPresentIntervalList( BeDX9EnumDeviceInfo* pDeviceInfo, BeDX9EnumDeviceSettingsCombo* pDeviceCombo )
{
  BeUInt uPi;

  std::vector<BeInt> kPresentIntervalList;
  kPresentIntervalList.clear();
    kPresentIntervalList.push_back( D3DPRESENT_INTERVAL_IMMEDIATE );
    kPresentIntervalList.push_back( D3DPRESENT_INTERVAL_DEFAULT );
    kPresentIntervalList.push_back( D3DPRESENT_INTERVAL_ONE );
    kPresentIntervalList.push_back( D3DPRESENT_INTERVAL_TWO );
    kPresentIntervalList.push_back( D3DPRESENT_INTERVAL_THREE );
    kPresentIntervalList.push_back( D3DPRESENT_INTERVAL_FOUR );

    for( BeUInt i = 0; i < kPresentIntervalList.size(); ++i )
    {
        uPi = kPresentIntervalList.at( i );
        if( pDeviceCombo->m_bIsWindowed )
        {
            if( uPi == D3DPRESENT_INTERVAL_TWO ||
                uPi == D3DPRESENT_INTERVAL_THREE ||
                uPi == D3DPRESENT_INTERVAL_FOUR )
            {
                // These intervals are not supported in windowed mode.
                continue;
            }
        }
        // Note that D3DPRESENT_INTERVAL_DEFAULT is zero, so you
        // can't do a caps check for it -- it is always available.
        if( uPi == D3DPRESENT_INTERVAL_DEFAULT ||
            ( pDeviceInfo->m_kCaps.PresentationIntervals & uPi ) )
        {
            pDeviceCombo->m_kPresentIntervalList.push_back( uPi );
        }
    }
}
*/
/////////////////////////////////////////////////////////////////////////////
BeInt BeRenderDeviceGLEnumerator::EnumerateDeviceCombos( BeVoidP pHDC, BeGLEnumAdapterInfo* pAdapterInfo, BeGLEnumDeviceInfo* pDeviceInfo )
{
  
  BE_UNUSED(pHDC);
  BE_UNUSED(pAdapterInfo);
  BE_UNUSED(pDeviceInfo);
  
  const BeRenderTypes::ERenderFormat kBackBufferFormatArray[] =
    {
        BeRenderTypes::RFORMAT_R8G8B8A8,
        BeRenderTypes::RFORMAT_R8G8B8,
        BeRenderTypes::RFORMAT_R10G10B10A2,
        BeRenderTypes::RFORMAT_R5G5B5A1,
        BeRenderTypes::RFORMAT_R5G5B5
    };
    const BeUInt uBackBufferFormatArrayCount = sizeof( kBackBufferFormatArray ) / sizeof( kBackBufferFormatArray[0] );

  for( BeUInt iBackBufferFormat = 0; iBackBufferFormat < uBackBufferFormatArrayCount; ++iBackBufferFormat )
    {
        BeRenderTypes::ERenderFormat kBackBufferFormat = kBackBufferFormatArray[iBackBufferFormat];
    
    for( BeUInt iPixelFormat = 0; iPixelFormat < pDeviceInfo->m_kDevicePixelFormatsList.size(); ++iPixelFormat )
    {
      PIXELFORMATDESCRIPTOR kPixelFormat = pDeviceInfo->m_kDevicePixelFormatsList[iPixelFormat];
      BeRenderTypes::ERenderFormat kPixelRenderFormat = BeRenderDeviceGL::ConvertPixelFormatDescToRenderFormat(&kPixelFormat);
          
      if ( kBackBufferFormat == kPixelRenderFormat )
      {

        // At this point, we have an adapter/device/adapterformat/backbufferformat/iswindowed
        // DeviceCombo that is supported by the system and acceptable to the app. We still 
        // need to find one or more suitable depth/stencil buffer format,
        // multisample type, and present interval.
        BeGLEnumDeviceSettingsCombo* pDeviceCombo = BeNew BeGLEnumDeviceSettingsCombo;
        pDeviceCombo->m_uAdapterOrdinal = pAdapterInfo->m_uAdapterOrdinal;
        pDeviceCombo->m_kDeviceType = pDeviceInfo->m_kDeviceType;
        pDeviceCombo->m_kBackBufferFormat = kBackBufferFormat;
        pDeviceCombo->m_pAdapterInfo = pAdapterInfo;
        pDeviceCombo->m_pDeviceInfo = pDeviceInfo;
        
        BuildDepthStencilFormatList( pDeviceCombo );
        
        /*
        BuildMultiSampleTypeList( pHDC, pDeviceCombo );
        if( pDeviceCombo->m_kMultiSampleTypeList.size() == 0 )
        {
          BeDelete pDeviceCombo;
          continue;
        }
        */
        
        pDeviceInfo->m_kDeviceSettingsComboList.push_back( pDeviceCombo );

        break;

      }

    }

  }
    
    return S_OK;

}
/////////////////////////////////////////////////////////////////////////////
BeInt BeRenderDeviceGLEnumerator::EnumerateDevices( BeGLEnumAdapterInfo* pAdapterInfo, BeVoidP pAdapterFormatList )
{
	BE_UNUSED(pAdapterInfo);
	BE_UNUSED(pAdapterFormatList);
  
    const BeUInt uDevTypeArrayCount = DEVICE_TYPE_COUNT;

	HDC hDC = NULL;
  
  BeBoolean bWindowsHasBeenCreated = BE_FALSE;
  if ( BeRenderDevice::m_hWindow == NULL )
  {
    if ( !BeRenderDevice::CreateAppWindow(L"CheckGL", 640, 480, 32, BE_FALSE, BeRenderDeviceGL::WndProcCheckGL) )
      return -1;
    else
      bWindowsHasBeenCreated = BE_TRUE;
  }

  /* What is the highest pixel format?... */
  hDC = GetDC(BeRenderDevice::m_hWindow);
  if ( !hDC )
  {
    BeRenderDevice::DestroyAppWindow();
    return -1;
  }

  BeUInt uPixelFormat;
  static PIXELFORMATDESCRIPTOR pfd =        // pfd Tells Windows How We Want Things To Be
    {
      sizeof(PIXELFORMATDESCRIPTOR),        // Size Of This Pixel Format Descriptor
      1,                      // Version Number
      PFD_DRAW_TO_WINDOW |            // Format Must Support Window
      PFD_SUPPORT_OPENGL |            // Format Must Support OpenGL
      PFD_DOUBLEBUFFER,              // Must Support Double Buffering
      PFD_TYPE_RGBA,                // Request An RGBA Format
      32,                      // Select Our Color Depth
      0, 0, 0, 0, 0, 0,              // Color Bits Ignored
      0,                      // No Alpha Buffer
      0,                      // Shift Bit Ignored
      0,                      // No Accumulation Buffer
      0, 0, 0, 0,                  // Accumulation Bits Ignored
      32,                      // 16Bit Z-Buffer (Depth Buffer)  
      0,                      // No Stencil Buffer
      0,                      // No Auxiliary Buffer
      PFD_MAIN_PLANE,                // Main Drawing Layer
      0,                      // Reserved
      0, 0, 0                    // Layer Masks Ignored
    };
  
  uPixelFormat = ChoosePixelFormat( hDC, &pfd );
    if ( !uPixelFormat )  // Did Windows Find A Matching Pixel Format?
  {
    ReleaseDC(NULL, hDC);
      return -1;                // Return FALSE
  }

    if( !SetPixelFormat( hDC, uPixelFormat, &pfd ) )    // Are We Able To Set The Pixel Format?
  {
    ReleaseDC(NULL, hDC);
      return -1;                // Return FALSE
  }

  HGLRC tempOpenGLContext = wglCreateContext(hDC); // Create an OpenGL 2.1 context for our device context
  wglMakeCurrent(hDC, tempOpenGLContext); // Make the OpenGL 2.1 context current and active

  s_kCurrentGLEWContext = BeNew GLEWContext();
  s_kCurrentWGLEWContext = BeNew WGLEWContext();

  glewExperimental = true;

  GLenum error = glewInit(); // Enable GLEW
  if (error != GLEW_OK) // If GLEW fails
      return BE_FALSE;

  error = wglewInit(); // Enable GLEW
  if (error != GLEW_OK) // If GLEW fails
      return BE_FALSE;

  CheckGLError();

    // Enumerate each Direct3D device type
    for( BeUInt uDeviceType = 0; uDeviceType < uDevTypeArrayCount; uDeviceType++ )
    {
        BeGLEnumDeviceInfo* pDeviceInfo = BeNew BeGLEnumDeviceInfo;
        
        // Fill struct w/ AdapterOrdinal and D3DDEVTYPE
        pDeviceInfo->m_uAdapterOrdinal = pAdapterInfo->m_uAdapterOrdinal;
        pDeviceInfo->m_kDeviceType = (EGLDeviceType)uDeviceType;

    PIXELFORMATDESCRIPTOR pfdTmp;
    ZeroMemory(&pfdTmp, sizeof(PIXELFORMATDESCRIPTOR));
    BeInt iMaxPixelFormat = DescribePixelFormat(hDC, 1, sizeof(PIXELFORMATDESCRIPTOR), &pfdTmp);
    for ( BeInt i = 1; i <= iMaxPixelFormat; ++i )
    {
      DescribePixelFormat(hDC, i, sizeof(PIXELFORMATDESCRIPTOR), &pfdTmp);
      if ( (pfdTmp.dwFlags & PFD_SUPPORT_OPENGL) )
      {
        switch( uDeviceType )
        {
          case DEVICE_ICD_ACCELERATED:
          {
            if ( !(pfdTmp.dwFlags & PFD_GENERIC_FORMAT) )
              pDeviceInfo->m_kDevicePixelFormatsList.push_back(pfdTmp);
            break;
          }
          case DEVICE_MCD_ACCELERATED:
          {
            if ( (pfdTmp.dwFlags & PFD_GENERIC_FORMAT) && (pfdTmp.dwFlags & PFD_GENERIC_ACCELERATED) )
              pDeviceInfo->m_kDevicePixelFormatsList.push_back(pfdTmp);
            break;
          }
          case DEVICE_NOT_ACCELERATED:
          {
            if ( (pfdTmp.dwFlags & PFD_GENERIC_FORMAT) && !(pfdTmp.dwFlags & PFD_GENERIC_ACCELERATED) )
              pDeviceInfo->m_kDevicePixelFormatsList.push_back(pfdTmp);
            break;
          }
        }
      }
    }
    
        // Get info for each devicecombo on this device
    if ( pDeviceInfo->m_kDevicePixelFormatsList.size() > 0 )
    {
      if( FAILED( EnumerateDeviceCombos( hDC, pAdapterInfo, pDeviceInfo ) ) )
      {
        BeDelete pDeviceInfo;
        continue;
      }
    }
    else
    {
      BeDelete pDeviceInfo;
      continue;
    }

        // If at least one devicecombo for this device is found, 
        // add the deviceInfo to the list
    if( pDeviceInfo->m_kDeviceSettingsComboList.size() > 0 )
            pAdapterInfo->m_kDeviceInfoList.push_back( pDeviceInfo );
    else
            BeDelete pDeviceInfo;
      
    }

    BeDelete s_kCurrentGLEWContext; s_kCurrentGLEWContext = NULL;
    BeDelete s_kCurrentWGLEWContext; s_kCurrentWGLEWContext = NULL;

  wglMakeCurrent(NULL, NULL); // Remove the temporary context from being active
  wglDeleteContext(tempOpenGLContext); // Delete the temporary OpenGL 2.1 context

  ReleaseDC(NULL, hDC);
  if ( bWindowsHasBeenCreated )
    BeRenderDevice::DestroyAppWindow();
  
    return S_OK;
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderDeviceGLEnumerator::EnumerateDisplayModes( BeStringBase16 strAdapterName, BeVoidP pDisplayModesList )
{

  std::vector<BeGLDisplayMode>* pDisplayModes = static_cast<std::vector<BeGLDisplayMode>*>(pDisplayModesList);
  pDisplayModes->clear();
  
  DEVMODE kDeviceMode;
  kDeviceMode.dmSize = sizeof(DEVMODE);
  for ( BeUInt uNumMode = 0; ; ++uNumMode )
  {
    if ( EnumDisplaySettings( strAdapterName.c_str(), uNumMode, &kDeviceMode ) != 0 )
    {
      BeGLDisplayMode kNewDisplayMode;
      kNewDisplayMode.m_uWidth = kDeviceMode.dmPelsWidth;
      kNewDisplayMode.m_uHeight = kDeviceMode.dmPelsHeight;
      kNewDisplayMode.m_uRefreshRate = kDeviceMode.dmDisplayFrequency;
      kNewDisplayMode.m_iFlags = kDeviceMode.dmDisplayFlags;
      kNewDisplayMode.m_iBits = (BeShort)kDeviceMode.dmBitsPerPel;
      kNewDisplayMode.m_iFormat = GL_RGBA8;
      
      pDisplayModes->push_back(kNewDisplayMode);
    }
    else
      break;
  }

}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeRenderDeviceGLEnumerator::ExecuteEnumerationProcess(void)
{
  
    for ( BeUInt i = 0; i < m_kAdapterInfoList.size(); ++i )
    {
        BeGLEnumAdapterInfo* pAdapterInfo = m_kAdapterInfoList.at(i);
        BeDelete pAdapterInfo;
        pAdapterInfo = NULL;
    }
    m_kAdapterInfoList.clear();

    const BeInt kAllowedAdapterFormatArray[] =
    {
        GL_RGBA8//,
        //GL_RGB5_A1,
        //GL_R5_G6_B5,
        //GL_RGB10_A2
    };
    const BeUInt uAllowedAdapterFormatArrayCount = sizeof( kAllowedAdapterFormatArray ) / sizeof( kAllowedAdapterFormatArray[0] );

    BeUInt uMinWidth = 640;
    BeUInt uMinHeight = 480;
    BeUInt uMaxWidth = UINT_MAX;
    BeUInt uMaxHeight = UINT_MAX;
    BeUInt uRefreshMin = 0;
    BeUInt uRefreshMax = UINT_MAX;



  DISPLAY_DEVICE  dd;
  dd.cb = sizeof(DISPLAY_DEVICE);
  
  BeUInt uNumAdapters = 0;
  BeInt iDevice = 0;
  while ( EnumDisplayDevices(0, iDevice, &dd, 0) )
  {
    DISPLAY_DEVICE ddMon;
    ZeroMemory(&ddMon, sizeof(ddMon));
    ddMon.cb = sizeof(ddMon);
    DWORD devMon = 0;

    if ( EnumDisplayDevices(dd.DeviceName, devMon, &ddMon, 0) )
      uNumAdapters++;
  
    ZeroMemory(&dd, sizeof(dd));
    dd.cb = sizeof(dd);
    iDevice++; 
  }
  
    for( BeUInt uAdapterOrdinal = 0; uAdapterOrdinal < uNumAdapters; ++uAdapterOrdinal )
    {

    BeGLEnumAdapterInfo* pAdapterInfo = BeNew BeGLEnumAdapterInfo;
    pAdapterInfo->m_uAdapterOrdinal = uAdapterOrdinal;
    pAdapterInfo->m_kAdapterIdentifier.cb = sizeof(pAdapterInfo->m_kAdapterIdentifier);
    EnumDisplayDevices(0, uAdapterOrdinal, &pAdapterInfo->m_kAdapterIdentifier, 0);

    std::vector<BeGLDisplayMode> kDisplayModes;
    EnumerateDisplayModes(pAdapterInfo->m_kAdapterIdentifier.DeviceName, &kDisplayModes);

    // Get list of all display modes on this adapter.  
    // Also build a temporary list of all display adapter formats.
    pAdapterInfo->m_kAdapterFormatList.clear();
          
    for( BeUInt uFormatList = 0; uFormatList < uAllowedAdapterFormatArrayCount; ++uFormatList )
    {
      //BeInt kAllowedAdapterFormat = kAllowedAdapterFormatArray[uFormatList];
      BeUInt uNumAdapterModes = kDisplayModes.size();

      for( BeUInt uMode = 0; uMode < uNumAdapterModes; ++uMode )
      {
        const BeGLDisplayMode& kDisplayMode = kDisplayModes.at(uMode);
        
        if( kDisplayMode.m_uWidth < uMinWidth ||
          kDisplayMode.m_uHeight < uMinHeight ||
          kDisplayMode.m_uWidth > uMaxWidth ||
          kDisplayMode.m_uHeight > uMaxHeight ||
          kDisplayMode.m_uRefreshRate < uRefreshMin ||
          kDisplayMode.m_uRefreshRate > uRefreshMax )
        {
          continue;
        }

        pAdapterInfo->m_kDisplayModeList.push_back( kDisplayMode );

        BeBoolean bContains = BE_FALSE;
        for( BeUInt j = 0; j < pAdapterInfo->m_kAdapterFormatList.size(); ++j )
        {
          if( pAdapterInfo->m_kAdapterFormatList[j] == kDisplayMode.m_iFormat )
          {
            bContains = BE_TRUE;
            break;
          }
        }

        if( !bContains )
          pAdapterInfo->m_kAdapterFormatList.push_back( kDisplayMode.m_iFormat );
          
      }

    }
    
    // Sort displaymode list
    qsort( &pAdapterInfo->m_kDisplayModeList[0],
         pAdapterInfo->m_kDisplayModeList.size(), sizeof( BeGLDisplayMode ),
         SortModesCallback );

    // Get info for each device on this adapter
    if( FAILED( EnumerateDevices( pAdapterInfo, &pAdapterInfo->m_kAdapterFormatList ) ) )
    {
      BeDelete pAdapterInfo;
      continue;
    }

    // If at least one device on this adapter is available and compatible with the app, add the adapterInfo to the list
    if( pAdapterInfo->m_kDeviceInfoList.size() > 0 )
            m_kAdapterInfoList.push_back( pAdapterInfo );
        else
            BeDelete pAdapterInfo;

  }
  

  
  //
    // Check for 2 or more adapters with the same name. Append the name
    // with some instance number if that's the case to help distinguish
    // them.
    //
    bool bUniqueDesc = BE_TRUE;
    BeGLEnumAdapterInfo* pAdapterInfo;
    for( BeUInt i = 0; i < m_kAdapterInfoList.size(); i++ )
    {
        BeGLEnumAdapterInfo* pAdapterInfo1 = m_kAdapterInfoList.at( i );
        for( BeUInt j = i + 1; j < m_kAdapterInfoList.size(); j++ )
        {
            BeGLEnumAdapterInfo* pAdapterInfo2 = m_kAdapterInfoList.at( j );
      if( wcsncmp( pAdapterInfo1->m_kAdapterIdentifier.DeviceString,
                         pAdapterInfo2->m_kAdapterIdentifier.DeviceString, 128 ) == 0 )
            {
                bUniqueDesc = BE_FALSE;
                break;
            }
        }

        if( !bUniqueDesc )
            break;
    }

    for( BeUInt i = 0; i < m_kAdapterInfoList.size(); i++ )
    {
        pAdapterInfo = m_kAdapterInfoList.at( i );

        BeString16 strTemp(&pAdapterInfo->m_kAdapterIdentifier.DeviceString[0]);
        pAdapterInfo->m_strUniqueDescription = strTemp;
    
        if( !bUniqueDesc )
        {
            pAdapterInfo->m_strUniqueDescription += L" (#";
            pAdapterInfo->m_strUniqueDescription += ConvertString8To16( ParseIntToString8(pAdapterInfo->m_uAdapterOrdinal) );
            pAdapterInfo->m_strUniqueDescription += L")";
        }
    }
    
  return BE_TRUE;

}
/////////////////////////////////////////////////////////////////////////////
BeInt BeRenderDeviceGLEnumerator::FillPixelFormat( BeVoidP pHDC, BeVoidP pPixelFormat )
{
  
  PIXELFORMATDESCRIPTOR* pGLPixelFormat = static_cast<PIXELFORMATDESCRIPTOR*>(pPixelFormat);

  PIXELFORMATDESCRIPTOR pfd =        // pfd Tells Windows How We Want Things To Be
    {
      sizeof(PIXELFORMATDESCRIPTOR),        // Size Of This Pixel Format Descriptor
      1,                      // Version Number
      PFD_DRAW_TO_WINDOW |            // Format Must Support Window
      PFD_SUPPORT_OPENGL |            // Format Must Support OpenGL
      PFD_DOUBLEBUFFER,              // Must Support Double Buffering
      PFD_TYPE_RGBA,                // Request An RGBA Format
      32,                      // Select Our Color Depth
      0, 0, 0, 0, 0, 0,              // Color Bits Ignored
      0,                      // No Alpha Buffer
      0,                      // Shift Bit Ignored
      0,                      // No Accumulation Buffer
      0, 0, 0, 0,                  // Accumulation Bits Ignored
      32,                      // 16Bit Z-Buffer (Depth Buffer)  
      0,                      // No Stencil Buffer
      0,                      // No Auxiliary Buffer
      PFD_MAIN_PLANE,                // Main Drawing Layer
      0,                      // Reserved
      0, 0, 0                    // Layer Masks Ignored
    };

  BeUInt uPixelFormatIdx = ChoosePixelFormat( (HDC)pHDC, &pfd );
  if( uPixelFormatIdx == 0 )    // Are We Able To Set The Pixel Format?
        return -1;                // Return FALSE
    
  *pGLPixelFormat = pfd;
  return uPixelFormatIdx;
  
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderDeviceGLEnumerator::FillDisplayModesArray( BeUInt uAdapterOrdinal, BeRenderTypes::BeDeviceAdapter& kDeviceAdapter )
{

  BE_UNUSED(uAdapterOrdinal);
  BE_UNUSED(kDeviceAdapter);

  BeGLEnumAdapterInfo* pAdapterInfo = m_kAdapterInfoList.at(uAdapterOrdinal);
  kDeviceAdapter.m_strAdapterName = pAdapterInfo->m_strUniqueDescription;

  for ( BeUInt i = 0; i < pAdapterInfo->m_kDeviceInfoList.size(); ++i )
  {
    BeGLEnumDeviceInfo* pDeviceInfo = pAdapterInfo->m_kDeviceInfoList.at(i);
    if ( pDeviceInfo->m_kDeviceType == DEVICE_ICD_ACCELERATED )
    {
      //for ( BeUInt j = 0; j < pDeviceInfo->m_kDeviceSettingsComboList.size(); ++j )
      //{
      //  BeRenderDeviceGLEnumerator::BeGLEnumDeviceSettingsCombo* pSettingsCombo = pDeviceInfo->m_kDeviceSettingsComboList.at(j);
        
          BeInt iLastWidth = -1;
          BeInt iLastHeight = -1;

          for ( BeUInt k = 0; k < pAdapterInfo->m_kDisplayModeList.size(); ++k )
          {
            BeGLDisplayMode& kDisplayModeDesc = pAdapterInfo->m_kDisplayModeList.at(k);
            if ( (iLastWidth != (BeInt)kDisplayModeDesc.m_uWidth || iLastHeight != (BeInt)kDisplayModeDesc.m_uHeight) )
            {
              iLastWidth = kDisplayModeDesc.m_uWidth;
              iLastHeight = kDisplayModeDesc.m_uHeight;

              BeRenderTypes::BeRenderDeviceDisplayMode kDisplayMode;
              kDisplayMode.m_uResWidth = kDisplayModeDesc.m_uWidth;
              kDisplayMode.m_uResHeight = kDisplayModeDesc.m_uHeight;
              kDisplayMode.m_kFormat = (BeInt)kDisplayModeDesc.m_iFormat;
              kDisplayMode.m_iRefreshRate = (BeShort)kDisplayModeDesc.m_uRefreshRate;
              
              kDeviceAdapter.m_kEnumWindowedDisplayModes.push_back(kDisplayMode);
              kDeviceAdapter.m_kEnumFullDisplayModes.push_back(kDisplayMode);
            }
          }

        
      //}
    }
  }

}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeRenderDeviceGLEnumerator::FindBestDisplayMode( BeUInt uAdapterOrdinal, const BeRenderTypes::BeRenderDeviceSettings& kRenderDeviceSettings, BeRenderTypes::BeRenderDeviceDisplayMode* pDisplayMode )
{

  BE_UNUSED(uAdapterOrdinal);
  BE_UNUSED(kRenderDeviceSettings);
  BE_UNUSED(pDisplayMode);

  BeGLEnumAdapterInfo* pAdapterInfo = m_kAdapterInfoList.at(uAdapterOrdinal);
  for ( BeUInt i = 0; i < pAdapterInfo->m_kDeviceInfoList.size(); ++i )
  {
    BeGLEnumDeviceInfo* pDeviceInfo = pAdapterInfo->m_kDeviceInfoList.at(i);
    if ( pDeviceInfo->m_kDeviceType == DEVICE_ICD_ACCELERATED )
    {
      for ( BeUInt j = 0; j < pDeviceInfo->m_kDeviceSettingsComboList.size(); ++j )
      {
        BeRenderDeviceGLEnumerator::BeGLEnumDeviceSettingsCombo* pSettingsCombo = pDeviceInfo->m_kDeviceSettingsComboList.at(j);  
        for ( BeUInt k = 0; k < pAdapterInfo->m_kDisplayModeList.size(); ++k )
        {
          BeGLDisplayMode& kDisplayModeDesc = pAdapterInfo->m_kDisplayModeList.at(k);
          if ( kDisplayModeDesc.m_uWidth == kRenderDeviceSettings.m_uBufferWidth && kDisplayModeDesc.m_uHeight == kRenderDeviceSettings.m_uBufferHeight )
          {
            m_pCurrentSelectedSettingsCombo = pSettingsCombo;
            pDisplayMode->m_uResWidth = kDisplayModeDesc.m_uWidth;
            pDisplayMode->m_uResHeight = kDisplayModeDesc.m_uHeight;
            pDisplayMode->m_kFormat = kDisplayModeDesc.m_iFormat;
            pDisplayMode->m_bWindowed = kRenderDeviceSettings.m_bWindowed;
            pDisplayMode->m_iRefreshRate = (BeShort)kDisplayModeDesc.m_uRefreshRate;
            return BE_TRUE;
          }
        }
        BeInt iTemporarySettings = -1;
        for ( BeUInt k = 0; k < pAdapterInfo->m_kDisplayModeList.size(); ++k )
        {
          BeGLDisplayMode& kDisplayModeDesc = pAdapterInfo->m_kDisplayModeList.at(k);
          if ( kDisplayModeDesc.m_uWidth <= kRenderDeviceSettings.m_uBufferWidth && kDisplayModeDesc.m_uHeight <= kRenderDeviceSettings.m_uBufferHeight )
          {
            iTemporarySettings = k;
          }
          else if ( kDisplayModeDesc.m_uWidth > kRenderDeviceSettings.m_uBufferWidth || kDisplayModeDesc.m_uHeight > kRenderDeviceSettings.m_uBufferHeight )
          {
            if ( iTemporarySettings == -1 )
              iTemporarySettings = k;

            m_pCurrentSelectedSettingsCombo = pSettingsCombo;
            BeGLDisplayMode& kDisplayModeDescTmp = pAdapterInfo->m_kDisplayModeList.at(iTemporarySettings);
            pDisplayMode->m_uResWidth = kDisplayModeDescTmp.m_uWidth;
            pDisplayMode->m_uResHeight = kDisplayModeDescTmp.m_uHeight;
            pDisplayMode->m_kFormat = (BeInt)kDisplayModeDescTmp.m_iFormat;
            pDisplayMode->m_bWindowed = kRenderDeviceSettings.m_bWindowed;
            pDisplayMode->m_iRefreshRate = (BeShort)kDisplayModeDescTmp.m_uRefreshRate;
            return BE_TRUE;
          }
        }

      }
    }
  }

  return BE_FALSE;
}
/////////////////////////////////////////////////////////////////////////////

#endif
