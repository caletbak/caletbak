/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeVertexBufferGL.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEVERTEXBUFFERGL_H
#define BE_BEVERTEXBUFFERGL_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>
#include <Render/BeVertexBuffer.h>

/////////////////////////////////////////////////
/// \class BeVertexBufferGL
/// \brief High-level API vertex buffer object
/////////////////////////////////////////////////
class RENDER_API BeVertexBufferGL : public BeVertexBuffer
{

public:

    // Methods
    // {

        /// \brief  Constructor
        BeVertexBufferGL();

        /// \brief  Destructor
        ~BeVertexBufferGL ();

    // }



    // Getters
    // {

        /// \brief  Returns the direct pointer to the vertex buffer API impl
        virtual BeVoidP GetVertexBufferPtr(void);

    // }



    // Setters
    // {

        /// \brief  Sets the direct pointer to the vertex buffer API impl
        virtual void SetVertexBufferPtr(BeVoidP pVertexBufferPtr, BeSize_T iVertexSize);

    // }

private:

    // Forward declarations
    class BeVertexBufferGLImpl;

    /// \brief  API Implementation
    BeVertexBufferGLImpl* m_pVertexBufferImpl;

};

#endif // #ifndef BE_BEVERTEXBUFFERGL_H
