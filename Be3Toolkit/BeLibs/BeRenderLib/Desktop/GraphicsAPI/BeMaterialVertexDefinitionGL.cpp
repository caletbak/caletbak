/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeMaterialVertexDefinitionGL.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <GraphicsAPI/BeMaterialVertexDefinitionGL.h>
#include <Render/BeMaterial.h>

#if TARGET_WINDOWS

// OpenGL libs
#pragma comment (lib, "opengl32.lib")
#pragma comment (lib, "OpenGLEWLib.lib")

#include <windows.h>

#include <GL/glew.h>

/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
class RENDER_API BeMaterialVertexDefinitionGL::BeVertexDefinitionGLImpl
{

public:

    /// \brief  Constructor
    BeVertexDefinitionGLImpl();
    /// \brief  Destructor
    ~BeVertexDefinitionGLImpl();

    /// \brief  API Vertex definition object
    GLuint m_uVertexDefinitionAPI;

};
/////////////////////////////////////////////////////////////////////////////
BeMaterialVertexDefinitionGL::BeVertexDefinitionGLImpl::BeVertexDefinitionGLImpl()
:
    m_uVertexDefinitionAPI(0)
{
}
/////////////////////////////////////////////////////////////////////////////
BeMaterialVertexDefinitionGL::BeVertexDefinitionGLImpl::~BeVertexDefinitionGLImpl()
{
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeMaterialVertexDefinitionGL::BeMaterialVertexDefinitionGL()
:
    m_pVertexDefinitionImpl(NULL)
{
    m_pVertexDefinitionImpl = BeNew BeVertexDefinitionGLImpl();
}
/////////////////////////////////////////////////////////////////////////////
BeMaterialVertexDefinitionGL::~BeMaterialVertexDefinitionGL()
{
    if (m_pVertexDefinitionImpl != NULL)
    {
        BeDelete m_pVertexDefinitionImpl;
        m_pVertexDefinitionImpl = NULL;
    }
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeMaterialVertexDefinitionGL::PrepareVertexDefinition(BeRenderDevice* pRenderDevice, BeMaterialProgram* pMaterialProgram)
{
    BE_UNUSED(pRenderDevice);
    BE_UNUSED(pMaterialProgram);
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeVoidP BeMaterialVertexDefinitionGL::GetVertexDefinitionPtr(void)
{
    return &m_pVertexDefinitionImpl->m_uVertexDefinitionAPI;
}
/////////////////////////////////////////////////////////////////////////////
void BeMaterialVertexDefinitionGL::SetVertexDefinitionPtr(BeVoidP pVertexDefinitionPtr)
{
    GLuint uConstantBufferAPI = *(reinterpret_cast<GLuint*>(pVertexDefinitionPtr));

    m_pVertexDefinitionImpl->m_uVertexDefinitionAPI = uConstantBufferAPI;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeInt BeMaterialVertexDefinitionGL::ConvertAttributeTypeToAPIFormat(BeMaterialVertexAttribute::eBeMaterialAttributeType eType)
{
    BE_UNUSED(eType);

    return 0;
}
/////////////////////////////////////////////////////////////////////////////

#endif

