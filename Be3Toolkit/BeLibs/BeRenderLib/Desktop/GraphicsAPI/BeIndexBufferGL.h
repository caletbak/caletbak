/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeIndexBufferGL.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEINDEXBUFFERGL_H
#define BE_BEINDEXBUFFERGL_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>
#include <Render/BeIndexBuffer.h>

/////////////////////////////////////////////////
/// \class BeIndexBufferGL
/// \brief High-level API index buffer object
/////////////////////////////////////////////////
class RENDER_API BeIndexBufferGL : public BeIndexBuffer
{

public:

    // Methods
    // {

        /// \brief  Constructor
        BeIndexBufferGL();

        /// \brief  Destructor
        ~BeIndexBufferGL();

    // }



    // Getters
    // {

        /// \brief  Returns the direct pointer to the index buffer API impl
        virtual BeVoidP GetIndexBufferPtr(void);

    // }



    // Setters
    // {

        /// \brief  Sets the direct pointer to the index buffer API impl
        virtual void SetIndexBufferPtr(BeVoidP pIndexBufferPtr, BeInt iIndicesCount);

    // }

private:

    // Forward declarations
    class BeIndexBufferGLImpl;

    /// \brief  API Implementation
    BeIndexBufferGLImpl* m_pIndexBufferImpl;

};

#endif // #ifndef BE_BEINDEXBUFFERGL_H
