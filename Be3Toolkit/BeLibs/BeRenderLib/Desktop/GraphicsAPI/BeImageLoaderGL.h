/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeImageLoaderGL.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEIMAGELOADERGL_H
#define BE_BEIMAGELOADERGL_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>
#include <IO/BeImageLoader.h>

/////////////////////////////////////////////////
/// \class  BeImageLoaderGL
/// \brief  Image loader helper GL API
/////////////////////////////////////////////////
class RENDER_API BeImageLoaderGL : public BeImageLoader
{
public:

    /// \brief  Constructor
    BeImageLoaderGL();
    /// \brief  Destructor
    ~BeImageLoaderGL();



    // \brief  Methods
    // {

        /// \brief  Loads an image if possible from a buffer
        virtual BeVoidP LoadFileImage(BeVoidP pData, BeInt iDataSize);

        /// \brief  Generates the pixel array from a previous loaded image
        virtual BeVector4D* GeneratePixelArray(void);

        /// \brief  Releases the current loaded API image
        virtual void Release(void);

    // }



    // \brief  Getters
    // {

        /// \brief  Returns the image width after being loaded, -1 otherwise
        virtual BeInt GetImageWidth(void);

        /// \brief  Returns the image height after being loaded, -1 otherwise
        virtual BeInt GetImageHeight(void);

    // }

private:

    friend class BeRenderDeviceGL;

    /// \brief  Init the static engine resources
    static void InitStaticResources(void);

    /// \brief  Destroy the static engine resources
    static void DestroyStaticResources(void);

};

#endif // #ifndef BE_BEIMAGELOADERGL_H
