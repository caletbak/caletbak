/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeTextureSamplerGL.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <GraphicsAPI/BeTextureSamplerGL.h>
#include <GraphicsAPI/BeRenderDeviceGL.h>
#include <GraphicsAPI/BeErrorCheckGL.h>
#include <Render/BeRenderDevice.h>

#if TARGET_WINDOWS

// OpenGL libs
#pragma comment (lib, "opengl32.lib")
#pragma comment (lib, "OpenGLEWLib.lib")

#include <GL/glew.h>

/////////////////////////////////////////////////////////////////////////////
class RENDER_API BeTextureSamplerGL::BeTextureSamplerGLImpl
{

public:

    /// \brief  Constructor
    BeTextureSamplerGLImpl();
    /// \brief  Destructor
    ~BeTextureSamplerGLImpl();

    /// \brief  API texture sampler object
    GLuint m_uTextureSampler;
};
/////////////////////////////////////////////////////////////////////////////
BeTextureSamplerGL::BeTextureSamplerGLImpl::BeTextureSamplerGLImpl()
:
    m_uTextureSampler(0)
{
}
/////////////////////////////////////////////////////////////////////////////
BeTextureSamplerGL::BeTextureSamplerGLImpl::~BeTextureSamplerGLImpl()
{
    if (m_uTextureSampler > 0)
    {
        glDeleteSamplers(1, &m_uTextureSampler);
        m_uTextureSampler = 0;
    }
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeTextureSamplerGL::PrepareSampler(BeRenderDevice* pRenderDevice)
{
    GLuint uTextureSampler = 0;
    glGenSamplers(1, &uTextureSampler);

    glSamplerParameteri(uTextureSampler, GL_TEXTURE_WRAP_S, ConvertAddressToAPI(m_strAddressU));
    glSamplerParameteri(uTextureSampler, GL_TEXTURE_WRAP_T, ConvertAddressToAPI(m_strAddressV));
    glSamplerParameteri(uTextureSampler, GL_TEXTURE_WRAP_R, ConvertAddressToAPI(m_strAddressW));
    BeFloat kBorderColor[4];
    kBorderColor[0] = m_kBorderColor.m_fX;
    kBorderColor[1] = m_kBorderColor.m_fY;
    kBorderColor[2] = m_kBorderColor.m_fZ;
    kBorderColor[3] = m_kBorderColor.m_fW;
    glSamplerParameterfv(uTextureSampler, GL_TEXTURE_BORDER_COLOR, &kBorderColor[0]);
    glSamplerParameteri(uTextureSampler, GL_TEXTURE_COMPARE_FUNC, ConvertComparisionFuncToAPI(m_strComparisionFunc));
    glSamplerParameteri(uTextureSampler, GL_TEXTURE_MAG_FILTER, ConvertMagFilterToAPI(m_strFilterMag));
    glSamplerParameteri(uTextureSampler, GL_TEXTURE_MIN_FILTER, ConvertMinMipFilterToAPI(m_strFilterMin, m_strFilterMip));
    glSamplerParameterf(uTextureSampler, GL_TEXTURE_MAX_ANISOTROPY_EXT, (BeFloat) m_MaxAnisotropy);
    glSamplerParameterf(uTextureSampler, GL_TEXTURE_MAX_LOD, m_MaxLOD);
    glSamplerParameterf(uTextureSampler, GL_TEXTURE_MIN_LOD, m_MinLOD);
    glSamplerParameterf(uTextureSampler, GL_TEXTURE_LOD_BIAS, m_MipLODBias);

    CheckGLError();

    m_pTextureSamplerImpl->m_uTextureSampler = uTextureSampler;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeTextureSamplerGL::BeTextureSamplerGL()
:
m_pTextureSamplerImpl(NULL)
{
    m_pTextureSamplerImpl = BeNew BeTextureSamplerGLImpl();
}
/////////////////////////////////////////////////////////////////////////////
BeTextureSamplerGL::~BeTextureSamplerGL()
{
    if (m_pTextureSamplerImpl != NULL)
    {
        BeDelete m_pTextureSamplerImpl;
        m_pTextureSamplerImpl = NULL;
    }
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeVoidP BeTextureSamplerGL::GetTextureSamplerPtr(void)
{
    return &m_pTextureSamplerImpl->m_uTextureSampler;
}
/////////////////////////////////////////////////////////////////////////////
void BeTextureSamplerGL::SetTextureSamplerPtr(BeVoidP pSamplerPtr)
{
    GLuint uSamplerAPI = *(reinterpret_cast<GLuint*>(pSamplerPtr));

    m_pTextureSamplerImpl->m_uTextureSampler = uSamplerAPI;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeInt BeTextureSamplerGL::ConvertMinMipFilterToAPI(const BeString16& strMinFilter, const BeString16& strMipFilter)
{
    BeInt iFilterResult = 0;

    BeInt iMinFilter = GL_NEAREST;
    BeInt iMipFilter = GL_NEAREST;

    if (strMinFilter.Compare(L"LINEAR") == 0)
    {
        iMinFilter = GL_LINEAR;
    }
    if (strMipFilter.Compare(L"LINEAR") == 0)
    {
        iMipFilter = GL_LINEAR;
    }

    if (iMinFilter == GL_NEAREST)
    {
        if (iMipFilter == GL_NEAREST)
        {
            iFilterResult = GL_NEAREST_MIPMAP_NEAREST;
        }
        else
        {
            iFilterResult = GL_NEAREST_MIPMAP_LINEAR;
        }
    }
    else
    {
        if (iMipFilter == GL_NEAREST)
        {
            iFilterResult = GL_LINEAR_MIPMAP_NEAREST;
        }
        else
        {
            iFilterResult = GL_LINEAR_MIPMAP_LINEAR;
        }
    }

    return iFilterResult;
}
/////////////////////////////////////////////////////////////////////////////
BeInt BeTextureSamplerGL::ConvertMagFilterToAPI(const BeString16& strMagFilter)
{
    BeInt iFilterResult = GL_NEAREST;

    if (strMagFilter.Compare(L"LINEAR") == 0)
    {
        iFilterResult = GL_LINEAR;
    }

    return iFilterResult;
}
/////////////////////////////////////////////////////////////////////////////
BeInt BeTextureSamplerGL::ConvertAddressToAPI(const BeString16& strAddress)
{
    if (strAddress.Compare(L"WRAP") == 0)
    {
        return GL_REPEAT;
    }
    else if (strAddress.Compare(L"MIRROR") == 0)
    {
        return GL_MIRRORED_REPEAT;
    }
    else if (strAddress.Compare(L"CLAMP") == 0)
    {
        return GL_CLAMP;
    }
    else if (strAddress.Compare(L"BORDER") == 0)
    {
        return GL_CLAMP_TO_BORDER;
    }
    else if (strAddress.Compare(L"MIRROR_ONCE") == 0)
    {
        return GL_MIRRORED_REPEAT;
    }

    return 0;
}
/////////////////////////////////////////////////////////////////////////////
BeInt BeTextureSamplerGL::ConvertComparisionFuncToAPI(const BeString16& strFunc)
{
    if (strFunc.Compare(L"NEVER") == 0)
    {
        return GL_NEVER;
    }
    else if (strFunc.Compare(L"LESS") == 0)
    {
        return GL_LESS;
    }
    else if (strFunc.Compare(L"EQUAL") == 0)
    {
        return GL_EQUAL;
    }
    else if (strFunc.Compare(L"LESS_EQUAL") == 0)
    {
        return GL_LEQUAL;
    }
    else if (strFunc.Compare(L"GREATER") == 0)
    {
        return GL_GREATER;
    }
    else if (strFunc.Compare(L"NOT_EQUAL") == 0)
    {
        return GL_NOTEQUAL;
    }
    else if (strFunc.Compare(L"GREATER_EQUAL") == 0)
    {
        return GL_GEQUAL;
    }
    else if (strFunc.Compare(L"ALWAYS") == 0)
    {
        return GL_ALWAYS;
    }

    return 0;
}
/////////////////////////////////////////////////////////////////////////////

#endif

