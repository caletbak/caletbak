/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeIndexBufferGL.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <GraphicsAPI/BeConstantBufferGL.h>
#include <GraphicsAPI/BeRenderDeviceGL.h>
#include <Managers/BeRenderManager.h>

#if TARGET_WINDOWS

// OpenGL libs
#pragma comment (lib, "opengl32.lib")
#pragma comment (lib, "OpenGLEWLib.lib")

#include <windows.h>

#include <GL/glew.h>

/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
class RENDER_API BeConstantBufferGL::BeConstantBufferGLImpl
{

public:

    /// \brief  Constructor
    BeConstantBufferGLImpl();
    /// \brief  Destructor
    ~BeConstantBufferGLImpl();

    /// \brief  API Constant buffer UBO object
    GLuint m_uConstantBufferAPI;

};
/////////////////////////////////////////////////////////////////////////////
BeConstantBufferGL::BeConstantBufferGLImpl::BeConstantBufferGLImpl()
:
    m_uConstantBufferAPI(0)
{
}
/////////////////////////////////////////////////////////////////////////////
BeConstantBufferGL::BeConstantBufferGLImpl::~BeConstantBufferGLImpl()
{
    if (m_uConstantBufferAPI > 0)
    {
        glDeleteBuffers(1, &m_uConstantBufferAPI);
        m_uConstantBufferAPI = 0;
    }
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeConstantBufferGL::BeConstantBufferGL()
:
    m_pConstantBufferImpl(NULL)
{
    m_pConstantBufferImpl = BeNew BeConstantBufferGLImpl();
}
/////////////////////////////////////////////////////////////////////////////
BeConstantBufferGL::~BeConstantBufferGL()
{
    if (m_pConstantBufferImpl != NULL)
    {
        BeDelete m_pConstantBufferImpl;
        m_pConstantBufferImpl = NULL;
    }
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeVoidP BeConstantBufferGL::GetConstantBufferPtr(void)
{
    return &m_pConstantBufferImpl->m_uConstantBufferAPI;
}
/////////////////////////////////////////////////////////////////////////////
void BeConstantBufferGL::SetConstantBufferPtr(BeVoidP pConstantBufferPtr)
{
    GLuint uConstantBufferAPI = *(reinterpret_cast<GLuint*>(pConstantBufferPtr));

    m_pConstantBufferImpl->m_uConstantBufferAPI = uConstantBufferAPI;
}
/////////////////////////////////////////////////////////////////////////////

#endif

