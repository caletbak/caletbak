/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeRenderStatesGL.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <GraphicsAPI/BeRenderStatesGL.h>
#include <GraphicsAPI/BeRenderDeviceGL.h>
#include <GraphicsAPI/BeErrorCheckGL.h>
#include <Render/BeRenderDevice.h>

#if TARGET_WINDOWS

// OpenGL libs
#pragma comment (lib, "opengl32.lib")
#pragma comment (lib, "OpenGLEWLib.lib")

#include <GL/glew.h>

/////////////////////////////////////////////////////////////////////////////
class RENDER_API BeRenderStatesGL::BeRenderStatesGLImpl
{

public:

    /// \brief  Constructor
    BeRenderStatesGLImpl();
    /// \brief  Destructor
    ~BeRenderStatesGLImpl();



};
/////////////////////////////////////////////////////////////////////////////
BeRenderStatesGL::BeRenderStatesGLImpl::BeRenderStatesGLImpl()
{
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    CheckGLError();

    glCullFace(GL_FRONT);
    CheckGLError();

    glFrontFace(GL_CCW);
    CheckGLError();

    glEnable(GL_CULL_FACE);
    CheckGLError();

    glDisable(GL_POLYGON_OFFSET_FILL);
    glPolygonOffset(0.0f, 0.0f);
    CheckGLError();

    glDisable(GL_DEPTH_TEST);
    CheckGLError();

    glDisable(GL_SCISSOR_TEST);
    CheckGLError();

    glDisable(GL_MULTISAMPLE);
    CheckGLError();

    glDisable(GL_LINE_SMOOTH);
    CheckGLError();

    //glDisable(GL_BLEND);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glDepthMask(GL_FALSE);
}
/////////////////////////////////////////////////////////////////////////////
BeRenderStatesGL::BeRenderStatesGLImpl::~BeRenderStatesGLImpl()
{
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeRenderStatesGL::BeRenderStatesGL(BeRenderDevice* pRenderDevice)
:
    m_pRenderStatesImpl(NULL)
{
    m_pRenderDevice = pRenderDevice;

    m_pRenderStatesImpl = BeNew BeRenderStatesGLImpl();
}
/////////////////////////////////////////////////////////////////////////////
BeRenderStatesGL::~BeRenderStatesGL()
{
    if (m_pRenderStatesImpl != NULL)
    {
        BeDelete m_pRenderStatesImpl;
        m_pRenderStatesImpl = NULL;
    }
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeVoidP BeRenderStatesGL::GetRenderStatesPtr(ERenderStatesType eRSType)
{
    BE_UNUSED(eRSType);

    return NULL;
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderStatesGL::SetRenderStatesPtr(BeVoidP pRSPtr, ERenderStatesType eRSType)
{
    BE_UNUSED(pRSPtr);
    BE_UNUSED(eRSType);
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeRenderStatesGL::UpdateRenderStates()
{
    m_kPendingRenderStatesMutex.Lock();

    std::map<ERenderStateID, BeVoidP>::iterator it = m_kPendingRenderStatesUpdates.begin();
    while (it != m_kPendingRenderStatesUpdates.end())
    {
        ERenderStateID eRenderStateID = it->first;



        it++;
    }
    m_kPendingRenderStatesUpdates.clear();

    m_kPendingRenderStatesMutex.UnLock();

    //pRenderDevice->UpdateConstantBuffer(m_pMaterialImpl->m_pConstantBufferAPI, m_pMaterialImpl->m_pConstantBuffer, m_pMaterialImpl->m_iConstantBufferSize);
}
/////////////////////////////////////////////////////////////////////////////

#endif

