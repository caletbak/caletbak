/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeErrorCheckGL.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEERRORCHECKGL_H
#define BE_BEERRORCHECKGL_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>

bool _check_gl_error(const char *file, int line);

#define CheckGLError() _check_gl_error(__FILE__,__LINE__)

#endif // #ifndef BE_BEERRORCHECKGL_H
