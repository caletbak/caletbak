/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeErrorCheckGL.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <GraphicsAPI/BeErrorCheckGL.h>

#if TARGET_WINDOWS

// OpenGL libs
#pragma comment (lib, "opengl32.lib")
#pragma comment (lib, "OpenGLEWLib.lib")

#include <windows.h>

#include <GL/glew.h>

using namespace std;

bool _check_gl_error(const char *file, int line)
{
#if _DEBUG
    GLenum err(glGetError());

    bool bThereWasError = false;

    while (err != GL_NO_ERROR)
    {
        std::string error;

        switch (err)
        {
        case GL_INVALID_OPERATION:              error = "INVALID_OPERATION";                break;
        case GL_INVALID_ENUM:                   error = "INVALID_ENUM";                     break;
        case GL_INVALID_VALUE:                  error = "INVALID_VALUE";                    break;
        case GL_OUT_OF_MEMORY:                  error = "OUT_OF_MEMORY";                    break;
        case GL_INVALID_FRAMEBUFFER_OPERATION:  error = "INVALID_FRAMEBUFFER_OPERATION";    break;
        }

        BE_RENDER_WARNING("GL_%s - %s: %d", error.c_str(), file, line);

        bThereWasError = true;

        err = glGetError();
    }

    return bThereWasError;
#else
    return false;
#endif
}

#endif

