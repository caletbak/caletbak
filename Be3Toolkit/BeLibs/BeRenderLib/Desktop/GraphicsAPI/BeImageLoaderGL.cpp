/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeImageLoaderGL.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <GraphicsAPI/BeImageLoaderGL.h>

#include <FreeImage.h>



/////////////////////////////////////////////////////////////////////////////
BeImageLoaderGL::BeImageLoaderGL()
{
}
/////////////////////////////////////////////////////////////////////////////
BeImageLoaderGL::~BeImageLoaderGL()
{
    Release();
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
// FreeImage error handler
// @param fif Format / Plugin responsible for the error
// @param message Error message
void FreeImageErrorHandler(FREE_IMAGE_FORMAT fif, const char *message)
{
    BE_RENDER_DEBUG("\n*** ");
    if (fif != FIF_UNKNOWN)
    {
        BE_RENDER_DEBUG("%s Format\n", FreeImage_GetFormatFromFIF(fif));
    }
    BE_RENDER_DEBUG(message);
    BE_RENDER_DEBUG(" ***\n");
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeImageLoaderGL::InitStaticResources(void)
{
    FreeImage_Initialise(BE_FALSE);

    FreeImage_SetOutputMessage(FreeImageErrorHandler);
}
/////////////////////////////////////////////////////////////////////////////
void BeImageLoaderGL::DestroyStaticResources(void)
{
    FreeImage_DeInitialise();
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeImageLoaderGL::Release()
{
    if (m_pImageObjectAPI)
    {
        FIBITMAP* pAPIObject = reinterpret_cast<FIBITMAP*>(m_pImageObjectAPI);
        FreeImage_Unload(pAPIObject);

        m_pImageObjectAPI = NULL;
    }
}
/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
BeVoidP BeImageLoaderGL::LoadFileImage(BeVoidP pData, BeInt iDataSize)
{
    Release();

    unsigned char* pBuffer = reinterpret_cast<unsigned char*>(pData);

    // attach the binary data to a memory stream
    FIMEMORY *hmem = FreeImage_OpenMemory(pBuffer, iDataSize);

    // get the file type
    FREE_IMAGE_FORMAT fif = FreeImage_GetFileTypeFromMemory(hmem, 0);

    // load an image from the memory stream
    FIBITMAP *pNewImage = FreeImage_LoadFromMemory(fif, hmem, 0);

    // always close the memory stream
    FreeImage_CloseMemory(hmem);

    m_pImageObjectAPI = pNewImage;

    return m_pImageObjectAPI;
}
/////////////////////////////////////////////////////////////////////////////
BeVector4D* BeImageLoaderGL::GeneratePixelArray()
{
    if (m_pImagePixels)
    {
        BeDeleteArray m_pImagePixels;
        m_pImagePixels = NULL;
    }

    if (m_pImageObjectAPI)
    {
        FIBITMAP* pAPIObject = reinterpret_cast<FIBITMAP*>(m_pImageObjectAPI);

        if (FreeImage_HasPixels(pAPIObject))
        {
            unsigned width = GetImageWidth();
            unsigned height = GetImageHeight();

            BeInt iTotalPixels = width * height;

            m_pImagePixels = BeNew BeVector4D[iTotalPixels];

            FIBITMAP* pImageObjectToUse = pAPIObject;

            BeInt iBPP = FreeImage_GetBPP(pAPIObject);
            if (iBPP != 32)
            {
                pImageObjectToUse = FreeImage_ConvertTo32Bits(pAPIObject);
            }

            unsigned pitch = FreeImage_GetPitch(pImageObjectToUse);

            unsigned char* bits = (unsigned char*)FreeImage_GetBits(pImageObjectToUse);

            BeInt iCurrentPix = iTotalPixels - width;

            for (BeUInt y = 0; y < height; y++)
            {
                BYTE *pixel = (BYTE*)bits;

                for (BeUInt x = 0; x < width; x++)
                {
                    m_pImagePixels[iCurrentPix].m_fX = (pixel[FI_RGBA_RED] & 0xFF) / 255.0f;
                    m_pImagePixels[iCurrentPix].m_fY = (pixel[FI_RGBA_GREEN] & 0xFF) / 255.0f;
                    m_pImagePixels[iCurrentPix].m_fZ = (pixel[FI_RGBA_BLUE] & 0xFF) / 255.0f;
                    m_pImagePixels[iCurrentPix].m_fW = (pixel[FI_RGBA_ALPHA] & 0xFF) / 255.0f;

                    pixel += 4;
                    iCurrentPix++;
                }

                // next line
                bits += pitch;
                iCurrentPix -= (width * 2);
            }

            if (iBPP != 32)
            {
                FreeImage_Unload(pImageObjectToUse);
            }
        }
    }

    return m_pImagePixels;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeInt BeImageLoaderGL::GetImageWidth(void)
{
    if (m_pImageObjectAPI)
    {
        FIBITMAP* pAPIObject = reinterpret_cast<FIBITMAP*>(m_pImageObjectAPI);

        return FreeImage_GetWidth(pAPIObject);
    }

    return -1;
}
/////////////////////////////////////////////////////////////////////////////
BeInt BeImageLoaderGL::GetImageHeight(void)
{
    if (m_pImageObjectAPI)
    {
        FIBITMAP* pAPIObject = reinterpret_cast<FIBITMAP*>(m_pImageObjectAPI);

        return FreeImage_GetHeight(pAPIObject);
    }

    return -1;
}
/////////////////////////////////////////////////////////////////////////////
