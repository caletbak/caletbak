/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeRenderDeviceGL.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BERENDERDEVICEGL_H
#define BE_BERENDERDEVICEGL_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>

#include <Render/BeRenderDevice.h>
#include <GraphicsAPI/BeRenderDeviceGLEnumerator.h>
#include <Managers/BeMaterialsManager.h>
#include <Render/BeRenderStates.h>

#if TARGET_WINDOWS

#include <GL/glew.h>
#include <GL/wglew.h>

// Forward declarations
class BeVertexBuffer;
class BeIndexBuffer;
class BeConstantBuffer;

/////////////////////////////////////////////////
/// \class BeRenderDeviceGL
/// \brief GL rendering device
/////////////////////////////////////////////////
class RENDER_API BeRenderDeviceGL : public BeRenderDevice
{

  /// \brief  Class friendship
  friend class BeRenderDeviceGLEnumerator;
      
  public:
    
    /// \brief Constructor
    BeRenderDeviceGL();
    /// \brief Destructor
    ~BeRenderDeviceGL();

    // Getters
    // {

        /// \brief  Returns if a GL extension is available
        BeBoolean IsGLExtensionAvailable(BeString8 strGLExtension);

        /// \brief  Returns the MX current GL Context being used
        static GLEWContext* BeRenderDeviceGL::GetGLEWContext(void);

        /// \brief  Returns the MX current WGL Context being used
        static WGLEWContext* BeRenderDeviceGL::GetWGLEWContext(void);

    // }



    // Setters
    // {

        /// \brief  Sets the viewport
        virtual void SetViewport(const BeViewport& kViewport);

    // }



    // Methods
    // {

      /// \brief  Creates the render device
      virtual BeBoolean CreateRenderDevice(const BeRenderTypes::BeRenderDeviceSettings& kRenderDeviceSettings);

      /// \brief  Return the render device settings used when starting the app
      virtual const BeRenderTypes::BeRenderDeviceSettings& GetRenderDeviceSettings(void);

      /// \brief  Begins a scene
      virtual void BeginScene (const BeMatrix& kViewProjMatrix);

      /// \brief  Ends a scene
      virtual void EndScene (void);

      /// \brief  Clears current frame buffer
      virtual void Clear (const BeRenderTypes::EClearMask clearMask, const BeVector4D& clearColour, const BeFloat clearZValue, const BeInt clearStencilValue);

      /// \brief  Presents the current frame
      virtual BeBoolean Present(void);

      /// \brief  Converts a render format to API specific format
      static BeInt ConvertRenderFormatToAPIFormat(const BeRenderTypes::ERenderFormat eRenderFormat);

      /// \brief  Converts a render format to API specific format
      static BeInt ConvertDepthStencilFormatToAPIFormat(const BeRenderTypes::EDepthStencilFormat eDepthStencilFormat);

      /// \brief  Extracts and converts from pixel format descriptor to Render Format
      static BeRenderTypes::ERenderFormat ConvertPixelFormatDescToRenderFormat(const BeVoidP pPixelFormatDesc);

      /// \brief  Extracts and converts from pixel format descriptor to Depth Stencil format
      static BeRenderTypes::EDepthStencilFormat ConvertPixelFormatDescToDepthStencilFormat(const BeVoidP pPixelFormatDesc);

      /// \brief Returns the number of adapters
      static BeInt CheckNumAdapters(void);

      // Checks if OpenGL is available
      static BeBoolean CheckGL(BeUInt uAdapterOrdinal);

      // \brief  Enumerates all the available combinations of display modes and store them in m_pDeviceEnumerator
      static BeBoolean EnumerateDisplayModes(BeUInt uAdapterOrdinal, BeRenderTypes::BeDeviceAdapter& kDeviceAdapter);
    
    // }



      // Buffer Methods
      // {

          /// \brief  Creates a vertex array object (if the used API need it)
          virtual BeVertexArrayObject* CreateVertexArrayObject(void);

          /// \brief  Sets a previously created Vertex Array object for rendering
          virtual void SetVertexArrayObject(BeVertexArrayObject* pVertexArrayObject);

          /// \brief  Frees a previously created Vertex Array Object
          virtual void FreeVertexArrayObject(BeVertexArrayObject* pVertexArrayObject);

          /// \brief  Creates a vertex buffer with the incoming buffer
          virtual BeVertexBuffer* CreateVertexBuffer(BeVoidP pMeshVertices, BeSize_T iVertexStructSize, BeUInt uNumVertices, const BeString8& strVSProgram, BeVertexArrayObject* pVAO = NULL);

          /// \brief  Sets a previously created Vertex Buffer for rendering
          virtual void SetVertexBuffer(BeVertexBuffer* pVertexBuffer);

          /// \brief  Frees a previously created Vertex Buffer
          virtual void FreeVertexBuffer(BeVertexBuffer* pVertexBuffer);

          /// \brief  Creates an index buffer with the incoming buffer
          virtual BeIndexBuffer* CreateIndexBuffer(BeVoidP pMeshIndices, BeUInt uNumTriangles);

          /// \brief  Sets a previously created Index Buffer for rendering
          virtual void SetIndexBuffer(BeIndexBuffer* pIndexBuffer);

          /// \brief  Frees a previously created Index Buffer
          virtual void FreeIndexBuffer(BeIndexBuffer* pIndexBuffer);

          /// \brief  Creates a constant buffer with the incoming buffer
          virtual BeConstantBuffer* CreateConstantBuffer(BeVoidP pConstantBuffer, BeUInt uConstantBufferSize);

          /// \brief  Updates a constant buffer with the incoming buffer
          virtual void UpdateConstantBuffer(BeConstantBuffer* pConstantBuffer, BeVoidP pBuffer, BeUInt uBufferSize);

          /// \brief  Sets a previously created constant Buffer for rendering
          virtual void SetConstantBuffer(BeConstantBuffer* pConstantBuffer, BeRenderTypes::EBeMaterialPipeline eMaterialPipeline);

          /// \brief  Frees a previously created constant Buffer
          virtual void FreeConstantBuffer(BeConstantBuffer* pConstantBuffer);

          /// \brief  Renders an indexed primitive
          virtual void RenderIndexedPrimitive(BeVertexBuffer* pVB, BeIndexBuffer* pIB, BeVertexArrayObject* pVAO);

          /// \brief  Creates a vertex definition
          virtual BeMaterialVertexDefinition* CreateVertexDefinition(void);

          /// \brief  Creates the API related objects for a vertex definition previously created
          virtual BeVoidP ResolveVertexDefinition(BeMaterialVertexDefinition* pVertexDefinition, BeVoidP pInputData, BeMaterialProgram* pMaterialProgram);

          /// \brief  Creates the API related objects for a texture sampler previously created
          virtual BeVoidP ResolveTextureSampler(BeVoidP pInputData);

      // }



      // Render states Methods
      // {

          /// \brief  Creates a render state API object
          virtual BeVoidP CreateRenderState(BeRenderStates::ERenderStatesType eRSType, BeVoidP pData);

          /// \brief  Updates a render state API object
          virtual void UpdateRenderState(BeRenderStates::ERenderStatesType eRSType, BeVoidP pData);

      // }



      // Texture Methods
      // {

          /// \brief  Binds the texture to the selected channel
          virtual void BindTexture(BeInt iCacheHandle, BeTexture::ETextureChannel eChannel);

          /// \brief  Creates a texture sampler
          virtual BeTextureSampler* CreateTextureSampler(void);

          /// \brief  Sets a previously created texture sampler for rendering
          virtual void SetSampler(BeTextureSampler* pTextureSampler, BeInt iSlot, BeRenderTypes::EBeMaterialPipeline eMaterialPipeline);

      // }


      // Material Methods
      // {

          /// \brief  Sets the material for rendering
          virtual void SetMaterialForRendering(BeMaterial* pMaterial);

          /// \brief  Creates a new material program
          virtual BeMaterialProgram* CreateMaterialProgramFromBuffer(BeRenderTypes::EBeMaterialPipeline eProgramType, BeVoidP pBufferData, BeInt iBufferSize, BeString8 strFunction);

      // }


  private:

    /// \brief  Window process callback - Specific GL behaviour events
    static BeLResult CALLBACK WndProcGL(BeWindowHandle hWnd, BeUInt uMessage, BeWParam wParam, BeLParam lParam);

    /// \brief  Window process callback - Only to check GL
    static BeLResult CALLBACK WndProcCheckGL(BeWindowHandle hWnd, BeUInt uMessage, BeWParam wParam, BeLParam lParam);



    /// \brief  Functions that process Window events
    //virtual void ProcessWindowEvent(BeUInt uMessage, WPARAM wParam, LPARAM lParam);

    /// \brief  Creates the GL rendering context
    BeBoolean CreateGLContext(void);

    /// \brief  Creates a texture from buffer
    virtual BeBoolean CreateTextureFromBuffer(BeRenderTypes::BeTextureCacheEntry* pCacheTextureEntry, BeVoidP pData, BeInt iDataSize);

    /// \brief  Creates a material from buffer
    virtual BeBoolean CreateMaterialFromConfig(BeRenderTypes::BeMaterialCacheEntry* pCacheMaterialEntry, const BeRenderTypes::BeMaterialConfig& kMaterialConfig);

    /// \brief  Creates the render states
    virtual void CreateRenderStates(void);


    // Forward declarations
    class BeRenderDeviceGLImpl;

    /// \brief  Render Device API Implementation
    static BeRenderDeviceGLImpl* m_pRenderDeviceImpl;

    /// \brief  Device enumerator
    static BeRenderDeviceGLEnumerator* m_pDeviceEnumerator;

};

extern GLEWContext* s_kCurrentGLEWContext;
extern WGLEWContext* s_kCurrentWGLEWContext;

#define glewGetContext BeRenderDeviceGL::GetGLEWContext

#define wglewGetContext BeRenderDeviceGL::GetWGLEWContext

#endif

#endif // #ifndef BE_BERENDERDEVICEGL_H
