/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeMaterialVertexDefinitionGL.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEMATERIALVERTEXDEFINITIONGL_H
#define BE_BEMATERIALVERTEXDEFINITIONGL_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>
#include <Render/BeMaterialVertexDefinition.h>
#include <Render/BeMaterialVertexAttribute.h>

/////////////////////////////////////////////////
/// \class BeMaterialVertexDefinitionGL
/// \brief High-level material vertex definition object
/////////////////////////////////////////////////
class RENDER_API BeMaterialVertexDefinitionGL : public BeMaterialVertexDefinition
{

    public:

        /// \brief  Constructor
        BeMaterialVertexDefinitionGL();

        /// \brief  Destructor
        virtual ~BeMaterialVertexDefinitionGL();



        // Methods
        // {

            /// \brief  Prepares the API vertex definition with the added attributes
            virtual void PrepareVertexDefinition(BeRenderDevice* pRenderDevice, BeMaterialProgram* pMaterialProgram);

        // }

        

        // Getters
        // {

            /// \brief  Returns the direct pointer to the vertex definition API impl
            virtual BeVoidP GetVertexDefinitionPtr(void);

        // }



        // Setters
        // {

            /// \brief  Sets the direct pointer to the vertex definition API impl
            virtual void SetVertexDefinitionPtr(BeVoidP pVDefPtr);

        // }

    private:

        /// \brief  Converts the attribute type to API format
        virtual BeInt ConvertAttributeTypeToAPIFormat(BeMaterialVertexAttribute::eBeMaterialAttributeType eType);

        // Forward declarations
        class BeVertexDefinitionGLImpl;

        /// \brief  API Implementation
        BeVertexDefinitionGLImpl* m_pVertexDefinitionImpl;

};

#endif // #ifndef BE_BEMATERIALVERTEXDEFINITIONGL_H
