/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeMaterialGL.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <GraphicsAPI/BeMaterialGL.h>
#include <GraphicsAPI/BeConstantBufferGL.h>
#include <GraphicsAPI/BeRenderDeviceGL.h>
#include <GraphicsAPI/BeMaterialProgramGL.h>
#include <GraphicsAPI/BeErrorCheckGL.h>

#if TARGET_WINDOWS

// OpenGL libs
#pragma comment (lib, "opengl32.lib")
#pragma comment (lib, "OpenGLEWLib.lib")
#pragma comment (lib, "glu32.lib")

#include <GL/glew.h>

/////////////////////////////////////////////////////////////////////////////
class RENDER_API BeMaterialGL::BeMaterialGLImpl
{

  public:

    /// \brief  Constructor
    BeMaterialGLImpl();
    /// \brief  Destructor
    ~BeMaterialGLImpl();

    /// \brief  API Material VS program
    BeMaterialProgramGL* m_pProgramVS;

    /// \brief  API Material PS program
    BeMaterialProgramGL* m_pProgramPS;

    /// \brief  API Material program object
    BeInt m_uProgramLinked;
};
/////////////////////////////////////////////////////////////////////////////
BeMaterialGL::BeMaterialGLImpl::BeMaterialGLImpl()
:
    m_pProgramVS(NULL),
    m_pProgramPS(NULL),
    m_uProgramLinked(0)
{
}
/////////////////////////////////////////////////////////////////////////////
BeMaterialGL::BeMaterialGLImpl::~BeMaterialGLImpl()
{
    if (m_uProgramLinked > 0)
    {
        glDeleteProgram(m_uProgramLinked);
        m_uProgramLinked = 0;
    }

    if (m_pProgramVS != NULL)
    {
        BeDelete m_pProgramVS;
        m_pProgramVS = NULL;
    }

    if (m_pProgramPS != NULL)
    {
        BeDelete m_pProgramPS;
        m_pProgramPS = NULL;
    }
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeMaterialGL::BeMaterialGL()
:
    m_pMaterialImpl(NULL)
{
    m_pMaterialImpl = BeNew BeMaterialGLImpl();
}
/////////////////////////////////////////////////////////////////////////////
BeMaterialGL::~BeMaterialGL()
{
    if (m_pMaterialImpl != NULL)
    {
        BeDelete m_pMaterialImpl;
        m_pMaterialImpl = NULL;
    }
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeMaterialGL::PrepareMaterial(BeRenderDevice* pRenderDevice)
{
    BeUInt uUBOBindingPointIndex = 0;

    if (m_pMaterialImpl->m_pProgramVS)
    {
        m_pMaterialImpl->m_pProgramVS->PrepareProgram(pRenderDevice);
        m_pMaterialImpl->m_pProgramVS->LinkUBOWithProgram(m_pMaterialImpl->m_uProgramLinked, uUBOBindingPointIndex);

        uUBOBindingPointIndex += m_pMaterialImpl->m_pProgramVS->GetConstantIndicesCount();
    }

    if (m_pMaterialImpl->m_pProgramPS)
    {
        m_pMaterialImpl->m_pProgramPS->PrepareProgram(pRenderDevice);
        m_pMaterialImpl->m_pProgramPS->LinkUBOWithProgram(m_pMaterialImpl->m_uProgramLinked, uUBOBindingPointIndex);

        uUBOBindingPointIndex += m_pMaterialImpl->m_pProgramVS->GetConstantIndicesCount();
    }
}
/////////////////////////////////////////////////////////////////////////////
void BeMaterialGL::RequestUpdateConstant(const BeString8& strConstantName, BeVoidP pValue)
{
    if (m_pMaterialImpl->m_pProgramVS)
    {
        m_pMaterialImpl->m_pProgramVS->RequestUpdateConstant(strConstantName, pValue);
    }

    if (m_pMaterialImpl->m_pProgramPS)
    {
        m_pMaterialImpl->m_pProgramPS->RequestUpdateConstant(strConstantName, pValue);
    }
}
/////////////////////////////////////////////////////////////////////////////
void BeMaterialGL::UpdateMaterial(BeRenderDevice* pRenderDevice)
{
    if (m_pMaterialImpl->m_pProgramVS)
    {
        m_pMaterialImpl->m_pProgramVS->UpdateProgram(pRenderDevice);
    }

    if (m_pMaterialImpl->m_pProgramPS)
    {
        m_pMaterialImpl->m_pProgramPS->UpdateProgram(pRenderDevice);
    }
}
/////////////////////////////////////////////////////////////////////////////
void BeMaterialGL::SetConstantBuffers(BeRenderDevice* pRenderDevice)
{
    if (m_pMaterialImpl->m_pProgramVS)
    {
        m_pMaterialImpl->m_pProgramVS->SetConstantBuffer(pRenderDevice);
    }

    if (m_pMaterialImpl->m_pProgramPS)
    {
        m_pMaterialImpl->m_pProgramPS->SetConstantBuffer(pRenderDevice);
    }
}
/////////////////////////////////////////////////////////////////////////////
void BeMaterialGL::SetTextureSamplers(BeRenderDevice* pRenderDevice)
{
    if (m_pMaterialImpl->m_pProgramVS)
    {
        m_pMaterialImpl->m_pProgramVS->SetTextureSamplers(pRenderDevice);
    }

    if (m_pMaterialImpl->m_pProgramPS)
    {
        m_pMaterialImpl->m_pProgramPS->SetTextureSamplers(pRenderDevice);
    }
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeMaterialProgram* BeMaterialGL::GetMaterialProgram(BeRenderTypes::EBeMaterialPipeline eMaterialStep)
{
    switch (eMaterialStep)
    {
        case BeRenderTypes::MATERIAL_PIPELINE_VERTEX:
        {
            return m_pMaterialImpl->m_pProgramVS;
        }
        case BeRenderTypes::MATERIAL_PIPELINE_PIXEL:
        {
            return m_pMaterialImpl->m_pProgramPS;
        }
    }
    return NULL;
}
/////////////////////////////////////////////////////////////////////////////
BeInt BeMaterialGL::GetMaterialLinkedProgram()
{
    return m_pMaterialImpl->m_uProgramLinked;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeMaterialGL::SetMaterialForRendering(BeVoidP pRenderContext)
{
    if (m_pMaterialImpl->m_uProgramLinked > 0)
    {
        glUseProgram(m_pMaterialImpl->m_uProgramLinked);

        CheckGLError();
    }
}
/////////////////////////////////////////////////////////////////////////////
void BeMaterialGL::SetMaterialProgram(BeMaterialProgram* pMaterialProgram, BeRenderTypes::EBeMaterialPipeline eMaterialStep)
{
    switch (eMaterialStep)
    {
        case BeRenderTypes::MATERIAL_PIPELINE_VERTEX:
        {
            m_pMaterialImpl->m_pProgramVS = (BeMaterialProgramGL*)pMaterialProgram;
            break;
        }
        case BeRenderTypes::MATERIAL_PIPELINE_PIXEL:
        {
            m_pMaterialImpl->m_pProgramPS = (BeMaterialProgramGL*)pMaterialProgram;
            break;
        }
    }
}
/////////////////////////////////////////////////////////////////////////////
void BeMaterialGL::SetMaterialProgramLinked(BeUInt uProgramLinked)
{
    m_pMaterialImpl->m_uProgramLinked = uProgramLinked;
}
/////////////////////////////////////////////////////////////////////////////

#endif

