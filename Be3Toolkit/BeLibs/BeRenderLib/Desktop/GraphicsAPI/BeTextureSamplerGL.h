/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeTextureSamplerGL.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BETEXTURESAMPLERGL_H
#define BE_BETEXTURESAMPLERGL_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>
#include <Render/BeTextureSampler.h>

/////////////////////////////////////////////////
/// \class BeTextureSamplerGL
/// \brief High-level texture sampler API
/////////////////////////////////////////////////
class RENDER_API BeTextureSamplerGL : public BeTextureSampler
{

  public:

      /// \brief  Constructor
      BeTextureSamplerGL();
      /// \brief  Destructor
      ~BeTextureSamplerGL();



      // Methods
      // {

        /// \brief  Prepares the texture sampler internally
        virtual void PrepareSampler(BeRenderDevice* pRenderDevice);

      // }



      // Getters
      // {

        /// \brief  Returns the direct pointer to the texture sampler API impl
        virtual BeVoidP GetTextureSamplerPtr(void);

      // }



      // Setters
      // {

        /// \brief  Sets the direct pointer to the texture sampler API impl
        virtual void SetTextureSamplerPtr(BeVoidP pSamplerPtr);

      // }

  private:

    /// \brief  Converts the min filters to API value
    BeInt ConvertMinMipFilterToAPI(const BeString16& strMinFilter, const BeString16& strMipFilter);

    /// \brief  Converts the mag filters to API value
    BeInt ConvertMagFilterToAPI(const BeString16& strMagFilter);

    /// \brief  Converts an address name to API value
    virtual BeInt ConvertAddressToAPI(const BeString16& strAddress);

    /// \brief  Converts a comparision func to API value
    virtual BeInt ConvertComparisionFuncToAPI(const BeString16& strFunc);

  private:

    // Forward declarations
    class BeTextureSamplerGLImpl;

    /// \brief  Texture sampler API Implementation
    BeTextureSamplerGLImpl* m_pTextureSamplerImpl;

};

#endif // #ifndef BE_BETEXTURESAMPLERGL_H
