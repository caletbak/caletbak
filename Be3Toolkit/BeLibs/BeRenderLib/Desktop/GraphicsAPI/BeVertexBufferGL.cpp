/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeVertexBufferGL.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <GraphicsAPI/BeVertexBufferGL.h>
#include <GraphicsAPI/BeRenderDeviceGL.h>
#include <Managers/BeRenderManager.h>

#if TARGET_WINDOWS

// OpenGL libs
#pragma comment (lib, "opengl32.lib")
#pragma comment (lib, "OpenGLEWLib.lib")

#include <windows.h>

#include <GL/glew.h>

/////////////////////////////////////////////////////////////////////////////
class RENDER_API BeVertexBufferGL::BeVertexBufferGLImpl
{

public:

    /// \brief  Constructor
    BeVertexBufferGLImpl();
    /// \brief  Destructor
    ~BeVertexBufferGLImpl();

    /// \brief  API Material object
    GLuint m_uVertexBufferAPI;

};
/////////////////////////////////////////////////////////////////////////////
BeVertexBufferGL::BeVertexBufferGLImpl::BeVertexBufferGLImpl()
:
    m_uVertexBufferAPI(0)
{
}
/////////////////////////////////////////////////////////////////////////////
BeVertexBufferGL::BeVertexBufferGLImpl::~BeVertexBufferGLImpl()
{
    if (m_uVertexBufferAPI > 0)
    {
        glDeleteBuffers(1, &m_uVertexBufferAPI);
        m_uVertexBufferAPI = 0;
    }
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////

BeVertexBufferGL::BeVertexBufferGL()
:
    m_pVertexBufferImpl(NULL)
{
    m_pVertexBufferImpl = BeNew BeVertexBufferGLImpl();
}

/////////////////////////////////////////////////////////////////////////////

BeVertexBufferGL::~BeVertexBufferGL()
{
    if (m_pVertexBufferImpl != NULL)
    {
        BeDelete m_pVertexBufferImpl;
        m_pVertexBufferImpl = NULL;
    }
}

/////////////////////////////////////////////////////////////////////////////

BeVoidP BeVertexBufferGL::GetVertexBufferPtr(void)
{
    return &m_pVertexBufferImpl->m_uVertexBufferAPI;
}

/////////////////////////////////////////////////////////////////////////////

void BeVertexBufferGL::SetVertexBufferPtr(BeVoidP pVertexBufferPtr, BeSize_T iVertexSize)
{
    GLuint uVertexBufferAPI = *(reinterpret_cast<GLuint*>(pVertexBufferPtr));

    m_pVertexBufferImpl->m_uVertexBufferAPI = uVertexBufferAPI;

    m_iVertexSize = iVertexSize;
}

/////////////////////////////////////////////////////////////////////////////

#endif
