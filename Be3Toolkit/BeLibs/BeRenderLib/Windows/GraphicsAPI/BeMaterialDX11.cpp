/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeMaterialDX11.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <GraphicsAPI/BeMaterialDX11.h>
#include <GraphicsAPI/BeConstantBufferDX11.h>
#include <GraphicsAPI/BeTextureSamplerDX11.h>
#include <GraphicsAPI/BeMaterialProgramDX11.h>
#include <Render/BeRenderDevice.h>
#include <Render/BeMaterialConstant.h>
#include <Render/BeMaterialVertexDefinition.h>

// DirectX11 libs
#pragma comment (lib, "d3d11.lib") 

#include <d3d11.h>

/////////////////////////////////////////////////////////////////////////////
class RENDER_API BeMaterialDX11::BeMaterialDX11Impl
{

  public:

    /// \brief  Constructor
    BeMaterialDX11Impl();
    /// \brief  Destructor
    ~BeMaterialDX11Impl();

    /// \brief  API Material VS program
    BeMaterialProgramDX11* m_pProgramVS;

    /// \brief  API Material PS program
    BeMaterialProgramDX11* m_pProgramPS;
};
/////////////////////////////////////////////////////////////////////////////
BeMaterialDX11::BeMaterialDX11Impl::BeMaterialDX11Impl()
:
    m_pProgramVS(NULL),
    m_pProgramPS(NULL)
{
}
/////////////////////////////////////////////////////////////////////////////
BeMaterialDX11::BeMaterialDX11Impl::~BeMaterialDX11Impl()
{
    if (m_pProgramVS != NULL)
    {
        BeDelete m_pProgramVS;
        m_pProgramVS = NULL;
    }

    if (m_pProgramPS != NULL)
    {
        BeDelete m_pProgramPS;
        m_pProgramPS = NULL;
    }
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeMaterialDX11::BeMaterialDX11()
:
    m_pMaterialImpl(NULL)
{
    m_pMaterialImpl = BeNew BeMaterialDX11Impl();
}
/////////////////////////////////////////////////////////////////////////////
BeMaterialDX11::~BeMaterialDX11()
{
    if (m_pMaterialImpl != NULL)
    {
        BeDelete m_pMaterialImpl;
        m_pMaterialImpl = NULL;
    }
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeMaterialDX11::PrepareMaterial(BeRenderDevice* pRenderDevice)
{
    if (m_pMaterialImpl->m_pProgramVS)
    {
        m_pMaterialImpl->m_pProgramVS->PrepareProgram(pRenderDevice);
    }

    if (m_pMaterialImpl->m_pProgramPS)
    {
        m_pMaterialImpl->m_pProgramPS->PrepareProgram(pRenderDevice);
    }
}
/////////////////////////////////////////////////////////////////////////////
void BeMaterialDX11::RequestUpdateConstant(const BeString8& strConstantName, BeVoidP pValue)
{
    if (m_pMaterialImpl->m_pProgramVS)
    {
        m_pMaterialImpl->m_pProgramVS->RequestUpdateConstant(strConstantName, pValue);
    }

    if (m_pMaterialImpl->m_pProgramPS)
    {
        m_pMaterialImpl->m_pProgramPS->RequestUpdateConstant(strConstantName, pValue);
    }
}
/////////////////////////////////////////////////////////////////////////////
void BeMaterialDX11::UpdateMaterial(BeRenderDevice* pRenderDevice)
{
    if (m_pMaterialImpl->m_pProgramVS)
    {
        m_pMaterialImpl->m_pProgramVS->UpdateProgram(pRenderDevice);
    }

    if (m_pMaterialImpl->m_pProgramPS)
    {
        m_pMaterialImpl->m_pProgramPS->UpdateProgram(pRenderDevice);
    }
}
/////////////////////////////////////////////////////////////////////////////
void BeMaterialDX11::SetConstantBuffers(BeRenderDevice* pRenderDevice)
{
    if (m_pMaterialImpl->m_pProgramVS)
    {
        m_pMaterialImpl->m_pProgramVS->SetConstantBuffer(pRenderDevice);
    }

    if (m_pMaterialImpl->m_pProgramPS)
    {
        m_pMaterialImpl->m_pProgramPS->SetConstantBuffer(pRenderDevice);
    }
}
/////////////////////////////////////////////////////////////////////////////
void BeMaterialDX11::SetTextureSamplers(BeRenderDevice* pRenderDevice)
{
    if (m_pMaterialImpl->m_pProgramVS)
    {
        m_pMaterialImpl->m_pProgramVS->SetTextureSamplers(pRenderDevice);
    }

    if (m_pMaterialImpl->m_pProgramPS)
    {
        m_pMaterialImpl->m_pProgramPS->SetTextureSamplers(pRenderDevice);
    }
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeMaterialProgram* BeMaterialDX11::GetMaterialProgram(BeRenderTypes::EBeMaterialPipeline eMaterialStep)
{
    switch (eMaterialStep)
    {
        case BeRenderTypes::MATERIAL_PIPELINE_VERTEX:
        {
            return m_pMaterialImpl->m_pProgramVS;
        }
        case BeRenderTypes::MATERIAL_PIPELINE_PIXEL:
        {
            return m_pMaterialImpl->m_pProgramPS;
        }
    }
    return NULL;
}
/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
void BeMaterialDX11::SetMaterialForRendering(BeVoidP pRenderContext)
{
    if (m_pMaterialImpl->m_pProgramVS)
    {
        m_pMaterialImpl->m_pProgramVS->SetProgramForRendering(pRenderContext);
    }

    if (m_pMaterialImpl->m_pProgramPS)
    {
        m_pMaterialImpl->m_pProgramPS->SetProgramForRendering(pRenderContext);
    }
}
/////////////////////////////////////////////////////////////////////////////
void BeMaterialDX11::SetMaterialProgram(BeMaterialProgram* pMaterialProgram, BeRenderTypes::EBeMaterialPipeline eMaterialStep)
{
    switch (eMaterialStep)
    {
        case BeRenderTypes::MATERIAL_PIPELINE_VERTEX:
        {
            m_pMaterialImpl->m_pProgramVS = (BeMaterialProgramDX11*)pMaterialProgram;
            break;
        }
        case BeRenderTypes::MATERIAL_PIPELINE_PIXEL:
        {
            m_pMaterialImpl->m_pProgramPS = (BeMaterialProgramDX11*)pMaterialProgram;
            break;
        }
    }
}
/////////////////////////////////////////////////////////////////////////////

