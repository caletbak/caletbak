/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeIndexBufferDX11.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <GraphicsAPI/BeConstantBufferDX11.h>
#include <Render/BeRenderDevice.h>
#include <Managers/BeRenderManager.h>

// DirectX11 libs
#pragma comment (lib, "d3d11.lib") 

#include <d3d11.h>

/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
class RENDER_API BeConstantBufferDX11::BeConstantBufferDX11Impl
{

public:

    /// \brief  Constructor
    BeConstantBufferDX11Impl();
    /// \brief  Destructor
    ~BeConstantBufferDX11Impl();

    /// \brief  API Constant buffer object
    ID3D11Buffer* m_pConstantBufferAPI;

};
/////////////////////////////////////////////////////////////////////////////
BeConstantBufferDX11::BeConstantBufferDX11Impl::BeConstantBufferDX11Impl()
:
    m_pConstantBufferAPI(NULL)
{
}
/////////////////////////////////////////////////////////////////////////////
BeConstantBufferDX11::BeConstantBufferDX11Impl::~BeConstantBufferDX11Impl()
{
    if (m_pConstantBufferAPI != NULL)
    {
        m_pConstantBufferAPI->Release();
        m_pConstantBufferAPI = NULL;
    }
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeConstantBufferDX11::BeConstantBufferDX11()
:
    m_pConstantBufferImpl(NULL)
{
    m_pConstantBufferImpl = BeNew BeConstantBufferDX11Impl();
}
/////////////////////////////////////////////////////////////////////////////
BeConstantBufferDX11::~BeConstantBufferDX11()
{
    if (m_pConstantBufferImpl != NULL)
    {
        BeDelete m_pConstantBufferImpl;
        m_pConstantBufferImpl = NULL;
    }
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeVoidP BeConstantBufferDX11::GetConstantBufferPtr(void)
{
    return m_pConstantBufferImpl->m_pConstantBufferAPI;
}
/////////////////////////////////////////////////////////////////////////////
void BeConstantBufferDX11::SetConstantBufferPtr(BeVoidP pConstantBufferPtr)
{
    ID3D11Buffer* pConstantBufferAPI = reinterpret_cast<ID3D11Buffer*>(pConstantBufferPtr);

    m_pConstantBufferImpl->m_pConstantBufferAPI = pConstantBufferAPI;
}
/////////////////////////////////////////////////////////////////////////////

