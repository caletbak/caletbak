/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeMaterialProgramDX11.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEMATERIALPROGRAMDX11_H
#define BE_BEMATERIALPROGRAMDX11_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>
#include <Render/BeMaterialProgram.h>
#include <BeRenderTypes.h>

/////////////////////////////////////////////////
/// \class BeMaterialProgramDX11
/// \brief High-level DX11 material program object
/////////////////////////////////////////////////
class RENDER_API BeMaterialProgramDX11 : public BeMaterialProgram
{
  public:

      /// \brief Constructor
      BeMaterialProgramDX11(BeRenderTypes::EBeMaterialPipeline eProgramType);
      /// \brief Destructor
      ~BeMaterialProgramDX11();



      // Methods
      // {

          /// \brief  Prepares the program with its constant and so
          virtual void PrepareProgram(BeRenderDevice* pRenderDevice);

          /// \brief  Updates the program and constants all together
          virtual void UpdateProgram(BeRenderDevice* pRenderDevice);

          /// \brief  Sets the program constant buffers
          virtual void SetConstantBuffer(BeRenderDevice* pRenderDevice);

          /// \brief  Sets the program texture samplers
          virtual void SetTextureSamplers(BeRenderDevice* pRenderDevice);

          /// \brief  Sets the program for rendering
          virtual void SetProgramForRendering(BeVoidP pRenderContext);

      // }



      // Getters
      // {

          /// \brief  Returns the direct pointer to the material compiled data API impl
          virtual BeVoidP GetMaterialProgramCompiledDataPtr(void);

          /// \brief  Returns the number of uniform indices the constant buffer needs
          virtual BeInt GetConstantIndicesCount(void);

          /// \brief  Returns the direct pointer to the material API impl
          virtual BeVoidP GetMaterialProgramPtr(void);

      // }



      // Setters
      // {

          /// \brief  Sets the direct pointer to the material compiled data API impl
          virtual void SetMaterialProgramCompiledDataPtr(BeVoidP pMaterialCompiledDataPtr);

          /// \brief  Sets the direct pointer to the material API impl
          virtual void SetMaterialProgramPtr(BeVoidP pMaterialPtr);

      // }

private:

    // Forward declarations
    class BeMaterialProgramDX11Impl;

    /// \brief  Render Device API Implementation
    BeMaterialProgramDX11Impl* m_pMaterialProgramImpl;

};

#endif // #ifndef BE_BEMATERIALPROGRAMDX11_H
