/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeConstantBufferDX11.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BECONSTANTBUFFERDX11_H
#define BE_BECONSTANTBUFFERDX11_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>
#include <Render/BeConstantBuffer.h>

// Forward declarations
class BeRenderDevice;

/////////////////////////////////////////////////
/// \class BeConstantBufferDX11
/// \brief High-level API constant buffer object
/////////////////////////////////////////////////
class RENDER_API BeConstantBufferDX11 : public BeConstantBuffer
{

  public:

    // Methods
    // {

        /// \brief  Constructor
        BeConstantBufferDX11();

        /// \brief  Destructor
        ~BeConstantBufferDX11();

    // }



    // Getters
    // {

        /// \brief  Returns the direct pointer to the constant buffer API impl
        virtual BeVoidP GetConstantBufferPtr(void);

    // }



    // Setters
    // {

        /// \brief  Sets the direct pointer to the constant buffer API impl
        virtual void SetConstantBufferPtr(BeVoidP pConstantBufferPtr);

    // }

private:

    // Forward declarations
    class BeConstantBufferDX11Impl;

    /// \brief  API Implementation
    BeConstantBufferDX11Impl* m_pConstantBufferImpl;

};

#endif // #ifndef BE_BECONSTANTBUFFERDX11_H
