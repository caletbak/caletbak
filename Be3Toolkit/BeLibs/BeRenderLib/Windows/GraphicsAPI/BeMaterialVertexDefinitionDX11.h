/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeMaterialVertexDefinitionDX11.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEMATERIALVERTEXDEFINITIONDX11_H
#define BE_BEMATERIALVERTEXDEFINITIONDX11_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>
#include <Render/BeMaterialVertexDefinition.h>
#include <Render/BeMaterialVertexAttribute.h>

/////////////////////////////////////////////////
/// \class BeMaterialVertexDefinitionDX11
/// \brief High-level material vertex definition object
/////////////////////////////////////////////////
class RENDER_API BeMaterialVertexDefinitionDX11 : public BeMaterialVertexDefinition
{

    public:

        /// \brief  Constructor
        BeMaterialVertexDefinitionDX11();

        /// \brief  Destructor
        ~BeMaterialVertexDefinitionDX11();

        // Methods
        // {

            /// \brief  Prepares the API vertex definition with the added attributes
            virtual void PrepareVertexDefinition(BeRenderDevice* pRenderDevice, BeMaterialProgram* pMaterialProgram);

        // }

        

        // Getters
        // {

            /// \brief  Returns the direct pointer to the vertex definition API impl
            virtual BeVoidP GetVertexDefinitionPtr(void);

        // }



        // Setters
        // {

            /// \brief  Sets the direct pointer to the vertex definition API impl
            virtual void SetVertexDefinitionPtr(BeVoidP pVDefPtr);

        // }

    private:

        /// \brief  Converts the attribute type to API format
        virtual BeInt ConvertAttributeTypeToAPIFormat(BeMaterialVertexAttribute::eBeMaterialAttributeType eType);

        // Forward declarations
        class BeVertexDefinitionDX11Impl;

        /// \brief  API Implementation
        BeVertexDefinitionDX11Impl* m_pVertexDefinitionImpl;

};

#endif // #ifndef BE_BEMATERIALVERTEXDEFINITIONDX11_H
