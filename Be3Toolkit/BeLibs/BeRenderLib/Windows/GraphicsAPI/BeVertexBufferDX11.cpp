/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeVertexBufferDX11.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <GraphicsAPI/BeVertexBufferDX11.h>
#include <Render/BeRenderDevice.h>
#include <Managers/BeRenderManager.h>

// DirectX11 libs
#pragma comment (lib, "d3d11.lib") 

#include <d3d11.h>

/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
class RENDER_API BeVertexBufferDX11::BeVertexBufferDX11Impl
{

public:

    /// \brief  Constructor
    BeVertexBufferDX11Impl();
    /// \brief  Destructor
    ~BeVertexBufferDX11Impl();

    /// \brief  API Material object
    ID3D11Buffer* m_pVertexBufferAPI;

};
/////////////////////////////////////////////////////////////////////////////
BeVertexBufferDX11::BeVertexBufferDX11Impl::BeVertexBufferDX11Impl()
:
    m_pVertexBufferAPI(NULL)
{
}
/////////////////////////////////////////////////////////////////////////////
BeVertexBufferDX11::BeVertexBufferDX11Impl::~BeVertexBufferDX11Impl()
{
    if (m_pVertexBufferAPI != NULL)
    {
        m_pVertexBufferAPI->Release();
        m_pVertexBufferAPI = NULL;
    }
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////

BeVertexBufferDX11::BeVertexBufferDX11()
:
    m_pVertexBufferImpl(NULL)
{
    m_pVertexBufferImpl = BeNew BeVertexBufferDX11Impl ();
}

/////////////////////////////////////////////////////////////////////////////

BeVertexBufferDX11::~BeVertexBufferDX11()
{
    if (m_pVertexBufferImpl != NULL)
    {
        BeDelete m_pVertexBufferImpl;
        m_pVertexBufferImpl = NULL;
    }
}

/////////////////////////////////////////////////////////////////////////////

BeVoidP BeVertexBufferDX11::GetVertexBufferPtr(void)
{
    return m_pVertexBufferImpl->m_pVertexBufferAPI;
}

/////////////////////////////////////////////////////////////////////////////

void BeVertexBufferDX11::SetVertexBufferPtr(BeVoidP pVertexBufferPtr, BeSize_T iVertexSize)
{
    ID3D11Buffer* pVertexBufferAPI = reinterpret_cast<ID3D11Buffer*>(pVertexBufferPtr);

    m_pVertexBufferImpl->m_pVertexBufferAPI = pVertexBufferAPI;

    m_iVertexSize = iVertexSize;
}

/////////////////////////////////////////////////////////////////////////////

