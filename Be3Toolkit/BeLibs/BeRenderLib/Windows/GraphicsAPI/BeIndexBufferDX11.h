/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeIndexBufferDX11.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEINDEXBUFFERDX11_H
#define BE_BEINDEXBUFFERDX11_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>
#include <Render/BeIndexBuffer.h>

/////////////////////////////////////////////////
/// \class BeIndexBufferDX11
/// \brief High-level API index buffer object
/////////////////////////////////////////////////
class RENDER_API BeIndexBufferDX11 : public BeIndexBuffer
{

  public:

    // Methods
    // {

        /// \brief  Constructor
        BeIndexBufferDX11();

        /// \brief  Destructor
        ~BeIndexBufferDX11();

    // }



    // Getters
    // {

        /// \brief  Returns the direct pointer to the index buffer API impl
        virtual BeVoidP GetIndexBufferPtr(void);

    // }



    // Setters
    // {

        /// \brief  Sets the direct pointer to the index buffer API impl
        virtual void SetIndexBufferPtr(BeVoidP pIndexBufferPtr, BeInt iIndicesCount);

    // }

private:

    // Forward declarations
    class BeIndexBufferDX11Impl;

    /// \brief  API Implementation
    BeIndexBufferDX11Impl* m_pIndexBufferImpl;

};

#endif // #ifndef BE_BEINDEXBUFFERDX11_H
