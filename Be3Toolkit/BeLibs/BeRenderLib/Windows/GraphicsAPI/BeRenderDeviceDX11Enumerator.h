/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeRenderDeviceDX11Enumerator.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BERENDERDEVICEDX11ENUMERATOR_H
#define BE_BERENDERDEVICEDX11ENUMERATOR_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>

#include <Render/BeRenderDeviceEnumerator.h>



/////////////////////////////////////////////////
/// \class BeRenderDeviceDX11Enumerator
/// \brief DX11 rendering device enumerator
/////////////////////////////////////////////////
class RENDER_API BeRenderDeviceDX11Enumerator : public BeRenderDeviceEnumerator
{
      
  public:
    
    /// \brief  Constructor
    BeRenderDeviceDX11Enumerator();
    /// \brief  Destructor
    ~BeRenderDeviceDX11Enumerator();
    


    // Overload
    // {

      /// \brief  Executes all the adapters, devices and display modes enumeration processes
      virtual BeBoolean ExecuteEnumerationProcess(void);

      /// \brief  Fills the display modes array with the enumeration results
      virtual void FillDisplayModesArray( BeUInt uAdapterOrdinal, BeRenderTypes::BeDeviceAdapter& kDeviceAdapter );

      /// \brief  Find the best suitable display mode taking into account the input data
      virtual BeBoolean FindBestDisplayMode( BeUInt uAdapterOrdinal, const BeRenderTypes::BeRenderDeviceSettings& kRenderDeviceSettings, BeRenderTypes::BeRenderDeviceDisplayMode* pDisplayMode );

    // }



    // Methods
    // {

      /// \brief  Fill the swap chain desc with the found display mode desc
      void FillSwapChainDesc( const BeRenderTypes::BeRenderDeviceDisplayMode& kDisplayMode, BeVoidP pSwapChainDesc );

    // }

  private:

    // Forward declarations
    class BeDX11EnumAdapterInfo;
    class BeDX11EnumDeviceInfo;
    class BeDX11EnumOutputInfo;
    class BeDX11EnumDeviceSettingsCombo;
    
    // Methods
    // {
    
      /// \brief  Enumerates and builds the device info
      BeInt EnumerateDevices( BeDX11EnumAdapterInfo* pAdapterInfo );

      /// \brief  Enumerates and builds the outputs info
      BeInt EnumerateOutputs( BeDX11EnumAdapterInfo* pAdapterInfo );

      /// \brief  Enumerates and builds the devices combo settings info
      BeInt EnumerateDeviceCombos( BeDX11EnumAdapterInfo* pAdapterInfo );

      /// \brief  Enumerates and builds the available display modes
      BeInt EnumerateDisplayModes( BeDX11EnumOutputInfo* pOutputInfo );
      
      /// \brief  Builds the available MultiSample Quality list
      void BuildMultiSampleQualityList( BeInt format, BeDX11EnumDeviceSettingsCombo* pDeviceCombo );

    // }



    /// \brief  The DXGI factory
    void* m_pFactory;

    /// \brief  The adapters info list
    std::vector<BeDX11EnumAdapterInfo*> m_kAdapterInfoList;

    /// \brief  The current selected settings combo
    BeDX11EnumDeviceSettingsCombo* m_pCurrentSelectedSettingsCombo;
            
};
// Smart pointer BeRenderDeviceDX11Enumerator
typedef BeSmartPointer<BeRenderDeviceDX11Enumerator> BeRenderDeviceDX11EnumeratorPtr;

#endif // #ifndef BE_BERENDERDEVICEDX11ENUMERATOR_H
