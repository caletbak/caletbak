/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeIndexBufferDX11.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <GraphicsAPI/BeIndexBufferDX11.h>
#include <Render/BeRenderDevice.h>
#include <Managers/BeRenderManager.h>

// DirectX11 libs
#pragma comment (lib, "d3d11.lib") 

#include <d3d11.h>

/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
class RENDER_API BeIndexBufferDX11::BeIndexBufferDX11Impl
{

public:

    /// \brief  Constructor
    BeIndexBufferDX11Impl();
    /// \brief  Destructor
    ~BeIndexBufferDX11Impl();

    /// \brief  API Index buffer object
    ID3D11Buffer* m_pIndexBufferAPI;

};
/////////////////////////////////////////////////////////////////////////////
BeIndexBufferDX11::BeIndexBufferDX11Impl::BeIndexBufferDX11Impl()
:
    m_pIndexBufferAPI(NULL)
{
}
/////////////////////////////////////////////////////////////////////////////
BeIndexBufferDX11::BeIndexBufferDX11Impl::~BeIndexBufferDX11Impl()
{
    if (m_pIndexBufferAPI != NULL)
    {
        m_pIndexBufferAPI->Release();
        m_pIndexBufferAPI = NULL;
    }
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////

BeIndexBufferDX11::BeIndexBufferDX11()
:
    m_pIndexBufferImpl(NULL)
{
    m_pIndexBufferImpl = BeNew BeIndexBufferDX11Impl();
}

/////////////////////////////////////////////////////////////////////////////

BeIndexBufferDX11::~BeIndexBufferDX11()
{
    if (m_pIndexBufferImpl != NULL)
    {
        BeDelete m_pIndexBufferImpl;
        m_pIndexBufferImpl = NULL;
    }
}

/////////////////////////////////////////////////////////////////////////////

BeVoidP BeIndexBufferDX11::GetIndexBufferPtr(void)
{
    return m_pIndexBufferImpl->m_pIndexBufferAPI;
}

/////////////////////////////////////////////////////////////////////////////

void BeIndexBufferDX11::SetIndexBufferPtr(BeVoidP pIndexBufferPtr, BeInt iIndicesCount)
{
    ID3D11Buffer* pIndexBufferAPI = reinterpret_cast<ID3D11Buffer*>(pIndexBufferPtr);

    m_pIndexBufferImpl->m_pIndexBufferAPI = pIndexBufferAPI;

    m_iIndicesCount = iIndicesCount;
}

/////////////////////////////////////////////////////////////////////////////

