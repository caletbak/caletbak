/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeTextureSamplerDX11.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BETEXTURESAMPLERDX11_H
#define BE_BETEXTURESAMPLERDX11_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>
#include <Render/BeTextureSampler.h>

/////////////////////////////////////////////////
/// \class BeTextureSamplerDX11
/// \brief High-level texture sampler API
/////////////////////////////////////////////////
class RENDER_API BeTextureSamplerDX11 : public BeTextureSampler
{

public:

    /// \brief  Constructor
    BeTextureSamplerDX11();
    /// \brief  Destructor
    ~BeTextureSamplerDX11();



    // Methods
    // {

        /// \brief  Prepares the texture sampler internally
        virtual void PrepareSampler(BeRenderDevice* pRenderDevice);

    // }



    // Getters
    // {

        /// \brief  Returns the direct pointer to the texture sampler API impl
        virtual BeVoidP GetTextureSamplerPtr(void);

    // }



    // Setters
    // {

        /// \brief  Sets the direct pointer to the texture sampler API impl
        virtual void SetTextureSamplerPtr(BeVoidP pSamplerPtr);

    // }

private:

    /// \brief  Converts a set of filters to API value
    BeInt ConvertFilterToAPI(const BeString16& strMinFilter, const BeString16& strMagFilter, const BeString16& strMipFilter, const BeString16& strFilterFunc);

    /// \brief  Converts an address name to API value
    virtual BeInt ConvertAddressToAPI(const BeString16& strAddress);

    /// \brief  Converts a comparision func to API value
    virtual BeInt ConvertComparisionFuncToAPI(const BeString16& strFunc);

private:

    // Forward declarations
    class BeTextureSamplerDX11Impl;

    /// \brief  Texture sampler API Implementation
    BeTextureSamplerDX11Impl* m_pTextureSamplerImpl;

};

#endif // #ifndef BE_BETEXTURESAMPLERDX11_H
