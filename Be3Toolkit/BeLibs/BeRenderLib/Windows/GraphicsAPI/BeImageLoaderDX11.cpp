/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeImageLoaderDX11.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <GraphicsAPI/BeImageLoaderDX11.h>

#include <DirectXTex/DirectXTex.h>

using namespace DirectX;

// DirectX11 libs
#pragma comment (lib, "d3d11.lib") 

#include <d3d11.h>

/////////////////////////////////////////////////////////////////////////////
BeImageLoaderDX11::BeImageLoaderDX11()
{
}
/////////////////////////////////////////////////////////////////////////////
BeImageLoaderDX11::~BeImageLoaderDX11()
{
    Release();
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeImageLoaderDX11::Release()
{
    if (m_pImageObjectAPI)
    {
        ScratchImage* pAPIObject = reinterpret_cast<ScratchImage*>(m_pImageObjectAPI);
        pAPIObject->Release();

        BeDelete m_pImageObjectAPI;
        m_pImageObjectAPI = NULL;
    }
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeVoidP BeImageLoaderDX11::LoadFileImage(BeVoidP pData, BeInt iDataSize)
{
    Release();

    HRESULT hr = -1;

    ScratchImage kImageLoaded;
    hr = LoadFromWICMemory(pData, iDataSize, WIC_FLAGS_NONE, NULL, kImageLoaded);

    if (FAILED(hr))
    {
        return NULL;
    }
    else
    {
        // Generate the mipmaps for the image, on the CPU.  Different functions are used for 3D and
        // everything else (1D, 2D, cube).
        ScratchImage* kMipMapedImage = BeNew ScratchImage;
        if (kImageLoaded.GetMetadata().dimension == TEX_DIMENSION_TEXTURE3D)
        {
            hr = GenerateMipMaps3D(kImageLoaded.GetImages(), kImageLoaded.GetImageCount(), kImageLoaded.GetMetadata(), TEX_FILTER_FANT, 0, *kMipMapedImage);
        }
        else
        {
            hr = GenerateMipMaps(kImageLoaded.GetImages(), kImageLoaded.GetImageCount(), kImageLoaded.GetMetadata(), TEX_FILTER_FANT, 0, *kMipMapedImage);
        }

        kImageLoaded.Release();

        // Return on any failure.
        if (FAILED(hr))
        {
            return NULL;
        }
        else
        {
            m_pImageObjectAPI = kMipMapedImage;

            return m_pImageObjectAPI;
        }
    }

    return NULL;
}
/////////////////////////////////////////////////////////////////////////////
BeVector4D* BeImageLoaderDX11::GeneratePixelArray()
{
    if (m_pImagePixels)
    {
        BeDeleteArray m_pImagePixels;
        m_pImagePixels = NULL;
    }

    if (m_pImageObjectAPI)
    {
        ScratchImage* pAPIObject = reinterpret_cast<ScratchImage*>(m_pImageObjectAPI);

        unsigned width = GetImageWidth();
        unsigned height = GetImageHeight();

        BeInt iTotalPixels = width * height;

        m_pImagePixels = BeNew BeVector4D[iTotalPixels];

        BeInt iBPP = pAPIObject->GetPixelsSize();

        const DirectX::Image* pImage = pAPIObject->GetImage(0, 0, 0);

        unsigned char* bits = (unsigned char*) pImage->pixels;

        BeInt iCurrentPix = 0;

        for (BeUInt y = 0; y < pImage->height; y++)
        {
            BYTE *pixel = (BYTE*)bits;

            for (BeUInt x = 0; x < pImage->width; x++)
            {
                if (pImage->format == DXGI_FORMAT_B8G8R8A8_UNORM)
                {
                    m_pImagePixels[iCurrentPix].m_fX = (pixel[2] & 0xFF) / 255.0f;
                    m_pImagePixels[iCurrentPix].m_fY = (pixel[1] & 0xFF) / 255.0f;
                    m_pImagePixels[iCurrentPix].m_fZ = (pixel[0] & 0xFF) / 255.0f;
                }
                else if (pImage->format == DXGI_FORMAT_R8G8B8A8_UNORM)
                {
                    m_pImagePixels[iCurrentPix].m_fX = (pixel[0] & 0xFF) / 255.0f;
                    m_pImagePixels[iCurrentPix].m_fY = (pixel[1] & 0xFF) / 255.0f;
                    m_pImagePixels[iCurrentPix].m_fZ = (pixel[2] & 0xFF) / 255.0f;
                }
                m_pImagePixels[iCurrentPix].m_fW = (pixel[3] & 0xFF) / 255.0f;

                pixel += 4;
                iCurrentPix++;
            }

            // next line
            bits += pImage->rowPitch;
        }
    }

    return m_pImagePixels;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeInt BeImageLoaderDX11::GetImageWidth(void)
{
    if (m_pImageObjectAPI)
    {
        ScratchImage* pAPIObject = reinterpret_cast<ScratchImage*>(m_pImageObjectAPI);

        return pAPIObject->GetMetadata().width;
    }

    return -1;
}
/////////////////////////////////////////////////////////////////////////////
BeInt BeImageLoaderDX11::GetImageHeight(void)
{
    if (m_pImageObjectAPI)
    {
        ScratchImage* pAPIObject = reinterpret_cast<ScratchImage*>(m_pImageObjectAPI);

        return pAPIObject->GetMetadata().height;
    }

    return -1;
}
/////////////////////////////////////////////////////////////////////////////

