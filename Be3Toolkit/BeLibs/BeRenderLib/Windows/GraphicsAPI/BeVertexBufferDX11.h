/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeVertexBufferDX11.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEVERTEXBUFFERDX11_H
#define BE_BEVERTEXBUFFERDX11_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>
#include <Render/BeVertexBuffer.h>

/////////////////////////////////////////////////
/// \class BeVertexBufferDX11
/// \brief High-level API vertex buffer object
/////////////////////////////////////////////////
class RENDER_API BeVertexBufferDX11 : public BeVertexBuffer
{

public:

    // Methods
    // {

        /// \brief  Constructor
        BeVertexBufferDX11 ();

        /// \brief  Destructor
        ~BeVertexBufferDX11 ();

    // }



    // Getters
    // {

        /// \brief  Returns the direct pointer to the index buffer API impl
        virtual BeVoidP GetVertexBufferPtr(void);

    // }



    // Setters
    // {

        /// \brief  Sets the direct pointer to the index buffer API impl
        virtual void SetVertexBufferPtr(BeVoidP pVertexBufferPtr, BeSize_T iVertexSize);

    // }

private:

    // Forward declarations
    class BeVertexBufferDX11Impl;

    /// \brief  API Implementation
    BeVertexBufferDX11Impl* m_pVertexBufferImpl;

};

#endif // #ifndef BE_BEVERTEXBUFFERDX11_H
