/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeTextureDX11.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BETEXTUREDX11_H
#define BE_BETEXTUREDX11_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>

#include <Render/BeTexture.h>

/////////////////////////////////////////////////
/// \class BeTextureDX11
/// \brief Dx11 texture object
/////////////////////////////////////////////////
class RENDER_API BeTextureDX11 : public BeTexture
{
      
  public:
    
    /// \brief Constructor
    BeTextureDX11();
    /// \brief Destructor
    ~BeTextureDX11();

    // Getters
    // {

      /// \brief  Returns the direct pointer to the texture API impl
      virtual BeVoidP GetTexturePtr(void);

    // }



    // Methods
    // {

      /// \brief  Sets the direct pointer to the texture API impl
      virtual void SetTexturePtr(BeVoidP pTexturePtr);

    // }



    // Forward declarations
    class BeTextureDX11Impl;

    /// \brief  Render Device API Implementation
    BeTextureDX11Impl* m_pTextureImpl;

};

#endif // #ifndef BE_BETEXTUREDX11_H
