/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeVertexArrayObjectDX11.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEVERTEXARRAYOBJECTDX11_H
#define BE_BEVERTEXARRAYOBJECTDX11_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>
#include <Render/BeVertexArrayObject.h>

/////////////////////////////////////////////////
/// \class BeVertexArrayObjectDX11
/// \brief High-level VAO object
/////////////////////////////////////////////////
class RENDER_API BeVertexArrayObjectDX11 : public BeVertexArrayObject
{

public:

    /// \brief Constructor
    BeVertexArrayObjectDX11();
    /// \brief Destructor
    ~BeVertexArrayObjectDX11();

    // Getters
    // {

        /// \brief  Returns the direct pointer to the vertex array object API impl
        virtual BeVoidP GetVertexArrayObjectPtr(void);

    // }



    // Setters
    // {

        /// \brief  Sets the direct pointer to the vertex array object API impl
        virtual void SetVertexArrayObjectPtr(BeVoidP pVAOPtr);

    // }



    // Methods
    // {

        /// \brief  Prepares the attributes for this VAO object
        virtual void PrepareVertexArrayObjectAttributes(BeMaterialVertexDefinition* pVertexDefinition);

        /// \brief  Enable/Disable all the attributes for this VAO object
        virtual void EnableVertexArrayObjectAttributes(BeMaterialVertexDefinition* pVertexDefinition, BeBoolean bEnable);

    // }

private:

    // Forward declarations
    class BeVertexArrayObjectDX11Impl;

    /// \brief  Render Device API Implementation
    BeVertexArrayObjectDX11Impl* m_pVertexArrayObjectImpl;

};

#endif // #ifndef BE_BEVERTEXARRAYOBJECTDX11_H
