/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeMaterialDX11.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEMATERIALDX11_H
#define BE_BEMATERIALDX11_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>

#include <Managers/BeMaterialsManager.h>
#include <Render/BeMaterial.h>

// Forward declarations
class BeMaterialProgram;

/////////////////////////////////////////////////
/// \class BeMaterialDX11
/// \brief Dx11 material object
/////////////////////////////////////////////////
class RENDER_API BeMaterialDX11 : public BeMaterial
{
      
  public:
    
    /// \brief Constructor
    BeMaterialDX11();
    /// \brief Destructor
    ~BeMaterialDX11();

    // Methods
    // {

        /// \brief  Prepares the material with its constant and so
        virtual void PrepareMaterial(BeRenderDevice* pRenderDevice);

        /// \brief  Make a request for updating a constant in the material
        virtual void RequestUpdateConstant(const BeString8& strConstantName, BeVoidP pValue);

        /// \brief  Updates the material and constants all together
        virtual void UpdateMaterial(BeRenderDevice* pRenderDevice);

        /// \brief  Sets the material constant buffers into the render context
        virtual void SetConstantBuffers(BeRenderDevice* pRenderDevice);

        /// \brief  Sets the material texture samplers into the render context
        virtual void SetTextureSamplers(BeRenderDevice* pRenderDevice);

    // }



    // Getters
    // {

        /// \brief  Returns the direct pointer to the material API impl
        virtual BeMaterialProgram* GetMaterialProgram(BeRenderTypes::EBeMaterialPipeline eMaterialStep);

    // }



    // Methods
    // {

        /// \brief  Sets the current material for rendering
        virtual void SetMaterialForRendering(BeVoidP pRenderContext);

        /// \brief  Sets the direct pointer to the material API impl
        virtual void SetMaterialProgram(BeMaterialProgram* pMaterialProgram, BeRenderTypes::EBeMaterialPipeline eMaterialStep);

    // }

private:

    // Forward declarations
    class BeMaterialDX11Impl;

    /// \brief  Render Device API Implementation
    BeMaterialDX11Impl* m_pMaterialImpl;

};

#endif // #ifndef BE_BEMATERIALDX11_H
