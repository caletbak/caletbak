/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeRenderStatesDX11.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <GraphicsAPI/BeRenderStatesDX11.h>
#include <Render/BeRenderDevice.h>

// DirectX11 libs
#pragma comment (lib, "d3d11.lib") 

#include <d3d11.h>

/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
class RENDER_API BeRenderStatesDX11::BeRenderStatesDX11Impl
{

public:

    /// \brief  Constructor
    BeRenderStatesDX11Impl(BeRenderDevice* pRenderDevice);
    /// \brief  Destructor
    ~BeRenderStatesDX11Impl();

    /// \brief  Render State Rasterizer API
    ID3D11RasterizerState* m_pRasterizerState;
    /// \brief  Render State Rasterizer DESC API
    D3D11_RASTERIZER_DESC m_pRasterizerStatesDesc;

    /// \brief  Render Blend State API
    ID3D11BlendState* m_pBlendState;
    /// \brief  Render Blend state DESC API
    D3D11_BLEND_DESC m_pBlendStateDesc;

private:

    /// \brief  The Render device
    BeRenderDevice* m_pRenderDevice;

};
/////////////////////////////////////////////////////////////////////////////
BeRenderStatesDX11::BeRenderStatesDX11Impl::BeRenderStatesDX11Impl(BeRenderDevice* pRenderDevice)
:
    m_pRenderDevice(pRenderDevice),
    m_pRasterizerState(NULL),
    m_pBlendState(NULL)
{
    // Creating default rasterizer state
    D3D11_RASTERIZER_DESC rasterizerStateDesc;
    rasterizerStateDesc.FillMode = D3D11_FILL_SOLID;
    rasterizerStateDesc.CullMode = D3D11_CULL_FRONT;
    rasterizerStateDesc.FrontCounterClockwise = true;
    rasterizerStateDesc.DepthBias = false;
    rasterizerStateDesc.DepthBiasClamp = 0;
    rasterizerStateDesc.SlopeScaledDepthBias = 0;
    rasterizerStateDesc.DepthClipEnable = false;
    rasterizerStateDesc.ScissorEnable = false;
    rasterizerStateDesc.MultisampleEnable = false;
    rasterizerStateDesc.AntialiasedLineEnable = false;

    BeVoidP pRenderRasterizerState = (BeVoidP) m_pRenderDevice->CreateRenderState(BeRenderStates::RENDER_STATES_RASTERIZER, (BeVoidP) &rasterizerStateDesc);
    m_pRasterizerState = reinterpret_cast<ID3D11RasterizerState*>(pRenderRasterizerState);
    m_pRasterizerState->GetDesc(&m_pRasterizerStatesDesc);
    m_pRenderDevice->UpdateRenderState(BeRenderStates::RENDER_STATES_RASTERIZER, (BeVoidP) m_pRasterizerState);

    D3D11_BLEND_DESC blendStateDesc;
    blendStateDesc.AlphaToCoverageEnable = false;
    blendStateDesc.IndependentBlendEnable = false;
    for (BeInt i = 0; i < 8; ++i)
    {
        blendStateDesc.RenderTarget[i].BlendEnable = true;
        blendStateDesc.RenderTarget[i].SrcBlend = D3D11_BLEND_SRC_ALPHA;
        blendStateDesc.RenderTarget[i].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
        blendStateDesc.RenderTarget[i].BlendOp = D3D11_BLEND_OP_ADD;
        blendStateDesc.RenderTarget[i].SrcBlendAlpha = D3D11_BLEND_ONE;
        blendStateDesc.RenderTarget[i].DestBlendAlpha = D3D11_BLEND_ZERO;
        blendStateDesc.RenderTarget[i].BlendOpAlpha = D3D11_BLEND_OP_ADD;
        blendStateDesc.RenderTarget[i].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
    }

    BeVoidP pRenderBlendState = (BeVoidP)m_pRenderDevice->CreateRenderState(BeRenderStates::RENDER_STATES_BLEND, (BeVoidP)&blendStateDesc);
    m_pBlendState = reinterpret_cast<ID3D11BlendState*>(pRenderBlendState);
    m_pBlendState->GetDesc(&m_pBlendStateDesc);
    m_pRenderDevice->UpdateRenderState(BeRenderStates::RENDER_STATES_BLEND, (BeVoidP)m_pBlendState);
}
/////////////////////////////////////////////////////////////////////////////
BeRenderStatesDX11::BeRenderStatesDX11Impl::~BeRenderStatesDX11Impl()
{
    if (m_pRasterizerState != NULL)
    {
        m_pRasterizerState->Release();
        m_pRasterizerState = NULL;
    }

    if (m_pBlendState != NULL)
    {
        m_pBlendState->Release();
        m_pBlendState = NULL;
    }
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeRenderStatesDX11::BeRenderStatesDX11(BeRenderDevice* pRenderDevice)
:
    m_pRenderStatesImpl(NULL)
{
    m_pRenderDevice = pRenderDevice;

    m_pRenderStatesImpl = BeNew BeRenderStatesDX11Impl(pRenderDevice);
}
/////////////////////////////////////////////////////////////////////////////
BeRenderStatesDX11::~BeRenderStatesDX11()
{
    if (m_pRenderStatesImpl != NULL)
    {
        BeDelete m_pRenderStatesImpl;
        m_pRenderStatesImpl = NULL;
    }
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeVoidP BeRenderStatesDX11::GetRenderStatesPtr(ERenderStatesType eRSType)
{
    switch (eRSType)
    {
        case BeRenderStates::RENDER_STATES_RASTERIZER:
        {
            return m_pRenderStatesImpl->m_pRasterizerState;
        }
        case BeRenderStates::RENDER_STATES_BLEND:
        {
            return m_pRenderStatesImpl->m_pBlendState;
        }
    }

    return NULL;
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderStatesDX11::SetRenderStatesPtr(BeVoidP pRSPtr, ERenderStatesType eRSType)
{
    switch (eRSType)
    {
        case BeRenderStates::RENDER_STATES_RASTERIZER:
        {
            ID3D11RasterizerState* pRasterizerStateAPI = reinterpret_cast<ID3D11RasterizerState*>(pRSPtr);

            m_pRenderStatesImpl->m_pRasterizerState = pRasterizerStateAPI;

            break;
        }
        case BeRenderStates::RENDER_STATES_BLEND:
        {
            ID3D11BlendState* pBlendStateAPI = reinterpret_cast<ID3D11BlendState*>(pRSPtr);

            m_pRenderStatesImpl->m_pBlendState = pBlendStateAPI;

            break;
        }
    }
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeRenderStatesDX11::UpdateRenderStates()
{
    m_kPendingRenderStatesMutex.Lock();

    std::map<ERenderStateID, BeVoidP>::iterator it = m_kPendingRenderStatesUpdates.begin();
    while (it != m_kPendingRenderStatesUpdates.end())
    {
        ERenderStateID eRenderStateID = it->first;

        

        it++;
    }
    m_kPendingRenderStatesUpdates.clear();

    m_kPendingRenderStatesMutex.UnLock();

    //pRenderDevice->UpdateConstantBuffer(m_pMaterialImpl->m_pConstantBufferAPI, m_pMaterialImpl->m_pConstantBuffer, m_pMaterialImpl->m_iConstantBufferSize);
}
/////////////////////////////////////////////////////////////////////////////

