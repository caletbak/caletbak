/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeRenderDeviceDX11.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <GraphicsAPI/BeRenderDeviceDX11.h>
#include <GraphicsAPI/BeTextureDX11.h>
#include <GraphicsAPI/BeMaterialDX11.h>
#include <GraphicsAPI/BeVertexBufferDX11.h>
#include <GraphicsAPI/BeIndexBufferDX11.h>
#include <GraphicsAPI/BeConstantBufferDX11.h>
#include <GraphicsAPI/BeMaterialVertexDefinitionDX11.h>
#include <GraphicsAPI/BeRenderStatesDX11.h>
#include <GraphicsAPI/BeTextureSamplerDX11.h>
#include <GraphicsAPI/BeMaterialProgramDX11.h>
#include <GraphicsAPI/BeImageLoaderDX11.h>
#include <GraphicsAPI/BeVertexArrayObjectDX11.h>
#include <Managers/BeTextureManager.h>

#include <DirectXTex/DirectXTex.h>

using namespace DirectX;

// DirectX11 libs
#pragma comment (lib, "d3d11.lib") 
#pragma comment (lib, "D3dcompiler.lib") 
#pragma comment (lib, "DXGI.lib")

#include <directxmath.h>
#include <d3d11.h>
#include <D3Dcompiler.h>
#include <DXGI.h>

/////////////////////////////////////////////////////////////////////////////
class RENDER_API BeRenderDeviceDX11::BeRenderDeviceDX11Impl
{

  public:

        /// \brief  Mesh buffers container
        struct BeMeshContainerDX11
        {
            /// \brief  Mesh vertex buffer
            ID3D11Buffer* m_pMeshVB;

            /// \brief  Mesh index buffer
            ID3D11Buffer* m_pMeshIB;
        };

        /// \brief  Constructor
        BeRenderDeviceDX11Impl();
        /// \brief  Destructor
        ~BeRenderDeviceDX11Impl();

        /// \brief  Release
        void Release(void);



        /// \brief  The render device settings
        BeRenderTypes::BeRenderDeviceSettings m_kRenderDeviceSettings;
        DXGI_SWAP_CHAIN_DESC m_kSwapChainDesc;

        /// \brief  API Render Device debug
        ID3D11Debug* m_pRenderDeviceDebug;

        /// \brief  API Render Device
        ID3D11Device* m_pRenderDevice;

        /// \brief  API Render Device Context
        ID3D11DeviceContext* m_pRenderDeviceContext;
    
        /// \brief  DXGI Factory
        IDXGIFactory* m_DXGIFactory;

        /// \brief  DX11 Swap Chain
        IDXGISwapChain* m_pSwapChain;

        /// \brief  DX11 Render Target View
        ID3D11RenderTargetView* m_pRenderTargetView;
    
};
/////////////////////////////////////////////////////////////////////////////
BeRenderDeviceDX11::BeRenderDeviceDX11Impl::BeRenderDeviceDX11Impl()
:
    m_pRenderDeviceDebug(NULL),
    m_pRenderDevice(NULL),
    m_DXGIFactory(NULL),
    m_pSwapChain(NULL),
    m_pRenderTargetView(NULL)
{
}
/////////////////////////////////////////////////////////////////////////////
BeRenderDeviceDX11::BeRenderDeviceDX11Impl::~BeRenderDeviceDX11Impl()
{
  Release();
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderDeviceDX11::BeRenderDeviceDX11Impl::Release(void)
{
  if ( m_pRenderTargetView != NULL )
  {
    ID3D11RenderTargetView* pNullRTV = NULL;
    if (m_pRenderDeviceContext != NULL)
        m_pRenderDeviceContext->OMSetRenderTargets(1, &pNullRTV, NULL);

    m_pRenderTargetView->Release();
    m_pRenderTargetView = NULL;
  }

  if ( m_pSwapChain != NULL )
  {
      m_pSwapChain->SetFullscreenState(false, NULL);

      m_pSwapChain->Release();
      m_pSwapChain = NULL;
  }
  
  if ( m_DXGIFactory != NULL )
  {
    m_DXGIFactory->Release();
    m_DXGIFactory = NULL;
  }

  if (m_pRenderDeviceContext != NULL)
  {
      m_pRenderDeviceContext->ClearState();
      m_pRenderDeviceContext->Flush();

      BeUInt uReferences = m_pRenderDeviceContext->Release();
      if (uReferences > 0)
          BE_RENDER_ERROR("D3D device context : there are unreleased objects !!.");
      m_pRenderDeviceContext = NULL;
  }

  if (m_pRenderDeviceDebug != NULL)
  {
      //m_pRenderDeviceDebug->ReportLiveDeviceObjects(D3D11_RLDO_DETAIL);

      m_pRenderDeviceDebug->Release();
      m_pRenderDeviceDebug = NULL;
  }

  if (m_pRenderDevice != NULL)
  {
      BeUInt uReferences = m_pRenderDevice->Release();
      if (uReferences > 0)
      {
          BE_RENDER_ERROR("D3D device : there are unreleased objects !!.");
      }
      m_pRenderDevice = NULL;
  }
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeRenderDeviceDX11::BeRenderDeviceDX11Impl* BeRenderDeviceDX11::m_pRenderDeviceImpl = NULL;
BeRenderDeviceDX11Enumerator* BeRenderDeviceDX11::m_pDeviceEnumerator = NULL;
/////////////////////////////////////////////////////////////////////////////
BeRenderDeviceDX11::BeRenderDeviceDX11()
{
    m_eRenderAPIBeingUsed = BeRenderTypes::RENDER_DEVICE_API_DX11;

    m_pRenderDeviceImpl = BeNew BeRenderDeviceDX11Impl();
}
/////////////////////////////////////////////////////////////////////////////
BeRenderDeviceDX11::~BeRenderDeviceDX11()
{
  BeRenderDevice::Release();

  if ( m_pRenderDeviceImpl != NULL )
  {
    BeDelete m_pRenderDeviceImpl;
    m_pRenderDeviceImpl = NULL;
  }
  if ( m_pDeviceEnumerator != NULL )
  {
    BeDelete m_pDeviceEnumerator;
    m_pDeviceEnumerator = NULL;
  }
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeBoolean BeRenderDeviceDX11::CreateTextureFromBuffer(BeRenderTypes::BeTextureCacheEntry* pCacheTextureEntry, BeVoidP pData, BeInt iDataSize)
{
  if (!m_pRenderDeviceImpl->m_pRenderDevice)
      return BE_FALSE;

  if ( pCacheTextureEntry->m_pTexture != NULL )
  {
    BeDelete pCacheTextureEntry->m_pTexture;
    pCacheTextureEntry->m_pTexture = NULL;
  }
  pCacheTextureEntry->m_pTexture = BeNew BeTextureDX11();

  BeTextureDX11* pTextureDX11 = static_cast<BeTextureDX11*>(pCacheTextureEntry->m_pTexture);

  HRESULT hr = -1;
  
  BeImageLoaderDX11 kImageLoader;
  kImageLoader.LoadFileImage(pData, iDataSize);

  if (kImageLoader.GetImageObjectAPI())
  {
      pCacheTextureEntry->m_xSize = kImageLoader.GetImageWidth();
      pCacheTextureEntry->m_ySize = kImageLoader.GetImageHeight();

      /*
      BeVector4D* pPixels = kImageLoader.GeneratePixelArray();

      BeString16 newfile = pCacheTextureEntry->m_strTextureName;
      newfile.Append(L".raw");

      BeFileTool f;
      f.OpenFile(newfile, BeFileTool::FILE_WRITING);

      for (BeInt i = 0; i < pCacheTextureEntry->m_xSize * pCacheTextureEntry->m_ySize; ++i)
      {
          f.WriteChar(((BeInt)(pPixels[i].m_fX * 255.0f)) & 0xFF);
          f.WriteChar(((BeInt)(pPixels[i].m_fY * 255.0f)) & 0xFF);
          f.WriteChar(((BeInt)(pPixels[i].m_fZ * 255.0f)) & 0xFF);
      }

      f.CloseFile();
      */

      ScratchImage* kImageLoaded = reinterpret_cast<ScratchImage*>(kImageLoader.GetImageObjectAPI());

      ID3D11ShaderResourceView* pNewDX11Texture = NULL;

      hr = CreateShaderResourceView(m_pRenderDeviceImpl->m_pRenderDevice, kImageLoaded->GetImages(), kImageLoaded->GetImageCount(), kImageLoaded->GetMetadata(), &pNewDX11Texture);

      // Return on any failure.
      if (FAILED(hr))
      {
          BeDelete pCacheTextureEntry->m_pTexture;
          pCacheTextureEntry->m_pTexture = NULL;

          BE_RENDER_ERROR("ERROR CreateTextureFromBuffer %s !!", ConvertString16To8(pCacheTextureEntry->m_strTextureName).ToRaw());
          return BE_FALSE;
      }

      pTextureDX11->SetTexturePtr(pNewDX11Texture);
  }
  else
  {
      BeDelete pCacheTextureEntry->m_pTexture;
      pCacheTextureEntry->m_pTexture = NULL;

      BE_RENDER_ERROR("ERROR CreateTextureFromBuffer %s !!", ConvertString16To8(pCacheTextureEntry->m_strTextureName).ToRaw());
      return BE_FALSE;
  }

  return BE_TRUE;
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeRenderDeviceDX11::CreateMaterialFromConfig(BeRenderTypes::BeMaterialCacheEntry* pCacheMaterialEntry, const BeRenderTypes::BeMaterialConfig& kMaterialConfig)
{
    BE_UNUSED(pCacheMaterialEntry);
    BE_UNUSED(kMaterialConfig);

    if (!m_pRenderDeviceImpl->m_pRenderDevice)
        return BE_FALSE;

    if (pCacheMaterialEntry->m_pMaterial != NULL)
    {
        BeDelete pCacheMaterialEntry->m_pMaterial;
        pCacheMaterialEntry->m_pMaterial = NULL;
    }
    pCacheMaterialEntry->m_pMaterial = BeNew BeMaterialDX11();

    BeMaterialDX11* pMaterialDX11 = static_cast<BeMaterialDX11*>(pCacheMaterialEntry->m_pMaterial);

    pMaterialDX11->SetMaterialProgram(kMaterialConfig.m_pVSProgram, BeRenderTypes::MATERIAL_PIPELINE_VERTEX);
    pMaterialDX11->SetMaterialProgram(kMaterialConfig.m_pPSProgram, BeRenderTypes::MATERIAL_PIPELINE_PIXEL);

    return BE_TRUE;
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderDeviceDX11::CreateRenderStates(void)
{
    m_pRenderStates = new BeRenderStatesDX11(this);
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
LRESULT CALLBACK BeRenderDeviceDX11::WndProcDX11(BeWindowHandle hWnd, BeUInt uMessage, WPARAM wParam, LPARAM lParam)
{

  switch (uMessage)
  {
    case WM_CLOSE:
    {
      if ( m_pRenderDeviceImpl->m_pSwapChain != NULL )
      {
        BeInt bIsFullScreen = 0;
        m_pRenderDeviceImpl->m_pSwapChain->GetFullscreenState(&bIsFullScreen, NULL);

        if ( bIsFullScreen )
        {
          m_pRenderDeviceImpl->m_pSwapChain->SetFullscreenState( BE_FALSE, NULL );
          
          ChangeDisplaySettings(NULL,0);
          ShowCursor(BE_TRUE);
        }
      }
      break;
    }

    case WM_SIZE: // If our window is resizing
    {

      if ( m_pRenderDeviceImpl->m_pSwapChain != NULL )
      {
        BeInt bIsFullScreen = 0;
        m_pRenderDeviceImpl->m_pSwapChain->GetFullscreenState(&bIsFullScreen, NULL);
                
        ID3D11RenderTargetView* pNullRTV = NULL;
        m_pRenderDeviceImpl->m_pRenderDeviceContext->OMSetRenderTargets(1, &pNullRTV, NULL);
        
        if ( m_pRenderDeviceImpl->m_pRenderTargetView != NULL )
        {
          m_pRenderDeviceImpl->m_pRenderTargetView->Release();
          m_pRenderDeviceImpl->m_pRenderTargetView = NULL;
        }
        
        RECT kRect;
        GetClientRect(hWnd, &kRect);
    
        BeInt iSizeX = kRect.right - kRect.left;
        BeInt iSizeY = kRect.bottom - kRect.top;

        HRESULT hr = m_pRenderDeviceImpl->m_pSwapChain->ResizeBuffers(
          m_pRenderDeviceImpl->m_kSwapChainDesc.BufferCount,
          iSizeX,
          iSizeY,
          m_pRenderDeviceImpl->m_kSwapChainDesc.BufferDesc.Format,
          m_pRenderDeviceImpl->m_kSwapChainDesc.Flags
        );
        
        // Create a render target view 
        ID3D11Texture2D* pBackBuffer; 
        hr = m_pRenderDeviceImpl->m_pSwapChain->GetBuffer( 0, __uuidof( ID3D11Texture2D ), ( LPVOID* )&pBackBuffer ); 
        if( FAILED( hr ) )
        {
          BE_RENDER_ERROR( "Error getting swap chain buffer." );
          return BE_FALSE; 
        }
       
        hr = m_pRenderDeviceImpl->m_pRenderDevice->CreateRenderTargetView( pBackBuffer, NULL, &m_pRenderDeviceImpl->m_pRenderTargetView ); 
        pBackBuffer->Release(); 
        if ( FAILED( hr ) ) 
        {
          BE_RENDER_ERROR( "Error creating render target view." );
          return BE_FALSE; 
        }
       
        m_pRenderDeviceImpl->m_pRenderDeviceContext->OMSetRenderTargets(1, &m_pRenderDeviceImpl->m_pRenderTargetView, NULL);

        if ( FAILED(hr) )
          BE_RENDER_DEBUG("m_pSwapChain->ResizeBuffers Failed !");

      }
        
      return 0;
    }
  }

  return BeRenderDevice::WndProc(hWnd, uMessage, wParam, lParam);
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeInt BeRenderDeviceDX11::ConvertRenderFormatToAPIFormat(const BeRenderTypes::ERenderFormat eRenderFormat)
{
  switch(eRenderFormat)
  {
    case BeRenderTypes::RFORMAT_UNKNOWN:
      return 0;

    case BeRenderTypes::RFORMAT_R8G8B8:
      return DXGI_FORMAT_B8G8R8X8_UNORM_SRGB;
    case BeRenderTypes::RFORMAT_R8G8B8A8:
      return DXGI_FORMAT_R8G8B8A8_UNORM_SRGB;

    case BeRenderTypes::RFORMAT_R5G5B5:
      return DXGI_FORMAT_B5G5R5A1_UNORM;
    case BeRenderTypes::RFORMAT_R5G5B5A1:
      return DXGI_FORMAT_B5G5R5A1_UNORM;

    case BeRenderTypes::RFORMAT_R4G4B4:
      return 0;
    case BeRenderTypes::RFORMAT_R4G4B4A4:
      return 0;

    case BeRenderTypes::RFORMAT_R5G6B5:
      return DXGI_FORMAT_B5G6R5_UNORM;

    case BeRenderTypes::RFORMAT_R10G10B10:
      return DXGI_FORMAT_R10G10B10A2_TYPELESS;
    case BeRenderTypes::RFORMAT_R10G10B10A2:
      return DXGI_FORMAT_R10G10B10A2_UNORM;
            
    case BeRenderTypes::RFORMAT_R16G16B16:
      return DXGI_FORMAT_R16G16B16A16_FLOAT;
    case BeRenderTypes::RFORMAT_R16G16B16A16:
      return DXGI_FORMAT_R16G16B16A16_UNORM;
  }

  return 0;
}
/////////////////////////////////////////////////////////////////////////////
BeInt BeRenderDeviceDX11::ConvertDepthStencilFormatToAPIFormat(const BeRenderTypes::EDepthStencilFormat eDepthStencilFormat)
{
  switch(eDepthStencilFormat)
  {
    case BeRenderTypes::DSFORMAT_UNKNOWN:
      return 0;

    case BeRenderTypes::DSFORMAT_DEPTH_32:
      return DXGI_FORMAT_D32_FLOAT;
    case BeRenderTypes::DSFORMAT_DEPTH_32_S8:
      return 0;
    case BeRenderTypes::DSFORMAT_DEPTH_24:
      return DXGI_FORMAT_D24_UNORM_S8_UINT;
    case BeRenderTypes::DSFORMAT_DEPTH_24_S8:
      return DXGI_FORMAT_D24_UNORM_S8_UINT;
    case BeRenderTypes::DSFORMAT_DEPTH_16:
      return 0;
    case BeRenderTypes::DSFORMAT_DEPTH_16_S8:
      return 0;
  }

  return 0;
}
/////////////////////////////////////////////////////////////////////////////
BeInt BeRenderDeviceDX11::CheckNumAdapters(void)
{
    IDXGIFactory* pFactory = NULL;
    HRESULT hr = CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)(&pFactory) );
    if( FAILED( hr ) )
        return -1;

    BeInt iNumAdapters = 0;
    for( BeInt index = 0; ; ++index )
    {
        IDXGIAdapter* pAdapter = NULL;
        hr = pFactory->EnumAdapters( index, &pAdapter );
        if( FAILED( hr ) )
            break;

        iNumAdapters++;
    }

    if ( pFactory != NULL )
        pFactory->Release();
  
    return iNumAdapters;
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeRenderDeviceDX11::CheckD3D11(BeUInt uAdapterOrdinal)
{
    BeInt hr = E_FAIL;
    HMODULE hLibHandle = NULL;

    // Manually load the d3d9.dll library.
    hLibHandle = LoadLibrary(L"d3d11.dll");

    if(hLibHandle != NULL)
    {
        // Define a function pointer to the Direct3DCreate9 function.
        typedef BeInt (WINAPI *LPDIRECT3DCREATE11)( UINT, void **);

        // Obtain the address of the Direct3DCreate11_0 function. 
        LPDIRECT3DCREATE11 Direct3DCreate11_0Ptr = NULL;
            
        Direct3DCreate11_0Ptr = (LPDIRECT3DCREATE11)GetProcAddress( hLibHandle, "D3D11CreateDevice" );
        if ( Direct3DCreate11_0Ptr != NULL)
        {

          IDXGIFactory* pFactory = NULL;
          hr = CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)(&pFactory) );
          if( FAILED( hr ) )
            return BE_FALSE;

          IDXGIAdapter* pAdapter = NULL;
          hr = pFactory->EnumAdapters( uAdapterOrdinal, &pAdapter );
          if( FAILED( hr ) )
          {
            pFactory->Release();
            return BE_FALSE;
          }

          //Create the D3D device
          ID3D11Device* pd3d11Device = NULL;
          ID3D11DeviceContext* pd3d11DeviceContext = NULL;
          hr = D3D11CreateDevice(pAdapter, D3D_DRIVER_TYPE_HARDWARE, NULL, 0, NULL, 0, D3D11_SDK_VERSION, &pd3d11Device, NULL, &pd3d11DeviceContext);
          
          if (pAdapter)
              pAdapter->Release();
          if (pFactory != NULL)
              pFactory->Release();

          if (SUCCEEDED(hr))
          {
              pd3d11DeviceContext->ClearState();
              pd3d11DeviceContext->Flush();
              pd3d11DeviceContext->Release();

              pd3d11Device->Release();
          }
        }
        else
        {
            // Direct3DCreate9Ex is not supported on this
            // operating system.
            hr = ERROR_NOT_SUPPORTED;
        }

        // Free the library.
        FreeLibrary( hLibHandle );

    }
    
    return (hr == S_OK);
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeRenderDeviceDX11::EnumerateDisplayModes(BeUInt uAdapterOrdinal, BeRenderTypes::BeDeviceAdapter& kDeviceAdapter)
{
  if ( m_pDeviceEnumerator != NULL )
  {
    BeDelete m_pDeviceEnumerator;
    m_pDeviceEnumerator = NULL;
  }

  m_pDeviceEnumerator = BeNew BeRenderDeviceDX11Enumerator();
  if ( m_pDeviceEnumerator->ExecuteEnumerationProcess() )
  {
    m_pDeviceEnumerator->FillDisplayModesArray(uAdapterOrdinal, kDeviceAdapter);
  }
  else
  {
    BeDelete m_pDeviceEnumerator;
    m_pDeviceEnumerator = NULL;
    return BE_FALSE;
  }

  if ( m_pDeviceEnumerator != NULL )
  {
    BeDelete m_pDeviceEnumerator;
    m_pDeviceEnumerator = NULL;
  }

  return BE_TRUE;
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeRenderDeviceDX11::CreateDXDevice(void)
{
  HRESULT hr;
    
  //Set up DX swap chain
  //--------------------------------------------------------------
  ZeroMemory(&m_pRenderDeviceImpl->m_kSwapChainDesc, sizeof(m_pRenderDeviceImpl->m_kSwapChainDesc));

  BeRenderTypes::BeRenderDeviceDisplayMode kRenderDeviceDisplayMode;
  kRenderDeviceDisplayMode.m_hWindowHandle = m_hWindow;
  if ( m_pDeviceEnumerator == NULL )
  {
    m_pDeviceEnumerator = BeNew BeRenderDeviceDX11Enumerator();
    if ( !m_pDeviceEnumerator->ExecuteEnumerationProcess() )
      return BE_FALSE;
  }
  m_pDeviceEnumerator->FindBestDisplayMode( m_pRenderDeviceImpl->m_kRenderDeviceSettings.m_uAdapterOrdinal, m_pRenderDeviceImpl->m_kRenderDeviceSettings, &kRenderDeviceDisplayMode );
  m_pDeviceEnumerator->FillSwapChainDesc( kRenderDeviceDisplayMode, &m_pRenderDeviceImpl->m_kSwapChainDesc );
  


  IDXGIFactory* pFactory = NULL;
  hr = CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)(&pFactory) );
  if( FAILED( hr ) )
    return BE_FALSE;

  IDXGIAdapter* pAdapter = NULL;
  hr = pFactory->EnumAdapters( m_pRenderDeviceImpl->m_kRenderDeviceSettings.m_uAdapterOrdinal, &pAdapter );

  pFactory->Release();
  pFactory = NULL;

  BeInt iCreationFlags = 0;

#ifdef _DEBUG
  iCreationFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

  //Create the D3D device
  ID3D11Device* pd3d11Device = NULL;
  ID3D11DeviceContext* pd3d11DeviceContext = NULL;
  D3D_FEATURE_LEVEL pd3d11FeatureLevel;
  hr = D3D11CreateDevice(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, iCreationFlags, NULL, 0, D3D11_SDK_VERSION, &pd3d11Device, &pd3d11FeatureLevel, &pd3d11DeviceContext);

  if (SUCCEEDED(hr))
  {
#ifdef _DEBUG
      pd3d11Device->QueryInterface(__uuidof(ID3D11Debug), reinterpret_cast<void**>(&m_pRenderDeviceImpl->m_pRenderDeviceDebug));
#endif
      m_pRenderDeviceImpl->m_pRenderDevice = pd3d11Device;
      m_pRenderDeviceImpl->m_pRenderDeviceContext = pd3d11DeviceContext;
  }

  if ( pAdapter )
    pAdapter->Release();

  if ( m_pRenderDeviceImpl->m_pRenderDevice != NULL )
  {
    
    IDXGIDevice* pDXGIDevice;
    hr = m_pRenderDeviceImpl->m_pRenderDevice->QueryInterface(__uuidof(IDXGIDevice), (void **)&pDXGIDevice);
          
    IDXGIAdapter* pDXGIAdapter;
    hr = pDXGIDevice->GetParent(__uuidof(IDXGIAdapter), (void **)&pDXGIAdapter);

    IDXGIFactory* pIDXGIFactory;
    pDXGIAdapter->GetParent(__uuidof(IDXGIFactory), (void **)&pIDXGIFactory);

    m_pRenderDeviceImpl->m_DXGIFactory = pIDXGIFactory;
    
    pDXGIDevice->Release();
    pDXGIAdapter->Release();
    
    if( FAILED( hr ) )
    {
        BE_RENDER_ERROR( "Error retrieving DXGI factory." );
        return BE_FALSE;
    }

    // Create the swapchain
    hr = m_pRenderDeviceImpl->m_DXGIFactory->CreateSwapChain( m_pRenderDeviceImpl->m_pRenderDevice, &m_pRenderDeviceImpl->m_kSwapChainDesc, &m_pRenderDeviceImpl->m_pSwapChain );
    if( FAILED( hr ) )
    {
        BE_RENDER_ERROR( "Error creating swap chain." );
        return BE_FALSE;
    }

    // Create a render target view 
    ID3D11Texture2D* pBackBuffer; 
    hr = m_pRenderDeviceImpl->m_pSwapChain->GetBuffer( 0, __uuidof( ID3D11Texture2D ), ( LPVOID* )&pBackBuffer ); 
    if( FAILED( hr ) )
    {
      BE_RENDER_ERROR( "Error getting swap chain buffer." );
      return BE_FALSE; 
    }
   
    hr = m_pRenderDeviceImpl->m_pRenderDevice->CreateRenderTargetView( pBackBuffer, NULL, &m_pRenderDeviceImpl->m_pRenderTargetView ); 
    pBackBuffer->Release(); 
    if ( FAILED( hr ) ) 
    {
      BE_RENDER_ERROR( "Error creating render target view." );
      return BE_FALSE; 
    }
    m_pRenderDeviceImpl->m_pRenderDeviceContext->OMSetRenderTargets(1, &m_pRenderDeviceImpl->m_pRenderTargetView, NULL);

    ShowCursor(m_pRenderDeviceImpl->m_kRenderDeviceSettings.m_bWindowed);
    m_pRenderDeviceImpl->m_DXGIFactory->MakeWindowAssociation(m_pRenderDeviceImpl->m_kSwapChainDesc.OutputWindow, DXGI_MWA_NO_WINDOW_CHANGES | DXGI_MWA_NO_ALT_ENTER);

    return BE_TRUE;
  }

  return BE_FALSE;
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeRenderDeviceDX11::CreateRenderDevice(const BeRenderTypes::BeRenderDeviceSettings& kRenderDeviceSettings)
{
  m_pRenderDeviceImpl->m_kRenderDeviceSettings = kRenderDeviceSettings;

  // Create our App window
  if ( CreateAppWindow(kRenderDeviceSettings.m_strWindowName, kRenderDeviceSettings.m_uBufferWidth, kRenderDeviceSettings.m_uBufferHeight, 32, BE_FALSE, WndProcDX11) )
  {

    // Create our Direct11 rendering context
    CreateDXDevice();

    ShowWindow(m_hWindow,SW_SHOW);            // Show The Window
    SetForegroundWindow(m_hWindow);            // Slightly Higher Priority
    SetFocus(m_hWindow);                // Sets Keyboard Focus To The Window

    // Set the primitive�s topology.
    m_pRenderDeviceImpl->m_pRenderDeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

    return BE_TRUE;

  }

  return BE_FALSE;
}
/////////////////////////////////////////////////////////////////////////////
const BeRenderTypes::BeRenderDeviceSettings& BeRenderDeviceDX11::GetRenderDeviceSettings(void)
{
    return m_pRenderDeviceImpl->m_kRenderDeviceSettings;
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderDeviceDX11::BeginScene (const BeMatrix& kViewProjMatrix)
{
    BE_UNUSED(kViewProjMatrix);

    m_mSceneViewProjectionMatrix = kViewProjMatrix;
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderDeviceDX11::EndScene (void)
{
  //
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderDeviceDX11::Clear (const BeRenderTypes::EClearMask clearMask, const BeVector4D& clearColour, const BeFloat clearZValue, const BeInt clearStencilValue)
{
  BeInt dx11Flags = 0;
  if (clearMask & BeRenderTypes::CLEAR_STENCIL)
  {
    dx11Flags |= D3D11_CLEAR_STENCIL;
  }
  if (clearMask & BeRenderTypes::CLEAR_ZBUFFER)
  {
    dx11Flags |= D3D11_CLEAR_DEPTH;
  }

  if (clearMask & BeRenderTypes::CLEAR_TARGET)
  {
      XMVECTORF32 kClearColor = { clearColour.m_fX, clearColour.m_fY, clearColour.m_fZ, clearColour.m_fW };

      m_pRenderDeviceImpl->m_pRenderDeviceContext->ClearRenderTargetView(m_pRenderDeviceImpl->m_pRenderTargetView, kClearColor);
  }

  BE_UNUSED(dx11Flags);
  BE_UNUSED(clearZValue);
  BE_UNUSED(clearStencilValue);
  /*
  if (dx11Flags != 0)
  {
    m_pRenderDeviceImpl->m_pRenderDevice->ClearDepthStencilView(m_pRenderDeviceImpl->m_pRenderTargetView, dx11Flags, clearZValue, clearStencilValue);
  }
  */
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeRenderDeviceDX11::Present(void)
{  
  
  BeInt hr = S_FALSE;
  
  //clear scene
  //m_pRenderDeviceImpl->m_pRenderDevice->ClearRenderTargetView( m_pRenderDeviceImpl->m_pRenderTargetView, D3DXCOLOR(0,0,1.0f,1.0f) );

  //flip buffers
  hr = m_pRenderDeviceImpl->m_pSwapChain->Present( 0, 0 );
  if ( hr != S_OK )
    BE_RENDER_WARNING("Render Device Present Not OK ! : %d", hr);
  
  BeBoolean bFailed = BE_FAILED(hr);
  if ( bFailed )
    BE_RENDER_WARNING("Render Device Present Failed !");
    
  return bFailed;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeRenderDeviceDX11::SetViewport(const BeViewport& kViewport)
{
    m_kCurrentViewport = kViewport;

    D3D11_VIEWPORT viewport[1];
    viewport[0].TopLeftX = kViewport.m_fTopLeftX;
    viewport[0].TopLeftY = kViewport.m_fTopLeftY;
    viewport[0].Width = kViewport.m_fWidth;
    viewport[0].Height = kViewport.m_fHeight;
    viewport[0].MinDepth = kViewport.m_fMinDepth;
    viewport[0].MaxDepth = kViewport.m_fMaxDepth;

    m_pRenderDeviceImpl->m_pRenderDeviceContext->RSSetViewports(1, viewport);
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeVertexArrayObject* BeRenderDeviceDX11::CreateVertexArrayObject(void)
{
    BeVertexArrayObjectDX11* pNewVertexArrayObject = BeNew BeVertexArrayObjectDX11();

    return pNewVertexArrayObject;
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderDeviceDX11::SetVertexArrayObject(BeVertexArrayObject* pVertexArrayObject)
{
    if (m_pCurrentVertexArrayObjectSet != pVertexArrayObject)
    {
        m_pCurrentVertexArrayObjectSet = pVertexArrayObject;
    }
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderDeviceDX11::FreeVertexArrayObject(BeVertexArrayObject* pVertexArrayObject)
{
    BeVertexArrayObjectDX11* pVertexArrayObjectAPI = reinterpret_cast<BeVertexArrayObjectDX11*>(pVertexArrayObject);

    BeDelete pVertexArrayObjectAPI;
}
/////////////////////////////////////////////////////////////////////////////
BeVertexBuffer* BeRenderDeviceDX11::CreateVertexBuffer(BeVoidP pMeshVertices, BeSize_T iVertexStructSize, BeUInt uNumVertices, const BeString8& strVSProgram, BeVertexArrayObject* pVAO)
{
    BeInt hr = S_FALSE;

    // Fill in a buffer description.
    D3D11_BUFFER_DESC bufferDesc;
    bufferDesc.Usage = D3D11_USAGE_DEFAULT;
    bufferDesc.ByteWidth = iVertexStructSize * uNumVertices;
    bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
    bufferDesc.CPUAccessFlags = 0;
    bufferDesc.MiscFlags = 0;

    // Fill in the subresource data.
    D3D11_SUBRESOURCE_DATA InitData;
    InitData.pSysMem = pMeshVertices;
    InitData.SysMemPitch = 0;
    InitData.SysMemSlicePitch = 0;

    // Create the vertex buffer.
    ID3D11Buffer* pAPIVertexBuffer = NULL;
    hr = m_pRenderDeviceImpl->m_pRenderDevice->CreateBuffer(&bufferDesc, &InitData, &pAPIVertexBuffer);
    if (hr != S_OK)
    {
        BE_RENDER_WARNING("Render Device could not create the vertex buffer! : %d", hr);
        return NULL;
    }

    BeVertexBufferDX11* pNewVertexBuffer = BeNew BeVertexBufferDX11();
    pNewVertexBuffer->SetVertexBufferPtr (pAPIVertexBuffer, iVertexStructSize);

    return pNewVertexBuffer;
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderDeviceDX11::SetVertexBuffer(BeVertexBuffer* pVertexBuffer)
{
    if (pVertexBuffer)
    {
        if (m_pCurrentVertexBufferSet != pVertexBuffer)
        {
            ID3D11Buffer* pVertexBufferAPI = reinterpret_cast<ID3D11Buffer*>(pVertexBuffer->GetVertexBufferPtr());

            BeUInt uStrides = pVertexBuffer->GetVertexElementSize();
            BeUInt uOffsets = 0;

            m_pRenderDeviceImpl->m_pRenderDeviceContext->IASetVertexBuffers(0, 1, &pVertexBufferAPI, &uStrides, &uOffsets);

            m_pCurrentVertexBufferSet = pVertexBuffer;
        }
    }
    else
    {
        m_pCurrentVertexBufferSet = NULL;

        m_pRenderDeviceImpl->m_pRenderDeviceContext->IASetVertexBuffers(0, 0, NULL, NULL, NULL);
    }
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderDeviceDX11::FreeVertexBuffer(BeVertexBuffer* pVertexBuffer)
{
    BeVertexBufferDX11* pVertexBufferAPI = reinterpret_cast<BeVertexBufferDX11*>(pVertexBuffer);

    BeDelete pVertexBufferAPI;
}
/////////////////////////////////////////////////////////////////////////////
BeIndexBuffer* BeRenderDeviceDX11::CreateIndexBuffer(BeVoidP pMeshIndices, BeUInt uNumTriangles)
{
    BeInt hr = S_FALSE;

    // Fill in a buffer description.
    D3D11_BUFFER_DESC bufferDesc;
    bufferDesc.Usage = D3D11_USAGE_DEFAULT;
    bufferDesc.ByteWidth = sizeof(BeUShort) * (uNumTriangles * 3);
    bufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
    bufferDesc.CPUAccessFlags = 0;
    bufferDesc.MiscFlags = 0;

    // Define the resource data.
    D3D11_SUBRESOURCE_DATA InitData;
    InitData.pSysMem = pMeshIndices;
    InitData.SysMemPitch = 0;
    InitData.SysMemSlicePitch = 0;

    // Create the index buffer.
    ID3D11Buffer* pAPIIndexBuffer = NULL;
    hr = m_pRenderDeviceImpl->m_pRenderDevice->CreateBuffer(&bufferDesc, &InitData, &pAPIIndexBuffer);
    if (hr != S_OK)
    {
        BE_RENDER_WARNING("Render Device could not create the index buffer! : %d", hr);
        return NULL;
    }

    BeIndexBufferDX11* pNewIndexBuffer = BeNew BeIndexBufferDX11();
    pNewIndexBuffer->SetIndexBufferPtr(pAPIIndexBuffer, uNumTriangles * 3);

    return pNewIndexBuffer;
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderDeviceDX11::SetIndexBuffer(BeIndexBuffer* pIndexBuffer)
{
    if (pIndexBuffer)
    {
        if (m_pCurrentIndexBufferSet != pIndexBuffer)
        {
            ID3D11Buffer* pIndexBufferAPI = reinterpret_cast<ID3D11Buffer*>(pIndexBuffer->GetIndexBufferPtr());

            m_pRenderDeviceImpl->m_pRenderDeviceContext->IASetIndexBuffer(pIndexBufferAPI, DXGI_FORMAT_R16_UINT, 0);

            m_pCurrentIndexBufferSet = pIndexBuffer;
        }
    }
    else
    {
        m_pCurrentIndexBufferSet = NULL;

        m_pRenderDeviceImpl->m_pRenderDeviceContext->IASetIndexBuffer(NULL, DXGI_FORMAT_R16_UINT, 0);
    }
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderDeviceDX11::FreeIndexBuffer(BeIndexBuffer* pIndexBuffer)
{
    BeIndexBufferDX11* pIndexBufferAPI = reinterpret_cast<BeIndexBufferDX11*>(pIndexBuffer);

    BeDelete pIndexBufferAPI;
}
/////////////////////////////////////////////////////////////////////////////
BeConstantBuffer* BeRenderDeviceDX11::CreateConstantBuffer(BeVoidP pConstantBuffer, BeUInt uConstantBufferSize)
{
    BeInt hr = S_FALSE;

    // Fill in a buffer description.
    D3D11_BUFFER_DESC cbDesc;
    cbDesc.ByteWidth = uConstantBufferSize;
    cbDesc.Usage = D3D11_USAGE_DYNAMIC;
    cbDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
    cbDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
    cbDesc.MiscFlags = 0;
    cbDesc.StructureByteStride = 0;

    // Fill in the subresource data.
    D3D11_SUBRESOURCE_DATA InitData;
    InitData.pSysMem = pConstantBuffer;
    InitData.SysMemPitch = 0;
    InitData.SysMemSlicePitch = 0;

    // Create the buffer.
    ID3D11Buffer* pAPIConstantBuffer = NULL;
    hr = m_pRenderDeviceImpl->m_pRenderDevice->CreateBuffer(&cbDesc, &InitData, &pAPIConstantBuffer);
    if (hr != S_OK)
    {
        BE_RENDER_DEBUG("DX11 device could not create the material constant buffer!");
        return NULL;
    }

    BeConstantBufferDX11* pNewConstantBuffer = BeNew BeConstantBufferDX11();
    pNewConstantBuffer->SetConstantBufferPtr(pAPIConstantBuffer);

    return pNewConstantBuffer;
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderDeviceDX11::UpdateConstantBuffer(BeConstantBuffer* pConstantBuffer, BeVoidP pBuffer, BeUInt uBufferSize)
{
    BeConstantBufferDX11* pConstantBufferDX11 = reinterpret_cast<BeConstantBufferDX11*>(pConstantBuffer);

    ID3D11Buffer* pConstantBufferAPI = reinterpret_cast<ID3D11Buffer*>(pConstantBufferDX11->GetConstantBufferPtr());

    BeInt hr = S_FALSE;

    // Fill in the subresource data.
    D3D11_MAPPED_SUBRESOURCE updateData;
    updateData.pData = pBuffer;
    updateData.RowPitch = 0;
    updateData.DepthPitch = 0;

    hr = m_pRenderDeviceImpl->m_pRenderDeviceContext->Map(pConstantBufferAPI, 0, D3D11_MAP_WRITE_DISCARD, 0, &updateData);
    if (hr == S_OK)
    {
        // copy data over to constant buffer
        memcpy_s(updateData.pData, updateData.RowPitch, pBuffer, uBufferSize);

        m_pRenderDeviceImpl->m_pRenderDeviceContext->Unmap(pConstantBufferAPI, 0);
    }
    else
    {
        BE_RENDER_DEBUG("DX11 device could not map the material constant buffer!");
    }
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderDeviceDX11::SetConstantBuffer(BeConstantBuffer* pConstantBuffer, BeRenderTypes::EBeMaterialPipeline eMaterialPipeline)
{
    if (pConstantBuffer)
    {
        ID3D11Buffer* pConstantBufferAPI = reinterpret_cast<ID3D11Buffer*>(pConstantBuffer->GetConstantBufferPtr());

        switch (eMaterialPipeline)
        {
            case BeRenderTypes::MATERIAL_PIPELINE_GEOMETRY:
            {
                if (m_pCurrentGSConstantBufferSet != pConstantBuffer)
                {
                    m_pRenderDeviceImpl->m_pRenderDeviceContext->GSSetConstantBuffers(0, 1, &pConstantBufferAPI);
                    m_pCurrentGSConstantBufferSet = pConstantBuffer;
                }
                break;
            }
            case BeRenderTypes::MATERIAL_PIPELINE_VERTEX:
            {
                if (m_pCurrentVSConstantBufferSet != pConstantBuffer)
                {
                    m_pRenderDeviceImpl->m_pRenderDeviceContext->VSSetConstantBuffers(0, 1, &pConstantBufferAPI);
                    m_pCurrentVSConstantBufferSet = pConstantBuffer;
                }
                break;
            }
            case BeRenderTypes::MATERIAL_PIPELINE_PIXEL:
            {
                if (m_pCurrentPSConstantBufferSet != pConstantBuffer)
                {
                    m_pRenderDeviceImpl->m_pRenderDeviceContext->PSSetConstantBuffers(0, 1, &pConstantBufferAPI);
                    m_pCurrentPSConstantBufferSet = pConstantBuffer;
                }
                break;
            }
        }
    }
    else
    {
        switch (eMaterialPipeline)
        {
            case BeRenderTypes::MATERIAL_PIPELINE_GEOMETRY:
            {
                m_pCurrentGSConstantBufferSet = NULL;
                m_pRenderDeviceImpl->m_pRenderDeviceContext->GSSetConstantBuffers(0, 0, NULL);

                break;
            }
            case BeRenderTypes::MATERIAL_PIPELINE_VERTEX:
            {
                m_pCurrentVSConstantBufferSet = NULL;
                m_pRenderDeviceImpl->m_pRenderDeviceContext->VSSetConstantBuffers(0, 0, NULL);

                break;
            }
            case BeRenderTypes::MATERIAL_PIPELINE_PIXEL:
            {
                m_pCurrentPSConstantBufferSet = NULL;
                m_pRenderDeviceImpl->m_pRenderDeviceContext->PSSetConstantBuffers(0, 0, NULL);

                break;
            }
        }
    }
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderDeviceDX11::FreeConstantBuffer(BeConstantBuffer* pConstantBuffer)
{
    BeConstantBufferDX11* pConstantBufferAPI = reinterpret_cast<BeConstantBufferDX11*>(pConstantBuffer);

    BeDelete pConstantBufferAPI;
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderDeviceDX11::RenderIndexedPrimitive(BeVertexBuffer* pVB, BeIndexBuffer* pIB, BeVertexArrayObject* pVAO)
{
    SetVertexBuffer(pVB);

    SetIndexBuffer(pIB);

    m_pRenderDeviceImpl->m_pRenderDeviceContext->DrawIndexed(pIB->GetIndexBufferCount(), 0, 0);
}
/////////////////////////////////////////////////////////////////////////////
BeMaterialVertexDefinition* BeRenderDeviceDX11::CreateVertexDefinition(void)
{
    BeMaterialVertexDefinitionDX11* pVertexDefinition = BeNew BeMaterialVertexDefinitionDX11();

    return pVertexDefinition;
}
/////////////////////////////////////////////////////////////////////////////
BeVoidP BeRenderDeviceDX11::ResolveVertexDefinition(BeMaterialVertexDefinition* pVertexDefinition, BeVoidP pInputData, BeMaterialProgram* pMaterialProgram)
{
    BE_UNUSED(pMaterialProgram);

    BeMaterialVertexDefinitionDX11* pVertexDefinitionAPI = reinterpret_cast<BeMaterialVertexDefinitionDX11*>(pVertexDefinition);

    BeInt hr = S_FALSE;

    ID3D11InputLayout* pNewInputLayout = NULL;

    D3D11_INPUT_ELEMENT_DESC* pInputElements = reinterpret_cast<D3D11_INPUT_ELEMENT_DESC*>(pInputData);

    ID3DBlob* pVBCompiledData = reinterpret_cast<ID3DBlob*>(pMaterialProgram->GetMaterialProgramCompiledDataPtr());

    hr = m_pRenderDeviceImpl->m_pRenderDevice->CreateInputLayout(pInputElements, pVertexDefinition->GetAttributeCount(), pVBCompiledData->GetBufferPointer(), pVBCompiledData->GetBufferSize(), &pNewInputLayout);
    if (hr != S_OK)
    {
        BE_RENDER_DEBUG("DX11 device could not create the input layout!");
        return NULL;
    }

    return pNewInputLayout;
}
/////////////////////////////////////////////////////////////////////////////
BeVoidP BeRenderDeviceDX11::ResolveTextureSampler(BeVoidP pInputData)
{
    BeInt hr = S_FALSE;

    ID3D11SamplerState* pNewSamplerState = NULL;

    D3D11_SAMPLER_DESC* pSamplerDesc = reinterpret_cast<D3D11_SAMPLER_DESC*>(pInputData);

    hr = m_pRenderDeviceImpl->m_pRenderDevice->CreateSamplerState(pSamplerDesc, &pNewSamplerState);
    if (hr != S_OK)
    {
        BE_RENDER_DEBUG("DX11 device could not create the texture sampler!");
        return NULL;
    }

    return pNewSamplerState;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeVoidP BeRenderDeviceDX11::CreateRenderState(BeRenderStates::ERenderStatesType eRSType, BeVoidP pData)
{
    switch (eRSType)
    {
        case BeRenderStates::RENDER_STATES_RASTERIZER:
        {
            D3D11_RASTERIZER_DESC* pDesc = reinterpret_cast<D3D11_RASTERIZER_DESC*>(pData);

            ID3D11RasterizerState* pNewRenderState = NULL;

            m_pRenderDeviceImpl->m_pRenderDevice->CreateRasterizerState(pDesc, &pNewRenderState);

            return (BeVoidP) pNewRenderState;
        }

        case BeRenderStates::RENDER_STATES_BLEND:
        {
            D3D11_BLEND_DESC* pDesc = reinterpret_cast<D3D11_BLEND_DESC*>(pData);

            ID3D11BlendState* pNewRenderState = NULL;

            m_pRenderDeviceImpl->m_pRenderDevice->CreateBlendState(pDesc, &pNewRenderState);

            return (BeVoidP)pNewRenderState;
        }
    }

    return NULL;
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderDeviceDX11::UpdateRenderState(BeRenderStates::ERenderStatesType eRSType, BeVoidP pData)
{
    switch (eRSType)
    {
        case BeRenderStates::RENDER_STATES_RASTERIZER:
        {
            ID3D11RasterizerState* pRenderState = reinterpret_cast<ID3D11RasterizerState*>(pData);

            m_pRenderDeviceImpl->m_pRenderDeviceContext->RSSetState(pRenderState);

            break;
        }
        case BeRenderStates::RENDER_STATES_BLEND:
        {
            ID3D11BlendState* pRenderState = reinterpret_cast<ID3D11BlendState*>(pData);

            m_pRenderDeviceImpl->m_pRenderDeviceContext->OMSetBlendState(pRenderState, NULL, 0xffffffff);

            break;
        }
    }
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeRenderDeviceDX11::BindTexture(BeInt iCacheHandle, BeTexture::ETextureChannel eChannel)
{
    if (iCacheHandle != -1)
    {
        BeRenderTypes::BeTextureCacheEntry* pTextureCached = GetTextureManager()->GetTextureCacheEntry(iCacheHandle);

        if (pTextureCached)
        {
            BeTextureDX11* pTexture = reinterpret_cast<BeTextureDX11*>(pTextureCached->m_pTexture);

            ID3D11ShaderResourceView* pTextureAPI = reinterpret_cast<ID3D11ShaderResourceView*>(pTexture->GetTexturePtr());

            m_pRenderDeviceImpl->m_pRenderDeviceContext->PSSetShaderResources((BeInt) eChannel, 1, &pTextureAPI);
        }
    }
    else
    {
        ID3D11ShaderResourceView* dummyResource[1];

        dummyResource[0] = NULL;

        m_pRenderDeviceImpl->m_pRenderDeviceContext->PSSetShaderResources((BeInt)eChannel, 1, dummyResource);
    }
}
/////////////////////////////////////////////////////////////////////////////
BeTextureSampler* BeRenderDeviceDX11::CreateTextureSampler(void)
{
    BeTextureSamplerDX11* pNewTextureSampler = BeNew BeTextureSamplerDX11();

    return pNewTextureSampler;
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderDeviceDX11::SetSampler(BeTextureSampler* pTextureSampler, BeInt iSlot, BeRenderTypes::EBeMaterialPipeline eMaterialPipeline)
{
    if (pTextureSampler)
    {
        ID3D11SamplerState* pSamplerAPI = reinterpret_cast<ID3D11SamplerState*>(pTextureSampler->GetTextureSamplerPtr());

        switch (eMaterialPipeline)
        {
            case BeRenderTypes::MATERIAL_PIPELINE_GEOMETRY:
            {
                if (m_pCurrentGSSamplerSet != pTextureSampler)
                {
                    m_pRenderDeviceImpl->m_pRenderDeviceContext->GSSetSamplers(iSlot, 1, &pSamplerAPI);
                    m_pCurrentGSSamplerSet = pTextureSampler;
                }
                break;
            }
            case BeRenderTypes::MATERIAL_PIPELINE_VERTEX:
            {
                if (m_pCurrentVSSamplerSet != pTextureSampler)
                {
                    m_pRenderDeviceImpl->m_pRenderDeviceContext->VSSetSamplers(iSlot, 1, &pSamplerAPI);
                    m_pCurrentVSSamplerSet = pTextureSampler;
                }
                break;
            }
            case BeRenderTypes::MATERIAL_PIPELINE_PIXEL:
            {
                if (m_pCurrentPSSamplerSet != pTextureSampler)
                {
                    m_pRenderDeviceImpl->m_pRenderDeviceContext->PSSetSamplers(iSlot, 1, &pSamplerAPI);
                    m_pCurrentPSSamplerSet = pTextureSampler;
                }
                break;
            }
        }
    }
    else
    {
        switch (eMaterialPipeline)
        {
            case BeRenderTypes::MATERIAL_PIPELINE_GEOMETRY:
            {
                m_pRenderDeviceImpl->m_pRenderDeviceContext->GSSetSamplers(iSlot, 0, NULL);
                m_pCurrentGSSamplerSet = NULL;

                break;
            }
            case BeRenderTypes::MATERIAL_PIPELINE_VERTEX:
            {
                m_pRenderDeviceImpl->m_pRenderDeviceContext->VSSetSamplers(iSlot, 0, NULL);
                m_pCurrentVSSamplerSet = NULL;

                break;
            }
            case BeRenderTypes::MATERIAL_PIPELINE_PIXEL:
            {
                m_pRenderDeviceImpl->m_pRenderDeviceContext->PSSetSamplers(iSlot, 0, NULL);
                m_pCurrentPSSamplerSet = NULL;

                break;
            }
        }
    }
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeRenderDeviceDX11::SetMaterialForRendering(BeMaterial* pMaterial)
{
    pMaterial->SetMaterialForRendering(m_pRenderDeviceImpl->m_pRenderDeviceContext);
}
/////////////////////////////////////////////////////////////////////////////
BeMaterialProgram* BeRenderDeviceDX11::CreateMaterialProgramFromBuffer(BeRenderTypes::EBeMaterialPipeline eProgramType, BeVoidP pBufferData, BeInt iBufferSize, BeString8 strFunction)
{
    if (pBufferData != NULL)
    {
        HRESULT hr = -1;

        UINT flags = D3DCOMPILE_ENABLE_STRICTNESS;
#if defined( DEBUG ) || defined( _DEBUG )
        flags |= D3DCOMPILE_DEBUG;
#endif

        ID3D11VertexShader* pMaterialVS = NULL;
        ID3D11PixelShader* pMaterialPS = NULL;

        ID3DBlob* pDX11MaterialObject = NULL;
        ID3DBlob* pDX11MaterialObjectErrors = NULL;

        switch (eProgramType)
        {
            case BeRenderTypes::MATERIAL_PIPELINE_VERTEX:
            {
                hr = D3DCompile(pBufferData, iBufferSize, NULL, NULL, NULL, strFunction.ToRaw(), "vs_4_0_level_9_1", flags, 0, &pDX11MaterialObject, &pDX11MaterialObjectErrors);
                break;
            }
            case BeRenderTypes::MATERIAL_PIPELINE_PIXEL:
            {
                hr = D3DCompile(pBufferData, iBufferSize, NULL, NULL, NULL, strFunction.ToRaw(), "ps_4_0_level_9_1", flags, 0, &pDX11MaterialObject, &pDX11MaterialObjectErrors);
                break;
            }
        }

        // Return on any failure.
        if (FAILED(hr))
        {
            if (pDX11MaterialObjectErrors)
            {
                OutputDebugStringA((char*)pDX11MaterialObjectErrors->GetBufferPointer());
                pDX11MaterialObjectErrors->Release();
            }

            if (pDX11MaterialObject)
                pDX11MaterialObject->Release();

            return NULL;
        }

        if (pDX11MaterialObjectErrors)
            pDX11MaterialObjectErrors->Release();

        switch (eProgramType)
        {
            case BeRenderTypes::MATERIAL_PIPELINE_VERTEX:
            {
                hr = m_pRenderDeviceImpl->m_pRenderDevice->CreateVertexShader(pDX11MaterialObject->GetBufferPointer(), pDX11MaterialObject->GetBufferSize(), NULL, &pMaterialVS);
                break;
            }
            case BeRenderTypes::MATERIAL_PIPELINE_PIXEL:
            {
                hr = m_pRenderDeviceImpl->m_pRenderDevice->CreatePixelShader(pDX11MaterialObject->GetBufferPointer(), pDX11MaterialObject->GetBufferSize(), NULL, &pMaterialPS);
                break;
            }
        }

        if (FAILED(hr))
        {
            if (pDX11MaterialObject)
                pDX11MaterialObject->Release();

            return NULL;
        }

        BeMaterialProgramDX11* pNewMaterialProgram = BeNew BeMaterialProgramDX11 (eProgramType);

        pNewMaterialProgram->SetMaterialProgramCompiledDataPtr(pDX11MaterialObject);

        switch (eProgramType)
        {
            case BeRenderTypes::MATERIAL_PIPELINE_VERTEX:
            {
                pNewMaterialProgram->SetMaterialProgramPtr(pMaterialVS);
                break;
            }
            case BeRenderTypes::MATERIAL_PIPELINE_PIXEL:
            {
                pNewMaterialProgram->SetMaterialProgramPtr(pMaterialPS);
                break;
            }
        }

        return pNewMaterialProgram;
    }
    
    return NULL;
}
/////////////////////////////////////////////////////////////////////////////

