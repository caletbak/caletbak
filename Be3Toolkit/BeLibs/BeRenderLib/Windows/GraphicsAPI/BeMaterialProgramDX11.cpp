/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeMaterialProgramDX11.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <GraphicsAPI/BeMaterialProgramDX11.h>
#include <GraphicsAPI/BeConstantBufferDX11.h>
#include <GraphicsAPI/BeRenderDeviceDX11.h>
#include <GraphicsAPI/BeMaterialVertexDefinitionDX11.h>
#include <GraphicsAPI/BeTextureSamplerDX11.h>
#include <Render/BeMaterialConstant.h>

// DirectX11 libs
#pragma comment (lib, "d3d11.lib") 

#include <d3d11.h>

/////////////////////////////////////////////////////////////////////////////
class RENDER_API BeMaterialProgramDX11::BeMaterialProgramDX11Impl
{

public:

    /// \brief  Constructor
    BeMaterialProgramDX11Impl();
    /// \brief  Destructor
    ~BeMaterialProgramDX11Impl();

    /// \brief  API Material compiled data object
    ID3DBlob* m_pMaterialCompiledData;

    /// \brief  API Material object
    ID3D11VertexShader* m_pMaterialVS;
    ID3D11PixelShader* m_pMaterialPS;

    /// \brief  API Material constants buffer
    BeConstantBufferDX11* m_pConstantBufferAPI;
    /// \brief  Material constants buffer
    BeUChar8* m_pConstantBuffer;
    /// \brief  Material constants buffer size
    BeInt m_iConstantBufferSize;
    /// \brief  Material constants buffer offsets
    std::map<BeString8, BeInt> m_kConstantBufferOffsets;
};
/////////////////////////////////////////////////////////////////////////////
BeMaterialProgramDX11::BeMaterialProgramDX11Impl::BeMaterialProgramDX11Impl()
:
    m_pMaterialCompiledData(NULL),
    m_pMaterialVS(NULL),
    m_pMaterialPS(NULL),
    m_pConstantBufferAPI(NULL),
    m_pConstantBuffer(NULL),
    m_iConstantBufferSize(0)
{
}
/////////////////////////////////////////////////////////////////////////////
BeMaterialProgramDX11::BeMaterialProgramDX11Impl::~BeMaterialProgramDX11Impl()
{
    if (m_pMaterialCompiledData != NULL)
    {
        m_pMaterialCompiledData->Release();
        m_pMaterialCompiledData = NULL;
    }

    if (m_pMaterialVS != NULL)
    {
        m_pMaterialVS->Release();
        m_pMaterialVS = NULL;
    }

    if (m_pMaterialPS != NULL)
    {
        m_pMaterialPS->Release();
        m_pMaterialPS = NULL;
    }

    if (m_pConstantBufferAPI != NULL)
    {
        BeDelete m_pConstantBufferAPI;
        m_pConstantBufferAPI = NULL;
    }
    
    if (m_pConstantBuffer != NULL)
    {
        BeDelete[] m_pConstantBuffer;
        m_pConstantBuffer = NULL;
    }

    m_kConstantBufferOffsets.clear();
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeMaterialProgramDX11::BeMaterialProgramDX11(BeRenderTypes::EBeMaterialPipeline eProgramType)
:
    m_pMaterialProgramImpl(NULL)
{
    m_eProgramType = eProgramType;

    m_pMaterialProgramImpl = BeNew BeMaterialProgramDX11Impl();
}
/////////////////////////////////////////////////////////////////////////////
BeMaterialProgramDX11::~BeMaterialProgramDX11()
{
    if (m_pMaterialProgramImpl != NULL)
    {
        BeDelete m_pMaterialProgramImpl;
        m_pMaterialProgramImpl = NULL;
    }
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeMaterialProgramDX11::PrepareProgram(BeRenderDevice* pRenderDevice)
{
    // Creating the constant buffer struct in memory
    BeInt iConstantBufferSize = 0;

    std::vector<BeMaterialConstant*>::iterator it = m_kConstants.begin();
    while (it != m_kConstants.end())
    {
        BeMaterialConstant* pConstant = reinterpret_cast<BeMaterialConstant*>(*it);

        m_pMaterialProgramImpl->m_kConstantBufferOffsets[pConstant->m_strName] = iConstantBufferSize;

        iConstantBufferSize += pConstant->GetTypeSizeInMemory();

        it++;
    }

    if (iConstantBufferSize > 0)
    {
        m_pMaterialProgramImpl->m_iConstantBufferSize = iConstantBufferSize;
        m_pMaterialProgramImpl->m_pConstantBuffer = BeNew BeUChar8[iConstantBufferSize];
        memset(m_pMaterialProgramImpl->m_pConstantBuffer, 0, iConstantBufferSize);

        BeConstantBuffer* pConstantBufferAPI = pRenderDevice->CreateConstantBuffer(m_pMaterialProgramImpl->m_pConstantBuffer, m_pMaterialProgramImpl->m_iConstantBufferSize);
        if (pConstantBufferAPI != NULL)
        {
            m_pMaterialProgramImpl->m_pConstantBufferAPI = reinterpret_cast<BeConstantBufferDX11*>(pConstantBufferAPI);
        }
    }
}
/////////////////////////////////////////////////////////////////////////////
void BeMaterialProgramDX11::UpdateProgram(BeRenderDevice* pRenderDevice)
{
    m_kPendingConstantsMutex.Lock();

    std::map<BeString8, BeVoidP>::iterator it = m_kPendingConstantUpdates.begin();
    while (it != m_kPendingConstantUpdates.end())
    {
        BeMaterialConstant* pConstant = GetConstantByName(it->first);

        memcpy(&m_pMaterialProgramImpl->m_pConstantBuffer[m_pMaterialProgramImpl->m_kConstantBufferOffsets[pConstant->m_strName]], it->second, pConstant->GetTypeSize());

        it++;
    }
    m_kPendingConstantUpdates.clear();

    m_kPendingConstantsMutex.UnLock();

    if (m_pMaterialProgramImpl->m_pConstantBufferAPI)
    {
        pRenderDevice->UpdateConstantBuffer(m_pMaterialProgramImpl->m_pConstantBufferAPI, m_pMaterialProgramImpl->m_pConstantBuffer, m_pMaterialProgramImpl->m_iConstantBufferSize);
    }
}
/////////////////////////////////////////////////////////////////////////////
void BeMaterialProgramDX11::SetConstantBuffer(BeRenderDevice* pRenderDevice)
{
    if (m_pMaterialProgramImpl->m_pConstantBufferAPI)
    {
        pRenderDevice->SetConstantBuffer(m_pMaterialProgramImpl->m_pConstantBufferAPI, m_eProgramType);
    }
}
/////////////////////////////////////////////////////////////////////////////
void BeMaterialProgramDX11::SetTextureSamplers(BeRenderDevice* pRenderDevice)
{
    for (BeUInt i = 0; i < m_kTextureChannels.size(); ++i)
    {
        BeRenderTypes::BeTextureChannel* pTextureChannel = m_kTextureChannels[i];

        BeTextureSamplerDX11* pTextureSampler = reinterpret_cast<BeTextureSamplerDX11*>(pTextureChannel->m_pTextureSampler);

        pRenderDevice->SetSampler(pTextureSampler, i, m_eProgramType);
    }
}
/////////////////////////////////////////////////////////////////////////////
void BeMaterialProgramDX11::SetProgramForRendering(BeVoidP pRenderContext)
{
    ID3D11DeviceContext* pContext = reinterpret_cast<ID3D11DeviceContext*>(pRenderContext);

    switch (m_eProgramType)
    {
        case BeRenderTypes::MATERIAL_PIPELINE_VERTEX:
        {
            ID3D11VertexShader* pVS = reinterpret_cast<ID3D11VertexShader*>(m_pMaterialProgramImpl->m_pMaterialVS);
            pContext->VSSetShader(pVS, NULL, 0);

            break;
        }
        case BeRenderTypes::MATERIAL_PIPELINE_PIXEL:
        {
            ID3D11PixelShader* pPS = reinterpret_cast<ID3D11PixelShader*>(m_pMaterialProgramImpl->m_pMaterialPS);
            pContext->PSSetShader(pPS, NULL, 0);

            break;
        }
    }

    if (m_pVertexDefinition)
    {
        BeMaterialVertexDefinitionDX11* pVertexDefinitionDX11 = reinterpret_cast<BeMaterialVertexDefinitionDX11*>(m_pVertexDefinition);

        ID3D11InputLayout* pInputLayout = reinterpret_cast<ID3D11InputLayout*>(pVertexDefinitionDX11->GetVertexDefinitionPtr());

        pContext->IASetInputLayout(pInputLayout);
    }
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeVoidP BeMaterialProgramDX11::GetMaterialProgramCompiledDataPtr(void)
{
    return m_pMaterialProgramImpl->m_pMaterialCompiledData;
}
/////////////////////////////////////////////////////////////////////////////
BeInt BeMaterialProgramDX11::GetConstantIndicesCount(void)
{
    return 0;
}
/////////////////////////////////////////////////////////////////////////////
BeVoidP BeMaterialProgramDX11::GetMaterialProgramPtr(void)
{
    switch (m_eProgramType)
    {
        case BeRenderTypes::MATERIAL_PIPELINE_VERTEX:
        {
            return m_pMaterialProgramImpl->m_pMaterialVS;
        }
        case BeRenderTypes::MATERIAL_PIPELINE_PIXEL:
        {
            return m_pMaterialProgramImpl->m_pMaterialPS;
        }
    }

    return NULL;
}
/////////////////////////////////////////////////////////////////////////////
void BeMaterialProgramDX11::SetMaterialProgramCompiledDataPtr(BeVoidP pMaterialCompiledDataPtr)
{
    m_pMaterialProgramImpl->m_pMaterialCompiledData = reinterpret_cast<ID3DBlob*>(pMaterialCompiledDataPtr);
}
/////////////////////////////////////////////////////////////////////////////
void BeMaterialProgramDX11::SetMaterialProgramPtr(BeVoidP pMaterialProgramPtr)
{
    switch (m_eProgramType)
    {
        case BeRenderTypes::MATERIAL_PIPELINE_VERTEX:
        {
            m_pMaterialProgramImpl->m_pMaterialVS = reinterpret_cast<ID3D11VertexShader*>(pMaterialProgramPtr);
            break;
        }
        case BeRenderTypes::MATERIAL_PIPELINE_PIXEL:
        {
            m_pMaterialProgramImpl->m_pMaterialPS = reinterpret_cast<ID3D11PixelShader*>(pMaterialProgramPtr);
            break;
        }
    }
}
/////////////////////////////////////////////////////////////////////////////

