/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeTextureSamplerDX11.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <GraphicsAPI/BeTextureSamplerDX11.h>
#include <Render/BeRenderDevice.h>

// DirectX11 libs
#pragma comment (lib, "d3d11.lib") 

#include <d3d11.h>



/////////////////////////////////////////////////////////////////////////////
class RENDER_API BeTextureSamplerDX11::BeTextureSamplerDX11Impl
{

public:

    /// \brief  Constructor
    BeTextureSamplerDX11Impl();
    /// \brief  Destructor
    ~BeTextureSamplerDX11Impl();

    /// \brief  API texture sampler object
    ID3D11SamplerState* m_pTextureSampler;

};
/////////////////////////////////////////////////////////////////////////////
BeTextureSamplerDX11::BeTextureSamplerDX11Impl::BeTextureSamplerDX11Impl()
:
    m_pTextureSampler(NULL)
{
}
/////////////////////////////////////////////////////////////////////////////
BeTextureSamplerDX11::BeTextureSamplerDX11Impl::~BeTextureSamplerDX11Impl()
{
    if (m_pTextureSampler)
    {
        m_pTextureSampler->Release();
        m_pTextureSampler = NULL;
    }
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeTextureSamplerDX11::BeTextureSamplerDX11()
:
    m_pTextureSamplerImpl(NULL)
{
    m_pTextureSamplerImpl = BeNew BeTextureSamplerDX11Impl();
}
/////////////////////////////////////////////////////////////////////////////
BeTextureSamplerDX11::~BeTextureSamplerDX11()
{
    if (m_pTextureSamplerImpl != NULL)
    {
        BeDelete m_pTextureSamplerImpl;
        m_pTextureSamplerImpl = NULL;
    }
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeTextureSamplerDX11::PrepareSampler(BeRenderDevice* pRenderDevice)
{
    D3D11_SAMPLER_DESC kSamplerDesc;
    kSamplerDesc.AddressU = (D3D11_TEXTURE_ADDRESS_MODE) ConvertAddressToAPI(m_strAddressU);
    kSamplerDesc.AddressV = (D3D11_TEXTURE_ADDRESS_MODE) ConvertAddressToAPI(m_strAddressV);
    kSamplerDesc.AddressW = (D3D11_TEXTURE_ADDRESS_MODE) ConvertAddressToAPI(m_strAddressW);
    kSamplerDesc.BorderColor[0] = m_kBorderColor.m_fX;
    kSamplerDesc.BorderColor[1] = m_kBorderColor.m_fY;
    kSamplerDesc.BorderColor[2] = m_kBorderColor.m_fZ;
    kSamplerDesc.BorderColor[3] = m_kBorderColor.m_fW;
    kSamplerDesc.ComparisonFunc = (D3D11_COMPARISON_FUNC) ConvertComparisionFuncToAPI(m_strComparisionFunc);
    kSamplerDesc.Filter = (D3D11_FILTER) ConvertFilterToAPI(m_strFilterMin, m_strFilterMag, m_strFilterMip, m_strFilterFunc);
    kSamplerDesc.MaxAnisotropy = m_MaxAnisotropy;
    kSamplerDesc.MaxLOD = m_MaxLOD;
    kSamplerDesc.MinLOD = m_MinLOD;
    kSamplerDesc.MipLODBias = m_MipLODBias;

    BeVoidP pTextureSampler = pRenderDevice->ResolveTextureSampler((BeVoidP) &kSamplerDesc);

    ID3D11SamplerState* pTextureAPISampler = reinterpret_cast<ID3D11SamplerState*>(pTextureSampler);

    m_pTextureSamplerImpl->m_pTextureSampler = pTextureAPISampler;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeVoidP BeTextureSamplerDX11::GetTextureSamplerPtr(void)
{
    return m_pTextureSamplerImpl->m_pTextureSampler;
}
/////////////////////////////////////////////////////////////////////////////
void BeTextureSamplerDX11::SetTextureSamplerPtr(BeVoidP pSamplerPtr)
{
    ID3D11SamplerState* pSamplerAPI = reinterpret_cast<ID3D11SamplerState*>(pSamplerPtr);

    m_pTextureSamplerImpl->m_pTextureSampler = pSamplerAPI;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeInt BeTextureSamplerDX11::ConvertFilterToAPI(const BeString16& strMinFilter, const BeString16& strMagFilter, const BeString16& strMipFilter, const BeString16& strFilterFunc)
{
    BeInt iFilterResult = 0;

    BeInt iMinFilter = 0;
    BeInt iMagFilter = 0;
    BeInt iMipFilter = 0;

    if (strMinFilter.Compare(L"LINEAR") == 0)
    {
        iMinFilter = 1;
    }
    if (strMagFilter.Compare(L"LINEAR") == 0)
    {
        iMagFilter = 1;
    }
    if (strMipFilter.Compare(L"LINEAR") == 0)
    {
        iMipFilter = 1;
    }

    BeInt iFilterFunc = 0;
    if (strFilterFunc.Compare(L"COMPARISION") == 0)
    {
        iFilterFunc = 1;
    }
    else if (strFilterFunc.Compare(L"MINIMUM") == 0)
    {
        iFilterFunc = 2;
    }
    else if (strFilterFunc.Compare(L"MAXIMUM") == 0)
    {
        iFilterFunc = 3;
    }

    iFilterResult += (iMinFilter << 4);
    iFilterResult += (iMagFilter << 2);
    iFilterResult += iMipFilter;

    if (strMinFilter.Compare(L"ANISOTROPIC") == 0 || strMagFilter.Compare(L"ANISOTROPIC") == 0 || strMipFilter.Compare(L"ANISOTROPIC") == 0)
    {
        iFilterResult = D3D11_FILTER_ANISOTROPIC;
    }

    iFilterResult += (0x80 * iFilterFunc);

    return iFilterResult;
}
/////////////////////////////////////////////////////////////////////////////
BeInt BeTextureSamplerDX11::ConvertAddressToAPI(const BeString16& strAddress)
{
    if (strAddress.Compare(L"WRAP") == 0)
    {
        return D3D11_TEXTURE_ADDRESS_WRAP;
    }
    else if (strAddress.Compare(L"MIRROR") == 0)
    {
        return D3D11_TEXTURE_ADDRESS_MIRROR;
    }
    else if (strAddress.Compare(L"CLAMP") == 0)
    {
        return D3D11_TEXTURE_ADDRESS_CLAMP;
    }
    else if (strAddress.Compare(L"BORDER") == 0)
    {
        return D3D11_TEXTURE_ADDRESS_BORDER;
    }
    else if (strAddress.Compare(L"MIRROR_ONCE") == 0)
    {
        return D3D11_TEXTURE_ADDRESS_MIRROR_ONCE;
    }

    return 0;
}
/////////////////////////////////////////////////////////////////////////////
BeInt BeTextureSamplerDX11::ConvertComparisionFuncToAPI(const BeString16& strFunc)
{
    if (strFunc.Compare(L"NEVER") == 0)
    {
        return D3D11_COMPARISON_NEVER;
    }
    else if (strFunc.Compare(L"LESS") == 0)
    {
        return D3D11_COMPARISON_LESS;
    }
    else if (strFunc.Compare(L"EQUAL") == 0)
    {
        return D3D11_COMPARISON_EQUAL;
    }
    else if (strFunc.Compare(L"LESS_EQUAL") == 0)
    {
        return D3D11_COMPARISON_LESS_EQUAL;
    }
    else if (strFunc.Compare(L"GREATER") == 0)
    {
        return D3D11_COMPARISON_GREATER;
    }
    else if (strFunc.Compare(L"NOT_EQUAL") == 0)
    {
        return D3D11_COMPARISON_NOT_EQUAL;
    }
    else if (strFunc.Compare(L"GREATER_EQUAL") == 0)
    {
        return D3D11_COMPARISON_GREATER_EQUAL;
    }
    else if (strFunc.Compare(L"ALWAYS") == 0)
    {
        return D3D11_COMPARISON_ALWAYS;
    }

    return 0;
}
/////////////////////////////////////////////////////////////////////////////

