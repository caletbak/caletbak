/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeRenderDeviceDX11Enumerator.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <GraphicsAPI/BeRenderDeviceDX11Enumerator.h>

// DirectX11 libs
#pragma comment (lib, "d3d11.lib") 
#pragma comment (lib, "d3d11.lib")
#pragma comment (lib, "DXGI.lib")

// DirectX11 includes
#include <d3d11.h>
#include <DXGI.h>



/////////////////////////////////////////////////////////////////////////////
class RENDER_API BeRenderDeviceDX11Enumerator::BeDX11EnumOutputInfo
{
    private:

    /// \brief  Disable equal operator
    const BeDX11EnumOutputInfo& operator =( const BeDX11EnumOutputInfo& rhs );

  public:

    /// \brief  Destructor
    ~BeDX11EnumOutputInfo();

    /// \brief  The adapter ordinal index
    BeUInt m_uAdapterOrdinal;
    /// \brief  The output ordinal index
    BeUInt m_uOutput;
    /// \brief  The output
    IDXGIOutput* m_pOutput;
    /// \brief  The output description
    DXGI_OUTPUT_DESC m_kOutputDesc;

    /// \brief  List of available display modes
    std::vector<DXGI_MODE_DESC> m_kDisplayModeList;

};
/////////////////////////////////////////////////////////////////////////////
BeRenderDeviceDX11Enumerator::BeDX11EnumOutputInfo::~BeDX11EnumOutputInfo()
{
    if (m_pOutput)
    {
        m_pOutput->Release();
        m_pOutput = NULL;
    }

    m_kDisplayModeList.clear();
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
class RENDER_API BeRenderDeviceDX11Enumerator::BeDX11EnumDeviceInfo
{
  private:

    /// \brief  Disable equal operator
    const BeDX11EnumDeviceInfo& operator =( const BeDX11EnumDeviceInfo& rhs );

  public:

    /// \brief  Destructor
    ~BeDX11EnumDeviceInfo();

    /// \brief  The adapter ordinal index
    BeUInt m_uAdapterOrdinal;
    /// \brief  The device type
    D3D_DRIVER_TYPE m_kDeviceType;

};
/////////////////////////////////////////////////////////////////////////////
BeRenderDeviceDX11Enumerator::BeDX11EnumDeviceInfo::~BeDX11EnumDeviceInfo()
{
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
class RENDER_API BeRenderDeviceDX11Enumerator::BeDX11EnumDeviceSettingsCombo
{

  public:

    /// \brief  Destructor
    ~BeDX11EnumDeviceSettingsCombo();

    /// \brief  The adapter ordinal index
    BeUInt m_uAdapterOrdinal;
    /// \brief  The device type
    D3D_DRIVER_TYPE m_kDeviceType;
    /// \brief  The backbuffer format
    DXGI_FORMAT m_kBackBufferFormat;
    /// \brief  The backbuffer format
    BeBoolean m_bIsWindowed;
    /// \brief  The output ordinal index
    BeUInt m_uOutput;

    /// \brief  List of valid sampling counts (multisampling)
    std::vector<BeUInt> m_kMultiSampleCountList;
    /// \brief  List of number of quality levels for each multisample count
    std::vector<BeUInt> m_kMultiSampleQualityList;

    /// \brief  The adapter info
    BeRenderDeviceDX11Enumerator::BeDX11EnumAdapterInfo* m_pAdapterInfo;
    /// \brief  The device info
    BeRenderDeviceDX11Enumerator::BeDX11EnumDeviceInfo* m_pDeviceInfo;
    /// \brief  The output info
    BeRenderDeviceDX11Enumerator::BeDX11EnumOutputInfo* m_pOutputInfo;

};
/////////////////////////////////////////////////////////////////////////////
BeRenderDeviceDX11Enumerator::BeDX11EnumDeviceSettingsCombo::~BeDX11EnumDeviceSettingsCombo()
{
  m_kMultiSampleCountList.clear();
  m_kMultiSampleQualityList.clear();

  m_pAdapterInfo = NULL;
    m_pDeviceInfo = NULL;
    m_pOutputInfo = NULL;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
class RENDER_API BeRenderDeviceDX11Enumerator::BeDX11EnumAdapterInfo
{
  private:

    /// \brief  Disable equal operator
    const BeDX11EnumAdapterInfo& operator =( const BeDX11EnumAdapterInfo& rhs );

  public:
      
    /// \brief  Destructor
    ~BeDX11EnumAdapterInfo();

    /// \brief  The adapter ordinal index
    BeUInt m_uAdapterOrdinal;
    /// \brief  The adapter description
    DXGI_ADAPTER_DESC m_kAdapterDesc;
    /// \brief  The adapter unique name
    BeString16 m_strUniqueDescription;
    /// \brief  The DXGI adapter
    IDXGIAdapter* m_pAdapter;

    /// \brief  List of outputs info
    std::vector<BeRenderDeviceDX11Enumerator::BeDX11EnumOutputInfo*> m_kOutputInfoList;
    /// \brief  List of devices info
    std::vector<BeRenderDeviceDX11Enumerator::BeDX11EnumDeviceInfo*> m_kDeviceInfoList;
      
    // List of BeDX11EnumDeviceSettingsCombo* with a unique set of BackBufferFormat, and Windowed
    std::vector<BeRenderDeviceDX11Enumerator::BeDX11EnumDeviceSettingsCombo*> m_kDeviceSettingsComboList;

};
/////////////////////////////////////////////////////////////////////////////
BeRenderDeviceDX11Enumerator::BeDX11EnumAdapterInfo::~BeDX11EnumAdapterInfo()
{
  for ( BeUInt i = 0; i < m_kOutputInfoList.size(); ++i )
  {
    BeRenderDeviceDX11Enumerator::BeDX11EnumOutputInfo* pOutputInfo = m_kOutputInfoList.at(i);
    BeDelete pOutputInfo;
    pOutputInfo = NULL;
  }
  m_kOutputInfoList.clear();

  for ( BeUInt i = 0; i < m_kDeviceInfoList.size(); ++i )
  {
    BeRenderDeviceDX11Enumerator::BeDX11EnumDeviceInfo* pDeviceInfo = m_kDeviceInfoList.at(i);
    BeDelete pDeviceInfo;
    pDeviceInfo = NULL;
  }
  m_kDeviceInfoList.clear();

  for ( BeUInt i = 0; i < m_kDeviceSettingsComboList.size(); ++i )
  {
    BeRenderDeviceDX11Enumerator::BeDX11EnumDeviceSettingsCombo* pSettingsCombo = m_kDeviceSettingsComboList.at(i);
    BeDelete pSettingsCombo;
    pSettingsCombo = NULL;
  }
  m_kDeviceSettingsComboList.clear();

  if (m_pAdapter)
  {
      m_pAdapter->Release();
      m_pAdapter = NULL;
  }
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeRenderDeviceDX11Enumerator::BeRenderDeviceDX11Enumerator()
:
  m_pCurrentSelectedSettingsCombo(NULL)
{
}
/////////////////////////////////////////////////////////////////////////////
BeRenderDeviceDX11Enumerator::~BeRenderDeviceDX11Enumerator()
{
  for ( BeUInt i = 0; i < m_kAdapterInfoList.size(); ++i )
  {
    BeDX11EnumAdapterInfo* pAdapterInfo = m_kAdapterInfoList.at(i);
    BeDelete pAdapterInfo;
    pAdapterInfo = NULL;
  }
  m_kAdapterInfoList.clear();
  
  if ( m_pFactory != NULL )
  {
    IDXGIFactory* pFactory = static_cast<IDXGIFactory*>(m_pFactory);
    pFactory->Release();
    
    pFactory = NULL;
    m_pFactory = NULL;
  }
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderDeviceDX11Enumerator::BuildMultiSampleQualityList( BeInt format, BeDX11EnumDeviceSettingsCombo* pDeviceCombo )
{
  DXGI_FORMAT fmt = (DXGI_FORMAT)format;

    ID3D11Device* pd3dDevice = NULL;
    ID3D11DeviceContext* pd3dDeviceContext = NULL;
    IDXGIAdapter* pAdapter = NULL;
    if( pDeviceCombo->m_kDeviceType == D3D_DRIVER_TYPE_HARDWARE )
    {
    IDXGIFactory* factory = static_cast<IDXGIFactory*>(m_pFactory);
        factory->EnumAdapters( pDeviceCombo->m_pAdapterInfo->m_uAdapterOrdinal, &pAdapter );
    }

    if (FAILED(D3D11CreateDevice(pAdapter, pDeviceCombo->m_kDeviceType, (HMODULE)0, 0, NULL, 0, D3D11_SDK_VERSION, &pd3dDevice, NULL, &pd3dDeviceContext)))
        return;
    
    for( int i = 1; i <= D3D11_MAX_MULTISAMPLE_SAMPLE_COUNT; ++i )
    {
        UINT Quality;
        if( SUCCEEDED( pd3dDevice->CheckMultisampleQualityLevels( fmt, i, &Quality ) ) && Quality > 0 )
        {
            pDeviceCombo->m_kMultiSampleCountList.push_back( i );
            pDeviceCombo->m_kMultiSampleQualityList.push_back( Quality );
        }
    }

    if ( pAdapter )
        pAdapter->Release();
    if (pd3dDeviceContext)
    {
        pd3dDeviceContext->ClearState();
        pd3dDeviceContext->Flush();
        pd3dDeviceContext->Release();
    }
    if ( pd3dDevice )
        pd3dDevice->Release();
}
/////////////////////////////////////////////////////////////////////////////
BeInt BeRenderDeviceDX11Enumerator::EnumerateDeviceCombos( BeDX11EnumAdapterInfo* pAdapterInfo )
{
  // Iterate through each combination of device driver type, output,
    // adapter format, and backbuffer format to build the adapter's device combo list.
    //

    for( BeUInt output = 0; output < pAdapterInfo->m_kOutputInfoList.size(); ++output )
    {
        BeDX11EnumOutputInfo* pOutputInfo = pAdapterInfo->m_kOutputInfoList.at( output );

        for( BeUInt device = 0; device < pAdapterInfo->m_kDeviceInfoList.size(); ++device )
        {
            BeDX11EnumDeviceInfo* pDeviceInfo = pAdapterInfo->m_kDeviceInfoList.at( device );

            DXGI_FORMAT backBufferFormatArray[] =
                {
                    DXGI_FORMAT_R8G8B8A8_UNORM_SRGB,   //This is DXUT's preferred mode

                    DXGI_FORMAT_R8G8B8A8_UNORM,    
                    DXGI_FORMAT_R16G16B16A16_FLOAT,
                    DXGI_FORMAT_R10G10B10A2_UNORM
                };
            const UINT backBufferFormatArrayCount = sizeof( backBufferFormatArray ) / sizeof
                ( backBufferFormatArray[0] );

            // Swap perferred modes for apps running in linear space
            //if( !DXUTIsInGammaCorrectMode() )
            //{
                backBufferFormatArray[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
                backBufferFormatArray[1] = DXGI_FORMAT_R8G8B8A8_UNORM_SRGB;
            //}

            for( BeUInt iBackBufferFormat = 0; iBackBufferFormat < backBufferFormatArrayCount; iBackBufferFormat++ )
            {
                DXGI_FORMAT backBufferFormat = backBufferFormatArray[iBackBufferFormat];

                for( BeInt nWindowed = 0; nWindowed < 2; nWindowed++ )
                {
                    if( !nWindowed && pOutputInfo->m_kDisplayModeList.size() == 0 )
                        continue;

                    // determine if there are any modes for this particular format
                    UINT iModes = 0;
                    for( BeUInt i = 0; i < pOutputInfo->m_kDisplayModeList.size(); i++ )
                    {
                        if( backBufferFormat == pOutputInfo->m_kDisplayModeList.at( i ).Format )
                            iModes ++;
                    }
                    if( 0 == iModes )
                        continue;

                    // If an application callback function has been provided, make sure this device
                    // is acceptable to the app.
          /*
                    if( m_IsD3D11DeviceAcceptableFunc != NULL )
                    {
                        if( !m_IsD3D11DeviceAcceptableFunc( pAdapterInfo->AdapterOrdinal, output,
                                                            pDeviceInfo->DeviceType, backBufferFormat,
                                                            FALSE != nWindowed,
                                                            m_pIsD3D11DeviceAcceptableFuncUserContext ) )
                            continue;
                    }
          */

                    // At this point, we have an adapter/device/backbufferformat/iswindowed
                    // DeviceCombo that is supported by the system. We still 
                    // need to find one or more suitable depth/stencil buffer format,
                    // multisample type, and present interval.
                    BeDX11EnumDeviceSettingsCombo* pDeviceCombo = BeNew BeDX11EnumDeviceSettingsCombo;
                    if( pDeviceCombo == NULL )
                        return E_OUTOFMEMORY;

                    pDeviceCombo->m_uAdapterOrdinal = pDeviceInfo->m_uAdapterOrdinal;
                    pDeviceCombo->m_kDeviceType = pDeviceInfo->m_kDeviceType;
                    pDeviceCombo->m_kBackBufferFormat = backBufferFormat;
                    pDeviceCombo->m_bIsWindowed = ( nWindowed != 0 );
                    pDeviceCombo->m_uOutput = pOutputInfo->m_uOutput;
                    pDeviceCombo->m_pAdapterInfo = pAdapterInfo;
                    pDeviceCombo->m_pDeviceInfo = pDeviceInfo;
                    pDeviceCombo->m_pOutputInfo = pOutputInfo;

                    BuildMultiSampleQualityList( backBufferFormat, pDeviceCombo );

                    pAdapterInfo->m_kDeviceSettingsComboList.push_back( pDeviceCombo );
                }
            }
        }
    }

    return S_OK;

}
/////////////////////////////////////////////////////////////////////////////
BeInt BeRenderDeviceDX11Enumerator::EnumerateDisplayModes( BeDX11EnumOutputInfo* pOutputInfo )
{

    HRESULT hr = S_OK;
    DXGI_FORMAT allowedAdapterFormatArray[] =
    {
        DXGI_FORMAT_R8G8B8A8_UNORM_SRGB,     //This is DXUT's preferred mode

        DXGI_FORMAT_R8G8B8A8_UNORM,      
        DXGI_FORMAT_R16G16B16A16_FLOAT,
        DXGI_FORMAT_R10G10B10A2_UNORM
    };

    int allowedAdapterFormatArrayCount = sizeof( allowedAdapterFormatArray ) / sizeof( allowedAdapterFormatArray[0] );

    // Swap perferred modes for apps running in linear space
    DXGI_FORMAT RemoteMode = DXGI_FORMAT_R8G8B8A8_UNORM_SRGB;
    //if( !DXUTIsInGammaCorrectMode() )
  //{
        allowedAdapterFormatArray[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
        allowedAdapterFormatArray[1] = DXGI_FORMAT_R8G8B8A8_UNORM_SRGB;
        RemoteMode = DXGI_FORMAT_R8G8B8A8_UNORM;
    //}

    // The fast path only enumerates R8G8B8A8_UNORM_SRGB modes
    //if( !m_bEnumerateAllAdapterFormats )
    //    allowedAdapterFormatArrayCount = 1;

    for( int f = 0; f < allowedAdapterFormatArrayCount; ++f )
    {
        // Fast-path: Try to grab at least 512 modes.
        //        This is to avoid calling GetDisplayModeList more times than necessary.
        //        GetDisplayModeList is an expensive call.
        UINT NumModes = 512;
        DXGI_MODE_DESC* pDesc = BeNew DXGI_MODE_DESC[ NumModes ];
        assert( pDesc );
        if( !pDesc )
            return E_OUTOFMEMORY;

        hr = pOutputInfo->m_pOutput->GetDisplayModeList( allowedAdapterFormatArray[f],
                                                         DXGI_ENUM_MODES_SCALING,
                                                         &NumModes,
                                                         pDesc );
        if( DXGI_ERROR_NOT_FOUND == hr )
        {
            BeDelete pDesc;
            NumModes = 0;
            break;
        }
        else if( MAKE_DXGI_HRESULT( 34 ) == hr && RemoteMode == allowedAdapterFormatArray[f] )
        {
            // DXGI cannot enumerate display modes over a remote session.  Therefore, create a fake display
            // mode for the current screen resolution for the remote session.
            if( 0 != GetSystemMetrics( 0x1100 ) ) // SM_REMOTESESSION
            {
                DEVMODE DevMode;
                DevMode.dmSize = sizeof( DEVMODE );
                if( EnumDisplaySettings( NULL, ENUM_CURRENT_SETTINGS, &DevMode ) )
                {
                    NumModes = 1;
                    pDesc[0].Width = DevMode.dmPelsWidth;
                    pDesc[0].Height = DevMode.dmPelsHeight;
                    pDesc[0].Format = RemoteMode;
                    pDesc[0].RefreshRate.Numerator = 60;
                    pDesc[0].RefreshRate.Denominator = 1;
                    pDesc[0].ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_PROGRESSIVE;
                    pDesc[0].Scaling = DXGI_MODE_SCALING_CENTERED;
                    hr = S_OK;
                }
            }
        }
        else if( DXGI_ERROR_MORE_DATA == hr )
        {
            // Slow path.  There were more than 512 modes.
            BeDelete pDesc;
            hr = pOutputInfo->m_pOutput->GetDisplayModeList( allowedAdapterFormatArray[f],
                                                             DXGI_ENUM_MODES_SCALING,
                                                             &NumModes,
                                                             NULL );
            if( FAILED( hr ) )
            {
                NumModes = 0;
                break;
            }

            pDesc = new DXGI_MODE_DESC[ NumModes ];
            assert( pDesc );
            if( !pDesc )
                return E_OUTOFMEMORY;

            hr = pOutputInfo->m_pOutput->GetDisplayModeList( allowedAdapterFormatArray[f],
                                                             DXGI_ENUM_MODES_SCALING,
                                                             &NumModes,
                                                             pDesc );
            if( FAILED( hr ) )
            {
                BeDelete pDesc;
                NumModes = 0;
                break;
            }

        }

        if( 0 == NumModes && 0 == f )
        {
            // No R8G8B8A8_UNORM_SRGB modes!
            // Abort the fast-path if we're on it
            allowedAdapterFormatArrayCount = sizeof( allowedAdapterFormatArray ) / sizeof
                ( allowedAdapterFormatArray[0] );
            BeDelete pDesc;
            continue;
        }

        if( SUCCEEDED( hr ) )
        {
            for( UINT m = 0; m < NumModes; m++ )
            {
                pOutputInfo->m_kDisplayModeList.push_back( pDesc[m] );
            }
        }

        BeDelete pDesc;
    }

    return hr;
}
/////////////////////////////////////////////////////////////////////////////
BeInt BeRenderDeviceDX11Enumerator::EnumerateDevices( BeDX11EnumAdapterInfo* pAdapterInfo )
{
    HRESULT hr;

    const D3D_DRIVER_TYPE devTypeArray[] =
    {
        D3D_DRIVER_TYPE_HARDWARE,
        D3D_DRIVER_TYPE_SOFTWARE,
        D3D_DRIVER_TYPE_REFERENCE,
    };

    const UINT devTypeArrayCount = sizeof( devTypeArray ) / sizeof( devTypeArray[0] );

    // Enumerate each Direct3D device type
    for( UINT iDeviceType = 0; iDeviceType < devTypeArrayCount; iDeviceType++ )
    {
        BeDX11EnumDeviceInfo* pDeviceInfo = BeNew BeDX11EnumDeviceInfo;
        if( pDeviceInfo == NULL )
            return E_OUTOFMEMORY;

        // Fill struct w/ AdapterOrdinal and D3DX11_DRIVER_TYPE
        pDeviceInfo->m_uAdapterOrdinal = pAdapterInfo->m_uAdapterOrdinal;
        pDeviceInfo->m_kDeviceType = devTypeArray[iDeviceType];

        // Call D3D11CreateDevice to ensure that this is a D3D11 device.
        ID3D11Device* pd3dDevice = NULL;
        ID3D11DeviceContext* pd3dDeviceContext = NULL;
        IDXGIAdapter* pAdapter = NULL;
        if( devTypeArray[iDeviceType] == D3D_DRIVER_TYPE_HARDWARE )
            pAdapter = pAdapterInfo->m_pAdapter;
    
        HMODULE wrp = NULL;
        if (devTypeArray[iDeviceType] == D3D_DRIVER_TYPE_SOFTWARE) wrp = LoadLibrary(L"D3D11WARP.dll");

        hr = D3D11CreateDevice(pAdapter, devTypeArray[iDeviceType], (HMODULE)wrp, 0, NULL, 0, D3D11_SDK_VERSION, &pd3dDevice, NULL, &pd3dDeviceContext);
        if( FAILED( hr ) )
        {
            BeDelete pDeviceInfo;
            continue;
        }

        if( devTypeArray[iDeviceType] != D3D_DRIVER_TYPE_HARDWARE )
        {
            IDXGIDevice* pDXGIDev = NULL;
            hr = pd3dDevice->QueryInterface( __uuidof( IDXGIDevice ), ( LPVOID* )&pDXGIDev );
            if( SUCCEEDED( hr ) && pDXGIDev )
            {
                if ( pAdapterInfo->m_pAdapter )
                    pAdapterInfo->m_pAdapter->Release();

                pDXGIDev->GetAdapter( &pAdapterInfo->m_pAdapter );
            }
            if ( pDXGIDev )
                pDXGIDev->Release();
        }

        if (pd3dDeviceContext)
        {
            pd3dDeviceContext->ClearState();
            pd3dDeviceContext->Flush();
            pd3dDeviceContext->Release();
        }

        if (pd3dDevice)
        {
            BeInt numReferences = pd3dDevice->Release();
            if (numReferences > 0)
            {
                BE_RENDER_DEBUG("There were live instances in the device!");
            }
        }

        pAdapterInfo->m_kDeviceInfoList.push_back( pDeviceInfo );
    }

    return S_OK;
}
/////////////////////////////////////////////////////////////////////////////
BeInt BeRenderDeviceDX11Enumerator::EnumerateOutputs( BeDX11EnumAdapterInfo* pAdapterInfo )
{
    HRESULT hr;
    IDXGIOutput* pOutput;

    for( int iOutput = 0; ; ++iOutput )
    {
        pOutput = NULL;
        hr = pAdapterInfo->m_pAdapter->EnumOutputs( iOutput, &pOutput );
        if( DXGI_ERROR_NOT_FOUND == hr )
        {
            return S_OK;
        }
        else if( FAILED( hr ) )
        {
            return hr;  //Something bad happened.
        }
        else //Success!
        {
            BeDX11EnumOutputInfo* pOutputInfo = BeNew BeDX11EnumOutputInfo;
            pOutput->GetDesc( &pOutputInfo->m_kOutputDesc );
            pOutputInfo->m_uOutput = iOutput;
            pOutputInfo->m_pOutput = pOutput;

            EnumerateDisplayModes( pOutputInfo );
            if( pOutputInfo->m_kDisplayModeList.size() <= 0 )
            {
                // If this output has no valid display mode, do not save it.
                BeDelete pOutputInfo;
                continue;
            }

            pAdapterInfo->m_kOutputInfoList.push_back( pOutputInfo );
        }
    }
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeRenderDeviceDX11Enumerator::ExecuteEnumerationProcess(void)
{

  HRESULT hr;
  
  hr = CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)(&m_pFactory) );
  if( FAILED( hr ) )
        return BE_FALSE;

  IDXGIFactory* pFactory = static_cast<IDXGIFactory*>(m_pFactory);
  


  for ( BeUInt i = 0; i < m_kAdapterInfoList.size(); ++i )
  {
    BeDX11EnumAdapterInfo* pAdapterInfo = m_kAdapterInfoList.at(i);
    BeDelete pAdapterInfo;
    pAdapterInfo = NULL;
  }
  m_kAdapterInfoList.clear();



  for( BeInt index = 0; ; ++index )
    {
        IDXGIAdapter* pAdapter = NULL;
        hr = pFactory->EnumAdapters( index, &pAdapter );
        if( FAILED( hr ) )
            break;

    BeDX11EnumAdapterInfo* pAdapterInfo = BeNew BeDX11EnumAdapterInfo;
    pAdapterInfo->m_uAdapterOrdinal = index;
    pAdapter->GetDesc( &pAdapterInfo->m_kAdapterDesc );
    pAdapterInfo->m_pAdapter = pAdapter;

    // Enumerate the device driver types on the adapter.
    hr = EnumerateDevices( pAdapterInfo );
    if( FAILED( hr ) )
    {
      BeDelete pAdapterInfo;
      continue;
    }

    hr = EnumerateOutputs( pAdapterInfo );
    if( FAILED( hr ) || pAdapterInfo->m_kOutputInfoList.size() <= 0 )
    {
      BeDelete pAdapterInfo;
      continue;
    }

    // Get info for each devicecombo on this device
    hr = EnumerateDeviceCombos( pAdapterInfo );
    if( FAILED( hr ) )
    {
      BeDelete pAdapterInfo;
      continue;
    }

    m_kAdapterInfoList.push_back( pAdapterInfo );
  }
  
  //
    // Check for 2 or more adapters with the same name. Append the name
    // with some instance number if that's the case to help distinguish
    // them.
    //
    bool bUniqueDesc = BE_TRUE;
    BeDX11EnumAdapterInfo* pAdapterInfo;
    for( BeUInt i = 0; i < m_kAdapterInfoList.size(); i++ )
    {
        BeDX11EnumAdapterInfo* pAdapterInfo1 = m_kAdapterInfoList.at( i );
        for( BeUInt j = i + 1; j < m_kAdapterInfoList.size(); j++ )
        {
            BeDX11EnumAdapterInfo* pAdapterInfo2 = m_kAdapterInfoList.at( j );
            if( wcsncmp( pAdapterInfo1->m_kAdapterDesc.Description,
                         pAdapterInfo2->m_kAdapterDesc.Description, 128 ) == 0 )
            {
                bUniqueDesc = BE_FALSE;
                break;
            }
        }

        if( !bUniqueDesc )
            break;
    }

    for( BeUInt i = 0; i < m_kAdapterInfoList.size(); i++ )
    {
        pAdapterInfo = m_kAdapterInfoList.at( i );

    BeString16 strTemp(&pAdapterInfo->m_kAdapterDesc.Description[0]);
    pAdapterInfo->m_strUniqueDescription = strTemp;
    
        if( !bUniqueDesc )
        {
      pAdapterInfo->m_strUniqueDescription.Append( L" (#" );
      pAdapterInfo->m_strUniqueDescription.Append( ConvertString8To16( ParseIntToString8(pAdapterInfo->m_uAdapterOrdinal) ) );
      pAdapterInfo->m_strUniqueDescription.Append( L")" );
        }
    }

  return BE_TRUE;

}
/////////////////////////////////////////////////////////////////////////////
void BeRenderDeviceDX11Enumerator::FillSwapChainDesc( const BeRenderTypes::BeRenderDeviceDisplayMode& kDisplayMode, BeVoidP pSwapChainDesc )
{
  DXGI_SWAP_CHAIN_DESC* pDXGISwapChainDesc = reinterpret_cast<DXGI_SWAP_CHAIN_DESC*>(pSwapChainDesc);

  //set buffer dimensions and format
  pDXGISwapChainDesc->BufferCount = 2;
  pDXGISwapChainDesc->BufferDesc.Width = kDisplayMode.m_uResWidth;
  pDXGISwapChainDesc->BufferDesc.Height = kDisplayMode.m_uResHeight;
  pDXGISwapChainDesc->BufferDesc.Format = (DXGI_FORMAT)kDisplayMode.m_kFormat;
  pDXGISwapChainDesc->BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;

  //output window handle
  pDXGISwapChainDesc->OutputWindow = kDisplayMode.m_hWindowHandle;
  pDXGISwapChainDesc->Windowed = kDisplayMode.m_bWindowed;  
  pDXGISwapChainDesc->Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

  //set refresh rate
  ZeroMemory( &pDXGISwapChainDesc->BufferDesc.RefreshRate, sizeof( DXGI_RATIONAL ) );

  //scanline
  pDXGISwapChainDesc->BufferDesc.ScanlineOrdering = (DXGI_MODE_SCANLINE_ORDER)kDisplayMode.m_iScanlineOrdering;
  pDXGISwapChainDesc->BufferDesc.Scaling = (DXGI_MODE_SCALING)kDisplayMode.m_iScaling;

  //sampling settings
  if (m_pCurrentSelectedSettingsCombo != NULL)
  {
      pDXGISwapChainDesc->SampleDesc.Count = m_pCurrentSelectedSettingsCombo->m_kMultiSampleCountList[0];
      pDXGISwapChainDesc->SampleDesc.Quality = m_pCurrentSelectedSettingsCombo->m_kMultiSampleQualityList[0] - 1;
  }
  else
  {
      pDXGISwapChainDesc->SampleDesc.Count = 0;
      pDXGISwapChainDesc->SampleDesc.Quality = 0;
  }

  //swap effect
  pDXGISwapChainDesc->SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
}
/////////////////////////////////////////////////////////////////////////////
void BeRenderDeviceDX11Enumerator::FillDisplayModesArray( BeUInt uAdapterOrdinal, BeRenderTypes::BeDeviceAdapter& kDeviceAdapter )
{

  BeDX11EnumAdapterInfo* pAdapterInfo = m_kAdapterInfoList.at(uAdapterOrdinal);
  kDeviceAdapter.m_strAdapterName = pAdapterInfo->m_strUniqueDescription;

  BeInt kDeviceType = -1;
  DXGI_FORMAT kFormat = DXGI_FORMAT_UNKNOWN;
  for ( BeUInt i = 0; i < pAdapterInfo->m_kDeviceSettingsComboList.size(); ++i )
  {
    BeRenderDeviceDX11Enumerator::BeDX11EnumDeviceSettingsCombo* pSettingsCombo = pAdapterInfo->m_kDeviceSettingsComboList.at(i);
    if ( kDeviceType == -1 )
      kDeviceType = pSettingsCombo->m_kDeviceType;
    if ( kFormat == DXGI_FORMAT_UNKNOWN )
      kFormat = pSettingsCombo->m_kBackBufferFormat;

    if ( pSettingsCombo->m_kBackBufferFormat == kFormat && pSettingsCombo->m_kDeviceType == kDeviceType )
    {
      BeRenderDeviceDX11Enumerator::BeDX11EnumOutputInfo* pOutputInfo = pSettingsCombo->m_pOutputInfo;
      
      BeInt iLastWidth = -1;
      BeInt iLastHeight = -1;

      for ( BeUInt j = 0; j < pOutputInfo->m_kDisplayModeList.size(); ++j )
      {
        DXGI_MODE_DESC& kDisplayModeDesc = pOutputInfo->m_kDisplayModeList.at(j);
        if ( kDisplayModeDesc.Format == kFormat && (iLastWidth != (BeInt)kDisplayModeDesc.Width || iLastHeight != (BeInt)kDisplayModeDesc.Height) )
        {
          iLastWidth = kDisplayModeDesc.Width;
          iLastHeight = kDisplayModeDesc.Height;

          BeRenderTypes::BeRenderDeviceDisplayMode kDisplayMode;
          kDisplayMode.m_uResWidth = kDisplayModeDesc.Width;
          kDisplayMode.m_uResHeight = kDisplayModeDesc.Height;
          kDisplayMode.m_bWindowed = pSettingsCombo->m_bIsWindowed;
          kDisplayMode.m_iRefreshRate = (BeShort)kDisplayModeDesc.RefreshRate.Numerator;
          kDisplayMode.m_iScanlineOrdering = (BeShort)kDisplayModeDesc.ScanlineOrdering;
          kDisplayMode.m_iScaling = (BeShort)kDisplayModeDesc.Scaling;

          if ( pSettingsCombo->m_bIsWindowed )
            kDeviceAdapter.m_kEnumWindowedDisplayModes.push_back(kDisplayMode);
          else
            kDeviceAdapter.m_kEnumFullDisplayModes.push_back(kDisplayMode);
        }
      }
    }
  }
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeRenderDeviceDX11Enumerator::FindBestDisplayMode( BeUInt uAdapterOrdinal, const BeRenderTypes::BeRenderDeviceSettings& kRenderDeviceSettings, BeRenderTypes::BeRenderDeviceDisplayMode* pDisplayMode )
{

  BeDX11EnumAdapterInfo* pAdapterInfo = m_kAdapterInfoList.at(uAdapterOrdinal);

  for ( BeUInt i = 0; i < pAdapterInfo->m_kDeviceSettingsComboList.size(); ++i )
  {
    BeRenderDeviceDX11Enumerator::BeDX11EnumDeviceSettingsCombo* pSettingsCombo = pAdapterInfo->m_kDeviceSettingsComboList.at(i);
    if ( pSettingsCombo->m_bIsWindowed == kRenderDeviceSettings.m_bWindowed )
    {
      BeRenderDeviceDX11Enumerator::BeDX11EnumOutputInfo* pOutputInfo = pSettingsCombo->m_pOutputInfo;
      for ( BeUInt j = 0; j < pOutputInfo->m_kDisplayModeList.size(); ++j )
      {
        DXGI_MODE_DESC& kDisplayModeDesc = pOutputInfo->m_kDisplayModeList.at(j);
        if ( kDisplayModeDesc.Width == kRenderDeviceSettings.m_uBufferWidth && kDisplayModeDesc.Height == kRenderDeviceSettings.m_uBufferHeight )
        {
          m_pCurrentSelectedSettingsCombo = pSettingsCombo;
          pDisplayMode->m_uResWidth = kDisplayModeDesc.Width;
          pDisplayMode->m_uResHeight = kDisplayModeDesc.Height;
          pDisplayMode->m_kFormat = (BeInt)kDisplayModeDesc.Format;
          pDisplayMode->m_bWindowed = pSettingsCombo->m_bIsWindowed;
          pDisplayMode->m_iRefreshRate = (BeShort)kDisplayModeDesc.RefreshRate.Numerator;
          pDisplayMode->m_iScanlineOrdering = (BeShort)kDisplayModeDesc.ScanlineOrdering;
          pDisplayMode->m_iScaling = (BeShort)kDisplayModeDesc.Scaling;
          return BE_TRUE;
        }
      }
      BeInt iTemporarySettings = -1;
      for ( BeUInt j = 0; j < pOutputInfo->m_kDisplayModeList.size(); ++j )
      {
        DXGI_MODE_DESC& kDisplayModeDesc = pOutputInfo->m_kDisplayModeList.at(j);
        if ( kDisplayModeDesc.Width <= kRenderDeviceSettings.m_uBufferWidth && kDisplayModeDesc.Height <= kRenderDeviceSettings.m_uBufferHeight )
        {
          iTemporarySettings = j;
        }
        else if ( kDisplayModeDesc.Width > kRenderDeviceSettings.m_uBufferWidth || kDisplayModeDesc.Height > kRenderDeviceSettings.m_uBufferHeight )
        {
          if ( iTemporarySettings == -1 )
            iTemporarySettings = j;

          m_pCurrentSelectedSettingsCombo = pSettingsCombo;
          DXGI_MODE_DESC& kDisplayModeDescTmp = pOutputInfo->m_kDisplayModeList.at(iTemporarySettings);
          pDisplayMode->m_uResWidth = kDisplayModeDescTmp.Width;
          pDisplayMode->m_uResHeight = kDisplayModeDescTmp.Height;
          pDisplayMode->m_kFormat = (BeInt)kDisplayModeDescTmp.Format;
          pDisplayMode->m_bWindowed = pSettingsCombo->m_bIsWindowed;
          pDisplayMode->m_iRefreshRate = (BeShort)kDisplayModeDescTmp.RefreshRate.Numerator;
          pDisplayMode->m_iScanlineOrdering = (BeShort)kDisplayModeDescTmp.ScanlineOrdering;
          pDisplayMode->m_iScaling = (BeShort)kDisplayModeDescTmp.Scaling;
          return BE_TRUE;
        }
      }
    }
  }

  return BE_FALSE;
}
/////////////////////////////////////////////////////////////////////////////

