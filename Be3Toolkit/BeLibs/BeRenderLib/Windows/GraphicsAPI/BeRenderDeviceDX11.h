/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeRenderDeviceDX11.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BERENDERDEVICEDX11_H
#define BE_BERENDERDEVICEDX11_H

#if TARGET_WINDOWS

#include <BeRenderLib/Pch/BeRenderLibPredef.h>

#include <GraphicsAPI/BeRenderDeviceDX11Enumerator.h>
#include <Managers/BeMaterialsManager.h>
#include <Render/BeRenderDevice.h>

// Forward declarations
class BeVertexBuffer;
class BeIndexBuffer;
class BeConstantBuffer;

/////////////////////////////////////////////////
/// \class BeRenderDeviceDX11
/// \brief Dx11 rendering device
/////////////////////////////////////////////////
class RENDER_API BeRenderDeviceDX11 : public BeRenderDevice
{
      
  public:
    
    /// \brief Constructor
    BeRenderDeviceDX11();
    /// \brief Destructor
    ~BeRenderDeviceDX11();

    // Getters
    // {


    // }



    // Setters
    // {

        /// \brief  Sets the viewport
        virtual void SetViewport(const BeViewport& kViewport);

    // }



    // Methods
    // {

      /// \brief  Creates the render device
      virtual BeBoolean CreateRenderDevice(const BeRenderTypes::BeRenderDeviceSettings& kRenderDeviceSettings);

      /// \brief  Return the render device settings used when starting the app
      virtual const BeRenderTypes::BeRenderDeviceSettings& GetRenderDeviceSettings(void);

      /// \brief  Begins a scene
      virtual void BeginScene (const BeMatrix& kViewProjMatrix);

      /// \brief  Ends a scene
      virtual void EndScene (void);

      /// \brief  Clears current frame buffer
      virtual void Clear (const BeRenderTypes::EClearMask clearMask, const BeVector4D& clearColour, const BeFloat clearZValue, const BeInt clearStencilValue);

      /// \brief  Presents the current frame
      virtual BeBoolean Present(void);
      
      /// \brief  Converts a render format to API specific format
      static BeInt ConvertRenderFormatToAPIFormat(const BeRenderTypes::ERenderFormat eRenderFormat);

      /// \brief  Converts a render format to API specific format
      static BeInt ConvertDepthStencilFormatToAPIFormat(const BeRenderTypes::EDepthStencilFormat eDepthStencilFormat);

      /// \brief Returns the number of adapters
      static BeInt CheckNumAdapters(void);

      // \brief  Checks if a DX11 is available
      static BeBoolean CheckD3D11(BeUInt uAdapterOrdinal);

      // \brief  Enumerates all the available combinations of display modes and store them in m_pDeviceEnumerator
      static BeBoolean EnumerateDisplayModes(BeUInt uAdapterOrdinal, BeRenderTypes::BeDeviceAdapter& kDeviceAdapter);
          
    // }


      // Buffer Methods
      // {

          /// \brief  Creates a vertex array object (if the used API need it)
          virtual BeVertexArrayObject* CreateVertexArrayObject(void);

          /// \brief  Sets a previously created Vertex Array object for rendering
          virtual void SetVertexArrayObject(BeVertexArrayObject* pVertexArrayObject);

          /// \brief  Frees a previously created Vertex Array Object
          virtual void FreeVertexArrayObject(BeVertexArrayObject* pVertexArrayObject);

          /// \brief  Creates a vertex buffer with the incoming buffer
          virtual BeVertexBuffer* CreateVertexBuffer(BeVoidP pMeshVertices, BeSize_T iVertexStructSize, BeUInt uNumVertices, const BeString8& strVSProgram, BeVertexArrayObject* pVAO = NULL);

          /// \brief  Sets a previously created Vertex Buffer for rendering
          virtual void SetVertexBuffer(BeVertexBuffer* pVertexBuffer);

          /// \brief  Frees a previously created Vertex Buffer
          virtual void FreeVertexBuffer(BeVertexBuffer* pVertexBuffer);

          /// \brief  Creates an index buffer with the incoming buffer
          virtual BeIndexBuffer* CreateIndexBuffer(BeVoidP pMeshIndices, BeUInt uNumTriangles);

          /// \brief  Sets a previously created Index Buffer for rendering
          virtual void SetIndexBuffer(BeIndexBuffer* pIndexBuffer);

          /// \brief  Frees a previously created Index Buffer
          virtual void FreeIndexBuffer(BeIndexBuffer* pIndexBuffer);

          /// \brief  Creates a constant buffer with the incoming buffer
          virtual BeConstantBuffer* CreateConstantBuffer(BeVoidP pConstantBuffer, BeUInt uConstantBufferSize);

          /// \brief  Updates a constant buffer with the incoming buffer
          virtual void UpdateConstantBuffer(BeConstantBuffer* pConstantBuffer, BeVoidP pBuffer, BeUInt uBufferSize);

          /// \brief  Sets a previously created constant Buffer for rendering
          virtual void SetConstantBuffer(BeConstantBuffer* pConstantBuffer, BeRenderTypes::EBeMaterialPipeline eMaterialPipeline);

          /// \brief  Frees a previously created constant Buffer
          virtual void FreeConstantBuffer(BeConstantBuffer* pConstantBuffer);

          /// \brief  Renders an indexed primitive
          virtual void RenderIndexedPrimitive(BeVertexBuffer* pVB, BeIndexBuffer* pIB, BeVertexArrayObject* pVAO);

          /// \brief  Creates a vertex definition
          virtual BeMaterialVertexDefinition* CreateVertexDefinition(void);

          /// \brief  Creates the API related objects for a vertex definition previously created
          virtual BeVoidP ResolveVertexDefinition(BeMaterialVertexDefinition* pVertexDefinition, BeVoidP pInputData, BeMaterialProgram* pMaterialProgram);

          /// \brief  Creates the API related objects for a texture sampler previously created
          virtual BeVoidP ResolveTextureSampler(BeVoidP pInputData);

      // }



      // Render states Methods
      // {

          /// \brief  Creates a render state API object
          virtual BeVoidP CreateRenderState(BeRenderStates::ERenderStatesType eRSType, BeVoidP pData);

          /// \brief  Updates a render state API object
          virtual void UpdateRenderState(BeRenderStates::ERenderStatesType eRSType, BeVoidP pData);

      // }



      // Texture Methods
      // {

          /// \brief  Binds the texture to the selected channel
          virtual void BindTexture(BeInt iCacheHandle, BeTexture::ETextureChannel eChannel);

          /// \brief  Creates a texture sampler
          virtual BeTextureSampler* CreateTextureSampler(void);

          /// \brief  Sets a previously created texture sampler for rendering
          virtual void SetSampler(BeTextureSampler* pTextureSampler, BeInt iSlot, BeRenderTypes::EBeMaterialPipeline eMaterialPipeline);

      // }


      // Material Methods
      // {

            /// \brief  Sets the material for rendering
            virtual void SetMaterialForRendering (BeMaterial* pMaterial);

            /// \brief  Creates a new material program
            virtual BeMaterialProgram* CreateMaterialProgramFromBuffer(BeRenderTypes::EBeMaterialPipeline eProgramType, BeVoidP pBufferData, BeInt iBufferSize, BeString8 strFunction);

      // }



  private:

    /// \brief  Window process callback - Specific DX11 behaviour events
    static BeLResult CALLBACK WndProcDX11(BeWindowHandle hWnd, BeUInt uMessage, BeWParam wParam, BeLParam lParam);
        
    /// \brief  Creates the DX device
    BeBoolean CreateDXDevice(void);

    /// \brief  Creates a texture from buffer
    virtual BeBoolean CreateTextureFromBuffer(BeRenderTypes::BeTextureCacheEntry* pCacheTextureEntry, BeVoidP pData, BeInt iDataSize);

    /// \brief  Creates a material from buffer
    virtual BeBoolean CreateMaterialFromConfig(BeRenderTypes::BeMaterialCacheEntry* pCacheMaterialEntry, const BeRenderTypes::BeMaterialConfig& kMaterialConfig);

    /// \brief  Creates the render states
    virtual void CreateRenderStates(void);

    // Forward declarations
    class BeRenderDeviceDX11Impl;
    
    /// \brief  Render Device API Implementation
    static BeRenderDeviceDX11Impl* m_pRenderDeviceImpl;

    /// \brief  Device enumerator
    static BeRenderDeviceDX11Enumerator* m_pDeviceEnumerator;

};

#endif

#endif // #ifndef BE_BERENDERDEVICEDX11_H
