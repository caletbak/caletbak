/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeMaterialVertexDefinitionDX11.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <GraphicsAPI/BeMaterialVertexDefinitionDX11.h>
#include <Render/BeMaterialVertexAttribute.h>
#include <Render/BeRenderDevice.h>

// DirectX11 libs
#pragma comment (lib, "d3d11.lib") 

#include <d3d11.h>

/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
class RENDER_API BeMaterialVertexDefinitionDX11::BeVertexDefinitionDX11Impl
{

public:

    /// \brief  Constructor
    BeVertexDefinitionDX11Impl();
    /// \brief  Destructor
    ~BeVertexDefinitionDX11Impl();

    /// \brief  API Vertex definition object
    ID3D11InputLayout* m_pVertexDefinitionAPI;

};
/////////////////////////////////////////////////////////////////////////////
BeMaterialVertexDefinitionDX11::BeVertexDefinitionDX11Impl::BeVertexDefinitionDX11Impl()
:
    m_pVertexDefinitionAPI(NULL)
{
}
/////////////////////////////////////////////////////////////////////////////
BeMaterialVertexDefinitionDX11::BeVertexDefinitionDX11Impl::~BeVertexDefinitionDX11Impl()
{
    if (m_pVertexDefinitionAPI != NULL)
    {
        m_pVertexDefinitionAPI->Release();
        m_pVertexDefinitionAPI = NULL;
    }
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeMaterialVertexDefinitionDX11::BeMaterialVertexDefinitionDX11()
:
    m_pVertexDefinitionImpl(NULL)
{
    m_pVertexDefinitionImpl = BeNew BeVertexDefinitionDX11Impl();
}
/////////////////////////////////////////////////////////////////////////////
BeMaterialVertexDefinitionDX11::~BeMaterialVertexDefinitionDX11()
{
    if (m_pVertexDefinitionImpl != NULL)
    {
        BeDelete m_pVertexDefinitionImpl;
        m_pVertexDefinitionImpl = NULL;
    }
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeMaterialVertexDefinitionDX11::PrepareVertexDefinition(BeRenderDevice* pRenderDevice, BeMaterialProgram* pMaterialProgram)
{
    if (m_kAttributes.size() > 0)
    {
        D3D11_INPUT_ELEMENT_DESC* pInputElementDesc = BeNew D3D11_INPUT_ELEMENT_DESC[m_kAttributes.size()];
        std::string* pAttributeNames = BeNew std::string[m_kAttributes.size()];

        BeInt iCurrentIndex = 0;
        BeInt iCurrentOffset = 0;

        std::vector<BeMaterialVertexAttribute*>::iterator it = m_kAttributes.begin();
        while (it != m_kAttributes.end())
        {
            BeMaterialVertexAttribute* pAttribute = reinterpret_cast<BeMaterialVertexAttribute*>(*it);

            pAttributeNames[iCurrentIndex] = pAttribute->m_strSemantic.GetStringBase();

            pInputElementDesc[iCurrentIndex].SemanticName = pAttributeNames[iCurrentIndex].c_str();
            pInputElementDesc[iCurrentIndex].SemanticIndex = pAttribute->m_strSemanticIndex;
            pInputElementDesc[iCurrentIndex].Format = (DXGI_FORMAT)ConvertAttributeTypeToAPIFormat(pAttribute->m_eType);
            pInputElementDesc[iCurrentIndex].InputSlot = 0;
            pInputElementDesc[iCurrentIndex].AlignedByteOffset = iCurrentOffset;
            pInputElementDesc[iCurrentIndex].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
            pInputElementDesc[iCurrentIndex].InstanceDataStepRate = 0;

            iCurrentIndex++;
            iCurrentOffset += pAttribute->GetTypeSize();

            it++;
        }

        BeVoidP pInputLayout = pRenderDevice->ResolveVertexDefinition(this, pInputElementDesc, pMaterialProgram);

        m_pVertexDefinitionImpl->m_pVertexDefinitionAPI = reinterpret_cast<ID3D11InputLayout*>(pInputLayout);

        BeDeleteArray pAttributeNames;
        BeDeleteArray pInputElementDesc;
    }
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeVoidP BeMaterialVertexDefinitionDX11::GetVertexDefinitionPtr(void)
{
    return m_pVertexDefinitionImpl->m_pVertexDefinitionAPI;
}
/////////////////////////////////////////////////////////////////////////////
void BeMaterialVertexDefinitionDX11::SetVertexDefinitionPtr(BeVoidP pVertexDefinitionPtr)
{
    ID3D11InputLayout* pVertexDefinitionAPI = reinterpret_cast<ID3D11InputLayout*>(pVertexDefinitionPtr);

    m_pVertexDefinitionImpl->m_pVertexDefinitionAPI = pVertexDefinitionAPI;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeInt BeMaterialVertexDefinitionDX11::ConvertAttributeTypeToAPIFormat(BeMaterialVertexAttribute::eBeMaterialAttributeType eType)
{
    switch (eType)
    {
        case BeMaterialVertexAttribute::ATTRIBUTE_FLOAT:
        {
            return (BeInt) DXGI_FORMAT_R32_FLOAT;
            break;
        }
        case BeMaterialVertexAttribute::ATTRIBUTE_FLOAT2:
        {
            return (BeInt)DXGI_FORMAT_R32G32_FLOAT;
            break;
        }
        case BeMaterialVertexAttribute::ATTRIBUTE_FLOAT3:
        {
            return (BeInt)DXGI_FORMAT_R32G32B32_FLOAT;
            break;
        }
        case BeMaterialVertexAttribute::ATTRIBUTE_FLOAT4:
        {
            return (BeInt)DXGI_FORMAT_R32G32B32A32_FLOAT;
            break;
        }
        case BeMaterialVertexAttribute::ATTRIBUTE_MATRIX:
        {
            return (BeInt)DXGI_FORMAT_R32G32B32A32_FLOAT;
            break;
        }
    }

    return 0;
}
/////////////////////////////////////////////////////////////////////////////

