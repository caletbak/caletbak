/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeRenderStatesDX11.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BERENDERSTATESDX11_H
#define BE_BERENDERSTATESDX11_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>
#include <Render/BeRenderStates.h>

/////////////////////////////////////////////////
/// \class BeRenderStatesDX11
/// \brief Class for updating the Render states
/////////////////////////////////////////////////
class RENDER_API BeRenderStatesDX11 : public BeRenderStates
{

    public:

        /// \brief Constructor
        BeRenderStatesDX11(BeRenderDevice* pRenderDevice);
        /// \brief Destructor
        ~BeRenderStatesDX11();

        // Getters
        // {

            /// \brief  Returns the direct pointer to the render states API impl
            virtual BeVoidP GetRenderStatesPtr(ERenderStatesType eRSType);

        // }



        // Setters
        // {

            /// \brief  Sets the direct pointer to the render states API impl
            virtual void SetRenderStatesPtr(BeVoidP pRSPtr, ERenderStatesType eRSType);

        // }



        // Methods
        // {

            /// \brief  Updates the material and constants all together
            virtual void UpdateRenderStates(void);

        // }

    private:

        // Forward declarations
        class BeRenderStatesDX11Impl;

        /// \brief  API Implementation
        BeRenderStatesDX11Impl* m_pRenderStatesImpl;

};

#endif // #ifndef BE_BERENDERSTATESDX11_H
