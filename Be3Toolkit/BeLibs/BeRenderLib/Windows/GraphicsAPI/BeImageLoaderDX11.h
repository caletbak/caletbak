/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeImageLoaderDX11.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEIMAGELOADERDX11_H
#define BE_BEIMAGELOADERDX11_H

#include <BeRenderLib/Pch/BeRenderLibPredef.h>
#include <IO/BeImageLoader.h>

/////////////////////////////////////////////////
/// \class  BeImageLoaderDX11
/// \brief  Image loader helper DX11 API
/////////////////////////////////////////////////
class RENDER_API BeImageLoaderDX11 : public BeImageLoader
{
    public:

        /// \brief  Constructor
        BeImageLoaderDX11();
        /// \brief  Destructor
        ~BeImageLoaderDX11();



        // \brief  Methods
        // {

            /// \brief  Loads an image if possible from a buffer
            virtual BeVoidP LoadFileImage(BeVoidP pData, BeInt iDataSize);

            /// \brief  Generates the pixel array from a previous loaded image
            virtual BeVector4D* GeneratePixelArray(void);

            /// \brief  Releases the current loaded API image
            virtual void Release(void);

        // }



        // \brief  Getters
        // {

            /// \brief  Returns the image width after being loaded, -1 otherwise
            virtual BeInt GetImageWidth(void);

            /// \brief  Returns the image height after being loaded, -1 otherwise
            virtual BeInt GetImageHeight(void);

        // }

};

#endif // #ifndef BE_BEIMAGELOADERDX11_H
