/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeFontLibPredef.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEFONTLIBPREDEF_H
#define BE_BEFONTLIBPREDEF_H

// Global BE3 include
#include <BeCommon/BakEngine3Common.h>

#if TARGET_WINDOWS
#ifdef FONT_IMPORTS
#  define FONT_API __declspec(dllimport)
#  pragma message("automatic link to BeFontLib.lib")
#  pragma comment(lib, "BeFontLib.lib")
#else
#  define FONT_API __declspec(dllexport)
#endif
#else
#  define FONT_API
#endif

#ifdef _DEBUG
#  define BE_FONT_DEBUG(x, ...) _OutputDebugString("[  FONT DEBUG]: ", true, x, ##__VA_ARGS__)
#  define BE_FONT_WARNING(x, ...) _OutputDebugString("[! FONT WARNING]: ", true, x, ##__VA_ARGS__)
#  define BE_FONT_ERROR(x, ...) _OutputDebugString("[* FONT ERROR]: ", true, x, ##__VA_ARGS__)
#  define BE_FONT_PRINT(x, ...) _OutputDebugString("", false, x, ##__VA_ARGS__)
#else
#  define BE_FONT_DEBUG(x, ...) x
#  define BE_FONT_WARNING(x, ...) x
#  define BE_FONT_ERROR(x, ...) x
#  define BE_FONT_PRINT(x, ...) x
#endif

#if defined(AS400) || defined(OS400)
  typedef pthread_id_np_t ThreadID;
#elif defined(VMS) 
  typedef pthread_t ThreadID;
#else
# ifdef USE_BEGIN_THREAD
  typedef BeUInt ThreadID;
# else
  typedef BeULong ThreadID;
# endif
#endif

#if (defined _MSC_VER)
#pragma warning(disable: 4018)
#pragma warning(disable: 4028)
#pragma warning(disable: 4100)
#pragma warning(disable: 4201)
#pragma warning(disable: 4204)
#pragma warning(disable: 4244)
#pragma warning(disable: 4245)
#pragma warning(disable: 4273)
#pragma warning(disable: 4309)
#pragma warning(disable: 4389)
#pragma warning(disable: 4701)
#pragma warning(disable: 4702)
#pragma warning(disable: 4703)
#pragma warning(disable: 4996)
#endif

// External includes
#include <BeCoreLib/Pch/BeCoreLib.h>
#include <BeMathLib/Pch/BeMathLib.h>
#include <BeInputLib/Pch/BeInputLib.h>
#include <BeUtilityLib/Pch/BeUtilityLib.h>
#include <BeRenderLib/Pch/BeRenderLib.h>
#include <BeFontLib/Pch/BeFontLib.h>

#endif // #ifndef BE_BEFONTLIBPREDEF_H

