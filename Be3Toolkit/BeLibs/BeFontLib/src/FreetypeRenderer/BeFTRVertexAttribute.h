/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeFTRVertexAttribute.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_FTRVERTEXATTRIBUTE_H
#define BE_FTRVERTEXATTRIBUTE_H

#include <BeFontLib/pch/BeFontLibPredef.h>

#include <FreetypeRenderer/BeFTRVector.h>

// Maximum number of attributes per vertex
#define MAX_VERTEX_ATTRIBUTE    16

class FONT_API BeFTRVertexAttribute
{
public:

    /// \brief  Generic vertex attribute.
    struct BeFTRVertexAttributeData
    {
        /// \brief  atribute name
        BeChar8* m_pName;

        /// \brief  index of the generic vertex attribute to be modified
        BeInt m_iIndex;

        /// \brief  Number of components per generic vertex attribute
        BeInt m_iSize;

        /// \brief  data type of each component in the array
        BeInt m_iType;

        /// \brief  whether fixed-point data values should be normalized (GL_TRUE) or converted directly as fixed - point values(GL_FALSE) when they are accessed
        BeBoolean m_bNormalized;

        /// \brief  byte offset between consecutive generic vertex attributes.
        BeInt m_iStride;

        /// \brief  pointer to the first component of the first attribute element in the array
        BeVoidP m_pPointer;

        /// \brief  pointer to the function that enable this attribute
        void(*enable)(BeVoidP);
    };



    /// \brief  Create an attribute from the given parameters
    static BeFTRVertexAttributeData* VertexAttributeNew(BeChar8* pName, BeInt iSize, BeInt iType, BeBoolean bNormalized, BeInt iStride, BeVoidP pPointer);

    /// \brief  Delete a vertex attribute
    static void VertexAttributeDelete(BeFTRVertexAttributeData* pSelf);

    /// \brief  Create an attribute from the given description
    static BeFTRVertexAttributeData* VertexAttributeParse(BeChar8* pFormat);



    /// \brief  Enable a vertex attribute
    static void VertexAttributeEnable(BeFTRVertexAttributeData* pAttr);

};

#endif // BE_FTRVERTEXATTRIBUTE_H
