/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeFTRFontManager.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <BeFontLib/pch/BeFontLibPredef.h>

#include <FreetypeRenderer/BeFTRFontManager.h>

/////////////////////////////////////////////////////////////////////////////
BeFTRFontManager::BeFTRFontManagerData* BeFTRFontManager::FontManagerNew(BeSize_T iWidth, BeSize_T iHeight, BeSize_T iDepth)
{
    BeFTRFontManager::BeFTRFontManagerData* pSelf;

    BeFTRTextureAtlas::BeFTRTextureAtlasData* pAtlas = BeFTRTextureAtlas::TextureAtlasNew(iWidth, iHeight, iDepth);

    pSelf = (BeFTRFontManager::BeFTRFontManagerData*)malloc(sizeof(BeFTRFontManager::BeFTRFontManagerData));

    if (!pSelf)
    {
		BE_FONT_ERROR("line %d: No more memory for allocating data\n", __LINE__ );
        exit( EXIT_FAILURE );
    }

    pSelf->m_pAtlas = pAtlas;
    pSelf->m_pFonts = BeFTRVector::VectorNew(sizeof(BeFTRTextureFont::BeFTRTextureFontData*));
    pSelf->m_pCache = wcsdup(L" ");

    return pSelf;
}
/////////////////////////////////////////////////////////////////////////////
void BeFTRFontManager::FontManagerDelete(BeFTRFontManager::BeFTRFontManagerData* pSelf)
{
    BeSize_T i;
    BeFTRTextureFont::BeFTRTextureFontData* pFont;

    BE_ASSERT(pSelf);

    for (i = 0;  i <BeFTRVector::VectorSize(pSelf->m_pFonts); ++i)
    {
        pFont = *(BeFTRTextureFont::BeFTRTextureFontData**)BeFTRVector::VectorGet(pSelf->m_pFonts, i);
        BeFTRTextureFont::TextureFontDelete(pFont);
    }

    BeFTRVector::VectorDelete(pSelf->m_pFonts);
    BeFTRTextureAtlas::TextureAtlasDelete(pSelf->m_pAtlas);

    if (pSelf->m_pCache)
    {
        free(pSelf->m_pCache);
    }

    free(pSelf);
}
/////////////////////////////////////////////////////////////////////////////
void BeFTRFontManager::FontManagerDeleteFont(BeFTRFontManager::BeFTRFontManagerData* pSelf, BeFTRTextureFont::BeFTRTextureFontData* pFont)
{
    BeSize_T i;
    BeFTRTextureFont::BeFTRTextureFontData* pOther;

    BE_ASSERT(pSelf);
    BE_ASSERT(pFont);
    
    for (i = 0; i < pSelf->m_pFonts->m_iSize; ++i)
    {
        pOther = (BeFTRTextureFont::BeFTRTextureFontData*)BeFTRVector::VectorGet(pSelf->m_pFonts, i);
        if ((strcmp(pFont->m_pFilename, pOther->m_pFilename) == 0) && (pFont->m_fSize == pOther->m_fSize))
        {
            BeFTRVector::VectorErase(pSelf->m_pFonts, i);
            break;
        }
    }

    BeFTRTextureFont::TextureFontDelete(pFont);
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeFTRTextureFont::BeFTRTextureFontData* BeFTRFontManager::FontManagerGetFromFilename(BeFTRFontManagerData* pSelf, const BeChar8* pFilename, const BeFloat fSize)
{
    BeSize_T i;
    BeFTRTextureFont::BeFTRTextureFontData* pFont;

    BE_ASSERT(pSelf);
    for (i = 0; i<BeFTRVector::VectorSize(pSelf->m_pFonts); ++i)
    {
        pFont = *(BeFTRTextureFont::BeFTRTextureFontData**)BeFTRVector::VectorGet(pSelf->m_pFonts, i);
        if ((strcmp(pFont->m_pFilename, pFilename) == 0) && (pFont->m_fSize == fSize))
        {
            return pFont;
        }
    }

    pFont = BeFTRTextureFont::TextureFontNew(pSelf->m_pAtlas, pFilename, fSize, 0.0f);
    if (pFont)
    {
        BeFTRVector::VectorPushBack(pSelf->m_pFonts, &pFont);

        BeFTRTextureFont::TextureFontLoadGlyphs(pFont, pSelf->m_pCache);

        return pFont;
    }

    BE_FONT_ERROR("Unable to load \"%s\" (size=%.1f)\n", pFilename, fSize);

    return 0;
}
/////////////////////////////////////////////////////////////////////////////
BeFTRTextureFont::BeFTRTextureFontData* BeFTRFontManager::FontManagerGetFromDescription(BeFTRFontManagerData* pSelf, const BeChar8* pFamily, const BeFloat fSize, const BeInt iBold, const BeInt iItalic)
{
    BeFTRTextureFont::BeFTRTextureFontData* pFont;
    BeChar8* pFilename = 0;

    BE_ASSERT(pSelf);

    BeString8 strFilename = pFamily;

    if (BeFileTool::FileExists(strFilename))
    {
        pFilename = strdup(pFamily);
    }
    else
    {
#if defined(_WIN32) || defined(_WIN64)
        BE_FONT_ERROR("\"FontManagerGetFromDescription\" not implemented yet.\n");
        return 0;
#endif
        pFilename = FontManagerMatchDescription(pSelf, pFamily, fSize, iBold, iItalic);
        if (!pFilename)
        {
            BE_FONT_ERROR("No \"%s (size=%.1f, bold=%d, italic=%d)\" font available.\n", pFamily, fSize, iBold, iItalic);
            return 0;
        }
    }

    pFont = FontManagerGetFromFilename(pSelf, pFilename, fSize);

    free(pFilename);

    return pFont;
}
/////////////////////////////////////////////////////////////////////////////
BeFTRTextureFont::BeFTRTextureFontData* BeFTRFontManager::FontManagerGetFromMarkup(BeFTRFontManagerData* pSelf, const BeFTRMarkup::BeFTRMarkupData* pMarkup)
{
    BE_ASSERT(pSelf);
    BE_ASSERT(pMarkup);

    return FontManagerGetFromDescription(pSelf, pMarkup->m_pFamily, pMarkup->m_fSize, pMarkup->m_iBold, pMarkup->m_iItalic);
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeChar8* BeFTRFontManager::FontManagerMatchDescription(BeFTRFontManagerData* pSelf, const BeChar8* pFamily, const BeFloat fSize, const BeInt iBold, const BeInt iItalic)
{
    // Use of fontconfig is disabled by default.
    return 0;
}
/////////////////////////////////////////////////////////////////////////////

