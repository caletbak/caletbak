/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeFTRRenderer.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_FTRRENDERER_H
#define BE_FTRRENDERER_H

#include <FreetypeRenderer/BeFTRVector.h>
#include <FreetypeRenderer/BeFTRTextureAtlas.h>
#include <FreetypeRenderer/BeFTRTextureFont.h>

#endif // BE_FTRRENDERER_H
