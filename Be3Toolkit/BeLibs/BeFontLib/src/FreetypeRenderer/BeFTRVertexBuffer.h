/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeFTRVertexBuffer.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_FTRVERTEXBUFFER_H
#define BE_FTRVERTEXBUFFER_H

#include <BeFontLib/pch/BeFontLibPredef.h>

#include <FreetypeRenderer/BeFTRVector.h>
#include <FreetypeRenderer/BeFTRVertexAttribute.h>

class FONT_API BeFTRVertexBuffer
{
public:

    /// \brief  Generic vertex buffer
    struct BeFTRVertexBufferData
    {
        /// \brief  Format of the vertex buffer
        BeChar8* m_pFormat;

        /// \brief  Vector of vertices
        BeFTRVector::BeFTRVectorData* m_pVertices;

        /// \brief  GL identity of the vertices buffer
        //GLuint m_vertices_id;

        /// \brief  Vector of indices
        BeFTRVector::BeFTRVectorData* m_pIndices;

        /// \brief  GL identity of the indices buffer
        //GLushort m_indices_id;

        /// \brief  Current size of the vertices buffer in GPU
        //size_t m_GPU_vsize;

        /// \brief  Current size of the indices buffer in GPU
        //size_t m_GPU_isize;

        /// \brief  GL primitives to render
        //GLenum m_mode;

        /// \brief  Whether the vertex buffer needs to be uploaded to GPU memory
        BeChar8 m_cState;

        /// \brief  Individual items
        BeFTRVector::BeFTRVectorData* m_pItems;

        /// \brief  Array of attributes
        BeFTRVertexAttribute::BeFTRVertexAttributeData* m_kAttributes[MAX_VERTEX_ATTRIBUTE];
    };



    /// \brief  Creates an empty vertex buffer
    static BeFTRVertexBufferData* VertexBufferNew(const BeChar8* pFormat);

    /// \brief  Deletes vertex buffer and releases GPU memory
    static void VertexBufferDelete(BeFTRVertexBufferData* pSelf);



    /// \brief  Returns the number of items in the vertex buffer
    static BeSize_T VertexBufferSize(const BeFTRVertexBufferData* pSelf);

    /// \brief  Returns vertex format
    static const BeChar8* VertexBufferFormat(const BeFTRVertexBufferData* pSelf);



    /// \brief  Print information about a vertex buffer
    static void VertexBufferPrint(BeFTRVertexBufferData * pSelf);



    /// \brief  Prepare vertex buffer for render
    static void VertexBufferRenderSetup(BeFTRVertexBufferData* pSelf, BeInt iMode);

    /// \brief  Finish rendering by setting back modified states
    static void VertexBufferRenderFinish(BeFTRVertexBufferData* pSelf);

    /// \brief  Render vertex buffer
    static void VertexBufferRender(BeFTRVertexBufferData* pSelf, BeInt iMode);

    /// \brief  Render a specified item from the vertex buffer
    static void VertexBufferRenderItem(BeFTRVertexBufferData* pSelf, BeSize_T iIndex);

    /// \brief  Upload buffer to GPU memory
    static void VertexBufferUpload(BeFTRVertexBufferData* pSelf);

    /// \brief  Clear all items
    static void VertexBufferClear(BeFTRVertexBufferData* pSelf);



    /// \brief  Appends vertices at the end of the buffer
    static void VertexBufferPushBackVertices(BeFTRVertexBufferData* pSelf, const BeVoidP pVertices, const BeSize_T iVCount);

    /// \brief  Insert vertices in the buffer
    static void VertexBufferInsertVertices(BeFTRVertexBufferData* pSelf, const BeSize_T iIndex, const BeVoidP pVertices, const BeSize_T iVCount);

    /// \brief  Erase vertices in the buffer
    static void VertexBufferEraseVertices(BeFTRVertexBufferData* pSelf, const BeSize_T iFirst, const BeSize_T iLast);



    /// \brief  Appends indices at the end of the buffer
    static void VertexBufferPushBackIndices(BeFTRVertexBufferData* pSelf, const BeUShort* pIndices, const BeSize_T iIcount);

    /// \brief  Insert indices in the buffer
    static void VertexBufferInsertIndices(BeFTRVertexBufferData* pSelf, const BeSize_T iIndex, const BeUShort* pIndices, const BeSize_T iICount);

    /// \brief  Erase indices in the buffer
    static void VertexBufferEraseIndices(BeFTRVertexBufferData* pSelf, const BeSize_T iFirst, const BeSize_T iLast);



    /// \brief  Append a new item to the collection
    static BeSize_T VertexBufferPushBack(BeFTRVertexBufferData* pSelf, const BeVoidP pVertices, const BeSize_T iVCount, const BeUShort* pIndices, const BeSize_T iICount);

    /// \brief  Insert a new item into the vertex buffer
    static BeSize_T VertexBufferInsert(BeFTRVertexBufferData* pSelf, BeSize_T iIndex, const BeVoidP pVertices, const BeSize_T iVCount, const BeUShort* pIndices, const BeSize_T iICount);

    /// \brief  Erase an item from the vertex buffer
    static void VertexBufferErase(BeFTRVertexBufferData* pSelf, const BeSize_T iIndex);

};

#endif // BE_FTRVERTEXBUFFER_H
