/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeFTRImageEDT.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <BeFontLib/pch/BeFontLibPredef.h>

#include <FreetypeRenderer/BeFTRImageEDT.h>

/////////////////////////////////////////////////////////////////////////////
// Shorthand macro: add ubiquitous parameters dist, pGX, pGY, pImage and iW and call Distaa3()
#define DISTAA(c,xc,yc,xi,yi) (BeFTRImageEDT::Distaa3(pImage, pGX, pGY, iW, c, xc, yc, xi, yi))
#define SQRT2 1.4142136
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeFTRImageEDT::ComputeGradient(BeDouble* pImage, BeInt iW, BeInt iH, BeDouble* pGX, BeDouble *pGY)
{
    BeDouble fGLength;

    for (BeInt i = 1; i < iH - 1; i++)
    {
        for (BeInt j = 1; j < iW - 1; j++)
        {
            BeInt k = i * iW + j;

            if ((pImage[k]>0.0) && (pImage[k]<1.0))
            {
                pGX[k] = -pImage[k - iW - 1] - SQRT2 * pImage[k - 1] - pImage[k + iW - 1] + pImage[k - iW + 1] + SQRT2 * pImage[k + 1] + pImage[k + iW + 1];
                pGY[k] = -pImage[k - iW - 1] - SQRT2 * pImage[k - iW] - pImage[k + iW - 1] + pImage[k - iW + 1] + SQRT2 * pImage[k + iW] + pImage[k + iW + 1];

                fGLength = pGX[k] * pGX[k] + pGY[k] * pGY[k];

                if (fGLength > 0.0)
                {
                    fGLength = sqrt(fGLength);
                    pGX[k] = pGX[k] / fGLength;
                    pGY[k] = pGY[k] / fGLength;
                }
            }
        }
    }
}
/////////////////////////////////////////////////////////////////////////////
BeDouble BeFTRImageEDT::EdgedF(BeDouble fGX, BeDouble fGY, BeDouble fA)
{
    BeDouble fDF, fGLength, fTemp, fA1;

    if ((fGX == 0) || (fGY == 0))
    {
        fDF = 0.5 - fA;
    }
    else
    {
        fGLength = sqrt(fGX*fGX + fGY*fGY);
        if (fGLength>0)
        {
            fGX = fGX / fGLength;
            fGY = fGY / fGLength;
        }
        
        fGX = fabs(fGX);
        fGY = fabs(fGY);

        if (fGX < fGY)
        {
            fTemp = fGX;
            fGX = fGY;
            fGY = fTemp;
        }

        fA1 = 0.5 * fGY / fGX;

        if (fA < fA1)
        {
            fDF = 0.5*(fGX + fGY) - sqrt(2.0*fGX*fGY*fA);
        }
        else if (fA < (1.0 - fA1))
        {
            fDF = (0.5 - fA)*fGX;
        }
        else
        {
            fDF = -0.5*(fGX + fGY) + sqrt(2.0*fGX*fGY*(1.0 - fA));
        }
    }

    return fDF;
}
/////////////////////////////////////////////////////////////////////////////
BeDouble BeFTRImageEDT::Distaa3(BeDouble* pImage, BeDouble* pGXImage, BeDouble* pGYImage, BeInt iW, BeInt iC, BeInt iXC, BeInt iYC, BeInt iXI, BeInt iYI)
{
    BeDouble fDI, fDF, fDX, fDY, fGX, fGY, fA;
    BeInt iClosest;

    iClosest = iC - iXC - iYC*iW;
    fA = pImage[iClosest];
    fGX = pGXImage[iClosest];
    fGY = pGYImage[iClosest];

    if (fA > 1.0) fA = 1.0;
    if (fA < 0.0) fA = 0.0;
    if (fA == 0.0) return 1000000.0;

    fDX = (BeDouble)iXI;
    fDY = (BeDouble)iYI;
    fDI = sqrt(fDX*fDX + fDY*fDY);

    if (fDI == 0)
    {
        fDF = EdgedF(fGX, fGY, fA);
    }
    else
    {
        fDF = EdgedF(fDX, fDY, fA);
    }

    return fDI + fDF;
}
/////////////////////////////////////////////////////////////////////////////
void BeFTRImageEDT::Edtaa3(BeDouble* pImage, BeDouble *pGX, BeDouble* pGY, BeInt iW, BeInt iH, BeShort* pDistX, BeShort* pDistY, BeDouble* pDist)
{
    BeInt x, y, i, c;
    BeInt offset_u, offset_ur, offset_r, offset_rd, offset_d, offset_dl, offset_l, offset_lu;
    BeDouble olddist, newdist;
    BeInt cdistx, cdisty, newdistx, newdisty;
    BeInt changed;
    BeDouble epsilon = 1e-3;

    offset_u = -iW;
    offset_ur = -iW + 1;
    offset_r = 1;
    offset_rd = iW + 1;
    offset_d = iW;
    offset_dl = iW - 1;
    offset_l = -1;
    offset_lu = -iW - 1;

    for (i = 0; i < iW * iH; i++)
    {
        pDistX[i] = 0;
        pDistY[i] = 0;

        if (pImage[i] <= 0.0)
        {
            pDist[i] = 1000000.0;
        }
        else if (pImage[i]<1.0)
        {
            pDist[i] = EdgedF(pGX[i], pGY[i], pImage[i]);
        }
        else
        {
            pDist[i] = 0.0;
        }
    }

    do
    {
        changed = 0;

        for(y = 1; y < iH; y++)
        {
            i = y * iW;

            olddist = pDist[i];
            if(olddist > 0)
            {
                c = i + offset_u;
                cdistx = pDistX[c];
                cdisty = pDistY[c];
                newdistx = cdistx;
                newdisty = cdisty+1;
                newdist = DISTAA(c, cdistx, cdisty, newdistx, newdisty);

                if(newdist < olddist-epsilon)
                {
                    pDistX[i] = newdistx;
                    pDistY[i] = newdisty;
                    pDist[i] = newdist;
                    olddist=newdist;
                    changed = 1;
                }

                c = i+offset_ur;
                cdistx = pDistX[c];
                cdisty = pDistY[c];
                newdistx = cdistx-1;
                newdisty = cdisty+1;
                newdist = DISTAA(c, cdistx, cdisty, newdistx, newdisty);
                if(newdist < olddist-epsilon)
                {
                    pDistX[i] = newdistx;
                    pDistY[i] = newdisty;
                    pDist[i] = newdist;
                    changed = 1;
                }
            }
            i++;

            for(x = 1; x < iW-1; x++, i++)
            {
                olddist = pDist[i];
                if(olddist <= 0) continue;

                c = i+offset_l;
                cdistx = pDistX[c];
                cdisty = pDistY[c];

                newdistx = cdistx+1;
                newdisty = cdisty;
                newdist = DISTAA(c, cdistx, cdisty, newdistx, newdisty);
                if(newdist < olddist-epsilon)
                {
                    pDistX[i] = newdistx;
                    pDistY[i] = newdisty;
                    pDist[i] = newdist;
                    olddist=newdist;
                    changed = 1;
                }

                c = i+offset_lu;
                cdistx = pDistX[c];
                cdisty = pDistY[c];

                newdistx = cdistx+1;
                newdisty = cdisty+1;
                newdist = DISTAA(c, cdistx, cdisty, newdistx, newdisty);
                if(newdist < olddist-epsilon)
                {
                    pDistX[i] = newdistx;
                    pDistY[i] = newdisty;
                    pDist[i] = newdist;
                    olddist=newdist;
                    changed = 1;
                }

                c = i+offset_u;
                cdistx = pDistX[c];
                cdisty = pDistY[c];

                newdistx = cdistx;
                newdisty = cdisty+1;
                newdist = DISTAA(c, cdistx, cdisty, newdistx, newdisty);
                if(newdist < olddist-epsilon)
                {
                    pDistX[i] = newdistx;
                    pDistY[i] = newdisty;
                    pDist[i] = newdist;
                    olddist=newdist;
                    changed = 1;
                }

                c = i+offset_ur;
                cdistx = pDistX[c];
                cdisty = pDistY[c];

                newdistx = cdistx-1;
                newdisty = cdisty+1;
                newdist = DISTAA(c, cdistx, cdisty, newdistx, newdisty);
                if(newdist < olddist-epsilon)
                {
                    pDistX[i] = newdistx;
                    pDistY[i] = newdisty;
                    pDist[i] = newdist;
                    changed = 1;
                }
            }

            olddist = pDist[i];
            if(olddist > 0)
            {
                c = i+offset_l;
                cdistx = pDistX[c];
                cdisty = pDistY[c];

                newdistx = cdistx+1;
                newdisty = cdisty;
                newdist = DISTAA(c, cdistx, cdisty, newdistx, newdisty);
                if(newdist < olddist-epsilon)
                {
                    pDistX[i] = newdistx;
                    pDistY[i] = newdisty;
                    pDist[i] = newdist;
                    olddist=newdist;
                    changed = 1;
                }

                c = i+offset_lu;
                cdistx = pDistX[c];
                cdisty = pDistY[c];

                newdistx = cdistx+1;
                newdisty = cdisty+1;
                newdist = DISTAA(c, cdistx, cdisty, newdistx, newdisty);
                if(newdist < olddist-epsilon)
                {
                    pDistX[i] = newdistx;
                    pDistY[i] = newdisty;
                    pDist[i] = newdist;
                    olddist=newdist;
                    changed = 1;
                }

                c = i+offset_u;
                cdistx = pDistX[c];
                cdisty = pDistY[c];

                newdistx = cdistx;
                newdisty = cdisty+1;
                newdist = DISTAA(c, cdistx, cdisty, newdistx, newdisty);
                if(newdist < olddist-epsilon)
                {
                    pDistX[i] = newdistx;
                    pDistY[i] = newdisty;
                    pDist[i] = newdist;
                    changed = 1;
                }
            }

            i = y * iW + iW - 2;

            for (x = iW - 2; x >= 0; x--, i--)
            {
                olddist = pDist[i];
                if(olddist <= 0) continue;

                c = i+offset_r;
                cdistx = pDistX[c];
                cdisty = pDistY[c];

                newdistx = cdistx-1;
                newdisty = cdisty;
                newdist = DISTAA(c, cdistx, cdisty, newdistx, newdisty);
                if(newdist < olddist-epsilon)
                {
                    pDistX[i] = newdistx;
                    pDistY[i] = newdisty;
                    pDist[i] = newdist;
                    changed = 1;
                }
            }
        }
      
        for(y = iH-2; y >= 0; y--)
        {
            i = y * iW + iW - 1;

            olddist = pDist[i];
            if(olddist > 0)
            {
                c = i+offset_d;
                cdistx = pDistX[c];
                cdisty = pDistY[c];

                newdistx = cdistx;
                newdisty = cdisty-1;
                newdist = DISTAA(c, cdistx, cdisty, newdistx, newdisty);
                if(newdist < olddist-epsilon)
                {
                    pDistX[i] = newdistx;
                    pDistY[i] = newdisty;
                    pDist[i] = newdist;
                    olddist=newdist;
                    changed = 1;
                }

                c = i+offset_dl;
                cdistx = pDistX[c];
                cdisty = pDistY[c];

                newdistx = cdistx+1;
                newdisty = cdisty-1;
                newdist = DISTAA(c, cdistx, cdisty, newdistx, newdisty);
                if(newdist < olddist-epsilon)
                {
                    pDistX[i] = newdistx;
                    pDistY[i] = newdisty;
                    pDist[i] = newdist;
                    changed = 1;
                }
            }
            i--;

            for (x = iW - 2; x > 0; x--, i--)
            {
                olddist = pDist[i];
                if(olddist <= 0) continue;

                c = i+offset_r;
                cdistx = pDistX[c];
                cdisty = pDistY[c];

                newdistx = cdistx-1;
                newdisty = cdisty;
                newdist = DISTAA(c, cdistx, cdisty, newdistx, newdisty);
                if(newdist < olddist-epsilon)
                {
                    pDistX[i] = newdistx;
                    pDistY[i] = newdisty;
                    pDist[i] = newdist;
                    olddist=newdist;
                    changed = 1;
                }

                c = i+offset_rd;
                cdistx = pDistX[c];
                cdisty = pDistY[c];

                newdistx = cdistx-1;
                newdisty = cdisty-1;
                newdist = DISTAA(c, cdistx, cdisty, newdistx, newdisty);
                if(newdist < olddist-epsilon)
                {
                    pDistX[i] = newdistx;
                    pDistY[i] = newdisty;
                    pDist[i] = newdist;
                    olddist=newdist;
                    changed = 1;
                }

                c = i+offset_d;
                cdistx = pDistX[c];
                cdisty = pDistY[c];

                newdistx = cdistx;
                newdisty = cdisty-1;
                newdist = DISTAA(c, cdistx, cdisty, newdistx, newdisty);
                if(newdist < olddist-epsilon)
                {
                    pDistX[i] = newdistx;
                    pDistY[i] = newdisty;
                    pDist[i] = newdist;
                    olddist=newdist;
                    changed = 1;
                }

                c = i+offset_dl;
                cdistx = pDistX[c];
                cdisty = pDistY[c];

                newdistx = cdistx+1;
                newdisty = cdisty-1;
                newdist = DISTAA(c, cdistx, cdisty, newdistx, newdisty);
                if(newdist < olddist-epsilon)
                {
                    pDistX[i] = newdistx;
                    pDistY[i] = newdisty;
                    pDist[i] = newdist;
                    changed = 1;
                }
            }
            
            olddist = pDist[i];
            if(olddist > 0)
            {
                c = i+offset_r;
                cdistx = pDistX[c];
                cdisty = pDistY[c];

                newdistx = cdistx-1;
                newdisty = cdisty;
                newdist = DISTAA(c, cdistx, cdisty, newdistx, newdisty);
                if(newdist < olddist-epsilon)
                {
                    pDistX[i] = newdistx;
                    pDistY[i] = newdisty;
                    pDist[i] = newdist;
                    olddist=newdist;
                    changed = 1;
                }

                c = i+offset_rd;
                cdistx = pDistX[c];
                cdisty = pDistY[c];

                newdistx = cdistx-1;
                newdisty = cdisty-1;
                newdist = DISTAA(c, cdistx, cdisty, newdistx, newdisty);
                if(newdist < olddist-epsilon)
                {
                    pDistX[i] = newdistx;
                    pDistY[i] = newdisty;
                    pDist[i] = newdist;
                    olddist=newdist;
                    changed = 1;
                }

                c = i+offset_d;
                cdistx = pDistX[c];
                cdisty = pDistY[c];

                newdistx = cdistx;
                newdisty = cdisty-1;
                newdist = DISTAA(c, cdistx, cdisty, newdistx, newdisty);
                if(newdist < olddist-epsilon)
                {
                    pDistX[i] = newdistx;
                    pDistY[i] = newdisty;
                    pDist[i] = newdist;
                    changed = 1;
                }
            }

            i = y * iW + 1;
            for (x = 1; x< iW; x++, i++)
            {
                olddist = pDist[i];
                if(olddist <= 0) continue;

                c = i+offset_l;
                cdistx = pDistX[c];
                cdisty = pDistY[c];

                newdistx = cdistx+1;
                newdisty = cdisty;
                newdist = DISTAA(c, cdistx, cdisty, newdistx, newdisty);
                if(newdist < olddist-epsilon)
                {
                    pDistX[i] = newdistx;
                    pDistY[i] = newdisty;
                    pDist[i] = newdist;
                    changed = 1;
                }
            }
        }
    }
    while(changed);
}
/////////////////////////////////////////////////////////////////////////////
