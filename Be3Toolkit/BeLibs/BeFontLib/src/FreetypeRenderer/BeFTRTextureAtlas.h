/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeFTRTextureAtlas.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_FTRTEXTUREATLAS_H
#define BE_FTRTEXTUREATLAS_H

#include <FreetypeRenderer/BeFTRVector.h>

class FONT_API BeFTRTextureAtlas
{
public:

    /// \brief  A texture atlas is used to pack several small regions into a single texture
    struct BeFTRTextureAtlasData
    {
        /// \brief  Allocated nodes
        BeFTRVector::BeFTRVectorData* m_pNodes;

        /// \brief  Width (in pixels) of the underlying texture
        BeSize_T m_iWidth;

        /// \brief  Height (in pixels) of the underlying texture
        BeSize_T m_iHeight;

        /// \brief  Depth (in bytes) of the underlying texture
        BeSize_T m_iDepth;

        /// \brief  Allocated surface size
        BeSize_T m_iUsed;

        /// \brief  Texture identity
        BeTexture* m_pTexture;

        /// \brief  Atlas data
        unsigned char * m_pData;
    };



    /// \brief  Creates a new empty texture atlas
    static BeFTRTextureAtlasData* TextureAtlasNew(const BeSize_T iWidth, const BeSize_T iHeight, const BeSize_T iDepth);

    /// \brief  Deletes a texture atlas
    static void TextureAtlasDelete(BeFTRTextureAtlasData* pSelf);



    /// \brief  Upload atlas to video memory
    static void TextureAtlasUpload(BeFTRTextureAtlasData* pSelf);



    /// \brief  Allocate a new region in the atlas
    static BeVector4D TextureAtlasGetRegion(BeFTRTextureAtlasData* pSelf, const BeSize_T iWidth, const BeSize_T iHeight, BeBoolean bPreGetRealRegion = BE_FALSE);

    /// \brief  Upload data to the specified atlas region
    static void TextureAtlasSetRegion(BeFTRTextureAtlasData* pSelf, const BeSize_T iX, const BeSize_T iY, const BeSize_T iWidth, const BeSize_T iHeight, const BeUChar8* pData, const BeSize_T iStride, const BeInt iTextureComponentOffset = 0);

    /// \brief  Remove all allocated regions from the atlas
    static void TextureAtlasClear(BeFTRTextureAtlasData* pSelf);



    /// \brief  Fits the atlas
    static BeInt TextureAtlasFit(BeFTRTextureAtlasData* pSelf, const BeSize_T iIndex, const BeSize_T iWidth, const BeSize_T iHeight);

    /// \brief  Merges the atlas
    static void TextureAtlasMerge(BeFTRTextureAtlasData* pSelf);

};

#endif // BE_FTRTEXTUREATLAS_H
