/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeFTRTextureAtlas.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <BeFontLib/pch/BeFontLibPredef.h>

#include <FreetypeRenderer/BeFTRTextureAtlas.h>

/////////////////////////////////////////////////////////////////////////////
BeFTRTextureAtlas::BeFTRTextureAtlasData* BeFTRTextureAtlas::TextureAtlasNew(const BeSize_T iWidth, const BeSize_T iHeight, const BeSize_T iDepth)
{
    BE_FONT_DEBUG("texture_atlas_new: %d, %d   %d\n", (int)iWidth, (int)iHeight, (int)iDepth);

    BeFTRTextureAtlas::BeFTRTextureAtlasData* self = (BeFTRTextureAtlas::BeFTRTextureAtlasData*)malloc(sizeof(BeFTRTextureAtlas::BeFTRTextureAtlasData));

    // We want a one pixel border around the whole atlas to avoid any artefact when
    // sampling texture
    BeVector3D node(1.0f, 1.0f, iWidth - 2);

    BE_ASSERT((iDepth == 1) || (iDepth == 3) || (iDepth == 4));
    if( self == NULL)
    {
        BE_FONT_ERROR("line %d: No more memory for allocating data\n", __LINE__);
        exit( EXIT_FAILURE );
    }

    self->m_pNodes = BeFTRVector::VectorNew(sizeof(BeVector3D));
    self->m_iUsed = 0;
    self->m_iWidth = iWidth;
    self->m_iHeight = iHeight;
    self->m_iDepth = iDepth;
    self->m_pTexture = NULL;

    BeFTRVector::VectorPushBack(self->m_pNodes, &node);
    self->m_pData = (unsigned char *)calloc(iWidth*iHeight*iDepth, sizeof(unsigned char));

    if (self->m_pData == NULL)
    {
        BE_FONT_ERROR("line %d: No more memory for allocating data\n", __LINE__);
        exit( EXIT_FAILURE );
    }

    return self;
}
/////////////////////////////////////////////////////////////////////////////
void BeFTRTextureAtlas::TextureAtlasDelete(BeFTRTextureAtlas::BeFTRTextureAtlasData* pSelf)
{
    BE_ASSERT(pSelf);

    BeFTRVector::VectorDelete(pSelf->m_pNodes);
    if (pSelf->m_pData)
    {
        free(pSelf->m_pData);
    }

    if (pSelf->m_pTexture)
    {
        //glDeleteTextures( 1, &self->id );
        BeDelete pSelf->m_pTexture;
    }

    free(pSelf);
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeFTRTextureAtlas::TextureAtlasUpload(BeFTRTextureAtlas::BeFTRTextureAtlasData* pSelf)
{
    BE_ASSERT(pSelf);
    BE_ASSERT(pSelf->m_pData);

    /*
    if (!self->id)
    {
        glGenTextures(1, &self->id);
    }

    glBindTexture(GL_TEXTURE_2D, self->id);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    if (self->depth == 4)
    {
#ifdef GL_UNSIGNED_INT_8_8_8_8_REV
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, self->width, self->height,
            0, GL_BGRA, GL_UNSIGNED_INT_8_8_8_8_REV, self->data);
#else
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, self->width, self->height,
            0, GL_RGBA, GL_UNSIGNED_BYTE, self->data);
#endif
    }
    else if (self->depth == 3)
    {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, self->width, self->height,
            0, GL_RGB, GL_UNSIGNED_BYTE, self->data);
    }
    else
    {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_ALPHA, self->width, self->height,
            0, GL_ALPHA, GL_UNSIGNED_BYTE, self->data);
    }
    */
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeVector4D BeFTRTextureAtlas::TextureAtlasGetRegion(BeFTRTextureAtlas::BeFTRTextureAtlasData* pSelf, const BeSize_T iWidth, const BeSize_T iHeight, BeBoolean bPreGetRealRegion)
{

    int y, best_height, best_width, best_index;
    BeVector3D *node, *prev;
    BeVector4D region(0.0f, 0.0f, iWidth, iHeight);

    size_t i;

    BE_ASSERT(pSelf);

    best_height = INT_MAX;
    best_index = -1;
    best_width = INT_MAX;
    for (i = 0; i < pSelf->m_pNodes->m_iSize; ++i)
    {
        y = TextureAtlasFit(pSelf, i, iWidth, iHeight);
        if (y >= 0)
        {
            node = (BeVector3D*)BeFTRVector::VectorGet(pSelf->m_pNodes, i);
            if (((y + iHeight) < best_height) || (((y + iHeight) == best_height) && (node->m_fZ < best_width)))
            {
                best_height = y + iHeight;
                best_index = i;
                best_width = node->m_fZ;
                region.m_fX = node->m_fX;
                region.m_fY = y;
            }
        }
    }

    if (best_index == -1)
    {
        region.m_fX = -1;
        region.m_fY = -1;
        region.m_fZ = 0;
        region.m_fW = 0;
        return region;
    }

    if (bPreGetRealRegion)
    {
        return region;
    }

    node = (BeVector3D*)malloc(sizeof(BeVector3D));
    if (node == NULL)
    {
        BE_FONT_ERROR("line %d: No more memory for allocating data\n", __LINE__);
        exit(EXIT_FAILURE);
    }

    node->m_fX = region.m_fX;
    node->m_fY = region.m_fY + iHeight;
    node->m_fZ = iWidth;
    BeFTRVector::VectorInsert(pSelf->m_pNodes, best_index, node);
    free(node);

    for (i = best_index + 1; i < pSelf->m_pNodes->m_iSize; ++i)
    {
        node = (BeVector3D*)BeFTRVector::VectorGet(pSelf->m_pNodes, i);
        prev = (BeVector3D*)BeFTRVector::VectorGet(pSelf->m_pNodes, i - 1);

        if (node->m_fX < (prev->m_fX + prev->m_fZ))
        {
            int shrink = prev->m_fX + prev->m_fZ - node->m_fX;
            node->m_fX += shrink;
            node->m_fZ -= shrink;
            if (node->m_fZ <= 0)
            {
                BeFTRVector::VectorErase(pSelf->m_pNodes, i);
                --i;
            }
            else
            {
                break;
            }
        }
        else
        {
            break;
        }
    }

    TextureAtlasMerge(pSelf);
    pSelf->m_iUsed += iWidth * iHeight;

    return region;
}
/////////////////////////////////////////////////////////////////////////////
void BeFTRTextureAtlas::TextureAtlasSetRegion(BeFTRTextureAtlas::BeFTRTextureAtlasData* pSelf, const BeSize_T iX, const BeSize_T iY, const BeSize_T iWidth, const BeSize_T iHeight, const BeUChar8* pData, const BeSize_T iStride, const BeInt iTextureComponentOffset)
{
    size_t i, j;
    size_t depth;
    size_t charsize;

    BE_ASSERT(pSelf);
    BE_ASSERT(iX > 0);
    BE_ASSERT(iY > 0);
    BE_ASSERT(iX < (pSelf->m_iWidth - 1));
    BE_ASSERT((iX + iWidth) <= (pSelf->m_iWidth - 1));
    BE_ASSERT(iY < (pSelf->m_iHeight - 1));
    BE_ASSERT((iY + iHeight) <= (pSelf->m_iHeight - 1));

    depth = pSelf->m_iDepth;
    charsize = sizeof(BeChar8);

#if 0
    for( i=0; i<height; ++i )
    {
        memcpy( pSelf->data+((y+i)*pSelf->width + x ) * charsize * depth, 
                data + (i*stride) * charsize, width * charsize * depth  );
    }
#else
    for (i = 0; i < iHeight; ++i)
    {
        for (j = 0; j < iWidth; ++j)
        {
            pSelf->m_pData[(((iY + i)*pSelf->m_iWidth + iX + j) * charsize * depth) + iTextureComponentOffset] = pData[(i*iStride) + j];
        }
    }
#endif
}
/////////////////////////////////////////////////////////////////////////////
void BeFTRTextureAtlas::TextureAtlasClear(BeFTRTextureAtlas::BeFTRTextureAtlasData* pSelf)
{
    BeVector3D node(1.0f, 1.0f, 1.0f);

    BE_ASSERT(pSelf);
    BE_ASSERT(pSelf->m_pData);

    BeFTRVector::VectorClear(pSelf->m_pNodes);
    pSelf->m_iUsed = 0;

    // We want a one pixel border around the whole atlas to avoid any artefact when
    // sampling texture
    node.m_fZ = pSelf->m_iWidth - 2;

    BeFTRVector::VectorPushBack(pSelf->m_pNodes, &node);
    memset(pSelf->m_pData, 0, pSelf->m_iWidth * pSelf->m_iHeight * pSelf->m_iDepth);
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeInt BeFTRTextureAtlas::TextureAtlasFit(BeFTRTextureAtlas::BeFTRTextureAtlasData* pSelf, const BeSize_T iIndex, const BeSize_T iWidth, const BeSize_T iHeight)
{
    BeVector3D *node;
    int x, y, width_left;
    size_t i;

    BE_ASSERT(pSelf);

    node = (BeVector3D *)(BeFTRVector::VectorGet(pSelf->m_pNodes, iIndex));
    x = node->m_fX;
    y = node->m_fY;
    width_left = iWidth;
    i = iIndex;

    if ((x + iWidth) > (pSelf->m_iWidth - 1))
    {
        return -1;
    }

    y = node->m_fY;

    while( width_left > 0 )
    {
        node = (BeVector3D *)(BeFTRVector::VectorGet(pSelf->m_pNodes, i));
        if (node->m_fY > y)
        {
            y = node->m_fY;
        }
        if ((y + iHeight) > (pSelf->m_iHeight - 1))
        {
            return -1;
        }
        width_left -= (node->m_fZ);
        ++i;
    }

    return y;
}
/////////////////////////////////////////////////////////////////////////////
void BeFTRTextureAtlas::TextureAtlasMerge(BeFTRTextureAtlas::BeFTRTextureAtlasData* pSelf)
{
    BeVector3D *node, *next;
    size_t i;

    BE_ASSERT(pSelf);

    for (i = 0; i< pSelf->m_pNodes->m_iSize - 1; ++i)
    {
        node = (BeVector3D *)(BeFTRVector::VectorGet(pSelf->m_pNodes, i));
        next = (BeVector3D *)(BeFTRVector::VectorGet(pSelf->m_pNodes, i + 1));
        if (node->m_fY == next->m_fY)
        {
            node->m_fZ += next->m_fZ;

            BeFTRVector::VectorErase(pSelf->m_pNodes, i + 1);

            --i;
        }
    }
}
/////////////////////////////////////////////////////////////////////////////
