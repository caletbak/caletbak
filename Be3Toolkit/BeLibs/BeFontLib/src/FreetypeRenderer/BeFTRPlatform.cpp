/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeFTRPlatform.h
//
/////////////////////////////////////////////////////////////////////////////

#include <FreetypeRenderer/BeFTRPlatform.h>

#if defined(_WIN32) || defined(_WIN64)

/*
double round (float v)
{
    return floor(v+0.5f);
}
*/

// strndup() is not available on Windows
char *strndup( const char *s1, size_t n)
{
    char *copy= (char*)malloc( n+1 );
    memcpy( copy, s1, n );
    copy[n] = 0;
    return copy;
};
#endif 


// strndup() was only added in OSX lion
#if defined(__APPLE__)
char *strndup( const char *s1, size_t n)
{
    char *copy = (char*)calloc( n+1, sizeof(char) );
    memcpy( copy, s1, n );
    return copy;
};
#endif
