/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeFTRPlatform.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_FTRPLATFORM_H
#define BE_FTRPLATFORM_H

#include <BeFontLib/pch/BeFontLibPredef.h>

//-------------------------------------------------
// stdint.h is not available on VS2008 or lower
//-------------------------------------------------
#ifdef _MSC_VER
typedef __int32 int32_t;
typedef unsigned __int32 uint32_t;
typedef __int64 int64_t;
typedef unsigned __int64 uint64_t;
#else
#include <stdint.h>
#endif // _MSC_VER

#ifdef __APPLE__
    /* strndup() was only added in OSX lion */
    char * strndup( const char *s1, size_t n);
#elif defined(_WIN32) || defined(_WIN64) 
    /* does not exist on windows */
    char * strndup( const char *s1, size_t n);
    //double round (float v);
#    pragma warning (disable: 4244) // suspend warnings
#endif // _WIN32 || _WIN64

#endif // BE_FTRPLATFORM_H
