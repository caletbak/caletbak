/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeFTRTextBuffer.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <BeFontLib/pch/BeFontLibPredef.h>

#include <FreetypeRenderer/BeFTRTextBuffer.h>

#define SET_GLYPH_VERTEX(value,x0,y0,z0,s0,t0,r,g,b,a,sh,gm) {                  \
    BeFTRTextBuffer::BeGlyphVertex*  gv = &value;                               \
    gv->m_fX=x0;        gv->m_fY=y0;        gv->m_fZ=z0;                        \
    gv->m_fU=s0;        gv->m_fV=t0;                                            \
    gv->m_fR=r;         gv->m_fG=g;         gv->m_fB=b;         gv->m_fA=a;     \
    gv->m_fShift=sh;    gv->m_fGamma=gm; }

/////////////////////////////////////////////////////////////////////////////
BeFTRTextBuffer::BeFTRTextBufferData* BeFTRTextBuffer::TextBufferNew(BeSize_T iDepth)
{
    BeFTRTextBuffer::BeFTRTextBufferData* pSelf = (BeFTRTextBuffer::BeFTRTextBufferData *)malloc(sizeof(BeFTRTextBuffer::BeFTRTextBufferData));

    pSelf->m_pBuffer = BeFTRVertexBuffer::VertexBufferNew("vertex:3f,tex_coord:2f,color:4f,ashift:1f,agamma:1f");

    pSelf->m_pFontManager = BeFTRFontManager::FontManagerNew(512, 512, iDepth);
    //pSelf->shader = shader_load("shaders/text.vert", "shaders/text.frag");
    //pSelf->shader_texture = glGetUniformLocation(self->shader, "texture");
    //pSelf->shader_pixel = glGetUniformLocation(self->shader, "pixel");
    pSelf->m_iLineStart = 0;
    pSelf->m_fLineAscender = 0;
    pSelf->m_kBaseColor.m_fX = 0.0;
    pSelf->m_kBaseColor.m_fY = 0.0;
    pSelf->m_kBaseColor.m_fZ = 0.0;
    pSelf->m_kBaseColor.m_fW = 1.0;
    pSelf->m_fLineDescender = 0;

    return pSelf;
}
/////////////////////////////////////////////////////////////////////////////
void BeFTRTextBuffer::TextBufferClear(BeFTRTextBuffer::BeFTRTextBufferData* pSelf)
{
    BE_ASSERT(pSelf);

    BeFTRVertexBuffer::VertexBufferClear(pSelf->m_pBuffer);
    pSelf->m_iLineStart = 0;
    pSelf->m_fLineAscender = 0;
    pSelf->m_fLineDescender = 0;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeFTRTextBuffer::TextBufferRender(BeFTRTextBuffer::BeFTRTextBufferData* pSelf)
{
    /*
    glEnable( GL_BLEND );
    glEnable( GL_TEXTURE_2D );
    glBindTexture(GL_TEXTURE_2D, pSelf->manager->atlas->id);
    if (pSelf->manager->atlas->depth == 1)
    {
        //glDisable( GL_COLOR_MATERIAL );
        glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
        glBlendColor( 1, 1, 1, 1 );
    }
    else
    {
        //glEnable( GL_COLOR_MATERIAL );
        //glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
        //glBlendFunc( GL_ONE, GL_ONE_MINUS_SRC_ALPHA );
        //glBlendColor( 1.0, 1.0, 1.0, 1.0 );
        //glBlendFunc( GL_CONSTANT_COLOR_EXT,  GL_ONE_MINUS_SRC_COLOR );
        //glBlendColor( self->base_color.r,
        //self->base_color.g,
        //self->base_color.b,
        //self->base_color.a );
        glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
        glBlendColor( 1, 1, 1, 1 );
    }

    glUseProgram( self->shader );
    glUniform1i( self->shader_texture, 0 );
    glUniform3f( self->shader_pixel,
                 1.0/self->manager->atlas->width,
                 1.0/self->manager->atlas->height,
                 self->manager->atlas->depth );
    vertex_buffer_render( self->buffer, GL_TRIANGLES );
    glUseProgram( 0 );
    */
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeFTRTextBuffer::TextBufferPrintf(BeFTRTextBuffer::BeFTRTextBufferData* pSelf, BeVector2D* kPen, ...)
{
    BeFTRMarkup::BeFTRMarkupData *pMarkup;
    wchar_t *text;
    va_list args;

    if (BeFTRVertexBuffer::VertexBufferSize(pSelf->m_pBuffer) == 0)
    {
        pSelf->m_kOrigin = *kPen;
    }

    va_start(args, kPen);

    do
    {
        pMarkup = va_arg(args, BeFTRMarkup::BeFTRMarkupData*);

        if (pMarkup == NULL)
        {
            return;
        }
        text = va_arg(args, wchar_t *);

        std::wstring strText(text);
        TextBufferAddText(pSelf, kPen, pMarkup, text, strText.size());
    }
    while (pMarkup != 0);

    va_end ( args );
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeFTRTextBuffer::TextBufferMoveLastLine(BeFTRTextBuffer::BeFTRTextBufferData* pSelf, BeFloat fDY)
{
    size_t i, j;

    for (i = pSelf->m_iLineStart; i < BeFTRVector::VectorSize(pSelf->m_pBuffer->m_pItems); ++i)
    {
        BeVector4D* item = (BeVector4D*)BeFTRVector::VectorGet(pSelf->m_pBuffer->m_pItems, i);
        for( j=item->m_fX; j<item->m_fX + item->m_fY; ++j)
        {
            BeFTRTextBuffer::BeGlyphVertex* pVertex = (BeFTRTextBuffer::BeGlyphVertex*)BeFTRVector::VectorGet(pSelf->m_pBuffer->m_pVertices, j);
            pVertex->m_fY -= fDY;
        }
    }
}
/////////////////////////////////////////////////////////////////////////////
void BeFTRTextBuffer::TextBufferAddText(BeFTRTextBuffer::BeFTRTextBufferData* pSelf, BeVector2D* kPen, BeFTRMarkup::BeFTRMarkupData* pMarkup, BeWChar* wText, BeSize_T iLength)
{
    BeFTRFontManager::BeFTRFontManagerData* pManager = pSelf->m_pFontManager;
    size_t i;

    if (pMarkup == NULL)
    {
        return;
    }

    if (!pMarkup->m_pFont)
    {
        pMarkup->m_pFont = BeFTRFontManager::FontManagerGetFromMarkup(pManager, pMarkup);
        if (!pMarkup->m_pFont)
        {
            BE_FONT_ERROR("Houston, we've got a problem !\n");
            exit( EXIT_FAILURE );
        }
    }

    if (iLength == 0)
    {
        std::wstring strText(wText);
        iLength = strText.size();
    }

    if (BeFTRVertexBuffer::VertexBufferSize(pSelf->m_pBuffer) == 0)
    {
        pSelf->m_kOrigin = *kPen;
    }

    TextBufferAddWchar(pSelf, kPen, pMarkup, wText[0], 0);
    for (i = 1; i<iLength; ++i)
    {
        TextBufferAddWchar(pSelf, kPen, pMarkup, wText[i], wText[i - 1]);
    }
}
/////////////////////////////////////////////////////////////////////////////
void BeFTRTextBuffer::TextBufferAddWchar(BeFTRTextBuffer::BeFTRTextBufferData* pSelf, BeVector2D* kPen, BeFTRMarkup::BeFTRMarkupData* pMarkup, BeWChar wCurrent, BeWChar wPrevious)
{
    size_t vcount = 0;
    size_t icount = 0;
    BeFTRVertexBuffer::BeFTRVertexBufferData* pBuffer = pSelf->m_pBuffer;
    BeFTRTextureFont::BeFTRTextureFontData* pFont = pMarkup->m_pFont;
    BeFloat gamma = pMarkup->m_fGamma;

    // Maximum number of vertices is 20 (= 5x2 triangles) per glyph:
    //  - 2 triangles for background
    //  - 2 triangles for overline
    //  - 2 triangles for underline
    //  - 2 triangles for strikethrough
    //  - 2 triangles for glyph
    BeGlyphVertex vertices[4 * 5];
    BeUShort indices[6 * 5];
    BeFTRTextureFont::BeFTRTextureFontGlyph* glyph;
    BeFTRTextureFont::BeFTRTextureFontGlyph* black;
    float kerning = 0;
   
    if (wCurrent == L'\n')
    {
        kPen->m_fX = pSelf->m_kOrigin.m_fX;
        kPen->m_fY += pSelf->m_fLineDescender;
        pSelf->m_fLineDescender = 0;
        pSelf->m_fLineAscender = 0;
        pSelf->m_iLineStart = BeFTRVector::VectorSize(pSelf->m_pBuffer->m_pItems);
        return;
    }

    if (pMarkup->m_pFont->m_fAscender > pSelf->m_fLineAscender)
    {
        float y = kPen->m_fY;
        kPen->m_fY -= (pMarkup->m_pFont->m_fAscender - pSelf->m_fLineAscender);
        TextBufferMoveLastLine(pSelf, (int)(y - kPen->m_fY));
        pSelf->m_fLineAscender = pMarkup->m_pFont->m_fAscender;
    }

    if (pMarkup->m_pFont->m_fDescender < pSelf->m_fLineDescender)
    {
        pSelf->m_fLineDescender = pMarkup->m_pFont->m_fDescender;
    }

    glyph = BeFTRTextureFont::TextureFontGetGlyph(pFont, wCurrent);
    black = BeFTRTextureFont::TextureFontGetGlyph(pFont, -1);
        
    if( glyph == NULL )
    {
        return;
    }
    
    if (wPrevious && pMarkup->m_pFont->m_iKerning)
    {
        kerning = BeFTRTextureFont::TextureGlyphGetKerning(glyph, wPrevious);
    }
    kPen->m_fX += kerning;

    // Background
    if (pMarkup->m_kBackgroundColor.m_fW > 0)
    {
        float r = pMarkup->m_kBackgroundColor.m_fX;
        float g = pMarkup->m_kBackgroundColor.m_fY;
        float b = pMarkup->m_kBackgroundColor.m_fZ;
        float a = pMarkup->m_kBackgroundColor.m_fW;
        float x0 = (kPen->m_fX - kerning);
        float y0 = (int)(kPen->m_fY + pFont->m_fDescender);
        float x1 = (x0 + glyph->m_fAdvanceX);
        float y1 = (int)(y0 + pFont->m_fHeight + pFont->m_fLinegap);
        float s0 = black->m_fS0;
        float t0 = black->m_fT0;
        float s1 = black->m_fS1;
        float t1 = black->m_fT1;

        SET_GLYPH_VERTEX(vertices[vcount+0], (int)x0,y0,0,  s0,t0,  r,g,b,a,  x0-((int)x0), gamma );
        SET_GLYPH_VERTEX(vertices[vcount+1], (int)x0,y1,0,  s0,t1,  r,g,b,a,  x0-((int)x0), gamma );
        SET_GLYPH_VERTEX(vertices[vcount+2], (int)x1,y1,0,  s1,t1,  r,g,b,a,  x1-((int)x1), gamma );
        SET_GLYPH_VERTEX(vertices[vcount+3], (int)x1,y0,0,  s1,t0,  r,g,b,a,  x1-((int)x1), gamma );

        indices[icount + 0] = (BeUShort)(vcount+0);
        indices[icount + 1] = (BeUShort)(vcount+1);
        indices[icount + 2] = (BeUShort)(vcount+2);
        indices[icount + 3] = (BeUShort)(vcount+0);
        indices[icount + 4] = (BeUShort)(vcount+2);
        indices[icount + 5] = (BeUShort)(vcount+3);
        vcount += 4;
        icount += 6;
    }

    // Underline
    if (pMarkup->m_iUnderline)
    {
        float r = pMarkup->m_kUnderlineColor.m_fX;
        float g = pMarkup->m_kUnderlineColor.m_fY;
        float b = pMarkup->m_kUnderlineColor.m_fZ;
        float a = pMarkup->m_kUnderlineColor.m_fW;
        float x0 = (kPen->m_fX - kerning);
        float y0 = (int)(kPen->m_fY + pFont->m_fUnderLinePosition);
        float x1 = (x0 + glyph->m_fAdvanceX);
        float y1 = (int)(y0 + pFont->m_fUnderlineThickness);
        float s0 = black->m_fS0;
        float t0 = black->m_fT0;
        float s1 = black->m_fS1;
        float t1 = black->m_fT1;

        SET_GLYPH_VERTEX(vertices[vcount+0], (int)x0,y0,0,  s0,t0,  r,g,b,a,  x0-((int)x0), gamma );
        SET_GLYPH_VERTEX(vertices[vcount+1], (int)x0,y1,0,  s0,t1,  r,g,b,a,  x0-((int)x0), gamma );
        SET_GLYPH_VERTEX(vertices[vcount+2], (int)x1,y1,0,  s1,t1,  r,g,b,a,  x1-((int)x1), gamma );
        SET_GLYPH_VERTEX(vertices[vcount+3], (int)x1,y0,0,  s1,t0,  r,g,b,a,  x1-((int)x1), gamma );

        indices[icount + 0] = (BeUShort)(vcount+0);
        indices[icount + 1] = (BeUShort)(vcount+1);
        indices[icount + 2] = (BeUShort)(vcount+2);
        indices[icount + 3] = (BeUShort)(vcount+0);
        indices[icount + 4] = (BeUShort)(vcount+2);
        indices[icount + 5] = (BeUShort)(vcount+3);
        vcount += 4;
        icount += 6;
    }
    
    // Overline
    if (pMarkup->m_iOverline)
    {
        float r = pMarkup->m_kOutlineColor.m_fX;
        float g = pMarkup->m_kOutlineColor.m_fY;
        float b = pMarkup->m_kOutlineColor.m_fZ;
        float a = pMarkup->m_kOutlineColor.m_fW;
        float x0 = (kPen->m_fX - kerning);
        float y0 = (int)(kPen->m_fY + (int)pFont->m_fAscender);
        float x1 = (x0 + glyph->m_fAdvanceX);
        float y1 = (int)(y0 + (int)pFont->m_fUnderlineThickness);
        float s0 = black->m_fS0;
        float t0 = black->m_fT0;
        float s1 = black->m_fS1;
        float t1 = black->m_fT1;

        SET_GLYPH_VERTEX(vertices[vcount+0], (int)x0,y0,0,  s0,t0,  r,g,b,a,  x0-((int)x0), gamma );
        SET_GLYPH_VERTEX(vertices[vcount+1], (int)x0,y1,0,  s0,t1,  r,g,b,a,  x0-((int)x0), gamma );
        SET_GLYPH_VERTEX(vertices[vcount+2], (int)x1,y1,0,  s1,t1,  r,g,b,a,  x1-((int)x1), gamma );
        SET_GLYPH_VERTEX(vertices[vcount+3], (int)x1,y0,0,  s1,t0,  r,g,b,a,  x1-((int)x1), gamma );

        indices[icount + 0] = (BeUShort)(vcount+0);
        indices[icount + 1] = (BeUShort)(vcount+1);
        indices[icount + 2] = (BeUShort)(vcount+2);
        indices[icount + 3] = (BeUShort)(vcount+0);
        indices[icount + 4] = (BeUShort)(vcount+2);
        indices[icount + 5] = (BeUShort)(vcount+3);
        vcount += 4;
        icount += 6;
    }
        
    /* Strikethrough */
    if (pMarkup->m_iStrikethrough)
    {
        float r = pMarkup->m_kStrikethroughColor.m_fX;
        float g = pMarkup->m_kStrikethroughColor.m_fY;
        float b = pMarkup->m_kStrikethroughColor.m_fZ;
        float a = pMarkup->m_kStrikethroughColor.m_fW;
        float x0 = (kPen->m_fX - kerning);
        float y0 = (int)(kPen->m_fY + (int)pFont->m_fAscender*.33);
        float x1  = ( x0 + glyph->m_fAdvanceX );
        float y1 = (int)(y0 + (int)pFont->m_fUnderlineThickness);
        float s0 = black->m_fS0;
        float t0 = black->m_fT0;
        float s1 = black->m_fS1;
        float t1 = black->m_fT1;

        SET_GLYPH_VERTEX(vertices[vcount+0], (int)x0,y0,0,  s0,t0,  r,g,b,a,  x0-((int)x0), gamma );
        SET_GLYPH_VERTEX(vertices[vcount+1], (int)x0,y1,0,  s0,t1,  r,g,b,a,  x0-((int)x0), gamma );
        SET_GLYPH_VERTEX(vertices[vcount+2], (int)x1,y1,0,  s1,t1,  r,g,b,a,  x1-((int)x1), gamma );
        SET_GLYPH_VERTEX(vertices[vcount+3], (int)x1,y0,0,  s1,t0,  r,g,b,a,  x1-((int)x1), gamma );

        indices[icount + 0] = (BeUShort)(vcount+0);
        indices[icount + 1] = (BeUShort)(vcount+1);
        indices[icount + 2] = (BeUShort)(vcount+2);
        indices[icount + 3] = (BeUShort)(vcount+0);
        indices[icount + 4] = (BeUShort)(vcount+2);
        indices[icount + 5] = (BeUShort)(vcount+3);
        vcount += 4;
        icount += 6;
    }

    {
        // Actual glyph
        float r = pMarkup->m_kForegroundColor.m_fX;
        float g = pMarkup->m_kForegroundColor.m_fY;
        float b = pMarkup->m_kForegroundColor.m_fZ;
        float a = pMarkup->m_kForegroundColor.m_fW;
        float x0 = (kPen->m_fX + glyph->m_iOffsetX);
        float y0 = (int)(kPen->m_fY + glyph->m_iOffsetY);
        float x1 = ( x0 + glyph->m_iWidth );
        float y1 = (int)( y0 - glyph->m_iHeight );
        float s0 = glyph->m_fS0;
        float t0 = glyph->m_fT0;
        float s1 = glyph->m_fS1;
        float t1 = glyph->m_fT1;

        SET_GLYPH_VERTEX(vertices[vcount+0], (int)x0,y0,0,  s0,t0,  r,g,b,a,  x0-((int)x0), gamma );
        SET_GLYPH_VERTEX(vertices[vcount+1], (int)x0,y1,0,  s0,t1,  r,g,b,a,  x0-((int)x0), gamma );
        SET_GLYPH_VERTEX(vertices[vcount+2], (int)x1,y1,0,  s1,t1,  r,g,b,a,  x1-((int)x1), gamma );
        SET_GLYPH_VERTEX(vertices[vcount+3], (int)x1,y0,0,  s1,t0,  r,g,b,a,  x1-((int)x1), gamma );

        indices[icount + 0] = (BeUShort)(vcount+0);
        indices[icount + 1] = (BeUShort)(vcount+1);
        indices[icount + 2] = (BeUShort)(vcount+2);
        indices[icount + 3] = (BeUShort)(vcount+0);
        indices[icount + 4] = (BeUShort)(vcount+2);
        indices[icount + 5] = (BeUShort)(vcount+3);
        vcount += 4;
        icount += 6;

        BeFTRVertexBuffer::VertexBufferPushBack(pBuffer, vertices, vcount, indices, icount);
        kPen->m_fX += glyph->m_fAdvanceX * (1.0 + pMarkup->m_fSpacing);
    }
}
/////////////////////////////////////////////////////////////////////////////
