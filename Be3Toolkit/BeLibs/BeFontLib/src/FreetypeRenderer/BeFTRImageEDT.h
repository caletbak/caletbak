/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeFTRImageEDT.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_FTRIMAGEEDT_H
#define BE_FTRIMAGEEDT_H

#include <BeFontLib/pch/BeFontLibPredef.h>

class FONT_API BeFTRImageEDT
{
public:

    /// \brief  Compute the local gradient at edge pixels using convolution filters
    static void ComputeGradient(BeDouble* pImage, BeInt iW, BeInt iH, BeDouble* pGX, BeDouble *pGY);

    /// \brief  To approximate the distance to an edge in a certain pixel, with consideration to either the local gradient(gx, gy) or the direction to the pixel(dx, dy) and the pixel greyscale value a
    static double EdgedF(BeDouble fGX, BeDouble fGY, BeDouble fA);

    static double Distaa3(BeDouble* pImage, BeDouble* pGXImage, BeDouble* pGYImage, BeInt iW, BeInt iC, BeInt iXC, BeInt iYC, BeInt iXI, BeInt iYI);

    static void Edtaa3(BeDouble* pImage, BeDouble *pGX, BeDouble* pGY, BeInt iW, BeInt iH, BeShort* pDistX, BeShort* pDistY, BeDouble* pDist);
};

#endif // BE_FTRIMAGEEDT_H
