/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeFTRVertexAttribute.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <BeFontLib/pch/BeFontLibPredef.h>

#include <FreetypeRenderer/BeFTRVertexAttribute.h>
#include <FreetypeRenderer/BeFTRPlatform.h>

/////////////////////////////////////////////////////////////////////////////
BeFTRVertexAttribute::BeFTRVertexAttributeData* BeFTRVertexAttribute::VertexAttributeNew(BeChar8* pName, BeInt iSize, BeInt iType, BeBoolean bNormalized, BeInt iStride, BeVoidP pPointer)
{
    BeFTRVertexAttribute::BeFTRVertexAttributeData* pAttribute = (BeFTRVertexAttribute::BeFTRVertexAttributeData*)malloc(sizeof(BeFTRVertexAttribute::BeFTRVertexAttributeData));

    BE_ASSERT(iSize > 0);

    pAttribute->m_pName = (BeChar8*)strdup(pName);
    pAttribute->m_iIndex = -1;
    pAttribute->m_iSize = iSize;
    pAttribute->m_iType = iType;
    pAttribute->m_bNormalized = bNormalized;
    pAttribute->m_iStride = iStride;
    pAttribute->m_pPointer = pPointer;

    return pAttribute;
}
/////////////////////////////////////////////////////////////////////////////
void BeFTRVertexAttribute::VertexAttributeDelete(BeFTRVertexAttributeData* pSelf)
{
    BE_ASSERT(pSelf);

    free(pSelf->m_pName);
    free(pSelf);
}
/////////////////////////////////////////////////////////////////////////////
BeFTRVertexAttribute::BeFTRVertexAttributeData* BeFTRVertexAttribute::VertexAttributeParse(BeChar8* pFormat)
{
    BeInt type = 0;
    int size;
    bool normalized = false;
    char ctype;
    char *name;
    BeFTRVertexAttribute::BeFTRVertexAttributeData* attr;

    char *p = strchr(pFormat, ':');
    if( p != NULL)
    {
        name = strndup(pFormat, p - pFormat);
        if( *(++p) == '\0' ) 
        {
            BE_FONT_ERROR("No size specified for '%s' attribute\n", name);
            free( name );
            return 0;
        }
        size = *p - '0';

        if( *(++p) == '\0' ) 
        {
            BE_FONT_ERROR("No format specified for '%s' attribute\n", name);
            free( name );
            return 0;
        }
        ctype = *p;

        if( *(++p) != '\0' ) 
        {
            if( *p == 'n' )
            {
                normalized = true;
            }
        }

    }
    else
    {
        BE_FONT_ERROR("Vertex attribute format not understood ('%s')\n", pFormat);
        return 0;
    }

    switch( ctype )
    {
        case 'b': type = BeCoreTypes::BE_CHAR8;             break;
        case 'B': type = BeCoreTypes::BE_UNSIGNED_CHAR8;    break;
        case 's': type = BeCoreTypes::BE_SHORT;             break;
        case 'S': type = BeCoreTypes::BE_UNSIGNED_SHORT;    break;
        case 'i': type = BeCoreTypes::BE_INT;               break;
        case 'I': type = BeCoreTypes::BE_UNSIGNED_INT;      break;
        case 'f': type = BeCoreTypes::BE_FLOAT;             break;
        default:  type = 0;                                 break;
    }

    attr = VertexAttributeNew( name, size, type, normalized, 0, 0 );
    free( name );

    return attr;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeFTRVertexAttribute::VertexAttributeEnable(BeFTRVertexAttribute::BeFTRVertexAttributeData* pAttr)
{
    /*
    if( attr->index == -1 )
    {
        GLint program;
        glGetIntegerv( GL_CURRENT_PROGRAM, &program );
        if( program == 0)
        {
            return;
        }
        attr->index = glGetAttribLocation( program, attr->name );
        if( attr->index == -1 )
        {
            return;
        }
    }
    glEnableVertexAttribArray( attr->index );
    glVertexAttribPointer( attr->index, attr->size, attr->type, attr->normalized, attr->stride, attr->pointer );
    */
}
/////////////////////////////////////////////////////////////////////////////

