/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeFTRMarkup.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_FTRMARKUP_H
#define BE_FTRMARKUP_H

#include <FreetypeRenderer/BeFTRTextureFont.h>

class FONT_API BeFTRMarkup
{
public:

    struct BeFTRMarkupData
    {
        /// \brief  font family name such as "normal", "sans", "serif" or "monospace"
        BeChar8* m_pFamily;

        /// \brief  Font size
        BeFloat m_fSize;

        /// \brief  Whether text is bold
        BeInt m_iBold;

        /// \brief  Whether text is italic
        BeInt m_iItalic;

        /// \brief  Vertical displacement from the baseline
        BeFloat m_fRise;

        /// \brief  Spacing between letters
        BeFloat m_fSpacing;

        /// \brief  Gamma correction
        BeFloat m_fGamma;

        /// \brief  Text color
        BeVector4D m_kForegroundColor;

        /// \brief  Background color
        BeVector4D m_kBackgroundColor;

        /// \brief  Whether outline is active
        BeInt m_iOutline;

        /// \brief  Outline color
        BeVector4D m_kOutlineColor;

        /// \brief  Whether underline is active
        BeInt m_iUnderline;

        /// \brief  Underline color
        BeVector4D m_kUnderlineColor;

        /// \brief  Whether overline is active
        BeInt m_iOverline;

        /// \brief  Overline color
        BeVector4D m_kOverlineColor;

        /// \brief  Whether strikethrough is active
        BeInt m_iStrikethrough;

        /// \brief  Strikethrough color
        BeVector4D m_kStrikethroughColor;

        /// \brief  Pointer on the corresponding font (family/size/bold/italic)
        BeFTRTextureFont::BeFTRTextureFontData* m_pFont;

    };
};

extern BeFTRMarkup::BeFTRMarkupData kDefaultMarkup;

#endif // BE_FTRMARKUP_H
