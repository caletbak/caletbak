/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeFTRTextureFont.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <BeFontLib/pch/BeFontLibPredef.h>

//#include "platform.h"

#include <FreetypeRenderer/BeFTRTextureFont.h>

#undef __FTERRORS_H__
#define FT_ERRORDEF( e, v, s )  { e, s },
#define FT_ERROR_START_LIST     {
#define FT_ERROR_END_LIST       { 0, 0 } };
const struct
{
    int          code;
    const char*  message;
} FT_Errors[] =
#include FT_ERRORS_H

#define USE_LIB_MEMORY  1

/////////////////////////////////////////////////////////////////////////////
BeFTRTextureFont::BeFTRTextureFontData* BeFTRTextureFont::TextureFontNew(BeFTRTextureAtlas::BeFTRTextureAtlasData* pAtlas, const BeChar8* pFilename, const BeFloat fSize, const BeFloat fStrokeSize, FT_Encoding kEncoding)
{
    BeFTRTextureFont::BeFTRTextureFontData* pSelf = (BeFTRTextureFont::BeFTRTextureFontData*)malloc(sizeof(BeFTRTextureFont::BeFTRTextureFontData));
    FT_Library library;
    FT_Face face;
    FT_Size_Metrics metrics;

    BE_ASSERT(pFilename);
    BE_ASSERT(fSize);

    if (pSelf == NULL)
    {
        BE_FONT_ERROR("line %d: No more memory for allocating data\n", __LINE__);
        exit(EXIT_FAILURE);
    }

    pSelf->m_pGlyphs = BeFTRVector::VectorNew(sizeof(BeFTRTextureFont::BeFTRTextureFontGlyph*));
    pSelf->m_pAtlas = pAtlas;
    pSelf->m_fHeight = 0;
    pSelf->m_fAscender = 0;
    pSelf->m_fDescender = 0;
    pSelf->m_pFilename = strdup(pFilename);
    pSelf->m_fSize = fSize;

    if (fStrokeSize > 0.0f)
    {
        pSelf->m_iOutlineType = 1;
        pSelf->m_fOutlineThickness = fStrokeSize;
    }
    else
    {
        pSelf->m_iOutlineType = 0;
        pSelf->m_fOutlineThickness = 0.0;
    }

    pSelf->m_iHinting = 1;
    pSelf->m_iKerning = 1;
    pSelf->m_iFiltering = 1;
    // FT_LCD_FILTER_LIGHT   is (0x00, 0x55, 0x56, 0x55, 0x00)
    // FT_LCD_FILTER_DEFAULT is (0x10, 0x40, 0x70, 0x40, 0x10)
    pSelf->m_cLcdWeights[0] = 0x10;
    pSelf->m_cLcdWeights[1] = 0x40;
    pSelf->m_cLcdWeights[2] = 0x70;
    pSelf->m_cLcdWeights[3] = 0x40;
    pSelf->m_cLcdWeights[4] = 0x10;
    pSelf->m_pFontEncoding = kEncoding;

    /* Get font metrics at high resolution */

#if USE_LIB_MEMORY
    if (!TextureFontLoadFace(&library, pSelf->m_pFilename, pSelf->m_fSize * 100, &face, pSelf->m_pFontEncoding))
    {
        return pSelf;
    }
#else
    if (!TextureFontLoadFace(&pSelf->library, pSelf->filename, pSelf->size, &pSelf->face, pSelf->fontEncoding))
    {
        return pSelf;
    }

    library = pSelf->library;
    face = pSelf->face;
#endif

    // 64 * 64 because of 26.6 encoding AND the transform matrix used
    // in texture_font_load_face (hres = 64)
    pSelf->m_fUnderLinePosition = face->underline_position / (float)(64.0f*64.0f) * pSelf->m_fSize;
    pSelf->m_fUnderLinePosition = round(pSelf->m_fUnderLinePosition);
    if (pSelf->m_fUnderLinePosition > -2)
    {
        pSelf->m_fUnderLinePosition = -2.0;
    }

    pSelf->m_fUnderlineThickness = face->underline_thickness / (float)(64.0f*64.0f) * pSelf->m_fSize;
    pSelf->m_fUnderlineThickness = round(pSelf->m_fUnderlineThickness);
    if (pSelf->m_fUnderlineThickness < 1)
    {
        pSelf->m_fUnderlineThickness = 1.0;
    }

    metrics = face->size->metrics;
    pSelf->m_fAscender = (metrics.ascender >> 6) / 100.0;
    pSelf->m_fDescender = (metrics.descender >> 6) / 100.0;
    pSelf->m_fHeight = (metrics.height >> 6) / 100.0;
    pSelf->m_fLinegap = pSelf->m_fHeight - pSelf->m_fAscender + pSelf->m_fDescender;

#if USE_LIB_MEMORY
    FT_Done_Face(face);
    FT_Done_FreeType(library);
#endif

    /* -1 is a special glyph */
    TextureFontGetGlyph(pSelf, -1);

    return pSelf;
}
/////////////////////////////////////////////////////////////////////////////
void BeFTRTextureFont::TextureFontDelete(BeFTRTextureFont::BeFTRTextureFontData* pSelf)
{
    size_t i;
    BeFTRTextureFont::BeFTRTextureFontGlyph* glyph;

    BE_ASSERT(pSelf);

    if (pSelf->m_pFilename)
    {
        free(pSelf->m_pFilename);
    }

    for (i = 0; i<BeFTRVector::VectorSize(pSelf->m_pGlyphs); ++i)
    {
        glyph = *(BeFTRTextureFont::BeFTRTextureFontGlyph**)BeFTRVector::VectorGet(pSelf->m_pGlyphs, i);
        TextureGlyphDelete(glyph);
    }

    BeFTRVector::VectorDelete(pSelf->m_pGlyphs);
    free(pSelf);
}
/////////////////////////////////////////////////////////////////////////////
void BeFTRTextureFont::TextureFontClose(BeFTRTextureFont::BeFTRTextureFontData* pSelf)
{
#if !USE_LIB_MEMORY
    BE_ASSERT(pSelf);

    if (pSelf->face)
    {
        FT_Done_Face(pSelf->face);
        pSelf->face = NULL;
    }
    if (pSelf->library)
    {
        FT_Done_FreeType(pSelf->library);
        pSelf->library = NULL;
    }
#endif
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeInt BeFTRTextureFont::TextureFontLoadFace(FT_Library* pLibrary, const BeChar8* pFilename, const BeFloat size, FT_Face* pFace, FT_Encoding kEncoding)
{
    BE_FONT_DEBUG("texture_font_load_face: %s\n", pFilename);

    size_t hres = 64;
    FT_Error error;
    FT_Matrix matrix = { (int)((1.0/hres) * 0x10000L),
                         (int)((0.0)      * 0x10000L),
                         (int)((0.0)      * 0x10000L),
                         (int)((1.0)      * 0x10000L) };

    BE_ASSERT(pLibrary);
    BE_ASSERT(pFilename);
    BE_ASSERT(size);

    /* Initialize library */
    error = FT_Init_FreeType(pLibrary);
    if( error )
    {
        BE_FONT_ERROR("FT_Error (0x%02x) : %s\n", FT_Errors[error].code, FT_Errors[error].message);

        return 0;
    }

    /* Load face */
    error = FT_New_Face(*pLibrary, pFilename, 0, pFace);
    if( error )
    {
        BE_FONT_ERROR("FT_Error (line %d, code 0x%02x) : %s\n", __LINE__, FT_Errors[error].code, FT_Errors[error].message);

        FT_Done_FreeType(*pLibrary);

        return 0;
    }

    /* Select charmap */
    error = FT_Select_Charmap(*pFace, kEncoding);
    if( error )
    {
        BE_FONT_ERROR("FT_Error (line %d, code 0x%02x) : %s\n", __LINE__, FT_Errors[error].code, FT_Errors[error].message );

        FT_Done_Face(*pFace);

        FT_Done_FreeType(*pLibrary);

        return 0;
    }

    /* Set char size */
    error = FT_Set_Char_Size(*pFace, (int)(size * 64), 0, 72 * hres, 72);
    if( error )
    {
        BE_FONT_ERROR("FT_Error (line %d, code 0x%02x) : %s\n", __LINE__, FT_Errors[error].code, FT_Errors[error].message );

        FT_Done_Face(*pFace);

        FT_Done_FreeType(*pLibrary);

        return 0;
    }

    /* Set transform matrix */
    FT_Set_Transform(*pFace, &matrix, NULL);

    return 1;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeFTRTextureFont::BeFTRTextureFontGlyph* BeFTRTextureFont::TextureGlyphNew(void)
{
    BeFTRTextureFont::BeFTRTextureFontGlyph* pSelf = (BeFTRTextureFont::BeFTRTextureFontGlyph*)malloc(sizeof(BeFTRTextureFont::BeFTRTextureFontGlyph));
    if (pSelf == NULL)
    {
        BE_FONT_ERROR("line %d: No more memory for allocating data\n", __LINE__);
        exit( EXIT_FAILURE );
    }

    pSelf->m_uId = 0;
    pSelf->m_iWidth = 0;
    pSelf->m_iHeight = 0;
    pSelf->m_iOutlineType = 0;
    pSelf->m_fOutlineThickness = 0.0;
    pSelf->m_iOffsetX = 0;
    pSelf->m_iOffsetY = 0;
    pSelf->m_fAdvanceX = 0.0;
    pSelf->m_fAdvanceY = 0.0;
    pSelf->m_fS0 = 0.0;
    pSelf->m_fT0 = 0.0;
    pSelf->m_fS1 = 0.0;
    pSelf->m_fT1 = 0.0;
    pSelf->m_pKerning = BeFTRVector::VectorNew(sizeof(BeFTRTextureFont::BeFTRTextureFontKerning));

    return pSelf;
}
/////////////////////////////////////////////////////////////////////////////
void BeFTRTextureFont::TextureGlyphDelete(BeFTRTextureFont::BeFTRTextureFontGlyph* pSelf)
{
    BE_ASSERT(pSelf);

    BeFTRVector::VectorDelete(pSelf->m_pKerning);

    free(pSelf);
}
/////////////////////////////////////////////////////////////////////////////
BeFloat BeFTRTextureFont::TextureGlyphGetKerning(const BeFTRTextureFont::BeFTRTextureFontGlyph* pSelf, const BeWChar wCharcode)
{
    size_t i;

    BE_ASSERT(pSelf);
    for (i = 0; i<BeFTRVector::VectorSize(pSelf->m_pKerning); ++i)
    {
        BeFTRTextureFont::BeFTRTextureFontKerning* kerning = (BeFTRTextureFont::BeFTRTextureFontKerning *)BeFTRVector::VectorGet(pSelf->m_pKerning, i);
        if (kerning->m_wCharcode == wCharcode)
        {
            return kerning->m_fKerning;
        }
    }
    return 0;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeFTRTextureFont::TextureFontGenerateKerning(BeFTRTextureFont::BeFTRTextureFontData* pSelf)
{
    size_t i, j;
    FT_Library library;
    FT_Face face;
    FT_UInt glyph_index, prev_index;
    BeFTRTextureFont::BeFTRTextureFontGlyph*glyph, *prev_glyph;
    FT_Vector kerning;

    BE_ASSERT(pSelf);

    /* Load font */
#if USE_LIB_MEMORY
    if (!TextureFontLoadFace(&library, pSelf->m_pFilename, pSelf->m_fSize, &face, pSelf->m_pFontEncoding))
    {
        return;
    }
#else
    face = pSelf->face;
#endif

    /* For each glyph couple combination, check if kerning is necessary */
    /* Starts at index 1 since 0 is for the special backgroudn glyph */
    for (i = 1; i<pSelf->m_pGlyphs->m_iSize; ++i)
    {
        glyph = *(BeFTRTextureFont::BeFTRTextureFontGlyph**)BeFTRVector::VectorGet(pSelf->m_pGlyphs, i);
        glyph_index = FT_Get_Char_Index( face, glyph->m_wCharcode );

        BeFTRVector::VectorClear(glyph->m_pKerning);

        for (j = 1; j<pSelf->m_pGlyphs->m_iSize; ++j)
        {
            prev_glyph = *(BeFTRTextureFont::BeFTRTextureFontGlyph**)BeFTRVector::VectorGet(pSelf->m_pGlyphs, j);
            prev_index = FT_Get_Char_Index(face, prev_glyph->m_wCharcode);

            FT_Get_Kerning( face, prev_index, glyph_index, FT_KERNING_UNFITTED, &kerning );

            // printf("%c(%d)-%c(%d): %ld\n",
            //       prev_glyph->m_wCharcode, prev_glyph->m_wCharcode,
            //       glyph_index, glyph_index, kerning.x);
            if( kerning.x )
            {
                // 64 * 64 because of 26.6 encoding AND the transform matrix used
                // in texture_font_load_face (hres = 64)
                BeFTRTextureFont::BeFTRTextureFontKerning k = { prev_glyph->m_wCharcode, kerning.x / (float)(64.0f*64.0f) };
                BeFTRVector::VectorPushBack(glyph->m_pKerning, &k);
            }
        }
    }

#if USE_LIB_MEMORY
    FT_Done_Face( face );
    FT_Done_FreeType( library );
#endif
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeSize_T BeFTRTextureFont::TextureFontLoadGlyphs(BeFTRTextureFont::BeFTRTextureFontData* pSelf, const BeWChar* pCharcodes, BeInt iArraySize, BeInt iPreW, BeInt iPreH)
{
    size_t i, x, y, width, height, depth, w, h;
    size_t x_MAX, y_MAX, biggestW, biggestH;

#if USE_LIB_MEMORY
    FT_Library library;
#endif

    FT_Error error;
    FT_Face face;
    FT_Glyph ft_glyph;
    FT_GlyphSlot slot;
    FT_Bitmap ft_bitmap;
    
    FT_UInt glyph_index;
    BeFTRTextureFont::BeFTRTextureFontGlyph* glyph;
    BeVector4D region;
    size_t missed = 0;

    BE_ASSERT(pSelf);
    BE_ASSERT(pCharcodes);

    if (iArraySize == -1)
    {
        std::wstring strCharCodes(pCharcodes);
        iArraySize = strCharCodes.size();
    }

    width = pSelf->m_pAtlas->m_iWidth;
    height = pSelf->m_pAtlas->m_iHeight;
    depth = pSelf->m_pAtlas->m_iDepth;

#if USE_LIB_MEMORY
    if (!TextureFontLoadFace(&library, pSelf->m_pFilename, pSelf->m_fSize, &face, pSelf->m_pFontEncoding))
    {
        return iArraySize;
    }
#else
    library = pSelf->library;
    face = pSelf->face;
#endif

    /* Load each glyph */
    for (i = 0; i<iArraySize; ++i)
    {
        //FT_Int32 flags = 0;

        int ft_bitmap_width = 0;
        int ft_bitmap_rows = 0;
        int ft_bitmap_pitch = 0;
        int ft_glyph_top = 0;
        int ft_glyph_left = 0;

        int ft_glyph_top_MAX = 0;
        int ft_glyph_left_MAX = 0;

        glyph_index = FT_Get_Char_Index( face, pCharcodes[i] );

        int strokePix = floor(pSelf->m_fOutlineThickness + 0.5);

        // We want each glyph to be separated by at least one black pixel
        // (for example for shader used in demo-subpixel.c)
        if (iPreW != -1 && iPreH != -1)
        {
            w = iPreW + 1 + strokePix;
            h = iPreH + 1 + strokePix;
            region = BeFTRTextureAtlas::TextureAtlasGetRegion(pSelf->m_pAtlas, w, h, true);
            if (region.m_fX < 0)
            {
                missed++;
                //FONTLIB_ERROR("Texture atlas is full (line %d)\n", __LINE__);
                continue;
            }
        }
        else
        {
            w = 0;
            h = 0;
        }

        biggestW = w;
        biggestH = h;

        bool bHasGeneratedStrokedGlyphs = false;
        int iTotalGeneratingPhases = 1;
        if (pSelf->m_iOutlineType != 0)
        {
            iTotalGeneratingPhases = 2;
        }

        bool bStopProcessingGlyphs = false;

        for (int glyphGenerationPhase = iTotalGeneratingPhases-1; glyphGenerationPhase >= 0; --glyphGenerationPhase)
        {
            FT_Int32 flags = 0;

            if (glyphGenerationPhase > 0)
            {
                flags |= FT_LOAD_NO_BITMAP;
            }
            else
            {
                flags |= FT_LOAD_RENDER;
            }

            if (!pSelf->m_iHinting)
            {
                flags |= FT_LOAD_NO_HINTING | FT_LOAD_NO_AUTOHINT;
            }
            else
            {
                flags |= FT_LOAD_FORCE_AUTOHINT;
            }

            error = FT_Load_Glyph(face, glyph_index, flags);
            if (error)
            {
                BE_FONT_ERROR("FT_Error (line %d, code 0x%02x) : %s\n", __LINE__, FT_Errors[error].code, FT_Errors[error].message);

#if USE_LIB_MEMORY
                FT_Done_Face( face );
                FT_Done_FreeType( library );
#endif
                return iArraySize - i;
            }

            if (glyphGenerationPhase == 0)
            {
                slot = face->glyph;
                ft_bitmap = slot->bitmap;
                ft_bitmap_width = slot->bitmap.width;
                ft_bitmap_rows = slot->bitmap.rows;
                ft_bitmap_pitch = slot->bitmap.pitch;
                ft_glyph_top = slot->bitmap_top;
                ft_glyph_left = slot->bitmap_left;
            }
            else
            {
                bHasGeneratedStrokedGlyphs = true;

                FT_Stroker stroker;
                FT_BitmapGlyph ft_bitmap_glyph;
                error = FT_Stroker_New(library, &stroker);

                if (error)
                {
                    BE_FONT_ERROR("FT_Error (0x%02x) : %s\n", FT_Errors[error].code, FT_Errors[error].message);

#if USE_LIB_MEMORY
                    FT_Done_Face( face );
#endif
                    FT_Stroker_Done(stroker);
#if USE_LIB_MEMORY
                    FT_Done_FreeType( library );
#endif
                    return 0;
                }
                FT_Stroker_Set(stroker, (int)(pSelf->m_fOutlineThickness * 64), FT_STROKER_LINECAP_ROUND, FT_STROKER_LINEJOIN_ROUND, 0);

                error = FT_Get_Glyph(face->glyph, &ft_glyph);
                if (error)
                {
                    BE_FONT_ERROR("FT_Error (0x%02x) : %s\n", FT_Errors[error].code, FT_Errors[error].message);

                    FT_Done_Glyph (ft_glyph);

#if USE_LIB_MEMORY
                    FT_Done_Face( face );
#endif
                    FT_Stroker_Done(stroker);
#if USE_LIB_MEMORY
                    FT_Done_FreeType( library );
#endif
                    return 0;
                }

                if (pSelf->m_iOutlineType == 1)
                {
                    error = FT_Glyph_Stroke(&ft_glyph, stroker, 1);
                }
                else if (pSelf->m_iOutlineType == 2)
                {
                    error = FT_Glyph_StrokeBorder(&ft_glyph, stroker, 0, 1);
                }
                else if (pSelf->m_iOutlineType == 3)
                {
                    error = FT_Glyph_StrokeBorder(&ft_glyph, stroker, 1, 1);
                }
                if (error)
                {
                    BE_FONT_ERROR("FT_Error (0x%02x) : %s\n", FT_Errors[error].code, FT_Errors[error].message);

                    FT_Done_Glyph(ft_glyph);

#if USE_LIB_MEMORY
                    FT_Done_Face( face );
#endif
                    FT_Stroker_Done(stroker);
#if USE_LIB_MEMORY
                    FT_Done_FreeType( library );
#endif
                    return 0;
                }

                error = FT_Glyph_To_Bitmap(&ft_glyph, FT_RENDER_MODE_NORMAL, 0, 1);
                if (error)
                {
                    BE_FONT_ERROR("FT_Error (0x%02x) : %s\n", FT_Errors[error].code, FT_Errors[error].message);

                    FT_Done_Glyph(ft_glyph);

#if USE_LIB_MEMORY
                    FT_Done_Face( face );
#endif
                    FT_Stroker_Done(stroker);
#if USE_LIB_MEMORY
                    FT_Done_FreeType( library );
#endif
                    return 0;
                }

                ft_bitmap_glyph = (FT_BitmapGlyph)ft_glyph;
                ft_bitmap = ft_bitmap_glyph->bitmap;
                ft_bitmap_width = ft_bitmap.width;
                ft_bitmap_rows = ft_bitmap.rows;
                ft_bitmap_pitch = ft_bitmap.pitch;
                ft_glyph_top = ft_bitmap_glyph->top;
                ft_glyph_left = ft_bitmap_glyph->left;
                FT_Stroker_Done(stroker);
            }

            w = ft_bitmap_width + 1;
            h = ft_bitmap_rows + 1;

            if (iTotalGeneratingPhases == 1 || (iTotalGeneratingPhases > 1 && glyphGenerationPhase == 1))
            {
                region = BeFTRTextureAtlas::TextureAtlasGetRegion(pSelf->m_pAtlas, w, h);
                if (region.m_fX < 0)
                {
                    FT_Done_Glyph(ft_glyph);

                    missed++;

                    BE_FONT_ERROR("Texture atlas is full post-get-region (line %d)\n", __LINE__);

                    bStopProcessingGlyphs = true;
                    break;
                }

                x = region.m_fX;
                y = region.m_fY;

                x_MAX = x;
                y_MAX = y;
                ft_glyph_left_MAX = ft_glyph_left;
                ft_glyph_top_MAX = ft_glyph_top;

                w = w - 1;
                h = h - 1;

                biggestW = w;
                biggestH = h;
            }
            else
            {
                w = w - 1;
                h = h - 1;
            }

            if (ft_bitmap.buffer != NULL)
            {
                int offsetX = 0;
                int offsetY = 0;
                if (glyphGenerationPhase == 0)
                {
                    offsetX = floor(((biggestW - w) / 2) + 0.5f);
                    offsetY = floor(((biggestH - h) / 2) + 0.5f);
                }

#if TARGET_WINDOWS
                int iDestPhaseBuffer = 2 - glyphGenerationPhase;
#else
                int iDestPhaseBuffer = glyphGenerationPhase;
#endif
                BeFTRTextureAtlas::TextureAtlasSetRegion(pSelf->m_pAtlas, x + offsetX, y + offsetY, w, h, ft_bitmap.buffer, ft_bitmap.pitch, iDestPhaseBuffer);
            }
        }

        if (bStopProcessingGlyphs)
            continue;

        glyph = TextureGlyphNew();
        glyph->m_wCharcode = pCharcodes[i];
        glyph->m_iWidth = biggestW;
        glyph->m_iHeight = biggestH;
        glyph->m_iOutlineType = pSelf->m_iOutlineType;
        glyph->m_fOutlineThickness = pSelf->m_fOutlineThickness;
        glyph->m_iOffsetX = ft_glyph_left_MAX;
        glyph->m_iOffsetY = ft_glyph_top_MAX;
        glyph->m_fS0 = x_MAX/(float)width;
        glyph->m_fT0 = y_MAX / (float)height;
        glyph->m_fS1 = (x_MAX + glyph->m_iWidth) / (float)width;
        glyph->m_fT1 = (y_MAX + glyph->m_iHeight) / (float)height;

        // Discard hinting to get advance
#if USE_LIB_MEMORY
        FT_Load_Glyph( face, glyph_index, FT_LOAD_RENDER | FT_LOAD_NO_HINTING);
#endif
        slot = face->glyph;
        glyph->m_fAdvanceX = slot->advance.x/64.0;
        glyph->m_fAdvanceY = slot->advance.y / 64.0;

        BeFTRVector::VectorPushBack(pSelf->m_pGlyphs, &glyph);

        if (bHasGeneratedStrokedGlyphs)
        {
            FT_Done_Glyph( ft_glyph );
        }
    }

#if USE_LIB_MEMORY
    FT_Done_Face( face );
    FT_Done_FreeType( library );
#endif

    BeFTRTextureAtlas::TextureAtlasUpload(pSelf->m_pAtlas);

    TextureFontGenerateKerning(pSelf);

    return missed;
}
/////////////////////////////////////////////////////////////////////////////
BeFTRTextureFont::BeFTRTextureFontGlyph* BeFTRTextureFont::TextureFontGetGlyph(BeFTRTextureFont::BeFTRTextureFontData* pSelf, BeWChar wCharcode, BeBoolean bAvoidToLoadIfItDoesNotExist)
{
    size_t i;
    wchar_t buffer[2] = { 0, 0 };
    BeFTRTextureFont::BeFTRTextureFontGlyph* pGlyph;

    BE_ASSERT(pSelf);
    BE_ASSERT(pSelf->m_pFilename);
    BE_ASSERT(pSelf->m_pAtlas);

    /* Check if charcode has been already loaded */
    for (i = 0; i<pSelf->m_pGlyphs->m_iSize; ++i)
    {
		pGlyph = *(BeFTRTextureFont::BeFTRTextureFontGlyph**)BeFTRVector::VectorGet(pSelf->m_pGlyphs, i);

        // If charcode is -1, we don't care about outline type or thickness
        if ((pGlyph->m_wCharcode == wCharcode) &&
            ((wCharcode == (wchar_t)(-1)) ||
            ((pGlyph->m_iOutlineType == pSelf->m_iOutlineType) &&
            (pGlyph->m_fOutlineThickness == pSelf->m_fOutlineThickness))))
        {
            return pGlyph;
        }
    }

    /* charcode -1 is special : it is used for line drawing (overline,
     * underline, strikethrough) and background.
     */
    if (wCharcode == (wchar_t)(-1))
    {
        size_t width = pSelf->m_pAtlas->m_iWidth;
        size_t height = pSelf->m_pAtlas->m_iHeight;
        BeVector4D region = BeFTRTextureAtlas::TextureAtlasGetRegion(pSelf->m_pAtlas, 5, 5);

        pGlyph = TextureGlyphNew();

        static BeUChar8 data[4 * 4 * 3];
        for (BeInt iFillIdx = 0; iFillIdx < 4 * 4 * 3; ++iFillIdx)
        {
            data[iFillIdx] = (BeUChar8) -1;
        }

        if (region.m_fX < 0)
        {
            BE_FONT_ERROR("Texture atlas is full (line %d)\n", __LINE__);
            return NULL;
        }

        BeFTRTextureAtlas::TextureAtlasSetRegion(pSelf->m_pAtlas, region.m_fX, region.m_fY, 4, 4, data, 0);
		pGlyph->m_wCharcode = (wchar_t)(-1);
		pGlyph->m_fS0 = (region.m_fX+2)/(float)width;
		pGlyph->m_fT0 = (region.m_fY + 2) / (float)height;
		pGlyph->m_fS1 = (region.m_fX + 3) / (float)width;
		pGlyph->m_fT1 = (region.m_fY + 3) / (float)height;

        BeFTRVector::VectorPushBack(pSelf->m_pGlyphs, &pGlyph);

        return pGlyph; //*(texture_glyph_t **) vector_back( pSelf->m_pGlyphs );
    }

    if (!bAvoidToLoadIfItDoesNotExist)
    {
        /* Glyph has not been already loaded */
        buffer[0] = wCharcode;
        if (TextureFontLoadGlyphs(pSelf, buffer) == 0)
        {
            return *(BeFTRTextureFont::BeFTRTextureFontGlyph**)BeFTRVector::VectorBack(pSelf->m_pGlyphs);
        }
    }

    return NULL;
}
/////////////////////////////////////////////////////////////////////////////
