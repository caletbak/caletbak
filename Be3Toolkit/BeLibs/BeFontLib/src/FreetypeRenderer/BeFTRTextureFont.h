/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeFTRTextureFont.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_FTRTEXTUREFONT_H
#define BE_FTRTEXTUREFONT_H

#include <BeFontLib/pch/BeFontLibPredef.h>

#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_STROKER_H
// #include FT_ADVANCES_H
#include FT_LCD_FILTER_H

#include <FreetypeRenderer/BeFTRVector.h>
#include <FreetypeRenderer/BeFTRTextureAtlas.h>

class FONT_API BeFTRTextureFont
{
public:

    /// \brief  A structure that hold a kerning value relatively to a charcode
    struct BeFTRTextureFontKerning
    {
        /// \brief  Left character code in the kern pair
        BeWChar m_wCharcode;

        /// \brief  Kerning value (in fractional pixels)
        BeFloat m_fKerning;
    };

    /*
     * Glyph metrics:
     * --------------
     *
     *                       xmin                     xmax
     *                        |                         |
     *                        |<-------- width -------->|
     *                        |                         |
     *              |         +-------------------------+----------------- ymax
     *              |         |    ggggggggg   ggggg    |     ^        ^
     *              |         |   g:::::::::ggg::::g    |     |        |
     *              |         |  g:::::::::::::::::g    |     |        |
     *              |         | g::::::ggggg::::::gg    |     |        |
     *              |         | g:::::g     g:::::g     |     |        |
     *    offset_x -|-------->| g:::::g     g:::::g     |  offset_y    |
     *              |         | g:::::g     g:::::g     |     |        |
     *              |         | g::::::g    g:::::g     |     |        |
     *              |         | g:::::::ggggg:::::g     |     |        |
     *              |         |  g::::::::::::::::g     |     |      height
     *              |         |   gg::::::::::::::g     |     |        |
     *  baseline ---*---------|---- gggggggg::::::g-----*--------      |
     *            / |         |             g:::::g     |              |
     *     origin   |         | gggggg      g:::::g     |              |
     *              |         | g:::::gg   gg:::::g     |              |
     *              |         |  g::::::ggg:::::::g     |              |
     *              |         |   gg:::::::::::::g      |              |
     *              |         |     ggg::::::ggg        |              |
     *              |         |         gggggg          |              v
     *              |         +-------------------------+----------------- ymin
     *              |                                   |
     *              |------------- advance_x ---------->|
     */

    /// \brief  A structure that describe a glyph
    struct BeFTRTextureFontGlyph
    {
        /// \brief  Wide character this glyph represents
        BeWChar m_wCharcode;

        /// \brief  Glyph id (used for display lists)
        BeUInt m_uId;

        /// \brief  Glyph's width in pixels
        BeSize_T m_iWidth;

        /// \brief  Glyph's height in pixels
        BeSize_T m_iHeight;

        /// \brief  Glyph's left bearing expressed in integer pixels
        BeInt m_iOffsetX;

        /// \brief  Glyphs's top bearing expressed in integer pixels
        BeInt m_iOffsetY;

        /// \brief  For horizontal text layouts, this is the horizontal distance (in fractional pixels) used to increment the pen position when the glyph is drawn as part of a string of text
        BeFloat m_fAdvanceX;

        /// \brief  For vertical text layouts, this is the vertical distance (in fractional pixels) used to increment the pen position when the glyph is drawn as part of a string of text
        BeFloat m_fAdvanceY;

        /// \brief  First normalized texture coordinate (x) of top-left corner
        BeFloat m_fS0;

        /// \brief  Second normalized texture coordinate (y) of top-left corner
        BeFloat m_fT0;

        /// \brief  First normalized texture coordinate (x) of bottom-right corner
        BeFloat m_fS1;

        /// \brief  Second normalized texture coordinate (y) of bottom-right corner
        BeFloat m_fT1;

        /// \brief  A vector of kerning pairs relative to this glyph
        BeFTRVector::BeFTRVectorData* m_pKerning;

        /// \brief  Glyph outline type (0 = None, 1 = line, 2 = inner, 3 = outer)
        BeInt m_iOutlineType;

        /// \brief  Glyph outline thickness
        BeFloat m_fOutlineThickness;
    };



    /// \brief  Texture font structure
    struct BeFTRTextureFontData
    {
        /// \brief  Vector of glyphs contained in this font.
        BeFTRVector::BeFTRVectorData* m_pGlyphs;

        /// \brief  Atlas structure to store glyphs data
        BeFTRTextureAtlas::BeFTRTextureAtlasData* m_pAtlas;

        /// \brief  Font filename
        BeChar8* m_pFilename;

        /// \brief  Font size
        BeFloat m_fSize;

        /// \brief  Whether to use autohint when rendering font
        BeInt m_iHinting;

        /// \brief  Outline type (0 = None, 1 = line, 2 = inner, 3 = outer)
        BeInt m_iOutlineType;

        /// \brief  Outline thickness
        BeFloat m_fOutlineThickness;

        /// \brief  Whether to use our own lcd filter
        BeInt m_iFiltering;

        /// \brief  Whether to use kerning if available
        BeInt m_iKerning;

        /// \brief  LCD filter weights
        BeUChar8 m_cLcdWeights[5];

        /// \brief  This field is simply used to compute a default line spacing (i.e., the baseline-to-baseline distance) when writing text with this font. Note that it usually is larger than the sum of the ascender and descender taken as absolute values. There is also no guarantee that no glyphs extend above or below subsequent baselines when using this distance
        BeFloat m_fHeight;

        /// \brief  This field is the distance that must be placed between two lines of text.The baseline - to - baseline distance should be computed as: ascender - descender + linegap
        BeFloat m_fLinegap;

        /// \brief  The ascender is the vertical distance from the horizontal baseline to the highest 'character' coordinate in a font face. Unfortunately, font formats define the ascender differently. For some, it represents the ascent of all capital latin characters (without accents), for others it is the ascent of the highest accented character, and finally, other formats define it as being equal to bbox
        BeFloat m_fAscender;

        /// \brief  The descender is the vertical distance from the horizontal baseline to the lowest 'character' coordinate in a font face. Unfortunately, font formats define the descender differently. For some, it represents the descent of all capital latin characters (without accents), for others it is the ascent of the lowest accented character, and finally, other formats define it as being equal to bbox.yMin. This field is negative for values below the baseline
        BeFloat m_fDescender;

        /// \brief  The position of the underline line for this face. It is the center of the underlining stem.Only relevant for scalable formats
        BeFloat m_fUnderLinePosition;

        /// \brief  The thickness of the underline for this face. Only relevant for scalable formats
        BeFloat m_fUnderlineThickness;

        FT_Encoding m_pFontEncoding;
        FT_Library m_pLibrary;
        FT_Face m_pFace;
    };



    /// \brief  This function creates a new texture font from given filename and size.  The texture atlas is used to store glyph on demand.Note the depth of the atlas will determine if the font is rendered as alpha channel only(depth = 1) or RGB(depth = 3) that correspond to subpixel rendering(if available on your freetype implementation)
    static BeFTRTextureFontData* TextureFontNew(BeFTRTextureAtlas::BeFTRTextureAtlasData* pAtlas, const BeChar8* pFilename, const BeFloat fSize, const BeFloat fStrokeSize, FT_Encoding kEncoding = FT_ENCODING_UNICODE);

    /// \brief  Delete a texture font. Note that this does not delete the glyph from the texture atlas
    static void TextureFontDelete(BeFTRTextureFontData* pSelf);

    /// \brief  Closes a texture font. Note that this does not delete the glyph from the texture atlas
    static void TextureFontClose(BeFTRTextureFontData* pSelf);



    /// \brief  Loads a texture font face
    static BeInt TextureFontLoadFace(FT_Library* pLibrary, const BeChar8* pFilename, const BeFloat size, FT_Face* pFace, FT_Encoding kEncoding);



    /// \brief  Creates a new empty glyph
    static BeFTRTextureFontGlyph* TextureGlyphNew(void);

    /// \brief  Deletes a glyph
    static void TextureGlyphDelete(BeFTRTextureFontGlyph* pSelf);

    /// \brief  Get the kerning between two horizontal glyphs
    static BeFloat TextureGlyphGetKerning(const BeFTRTextureFontGlyph* pSelf, const BeWChar wCharcode);



    /// \brief  Generates the font kerning
    static void TextureFontGenerateKerning(BeFTRTextureFontData* pSelf);



    /// \brief  Request the loading of several glyphs at once
    static BeSize_T TextureFontLoadGlyphs(BeFTRTextureFontData* pSelf, const BeWChar* pCharcodes, BeInt iArraySize = -1, BeInt iPreW = -1, BeInt iPreH = -1);

    /// \brief  Request a new glyph from the font. If it has not been created yet, it will be
    static BeFTRTextureFontGlyph* TextureFontGetGlyph(BeFTRTextureFontData* pSelf, BeWChar wCharcode, BeBoolean bAvoidToLoadIfItDoesNotExist = BE_FALSE);
};

#endif // BE_FTRTEXTUREFONT_H

