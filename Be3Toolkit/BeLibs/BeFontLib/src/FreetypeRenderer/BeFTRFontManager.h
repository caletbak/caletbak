/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeFTRFontManager.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_FTRFONTMANAGER_H
#define BE_FTRFONTMANAGER_H

#include <BeFontLib/pch/BeFontLibPredef.h>

#include <FreetypeRenderer/BeFTRTextureFont.h>
#include <FreetypeRenderer/BeFTRTextureAtlas.h>
#include <FreetypeRenderer/BeFTRVector.h>
#include <FreetypeRenderer/BeFTRMarkup.h>

class FONT_API BeFTRFontManager
{
public:

    /// \brief  Structure in charge of caching fonts
    struct BeFTRFontManagerData
    {
        /// \brief  Texture atlas to hold font glyphs
        BeFTRTextureAtlas::BeFTRTextureAtlasData* m_pAtlas;

        /// \brief  Cached Textures
        BeFTRVector::BeFTRVectorData* m_pFonts;

        /// \brief  Default glyphs to be loaded when loading a new font
        BeWChar* m_pCache;
    };



    /// \brief  Creates a new empty font manager
    static BeFTRFontManagerData* FontManagerNew(BeSize_T iWidth, BeSize_T iHeight, BeSize_T iDepth);

    /// \brief  Deletes a font manager
    static void FontManagerDelete(BeFTRFontManagerData* pSelf);

    /// \brief  Deletes a font from the font manager
    static void FontManagerDeleteFont(BeFTRFontManagerData* pSelf, BeFTRTextureFont::BeFTRTextureFontData* pFont);



    /// \brief  Request for a font based on a filename
    static BeFTRTextureFont::BeFTRTextureFontData* FontManagerGetFromFilename(BeFTRFontManagerData* pSelf, const BeChar8* pFilename, const BeFloat fSize);

    /// \brief  Request for a font based on a description
    static BeFTRTextureFont::BeFTRTextureFontData* FontManagerGetFromDescription(BeFTRFontManagerData* pSelf, const BeChar8* pFamily, const BeFloat fSize, const BeInt iBold, const BeInt iItalic);

    /// \brief  Request for a font based on a markup
    static BeFTRTextureFont::BeFTRTextureFontData* FontManagerGetFromMarkup(BeFTRFontManagerData* pSelf, const BeFTRMarkup::BeFTRMarkupData* pMarkup);



    /// \brief  Search for a font filename that match description
    static BeChar8* FontManagerMatchDescription(BeFTRFontManagerData* pSelf, const BeChar8* pFamily, const BeFloat fSize, const BeInt iBold, const BeInt iItalic);

};

#endif // BE_FTRFONTMANAGER_H

