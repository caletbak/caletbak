/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeFTRVertexBuffer.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <BeFontLib/pch/BeFontLibPredef.h>

#include <FreetypeRenderer/BeFTRVertexBuffer.h>
#include <FreetypeRenderer/BeFTRPlatform.h>

// Buffer status
#define CLEAN  (0)
#define DIRTY  (1)
#define FROZEN (2)

/////////////////////////////////////////////////////////////////////////////
BeFTRVertexBuffer::BeFTRVertexBufferData* BeFTRVertexBuffer::VertexBufferNew(const BeChar8* pFormat)
{
    BE_FONT_DEBUG("VertexBufferNew: %s\n", pFormat);

    size_t i, index = 0, stride = 0;
    const char *start = 0, *end = 0;
    BeChar8* pointer = 0;

    BeFTRVertexBuffer::BeFTRVertexBufferData*self = (BeFTRVertexBuffer::BeFTRVertexBufferData*)malloc(sizeof(BeFTRVertexBuffer::BeFTRVertexBufferData));
    if( !self )
    {
        return NULL;
    }

    self->m_pFormat = strdup(pFormat);

    for( i=0; i<MAX_VERTEX_ATTRIBUTE; ++i )
    {
        self->m_kAttributes[i] = 0;
    }

    start = pFormat;
    do
    {
        char *desc = 0;
        BeFTRVertexAttribute::BeFTRVertexAttributeData* attribute;

        BeUInt attribute_size = 0;
        end = (char *) (strchr(start+1, ','));

        if (end == NULL)
        {
            desc = strdup( start );
        }
        else
        {
            desc = strndup( start, end-start );
        }

        attribute = BeFTRVertexAttribute::VertexAttributeParse(desc);
        start = end+1;
        free(desc);
        attribute->m_pPointer = pointer;

        switch( attribute->m_iType )
        {
            case BeCoreTypes::BE_BOOLEAN:               attribute_size = sizeof(BeBoolean); break;
            case BeCoreTypes::BE_CHAR8:                 attribute_size = sizeof(BeChar8); break;
            case BeCoreTypes::BE_UNSIGNED_CHAR8:        attribute_size = sizeof(BeUChar8); break;
            case BeCoreTypes::BE_SHORT:                 attribute_size = sizeof(BeShort); break;
            case BeCoreTypes::BE_UNSIGNED_SHORT:        attribute_size = sizeof(BeUShort); break;
            case BeCoreTypes::BE_INT:                   attribute_size = sizeof(BeInt); break;
            case BeCoreTypes::BE_UNSIGNED_INT:          attribute_size = sizeof(BeUInt); break;
            case BeCoreTypes::BE_FLOAT:                 attribute_size = sizeof(BeFloat); break;
            default:                                    attribute_size = 0;
        }

        stride  += attribute->m_iSize * attribute_size;
        pointer += attribute->m_iSize * attribute_size;

        self->m_kAttributes[index] = attribute;

        index++;
    }
    while ( end && (index < MAX_VERTEX_ATTRIBUTE) );

    for( i=0; i<index; ++i )
    {
        self->m_kAttributes[i]->m_iStride = stride;
    }

    self->m_pVertices = BeFTRVector::VectorNew(stride);
    //self->vertices_id  = 0;
    //self->GPU_vsize = 0;

    self->m_pIndices = BeFTRVector::VectorNew(sizeof(BeUShort));
    //self->indices_id  = 0;
    //self->GPU_isize = 0;

    self->m_pItems = BeFTRVector::VectorNew(sizeof(BeVector4D));
    self->m_cState = DIRTY;
    //self->mode = GL_TRIANGLES;

    return self;
}
/////////////////////////////////////////////////////////////////////////////
void BeFTRVertexBuffer::VertexBufferDelete(BeFTRVertexBuffer::BeFTRVertexBufferData* pSelf)
{
    size_t i;

    BE_ASSERT(pSelf);

    for( i=0; i<MAX_VERTEX_ATTRIBUTE; ++i )
    {
        if (pSelf->m_kAttributes[i])
        {
            BeFTRVertexAttribute::VertexAttributeDelete(pSelf->m_kAttributes[i]);
        }
    }

    BeFTRVector::VectorDelete(pSelf->m_pVertices);
    pSelf->m_pVertices = 0;
    //if (pSelf->vertices_id)
    //{
    //    glDeleteBuffers(1, &pSelf->vertices_id);
    //}
    //pSelf->vertices_id = 0;

    BeFTRVector::VectorDelete(pSelf->m_pIndices);
    pSelf->m_pIndices = 0;
    //if (pSelf->indices_id)
    //{
    //    glDeleteBuffers(1, (GLuint*)&pSelf->indices_id);
    //}
    //pSelf->indices_id = 0;

    BeFTRVector::VectorDelete(pSelf->m_pItems);

    if (pSelf->m_pFormat)
    {
        free(pSelf->m_pFormat);
    }

    pSelf->m_pFormat = 0;
    pSelf->m_cState = 0;

    free(pSelf);
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeSize_T BeFTRVertexBuffer::VertexBufferSize(const BeFTRVertexBuffer::BeFTRVertexBufferData* pSelf)
{
    BE_ASSERT(pSelf);

    return BeFTRVector::VectorSize(pSelf->m_pItems);
}
/////////////////////////////////////////////////////////////////////////////
const BeChar8* BeFTRVertexBuffer::VertexBufferFormat(const BeFTRVertexBuffer::BeFTRVertexBufferData* pSelf)
{
    BE_ASSERT(pSelf);

    return pSelf->m_pFormat;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeFTRVertexBuffer::VertexBufferPrint(BeFTRVertexBuffer::BeFTRVertexBufferData * pSelf)
{
    int i = 0;

    BE_ASSERT(pSelf);

    BE_FONT_ERROR("%ld vertices, %ld indices\n", (long)BeFTRVector::VectorSize(pSelf->m_pVertices), (long)BeFTRVector::VectorSize(pSelf->m_pIndices));
    while (pSelf->m_kAttributes[i])
    {
        BE_FONT_ERROR("%s : %dx%s (+%p)\n",
                pSelf->m_kAttributes[i]->m_pName,
                pSelf->m_kAttributes[i]->m_iSize,
                BeCoreTypes::GetTypeNameString((BeCoreTypes::ETypeNames)pSelf->m_kAttributes[i]->m_iType).ToRaw(),
                pSelf->m_kAttributes[i]->m_pPointer);

        i += 1;
    }
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeFTRVertexBuffer::VertexBufferRenderSetup(BeFTRVertexBuffer::BeFTRVertexBufferData* pSelf, BeInt iMode)
{
    size_t i;

    if (pSelf->m_cState != CLEAN)
    {
        VertexBufferUpload(pSelf);
        pSelf->m_cState = CLEAN;
    }
    
    //glBindBuffer(GL_ARRAY_BUFFER, pSelf->vertices_id);

    for( i=0; i<MAX_VERTEX_ATTRIBUTE; ++i )
    {
        BeFTRVertexAttribute::BeFTRVertexAttributeData* attribute = pSelf->m_kAttributes[i];
        if ( attribute == 0 )
        {
            continue;
        }
        else
        {
            BeFTRVertexAttribute::VertexAttributeEnable(attribute);
        }
    }

    if (pSelf->m_pIndices->m_iSize)
    {
        //glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, pSelf->indices_id);
    }
    //pSelf->mode = mode;
}
/////////////////////////////////////////////////////////////////////////////
void BeFTRVertexBuffer::VertexBufferRenderFinish(BeFTRVertexBuffer::BeFTRVertexBufferData* pSelf)
{
    //glBindBuffer( GL_ARRAY_BUFFER, 0 );
    //glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
}
/////////////////////////////////////////////////////////////////////////////
void BeFTRVertexBuffer::VertexBufferRender(BeFTRVertexBuffer::BeFTRVertexBufferData* pSelf, BeInt iMode)
{
    size_t vcount = pSelf->m_pVertices->m_iSize;
    size_t icount = pSelf->m_pIndices->m_iSize;

    VertexBufferRenderSetup(pSelf, iMode);
    if (icount)
    {
        //glDrawElements(iMode, icount, GL_UNSIGNED_SHORT, 0);
    }
    else
    {
        //glDrawArrays(iMode, 0, vcount);
    }

    VertexBufferRenderFinish(pSelf);
}
/////////////////////////////////////////////////////////////////////////////
void BeFTRVertexBuffer::VertexBufferRenderItem(BeFTRVertexBuffer::BeFTRVertexBufferData* pSelf, BeSize_T iIndex)
{ 
    BeVector4D* item = (BeVector4D*)BeFTRVector::VectorGet(pSelf->m_pItems, iIndex);

    BE_ASSERT(pSelf);
    BE_ASSERT(iIndex < BeFTRVector::VectorSize(pSelf->m_pItems));

    if (pSelf->m_pIndices->m_iSize)
    {
        BeSize_T start = item->m_fZ;
        BeSize_T count = item->m_fW;

        //glDrawElements(pSelf->mode, count, GL_UNSIGNED_SHORT, (void *)(start*sizeof(BeUShort)));
    }
    else if (pSelf->m_pVertices->m_iSize)
    {
        BeSize_T start = item->m_fX;
        BeSize_T count = item->m_fY;

        //glDrawArrays(pSelf->mode, start*pSelf->vertices->item_size, count);
    }
}
/////////////////////////////////////////////////////////////////////////////
void BeFTRVertexBuffer::VertexBufferUpload(BeFTRVertexBuffer::BeFTRVertexBufferData* pSelf)
{
    BeSize_T vsize, isize;

    if (pSelf->m_cState == FROZEN)
    {
        return;
    }

    vsize = pSelf->m_pVertices->m_iSize * pSelf->m_pVertices->m_iItemSize;
    isize = pSelf->m_pIndices->m_iSize * pSelf->m_pIndices->m_iItemSize;

    //if (!pSelf->vertices_id)
    //{
        //glGenBuffers(1, &pSelf->vertices_id);
    //}
    //if (!self->indices_id)
    //{
        //glGenBuffers(1, (GLuint*)&pSelf->indices_id);
    //}

    // Always upload vertices first such that indices do not point to non
    // existing data (if we get interrupted in between for example).

    // Upload vertices
    /*
    glBindBuffer(GL_ARRAY_BUFFER, self->vertices_id);
    if (vsize != self->GPU_vsize)
    {
        glBufferData(GL_ARRAY_BUFFER,
            vsize, self->vertices->items, GL_DYNAMIC_DRAW);
        self->GPU_vsize = vsize;
    }
    else
    {
        glBufferSubData(GL_ARRAY_BUFFER,
            0, vsize, self->vertices->items);
    }
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // Upload indices
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, self->indices_id);
    if (isize != self->GPU_isize)
    {
        glBufferData(GL_ELEMENT_ARRAY_BUFFER,
            isize, self->indices->items, GL_DYNAMIC_DRAW);
        self->GPU_isize = isize;
    }
    else
    {
        glBufferSubData(GL_ELEMENT_ARRAY_BUFFER,
            0, isize, self->indices->items);
    }
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    */
}
/////////////////////////////////////////////////////////////////////////////
void BeFTRVertexBuffer::VertexBufferClear(BeFTRVertexBuffer::BeFTRVertexBufferData* pSelf)
{
    BE_ASSERT(pSelf);

    pSelf->m_cState = FROZEN;
    BeFTRVector::VectorClear(pSelf->m_pIndices);
    BeFTRVector::VectorClear(pSelf->m_pVertices);
    BeFTRVector::VectorClear(pSelf->m_pItems);
    pSelf->m_cState = DIRTY;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeFTRVertexBuffer::VertexBufferPushBackVertices(BeFTRVertexBuffer::BeFTRVertexBufferData* pSelf, const BeVoidP pVertices, const BeSize_T iVCount)
{
    BE_ASSERT(pSelf);

    pSelf->m_cState |= DIRTY;
    BeFTRVector::VectorPushBackData(pSelf->m_pVertices, pVertices, iVCount);
}
/////////////////////////////////////////////////////////////////////////////
void BeFTRVertexBuffer::VertexBufferInsertVertices(BeFTRVertexBuffer::BeFTRVertexBufferData* pSelf, const BeSize_T iIndex, const BeVoidP pVertices, const BeSize_T iVCount)
{
    size_t i;
    BE_ASSERT(pSelf);
    BE_ASSERT(pSelf->m_pVertices);
    BE_ASSERT(iIndex < pSelf->m_pVertices->m_iSize + 1);

    pSelf->m_cState |= DIRTY;

    for (i = 0; i < pSelf->m_pIndices->m_iSize; ++i)
    {
        if (*(BeUShort*)(BeFTRVector::VectorGet(pSelf->m_pIndices, i)) > iIndex)
        {
            *(BeUShort*)(BeFTRVector::VectorGet(pSelf->m_pIndices, i)) += ((BeUShort)iIndex);
        }
    }

    BeFTRVector::VectorInsertData(pSelf->m_pVertices, iIndex, pVertices, iVCount);
}
/////////////////////////////////////////////////////////////////////////////
void BeFTRVertexBuffer::VertexBufferEraseVertices(BeFTRVertexBuffer::BeFTRVertexBufferData* pSelf, const BeSize_T iFirst, const BeSize_T iLast)
{
    size_t i;
    BE_ASSERT(pSelf);
    BE_ASSERT(pSelf->m_pVertices);
    BE_ASSERT(iFirst < pSelf->m_pVertices->m_iSize);
    BE_ASSERT((iFirst + iLast) <= pSelf->m_pVertices->m_iSize);
    BE_ASSERT(iLast > iFirst);

    pSelf->m_cState |= DIRTY;
    for (i = 0; i<pSelf->m_pIndices->m_iSize; ++i)
    {
        if (*(BeUShort *)(BeFTRVector::VectorGet(pSelf->m_pIndices, i)) > iFirst)
        {
            *(BeUShort *)(BeFTRVector::VectorGet(pSelf->m_pIndices, i)) -= ((BeUShort)(iLast - iFirst));
        }
    }
    BeFTRVector::VectorEraseRange(pSelf->m_pVertices, iFirst, iLast);
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeFTRVertexBuffer::VertexBufferPushBackIndices(BeFTRVertexBuffer::BeFTRVertexBufferData* pSelf, const BeUShort* pIndices, const BeSize_T iIcount)
{
    BE_ASSERT(pSelf);

    pSelf->m_cState |= DIRTY;
    BeFTRVector::VectorPushBackData(pSelf->m_pIndices, pIndices, iIcount);
}
/////////////////////////////////////////////////////////////////////////////
void BeFTRVertexBuffer::VertexBufferInsertIndices(BeFTRVertexBuffer::BeFTRVertexBufferData* pSelf, const BeSize_T iIndex, const BeUShort* pIndices, const BeSize_T iICount)
{
    BE_ASSERT(pSelf);
    BE_ASSERT(pSelf->m_pIndices);
    BE_ASSERT(iIndex < pSelf->m_pIndices->m_iSize + 1);

    pSelf->m_cState |= DIRTY;
    BeFTRVector::VectorInsertData(pSelf->m_pIndices, iIndex, pIndices, iICount);
}
/////////////////////////////////////////////////////////////////////////////
void BeFTRVertexBuffer::VertexBufferEraseIndices(BeFTRVertexBuffer::BeFTRVertexBufferData* pSelf, const BeSize_T iFirst, const BeSize_T iLast)
{
    BE_ASSERT(pSelf);
    BE_ASSERT(pSelf->m_pIndices);
    BE_ASSERT(iFirst < pSelf->m_pIndices->m_iSize);
    BE_ASSERT((iLast) <= pSelf->m_pIndices->m_iSize);

    pSelf->m_cState |= DIRTY;
    BeFTRVector::VectorEraseRange(pSelf->m_pIndices, iFirst, iLast);
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeSize_T BeFTRVertexBuffer::VertexBufferPushBack(BeFTRVertexBuffer::BeFTRVertexBufferData* pSelf, const BeVoidP pVertices, const BeSize_T iVCount, const BeUShort* pIndices, const BeSize_T iICount)
{
    return VertexBufferInsert(pSelf, BeFTRVector::VectorSize(pSelf->m_pItems), pVertices, iVCount, pIndices, iICount);
}
/////////////////////////////////////////////////////////////////////////////
BeSize_T BeFTRVertexBuffer::VertexBufferInsert(BeFTRVertexBuffer::BeFTRVertexBufferData* pSelf, BeSize_T iIndex, const BeVoidP pVertices, const BeSize_T iVCount, const BeUShort* pIndices, const BeSize_T iICount)
{
    size_t vstart, istart, i;
    BeVector4D item;
    BE_ASSERT(pSelf);
    BE_ASSERT(pVertices);
    BE_ASSERT(pIndices);

    pSelf->m_cState = FROZEN;

    // Push back vertices
    vstart = BeFTRVector::VectorSize(pSelf->m_pVertices);
    VertexBufferPushBackVertices(pSelf, pVertices, iVCount);

    // Push back indices
    istart = BeFTRVector::VectorSize(pSelf->m_pIndices);
    VertexBufferPushBackIndices(pSelf, pIndices, iICount);

    // Update indices within the vertex buffer
    for (i = 0; i < iICount; ++i)
    {
        *(BeUShort*)(BeFTRVector::VectorGet(pSelf->m_pIndices, istart + i)) += ((BeUShort)vstart);
    }
    
    // Insert item
    item.m_fX = vstart;
    item.m_fY = iVCount;
    item.m_fZ = istart;
    item.m_fW = iICount;
    BeFTRVector::VectorInsert(pSelf->m_pItems, iIndex, &item);

    pSelf->m_cState = DIRTY;
    return iIndex;
}
/////////////////////////////////////////////////////////////////////////////
void BeFTRVertexBuffer::VertexBufferErase(BeFTRVertexBuffer::BeFTRVertexBufferData* pSelf, const BeSize_T iIndex)
{
    BeVector4D* item;
    BeSize_T vstart, vcount, istart, icount, i;

    BE_ASSERT(pSelf);
    BE_ASSERT(iIndex < BeFTRVector::VectorSize(pSelf->m_pItems));

    item = (BeVector4D*)BeFTRVector::VectorGet(pSelf->m_pItems, iIndex);
    vstart = item->m_fX;
    vcount = item->m_fY;
    istart = item->m_fZ;
    icount = item->m_fW;

    // Update items
    for (i = 0; i<BeFTRVector::VectorSize(pSelf->m_pItems); ++i)
    {
        item = (BeVector4D*)BeFTRVector::VectorGet(pSelf->m_pItems, i);
        if( item->m_fX > vstart)
        {
            item->m_fX -= vcount;
            item->m_fZ -= icount;
        }
    }

    pSelf->m_cState = FROZEN;
    VertexBufferEraseIndices(pSelf, istart, istart + icount);
    VertexBufferEraseVertices(pSelf, vstart, vstart + vcount);
    BeFTRVector::VectorErase(pSelf->m_pItems, iIndex);
    pSelf->m_cState = DIRTY;
}
/////////////////////////////////////////////////////////////////////////////
