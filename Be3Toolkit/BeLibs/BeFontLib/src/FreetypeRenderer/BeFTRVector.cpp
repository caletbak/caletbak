/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeFTRVector.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <BeFontLib/pch/BeFontLibPredef.h>

#include <FreetypeRenderer/BeFTRVector.h>

/////////////////////////////////////////////////////////////////////////////
BeFTRVector::BeFTRVectorData* BeFTRVector::VectorNew(BeSize_T iItemSize)
{
    BeFTRVector::BeFTRVectorData* pSelf = (BeFTRVector::BeFTRVectorData*)malloc(sizeof(BeFTRVector::BeFTRVectorData));
    BE_ASSERT(iItemSize);

    if (!pSelf)
    {
        BE_FONT_ERROR("line %d: No more memory for allocating data\n", __LINE__);
        exit( EXIT_FAILURE );
    }

    pSelf->m_iItemSize = iItemSize;
    pSelf->m_iSize = 0;
    pSelf->m_iCapacity = 1;
    pSelf->m_pItems = malloc(pSelf->m_iItemSize * pSelf->m_iCapacity);

    return pSelf;
}
/////////////////////////////////////////////////////////////////////////////
void BeFTRVector::VectorDelete(BeFTRVector::BeFTRVectorData* pSelf)
{
    BE_ASSERT(pSelf);

    free(pSelf->m_pItems);
    free(pSelf);
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
const BeVoidP BeFTRVector::VectorGet(const BeFTRVector::BeFTRVectorData* pSelf, BeSize_T iIndex)
{
    BE_ASSERT(pSelf);
    BE_ASSERT(pSelf->m_iSize);
    BE_ASSERT(iIndex  < pSelf->m_iSize);

    return (char*)(pSelf->m_pItems) + iIndex * pSelf->m_iItemSize;
}
/////////////////////////////////////////////////////////////////////////////
const BeVoidP BeFTRVector::VectorFront(const BeFTRVector::BeFTRVectorData* pSelf)
{
    BE_ASSERT(pSelf);
    BE_ASSERT(pSelf->m_iSize);

    return VectorGet(pSelf, 0);
}
/////////////////////////////////////////////////////////////////////////////
const BeVoidP BeFTRVector::VectorBack(const BeFTRVector::BeFTRVectorData* pSelf)
{
    BE_ASSERT(pSelf);
    BE_ASSERT(pSelf->m_iSize);

    return VectorGet(pSelf, pSelf->m_iSize - 1);
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeInt BeFTRVector::VectorContains(const BeFTRVector::BeFTRVectorData* pSelf, const BeVoidP pItem, BeInt(*cmp)(const BeVoidP, const BeVoidP))
{
    BeSize_T i;
    BE_ASSERT(pSelf);

    for (i = 0; i<pSelf->m_iSize; ++i)
    {
        if ((*cmp)(pItem, VectorGet(pSelf, i)) == 0)
        {
            return 1;
        }
    }

    return 0;
}
/////////////////////////////////////////////////////////////////////////////
BeInt BeFTRVector::VectorEmpty(const BeFTRVector::BeFTRVectorData* pSelf)
{
    BE_ASSERT(pSelf);

    return pSelf->m_iSize == 0;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeSize_T BeFTRVector::VectorSize(const BeFTRVector::BeFTRVectorData* pSelf)
{
    BE_ASSERT(pSelf);

    return pSelf->m_iSize;
}
/////////////////////////////////////////////////////////////////////////////
void BeFTRVector::VectorReserve(BeFTRVector::BeFTRVectorData* pSelf, const BeSize_T iSize)
{
    BE_ASSERT(pSelf);

    if (pSelf->m_iCapacity < iSize)
    {
        pSelf->m_pItems = realloc(pSelf->m_pItems, iSize * pSelf->m_iItemSize);
        pSelf->m_iCapacity = iSize;
    }
}
/////////////////////////////////////////////////////////////////////////////
BeSize_T BeFTRVector::VectorCapacity(const BeFTRVector::BeFTRVectorData* pSelf)
{
    BE_ASSERT(pSelf);

    return pSelf->m_iCapacity;
}
/////////////////////////////////////////////////////////////////////////////
void BeFTRVector::VectorShrink(BeFTRVector::BeFTRVectorData* pSelf)
{
    BE_ASSERT(pSelf);

    if (pSelf->m_iCapacity > pSelf->m_iSize)
    {
        pSelf->m_pItems = realloc(pSelf->m_pItems, pSelf->m_iSize * pSelf->m_iItemSize);
    }

    pSelf->m_iCapacity = pSelf->m_iSize;
}
/////////////////////////////////////////////////////////////////////////////
void BeFTRVector::VectorClear(BeFTRVector::BeFTRVectorData* pSelf)
{
    BE_ASSERT(pSelf);

    pSelf->m_iSize = 0;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeFTRVector::VectorSet(BeFTRVector::BeFTRVectorData* pSelf, const BeSize_T iIndex, const BeVoidP pItem)
{
    BE_ASSERT(pSelf);
    BE_ASSERT(pSelf->m_iSize);
    BE_ASSERT(iIndex  < pSelf->m_iSize);

    memcpy((char *)(pSelf->m_pItems) + iIndex * pSelf->m_iItemSize, pItem, pSelf->m_iItemSize);
}
/////////////////////////////////////////////////////////////////////////////
void BeFTRVector::VectorErase(BeFTRVector::BeFTRVectorData* pSelf, const BeSize_T iIndex)
{
    BE_ASSERT(pSelf);
    BE_ASSERT(iIndex < pSelf->m_iSize);

    VectorEraseRange(pSelf, iIndex, iIndex + 1);
}
/////////////////////////////////////////////////////////////////////////////
void BeFTRVector::VectorEraseRange(BeFTRVector::BeFTRVectorData* pSelf, const BeSize_T iFirst, const BeSize_T iLast)
{
    BE_ASSERT(pSelf);
    BE_ASSERT(iFirst < pSelf->m_iSize);
    BE_ASSERT(iLast  < pSelf->m_iSize + 1);
    BE_ASSERT(iFirst < iLast);

    memmove((char *)(pSelf->m_pItems) + iFirst * pSelf->m_iItemSize,
        (char *)(pSelf->m_pItems) + iLast  * pSelf->m_iItemSize,
        (pSelf->m_iSize - iLast)   * pSelf->m_iItemSize);

    pSelf->m_iSize -= (iLast - iFirst);
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeFTRVector::VectorInsert(BeFTRVector::BeFTRVectorData* pSelf, const BeSize_T iIndex, const BeVoidP pItem)
{
    BE_ASSERT(pSelf);
    BE_ASSERT(iIndex <= pSelf->m_iSize);

    if (pSelf->m_iCapacity <= pSelf->m_iSize)
    {
        VectorReserve(pSelf, 2 * pSelf->m_iCapacity);
    }

    if (iIndex < pSelf->m_iSize)
    {
        memmove((char *)(pSelf->m_pItems) + (iIndex + 1) * pSelf->m_iItemSize,
            (char *)(pSelf->m_pItems) + (iIndex + 0) * pSelf->m_iItemSize,
            (pSelf->m_iSize - iIndex)  * pSelf->m_iItemSize);
    }

    pSelf->m_iSize++;
    VectorSet(pSelf, iIndex, pItem);
}
/////////////////////////////////////////////////////////////////////////////
void BeFTRVector::VectorPushBack(BeFTRVector::BeFTRVectorData* pSelf, const BeVoidP pItem)
{
    VectorInsert(pSelf, pSelf->m_iSize, pItem);
}
/////////////////////////////////////////////////////////////////////////////
void BeFTRVector::VectorPopBack(BeFTRVector::BeFTRVectorData* pSelf)
{
    BE_ASSERT(pSelf);
    BE_ASSERT(pSelf->m_iSize);

    pSelf->m_iSize--;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeFTRVector::VectorResize(BeFTRVector::BeFTRVectorData* pSelf, const BeSize_T iSize)
{
    BE_ASSERT(pSelf);

    if (iSize > pSelf->m_iCapacity)
    {
        VectorReserve(pSelf, iSize);
        pSelf->m_iSize = pSelf->m_iCapacity;
    }
    else
    {
        pSelf->m_iSize = iSize;
    }
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeFTRVector::VectorInsertData(BeFTRVector::BeFTRVectorData* pSelf, const BeSize_T iIndex, const BeVoidP pData, const BeSize_T iCount)
{
    BE_ASSERT(pSelf);
    BE_ASSERT(iIndex < pSelf->m_iSize);
    BE_ASSERT(pData);
    BE_ASSERT(iCount);

    if (pSelf->m_iCapacity < (pSelf->m_iSize + iCount))
    {
        VectorReserve(pSelf, pSelf->m_iSize + iCount);
    }

    memmove((char *)(pSelf->m_pItems) + (iIndex + iCount) * pSelf->m_iItemSize,
        (char *)(pSelf->m_pItems) + (iIndex)* pSelf->m_iItemSize,
        iCount*pSelf->m_iItemSize);

    memmove((char *)(pSelf->m_pItems) + iIndex * pSelf->m_iItemSize, pData,
        iCount*pSelf->m_iItemSize);

    pSelf->m_iSize += iCount;
}
/////////////////////////////////////////////////////////////////////////////
void BeFTRVector::VectorPushBackData(BeFTRVector::BeFTRVectorData* pSelf, const BeVoidP pData, const BeSize_T iCount)
{
    BE_ASSERT(pSelf);
    BE_ASSERT(pData);
    BE_ASSERT(iCount);

    if (pSelf->m_iCapacity < (pSelf->m_iSize + iCount))
    {
        VectorReserve(pSelf, pSelf->m_iSize + iCount);
    }

    memmove((char *)(pSelf->m_pItems) + pSelf->m_iSize * pSelf->m_iItemSize, pData,
        iCount*pSelf->m_iItemSize);

    pSelf->m_iSize += iCount;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeFTRVector::VectorSort(BeFTRVector::BeFTRVectorData* pSelf, BeInt(*cmp)(const BeVoidP, const BeVoidP))
{
    BE_ASSERT(pSelf);
    BE_ASSERT(pSelf->m_iSize);

    qsort(pSelf->m_pItems, pSelf->m_iSize, pSelf->m_iItemSize, cmp);
}
/////////////////////////////////////////////////////////////////////////////

