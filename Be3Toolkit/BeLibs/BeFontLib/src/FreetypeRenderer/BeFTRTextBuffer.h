/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeFTRTextBuffer.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_FTRTEXTBUFFER_H
#define BE_FTRTEXTBUFFER_H

#include <FreetypeRenderer/BeFTRFontManager.h>
#include <FreetypeRenderer/BeFTRMarkup.h>
#include <FreetypeRenderer/BeFTRVertexBuffer.h>

//#include "shader.h"

/// \brief  Use LCD filtering
#define LCD_FILTERING_ON    3

/// \brief  Do not use LCD filtering
#define LCD_FILTERING_OFF   1

class FONT_API BeFTRTextBuffer
{
    /// \brief  Text buffer structure
    struct BeFTRTextBufferData
    {
        /// \brief  Vertex buffer
        BeFTRVertexBuffer::BeFTRVertexBufferData* m_pBuffer;

        /// \brief  Font manager
        BeFTRFontManager::BeFTRFontManagerData *m_pFontManager;

        /// \brief  Base color for text
        BeVector4D m_kBaseColor;


        /// \brief  Pen origin
        BeVector2D m_kOrigin;

        /// \brief  Index (in the vertex buffer) of the line start
        BeSize_T m_iLineStart;

        /// \brief  Current line ascender
        BeFloat m_fLineAscender;

        /// \brief  Current line decender
        BeFloat m_fLineDescender;

        /*
        /// \brief  Shader handler
        GLuint shader;

        /// \brief  Shader "texture" location
        GLuint shader_texture;

        /// \brief  Shader "pixel" location
        GLuint shader_pixel;
        */

    };



    /// \brief  Glyph vertex structure
    struct BeGlyphVertex
    {
        /// \brief  Vertex x coordinates
        BeFloat m_fX;

        /// \brief  Vertex y coordinates
        BeFloat m_fY;

        /// \brief  Vertex z coordinates
        BeFloat m_fZ;

        /// \brief  Texture first coordinate
        BeFloat m_fU;

        /// \brief  Texture second coordinate
        BeFloat m_fV;

        /// \brief  Color red component
        BeFloat m_fR;

        /// \brief  Color green component
        BeFloat m_fG;

        /// \brief  Color blue component
        BeFloat m_fB;

        /// \brief  Color alpha component
        BeFloat m_fA;

        /// \brief  Shift along x
        BeFloat m_fShift;

        /// \brief  Color gamma correction
        BeFloat m_fGamma;

    };



    /// \brief  Creates a new empty text buffer
    static BeFTRTextBufferData* TextBufferNew(BeSize_T iDepth);

    /// \brief  Clear text buffer
    static void TextBufferClear(BeFTRTextBufferData* pSelf);



    /// \brief  Render a text buffer
    static void TextBufferRender(BeFTRTextBufferData* pSelf);



    /// \brief  Print some text to the text buffer
    static void TextBufferPrintf(BeFTRTextBufferData* pSelf, BeVector2D* kPen, ...);



    /// \brief  Moves the last line of the text buffer
    static void TextBufferMoveLastLine(BeFTRTextBufferData* pSelf, BeFloat fDY);

    /// \brief  Add some text to the text buffer
    static void TextBufferAddText(BeFTRTextBufferData* pSelf, BeVector2D* kPen, BeFTRMarkup::BeFTRMarkupData* pMarkup, BeWChar* wText, BeSize_T iLength);

    /// \brief  Add a char to the text buffer
    static void TextBufferAddWchar(BeFTRTextBufferData* pSelf, BeVector2D* kPen, BeFTRMarkup::BeFTRMarkupData* pMarkup, BeWChar wCurrent, BeWChar wPrevious);

};

#endif // BE_FTRTEXTBUFFER_H
