/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeFTRVector.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_FTRVECTOR_H
#define BE_FTRVECTOR_H

#include <BeFontLib/pch/BeFontLibPredef.h>

class FONT_API BeFTRVector
{
public:

    struct BeFTRVectorData
    {
        /// \brief  Pointer to dynamically allocated items
        BeVoidP m_pItems;

        /// \brief  Number of items that can be held in currently allocated storage
        BeSize_T m_iCapacity;

        /// \brief  Number of items
        BeSize_T m_iSize;

        /// \brief  Size (in bytes) of a single item
        BeSize_T m_iItemSize;
    };



    /// \brief  Creates a new empty vector
    static BeFTRVectorData* VectorNew(BeSize_T iItemSize);

    /// \brief  Deletes a vector
    static void VectorDelete(BeFTRVectorData* pSelf);



    /// \brief  Returns a pointer to the item located at specified index
    static const BeVoidP VectorGet(const BeFTRVectorData* pSelf, BeSize_T iIndex);

    /// \brief  Returns a pointer to the first item
    static const BeVoidP VectorFront(const BeFTRVectorData* pSelf);

    /// \brief  Returns a pointer to the last item
    static const BeVoidP VectorBack(const BeFTRVectorData* pSelf);



    /// \brief  Check if an item is contained within the vector
    static BeInt VectorContains(const BeFTRVectorData* pSelf, const BeVoidP pItem, BeInt(*cmp)(const BeVoidP, const BeVoidP));

    /// \brief  Checks whether the vector is empty
    static BeInt VectorEmpty(const BeFTRVectorData* pSelf);



    /// \brief  Returns the number of items
    static BeSize_T VectorSize(const BeFTRVectorData* pSelf);

    /// \brief  Reserve storage such that it can hold at last size items.
    static void VectorReserve(BeFTRVectorData* pSelf, const BeSize_T iSize);

    /// \brief  Returns current storage capacity
    static BeSize_T VectorCapacity(const BeFTRVectorData* pSelf);

    /// \brief  Decrease capacity to fit actual size
    static void VectorShrink(BeFTRVectorData* pSelf);

    /// \brief  Removes all items
    static void VectorClear(BeFTRVectorData* pSelf);



    /// \brief  Replace an item
    static void VectorSet(BeFTRVectorData* pSelf, const BeSize_T iIndex, const BeVoidP pItem);

    /// \brief  Erase an item
    static void VectorErase(BeFTRVectorData* pSelf, const BeSize_T iIndex);

    /// \brief  Erase a range of items.
    static void VectorEraseRange(BeFTRVectorData* pSelf, const BeSize_T iFirst, const BeSize_T iLast);



    /// \brief  Insert a single item at specified index
    static void VectorInsert(BeFTRVectorData* pSelf, const BeSize_T iIndex, const BeVoidP pItem);

    /// \brief  Appends given item to the end of the vector
    static void VectorPushBack(BeFTRVectorData* pSelf, const BeVoidP pItem);

    /// \brief  Removes the last item of the vector
    static void VectorPopBack(BeFTRVectorData* pSelf);



    /// \brief  Resizes the vector to contain size items
    static void VectorResize(BeFTRVectorData* pSelf, const BeSize_T iSize);



    /// \brief  Insert raw data at specified index
    static void VectorInsertData(BeFTRVectorData* pSelf, const BeSize_T iIndex, const BeVoidP pData, const BeSize_T iCount);

    /// \brief  Append raw data to the end of the vector
    static void VectorPushBackData(BeFTRVectorData* pSelf, const BeVoidP pData, const BeSize_T iCount);



    /// \brief  Sort vector items according to cmp function
    static void VectorSort(BeFTRVectorData* pSelf, BeInt(*cmp)(const BeVoidP, const BeVoidP));

};

#endif // BE_FTRVECTOR_H
