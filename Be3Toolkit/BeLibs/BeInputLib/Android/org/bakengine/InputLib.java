
package org.bakengine;

import android.app.Activity;
import android.content.Context;
import android.view.inputmethod.InputMethodManager;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

public class InputLib
{
    
	private static native void onNativeInit (Class<?> thisClass);
    
	private static final String TAG = "InputLib";
    
    private static InputLib m_instance;
    
    static
	{
        BeDebug.i(TAG, "Initialising input JNI (java)");
		onNativeInit (InputLib.class);
    }
    
	public InputLib ()
	{
        m_instance = this;
	}
    
    public static void Keyboard_Show()
    {
        BeDebug.i(TAG, "Show keyboard");
		Activity s_hostActivity = BeNativeActivity.s_gameActivity;
        
        InputMethodManager imm = (InputMethodManager)s_hostActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        s_hostActivity.getWindow().getDecorView().getRootView().requestFocus();
        imm.showSoftInput(s_hostActivity.getWindow().getDecorView().getRootView(), InputMethodManager.SHOW_IMPLICIT);
    }
    
    public void Keyboard_Hide()
    {
        BeDebug.i(TAG, "Hide keyboard");
		Activity s_hostActivity = BeNativeActivity.s_gameActivity;
        
        InputMethodManager imm = (InputMethodManager)s_hostActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(s_hostActivity.getWindow().getDecorView().getRootView().getWindowToken(), 0);
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
