
package org.bakengine;

import android.content.Context;
import android.view.MotionEvent;


public abstract class BaseGestureDetector 
{
    protected boolean m_gestureInProgress;
    protected boolean m_gestureHasStarted;
    
    protected MotionEvent m_prevEvent;
    protected MotionEvent m_currEvent;
    
    protected float m_currPressure;
    protected float m_prevPressure;
    protected long m_timeDelta;
    protected final Context mContext;
    
    protected static final float PRESSURE_THRESHOLD = 0.67f;
    
    public enum GestureProgress
    {
    	GESTURE_NOTSTARTED,
    	GESTURE_STARTED,
    	GESTURE_INPROGRESS,
    	GESTURE_ENDED,
    };
    
    public BaseGestureDetector(Context context)
    {
    	mContext = context; 
    }
    
    public boolean onTouchEvent(MotionEvent event)
    {
    	final int actionCode = event.getAction() & MotionEvent.ACTION_MASK;
    	if (!m_gestureInProgress) 
    	{
    		handleStartProgressEvent(actionCode, event);
    		m_gestureHasStarted = true;
    	} 
    	else 
    	{
    		handleInProgressEvent(actionCode, event);
    	}
    	return true;
    }

	protected abstract void handleInProgressEvent(int actionCode, MotionEvent event);

	protected abstract void handleStartProgressEvent(int actionCode, MotionEvent event);
	
	protected void updateStateByEvent(MotionEvent curr)
	{
    	final MotionEvent prev = m_prevEvent;
    	
    	// Reset mCurrEvent
        if (m_currEvent != null)
        {
        	m_currEvent.recycle();
        	m_currEvent = null;
        }
        m_currEvent = MotionEvent.obtain(curr);
        
        
        // Delta time
        m_timeDelta = curr.getEventTime() - prev.getEventTime();

        // Pressure
        m_currPressure = curr.getPressure(curr.getActionIndex());
        m_prevPressure = prev.getPressure(prev.getActionIndex());
    }
	
	protected void resetState() 
	{
        if (m_prevEvent != null) 
        {
        	m_prevEvent.recycle();
        	m_prevEvent = null;
        }
        if (m_currEvent != null) 
        {
        	m_currEvent.recycle();
        	m_currEvent = null;
        }
        m_gestureInProgress = false;
    }
	
	public boolean isInProgress() 
	{
        return m_gestureInProgress;
    }
	
	public void gestureEnded()
	{
		m_gestureHasStarted = false;
	}
	public boolean isStarted()
	{
		return m_gestureHasStarted;
	}
	
	public long getTimeDelta() 
	{
		return m_timeDelta;
	}
	
	public long getEventTime() 
	{
		return m_currEvent.getEventTime();
	}
}
