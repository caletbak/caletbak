
package org.bakengine;

import android.content.Context;
import android.view.MotionEvent;


public class RotateGestureDetector extends TwoFingerGestureDetector
{
	/**
	 * Listener which must be implemented which is used by RotateGestureDetector
	 * to perform callbacks to any implementing class which is registered to a
	 * RotateGestureDetector via the constructor.
	 * 
	 * @see RotateGestureDetector.SimpleOnRotateGestureListener
	 */
	public interface OnRotateGestureListener 
	{
		public boolean onRotate(RotateGestureDetector detector);
		public boolean onRotateBegin(RotateGestureDetector detector);
		public void onRotateEnd(RotateGestureDetector detector);
	}
	
	/**
	 * Helper class which may be extended and where the methods may be
	 * implemented. This way it is not necessary to implement all methods
	 * of OnRotateGestureListener.
	 */
	public static class SimpleOnRotateGestureListener implements OnRotateGestureListener 
	{
	    public boolean onRotate(RotateGestureDetector detector) 
	    {
	        return false;
	    }

	    public boolean onRotateBegin(RotateGestureDetector detector) 
	    {
	        return true;
	    }

	    public void onRotateEnd(RotateGestureDetector detector) 
	    {
	    	// Do nothing, overridden implementation may be used
	    }
	}

    
    private final OnRotateGestureListener m_listener;
    private boolean m_sloppyGesture;

    public RotateGestureDetector(Context context, OnRotateGestureListener listener) 
    {
    	super(context);
        m_listener = listener;
    }

    @Override
    protected void handleStartProgressEvent(int actionCode, MotionEvent event)
    {
        switch (actionCode) 
        {
            case MotionEvent.ACTION_POINTER_DOWN:
                // At least the second finger is on screen now
            	
                resetState(); // In case we missed an UP/CANCEL event
                m_prevEvent = MotionEvent.obtain(event);
                m_timeDelta = 0;
                
                updateStateByEvent(event);
                
                // See if we have a sloppy gesture
                m_sloppyGesture = isSloppyGesture(event);
                if(!m_sloppyGesture)
                {
                	// No, start gesture now
                    m_gestureInProgress = m_listener.onRotateBegin(this);
                } 
            	break;
            
            case MotionEvent.ACTION_MOVE:
                if (!m_sloppyGesture)
                {
                	break;
                }
                
                // See if we still have a sloppy gesture
                m_sloppyGesture = isSloppyGesture(event);
                if(!m_sloppyGesture)
                {
                	// No, start normal gesture now
                    m_gestureInProgress = m_listener.onRotateBegin(this);
                }
    
                break;
                
            case MotionEvent.ACTION_POINTER_UP:
                if (!m_sloppyGesture) 
                {
                	break;
                }
           
                break; 
        }
    }

    
    @Override
    protected void handleInProgressEvent(int actionCode, MotionEvent event)
    { 	
        switch (actionCode) 
        {
            case MotionEvent.ACTION_POINTER_UP:
                // Gesture ended but 
                updateStateByEvent(event);

                if (!m_sloppyGesture) 
                {
                    m_listener.onRotateEnd(this);
                }

                resetState();
                break;

            case MotionEvent.ACTION_CANCEL:
                if (!m_sloppyGesture) 
                {
                    m_listener.onRotateEnd(this);
                }

                resetState();
                break;

            case MotionEvent.ACTION_MOVE:
                updateStateByEvent(event);

				// Only accept the event if our relative pressure is within
				// a certain limit. This can help filter shaky data as a
				// finger is lifted.
                if (m_currPressure / m_prevPressure > PRESSURE_THRESHOLD)
                {
                    final boolean updatePrevious = m_listener.onRotate(this);
                    if (updatePrevious)
                    {
                        m_prevEvent.recycle();
                        m_prevEvent = MotionEvent.obtain(event);
                    }
                }
                break;
        }
    }

    @Override
    protected void resetState()
    {
        super.resetState();
        m_sloppyGesture = false;
    }


    /**
     * Return the rotation difference from the previous rotate event to the current
     * event. 
     * 
     * @return The current rotation //difference in degrees.
     */
	public float getRotationDegreesDelta() 
	{
		double diffRadians = Math.atan2(m_currFingerDiffY, m_currFingerDiffX) - Math.atan2(m_prevFingerDiffY, m_prevFingerDiffX) ;
		return (float) (diffRadians);
		//return (float) (diffRadians * 180 / Math.PI);
	}
}
