
package org.bakengine;

import android.content.Context;
import android.util.DisplayMetrics;
import android.util.FloatMath;
import android.view.MotionEvent;
import android.view.ViewConfiguration;

public abstract class TwoFingerGestureDetector extends BaseGestureDetector
{
	private final float m_edgeSlop;
    private float m_rightSlopEdge;
    private float m_bottomSlopEdge;
    
	protected float m_prevFingerDiffX;
	protected float m_prevFingerDiffY;
	protected float m_currFingerDiffX;
	protected float m_currFingerDiffY;
    
    private float m_currLen;
    private float m_prevLen;
    
    public TwoFingerGestureDetector(Context context) 
    {
    	super(context);
    	ViewConfiguration config = ViewConfiguration.get(context);
        m_edgeSlop = config.getScaledEdgeSlop();       	
    }
    
    @Override
	protected abstract void handleStartProgressEvent(int actionCode, MotionEvent event);

	@Override
	protected abstract void handleInProgressEvent(int actionCode, MotionEvent event);
	
	protected void updateStateByEvent(MotionEvent curr)
	{
		super.updateStateByEvent(curr);
		
		final MotionEvent prev = m_prevEvent;
		
        m_currLen = -1;
        m_prevLen = -1;

        // Previous
        final float px0 = prev.getX(0);
        final float py0 = prev.getY(0);
        final float px1 = prev.getX(1);
        final float py1 = prev.getY(1);
        final float pvx = px1 - px0;
        final float pvy = py1 - py0;
        m_prevFingerDiffX = pvx;
        m_prevFingerDiffY = pvy;
        
        // Current
        final float cx0 = curr.getX(0);
        final float cy0 = curr.getY(0);
        final float cx1 = curr.getX(1);
        final float cy1 = curr.getY(1);
        final float cvx = cx1 - cx0;
        final float cvy = cy1 - cy0;
        m_currFingerDiffX = cvx;
        m_currFingerDiffY = cvy;
	}
	
    /**
     * Return the current distance between the two pointers forming the
     * gesture in progress.
     * 
     * @return Distance between pointers in pixels.
     */
    public float getCurrentSpan() {
        if (m_currLen == -1) 
        {
            final float cvx = m_currFingerDiffX;
            final float cvy = m_currFingerDiffY;
            m_currLen = FloatMath.sqrt(cvx*cvx + cvy*cvy);
        }
        return m_currLen;
    }

    /**
     * Return the previous distance between the two pointers forming the
     * gesture in progress.
     * 
     * @return Previous distance between pointers in pixels.
     */
    public float getPreviousSpan() {
        if (m_prevLen == -1) 
        {
            final float pvx = m_prevFingerDiffX;
            final float pvy = m_prevFingerDiffY;
            m_prevLen = FloatMath.sqrt(pvx*pvx + pvy*pvy);
        }
        return m_prevLen;
    }
    
    /**
     * MotionEvent has no getRawX(int) method; simulate it pending future API approval. 
     * @param event
     * @param pointerIndex
     * @return
     */
    protected static float getRawX(MotionEvent event, int pointerIndex) {
        float offset = event.getX() - event.getRawX();
        if(pointerIndex < event.getPointerCount())
        {
        	return event.getX(pointerIndex) + offset;
        } 
        return 0f;
    }

    /**
     * MotionEvent has no getRawY(int) method; simulate it pending future API approval. 
     * @param event
     * @param pointerIndex
     * @return
     */
    protected static float getRawY(MotionEvent event, int pointerIndex) 
    {
        float offset = event.getY() - event.getRawY();
        if(pointerIndex < event.getPointerCount())
        {
        	return event.getY(pointerIndex) + offset;
        } 
        return 0f;
    }

	/**
	 * Check if we have a sloppy gesture. Sloppy gestures can happen if the edge
	 * of the user's hand is touching the screen, for example.
	 * 
	 * @param event
	 * @return
	 */
    protected boolean isSloppyGesture(MotionEvent event)
    {
        // As orientation can change, query the metrics in touch down
        DisplayMetrics metrics = mContext.getResources().getDisplayMetrics();
        m_rightSlopEdge = metrics.widthPixels - m_edgeSlop;
        m_bottomSlopEdge = metrics.heightPixels - m_edgeSlop;
        
        final float edgeSlop = m_edgeSlop;
        final float rightSlop = m_rightSlopEdge;
        final float bottomSlop = m_bottomSlopEdge;
        
        final float x0 = event.getRawX();
        final float y0 = event.getRawY();
        final float x1 = getRawX(event, 1);
        final float y1 = getRawY(event, 1);

        boolean p0sloppy = x0 < edgeSlop || y0 < edgeSlop
                || x0 > rightSlop || y0 > bottomSlop;
        boolean p1sloppy = x1 < edgeSlop || y1 < edgeSlop
                || x1 > rightSlop || y1 > bottomSlop;

        if (p0sloppy && p1sloppy) {
            return true;
        } else if (p0sloppy) {
        	 return true;
        } else if (p1sloppy) {
        	 return true;
        } 
        return false;
    }
}
