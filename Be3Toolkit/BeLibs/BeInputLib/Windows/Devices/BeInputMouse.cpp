/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeInputMouse.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <Devices/BeInputMouse.h>

/////////////////////////////////////////////////////////////////////////////
BeInputMouse::MouseData BeInputMouse::s_currentMouseData;
/////////////////////////////////////////////////////////////////////////////
BeInputMouse::MouseData BeInputMouse::s_mouseDataToProcess;
/////////////////////////////////////////////////////////////////////////////
InputListenerCallback BeInputMouse::m_pMouseCallback = NULL;
/////////////////////////////////////////////////////////////////////////////
BeVoidP BeInputMouse::m_pMouseCallbackListener = NULL;
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeInputMouse::BeInputMouse()
{
}
/////////////////////////////////////////////////////////////////////////////
BeInputMouse::~BeInputMouse()
{
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeInputMouse::MouseData* BeInputMouse::GetMouseCurrentState (void)
{
  return &s_currentMouseData;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeInputMouse::RegistryInputManagerListener(BeVoidP mouseCallbackListener, InputListenerCallback mouseCallback)
{
  m_pMouseCallbackListener = mouseCallbackListener;
  m_pMouseCallback = mouseCallback;
}
/////////////////////////////////////////////////////////////////////////////
void BeInputMouse::CallCallbacks (BeInputMouse::MouseData* pMouseData)
{
  BE_UNUSED (pMouseData);

  if (m_pMouseCallbackListener && m_pMouseCallback)
  {
    m_pMouseCallback (m_pMouseCallbackListener, pMouseData);
  }
}
/////////////////////////////////////////////////////////////////////////////
void BeInputMouse::UpdateMouseState (void)
{
  s_currentMouseData.m_coordX = s_mouseDataToProcess.m_coordX;
  s_currentMouseData.m_coordY = s_mouseDataToProcess.m_coordY;

  if (s_currentMouseData.m_leftButton == BUTTON_IDLE && s_mouseDataToProcess.m_leftButton == BUTTON_DOWN)
  {
    s_currentMouseData.m_leftButton = BUTTON_DOWN;
    s_currentMouseData.m_buttonDidEvent = MOUSE_BUTTON_LEFT;
    CallCallbacks (&s_currentMouseData);
  }
  else if (s_currentMouseData.m_leftButton == BUTTON_DOWN && s_mouseDataToProcess.m_leftButton == BUTTON_DOWN)
  {
    s_currentMouseData.m_leftButton = BUTTON_PRESSED;
    s_currentMouseData.m_buttonDidEvent = MOUSE_BUTTON_LEFT;
    CallCallbacks (&s_currentMouseData);
  }
  else if ((s_currentMouseData.m_leftButton == BUTTON_DOWN || s_currentMouseData.m_leftButton == BUTTON_PRESSED) && s_mouseDataToProcess.m_leftButton == BUTTON_UP)
  {
    s_currentMouseData.m_leftButton = BUTTON_UP;
    s_currentMouseData.m_buttonDidEvent = MOUSE_BUTTON_LEFT;
    CallCallbacks (&s_currentMouseData);
    s_currentMouseData.m_leftButton = BUTTON_IDLE;
  }

  if (s_currentMouseData.m_rightButton == BUTTON_IDLE && s_mouseDataToProcess.m_rightButton == BUTTON_DOWN)
  {
    s_currentMouseData.m_rightButton = BUTTON_DOWN;
    s_currentMouseData.m_buttonDidEvent = MOUSE_BUTTON_RIGHT;
    CallCallbacks (&s_currentMouseData);
  }
  else if (s_currentMouseData.m_rightButton == BUTTON_DOWN && s_mouseDataToProcess.m_rightButton == BUTTON_DOWN)
  {
    s_currentMouseData.m_rightButton = BUTTON_PRESSED;
    s_currentMouseData.m_buttonDidEvent = MOUSE_BUTTON_RIGHT;
    CallCallbacks (&s_currentMouseData);
  }
  else if ((s_currentMouseData.m_rightButton == BUTTON_DOWN || s_currentMouseData.m_rightButton == BUTTON_PRESSED) && s_mouseDataToProcess.m_rightButton == BUTTON_UP)
  {
    s_currentMouseData.m_rightButton = BUTTON_UP;
    s_currentMouseData.m_buttonDidEvent = MOUSE_BUTTON_RIGHT;
    CallCallbacks (&s_currentMouseData);
    s_currentMouseData.m_rightButton = BUTTON_IDLE;
  }

  if (s_currentMouseData.m_mediumButton == BUTTON_IDLE && s_mouseDataToProcess.m_mediumButton == BUTTON_DOWN)
  {
    s_currentMouseData.m_mediumButton = BUTTON_DOWN;
    s_currentMouseData.m_buttonDidEvent = MOUSE_BUTTON_MEDIUM;
    CallCallbacks (&s_currentMouseData);
  }
  else if (s_currentMouseData.m_mediumButton == BUTTON_DOWN && s_mouseDataToProcess.m_mediumButton == BUTTON_DOWN)
  {
    s_currentMouseData.m_mediumButton = BUTTON_PRESSED;
    s_currentMouseData.m_buttonDidEvent = MOUSE_BUTTON_MEDIUM;
    CallCallbacks (&s_currentMouseData);
  }
  else if ((s_currentMouseData.m_mediumButton == BUTTON_DOWN || s_currentMouseData.m_mediumButton == BUTTON_PRESSED) && s_mouseDataToProcess.m_mediumButton == BUTTON_UP)
  {
    s_currentMouseData.m_mediumButton = BUTTON_UP;
    s_currentMouseData.m_buttonDidEvent = MOUSE_BUTTON_MEDIUM;
    CallCallbacks (&s_currentMouseData);
    s_currentMouseData.m_mediumButton = BUTTON_IDLE;
  }
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeInputMouse::MouseEventHandle (BeInt mouseEvent, BeInt coordX, BeInt coordY)
{
  s_mouseDataToProcess.m_coordX = coordX;
  s_mouseDataToProcess.m_coordY = coordY;

  if (mouseEvent == WM_LBUTTONDOWN || mouseEvent == WM_LBUTTONUP || mouseEvent == WM_LBUTTONDBLCLK)
  {
    if (mouseEvent == WM_LBUTTONDOWN)
    {
      s_mouseDataToProcess.m_leftButton = BUTTON_DOWN;
    }
    else if (mouseEvent == WM_LBUTTONUP)
    {
      s_mouseDataToProcess.m_leftButton = BUTTON_UP;
    }
    else if (mouseEvent == WM_LBUTTONDBLCLK)
    {
      s_mouseDataToProcess.m_leftButton = BUTTON_DOUBLECLICK;
    }
  }

  if (mouseEvent == WM_RBUTTONDOWN || mouseEvent == WM_RBUTTONUP || mouseEvent == WM_RBUTTONDBLCLK)
  {
    if (mouseEvent == WM_RBUTTONDOWN)
    {
      s_mouseDataToProcess.m_rightButton = BUTTON_DOWN;
    }
    else if (mouseEvent == WM_RBUTTONUP)
    {
      s_mouseDataToProcess.m_rightButton = BUTTON_UP;
    }
    else if (mouseEvent == WM_RBUTTONDBLCLK)
    {
      s_mouseDataToProcess.m_rightButton = BUTTON_DOUBLECLICK;
    }
  }

  if (mouseEvent == WM_MBUTTONDOWN || mouseEvent == WM_MBUTTONUP || mouseEvent == WM_MBUTTONDBLCLK)
  {
    if (mouseEvent == WM_MBUTTONDOWN)
    {
      s_mouseDataToProcess.m_mediumButton = BUTTON_DOWN;
    }
    else if (mouseEvent == WM_MBUTTONUP)
    {
      s_mouseDataToProcess.m_mediumButton = BUTTON_UP;
    }
    else if (mouseEvent == WM_MBUTTONDBLCLK)
    {
      s_mouseDataToProcess.m_mediumButton = BUTTON_DOUBLECLICK;
    }
  }
}
/////////////////////////////////////////////////////////////////////////////
