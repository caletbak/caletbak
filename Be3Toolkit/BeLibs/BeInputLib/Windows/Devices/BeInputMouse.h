/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeInputMouse.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEINPUTMOUSE_H
#define BE_BEINPUTMOUSE_H

#include <BeInputLib/Pch/BeInputLibPredef.h>
#include <BeInputTypes.h>

/////////////////////////////////////////////////
/// \class  BeInputMouse
/// \brief  File Tool class for reading and writing files
/////////////////////////////////////////////////
class INPUT_API BeInputMouse : public BeMemoryObject
{
  
  public:

    enum eMouseButtons
    {
      MOUSE_BUTTON_LEFT = 0x01,
      MOUSE_BUTTON_RIGHT = 0x02,
      MOUSE_BUTTON_MEDIUM = 0x04
    };

    class MouseData
    {
      public:

        BeInt m_coordX;
        BeInt m_coordY;

        eButtonStates m_leftButton;
        eButtonStates m_rightButton;
        eButtonStates m_mediumButton;

        eMouseButtons m_buttonDidEvent;

        MouseData ()
        :
          m_leftButton(BUTTON_IDLE),
          m_rightButton(BUTTON_IDLE),
          m_mediumButton(BUTTON_IDLE)
        {
        }
    };

    /// \brief  Constructor
    BeInputMouse(void);
    /// \brief  Destructor
    ~BeInputMouse();

    // \brief  Methods
    // {

      /// \brief  Updates the mouse state
      static void UpdateMouseState (void);

      /// \brief  Handles the mouse events
      static void MouseEventHandle (BeInt mouseEvent, BeInt coordX, BeInt coordY);

    // }



    // \brief  Getters
    // {

      /// \brief  Gets the mouse current state
      static MouseData* GetMouseCurrentState (void);

    // }

  private:

    /// \brief  Current mouse state
    static MouseData s_currentMouseData;

    /// \brief  Mouse data to be processed
    static MouseData s_mouseDataToProcess;

    // Mouse Listener callback
    //typedef void (*MouseListenerInternalCallback) (BeVoidP, BeInputMouse::MouseData*);

    /// \brief  Mouse listener callback to the Input Manager
    static InputListenerCallback m_pMouseCallback;

    /// \brief  Input Manager object for callback
    static BeVoidP m_pMouseCallbackListener;

    /// \brief  Private function for calling the input manager callback
    static void CallCallbacks (MouseData* pMouseData);

    /// \brief  Registration of the input manager callback
    static void RegistryInputManagerListener(BeVoidP mouseCallbackListener, InputListenerCallback mouseCallback);



    friend class BeInputManager;
};

#endif // #ifndef BE_BEINPUTMOUSE_H
