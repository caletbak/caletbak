/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeInputTypes.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEINPUTTYPES_H
#define BE_BEINPUTTYPES_H

#include <BeInputLib/Pch/BeInputLibPredef.h>

// Input Listener callback
typedef void(*InputListenerCallback) (BeVoidP, BeVoidP);

#endif // #ifndef BE_BEINPUTTYPES_H
