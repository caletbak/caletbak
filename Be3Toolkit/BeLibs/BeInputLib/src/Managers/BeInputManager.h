/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeInputManager.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEINPUTMANAGER_H
#define BE_BEINPUTMANAGER_H

#include <BeInputLib/Pch/BeInputLibPredef.h>
#include <BeInputTypes.h>

/////////////////////////////////////////////////
/// \class BeInputManager
/// \brief Manager that controls all the input
/////////////////////////////////////////////////
class INPUT_API BeInputManager : public BeMemoryObject
{
  public:

    /// \brief  Constructor
    BeInputManager(void);
    /// \brief  Destructor
    ~BeInputManager();

    // Setters
    // {

    // }



    // Methods
    // {

      /// \brief  Updates the input manager
      void Update (void);

      /// \brief  Registry a new mouse listener
      void RegistryInputListener(BeVoidP pObjectListener, InputListenerCallback pCallback);

      /// \brief  UnRegistry mouse listener
      void UnRegistryInputListener(BeVoidP pObjectListener, InputListenerCallback pCallback);

    // }


  private:

    class BeInputListener
    {
      public:

        BeVoidP m_pObjectListener;
        InputListenerCallback m_pInputListenerCallback;

        BeInputListener ()
        :
          m_pObjectListener(NULL),
          m_pInputListenerCallback(NULL)
        {
        }
    };

    /// \brief  Registered input listeners array type
    typedef std::vector<BeInputListener*> InputListenersArray;

    /// \brief  Registered input listeners
    InputListenersArray m_kRegisteredInputListeners;



    /// \brief  Gets a registered listener internally
    BeInputListener* GetInputListenerInternal (BeVoidP pObjectListener, InputListenersArray::iterator* arrayIt = NULL);

    /// \brief  Registry a new listener internally
    BeInputListener* RegistryInputListenerInternal (BeVoidP pObjectListener);

    /// \brief  UnRegistry a listener internally
    void UnRegistryInputListenerInternal (BeVoidP pObjectListener);

    /// \brief  Callback for receiving the mouse events
    static void MouseInternalListener(BeVoidP pObjectListener, BeVoidP pMouseData);
};

// Smart pointer BeModulesManagerPtr
typedef BeSmartPointer<BeInputManager> BeInputManagerPtr;

#endif // #ifndef BE_BEINPUTMANAGER_H
