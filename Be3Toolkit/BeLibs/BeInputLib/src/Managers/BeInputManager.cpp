/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeInputManager.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <Managers/BeInputManager.h>

#if TARGET_WINDOWS
#include <Devices/BeInputMouse.h>
#endif

/////////////////////////////////////////////////////////////////////////////
BeInputManager::BeInputManager(void)
{
#if TARGET_WINDOWS
  BeInputMouse::RegistryInputManagerListener (this, &MouseInternalListener);
#endif
}
/////////////////////////////////////////////////////////////////////////////
BeInputManager::~BeInputManager()
{
  for (BeUInt i = 0; i < m_kRegisteredInputListeners.size (); i++)
  {
    BeInputListener* inputListener = m_kRegisteredInputListeners.at (i);
    BeDelete inputListener;
  }
  m_kRegisteredInputListeners.clear ();
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeInputManager::Update (void)
{
#if TARGET_WINDOWS
  BeInputMouse::UpdateMouseState ();
#endif
}
/////////////////////////////////////////////////////////////////////////////
void BeInputManager::RegistryInputListener(BeVoidP pObjectListener, InputListenerCallback pCallback)
{
  BeInputListener* inputListener = RegistryInputListenerInternal (pObjectListener);
  inputListener->m_pInputListenerCallback = pCallback;
}
/////////////////////////////////////////////////////////////////////////////
void BeInputManager::UnRegistryInputListener(BeVoidP pObjectListener, InputListenerCallback pCallback)
{
  BE_UNUSED (pCallback);
  
  BeInputListener* inputListener = GetInputListenerInternal (pObjectListener);
  if (inputListener != NULL)
  {
      inputListener->m_pInputListenerCallback = NULL;
    UnRegistryInputListenerInternal (pObjectListener);
  }
}
/////////////////////////////////////////////////////////////////////////////
void BeInputManager::MouseInternalListener(BeVoidP pObjectListener, BeVoidP pMouseData)
{
#if TARGET_WINDOWS
    BeInputMouse::MouseData* pMouseDataInternal = reinterpret_cast<BeInputMouse::MouseData*>(pMouseData);

    BeInputManager* inputManager = static_cast<BeInputManager*> (pObjectListener);

    for (BeUInt i = 0; i < inputManager->m_kRegisteredInputListeners.size (); i++)
    {
        BeInputListener* inputListener = inputManager->m_kRegisteredInputListeners.at (i);

        if (inputListener->m_pInputListenerCallback != NULL)
        {
            inputListener->m_pInputListenerCallback(inputListener->m_pObjectListener, pMouseDataInternal);
        }
  }
#endif
}
/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
BeInputManager::BeInputListener* BeInputManager::GetInputListenerInternal (BeVoidP pObjectListener, InputListenersArray::iterator* arrayIt)
{
  InputListenersArray::iterator kListenersBegin(m_kRegisteredInputListeners.begin());
  if (arrayIt)
  {
    *arrayIt = kListenersBegin;
  }

  InputListenersArray::const_iterator kListenersEnd(m_kRegisteredInputListeners.end());
  while(kListenersBegin != kListenersEnd)
  {
    BeInputListener* inputListener = static_cast<BeInputListener*>(*kListenersBegin);

    if (inputListener->m_pObjectListener == pObjectListener)
    {
      return inputListener;
    }

    ++kListenersBegin;
    if (arrayIt)
    {
      *arrayIt = kListenersBegin;
    }
  }

  return NULL;
}
/////////////////////////////////////////////////////////////////////////////
BeInputManager::BeInputListener* BeInputManager::RegistryInputListenerInternal (BeVoidP pObjectListener)
{
  BeInputListener* inputListener = GetInputListenerInternal (pObjectListener);
  if (inputListener != NULL)
  {
    return inputListener;
  }

  inputListener = BeNew BeInputListener ();
  inputListener->m_pObjectListener = pObjectListener;
  m_kRegisteredInputListeners.push_back (inputListener);

  return inputListener;
}
/////////////////////////////////////////////////////////////////////////////
void BeInputManager::UnRegistryInputListenerInternal (BeVoidP pObjectListener)
{
  InputListenersArray::iterator kListenersIt;
  BeInputListener* inputListener = GetInputListenerInternal (pObjectListener, &kListenersIt);
  if (inputListener != NULL)
  {
    // If all the input listener are NULL, then delete the listener totally
    if (inputListener->m_pInputListenerCallback == NULL)
    {
      inputListener->m_pObjectListener = NULL;

      BeDelete inputListener;

      m_kRegisteredInputListeners.erase (kListenersIt);
    }
  }
}
/////////////////////////////////////////////////////////////////////////////

