/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeInputLibPredef.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_INPUTLIBPREDEF_H
#define BE_INPUTLIBPREDEF_H

// Global BE3 include
#include <BeCommon/BakEngine3Common.h>

#if TARGET_WINDOWS
#ifdef INPUT_IMPORTS
#  define INPUT_API __declspec(dllimport)
#  pragma message("automatic link to BeInputLib.lib")
#  pragma comment(lib, "BeInputLib.lib")
#else
#  define INPUT_API __declspec(dllexport)
#endif
#else
#  define INPUT_API
#endif

#ifdef _DEBUG
#  define BE_INPUT_DEBUG(x, ...) _OutputDebugString("[  INPUT DEBUG]: ", true, x, ##__VA_ARGS__)
#  define BE_INPUT_WARNING(x, ...) _OutputDebugString("[! INPUT WARNING]: ", true, x, ##__VA_ARGS__)
#  define BE_INPUT_ERROR(x, ...) _OutputDebugString("[* INPUT ERROR]: ", true, x, ##__VA_ARGS__)
#  define BE_INPUT_PRINT(x, ...) _OutputDebugString("", false, x, ##__VA_ARGS__)
#else
#  define BE_INPUT_DEBUG(x, ...) x
#  define BE_INPUT_WARNING(x, ...) x
#  define BE_INPUT_ERROR(x, ...) x
#  define BE_INPUT_PRINT(x, ...) x
#endif

enum eButtonStates
{
  BUTTON_IDLE = 0x00,
  BUTTON_DOWN = 0x01,
  BUTTON_PRESSED = 0x02,
  BUTTON_UP = 0x04,
  BUTTON_DOUBLECLICK = 0x08
};

// External includes
#include <BeCoreLib/Pch/BeCoreLib.h>

#endif // #ifndef BE_INPUTLIBPREDEF_H

