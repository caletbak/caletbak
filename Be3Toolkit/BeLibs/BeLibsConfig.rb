####################################################################
##
## BAK ENGINE 3
## BUILD SYSTEM
## 
## UbiLibsConfig.rb
## This ruby prepares the libraries configuration
##
## Javier Calet Toledo - 2013
##
###################################################################

#!/bin/sh

# BELIBS CONFIG

# CORE LIB
kProjectPropertiesConfig = Hash.new
kProjectPropertiesConfig["projectName"] = "BeCoreLib"
kProjectPropertiesConfig["projectPath"] = "BeLibs/"
kProjectPropertiesConfig["projectDependencies"] = []
kProjectPropertiesConfig["projectExternalDependencies"] = ["ZLibLib"]
kProjectPropertiesConfig["project3rdPartyDependencies"] = ["AndroidAppGlueLib"]
kProjectPropertiesConfig["projectExports"] = ["CORE_EXPORTS"]
kProjectPropertiesConfig["libraryType"] = ProjectProperties::LIBRARY_TYPE_STATIC
kProjectPropertiesConfig["androidTarget"] = $g_strAndroidTarget
$g_kProjectPropertiesConfig.push kProjectPropertiesConfig

# MATH LIB
kProjectPropertiesConfig = Hash.new
kProjectPropertiesConfig["projectName"] = "BeMathLib"
kProjectPropertiesConfig["projectPath"] = "BeLibs/"
kProjectPropertiesConfig["projectDependencies"] = ["BeCoreLib"]
kProjectPropertiesConfig["projectExports"] = ["MATH_EXPORTS"]
kProjectPropertiesConfig["libraryType"] = ProjectProperties::LIBRARY_TYPE_STATIC
kProjectPropertiesConfig["androidTarget"] = $g_strAndroidTarget
$g_kProjectPropertiesConfig.push kProjectPropertiesConfig

# UTILITY LIB
kProjectPropertiesConfig = Hash.new
kProjectPropertiesConfig["projectName"] = "BeUtilityLib"
kProjectPropertiesConfig["projectPath"] = "BeLibs/"
kProjectPropertiesConfig["projectDependencies"] = ["BeMathLib"]
kProjectPropertiesConfig["projectExports"] = ["UTILITY_EXPORTS"]
kProjectPropertiesConfig["libraryType"] = ProjectProperties::LIBRARY_TYPE_STATIC
kProjectPropertiesConfig["androidTarget"] = $g_strAndroidTarget
$g_kProjectPropertiesConfig.push kProjectPropertiesConfig

# INPUT LIB
kProjectPropertiesConfig = Hash.new
kProjectPropertiesConfig["projectName"] = "BeInputLib"
kProjectPropertiesConfig["projectPath"] = "BeLibs/"
kProjectPropertiesConfig["projectDependencies"] = ["BeUtilityLib"]
kProjectPropertiesConfig["projectExports"] = ["INPUT_EXPORTS"]
kProjectPropertiesConfig["libraryType"] = ProjectProperties::LIBRARY_TYPE_STATIC
kProjectPropertiesConfig["androidTarget"] = $g_strAndroidTarget
$g_kProjectPropertiesConfig.push kProjectPropertiesConfig

# SOUND LIB
kProjectPropertiesConfig = Hash.new
kProjectPropertiesConfig["projectName"] = "BeSoundLib"
kProjectPropertiesConfig["projectPath"] = "BeLibs/"
kProjectPropertiesConfig["projectDependencies"] = ["BeInputLib"]
kProjectPropertiesConfig["projectExports"] = ["SOUND_EXPORTS"]
kProjectPropertiesConfig["libraryType"] = ProjectProperties::LIBRARY_TYPE_STATIC
kProjectPropertiesConfig["androidTarget"] = $g_strAndroidTarget
$g_kProjectPropertiesConfig.push kProjectPropertiesConfig

# RENDER LIB
kProjectPropertiesConfig = Hash.new
kProjectPropertiesConfig["projectName"] = "BeRenderLib"
kProjectPropertiesConfig["projectPath"] = "BeLibs/"
kProjectPropertiesConfig["projectDependencies"] = ["BeSoundLib"]
kProjectPropertiesConfig["projectExternalDependencies"] = ["DXUtilsLib", "OpenGLEWLib", "FreeImageLib"]
kProjectPropertiesConfig["projectExports"] = ["RENDER_EXPORTS", "GLEW_STATIC", "FREEIMAGE_LIB"]
kProjectPropertiesConfig["libraryType"] = ProjectProperties::LIBRARY_TYPE_STATIC
kProjectPropertiesConfig["androidTarget"] = $g_strAndroidTarget
$g_kProjectPropertiesConfig.push kProjectPropertiesConfig

# FONT LIB
kProjectPropertiesConfig = Hash.new
kProjectPropertiesConfig["projectName"] = "BeFontLib"
kProjectPropertiesConfig["projectPath"] = "BeLibs/"
kProjectPropertiesConfig["projectDependencies"] = ["BeRenderLib"]
kProjectPropertiesConfig["projectExternalDependencies"] = ["FreetypeLib"]
kProjectPropertiesConfig["projectExports"] = ["FONT_EXPORTS", "FT2_BUILD_LIBRARY", "_CRT_SECURE_NO_WARNINGS", "FT_DEBUG_LEVEL_ERROR", "FT_DEBUG_LEVEL_TRACE", "_CRT_SECURE_NO_DEPRECATE"]
kProjectPropertiesConfig["libraryType"] = ProjectProperties::LIBRARY_TYPE_STATIC
kProjectPropertiesConfig["androidTarget"] = $g_strAndroidTarget
$g_kProjectPropertiesConfig.push kProjectPropertiesConfig

# SCENE LIB
kProjectPropertiesConfig = Hash.new
kProjectPropertiesConfig["projectName"] = "BeSceneLib"
kProjectPropertiesConfig["projectPath"] = "BeLibs/"
kProjectPropertiesConfig["projectDependencies"] = ["BeFontLib"]
kProjectPropertiesConfig["projectExports"] = ["SCENE_EXPORTS"]
kProjectPropertiesConfig["libraryType"] = ProjectProperties::LIBRARY_TYPE_STATIC
kProjectPropertiesConfig["androidTarget"] = $g_strAndroidTarget
$g_kProjectPropertiesConfig.push kProjectPropertiesConfig

# APPLICATION LIB
kProjectPropertiesConfig = Hash.new
kProjectPropertiesConfig["projectName"] = "BeApplicationLib"
kProjectPropertiesConfig["projectPath"] = "BeLibs/"
kProjectPropertiesConfig["projectDependencies"] = ["BeSceneLib"]
kProjectPropertiesConfig["projectExternalDependenciesLinkWhole"] = ["DXUtilsLib", "OpenGLEWLib", "FreeImageLib"]
kProjectPropertiesConfig["projectExports"] = ["APPLICATION_EXPORTS"]
kProjectPropertiesConfig["isFinalLibrary"] = true
kProjectPropertiesConfig["libraryType"] = ProjectProperties::LIBRARY_TYPE_SHARED
kProjectPropertiesConfig["androidTarget"] = $g_strAndroidTarget
$g_kProjectPropertiesConfig.push kProjectPropertiesConfig

BE3_MAIN_LIBRARY = kProjectPropertiesConfig["projectName"]
