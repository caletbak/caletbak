/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeSoundLibPredef.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_SOUNDLIBPREDEF_H
#define BE_SOUNDLIBPREDEF_H

// Global BE3 include
#include <BeCommon/BakEngine3Common.h>

#if TARGET_WINDOWS
#ifdef SOUND_IMPORTS
#  define SOUND_API __declspec(dllimport)
#  pragma message("automatic link to BeSoundLib.lib")
#  pragma comment(lib, "BeSoundLib.lib")
#else
#  define SOUND_API __declspec(dllexport)
#endif
#else
#  define SOUND_API
#endif

#ifdef _DEBUG
#  define BE_SOUND_DEBUG(x, ...) _OutputDebugString("[  SOUND DEBUG]: ", true, x, ##__VA_ARGS__)
#  define BE_SOUND_WARNING(x, ...) _OutputDebugString("[! SOUND WARNING]: ", true, x, ##__VA_ARGS__)
#  define BE_SOUND_ERROR(x, ...) _OutputDebugString("[* SOUND ERROR]: ", true, x, ##__VA_ARGS__)
#  define BE_SOUND_PRINT(x, ...) _OutputDebugString("", false, x, ##__VA_ARGS__)
#else
#  define BE_SOUND_DEBUG(x, ...) x
#  define BE_SOUND_WARNING(x, ...) x
#  define BE_SOUND_ERROR(x, ...) x
#  define BE_SOUND_PRINT(x, ...) x
#endif

// External includes
#include <BeCoreLib/Pch/BeCoreLib.h>

#endif // #ifndef BE_SOUNDLIBPREDEF_H

