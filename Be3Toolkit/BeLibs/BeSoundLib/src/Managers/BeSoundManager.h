/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeSoundManager.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BESOUNDMANAGER_H
#define BE_BESOUNDMANAGER_H

#include <BeSoundLib/Pch/BeSoundLibPredef.h>

// Forward declarations
class BeAudioSystem;

/////////////////////////////////////////////////
/// \class  BeSoundManager
/// \brief  Manager for playing sounds
/////////////////////////////////////////////////
class SOUND_API BeSoundManager : public BeMemoryObject
{
  // Friendly class definition
  friend class BeAudioSystem;

  public:

    /// \brief  Constructor
    BeSoundManager (BeAudioSystem* pAudioSystem);

    /// \brief  Destructor
    ~BeSoundManager ();

    // \brief  Methods
    // {

      /// \brief  Prepares a new sound in the sound manager and returns its ID
      BeInt PrepareSound (BeString16 strSoundPath, BeInt iSoundType, BeInt iMaxInstances);

      /// \brief  Releases a sound previously prepared
      void ReleaseSound (BeInt iSoundID);

      /// \brief  Plays a sound previously prepared
      void PlaySound (BeInt iSoundID);

      /// \brief  Plays a sound previously prepared and then releases the resource
      void PlaySoundAndRelease (BeInt iSoundID);

      /// \brief  Stops all the instances of a sound
      void StopSound (BeInt iSoundID);

    // }



    // \brief  Getters
    // {

      /// \brief  Gets the volume of a sound resource
      BeFloat GetVolume (BeInt iSoundID);

    // }



    // \brief  Setters
    // {

      /// \brief  Sets the volume of all the current or future instances of a sound resource
      void SetVolume (BeInt iSoundID, BeFloat fVolume);

    // }

  private:

    /// \brief  Updates the sound manager
    void Update (void);

    /// \brief  The Audio System
    BeAudioSystem* m_pAudioSystem;

};

#endif // #ifndef BE_BEINPUTMOUSE_H
