/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeMusicManager.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEMUSICMANAGER_H
#define BE_BEMUSICMANAGER_H

#include <BeSoundLib/Pch/BeSoundLibPredef.h>

// Forward declarations
class BeAudioSystem;

/////////////////////////////////////////////////
/// \class  BeMusicManager
/// \brief  Manager for playing music streams
/////////////////////////////////////////////////
class SOUND_API BeMusicManager : public BeMemoryObject
{
  // Friendly class definition
  friend class BeAudioSystem;

  public:

    /// \brief  Constructor
    BeMusicManager (BeAudioSystem* pAudioSystem);

    /// \brief  Destructor
    ~BeMusicManager ();

    // \brief  Methods
    // {

      /// \brief  Prepares a new music in the music manager and returns its ID
      BeInt PrepareMusic (BeString16 strSoundPath);

      /// \brief  Releases a music previously prepared
      void ReleaseMusic (BeInt iMusicID);

      /// \brief  Plays a music previously prepared
      void PlayMusic (BeInt iMusicID);

      /// \brief  Stops a music previously prepared
      void StopMusic (BeInt iMusicID);

      /// \brief  Makes a FadeIn volume effect to music - Default is from current volume to fFinishVolume (fInitVolume ignored)
      void FadeIn (BeInt iMusicID, BeInt iTimeMS, BeBoolean bFromCurrentVolume = BE_TRUE, BeFloat fInitVolume = 0.0f, BeFloat fFinishVolume = 1.0f);

      /// \brief  Makes a FadeOut volume effect to music - Default is to mute the music from current volume (fFinishVolume ignored)
      void FadeOut (BeInt iMusicID, BeInt iTimeMS, BeBoolean bAlsoReleaseMusic = BE_FALSE, BeBoolean bUntilMute = BE_TRUE, BeFloat fFinishVolume = 0.0f);

    // }



    // \brief  Getters
    // {

      /// \brief  Gets the volume of a music resource
      BeFloat GetVolume (BeInt iMusicID);

    // }



    // \brief  Setters
    // {

      /// \brief  Sets the volume to a music resource
      void SetVolume (BeInt iMusicID, BeFloat fVolume);

    // }

  private:

    /// \brief  Updates the music manager
    void Update (void);

    /// \brief  The Audio System
    BeAudioSystem* m_pAudioSystem;



    enum EFadeMusicType
    {
      FADE_IN,
      FADE_OUT,
      FADE_OUT_AND_RELEASE
    };

    struct BeFadeMusicProcess
    {
      EFadeMusicType m_eFadeMusicType;
      BeInt m_iMusicID;
      BeTimer m_kTimer;
      BeFloat m_fInitVolume;
      BeFloat m_fFinishVolume;
      BeBoolean m_bAlsoReleaseMusic;
    };
    typedef BeSmartPointer<BeFadeMusicProcess> BeFadeMusicProcessPtr;

    /// \brief  The Fade Music processes array type
    typedef std::vector<BeFadeMusicProcessPtr> FadeMusicProcessesArray;

    /// \brief  The Fade Music processes
    FadeMusicProcessesArray m_kFadeMusicProcesses;

};

#endif // #ifndef BE_BEMUSICMANAGER_H
