/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeMusicManager.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <Managers/BeMusicManager.h>

#include <BeAudioSystem.h>

/////////////////////////////////////////////////////////////////////////////
BeMusicManager::BeMusicManager(BeAudioSystem* pAudioSystem)
{
  m_pAudioSystem = pAudioSystem;
}
/////////////////////////////////////////////////////////////////////////////
BeMusicManager::~BeMusicManager()
{
  m_kFadeMusicProcesses.clear ();

  m_pAudioSystem = NULL;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeInt BeMusicManager::PrepareMusic (BeString16 strSoundPath)
{
  return m_pAudioSystem->PrepareAudioSound (strSoundPath, BeAudioSystem::SOUND_2D | BeAudioSystem::SOUND_LOOPING, 1);
}
/////////////////////////////////////////////////////////////////////////////
void BeMusicManager::ReleaseMusic (BeInt iMusicID)
{
  m_pAudioSystem->ReleaseAudioSound (iMusicID);
}
/////////////////////////////////////////////////////////////////////////////
void BeMusicManager::PlayMusic (BeInt iMusicID)
{
  m_pAudioSystem->PlayAudioSound (iMusicID);
}
/////////////////////////////////////////////////////////////////////////////
void BeMusicManager::StopMusic (BeInt iMusicID)
{
  m_pAudioSystem->StopAudioSound (iMusicID);
}
/////////////////////////////////////////////////////////////////////////////
void BeMusicManager::FadeIn (BeInt iMusicID, BeInt iTimeMS, BeBoolean bFromCurrentVolume, BeFloat fInitVolume, BeFloat fFinishVolume)
{
  BeBoolean bAvoidFadeIn = BE_FALSE;

  // If there is already a Fade IN for this music being processed we avoid a new one
  if (!bAvoidFadeIn)
  {
    for (BeUInt i = 0; i < m_kFadeMusicProcesses.size (); ++i)
    {
      if (m_kFadeMusicProcesses[i] != NULL && m_kFadeMusicProcesses[i]->m_iMusicID == iMusicID && m_kFadeMusicProcesses[i]->m_eFadeMusicType == BeMusicManager::FADE_IN)
      {
        bAvoidFadeIn = BE_TRUE;
      }
    }
  }

  if (!bAvoidFadeIn)
  {
    // If there is any Fade OUT being processed we delete it
    for (BeUInt i = 0; i < m_kFadeMusicProcesses.size (); ++i)
    {
      if (m_kFadeMusicProcesses[i] != NULL && m_kFadeMusicProcesses[i]->m_iMusicID == iMusicID && m_kFadeMusicProcesses[i]->m_eFadeMusicType == BeMusicManager::FADE_OUT)
      {
        m_kFadeMusicProcesses[i] = NULL;
      }
    }

    BeFadeMusicProcessPtr spFadeProcess = BeNew BeFadeMusicProcess ();
    spFadeProcess->m_eFadeMusicType = BeMusicManager::FADE_IN;
    spFadeProcess->m_iMusicID = iMusicID;
    spFadeProcess->m_kTimer.Start (iTimeMS);
    if (bFromCurrentVolume)
    {
      spFadeProcess->m_fInitVolume = GetVolume (iMusicID);
    }
    else
    {
      spFadeProcess->m_fInitVolume = fInitVolume;
      SetVolume (iMusicID, spFadeProcess->m_fInitVolume);
    }
    spFadeProcess->m_fFinishVolume = fFinishVolume;
    spFadeProcess->m_bAlsoReleaseMusic = BE_FALSE;

    for (BeUInt i = 0; i < m_kFadeMusicProcesses.size (); ++i)
    {
      if (m_kFadeMusicProcesses[i] == NULL)
      {
        m_kFadeMusicProcesses[i] = spFadeProcess;
        return;
      }
    }

    m_kFadeMusicProcesses.push_back (spFadeProcess);
  }
}
/////////////////////////////////////////////////////////////////////////////
void BeMusicManager::FadeOut (BeInt iMusicID, BeInt iTimeMS, BeBoolean bAlsoReleaseMusic, BeBoolean bUntilMute, BeFloat fFinishVolume)
{
  
  BeBoolean bAvoidFadeOut = BE_FALSE;

  // If there is already a Fade OUT for this music being processed we avoid a new one
  if (!bAvoidFadeOut)
  {
    for (BeUInt i = 0; i < m_kFadeMusicProcesses.size (); ++i)
    {
      if (m_kFadeMusicProcesses[i] != NULL && m_kFadeMusicProcesses[i]->m_iMusicID == iMusicID && m_kFadeMusicProcesses[i]->m_eFadeMusicType == BeMusicManager::FADE_OUT)
      {
        bAvoidFadeOut = BE_TRUE;
      }
    }
  }

  if (!bAvoidFadeOut)
  {
    // If there is any Fade IN being processed we delete it
    for (BeUInt i = 0; i < m_kFadeMusicProcesses.size (); ++i)
    {
      if (m_kFadeMusicProcesses[i] != NULL && m_kFadeMusicProcesses[i]->m_iMusicID == iMusicID && m_kFadeMusicProcesses[i]->m_eFadeMusicType == BeMusicManager::FADE_IN)
      {
        m_kFadeMusicProcesses[i] = NULL;
      }
    }

    BeFadeMusicProcessPtr spFadeProcess = BeNew BeFadeMusicProcess ();
    spFadeProcess->m_eFadeMusicType = BeMusicManager::FADE_OUT;
    spFadeProcess->m_iMusicID = iMusicID;
    spFadeProcess->m_kTimer.Start (iTimeMS);
    spFadeProcess->m_fInitVolume = GetVolume (iMusicID);
    if (bUntilMute)
    {
      spFadeProcess->m_fFinishVolume = 0.0f;
    }
    else
    {
      spFadeProcess->m_fFinishVolume = fFinishVolume;
    }
    spFadeProcess->m_bAlsoReleaseMusic = bAlsoReleaseMusic;

    for (BeUInt i = 0; i < m_kFadeMusicProcesses.size (); ++i)
    {
      if (m_kFadeMusicProcesses[i] == NULL)
      {
        m_kFadeMusicProcesses[i] = spFadeProcess;
        return;
      }
    }

    m_kFadeMusicProcesses.push_back (spFadeProcess);
  }
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeFloat BeMusicManager::GetVolume (BeInt iMusicID)
{
  return m_pAudioSystem->GetAudioSoundVolume (iMusicID);
}
/////////////////////////////////////////////////////////////////////////////
void BeMusicManager::SetVolume (BeInt iMusicID, BeFloat fVolume)
{
  m_pAudioSystem->SetAudioSoundVolume (iMusicID, fVolume);
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeMusicManager::Update (void)
{
  for (BeUInt i = 0; i < m_kFadeMusicProcesses.size (); ++i)
  {
    if (m_kFadeMusicProcesses[i] != NULL)
    {
      SetVolume (m_kFadeMusicProcesses[i]->m_iMusicID, m_kFadeMusicProcesses[i]->m_fInitVolume + ((m_kFadeMusicProcesses[i]->m_fFinishVolume - m_kFadeMusicProcesses[i]->m_fInitVolume) * m_kFadeMusicProcesses[i]->m_kTimer.GetDeltaPI2 ()));

      if (m_kFadeMusicProcesses[i]->m_kTimer.IsFinished ())
      {
        if (m_kFadeMusicProcesses[i]->m_bAlsoReleaseMusic)
        {
          ReleaseMusic (m_kFadeMusicProcesses[i]->m_iMusicID);
        }
        m_kFadeMusicProcesses[i] = NULL;
      }
    }
  }
}
/////////////////////////////////////////////////////////////////////////////

