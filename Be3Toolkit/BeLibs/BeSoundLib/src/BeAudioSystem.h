/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeAudioSystem.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEAUDIOSYSTEM_H
#define BE_BEAUDIOSYSTEM_H

#include <BeSoundLib/Pch/BeSoundLibPredef.h>

// Forward declarations
class BeMusicManager;
class BeSoundManager;

/////////////////////////////////////////////////
/// \class  BeAudioSystem
/// \brief  This class manages the selected audio system
/////////////////////////////////////////////////
class SOUND_API BeAudioSystem : public BeMemoryObject
{
  // Friendly class definition
  friend class BeMusicManager;
  friend class BeSoundManager;

  public:

    enum ESoundType
    {
      SOUND_2D = 0x01,
      SOUND_3D = 0x02,

      SOUND_LOOPING = 0x04
    };

    /// \brief  Constructor
    BeAudioSystem (void);

    /// \brief  Destructor
    ~BeAudioSystem ();



    // \brief  Methods
    // {

      /// \brief  Updates the Audio system
      void Update (void);

    // }



    // \brief  Getters
    // {

      /// \brief  Returns the Music Manager
      BeMusicManager* GetMusicManager (void);

      /// \brief  Returns the Sound Manager
      BeSoundManager* GetSoundManager (void);

    // }

  private:

    // \brief  Methods
    // {

      /// \brief  Prepares a new sound in the Audio system
      BeInt PrepareAudioSound (BeString16 strSoundPath, BeInt iSoundType, BeInt iMaxInstances);

      /// \brief  Releases a previously prepared sound in the Audio system
      void ReleaseAudioSound (BeInt iSoundID);

      /// \brief  Plays a new instance of a sound in the Audio System
      void PlayAudioSound (BeInt iSoundID);

      /// \brief  Plays a new instance of a sound in the Audio System and then releases it
      void PlayAudioSoundAndRelease (BeInt iSoundID);

      /// \brief  Stops all the instances of a sound in the Audio System
      void StopAudioSound (BeInt iSoundID);

    // }

    // \brief  Getters
    // {

      /// \brief  Sets the volume of all the current or future instances of a sound resource
      void SetAudioSoundVolume (BeInt iSoundID, BeFloat fVolume);

    // }

    // \brief  Setters
    // {

      /// \brief  Sets the volume of all the current or future instances of a sound resource
      BeFloat GetAudioSoundVolume (BeInt iSoundID);

    // }



    /// \brief  The Music Manager
    BeMusicManager* m_pMusicManager;

    /// \brief  The Sound Manager
    BeSoundManager* m_pSoundManager;

    // Forward declarations
    class BeAudioSystemImpl;
    class BeAudioResourceImpl;

    // Friendly class definition
    friend class BeAudioResourceImpl;

    /// \brief  Audio system API Implementation
    static BeAudioSystemImpl* m_pAudioSystemImpl;

    /// \brief  The BeAudioResourceImpl array type
    typedef std::vector<BeAudioResourceImpl*> AudioResourceImplArray;

    /// \brief  The audio resources previously prepared
    AudioResourceImplArray m_kAudioResources;

};

#endif // #ifndef BE_BEAUDIOSYSTEM_H
