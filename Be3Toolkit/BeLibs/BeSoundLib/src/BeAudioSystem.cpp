/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeAudioSystem.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <BeAudioSystem.h>

#include <Managers/BeMusicManager.h>
#include <Managers/BeSoundManager.h>

#if TARGET_WINDOWS
#define SOUND_USING_FMOD 1
#else
#define SOUND_USING_FMOD 0
#endif

#if SOUND_USING_FMOD
// FMOD libs
#pragma comment (lib, "fmod_vc.lib") 

// FMOD include files
#include <Include/FMOD/fmod.hpp>

void ERRCHECK(FMOD_RESULT result)
{
  if (result != FMOD_OK)
  {
    BE_SOUND_ERROR ("FMOD error %d", result);
  }
}

FMOD_RESULT F_CALLBACK FMODCustomOpen (const BeChar8* name, BeInt unicode, BeUInt* filesize, BeVoidP* handle, BeVoidP userdata)
{
  BE_UNUSED (unicode);
  BE_UNUSED (userdata);

  if (name)
  {
    FILE *fp;

    fopen_s(&fp, name, "rb");
    if (!fp)
    {
        return FMOD_ERR_FILE_NOTFOUND;
    }

    fseek(fp, 0, SEEK_END);
    *filesize = ftell(fp);
    fseek(fp, 0, SEEK_SET);

    *handle = fp;
  }

  return FMOD_OK;
}

FMOD_RESULT F_CALLBACK FMODCustomClose (BeVoidP handle, BeVoidP userdata)
{
  BE_UNUSED (userdata);

  if (!handle)
  {
      return FMOD_ERR_INVALID_PARAM;
  }

  fclose((FILE *)handle);

  return FMOD_OK;
}

FMOD_RESULT F_CALLBACK FMODCustomRead (BeVoidP handle, BeVoidP buffer, BeUInt sizebytes, BeUInt* bytesread, void *userdata)
{
  BE_UNUSED (userdata);

  if (!handle)
  {
      return FMOD_ERR_INVALID_PARAM;
  }

  if (bytesread)
  {
      *bytesread = (int)fread(buffer, 1, sizebytes, (FILE *)handle);
  
      if (*bytesread < sizebytes)
      {
          return FMOD_ERR_FILE_EOF;
      }
  }

  return FMOD_OK;
}

FMOD_RESULT F_CALLBACK FMODCustomSeek (BeVoidP handle, BeUInt pos, BeVoidP userdata)
{
  BE_UNUSED (userdata);

  if (!handle)
  {
      return FMOD_ERR_INVALID_PARAM;
  }

  fseek((FILE *)handle, pos, SEEK_SET);

  return FMOD_OK;
}
#endif



/////////////////////////////////////////////////////////////////////////////
class SOUND_API BeAudioSystem::BeAudioSystemImpl : public BeMemoryObject
{
  public:

    /// \brief  Constructor
    BeAudioSystemImpl ();

    /// \brief  Destructor
    ~BeAudioSystemImpl ();

    /// \brief  Release
    void Release (void);



    // \brief  Methods
    // {

      /// \brief  Updates the Audio system implementation
      void Update (void);

    // }

  private:

#if SOUND_USING_FMOD
    FMOD::System* m_pFMODSystem;
#endif

    friend class BeAudioSystem::BeAudioResourceImpl;

};
/////////////////////////////////////////////////////////////////////////////
BeAudioSystem::BeAudioSystemImpl::BeAudioSystemImpl ()
{
#if SOUND_USING_FMOD
  FMOD_RESULT kFMODResult;

  kFMODResult = FMOD::System_Create (&m_pFMODSystem);
  ERRCHECK (kFMODResult);

  BeUInt iFMODVersion;
  kFMODResult = m_pFMODSystem->getVersion (&iFMODVersion);
  ERRCHECK (kFMODResult);

  if (iFMODVersion < FMOD_VERSION)
  {
    BE_SOUND_ERROR ("FMOD lib version %08x doesn't match header version %08x", iFMODVersion, FMOD_VERSION);
  }

  kFMODResult = m_pFMODSystem->init (200, FMOD_INIT_NORMAL, NULL);
  ERRCHECK (kFMODResult);

  kFMODResult = m_pFMODSystem->setFileSystem (FMODCustomOpen, FMODCustomClose, FMODCustomRead, FMODCustomSeek, NULL, NULL, 2048);
  ERRCHECK(kFMODResult);
#endif
}
/////////////////////////////////////////////////////////////////////////////
BeAudioSystem::BeAudioSystemImpl::~BeAudioSystemImpl ()
{
  Release ();
}
/////////////////////////////////////////////////////////////////////////////
void BeAudioSystem::BeAudioSystemImpl::Release (void)
{
#if SOUND_USING_FMOD
  FMOD_RESULT kFMODResult;

  kFMODResult = m_pFMODSystem->close ();
  ERRCHECK(kFMODResult);

  kFMODResult = m_pFMODSystem->release ();
  ERRCHECK(kFMODResult);
#endif
}
/////////////////////////////////////////////////////////////////////////////
void BeAudioSystem::BeAudioSystemImpl::Update (void)
{
#if SOUND_USING_FMOD
  FMOD_RESULT kFMODResult;

  kFMODResult = m_pFMODSystem->update ();
  ERRCHECK(kFMODResult);
#endif
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
class SOUND_API BeAudioSystem::BeAudioResourceImpl : public BeMemoryObject
{
  public:

    /// \brief  Constructor
    BeAudioResourceImpl (BeString16 strSoundPath, BeInt eSoundType, BeInt iMaxInstances);

    /// \brief  Destructor
    ~BeAudioResourceImpl ();

    // \brief  Methods
    // {

      /// \brief  Updates the instances of the sound resource
      BeBoolean Update (void);

      /// \brief  Plays a new instance of a sound resource
      void Play (void);

      /// \brief  Plays a new instance of a sound resource and then releases the resource
      void PlayAndRelease (void);

      /// \brief  Stops all the instances of a sound resource
      void Stop (void);

    // }

    // \brief  Setters
    // {

      /// \brief  Sets the volume for current or future instances of a sound resource
      void SetVolume (BeFloat fVolume);

    // }

    // \brief  Getters
    // {

      /// \brief  Gets the volume for current or future instances of a sound resource
      BeFloat GetVolume (void);

    // }

#if SOUND_USING_FMOD
    FMOD::Sound*   m_pSound;
    FMOD::Channel** m_pChannel;
#endif

    /// \brief  Max sound instances
    BeInt m_iMaxInstances;

    /// \brief  Sound type - Combination flag from BeAudioSystem::ESoundType
    BeInt m_iSoundType;

    /// \brief  Sound volume for instances
    BeFloat m_fVolume;

    /// \brief  Releases the audio resource automatically after playing?
    BeBoolean m_bReleaseResourceAfterPlaying;
};
/////////////////////////////////////////////////////////////////////////////
BeAudioSystem::BeAudioResourceImpl::BeAudioResourceImpl (BeString16 strSoundPath, BeInt eSoundType, BeInt iMaxInstances)
:
  m_iMaxInstances (iMaxInstances),
  m_iSoundType (eSoundType),
  m_fVolume (1.0f),
  m_bReleaseResourceAfterPlaying (BE_FALSE)
{
#if SOUND_USING_FMOD
  BeInt iFMODFlags = FMOD_HARDWARE;

  // 2D/3D sound?
  if (m_iSoundType & BeAudioSystem::SOUND_2D)
  {
    iFMODFlags |= FMOD_2D;
  }
  else if (m_iSoundType & BeAudioSystem::SOUND_3D)
  {
    iFMODFlags |= FMOD_3D;
  }
  // Looping?
  if (m_iSoundType & BeAudioSystem::SOUND_LOOPING)
  {
    iFMODFlags |= FMOD_LOOP_NORMAL;
  }
  else
  {
    iFMODFlags |= FMOD_LOOP_OFF;
  }

  FMOD_RESULT kFMODResult;
  kFMODResult = BeAudioSystem::m_pAudioSystemImpl->m_pFMODSystem->createSound(ConvertString16To8(strSoundPath).ToRaw(), iFMODFlags, 0, &m_pSound);
  ERRCHECK(kFMODResult);

  m_pChannel = BeNew FMOD::Channel* [m_iMaxInstances];
  for (BeInt i = 0; i < m_iMaxInstances; ++i)
  {
    m_pChannel[i] = NULL;
  }
#endif
}
/////////////////////////////////////////////////////////////////////////////
BeAudioSystem::BeAudioResourceImpl::~BeAudioResourceImpl ()
{
#if SOUND_USING_FMOD
  if (m_pChannel)
  {
    for (BeInt i = 0; i < m_iMaxInstances; ++i)
    {
      if (m_pChannel[i])
      {
        BeBoolean isPlaying;
        m_pChannel[i]->isPlaying (&isPlaying);
        if (isPlaying)
        {
          m_pChannel[i]->stop ();
        }

        m_pChannel[i] = NULL;
      }
    }

    BeDeleteArray m_pChannel;
  }

  FMOD_RESULT kFMODResult;
  kFMODResult = m_pSound->release ();
  ERRCHECK (kFMODResult);
#endif
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeAudioSystem::BeAudioResourceImpl::Update (void)
{
  BeBoolean bChannelsActive = BE_FALSE;

#if SOUND_USING_FMOD
  for (BeInt i = 0; i < m_iMaxInstances; ++i)
  {
    if (m_pChannel[i])
    {
      BeBoolean isPlaying;
      m_pChannel[i]->isPlaying (&isPlaying);
      if (!isPlaying)
      {
        m_pChannel[i] = NULL;
      }
      else
      {
        bChannelsActive = BE_TRUE;
      }
    }
  }
#endif

  return bChannelsActive;
}
/////////////////////////////////////////////////////////////////////////////
void BeAudioSystem::BeAudioResourceImpl::Play (void)
{
#if SOUND_USING_FMOD
  for (BeInt i = 0; i < m_iMaxInstances; ++i)
  {
    if (m_pChannel[i] == NULL)
    {
      FMOD_RESULT kFMODResult;
      kFMODResult = BeAudioSystem::m_pAudioSystemImpl->m_pFMODSystem->playSound (m_pSound, 0, BE_TRUE, &m_pChannel[i]);
      m_pChannel[i]->setVolume (m_fVolume);
      ERRCHECK(kFMODResult);

      kFMODResult = m_pChannel[i]->setPaused (BE_FALSE);
      ERRCHECK(kFMODResult);
      break;
    }
  }
#endif
}
/////////////////////////////////////////////////////////////////////////////
void BeAudioSystem::BeAudioResourceImpl::PlayAndRelease (void)
{
  Play ();
  m_bReleaseResourceAfterPlaying = BE_TRUE;
}
/////////////////////////////////////////////////////////////////////////////
void BeAudioSystem::BeAudioResourceImpl::Stop (void)
{
#if SOUND_USING_FMOD
  for (BeInt i = 0; i < m_iMaxInstances; ++i)
  {
    if (m_pChannel[i])
    {
      BeBoolean isPlaying;
      m_pChannel[i]->isPlaying (&isPlaying);
      if (isPlaying)
      {
        m_pChannel[i]->stop ();
      }

      m_pChannel[i] = NULL;
    }
  }
#endif
}
/////////////////////////////////////////////////////////////////////////////
BeFloat BeAudioSystem::BeAudioResourceImpl::GetVolume ()
{
  return m_fVolume;
}
/////////////////////////////////////////////////////////////////////////////
void BeAudioSystem::BeAudioResourceImpl::SetVolume (BeFloat fVolume)
{
  m_fVolume = fVolume;

#if SOUND_USING_FMOD
  for (BeInt i = 0; i < m_iMaxInstances; ++i)
  {
    if (m_pChannel[i])
    {
      BeBoolean isPlaying;
      m_pChannel[i]->isPlaying (&isPlaying);
      if (isPlaying)
      {
        m_pChannel[i]->setVolume (m_fVolume);
      }
    }
  }
#endif
}
/////////////////////////////////////////////////////////////////////////////







/////////////////////////////////////////////////////////////////////////////
BeAudioSystem::BeAudioSystemImpl* BeAudioSystem::m_pAudioSystemImpl = NULL;
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeAudioSystem::BeAudioSystem(void)
{
  m_pAudioSystemImpl = BeNew BeAudioSystemImpl ();
  m_pMusicManager = BeNew BeMusicManager (this);
  m_pSoundManager = BeNew BeSoundManager (this);
}
/////////////////////////////////////////////////////////////////////////////
BeAudioSystem::~BeAudioSystem()
{
  if ( m_pMusicManager != NULL )
  {
    BeDelete m_pMusicManager;
    m_pMusicManager = NULL;
  }
  if ( m_pSoundManager != NULL )
  {
    BeDelete m_pSoundManager;
    m_pSoundManager = NULL;
  }

  for (BeUInt i = 0; i < m_kAudioResources.size (); ++i)
  {
    BeDelete m_kAudioResources[i];
  }
  m_kAudioResources.clear ();

  if ( m_pAudioSystemImpl != NULL )
  {
    BeDelete m_pAudioSystemImpl;
    m_pAudioSystemImpl = NULL;
  }
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeAudioSystem::Update (void)
{
  m_pMusicManager->Update ();
  m_pSoundManager->Update ();

  m_pAudioSystemImpl->Update ();

  for (BeUInt i = 0; i < m_kAudioResources.size (); ++i)
  {
    if (m_kAudioResources[i] != NULL)
    {
      BeBoolean bNoChannelsActive = m_kAudioResources[i]->Update ();
      if (!bNoChannelsActive && m_kAudioResources[i]->m_bReleaseResourceAfterPlaying)
      {
        ReleaseAudioSound (i);
      }
    }
  }
}
/////////////////////////////////////////////////////////////////////////////
BeMusicManager* BeAudioSystem::GetMusicManager (void)
{
  return m_pMusicManager;
}
/////////////////////////////////////////////////////////////////////////////
BeSoundManager* BeAudioSystem::GetSoundManager (void)
{
  return m_pSoundManager;
}
/////////////////////////////////////////////////////////////////////////////
BeInt BeAudioSystem::PrepareAudioSound (BeString16 strSoundPath, BeInt iSoundType, BeInt iMaxInstances)
{
  BeAudioResourceImpl* pAudioResource = BeNew BeAudioResourceImpl (strSoundPath, iSoundType, iMaxInstances);

  for (BeUInt i = 0; i < m_kAudioResources.size (); ++i)
  {
    if (m_kAudioResources[i] == NULL)
    {
      m_kAudioResources[i] = pAudioResource;
      return i;
    }
  }

  BeInt iResourceIndex = m_kAudioResources.size ();
  m_kAudioResources.push_back (pAudioResource);

  return iResourceIndex;
}
/////////////////////////////////////////////////////////////////////////////
void BeAudioSystem::ReleaseAudioSound (BeInt iSoundID)
{
  if (iSoundID >= 0 && iSoundID < (BeInt) m_kAudioResources.size () && m_kAudioResources[iSoundID] != NULL)
  {
    BeInt iCurrentIndex = 0;

    AudioResourceImplArray::iterator kAudioResourcesBegin( m_kAudioResources.begin() );
    AudioResourceImplArray::const_iterator kAudioResourcesEnd( m_kAudioResources.end() );
    while( kAudioResourcesBegin != kAudioResourcesEnd )
    {
      if (iCurrentIndex == iSoundID)
      {
        BeAudioResourceImpl* pAudioResource = static_cast<BeAudioResourceImpl*>(*kAudioResourcesBegin);

        BeDelete pAudioResource;

        m_kAudioResources[iSoundID] = NULL;
        break;
      }

      ++iCurrentIndex;
      ++kAudioResourcesBegin;
    }
  }
}
/////////////////////////////////////////////////////////////////////////////
void BeAudioSystem::PlayAudioSound (BeInt iSoundID)
{
  if (iSoundID >= 0 && iSoundID < (BeInt) m_kAudioResources.size () && m_kAudioResources[iSoundID] != NULL)
  {
    BeAudioResourceImpl* pAudioResource = m_kAudioResources[iSoundID];
    pAudioResource->Play ();
  }
}
void BeAudioSystem::PlayAudioSoundAndRelease (BeInt iSoundID)
{
  if (iSoundID >= 0 && iSoundID < (BeInt) m_kAudioResources.size () && m_kAudioResources[iSoundID] != NULL)
  {
    BeAudioResourceImpl* pAudioResource = m_kAudioResources[iSoundID];
    pAudioResource->PlayAndRelease ();
  }
}
/////////////////////////////////////////////////////////////////////////////
void BeAudioSystem::StopAudioSound (BeInt iSoundID)
{
  if (iSoundID >= 0 && iSoundID < (BeInt) m_kAudioResources.size () && m_kAudioResources[iSoundID] != NULL)
  {
    BeAudioResourceImpl* pAudioResource = m_kAudioResources[iSoundID];
    pAudioResource->Stop ();
  }
}
/////////////////////////////////////////////////////////////////////////////
BeFloat BeAudioSystem::GetAudioSoundVolume (BeInt iSoundID)
{
  if (iSoundID >= 0 && iSoundID < (BeInt) m_kAudioResources.size () && m_kAudioResources[iSoundID] != NULL)
  {
    BeAudioResourceImpl* pAudioResource = m_kAudioResources[iSoundID];
    return pAudioResource->GetVolume ();
  }

  return 0.0f;
}
/////////////////////////////////////////////////////////////////////////////
void BeAudioSystem::SetAudioSoundVolume (BeInt iSoundID, BeFloat fVolume)
{
  if (iSoundID >= 0 && iSoundID < (BeInt) m_kAudioResources.size () && m_kAudioResources[iSoundID] != NULL)
  {
    BeAudioResourceImpl* pAudioResource = m_kAudioResources[iSoundID];
    pAudioResource->SetVolume (fVolume);
  }
}
/////////////////////////////////////////////////////////////////////////////

