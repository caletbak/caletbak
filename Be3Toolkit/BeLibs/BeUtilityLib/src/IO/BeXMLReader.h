/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeXMLReader.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEXMLREADER_H
#define BE_BEXMLREADER_H

#include <BeUtilityLib/Pch/BeUtilityLibPredef.h>

/////////////////////////////////////////////////
/// \class BeXMLReader
/// \brief High speed XML parser and writer
/////////////////////////////////////////////////
class UTILITY_API BeXMLReader : public BeMemoryObject
{

  public:

    /// \brief Constructor
    BeXMLReader();
    /// \brief Constructor loading a file
    BeXMLReader(const BeString16& strXMLFilePath);
    /// \brief Destructor
    ~BeXMLReader();

    // Getters
    // {

      /// \brief  Gets the node child count
      BeInt GetNodeChildCount(void) const;

      /// \brief  Parses the node value to String
      BeString8 ParseNodeValueString(void);

      /// \brief  Parses the node value to Boolean
      BeBoolean ParseNodeValueBoolean(void);

      /// \brief  Parses the node value to Int
      BeInt ParseNodeValueInt(void);

      /// \brief  Parses the node value to Float
      BeFloat ParseNodeValueFloat(void);

      /// \brief  Parses the node value to Vector2D
      BeVector2D ParseNodeValueVector2D(void);

      /// \brief  Parses the node value to Vector3D
      BeVector3D ParseNodeValueVector3D(void);

      /// \brief  Parses the node value to Vector4D
      BeVector4D ParseNodeValueVector4D(void);

      /// \brief  Gets the node name
      BeString8 GetNodeName() const;



      /// \brief  Gets the node attribute count
      BeInt GetNodeAttributeCount(void) const;

      /// \brief  Parses the node attribute value strAttribName to String
      BeString8 ParseNodeAttributeString(const BeString8& strAttribName);

      /// \brief  Parses the node attribute value strAttribName to Boolean
      BeBoolean ParseNodeAttributeBoolean(const BeString8& strAttribName);

      /// \brief  Parses the node attribute value strAttribName to Int
      BeInt ParseNodeAttributeInt(const BeString8& strAttribName);

      /// \brief  Parses the node attribute value strAttribName to Float
      BeFloat ParseNodeAttributeFloat(const BeString8& strAttribName);

      /// \brief  Parses the node attribute value strAttribName to Vector2D
      BeVector2D ParseNodeAttributeVector2(const BeString8& strAttribName);

      /// \brief  Parses the node attribute value strAttribName to Vector3D
      BeVector3D ParseNodeAttributeVector3(const BeString8& strAttribName);

      /// \brief  Parses the node attribute value strAttribName to Vector4D
      BeVector4D ParseNodeAttributeVector4(const BeString8& strAttribName);

      /// \brief  Gets the current attribute name
      BeString8 GetNodeAttributeName() const;

    // }


    // XML Navigation
    // {

      /// \brief  Moves to the first root node
      BeBoolean MoveToXMLRoot(void);

      /// \brief  Moves to the first child
      BeBoolean MoveToFirstChild(void);

      /// \brief  Moves to the last child
      BeBoolean MoveToLastChild(void);

      /// \brief  Moves to the parent node
      BeBoolean MoveToParent(void);

      /// \brief  Moves to the next sibling
      BeBoolean MoveToNextSibling(void);

      /// \brief  Moves to the previous sibling
      BeBoolean MoveToPreviousSibling(void);

      /// \brief  Moves to the next node attribute
      BeBoolean MoveToNextAttribute(void);

      /// \brief  Moves to the previous node attribute
      BeBoolean MoveToPreviousAttribute(void);

      /// \brief  Moves to the node the XML path points. If it does not exists, the current node remains the same.
      /// The delimiter parameter is for setting how you are dividing the path. The search begins in the current node.
      /// Add "./" at the path beginning for searching from root node
      BeBoolean SearchXMLNode(const BeString8& strXMLPath, const BeString8& strDelimeters = "/");

    // }


    // Methods
    // {

      /// \brief  Creates DOM tree from file
      void ParseXMLFile(const BeString16& strXMLFilePath);

      /// \brief  Prints to console all the DOM tree for debugging purposes
      void PrintDOMTree(void);

    // }

  private:

    // Forward declarations
    class BeXMLReaderImpl;

    /// \brief  XML reader API Implementation
    BeXMLReaderImpl* m_pXMLReaderImpl;

};



#endif // #ifndef BE_BEXMLREADER_H
