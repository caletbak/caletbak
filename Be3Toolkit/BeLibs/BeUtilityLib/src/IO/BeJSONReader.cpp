/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeJSONReader.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <IO/BeJSONReader.h>
#include <Generic/BeParsingUtils.h>

#include <IO/BeFileTool.h>

#define JSON_USING_RAPIDJSON 1

#if JSON_USING_RAPIDJSON
// RapidJSON include files
#include <Include/RapidJSON/document.h>
#endif

/////////////////////////////////////////////////////////////////////////////
class UTILITY_API BeJSONReader::BeJSONReaderImpl
{

  public:

    typedef BeChar8 CHAR_TYPE;

    /// \brief  Constructor
    BeJSONReaderImpl();
    /// \brief  Destructor
    ~BeJSONReaderImpl();



    /// \brief  Creates DOM tree from file
    BeBoolean ParseJSONFileImpl(const BeString16& strJSONFilePath);

    /// \brief  Creates DOM tree from string
    BeBoolean ParseJSONStringImpl(const BeString8& strJSONString);

    /// \brief  Creates all the DOM tree
    BeJSONReader::BeJSONNode* CreateDOMTree(void);

  private:

#if JSON_USING_RAPIDJSON
      /// \brief  Creates a DOM tree node
      BeJSONReader::BeJSONNode* CreateDOMTreeImpl(rapidjson::Document::GenericValue& kNode);

      /// \brief  Creates a JSON value
      BeJSONReader::BeJSONValue* CreateJSONValueImpl(rapidjson::Document::GenericValue& kNode);

      /// \brief  JSON doc
      rapidjson::Document m_kJSONDoc;
#endif

};
/////////////////////////////////////////////////////////////////////////////
BeJSONReader::BeJSONReaderImpl::BeJSONReaderImpl()
{
}
/////////////////////////////////////////////////////////////////////////////
BeJSONReader::BeJSONReaderImpl::~BeJSONReaderImpl()
{
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeJSONReader::BeJSONReaderImpl::ParseJSONFileImpl(const BeString16& strJSONFilePath)
{
    BeVoidP pJSONFileData = BeFileTool::ReadEntireFile(strJSONFilePath, NULL);

    if (pJSONFileData)
    {
        BeString8 strJSON = reinterpret_cast<BeChar8*>(pJSONFileData);

#if JSON_USING_RAPIDJSON
        m_kJSONDoc.Parse(strJSON.ToRaw());
#endif

        BeFreeMemory(pJSONFileData);

#if JSON_USING_RAPIDJSON
        if (m_kJSONDoc.HasParseError())
        {
            BE_UTILITY_PRINT("JSON file had parse errors: %d", (BeInt) m_kJSONDoc.GetParseError());

            return BE_FALSE;
        }
        else
        {
            return BE_TRUE;
        }
#endif
    }

    return BE_FALSE;
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeJSONReader::BeJSONReaderImpl::ParseJSONStringImpl(const BeString8& strJSONString)
{
#if JSON_USING_RAPIDJSON
    m_kJSONDoc.Parse(strJSONString.ToRaw());

    if (m_kJSONDoc.HasParseError())
    {
        BE_UTILITY_PRINT("JSON file had parse errors: %d", (BeInt)m_kJSONDoc.GetParseError());

        return BE_FALSE;
    }
    else
    {
        return BE_TRUE;
    }
#else
    return BE_FALSE;
#endif
}
/////////////////////////////////////////////////////////////////////////////
#if JSON_USING_RAPIDJSON
BeJSONReader::BeJSONNode* BeJSONReader::BeJSONReaderImpl::CreateDOMTreeImpl(rapidjson::Document::GenericValue& kNode)
{
    BeJSONReader::BeJSONNode* pNewJSONNode = BeNew BeJSONReader::BeJSONNode ();

    if (kNode.MemberCount() > 0)
    {
        rapidjson::Document::MemberIterator memberIt = kNode.MemberBegin();
        while (memberIt != kNode.MemberEnd())
        {
            rapidjson::Document::GenericValue& kMemberName = memberIt->name;
            rapidjson::Document::GenericValue& kMemberValue = memberIt->value;

            BeString8 strMemberName = kMemberName.GetString();

            BeJSONValue* pNodeValue = CreateJSONValueImpl(kMemberValue);

            pNewJSONNode->m_kMembers[strMemberName] = pNodeValue;

            memberIt++;
        }
    }

    return pNewJSONNode;
}
#endif
/////////////////////////////////////////////////////////////////////////////
#if JSON_USING_RAPIDJSON
BeJSONReader::BeJSONValue* BeJSONReader::BeJSONReaderImpl::CreateJSONValueImpl(rapidjson::Document::GenericValue& kNode)
{
    BeJSONReader::BeJSONValue* pNodeValue = BeNew BeJSONReader::BeJSONValue();
    if (!kNode.IsNull())
    {
        if (kNode.IsObject())
        {
            BeJSONReader::BeJSONNode* pNewJSONNodeValue = CreateDOMTreeImpl(kNode);

            pNodeValue->SetValueAsNode(pNewJSONNodeValue);

            BeDelete pNewJSONNodeValue;
        }
        else if (kNode.IsNumber())
        {
            if (kNode.IsInt())
            {
                pNodeValue->SetValueAsInt(kNode.GetInt());
            }
            else if (kNode.IsUint())
            {
                pNodeValue->SetValueAsInt(kNode.GetUint());
            }
            else if (kNode.IsInt64())
            {
                pNodeValue->SetValueAsInt(kNode.GetInt64());
            }
            else if (kNode.IsUint64())
            {
                pNodeValue->SetValueAsInt(kNode.GetUint64());
            }
            else if (kNode.IsDouble())
            {
                pNodeValue->SetValueAsFloat(kNode.GetDouble());
            }
        }
        else if (kNode.IsBool())
        {
            pNodeValue->SetValueAsBoolean(kNode.GetBool());
        }
        else if (kNode.IsString())
        {
            pNodeValue->SetValueAsString(kNode.GetString());
        }
        else if (kNode.IsArray())
        {
            std::vector<BeJSONReader::BeJSONValue*> pNewJSONArrayValue;

            for (rapidjson::SizeType i = 0; i < kNode.Size(); i++)
            {
                rapidjson::Document::GenericValue& kArrayValue = kNode[i];

                BeJSONValue* pNodeValueTmp = CreateJSONValueImpl(kArrayValue);

                pNewJSONArrayValue.push_back(pNodeValueTmp);
            }

            pNodeValue->SetValueAsArray(pNewJSONArrayValue);

            for (BeUInt i = 0; i < pNewJSONArrayValue.size(); i++)
            {
                BeJSONValue* pNodeValueTmp = pNewJSONArrayValue[i];

                BeDelete pNodeValueTmp;
            }
            pNewJSONArrayValue.clear();
        }
    }

    return pNodeValue;
}
#endif
/////////////////////////////////////////////////////////////////////////////
BeJSONReader::BeJSONNode* BeJSONReader::BeJSONReaderImpl::CreateDOMTree(void)
{
    BeJSONReader::BeJSONNode* pDOMTree = NULL;

#if JSON_USING_RAPIDJSON
    pDOMTree = CreateDOMTreeImpl(m_kJSONDoc.Move());
#endif

    return pDOMTree;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeJSONReader::BeJSONValue::BeJSONValue()
:
    m_eValueType(JSON_VALUE_UNKNOWN),
    m_pValue(NULL)
{

}
/////////////////////////////////////////////////////////////////////////////
BeJSONReader::BeJSONValue::~BeJSONValue()
{
    if (m_pValue != NULL)
    {
        switch (m_eValueType)
        {
            case JSON_VALUE_INT:
            {
                BeInt* pValue = reinterpret_cast<BeInt*>(m_pValue);
                BeDelete pValue;

                break;
            }
            case JSON_VALUE_FLOAT:
            {
                BeFloat* pValue = reinterpret_cast<BeFloat*>(m_pValue);
                BeDelete pValue;

                break;
            }
            case JSON_VALUE_BOOLEAN:
            {
                BeBoolean* pValue = reinterpret_cast<BeBoolean*>(m_pValue);
                BeDelete pValue;

                break;
            }
            case JSON_VALUE_STRING:
            {
                BeString8* pValue = reinterpret_cast<BeString8*>(m_pValue);
                BeDelete pValue;

                break;
            }
            case JSON_VALUE_ARRAY:
            {
                std::vector<BeJSONValue*>* pValue = reinterpret_cast<std::vector<BeJSONValue*>*>(m_pValue);
                for (BeUInt i = 0; i < pValue->size(); ++i)
                {
                    BeJSONValue* pValueTemp = pValue->at(i);
                    BeDelete pValueTemp;
                }
                pValue->clear();

                BeDelete pValue;

                break;
            }
            case JSON_VALUE_NODE:
            {
                BeJSONReader::BeJSONNode* pValue = reinterpret_cast<BeJSONReader::BeJSONNode*>(m_pValue);
                BeDelete pValue;

                break;
            }
        }

        m_pValue = NULL;
    }
}
/////////////////////////////////////////////////////////////////////////////
BeJSONReader::BeJSONValue* BeJSONReader::BeJSONValue::operator[](const BeUInt uIdx)
{
    if (m_eValueType == JSON_VALUE_ARRAY)
    {
        std::vector<BeJSONValue*>* pValue = reinterpret_cast<std::vector<BeJSONValue*>*>(m_pValue);
        if (uIdx < pValue->size())
        {
            return pValue->at(uIdx);
        }
    }

    return NULL;
}
/////////////////////////////////////////////////////////////////////////////
BeInt BeJSONReader::BeJSONValue::GetValueArraySize(void)
{
    if (m_eValueType == JSON_VALUE_ARRAY)
    {
        std::vector<BeJSONValue*>* pValue = reinterpret_cast<std::vector<BeJSONValue*>*>(m_pValue);
        return pValue->size();
    }

    return -1;
}
/////////////////////////////////////////////////////////////////////////////
void BeJSONReader::BeJSONValue::GetValueAsInt(BeInt& pDest)
{
    if (m_eValueType == JSON_VALUE_INT)
    {
        BeInt* pValue = reinterpret_cast<BeInt*>(m_pValue);
        pDest = *pValue;
    }
    else
    {
        pDest = 0;
    }
}
/////////////////////////////////////////////////////////////////////////////
void BeJSONReader::BeJSONValue::GetValueAsFloat(BeFloat& pDest)
{
    if (m_eValueType == JSON_VALUE_FLOAT)
    {
        BeFloat* pValue = reinterpret_cast<BeFloat*>(m_pValue);
        pDest = *pValue;
    }
    else
    {
        pDest = 0.0f;
    }
}
/////////////////////////////////////////////////////////////////////////////
void BeJSONReader::BeJSONValue::GetValueAsBoolean(BeBoolean& pDest)
{
    if (m_eValueType == JSON_VALUE_BOOLEAN)
    {
        BeBoolean* pValue = reinterpret_cast<BeBoolean*>(m_pValue);
        pDest = *pValue;
    }
    else
    {
        pDest = false;
    }
}
/////////////////////////////////////////////////////////////////////////////
void BeJSONReader::BeJSONValue::GetValueAsString(BeString8& pDest)
{
    if (m_eValueType == JSON_VALUE_STRING)
    {
        BeString8* pValue = reinterpret_cast<BeString8*>(m_pValue);
        pDest = *pValue;
    }
    else
    {
        pDest = BeString8::EMPTY();
    }
}
/////////////////////////////////////////////////////////////////////////////
void BeJSONReader::BeJSONValue::GetValueAsArray(std::vector<BeJSONReader::BeJSONValue*>** pDest)
{
    if (m_eValueType == JSON_VALUE_ARRAY)
    {
        std::vector<BeJSONValue*>* pValue = reinterpret_cast<std::vector<BeJSONValue*>*>(m_pValue);
        *pDest = pValue;
    }
    else
    {
        *pDest = NULL;
    }
}
/////////////////////////////////////////////////////////////////////////////
void BeJSONReader::BeJSONValue::GetValueAsNode(BeJSONReader::BeJSONNode** pDest)
{
    if (m_eValueType == JSON_VALUE_NODE)
    {
        BeJSONReader::BeJSONNode* pValue = reinterpret_cast<BeJSONReader::BeJSONNode*>(m_pValue);
        *pDest = pValue;
    }
    else
    {
        *pDest = NULL;
    }
}
/////////////////////////////////////////////////////////////////////////////
BeInt BeJSONReader::BeJSONValue::GetValueAsInt(void)
{
    if (m_eValueType == JSON_VALUE_INT)
    {
        BeInt* pValue = reinterpret_cast<BeInt*>(m_pValue);
        return *pValue;
    }
    else
    {
        return 0;
    }
}
/////////////////////////////////////////////////////////////////////////////
BeFloat BeJSONReader::BeJSONValue::GetValueAsFloat(void)
{
    if (m_eValueType == JSON_VALUE_FLOAT)
    {
        BeFloat* pValue = reinterpret_cast<BeFloat*>(m_pValue);
        return *pValue;
    }
    else
    {
        return 0.0f;
    }
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeJSONReader::BeJSONValue::GetValueAsBoolean(void)
{
    if (m_eValueType == JSON_VALUE_BOOLEAN)
    {
        BeBoolean* pValue = reinterpret_cast<BeBoolean*>(m_pValue);
        return *pValue;
    }
    else
    {
        return false;
    }
}
/////////////////////////////////////////////////////////////////////////////
BeString8 BeJSONReader::BeJSONValue::GetValueAsString(void)
{
    if (m_eValueType == JSON_VALUE_STRING)
    {
        BeString8* pValue = reinterpret_cast<BeString8*>(m_pValue);
        return *pValue;
    }
    else
    {
        return BeString8::EMPTY();
    }
}
/////////////////////////////////////////////////////////////////////////////
std::vector<BeJSONReader::BeJSONValue*>* BeJSONReader::BeJSONValue::GetValueAsArray(void)
{
    if (m_eValueType == JSON_VALUE_ARRAY)
    {
        std::vector<BeJSONValue*>* pValue = reinterpret_cast<std::vector<BeJSONValue*>*>(m_pValue);
        return pValue;
    }
    else
    {
        return NULL;
    }
}
/////////////////////////////////////////////////////////////////////////////
BeJSONReader::BeJSONNode* BeJSONReader::BeJSONValue::GetValueAsNode(void)
{
    if (m_eValueType == JSON_VALUE_NODE)
    {
        BeJSONReader::BeJSONNode* pValue = reinterpret_cast<BeJSONReader::BeJSONNode*>(m_pValue);
        return pValue;
    }
    else
    {
        return NULL;
    }
}
/////////////////////////////////////////////////////////////////////////////
void BeJSONReader::BeJSONValue::SetValueAsInt(BeInt pValue)
{
    m_eValueType = JSON_VALUE_INT;

    BeInt* pNewValue = BeNew BeInt;
    *pNewValue = pValue;

    m_pValue = pNewValue;
}
/////////////////////////////////////////////////////////////////////////////
void BeJSONReader::BeJSONValue::SetValueAsFloat(BeFloat pValue)
{
    m_eValueType = JSON_VALUE_FLOAT;

    BeFloat* pNewValue = BeNew BeFloat;
    *pNewValue = pValue;

    m_pValue = pNewValue;
}
/////////////////////////////////////////////////////////////////////////////
void BeJSONReader::BeJSONValue::SetValueAsBoolean(BeBoolean pValue)
{
    m_eValueType = JSON_VALUE_BOOLEAN;

    BeBoolean* pNewValue = BeNew BeBoolean;
    *pNewValue = pValue;

    m_pValue = pNewValue;
}
/////////////////////////////////////////////////////////////////////////////
void BeJSONReader::BeJSONValue::SetValueAsString(const BeString8& pValue)
{
    m_eValueType = JSON_VALUE_STRING;

    BeString8* pNewValue = BeNew BeString8();
    *pNewValue = pValue;

    m_pValue = pNewValue;
}
/////////////////////////////////////////////////////////////////////////////
void BeJSONReader::BeJSONValue::SetValueAsArray(const std::vector<BeJSONReader::BeJSONValue*>& pValue)
{
    m_eValueType = JSON_VALUE_ARRAY;

    std::vector<BeJSONReader::BeJSONValue*>* pNewValue = BeNew std::vector<BeJSONReader::BeJSONValue*>();
    for (BeUInt i = 0; i < pValue.size(); ++i)
    {
        BeJSONReader::BeJSONValue* pNewValueToAdd = BeNew BeJSONReader::BeJSONValue();
        pNewValueToAdd->SetValueFromValue(pValue.at(i));

        pNewValue->push_back(pNewValueToAdd);
    }

    m_pValue = pNewValue;
}
/////////////////////////////////////////////////////////////////////////////
void BeJSONReader::BeJSONValue::SetValueAsNode(BeJSONReader::BeJSONNode* pValue)
{
    m_eValueType = JSON_VALUE_NODE;

    BeJSONReader::BeJSONNode* pNewValue = BeNew BeJSONReader::BeJSONNode();
    
    std::map<BeString8, BeJSONReader::BeJSONValue*>::iterator it = pValue->m_kMembers.begin();
    while (it != pValue->m_kMembers.end())
    {
        BeJSONReader::BeJSONValue* pNewValueToAdd = BeNew BeJSONReader::BeJSONValue(); 
        pNewValueToAdd->SetValueFromValue(it->second);

        pNewValue->m_kMembers[it->first] = pNewValueToAdd;

        it++;
    }

    m_pValue = pNewValue;
}
/////////////////////////////////////////////////////////////////////////////
void BeJSONReader::BeJSONValue::SetValueFromValue(BeJSONReader::BeJSONValue* pValue)
{
    switch (pValue->m_eValueType)
    {
        case JSON_VALUE_INT:
        {
            SetValueAsInt(pValue->GetValueAsInt());
            break;
        }
        case JSON_VALUE_FLOAT:
        {
            SetValueAsFloat(pValue->GetValueAsFloat());
            break;
        }
        case JSON_VALUE_BOOLEAN:
        {
            SetValueAsBoolean(pValue->GetValueAsBoolean());
            break;
        }
        case JSON_VALUE_STRING:
        {
            SetValueAsString(pValue->GetValueAsString());
            break;
        }
        case JSON_VALUE_ARRAY:
        {
            SetValueAsArray(*pValue->GetValueAsArray());
            break;
        }
        case JSON_VALUE_NODE:
        {
            SetValueAsNode(pValue->GetValueAsNode());
            break;
        }
    }
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeJSONReader::BeJSONNode::BeJSONNode()
{
}
/////////////////////////////////////////////////////////////////////////////
BeJSONReader::BeJSONNode::~BeJSONNode()
{
    std::map<BeString8, BeJSONValue*>::iterator it = m_kMembers.begin();
    while (it != m_kMembers.end())
    {
        BeJSONValue* pValue = reinterpret_cast<BeJSONValue*>(it->second);

        BeDelete pValue;

        it++;
    }
    m_kMembers.clear();
}
/////////////////////////////////////////////////////////////////////////////
BeInt BeJSONReader::BeJSONNode::GetMemberCount(void)
{
    return m_kMembers.size();
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeJSONReader::BeJSONNode::HasNode(const BeString8& strKey)
{
    std::map<BeString8, BeJSONValue*>::iterator it = m_kMembers.find(strKey);
    if (it != m_kMembers.end())
    {
        if (it->second->m_eValueType != JSON_VALUE_UNKNOWN)
        {
            return BE_TRUE;
        }
        else
        {
            return BE_FALSE;
        }
    }

    return BE_FALSE;
}
/////////////////////////////////////////////////////////////////////////////
BeJSONReader::BeJSONValue* BeJSONReader::BeJSONNode::operator[](const BeString8& strKey)
{
    std::map<BeString8, BeJSONValue*>::iterator it = m_kMembers.find(strKey);
    if (it != m_kMembers.end())
    {
        return it->second;
    }
    else
    {
        BeJSONValue* pNewJSONValue = BeNew BeJSONValue();

        m_kMembers[strKey] = pNewJSONValue;

        return pNewJSONValue;
    }
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeJSONReader::BeJSONReader()
:
    m_pJSONRoot(NULL)
{
    m_pJSONReaderImpl = BeNew BeJSONReaderImpl();

    m_pJSONRoot = BeNew BeJSONNode ();
}
/////////////////////////////////////////////////////////////////////////////
BeJSONReader::BeJSONReader(const BeString16& strJSONFilePath)
:
    m_pJSONRoot(NULL)
{
    m_pJSONReaderImpl = BeNew BeJSONReaderImpl();

    ParseJSONFile(strJSONFilePath);
}
/////////////////////////////////////////////////////////////////////////////
BeJSONReader::BeJSONReader(const BeString8& strJSONString)
:
    m_pJSONRoot(NULL)
{
    m_pJSONReaderImpl = BeNew BeJSONReaderImpl();

    ParseJSONString(strJSONString);
}
/////////////////////////////////////////////////////////////////////////////
BeJSONReader::~BeJSONReader()
{
    if (m_pJSONReaderImpl != NULL)
    {
        BeDelete m_pJSONReaderImpl;
        m_pJSONReaderImpl = NULL;
    }

    if (m_pJSONRoot != NULL)
    {
        BeDelete m_pJSONRoot;
        m_pJSONRoot = NULL;
    }
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeJSONReader::ParseJSONFile(const BeString16& strJSONFilePath)
{
    if (m_pJSONRoot)
    {
        BeDelete m_pJSONRoot;
        m_pJSONRoot = NULL;
    }

    if (m_pJSONReaderImpl->ParseJSONFileImpl(strJSONFilePath))
    {
        m_pJSONRoot = m_pJSONReaderImpl->CreateDOMTree();

        return BE_TRUE;
    }

    return BE_FALSE;
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeJSONReader::ParseJSONString(const BeString8& strJSONContent)
{
    if (m_pJSONRoot)
    {
        BeDelete m_pJSONRoot;
        m_pJSONRoot = NULL;
    }

    if (m_pJSONReaderImpl->ParseJSONStringImpl(strJSONContent))
    {
        m_pJSONRoot = m_pJSONReaderImpl->CreateDOMTree();

        return BE_TRUE;
    }

    return BE_FALSE;
}
/////////////////////////////////////////////////////////////////////////////
void BeJSONReader::CreateJSONString(BeJSONReader::BeJSONNode* pJSONNode, BeString8& strDest, BeBoolean bAvoidNotNeededChars, BeInt iTabCount)
{
    if (!bAvoidNotNeededChars)
    {
        if (iTabCount == 0)
        {
            strDest.Append("\n");

            for (BeInt i = iTabCount; i > 0; --i)
            {
                strDest.Append("\t");
            }
        }
    }

    strDest.Append("{");
    if (!bAvoidNotNeededChars)
    {
        strDest.Append("\n");
    }

    std::map<BeString8, BeJSONValue*>::iterator it = pJSONNode->m_kMembers.begin();
    while (it != pJSONNode->m_kMembers.end())
    {
        if (!bAvoidNotNeededChars)
        {
            for (BeInt i = iTabCount + 1; i > 0; --i)
            {
                strDest.Append("\t");
            }
        }

        BeJSONValue* pValue = reinterpret_cast<BeJSONValue*>(it->second);

        strDest.Append("\"");
        strDest.Append(it->first.ToRaw());
        strDest.Append("\":");

        if (pValue->m_eValueType != JSON_VALUE_UNKNOWN)
        {
            PrintDOMTreeValue(pValue, strDest, bAvoidNotNeededChars, iTabCount);

            if (pValue->m_eValueType == JSON_VALUE_ARRAY)
            {
                if (!bAvoidNotNeededChars)
                {
                    for (BeInt i = iTabCount + 1; i > 0; --i)
                    {
                        strDest.Append("\t");
                    }
                }

                if (std::next(it, 1) == pJSONNode->m_kMembers.end())
                {
                    strDest.Append("]");
                }
                else
                {
                    strDest.Append("],");
                }

                if (!bAvoidNotNeededChars)
                {
                    strDest.Append("\n");
                }
            }
            else
            {
                if (std::next(it, 1) != pJSONNode->m_kMembers.end())
                {
                    strDest.Append(",");
                }

                if (!bAvoidNotNeededChars)
                {
                    strDest.Append("\n");
                }
            }
        }

        it++;
    }

    if (!bAvoidNotNeededChars)
    {
        for (BeInt i = iTabCount; i > 0; --i)
        {
            strDest.Append("\t");
        }
    }

    strDest.Append("}");

    if (!bAvoidNotNeededChars && iTabCount == 0)
    {
        strDest.Append("\n");
    }
}
/////////////////////////////////////////////////////////////////////////////
void BeJSONReader::PrintDOMTreeValue(BeJSONReader::BeJSONValue* pJSONValue, BeString8& strDest, BeBoolean bAvoidNotNeededChars, BeInt iTabCount)
{
    switch (pJSONValue->m_eValueType)
    {
        case JSON_VALUE_NODE:
        {
            BeJSONReader::BeJSONNode* pNodeToPrint = NULL;
            pJSONValue->GetValueAsNode(&pNodeToPrint);

            if (pNodeToPrint)
            {
                CreateJSONString(pNodeToPrint, strDest, bAvoidNotNeededChars, iTabCount + 1);
            }

            break;
        }
        case JSON_VALUE_ARRAY:
        {
            std::vector<BeJSONReader::BeJSONValue*>* pArrayValue = NULL;
            pJSONValue->GetValueAsArray(&pArrayValue);

            strDest.Append("[");

            if (!bAvoidNotNeededChars)
            {
                strDest.Append("\n");
            }

            for (BeUInt v = 0; v < pArrayValue->size(); ++v)
            {
                if (!bAvoidNotNeededChars)
                {
                    for (BeInt i = iTabCount + 2; i > 0; --i)
                    {
                        strDest.Append("\t");
                    }
                }

                BeJSONReader::BeJSONValue* pCurrentValue = pArrayValue->at(v);
                if (pCurrentValue)
                    PrintDOMTreeValue(pCurrentValue, strDest, bAvoidNotNeededChars, iTabCount + 1);

                if (v != pArrayValue->size() - 1)
                {
                    strDest.Append(",");
                }

                if (!bAvoidNotNeededChars)
                {
                    strDest.Append("\n");
                }
            }

            break;
        }
        case JSON_VALUE_INT:
        {
            BeInt pValue;
            pJSONValue->GetValueAsInt(pValue);

            strDest.Append(ParseIntToString8(pValue));

            break;
        }
        case JSON_VALUE_FLOAT:
        {
            BeFloat pValue;
            pJSONValue->GetValueAsFloat(pValue);

            strDest.Append(ParseFloatToString8(pValue));

            break;
        }
        case JSON_VALUE_BOOLEAN:
        {
            BeBoolean pValue;
            pJSONValue->GetValueAsBoolean(pValue);

            if (pValue)
            {
                strDest.Append("true");
            }
            else
            {
                strDest.Append("false");
            }

            break;
        }
        case JSON_VALUE_STRING:
        {
            BeString8 pValue;
            pJSONValue->GetValueAsString(pValue);

            strDest.Append("\"");
            strDest.Append(pValue.ToRaw());
            strDest.Append("\"");

            break;
        }
    }
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeJSONReader::HasNode(const BeString8& strKey)
{
    return m_pJSONRoot->HasNode(strKey);
}
/////////////////////////////////////////////////////////////////////////////
BeJSONReader::BeJSONNode* BeJSONReader::GetRootNode(void)
{
    return m_pJSONRoot;
}
/////////////////////////////////////////////////////////////////////////////
BeJSONReader::BeJSONValue* BeJSONReader::operator[](const BeString8& strKey)
{
    return (*m_pJSONRoot)[strKey];
}
/////////////////////////////////////////////////////////////////////////////
