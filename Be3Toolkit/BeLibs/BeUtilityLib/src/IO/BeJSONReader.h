/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeJSONReader.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEJSONREADER_H
#define BE_BEJSONREADER_H

#include <BeUtilityLib/Pch/BeUtilityLibPredef.h>

/////////////////////////////////////////////////
/// \class BeJSONReader
/// \brief High speed JSON parser and writer
/////////////////////////////////////////////////
class UTILITY_API BeJSONReader : public BeMemoryObject
{

  public:

    /// \brief JSON Value object types
    enum eJSONValueType
    {
        JSON_VALUE_UNKNOWN = 0,

        JSON_VALUE_INT,
        JSON_VALUE_FLOAT,
        JSON_VALUE_BOOLEAN,
        JSON_VALUE_STRING,
        JSON_VALUE_ARRAY,

        JSON_VALUE_NODE
    };

    // Inner Forward declaration
    class BeJSONNode;

    /// \brief A JSON Value object
    class BeJSONValue
    {
        public:
            BeJSONValue();
            ~BeJSONValue();

            /// \brief Returns the value in index if the value type is an array, NULL otherwise
            BeJSONReader::BeJSONValue* operator[](const BeUInt uIdx);

            /// \brief Returns the array size if value is an array, otherwise return -1
            BeInt GetValueArraySize(void);

            /// \brief Returns the value as int
            void GetValueAsInt(BeInt& pDest);
            /// \brief Returns the value as float
            void GetValueAsFloat(BeFloat& pDest);
            /// \brief Returns the value as boolean
            void GetValueAsBoolean(BeBoolean& pDest);
            /// \brief Returns the value as BeString8
            void GetValueAsString(BeString8& pDest);
            /// \brief Returns the value as an array
            void GetValueAsArray(std::vector<BeJSONReader::BeJSONValue*>** pDest);
            /// \brief Returns the value as a JSON Node
            void GetValueAsNode(BeJSONReader::BeJSONNode** pDest);

            /// \brief Returns the value as int
            BeInt GetValueAsInt(void);
            /// \brief Returns the value as float
            BeFloat GetValueAsFloat(void);
            /// \brief Returns the value as boolean
            BeBoolean GetValueAsBoolean(void);
            /// \brief Returns the value as BeString8
            BeString8 GetValueAsString(void);
            /// \brief Returns the value as an array
            std::vector<BeJSONReader::BeJSONValue*>* GetValueAsArray(void);
            /// \brief Returns the value as a JSON Node
            BeJSONReader::BeJSONNode* GetValueAsNode(void);

            /// \brief Sets the value as int
            void SetValueAsInt(BeInt pValue);
            /// \brief Sets the value as float
            void SetValueAsFloat(BeFloat pValue);
            /// \brief Sets the value as boolean
            void SetValueAsBoolean(BeBoolean pValue);
            /// \brief Sets the value as BeString8
            void SetValueAsString(const BeString8& pValue);
            /// \brief Sets the value as an array
            void SetValueAsArray(const std::vector<BeJSONReader::BeJSONValue*>& pValue);
            /// \brief Sets the value as a JSON Node
            void SetValueAsNode(BeJSONReader::BeJSONNode* pValue);
            /// \brief Sets the value from a different value
            void SetValueFromValue(BeJSONReader::BeJSONValue* pValue);

            /// \brief The JSON Value type
            eJSONValueType      m_eValueType;

        private:

            BeVoidP             m_pValue;
    };

    /// \brief A JSON Node object
    class BeJSONNode
    {
        public:
            BeJSONNode();
            ~BeJSONNode();

            BeBoolean HasNode(const BeString8& strKey);

            BeJSONReader::BeJSONValue* operator[](const BeString8& strKey);

            BeInt GetMemberCount (void);

            std::map<BeString8, BeJSONReader::BeJSONValue*> m_kMembers;
    };

    /// \brief Constructor
    BeJSONReader();
    /// \brief Constructor loading a file
    BeJSONReader(const BeString16& strJSONFilePath);
    /// \brief Constructor with a string
    BeJSONReader(const BeString8& strJSONString);
    /// \brief Destructor
    ~BeJSONReader();

    // Getters
    // {

        /// \brief  Returns if a node with key exists
        BeBoolean HasNode(const BeString8& strKey);

        /// \brief  Returns root DOM node
        BeJSONReader::BeJSONNode* GetRootNode(void);

        /// \brief  Returns the node with key if exists
        BeJSONReader::BeJSONValue* operator[](const BeString8& strKey);

    // }



    // Methods
    // {

        /// \brief  Creates DOM tree from file
        BeBoolean ParseJSONFile(const BeString16& strJSONFilePath);

        /// \brief  Creates DOM tree from string
        BeBoolean ParseJSONString(const BeString8& strJSONContent);

        /// \brief  Prints to console all the DOM tree for debugging purposes
        void CreateJSONString(BeJSONReader::BeJSONNode* pJSONNode, BeString8& strDest, BeBoolean bAvoidNotNeededChars = false, BeInt iTabCount = 0);

    // }

  private:

    /// \brief  Prints to console a DOM tree value
    void PrintDOMTreeValue(BeJSONReader::BeJSONValue* pJSONValue, BeString8& strDest, BeBoolean bAvoidNotNeededChars, BeInt iTabCount);

    // Forward declarations
    class BeJSONReaderImpl;

    /// \brief  JSON reader API Implementation
    BeJSONReaderImpl* m_pJSONReaderImpl;

    // The parsed and ready to use JSON root DOM node
    BeJSONReader::BeJSONNode* m_pJSONRoot;

};



#endif // #ifndef BE_BEJSONREADER_H
