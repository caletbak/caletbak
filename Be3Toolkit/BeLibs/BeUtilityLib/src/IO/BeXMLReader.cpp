/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeXMLReader.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <IO/BeXMLReader.h>
#include <Generic/BeParsingUtils.h>

// RapidXML include files
#include <Include/RapidXML/rapidxml.hpp>
#include <Include/RapidXML/rapidxml_utils.hpp>

/////////////////////////////////////////////////////////////////////////////
class UTILITY_API BeXMLReader::BeXMLReaderImpl
{

  public:

    typedef BeChar8 CHAR_TYPE;

    /// \brief  Constructor
    BeXMLReaderImpl();
    /// \brief  Destructor
    ~BeXMLReaderImpl();



    /// \brief  Creates DOM tree from file
    void ParseXMLFileImpl(const BeString16& strXMLFilePath);

    /// \brief  Gets the node child count
    BeInt GetNodeChildCountImpl(void) const;

    /// \brief  Moves to the XML root node
    BeBoolean MoveToXMLRootImpl(void);
    /// \brief  Moves to the first child
    BeBoolean MoveToFirstChildImpl(void);
    /// \brief  Moves to the last child
    BeBoolean MoveToLastChildImpl(void);
    /// \brief  Moves to the parent node
    BeBoolean MoveToParentImpl(void);
    /// \brief  Moves to the next sibling
    BeBoolean MoveToNextSiblingImpl(void);
    /// \brief  Moves to the previous sibling
    BeBoolean MoveToPreviousSiblingImpl(void);

    /// \brief  Gets the node name
    CHAR_TYPE* GetNodeNameImpl() const;
    /// \brief  Parses the node value
    CHAR_TYPE* ParseNodeValueImpl(void);

    /// \brief  Gets the node attribute count
    BeInt GetNodeAttributeCountImpl(void) const;

    /// \brief  Moves to the next node attribute
    BeBoolean MoveToNextAttributeImpl(void);
    /// \brief  Moves to the previous node attribute
    BeBoolean MoveToPreviousAttributeImpl(void);

    /// \brief  Moves to the node the XML path points. If it does not exists, the current node remains the same.
    /// The delimiter parameter is for setting how you are dividing the path. The search begins in the current node.
    /// Add "." at the path beginning for searching from root node
    BeBoolean SearchXMLNodeImpl(const BeString8& strXMLPath, const BeString8& strDelimeters);

    /// \brief  Gets the current attribute name
    CHAR_TYPE* GetNodeAttributeNameImpl() const;
    /// \brief  Parses the node attribute value
    CHAR_TYPE* ParseNodeAttributeImpl(void);
    /// \brief  Parses the node attribute value strAttribName
    CHAR_TYPE* ParseNodeAttributeImpl(const BeString8& strAttribName);
    
    /// \brief  Prints to console all the DOM tree for debugging purposes
    void PrintDOMTreeImpl(void);
    
  private:

    /// \brief  Prints to console a DOM tree node
    void PrintDOMTreeNodeImpl(BeInt iTabCount = 0);

    /// \brief  Searches for a DOM tree node
    BeBoolean SearchDOMTreeNodeImpl(const BeStringTokenizer& strTok, const BeUInt uCurrentTokIndex);

    /// \brief  XML file
    rapidxml::file<CHAR_TYPE>* m_pFile;

    /// \brief  XML doc
    rapidxml::xml_document<CHAR_TYPE> m_kXMLDoc;

    /// \brief  XML Node
    rapidxml::xml_node<CHAR_TYPE>* m_pXMLNode;

    /// \brief  XML attrib
    rapidxml::xml_attribute<CHAR_TYPE>* m_pNodeAttrib;
    
};
/////////////////////////////////////////////////////////////////////////////
BeXMLReader::BeXMLReaderImpl::BeXMLReaderImpl()
:
  m_pFile(NULL),
  m_pXMLNode(NULL),
  m_pNodeAttrib(NULL)
{
}
/////////////////////////////////////////////////////////////////////////////
BeXMLReader::BeXMLReaderImpl::~BeXMLReaderImpl()
{
  m_pXMLNode = NULL;
  m_pNodeAttrib = NULL;

  if ( m_pFile != NULL )
  {
    BeDelete m_pFile;
    m_pFile = NULL;
  }
}
/////////////////////////////////////////////////////////////////////////////
void BeXMLReader::BeXMLReaderImpl::ParseXMLFileImpl(const BeString16& strXMLFilePath)
{
  if ( m_pFile != NULL )
  {
    BeDelete m_pFile;
    m_pFile = NULL;
  }
  
  BeString8 strFileName( ConvertString16To8(strXMLFilePath) );
  m_pFile = BeNew rapidxml::file<BeChar8>(strFileName.ToRaw());
  m_kXMLDoc.parse<rapidxml::parse_default>(m_pFile->data());
}
/////////////////////////////////////////////////////////////////////////////
BeInt BeXMLReader::BeXMLReaderImpl::GetNodeChildCountImpl(void) const
{
  BeInt iChildCount = 0;
  if ( m_pXMLNode == NULL )
  {
    for ( rapidxml::xml_node<CHAR_TYPE> *node = m_kXMLDoc.first_node(); node; node = node->next_sibling() )
    {
      if ( *node->name() != CHAR_TYPE('\0') )
      {
        iChildCount++;
      }
    }
  }
  else
  {
    for ( rapidxml::xml_node<CHAR_TYPE> *node = m_pXMLNode->first_node(); node; node = node->next_sibling() )
    {
      if ( *node->name() != CHAR_TYPE('\0') )
      {
        iChildCount++;
      }
    }
  }
  
  return iChildCount;
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeXMLReader::BeXMLReaderImpl::MoveToXMLRootImpl(void)
{
  m_pXMLNode = m_kXMLDoc.first_node();
  return (m_pXMLNode != NULL);
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeXMLReader::BeXMLReaderImpl::MoveToFirstChildImpl(void)
{
  if ( m_pXMLNode == NULL )
  {
    m_pXMLNode = m_kXMLDoc.first_node();
  }
  else
  {
    if ( m_pXMLNode->first_node() != NULL )
    {
      m_pXMLNode = m_pXMLNode->first_node();
    }
    else
    {
      return BE_FALSE;
    }
  }

  m_pNodeAttrib = NULL;
  return (m_pXMLNode != NULL);
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeXMLReader::BeXMLReaderImpl::MoveToLastChildImpl(void)
{
  if ( m_pXMLNode == NULL )
  {
    m_pXMLNode = m_kXMLDoc.last_node();
  }
  else
  {
    if ( m_pXMLNode->last_node() != NULL )
    {
      m_pXMLNode = m_pXMLNode->last_node();
    }
    else
    {
      return BE_FALSE;
    }
  }

  m_pNodeAttrib = NULL;
  return (m_pXMLNode != NULL);
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeXMLReader::BeXMLReaderImpl::MoveToParentImpl(void)
{
  if ( m_pXMLNode != NULL )
  {
    m_pXMLNode = m_pXMLNode->parent();
  }
  if ( m_pXMLNode == NULL )
  {
    m_pXMLNode = m_kXMLDoc.first_node();
  }
  
  m_pNodeAttrib = NULL;
  return (m_pXMLNode != NULL);
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeXMLReader::BeXMLReaderImpl::MoveToNextSiblingImpl(void)
{
  if ( m_pXMLNode == NULL )
  {
    return BE_FALSE;
  }

  if ( m_pXMLNode->next_sibling() != NULL )
  {
    m_pXMLNode = m_pXMLNode->next_sibling();
  }
  else
  {
    return BE_FALSE;
  }

  m_pNodeAttrib = NULL;
  return (m_pXMLNode != NULL);
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeXMLReader::BeXMLReaderImpl::MoveToPreviousSiblingImpl(void)
{
  if ( m_pXMLNode == NULL )
  {
    return BE_FALSE;
  }

  if ( m_pXMLNode->previous_sibling() != NULL )
  {
    m_pXMLNode = m_pXMLNode->previous_sibling();
  }
  else
  {
    return BE_FALSE;
  }

  m_pNodeAttrib = NULL;
  return (m_pXMLNode != NULL);
}
/////////////////////////////////////////////////////////////////////////////
BeXMLReader::BeXMLReaderImpl::CHAR_TYPE* BeXMLReader::BeXMLReaderImpl::GetNodeNameImpl() const
{
  if ( m_pXMLNode != NULL )
  {
    return m_pXMLNode->name();
  }

  return NULL;
}
/////////////////////////////////////////////////////////////////////////////
BeXMLReader::BeXMLReaderImpl::CHAR_TYPE* BeXMLReader::BeXMLReaderImpl::ParseNodeValueImpl(void)
{
  if ( m_pXMLNode != NULL )
  {
    return m_pXMLNode->value();
  }

  return NULL;
}
/////////////////////////////////////////////////////////////////////////////
BeInt BeXMLReader::BeXMLReaderImpl::GetNodeAttributeCountImpl(void) const
{
  if ( m_pXMLNode == NULL )
  {
    return -1;
  }

  BeInt uAttribCount = 0;
  for ( rapidxml::xml_attribute<CHAR_TYPE> *attr = m_pXMLNode->first_attribute(); attr; attr = attr->next_attribute() )
  {
    if ( *attr->name() != CHAR_TYPE('\0') )
    {
      uAttribCount++;
    }
  }
  
  return uAttribCount;
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeXMLReader::BeXMLReaderImpl::MoveToNextAttributeImpl(void)
{
  if ( m_pXMLNode == NULL )
  {
    return BE_FALSE;
  }

  if ( m_pNodeAttrib == NULL )
  {
    m_pNodeAttrib = m_pXMLNode->first_attribute();
  }
  else
  {
    if ( m_pNodeAttrib->next_attribute() )
    {
      m_pNodeAttrib = m_pNodeAttrib->next_attribute();
    }
    else
    {
      return BE_FALSE;
    }
  }

  return (m_pNodeAttrib != NULL);
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeXMLReader::BeXMLReaderImpl::MoveToPreviousAttributeImpl(void)
{
  if ( m_pXMLNode == NULL )
  {
    return BE_FALSE;
  }

  if ( m_pNodeAttrib == NULL )
  {
    m_pNodeAttrib = m_pXMLNode->last_attribute();
  }
  else
  {
    if ( m_pNodeAttrib->previous_attribute() )
    {
      m_pNodeAttrib = m_pNodeAttrib->previous_attribute();
    }
    else
    {
      return BE_FALSE;
    }
  }

  return (m_pNodeAttrib != NULL);
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeXMLReader::BeXMLReaderImpl::SearchXMLNodeImpl(const BeString8& strXMLPath, const BeString8& strDelimeters)
{
  rapidxml::xml_node<CHAR_TYPE>* pXMLNodeOld = m_pXMLNode;

  BeStringTokenizer strTok(strXMLPath, strDelimeters);
  if ( strTok.GetTokenCount() > 0 )
  {
    BeUInt uStartIndexToSearch = 0;
    if ( strTok.GetTokenAt(0) == "." )
    {
      m_pXMLNode = NULL;
      uStartIndexToSearch++;
    }

    if ( SearchDOMTreeNodeImpl(strTok, uStartIndexToSearch) )
    {
      return BE_TRUE;
    }
  }

  m_pXMLNode = pXMLNodeOld;
  return BE_FALSE;
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeXMLReader::BeXMLReaderImpl::SearchDOMTreeNodeImpl(const BeStringTokenizer& strTok, BeUInt uCurrentTokIndex)
{
  if ( m_pXMLNode == NULL )
  {
    m_pXMLNode = m_kXMLDoc.first_node( strTok.GetTokenAt(uCurrentTokIndex).ToRaw() );
  }
  else
  {
    m_pXMLNode = m_pXMLNode->first_node( strTok.GetTokenAt(uCurrentTokIndex).ToRaw() );
  }
  
  if ( m_pXMLNode != NULL )
  {
    if ( uCurrentTokIndex+1 == strTok.GetTokenCount() )
    {
      return BE_TRUE;
    }

    return SearchDOMTreeNodeImpl(strTok, uCurrentTokIndex+1);
  }

  return BE_FALSE;
}
/////////////////////////////////////////////////////////////////////////////
BeXMLReader::BeXMLReaderImpl::CHAR_TYPE* BeXMLReader::BeXMLReaderImpl::GetNodeAttributeNameImpl() const
{
  if ( m_pNodeAttrib != NULL )
  {
    return m_pNodeAttrib->name();
  }

  return NULL;
}
/////////////////////////////////////////////////////////////////////////////
BeXMLReader::BeXMLReaderImpl::CHAR_TYPE* BeXMLReader::BeXMLReaderImpl::ParseNodeAttributeImpl(void)
{
  if ( m_pNodeAttrib != NULL )
  {
    return m_pNodeAttrib->value();
  }

  return NULL;
}
/////////////////////////////////////////////////////////////////////////////
BeXMLReader::BeXMLReaderImpl::CHAR_TYPE* BeXMLReader::BeXMLReaderImpl::ParseNodeAttributeImpl(const BeString8& strAttribName)
{
  if ( m_pXMLNode == NULL )
  {
    return NULL;
  }

  rapidxml::xml_attribute<CHAR_TYPE> *attr = m_pXMLNode->first_attribute(strAttribName.ToRaw());
  if ( attr != NULL )
  {
    return attr->value();
  }

  return NULL;
}
/////////////////////////////////////////////////////////////////////////////
void BeXMLReader::BeXMLReaderImpl::PrintDOMTreeNodeImpl(BeInt iTabCount)
{
  for ( BeInt i = iTabCount; i > 0; --i )
  {
    BE_UTILITY_PRINT("\t");
  }
  
  if ( GetNodeAttributeCountImpl() == 0 )
  {
    BE_UTILITY_PRINT("<%s>\n", GetNodeNameImpl());
  }
  else
  {
    BE_UTILITY_PRINT("<%s ", GetNodeNameImpl());
    while ( MoveToNextAttributeImpl() )
    {
      BE_UTILITY_PRINT("%s=\"%s\" ", GetNodeAttributeNameImpl(), ParseNodeAttributeImpl());
    }
    BE_UTILITY_PRINT(">\n");
  }
  
  if ( ParseNodeValueImpl() != NULL && *ParseNodeValueImpl() != CHAR_TYPE('\0') )
  {
    for ( BeInt i = iTabCount+1; i > 0; --i )
    {
      BE_UTILITY_PRINT("\t");
    }
    BE_UTILITY_PRINT("%s\n", ParseNodeValueImpl());

  }

  if ( GetNodeChildCountImpl() > 0 )
  {
    MoveToFirstChildImpl();
    PrintDOMTreeNodeImpl(iTabCount + 1);
  }
  
  for ( BeInt i = iTabCount; i > 0; --i )
  {
    BE_UTILITY_PRINT("\t");
  }
  BE_UTILITY_PRINT("</%s>\n", GetNodeNameImpl());

  if ( MoveToNextSiblingImpl() )
  {
    PrintDOMTreeNodeImpl(iTabCount);
  }
  else
  {
    MoveToParentImpl();
  }

}
/////////////////////////////////////////////////////////////////////////////
void BeXMLReader::BeXMLReaderImpl::PrintDOMTreeImpl(void)
{
  rapidxml::xml_node<CHAR_TYPE>* pXMLNodeOld = m_pXMLNode;

  MoveToXMLRootImpl();
  PrintDOMTreeNodeImpl(0);

  m_pXMLNode = pXMLNodeOld;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeXMLReader::BeXMLReader()
{
  m_pXMLReaderImpl = BeNew BeXMLReaderImpl();
}
/////////////////////////////////////////////////////////////////////////////
BeXMLReader::BeXMLReader(const BeString16& strXMLFilePath)
{
  m_pXMLReaderImpl = BeNew BeXMLReaderImpl();
  m_pXMLReaderImpl->ParseXMLFileImpl(strXMLFilePath);
}
/////////////////////////////////////////////////////////////////////////////
BeXMLReader::~BeXMLReader()
{
  if ( m_pXMLReaderImpl != NULL )
  {
    BeDelete m_pXMLReaderImpl;
    m_pXMLReaderImpl = NULL;
  }
}
/////////////////////////////////////////////////////////////////////////////
BeInt BeXMLReader::GetNodeChildCount(void) const
{
  return m_pXMLReaderImpl->GetNodeChildCountImpl();
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeXMLReader::MoveToFirstChild(void)
{
  return m_pXMLReaderImpl->MoveToFirstChildImpl();
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeXMLReader::MoveToLastChild(void)
{
  return m_pXMLReaderImpl->MoveToLastChildImpl();
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeXMLReader::MoveToParent(void)
{
  return m_pXMLReaderImpl->MoveToParentImpl();
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeXMLReader::MoveToNextSibling(void)
{
  return m_pXMLReaderImpl->MoveToNextSiblingImpl();
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeXMLReader::MoveToPreviousSibling(void)
{
  return m_pXMLReaderImpl->MoveToPreviousSiblingImpl();
}
/////////////////////////////////////////////////////////////////////////////
BeString8 BeXMLReader::ParseNodeValueString(void)
{
  return m_pXMLReaderImpl->ParseNodeValueImpl();
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeXMLReader::ParseNodeValueBoolean(void)
{
  return ParseString8ToBoolean( m_pXMLReaderImpl->ParseNodeValueImpl() );
}
/////////////////////////////////////////////////////////////////////////////
BeInt BeXMLReader::ParseNodeValueInt(void)
{
  return ParseString8ToInt( m_pXMLReaderImpl->ParseNodeValueImpl() );
}
/////////////////////////////////////////////////////////////////////////////
BeFloat BeXMLReader::ParseNodeValueFloat(void)
{
    return ParseString8ToFloat(m_pXMLReaderImpl->ParseNodeValueImpl());
}
/////////////////////////////////////////////////////////////////////////////
BeVector2D BeXMLReader::ParseNodeValueVector2D(void)
{
  return ParseString8ToVector2D( m_pXMLReaderImpl->ParseNodeValueImpl() );
}
/////////////////////////////////////////////////////////////////////////////
BeVector3D BeXMLReader::ParseNodeValueVector3D(void)
{
  return ParseString8ToVector3D( m_pXMLReaderImpl->ParseNodeValueImpl() );
}
/////////////////////////////////////////////////////////////////////////////
BeVector4D BeXMLReader::ParseNodeValueVector4D(void)
{
  return ParseString8ToVector4D( m_pXMLReaderImpl->ParseNodeValueImpl() );
}
/////////////////////////////////////////////////////////////////////////////
BeString8 BeXMLReader::GetNodeName() const
{
  BeXMLReader::BeXMLReaderImpl::CHAR_TYPE* pNodeName = m_pXMLReaderImpl->GetNodeNameImpl();
  if ( pNodeName != NULL )
  {
    return BeString8(pNodeName);
  }

  return BeString8::EMPTY();
}
/////////////////////////////////////////////////////////////////////////////
BeInt BeXMLReader::GetNodeAttributeCount(void) const
{
  return m_pXMLReaderImpl->GetNodeAttributeCountImpl();
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeXMLReader::MoveToXMLRoot(void)
{
  return m_pXMLReaderImpl->MoveToXMLRootImpl();
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeXMLReader::MoveToNextAttribute(void)
{
  return m_pXMLReaderImpl->MoveToNextAttributeImpl();
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeXMLReader::MoveToPreviousAttribute(void)
{
  return m_pXMLReaderImpl->MoveToPreviousAttributeImpl();
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeXMLReader::SearchXMLNode(const BeString8& strXMLPath, const BeString8& strDelimeters)
{
  return m_pXMLReaderImpl->SearchXMLNodeImpl(strXMLPath, strDelimeters);
}
/////////////////////////////////////////////////////////////////////////////
BeString8 BeXMLReader::ParseNodeAttributeString(const BeString8& strAttribName)
{
  BE_UNUSED(strAttribName);
  return BeString8::EMPTY();
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeXMLReader::ParseNodeAttributeBoolean(const BeString8& strAttribName)
{
  BE_UNUSED(strAttribName);
  return BE_FALSE;
}
/////////////////////////////////////////////////////////////////////////////
BeInt BeXMLReader::ParseNodeAttributeInt(const BeString8& strAttribName)
{
  BE_UNUSED(strAttribName);
  return 0;
}
/////////////////////////////////////////////////////////////////////////////
BeFloat BeXMLReader::ParseNodeAttributeFloat(const BeString8& strAttribName)
{
    BE_UNUSED(strAttribName);
    return 0.0f;
}
/////////////////////////////////////////////////////////////////////////////
BeVector2D BeXMLReader::ParseNodeAttributeVector2(const BeString8& strAttribName)
{
  BE_UNUSED(strAttribName);
  return BeVector2D::ZERO();
}
/////////////////////////////////////////////////////////////////////////////
BeVector3D BeXMLReader::ParseNodeAttributeVector3(const BeString8& strAttribName)
{
  BE_UNUSED(strAttribName);
  return BeVector3D::ZERO();
}
/////////////////////////////////////////////////////////////////////////////
BeVector4D BeXMLReader::ParseNodeAttributeVector4(const BeString8& strAttribName)
{
  BE_UNUSED(strAttribName);
  return BeVector4D::ZERO();
}
/////////////////////////////////////////////////////////////////////////////
BeString8 BeXMLReader::GetNodeAttributeName() const
{
  BeXMLReader::BeXMLReaderImpl::CHAR_TYPE* pAttribName = m_pXMLReaderImpl->GetNodeAttributeNameImpl();
  if ( pAttribName != NULL )
  {
    return BeString8(pAttribName);
  }

  return BeString8::EMPTY();
}
/////////////////////////////////////////////////////////////////////////////
void BeXMLReader::ParseXMLFile(const BeString16& strXMLFilePath)
{
  m_pXMLReaderImpl->ParseXMLFileImpl(strXMLFilePath);
}
/////////////////////////////////////////////////////////////////////////////
void BeXMLReader::PrintDOMTree(void)
{
  m_pXMLReaderImpl->PrintDOMTreeImpl();
}
/////////////////////////////////////////////////////////////////////////////

