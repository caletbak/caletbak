/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeParsingUtils.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEPARSINGUTILS_H
#define BE_BEPARSINGUTILS_H

#include <BeUtilityLib/Pch/BeUtilityLibPredef.h>

#if TARGET_ANDROID
// Converts the content of the string 16 to array of wide chars with termination character
BeWChar* wcsdup(const BeString16& strOrig);
#endif



// Parses from BeInt to BeString8
BeString8 UTILITY_API ParseIntToString8(const BeInt iOrig);

// Parses from BeFloat to BeString8
BeString8 UTILITY_API ParseFloatToString8(const BeFloat fOrig);



// Parses from BeString8 to BeInt
BeInt UTILITY_API ParseString8ToInt(const BeString8& strOrig);

// Parses from BeString8 to BeFloat
BeFloat UTILITY_API ParseString8ToFloat(const BeString8& strOrig);

// Parses from BeString8 to BeBoolean
BeBoolean UTILITY_API ParseString8ToBoolean(const BeString8& strOrig);

// Parses from BeString8 to BeVector2D
BeVector2D UTILITY_API ParseString8ToVector2D(const BeString8& strOrig);

// Parses from BeString8 to BeVector3D
BeVector3D UTILITY_API ParseString8ToVector3D(const BeString8& strOrig);

// Parses from BeString8 to BeVector4D
BeVector4D UTILITY_API ParseString8ToVector4D(const BeString8& strOrig);

#endif // #ifndef BE_BEPARSINGUTILS_H
