/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeParsingUtils.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <Generic/BeParsingUtils.h>
#include <stdlib.h>

/////////////////////////////////////////////////////////////////////////////
#if TARGET_ANDROID
BeWChar* wcsdup(const BeString16& strOrig)
{
    wchar_t * result;

    result = (wchar_t *)malloc((strOrig.GetSize() + 1) * sizeof(wchar_t));

    wcscpy(result, strOrig.ToRaw());

    return result;
}
#endif
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeString8 ParseIntToString8(const BeInt iOrig)
{
  BeChar8 kBuffer[50];
#if TARGET_WINDOWS
  sprintf_s(kBuffer, "%d", iOrig);
#elif TARGET_ANDROID
  sprintf(kBuffer, "%d", iOrig);
#endif
  return BeString8(kBuffer);
}
/////////////////////////////////////////////////////////////////////////////
BeString8 ParseFloatToString8(const BeFloat fOrig)
{
    BeChar8 kBuffer[50];
#if TARGET_WINDOWS
    sprintf_s(kBuffer, "%.2f", fOrig);
#elif TARGET_ANDROID
    sprintf(kBuffer, "%.2f", fOrig);
#endif
    return BeString8(kBuffer);
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeInt ParseString8ToInt(const BeString8& strOrig)
{
  BeInt fValue = atoi(strOrig.ToRaw());
  return fValue;
}
/////////////////////////////////////////////////////////////////////////////
BeFloat ParseString8ToFloat(const BeString8& strOrig)
{
  BeFloat fValue = (BeFloat)atof(strOrig.ToRaw());
  return fValue;
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean ParseString8ToBoolean(const BeString8& strOrig)
{
  return ( strOrig == "TRUE" || strOrig == "true" );
}
/////////////////////////////////////////////////////////////////////////////
BeVector2D ParseString8ToVector2D(const BeString8& strOrig)
{
  BeVector2D retValue( BeVector2D::ZERO() );

  BeStringTokenizer strTok(strOrig, " ");
  BeFloat fX = ParseString8ToFloat(strTok.GetNextToken());
  BeFloat fY = ParseString8ToFloat(strTok.GetNextToken());
  retValue.SetValues( fX, fY );
  
  return retValue;
}
/////////////////////////////////////////////////////////////////////////////
BeVector3D ParseString8ToVector3D(const BeString8& strOrig)
{
  BeVector3D retValue( BeVector3D::ZERO() );

  BeStringTokenizer strTok(strOrig, " ");
  BeFloat fX = ParseString8ToFloat(strTok.GetNextToken());
  BeFloat fY = ParseString8ToFloat(strTok.GetNextToken());
  BeFloat fZ = ParseString8ToFloat(strTok.GetNextToken());
  retValue.SetValues( fX, fY, fZ );
  
  return retValue;
}
/////////////////////////////////////////////////////////////////////////////
BeVector4D ParseString8ToVector4D(const BeString8& strOrig)
{
  BeVector4D retValue( BeVector4D::ZERO() );

  BeStringTokenizer strTok(strOrig, " ");
  BeFloat fX = ParseString8ToFloat(strTok.GetNextToken());
  BeFloat fY = ParseString8ToFloat(strTok.GetNextToken());
  BeFloat fZ = ParseString8ToFloat(strTok.GetNextToken());
  BeFloat fW = ParseString8ToFloat(strTok.GetNextToken());
  retValue.SetValues( fX, fY, fZ, fW );
  
  return retValue;
}
/////////////////////////////////////////////////////////////////////////////


