/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeUtilityLibPredef.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_UTILITYLIBPREDEF_H
#define BE_UTILITYLIBPREDEF_H

// Global BE3 include
#include <BeCommon/BakEngine3Common.h>

#if TARGET_WINDOWS
#ifdef UTILITY_IMPORTS
#  define UTILITY_API __declspec(dllimport)
#  pragma message("automatic link to BeUtilityLib.lib")
#  pragma comment(lib, "BeUtilityLib.lib")
#else
#  define UTILITY_API __declspec(dllexport)
#endif
#else
#  define UTILITY_API
#endif

#ifdef _DEBUG
#  define BE_UTILITY_DEBUG(x, ...) _OutputDebugString("[  UTILITY DEBUG]: ", true, x, ##__VA_ARGS__)
#  define BE_UTILITY_WARNING(x, ...) _OutputDebugString("[! UTILITY WARNING]: ", true, x, ##__VA_ARGS__)
#  define BE_UTILITY_ERROR(x, ...) _OutputDebugString("[* UTILITY ERROR]: ", true, x, ##__VA_ARGS__)
#  define BE_UTILITY_PRINT(x, ...) _OutputDebugString("", false, x, ##__VA_ARGS__)
#else
#  define BE_UTILITY_DEBUG(x, ...) x
#  define BE_UTILITY_WARNING(x, ...) x
#  define BE_UTILITY_ERROR(x, ...) x
#  define BE_UTILITY_PRINT(x, ...) x
#endif

// External includes
#include <BeCoreLib/Pch/BeCoreLib.h>
#include <BeMathLib/Pch/BeMathLib.h>

#endif // #ifndef BE_UTILITYLIBPREDEF_H

