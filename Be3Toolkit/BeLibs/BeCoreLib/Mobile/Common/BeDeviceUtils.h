/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeDeviceUtils.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_DEVICEUTILSINTERFACE_H
#define BE_DEVICEUTILSINTERFACE_H

#ifdef TARGET_ANDROID
#include <BeJNI.h>
#endif

#include <BeCoreLib/Pch/BeCoreLibPredef.h>

#include <STL/BeString.h>

//#include <Display/DisplayObject.hpp>
//#include <json/value.h>

struct BeEMailAttachment;

#ifdef TARGET_ANDROID
#ifdef __cplusplus
extern "C"
{
#endif
    extern void Java_org_bakengine_BeDeviceUtils_onNativeInit(JNIEnv *env, jobject obj, jclass thisClass);
#ifdef __cplusplus
}
#endif
#endif

class BeDeviceUtils
{
public:

    static void Initialize();

    static void Deinitialise();

    static void ShowSpinner();

    static void HideSpinner();

    //static void SendMail(const BeString16& subject, const BeString16& message, const BeString8& recipient, BeEMailAttachment* attachment, bcn::display::DisplayObject *eventReceiver);

    static BeString8 GetDeviceFlag(const BeString8& key);

    static void SetDeviceFlag(const BeString8& key, const BeString8& value);

    static void OpenMarketForRating(void);

    static void OpenExternalURL(const BeString8& urlString);

    //static void ScheduleNotification(const BeString8& sku, const Json::Value& info, int timeLeft);

    //static void ScheduleNotification(const BeString8& sku, const BeString16& body, const BeString16& action, const Json::Value &info, int timeLeft);

    static void CancelNotification(const BeString8& sku);

    static BeString8 GetUniqueID();

    static BeString8 GetMarketProvider();

    static BeString8 GetMarketName();

    static BeString8 GetPackageName();

    static BeString8 GetExternalStorageDir();

    static BeString8 GetDeviceUserLanguage();

    static void DeleteExternalFile(const BeString8& filename);

    /////////////////////////////////////////////////////////////////////////////

    static BeBoolean HasTelephonyFeatures();

    static void ShowWifiSettings();

    static void SendFinishSignal();

    /////////////////////////////////////////////////////////////////////////////

    static const BeInt GAME_REQUEST_FINISH_FLAG;

    static BeInt s_iGameFinishRequested;

    static BeVoidP m_pViewController;
};

#endif // BE_DEVICEUTILSINTERFACE_H

