
package org.bakengine;

public class BeConstants
{
	public enum MarketProvider
	{
		MARKET_GOOGLE_PLAY,
		MARKET_AMAZON,
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// WARNING !!!!
	//
	// These flags are auto-modified when executing an android compilation

	public static final Boolean _DEBUG = true;

	public static final MarketProvider MARKET_PROVIDER = MarketProvider.MARKET_GOOGLE_PLAY;
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public static final int ACTIVITY_RESULT_REQUESTCODE_STORE = 971179;

	public static final int ACTIVITY_RESULT_REQUESTCODE_FACEBOOK = 0xFACE;

	public static final int ACTIVITY_RESULT_REQUESTCODE_LEADERBOARD = 0xEADE;

	public static final int ACTIVITY_RESULT_REQUESTCODE_ACHIEVEMENTS = 0xACEE;
	
	public static final int ACTIVITY_RESULT_REQUESTCODE_NOTIFICATION = 0x1000;
	
	public static final int ACTIVITY_RESULT_REQUESTCODE_CREATE_NOTIFICATION = 0x0500;
	
	public static final int ACTIVITY_RESULT_REQUESTCODE_DELETE_NOTIFICATION = 0x0550;
	
	public static final int ACTIVITY_RESULT_REQUESTCODE_PRESSED_NOTIFICATION = 0x0600;
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static String getMarketName ()
	{
		if (MARKET_PROVIDER == MarketProvider.MARKET_GOOGLE_PLAY)
		{
			return "TID_GEN_SYSTEM_CLOUDNAME_GOOGLE";
		}
		else if (MARKET_PROVIDER == MarketProvider.MARKET_AMAZON)
		{
			return "TID_GEN_SYSTEM_CLOUDNAME_AMAZON";
		}
	
		return "MARKET_UNKNOWN";
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public static String getMarketProvider ()
	{
		if (MARKET_PROVIDER == MarketProvider.MARKET_GOOGLE_PLAY)
		{
			return "MARKET_GOOGLE_PLAY";
		}
		else if (MARKET_PROVIDER == MarketProvider.MARKET_AMAZON)
		{
			return "MARKET_AMAZON";
		}

		return "MARKET_UNKNOWN";
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
