
package org.bakengine;

import android.util.Log;

public class BeDebug
{
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public static void d (String tagClass, String message)
	{
		if (BeConstants._DEBUG)
		{
			Log.d(tagClass, message);
		}
	}
	public static void e (String tagClass, String message, Throwable e)
	{
		if (BeConstants._DEBUG)
		{
			Log.e(tagClass, message, e);
		}
	}
	public static void i (String tagClass, String message)
	{
		if (BeConstants._DEBUG)
		{
			Log.i(tagClass, message);
		}
	}
	public static void v (String tagClass, String message)
	{
		if (BeConstants._DEBUG)
		{
			Log.v(tagClass, message);
		}
	}
	public static void w (String tagClass, String message)
	{
		if (BeConstants._DEBUG)
		{
			Log.w(tagClass, message);
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
