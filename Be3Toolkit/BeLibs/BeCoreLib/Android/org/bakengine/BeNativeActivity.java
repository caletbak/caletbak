package org.bakengine;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.NativeActivity;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

public class BeNativeActivity extends NativeActivity
{
	// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public static final int ACTIVITY_EVENT_START = 0x01;
	public static final int ACTIVITY_EVENT_STOP = 0x02;
	public static final int ACTIVITY_EVENT_PAUSE = 0x04;
	public static final int ACTIVITY_EVENT_RESUME = 0x08;
	public static final int ACTIVITY_EVENT_RESULT = 0x10;
	public static final int ACTIVITY_EVENT_FOCUS_GAINED = 0x16;
	public static final int ACTIVITY_EVENT_FOCUS_LOST = 0x32;

	public interface ActivityEventsListener
	{
		public boolean onHandleActivityEvent(final int eventType,
				final Activity activity, final Intent data,
				final int requestCode, final int resultCode);
	}

	private static HashMap<ActivityEventsListener, Integer> s_ActivityEventsListener = new HashMap<ActivityEventsListener, Integer>();

	public static void RegisterEventsListener(ActivityEventsListener activityEventsListener, int eventsToRegister)
	{
		if (!s_ActivityEventsListener.containsKey(activityEventsListener))
		{
			s_ActivityEventsListener.put(activityEventsListener, new Integer(eventsToRegister));
		}
	}

	public static void UnregisterEventsListener(ActivityEventsListener activityEventsListener)
	{
		if (s_ActivityEventsListener.containsKey(activityEventsListener))
		{
			s_ActivityEventsListener.remove(activityEventsListener);
		}
	}

	// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public static Activity s_gameActivity = null;

	public static boolean s_gameAlreadyStarted = false;

	public static boolean s_gamePaused = false;
	
	// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	private static final String TAG = "BeNativeActivity";

	// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	private static native void onNativeCreate(Class<?> thisClass, Bundle savedInstanceState);
	
	// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public BeNativeActivity()
	{
		super();

		BeDebug.i(TAG, "Creating BeNativeActivity");
	}

	// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public static void SendFinishSignal()
	{
		s_gameActivity.finish();
		
		System.exit(0);
	}

	// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@TargetApi(11)
    public static void HideSoftKeys (Activity activity)
    {
    	if (Build.VERSION.SDK_INT >= 19)
		{
			int mUIFlag = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
					| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
					| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
					| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
					| View.SYSTEM_UI_FLAG_FULLSCREEN
					| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
			
			int curVis = activity.getWindow().getDecorView().getSystemUiVisibility();
			if (curVis != mUIFlag)
			{
				activity.getWindow().getDecorView().setSystemUiVisibility(mUIFlag);
			}
		}
    }

	// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		//
		// Called when the activity is starting.
		//

		BeDebug.v(TAG, "onCreate");

		s_gameActivity = this;
		
		HideSoftKeys(this);
		
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		super.onCreate(savedInstanceState);
	}

	// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	@Override
	protected void onDestroy()
	{
		super.onDestroy();
	}

	// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	@Override
	protected void onStart()
	{
		super.onStart();

		if (s_ActivityEventsListener.size() > 0)
		{
			Iterator<Map.Entry<ActivityEventsListener, Integer>> iterator = s_ActivityEventsListener.entrySet().iterator();
			while (iterator.hasNext())
			{
				Map.Entry<ActivityEventsListener, Integer> entry = iterator.next();

				if ((entry.getValue().intValue() & ACTIVITY_EVENT_START) > 0)
				{
					ActivityEventsListener activityEventsListener = (ActivityEventsListener) entry.getKey();
					activityEventsListener.onHandleActivityEvent(ACTIVITY_EVENT_START, this, null, -1, -1);
				}
			}
		}

		BeDebug.v(TAG, "onStart");
	}

	// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	@Override
	protected void onStop()
	{
		super.onStop();
		
		if (s_ActivityEventsListener.size() > 0)
		{
			Iterator<Map.Entry<ActivityEventsListener, Integer>> iterator = s_ActivityEventsListener.entrySet().iterator();
			while (iterator.hasNext())
			{
				Map.Entry<ActivityEventsListener, Integer> entry = iterator.next();

				if ((entry.getValue().intValue() & ACTIVITY_EVENT_STOP) > 0)
				{
					ActivityEventsListener activityEventsListener = (ActivityEventsListener) entry.getKey();
					activityEventsListener.onHandleActivityEvent(ACTIVITY_EVENT_STOP, this, null, -1, -1);
				}
			}
		}

		BeDebug.v(TAG, "onStop");
	}

	// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	@Override
	protected void onPause()
	{
		super.onPause();
		
		if (s_ActivityEventsListener.size() > 0)
		{
			Iterator<Map.Entry<ActivityEventsListener, Integer>> iterator = s_ActivityEventsListener.entrySet().iterator();
			while (iterator.hasNext())
			{
				Map.Entry<ActivityEventsListener, Integer> entry = iterator.next();

				if ((entry.getValue().intValue() & ACTIVITY_EVENT_PAUSE) > 0)
				{
					ActivityEventsListener activityEventsListener = (ActivityEventsListener) entry.getKey();
					activityEventsListener.onHandleActivityEvent(ACTIVITY_EVENT_PAUSE, this, null, -1, -1);
				}
			}
		}

		s_gamePaused = true;

		BeDebug.v(TAG, "onPause");
	}

	// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	@Override
	protected void onResume()
	{
		super.onResume();
		if (s_ActivityEventsListener.size() > 0)
		{
			Iterator<Map.Entry<ActivityEventsListener, Integer>> iterator = s_ActivityEventsListener.entrySet().iterator();
			while (iterator.hasNext())
			{
				Map.Entry<ActivityEventsListener, Integer> entry = iterator.next();

				if ((entry.getValue().intValue() & ACTIVITY_EVENT_RESUME) > 0)
				{
					ActivityEventsListener activityEventsListener = (ActivityEventsListener) entry.getKey();
					activityEventsListener.onHandleActivityEvent(ACTIVITY_EVENT_RESUME, this, null, -1, -1);
				}
			}
		}

		BeDebug.v(TAG, "onResume");
	}

	// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public void onWindowFocusChanged(boolean hasFocus)
	{
		super.onWindowFocusChanged(hasFocus);
		if (hasFocus) 
		{
			if (s_ActivityEventsListener.size() > 0)
			{
				Iterator<Map.Entry<ActivityEventsListener, Integer>> iterator = s_ActivityEventsListener.entrySet().iterator();
				while (iterator.hasNext())
				{
					Map.Entry<ActivityEventsListener, Integer> entry = iterator
							.next();
					if ((entry.getValue().intValue() & ACTIVITY_EVENT_FOCUS_GAINED) > 0)
					{
						ActivityEventsListener activityEventsListener = (ActivityEventsListener) entry.getKey();
						activityEventsListener.onHandleActivityEvent(
										ACTIVITY_EVENT_FOCUS_GAINED, this,
										null, -1, -1);
					}
				}
			}

			s_gamePaused = false;

			HideSoftKeys(this);
		}
		else
		{
			if (s_ActivityEventsListener.size() > 0)
			{
				Iterator<Map.Entry<ActivityEventsListener, Integer>> iterator = s_ActivityEventsListener.entrySet().iterator();
				while (iterator.hasNext())
				{
					Map.Entry<ActivityEventsListener, Integer> entry = iterator.next();
					if ((entry.getValue().intValue() & ACTIVITY_EVENT_FOCUS_LOST) > 0)
					{
						ActivityEventsListener activityEventsListener = (ActivityEventsListener) entry.getKey();
						activityEventsListener.onHandleActivityEvent(ACTIVITY_EVENT_FOCUS_LOST, this, null, -1, -1);
					}
				}
			}

			s_gamePaused = true;
		}

	}

	// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		BeDebug.v(TAG, "onActivityResult");

		boolean avoidSuperActivityResult = false;
		if (s_ActivityEventsListener.size() > 0)
		{
			Iterator<Map.Entry<ActivityEventsListener, Integer>> iterator = s_ActivityEventsListener.entrySet().iterator();	
			while (iterator.hasNext())
			{
				Map.Entry<ActivityEventsListener, Integer> entry = iterator.next();

				if ((entry.getValue().intValue() & ACTIVITY_EVENT_RESULT) > 0)
				{
					ActivityEventsListener activityEventsListener = (ActivityEventsListener) entry.getKey();
					
					avoidSuperActivityResult = activityEventsListener
							.onHandleActivityEvent(ACTIVITY_EVENT_RESULT, this,	data, requestCode, resultCode);
							
					if (avoidSuperActivityResult)
					{
						break;
					}
				}
			}
		}

		if (!avoidSuperActivityResult)
		{
			super.onActivityResult(requestCode, resultCode, data);
		}
	}

	// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/*--------------------------------------- Utility Functions -------------------------------------------*/
	public static AssetManager getAssetMgr()
	{
		return s_gameActivity.getApplicationContext().getAssets();
	}
	/*--------------------------------------- Utility Functions end -------------------------------------------*/

}

// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
