
package org.bakengine;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import org.bakengine.R;

public class BeAlertDialog extends Activity
{
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private static final String TAG = "BeAlertDialog";

    private static Activity s_hostActivity = null;

    private static final int DIALOG_INFO = 0;
    private static final int DIALOG_TEXT_ENTRY = 1;
    private static final int DIALOG_NUMBER_ENTRY = 2;
    private static final int DIALOG_SPINNER = 3;

    private static final int BUTTON_POSITIVE = 1;
    private static final int BUTTON_NEGATIVE = 2;
    private static final int BUTTON_NEUTRAL = 3;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private static class ShowProgressDialogTask extends AsyncTask<String, Void, String> 
    {
        public static boolean s_needToExit = false; 

        @Override
        protected void onPreExecute()
        {
            s_currentProgressDialog = new ProgressDialog(s_hostActivity);
            s_currentProgressDialog.setIndeterminate(true);
            s_currentProgressDialog.show();
        }

        @Override
        protected void onPostExecute(String result)
        {
            s_currentProgressDialog.dismiss();
        }

        @Override
        protected String doInBackground(String... params)
        {
            while (!s_needToExit) {}

            s_needToExit = false;

            return null;
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private static int s_currentDialogID = -1;

    private static int s_currentDialogType = -1;

    private static Dialog s_currentDialog = null;

    private static ProgressDialog s_currentProgressDialog = null;

    private static BeAlertDialog.ShowProgressDialogTask s_currentProgressDialogTask = null;

    private static String s_title = "";

    private static String s_message = "";

    private static String s_buttonPositive = "";

    private static String s_buttonNegative = "";

    private static String s_buttonNeutral = "";

    private static boolean s_buttonPositiveAvoidDismiss = false;

    private static boolean s_buttonNegativeAvoidDismiss = false;

    private static boolean s_buttonNeutralAvoidDismiss = false;

    private static boolean s_bDialogDismissible = true;

    private static String s_currentDialogText = "";

    private static LinearLayout s_linearLayoutView = null;

    private static EditText s_textEntryView = null;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private static native void onNativeInit (Class<?> thisClass);

    private static native void onPressedButton (int dialogID, int buttonID, String writtenText, boolean bAvoidDismiss);

    static
    {
        onNativeInit (BeAlertDialog.class);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    class CustomListener implements View.OnClickListener
    {
        private final int m_iButtonID;

        public CustomListener(int iButtonID)
        {
            this.m_iButtonID = iButtonID;
        }

        @Override
        public void onClick(View v)
        {
            switch(m_iButtonID)
            {
                case DialogInterface.BUTTON_POSITIVE:
                {
                    if (s_currentDialogType == DIALOG_TEXT_ENTRY || s_currentDialogType == DIALOG_NUMBER_ENTRY)
                    {
                        if(s_textEntryView != null && s_textEntryView.getText() != null)
                        {
                            s_currentDialogText = s_textEntryView.getText().toString();
                        }
                        else
                        {
                            s_currentDialogText = "";
                        }
                    }

                    onPressedButton (s_currentDialogID, BUTTON_POSITIVE, s_currentDialogText, true);

                    break;
                }
                case DialogInterface.BUTTON_NEGATIVE:
                {
                    onPressedButton (s_currentDialogID, BUTTON_NEGATIVE, s_currentDialogText, true);
                    break;
                }
                case DialogInterface.BUTTON_NEUTRAL:
                {
                    onPressedButton (s_currentDialogID, BUTTON_NEUTRAL, s_currentDialogText, true);
                    break;
                }
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        s_currentDialog = CreateDialog (s_currentDialogType, s_title, s_message, s_buttonPositive, s_buttonNegative, s_buttonNeutral);

        s_currentDialog.show ();

        if (s_buttonPositiveAvoidDismiss)
        {
            Button theButton = ((AlertDialog)s_currentDialog).getButton(DialogInterface.BUTTON_POSITIVE);
            if (theButton != null)
            {
                theButton.setOnClickListener(new CustomListener(DialogInterface.BUTTON_POSITIVE));
            }
        }
        if (s_buttonNegativeAvoidDismiss)
        {
            Button theButton = ((AlertDialog)s_currentDialog).getButton(DialogInterface.BUTTON_NEGATIVE);
            if (theButton != null)
            {
                theButton.setOnClickListener(new CustomListener(DialogInterface.BUTTON_NEGATIVE));
            }
        }
        if (s_buttonNeutralAvoidDismiss)
        {
            Button theButton = ((AlertDialog)s_currentDialog).getButton(DialogInterface.BUTTON_NEUTRAL);
            if (theButton != null)
            {
                theButton.setOnClickListener(new CustomListener(DialogInterface.BUTTON_NEUTRAL));
            }
        }

        if (!s_bDialogDismissible)
        {
            s_currentDialog.setCancelable(false);
        }

        finish();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public static void Initialise ()
    {
        s_hostActivity = BeNativeActivity.s_gameActivity;
    }

    public static void Deinitialise ()
    {
        s_hostActivity = null;

        s_linearLayoutView = null;

        s_textEntryView = null;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private static Dialog CreateDialog(int dialogType, String title, String message, String possitiveButton, String negativeButton, String neutralButton)
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(s_hostActivity);

        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(message);

        if (dialogType == DIALOG_TEXT_ENTRY ||s_currentDialogType == DIALOG_NUMBER_ENTRY)
        {
            LayoutInflater factory = LayoutInflater.from(s_hostActivity);

            s_linearLayoutView = (LinearLayout) factory.inflate (R.layout.alert_dialog_text_entry, null);

            s_textEntryView = (EditText) s_linearLayoutView.getChildAt(0);

            if(s_currentDialogType == DIALOG_NUMBER_ENTRY)
            {
                s_textEntryView.setInputType(InputType.TYPE_CLASS_NUMBER);
            }
            else
            {
                s_textEntryView.setInputType(InputType.TYPE_CLASS_TEXT);
            }

            s_textEntryView.setText(s_currentDialogText);

            if (s_currentDialogText.length() > 0)
            {
                s_textEntryView.setSelection(s_currentDialogText.length());
            }

            alertDialogBuilder.setView(s_linearLayoutView);
        }

        if (possitiveButton.length() > 0)
        {
            alertDialogBuilder.setPositiveButton(possitiveButton, new DialogInterface.OnClickListener()
            {
                public void onClick(DialogInterface dialog, int whichButton) 
                {
                    if (s_currentDialogType == DIALOG_TEXT_ENTRY || s_currentDialogType == DIALOG_NUMBER_ENTRY)
                    {
                        if(s_textEntryView != null && s_textEntryView.getText() != null)
                        {
                            s_currentDialogText = s_textEntryView.getText().toString();
                        }
                        else
                        {
                            s_currentDialogText = "";
                        }

                        s_textEntryView = null;

                        s_linearLayoutView = null;
                    }

                    onPressedButton (s_currentDialogID, BUTTON_POSITIVE, s_currentDialogText, false);
                }
            });
        }
        if (negativeButton.length() > 0)
        {
            alertDialogBuilder.setNegativeButton(negativeButton, new DialogInterface.OnClickListener()
            {
                public void onClick(DialogInterface dialog, int whichButton) 
                {
                    onPressedButton (s_currentDialogID, BUTTON_NEGATIVE, s_currentDialogText, false);
                }
            });
        }
        if (neutralButton.length() > 0)
        {
            alertDialogBuilder.setNeutralButton(neutralButton, new DialogInterface.OnClickListener()
            {
                public void onClick(DialogInterface dialog, int whichButton) 
                {
                    onPressedButton (s_currentDialogID, BUTTON_NEUTRAL, s_currentDialogText, false);
                }
            });
        }

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener()
        {
            @Override
            public void onDismiss(final DialogInterface dialog)
            {
                BeDebug.i(TAG, "ALERT DIALOG DISMISSED!");

                onPressedButton (s_currentDialogID, BUTTON_NEGATIVE, s_currentDialogText, false);
            }
        });

        return alertDialog;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public static void Show (final int dialogID, final int dialogType, final String title, final String message, final String buttonPositive, final String buttonNegative, final String buttonNeutral, final String strDefaultEntryText, final boolean buttonPositiveAvoidDismiss, final boolean buttonNegativeAvoidDismiss, final boolean buttonNeutralAvoidDismiss, final boolean bDialogDismissible)
    {
        s_currentDialogID = dialogID;

        s_currentDialogType = dialogType;

        s_title = title;

        s_message = message;

        s_buttonPositive = buttonPositive;
        s_buttonNegative = buttonNegative;
        s_buttonNeutral = buttonNeutral;

        s_buttonPositiveAvoidDismiss = buttonPositiveAvoidDismiss;
        s_buttonNegativeAvoidDismiss = buttonNegativeAvoidDismiss;
        s_buttonNeutralAvoidDismiss = buttonNeutralAvoidDismiss;

        s_bDialogDismissible = bDialogDismissible;

        s_currentDialogText = strDefaultEntryText;

        if (s_currentDialogType == DIALOG_SPINNER)
        {
            /*
            s_hostActivity.runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                    s_currentProgressDialogTask = (ShowProgressDialogTask) new BeAlertDialog.ShowProgressDialogTask ().execute("");
                }
            });
            */
        }
        else
        {
            Intent intent = new Intent(s_hostActivity.getApplicationContext(), BeAlertDialog.class);

            s_hostActivity.startActivity(intent);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public static void Hide ()
    {
        BeDebug.i (TAG, "BeAlertDialog Hide");

        if (s_currentProgressDialog != null)
        {
            ShowProgressDialogTask.s_needToExit = true;

            BeDebug.i (TAG, "BeAlertDialog s_currentProgressDialog.dismiss");

            s_currentProgressDialog.dismiss ();

            s_currentProgressDialog = null;
        }

        if (s_currentDialog != null)
        {
            s_currentDialog.hide();

            s_currentDialog = null;
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public static void SetMaxCharacters(int numCharacters)
    {
        if (s_textEntryView != null)
        {
            BeDebug.i (TAG, "BeAlertDialog SetMaxCharacters numCharacters = " + numCharacters);
            
            if(numCharacters > 0)
            {
                s_textEntryView.setFilters( new InputFilter[]{ new InputFilter.LengthFilter(numCharacters) } );
            }
            else
            {
                s_textEntryView.setFilters( new InputFilter[]{} );
            }
        }
    }

}
