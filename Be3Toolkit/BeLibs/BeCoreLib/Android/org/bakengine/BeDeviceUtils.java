package org.bakengine;

import java.io.File;
import java.util.Locale;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.text.Html;

import org.bakengine.BeNativeActivity;
import org.bakengine.BeDebug;

public class BeDeviceUtils
{
    private static final String TAG = "BeDeviceUtils";

    private static Activity m_hostActivity = null; 

    private static native void onNativeInit(Class<?> thisClass);

    static
    {
        onNativeInit(BeDeviceUtils.class);
    }

    /**
    * This function initialize the activity and register the events that we want to handle.
    */
    public void Initialize()
    {
        m_hostActivity = BeNativeActivity.s_gameActivity;
    }

    public void Deinitialize()
    {
    }

    public void SendMail(String subject, String message, String recipient)
    {
        BeDebug.i(TAG, "Sending a mail");
        try
        {
            Intent send = new Intent(Intent.ACTION_SEND);
            send.putExtra(Intent.EXTRA_SUBJECT, subject);

            if(recipient != null && recipient != "")
            {
                send.putExtra(Intent.EXTRA_EMAIL, new String[]{recipient});
            }

            send.putExtra(Intent.EXTRA_TEXT,Html.fromHtml(message));
            send.setType("message/rfc822");
            
            m_hostActivity.startActivityForResult(Intent.createChooser(send, "Send mail..."), Activity.RESULT_OK);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void SetDeviceFlag(String key, String value)
    {
        synchronized(this)
        {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(m_hostActivity);

            SharedPreferences.Editor editor = prefs.edit();

            editor.putString(key, value);

            editor.commit();
        }
    }

    public String GetDeviceFlag(String key)
    {
        synchronized(this)
        {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(m_hostActivity);

            return prefs.getString(key, "");
        }
    }
    
    public String GetUniqueID()
    {
        String androidDeviceId = "";
        try
        {
            androidDeviceId = android.provider.Settings.Secure.getString(m_hostActivity.getApplicationContext().getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
            if (!androidDeviceId.isEmpty())
            {
                androidDeviceId = "Droid:" + androidDeviceId;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return androidDeviceId;
    }

    public String GetMarketProvider()
    {
        return BeConstants.getMarketProvider();
    }

    public String GetMarketName()
    {
        return BeConstants.getMarketName();
    }

    public void SendFinishSignal()
    {
        BeNativeActivity.SendFinishSignal();
    }

    public boolean HasTelephonyFeatures()
    {
        if(m_hostActivity.getPackageManager().hasSystemFeature(PackageManager.FEATURE_TELEPHONY))
        {
            return true;
        }

        return false;
    }

    public void ShowWifiSettings()
    {
        m_hostActivity.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
    }

    public static String GetPackageVersionName ()
    {
        try
        {
            PackageInfo info = m_hostActivity.getPackageManager().getPackageInfo(m_hostActivity.getPackageName(), 0);

            return info.versionName;
        }
        catch (NameNotFoundException e)
        {
            //Handle exception
        }

        return "Empty";
    }

    public static String GetPackageName ()
    {
        return m_hostActivity.getPackageName();
    }

    public static String GetDeviceUserLanguage ()
    {
        Locale kLocale = Locale.getDefault();
        String strLocaleLanguage = kLocale.getLanguage();
      
        BeDebug.i(TAG, "GetDeviceUserLanguage : " + strLocaleLanguage);

        if (strLocaleLanguage.compareTo("zh") == 0)
        {
            return "zh";
        }
        else if (strLocaleLanguage.compareTo("ko") == 0)
        {
            return "ko";
        }
        else if (strLocaleLanguage.compareTo("ja") == 0)
        {
            return "ja";
        }
        else if (strLocaleLanguage.compareTo("it") == 0)
        {
            return "it";
        }
        else if (strLocaleLanguage.compareTo("fr") == 0)
        {
            return "fr";
        }
        else if (strLocaleLanguage.compareTo("es") == 0 || strLocaleLanguage.compareTo("ca") == 0 || strLocaleLanguage.compareTo("eu") == 0)
        {
            return "es";
        }
        else if (strLocaleLanguage.compareTo("de") == 0)
        {
            return "de";
        }

        return "en";
    }

    public static String GetExternalStorageDir ()
    {
        File external = Environment.getExternalStorageDirectory();

        return external.getPath();
    }

    public static void DeleteExternalFile (String filename)
    {
        BeDebug.i(TAG, "DeleteExternalFile : " + filename);
        File kFile = new File(filename);
        if (kFile.exists())
        {
            if (kFile.delete())
            {
                BeDebug.i(TAG, "DeleteExternalFile DELETED");	
            }
        }
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    public void openMarketForRating ()
    {
        if (GetMarketProvider().compareTo("MARKET_GOOGLE_PLAY") == 0)
        {
            Uri uri = Uri.parse("market://details?id=" + m_hostActivity.getPackageName());
            Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);

            try
            {
                m_hostActivity.startActivity(goToMarket);
            }
            catch (ActivityNotFoundException e)
            {
                m_hostActivity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + m_hostActivity.getPackageName())));
            }
        }
        else if (GetMarketProvider().compareTo("MARKET_AMAZON") == 0)
        {
            Uri uri = Uri.parse("amzn://apps/android?p=" + m_hostActivity.getPackageName());
            Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);

            try
            {
                m_hostActivity.startActivity(goToMarket);
            }
            catch (ActivityNotFoundException e)
            {
                m_hostActivity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.amazon.com/gp/mas/dl/android?p=" + m_hostActivity.getPackageName())));
            }
        }
    }

    public void openExternalURL (String urlString)
    {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(urlString));

        m_hostActivity.startActivity(browserIntent);
    }
}