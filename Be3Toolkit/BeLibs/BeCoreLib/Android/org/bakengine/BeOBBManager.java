/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bakengine;

import com.google.android.vending.expansion.downloader.DownloadProgressInfo;
import com.google.android.vending.expansion.downloader.DownloaderClientMarshaller;
import com.google.android.vending.expansion.downloader.DownloaderServiceMarshaller;
import com.google.android.vending.expansion.downloader.Helpers;
import com.google.android.vending.expansion.downloader.IDownloaderClient;
import com.google.android.vending.expansion.downloader.IDownloaderService;
import com.google.android.vending.expansion.downloader.IStub;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Messenger;

import java.util.Vector;

/**
 * This is sample code for a project built against the downloader library. It
 * implements the IDownloaderClient that the client marshaler will talk to as
 * messages are delivered from the DownloaderService.
 */
public class BeOBBManager implements IDownloaderClient, BeNativeActivity.ActivityEventsListener
{
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private static final String TAG = "BeOBBManager";

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private static Activity s_hostActivity = null;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // 
    // We use a class initializer to allow the native code to cache some field offsets. 
    // This native function looks up and caches interesting class/field/method IDs. Throws on failure.
    // See: http://developer.android.com/guide/practices/jni.html
    // 

    private static native void onNativeInit (Class<?> thisClass);
    
    private static native void setDownloadState (int state);
    private static native void setDownloadProgress (float progress);
    private static native void showWifiDialog ();
    
    static
    {
        onNativeInit (BeOBBManager.class);
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private static final String LOG_TAG = "LVLDownloader";
    
    private static final int BE_OBB_MANAGER_STATE_NOT_STARTED = 0;
    private static final int BE_OBB_MANAGER_STATE_CHECKING = 1;
    private static final int BE_OBB_MANAGER_STATE_DOWNLOADING = 2;
    private static final int BE_OBB_MANAGER_STATE_DOWNLOADED = 3;
    private static final int BE_OBB_MANAGER_STATE_VERIFYING = 4;
    private static final int BE_OBB_MANAGER_STATE_READY = 5;

    private IDownloaderService mRemoteService;
    
    private static boolean mWifiDialogShown = false;
        
    private IStub mDownloaderClientStub;
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    public void Initialise()
    {
        s_hostActivity = BeNativeActivity.s_gameActivity;

        // 
        // Register class event listener.
        // 
        BeNativeActivity.RegisterEventsListener (this, BeNativeActivity.ACTIVITY_EVENT_START | BeNativeActivity.ACTIVITY_EVENT_STOP);
    }

    public void Deinitialise()
    {
        // 
        // Unregister class event listener.
        // 

        BeNativeActivity.UnregisterEventsListener (this);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public boolean onHandleActivityEvent (final int eventType, final Activity activity, final Intent data, final int requestCode, final int resultCode)
    {
        BeDebug.i(TAG, "eventType : " + eventType + " - activity : " + activity + " - data : " + data + " - requestCode : " + requestCode + " - resultCode : " + resultCode);
        switch(eventType)
        {
            case BeNativeActivity.ACTIVITY_EVENT_START:
            {
                if (!mWifiDialogShown)
                {
                    if (null != mDownloaderClientStub)
                    {
                        mDownloaderClientStub.connect(s_hostActivity);
                    }
                }

                break;
            }
            case BeNativeActivity.ACTIVITY_EVENT_STOP:
            {
                if (!mWifiDialogShown)
                {
                    if (null != mDownloaderClientStub)
                    {
                        mDownloaderClientStub.disconnect(s_hostActivity);
                    }
                }

                break;
            }

            default:
                break;
        }

        return false;
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public void SetPublicKey(String strPublicKey)
    {
        BeDebug.i(TAG, "OBB PUBLIC KEY: " + strPublicKey);

        BeOBBDownloaderService.BASE64_PUBLIC_KEY = strPublicKey;
    }
    
    /**
     * This is a little helper class that demonstrates simple testing of an
     * Expansion APK file delivered by Market. You may not wish to hard-code
     * things such as file lengths into your executable... and you may wish to
     * turn this code off during application development.
     */
    private static class XAPKFile
    {
        public final boolean mIsMain;
        public final int mFileVersion;
        public final long mFileSize;

        XAPKFile(boolean isMain, int fileVersion, long fileSize)
        {
            mIsMain = isMain;
            mFileVersion = fileVersion;
            mFileSize = fileSize;
        }
    }
       
    private static Vector<XAPKFile> xAPKS = new Vector<XAPKFile>();

    public void AddExpansionFile(int expansionAPKVersion, int expansionFileSize)
    {
        if (xAPKS.size() < 2)
        {
            boolean isMain = (xAPKS.size() == 0);

            xAPKS.add( new XAPKFile(isMain, expansionAPKVersion, expansionFileSize) );	
        }
    }
    
    /**
     * Go through each of the APK Expansion files defined in the structure above
     * and determine if the files are present and match the required size. Free
     * applications should definitely consider doing this, as this allows the
     * application to be launched for the first time without having a network
     * connection present. Paid applications that use LVL should probably do at
     * least one LVL check that requires the network to be present, so this is
     * not as necessary.
     * 
     * @return true if they are present.
     */
    boolean expansionFilesDelivered()
    {
        for (XAPKFile xf : xAPKS)
        {
            String fileName = Helpers.getExpansionAPKFileName(s_hostActivity, xf.mIsMain, xf.mFileVersion);
            
            BeDebug.i(TAG, "expansionFilesDelivered check for: " + fileName);
            
            if (!Helpers.doesFileExist(s_hostActivity, fileName, xf.mFileSize, false))
                return false;
        }
        return true;
    }

    /**
     * Calculating a moving average for the validation speed so we don't get
     * jumpy calculations for time etc.
     */
    static private final float SMOOTHING_FACTOR = 0.005f;
        
    /**
     * If the download isn't present, we initialize the download UI. This ties
     * all of the controls into the remote service calls.
     */
    private void initializeDownloadUI() 
    {
        if (null == mDownloaderClientStub)
        {
            final IDownloaderClient downloaderClient = this;

            s_hostActivity.runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                    mDownloaderClientStub = DownloaderClientMarshaller.CreateStub(downloaderClient, BeOBBDownloaderService.class);
                    mDownloaderClientStub.connect(s_hostActivity);
                }
            });
        }
    }

    /**
     * Called when the activity is first create; we wouldn't create a layout in
     * the case where we have the file and are moving to another activity
     * without downloading.
     */

    public void CheckForExpansionFiles()
    {
        initializeDownloadUI();

        if (!expansionFilesDelivered())
        {
            BeDebug.i(TAG, "OBB CheckForExpansionFiles NOT delivered.");

            setDownloadState(BE_OBB_MANAGER_STATE_DOWNLOADING);

            try
            {
                Intent launchIntent = s_hostActivity.getIntent();
                
                Intent intentToLaunchThisActivityFromNotification = new Intent(s_hostActivity, s_hostActivity.getClass());
                intentToLaunchThisActivityFromNotification.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intentToLaunchThisActivityFromNotification.setAction(launchIntent.getAction());

                if (launchIntent.getCategories() != null)
                {
                    for (String category : launchIntent.getCategories())
                    {
                        intentToLaunchThisActivityFromNotification.addCategory(category);
                    }
                }

                // Build PendingIntent used to open this activity from
                // Notification
                PendingIntent pendingIntent = PendingIntent.getActivity(
                        s_hostActivity,
                        0, intentToLaunchThisActivityFromNotification,
                        PendingIntent.FLAG_UPDATE_CURRENT);

                // Request to start the download
                int startResult = DownloaderClientMarshaller.startDownloadServiceIfRequired(s_hostActivity, pendingIntent, BeOBBDownloaderService.class);

                if (startResult != DownloaderClientMarshaller.NO_DOWNLOAD_REQUIRED)
                {
                    // The DownloaderService has started downloading the files,
                    // show progress
                    initializeDownloadUI();
                    return;
                }
                
                // otherwise, download not needed so we fall through to
                // starting the movie
            }
            catch (NameNotFoundException e)
            {
                BeDebug.i(TAG, "OBB: Cannot find own package! MAYDAY!");
                e.printStackTrace();
            }

        }
        else
        {
            setDownloadState(BE_OBB_MANAGER_STATE_DOWNLOADED);
        }
    }

    /**
     * Critical implementation detail. In onServiceConnected we create the
     * remote service and marshaler. This is how we pass the client information
     * back to the service so the client can be properly notified of changes. We
     * must do this every time we reconnect to the service.
     */
    @Override
    public void onServiceConnected(Messenger m)
    {
        BeDebug.i(TAG, "CheckForExpansionFiles onServiceConnected");

        mRemoteService = DownloaderServiceMarshaller.CreateProxy(m);
        mRemoteService.onClientUpdated(mDownloaderClientStub.getMessenger());
    }

    /**
     * The download state should trigger changes in the UI --- it may be useful
     * to show the state as being indeterminate at times. This sample can be
     * considered a guideline.
     */
    @Override
    public void onDownloadStateChanged(int newState)
    {
        BeDebug.i(TAG, "CheckForExpansionFiles onDownloadStateChanged: " + newState);

        switch (newState) {
            case IDownloaderClient.STATE_IDLE:
                // STATE_IDLE means the service is listening, so it's
                // safe to start making calls via mRemoteService.
                break;
            case IDownloaderClient.STATE_CONNECTING:
            case IDownloaderClient.STATE_FETCHING_URL:
                break;
            case IDownloaderClient.STATE_DOWNLOADING:
                break;

            case IDownloaderClient.STATE_FAILED_CANCELED:
            case IDownloaderClient.STATE_FAILED:
            case IDownloaderClient.STATE_FAILED_FETCHING_URL:
            case IDownloaderClient.STATE_FAILED_UNLICENSED:
            {
                mDownloaderClientStub = null;
                setDownloadState(BE_OBB_MANAGER_STATE_DOWNLOADED);
                break;
            }
            case IDownloaderClient.STATE_PAUSED_NEED_CELLULAR_PERMISSION:
            case IDownloaderClient.STATE_PAUSED_WIFI_DISABLED_NEED_CELLULAR_PERMISSION:
                if (!mWifiDialogShown)
                {
                    showWifiDialog();
                    mWifiDialogShown = true;
                }
                break;

            case IDownloaderClient.STATE_PAUSED_BY_REQUEST:
                break;
            case IDownloaderClient.STATE_PAUSED_ROAMING:
            case IDownloaderClient.STATE_PAUSED_SDCARD_UNAVAILABLE:
                break;
            case IDownloaderClient.STATE_COMPLETED:
            {
                mDownloaderClientStub = null;
                setDownloadState(BE_OBB_MANAGER_STATE_DOWNLOADED);
                return;
            }
            default:
                return;
        }
    }

    public void ProceedWithCellularConnection()
    {
        mRemoteService.setDownloadFlags(IDownloaderService.FLAGS_DOWNLOAD_OVER_CELLULAR);
        mRemoteService.requestContinueDownload();
        
        mWifiDialogShown = false;
    }
    
    /**
     * Sets the state of the various controls based on the progressinfo object
     * sent from the downloader service.
     */
    @Override
    public void onDownloadProgress(DownloadProgressInfo progress)
    {
        float fDownloadProgress = (progress.mOverallProgress * 100) / progress.mOverallTotal;

        setDownloadProgress (fDownloadProgress);
    }
}
