/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeJNI.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <BeJNI.h>

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

JavaVM* g_javaVM = NULL;

JavaVM* BeJNI::s_javaVM = NULL;

JavaVMAttachArgs BeJNI::s_kAttachArgs;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

JNIEXPORT jint JNI_OnLoad (JavaVM *vm, void *reserved)
{
    BE_CORE_DEBUG("JNI_OnLoad");

    JNIEnv env;

    if (vm->GetEnv (reinterpret_cast<void**> (&env), JNI_VERSION_1_6) != JNI_OK)
    {
        return -1;
    }

    g_javaVM = vm;

    BeJNI::SetVM (g_javaVM);

    BeJNI::s_kAttachArgs.version = JNI_VERSION_1_6;
    BeJNI::s_kAttachArgs.name = "JNI_Thread";
    BeJNI::s_kAttachArgs.group = NULL;

    return JNI_VERSION_1_6;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

JNIEXPORT void JNI_OnUnload (JavaVM *vm, void *reserved)
{
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

BeJNIThreadEnv::BeJNIThreadEnv ()
  : m_threadEnv (NULL)
  , m_didAttach (false)
  , m_localAllocCount (0)
{
    Attach ();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

BeJNIThreadEnv::~BeJNIThreadEnv ()
{
    Detach ();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BeJNIThreadEnv::Attach ()
{
    int status = BeJNI::GetVM ()->GetEnv ((void **) &m_threadEnv, JNI_VERSION_1_6);

    switch (status)
    {
        case JNI_OK:
        {
            BE_ASSERT_PTR (m_threadEnv);

            break;
        }

        case JNI_EDETACHED:
        {
            status = BeJNI::GetVM()->AttachCurrentThread(&m_threadEnv, &BeJNI::s_kAttachArgs);

            BE_ASSERT(status == 0);

            BE_ASSERT_PTR (m_threadEnv);

            m_didAttach = true;

            break;
        }

        case JNI_EVERSION:
        {
            BE_CORE_ERROR ("Invalid Java VM version.");

            break;
        }
    }

    BeJNI::CheckExceptions (*this);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BeJNIThreadEnv::Detach ()
{
    BeJNI::CheckExceptions (*this);

    BE_ASSERT (m_localAllocCount == 0);

    if (m_didAttach)
    {
        BeJNI::GetVM ()->DetachCurrentThread ();
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

JavaVM *BeJNI::GetVM ()
{
    return g_javaVM;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BeJNI::SetVM (JavaVM *javaVM)
{
    s_javaVM = javaVM;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BeJNI::CheckExceptions (BeJNIThreadEnv &threadEnv)
{
    // 
    // Check if Java threw an unhandled exception.
    // 

    JNIEnv *env = threadEnv.GetEnv ();

    BE_ASSERT_PTR (env);

    if (env && env->ExceptionCheck ())
    {
        jthrowable thrownException = env->ExceptionOccurred ();

        if (thrownException)
        {
            env->ExceptionDescribe ();

            env->ExceptionClear ();
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

jclass BeJNI::FindClass (BeJNIThreadEnv &threadEnv, const char *classId)
{
    BE_ASSERT (strlen (classId));

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    jclass foundClass = env->FindClass (classId);

    BeJNI::CheckExceptions (threadEnv);

    BE_ASSERT (foundClass);

    ++threadEnv.m_localAllocCount;

    return foundClass;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

jclass BeJNI::FindClass (BeJNIThreadEnv &threadEnv, jobject classLoaderObj, const char *classId)
{
    // 
    // Convert a 'Ljava/lang/String;' style class-id to class-binary names, e.g. 'Ljava.lang.String;'.
    // 

    BE_ASSERT (classLoaderObj);

    BE_ASSERT (strlen (classId));

    std::string
        binaryClassId (classId);

    char* stringBuffer = (char*)binaryClassId.c_str ();

    for (unsigned int i = 0; i < binaryClassId.size (); ++i)
    {
        if (stringBuffer[i] == '/')
        {
            stringBuffer[i] = '.';
        }
    }

    // 
    // Find and load the requested class from the class-loader object.
    // - Note that ClassLoader.forName() will correctly load and initialise objects with static initialisation blocks, 
    // - where ClassLoader.loadClass() will not.
    // 

    BeJNI::CheckExceptions (threadEnv);

    jclass classClass = BeJNI::FindClass (threadEnv, "java/lang/Class");

    jmethodID classClass_ForName = BeJNI::GetStaticMethodID (threadEnv, classClass, "forName", "(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;");

    jstring binaryClassIdString = BeJNI::NewString (threadEnv, binaryClassId.c_str ());

    jclass forNameResult = (jclass) BeJNI::CallStaticObjectMethod (threadEnv, classClass, classClass_ForName, binaryClassIdString, true, classLoaderObj);

    BE_ASSERT (forNameResult);

    BeJNI::DeleteLocalRef (threadEnv, binaryClassIdString);

    BeJNI::DeleteLocalRef (threadEnv, classClass);

    BeJNI::CheckExceptions (threadEnv);

    return forNameResult;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

jclass BeJNI::GetObjectClass (BeJNIThreadEnv &threadEnv, jobject obj)
{
    BE_ASSERT (obj);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    jclass objectClass = env->GetObjectClass (obj);

    BeJNI::CheckExceptions (threadEnv);

    BE_ASSERT (objectClass);

    ++threadEnv.m_localAllocCount;

    return objectClass;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

jobject BeJNI::GetObjectClassLoader (BeJNIThreadEnv &threadEnv, jobject obj)
{
    BE_ASSERT (obj);

    BeJNI::CheckExceptions (threadEnv);

    jclass objectClass = BeJNI::GetObjectClass (threadEnv, obj);

    jmethodID objectClass_GetClassLoader = BeJNI::GetMethodID (threadEnv, objectClass, "getClassLoader", "()Ljava/lang/ClassLoader;");

    BE_ASSERT (objectClass_GetClassLoader);

    jobject classLoader = BeJNI::CallObjectMethod (threadEnv, obj, objectClass_GetClassLoader);

    BeJNI::CheckExceptions (threadEnv);

    BE_ASSERT (classLoader);

    BeJNI::DeleteLocalRef (threadEnv, objectClass);

    return classLoader;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

jmethodID BeJNI::GetConstructor (BeJNIThreadEnv &threadEnv, jclass clazz, const char *constructorSignature)
{
    BE_ASSERT (clazz);

    BE_ASSERT (strlen (constructorSignature));

    BeJNI::CheckExceptions (threadEnv);

    jmethodID constructorMethodResult = BeJNI::GetMethodID (threadEnv, clazz, "<init>", constructorSignature);

    BeJNI::CheckExceptions (threadEnv);

    BE_ASSERT (constructorMethodResult);

    return constructorMethodResult;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

jmethodID BeJNI::GetMethodID (BeJNIThreadEnv &threadEnv, jclass clazz, const char* methodId, const char *methodSignature)
{
    BE_ASSERT (clazz);

    BE_ASSERT (strlen (methodId));

    BE_ASSERT (strlen (methodSignature));

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    jmethodID methodResult = env->GetMethodID (clazz, methodId, methodSignature);

    BeJNI::CheckExceptions (threadEnv);

    BE_ASSERT (methodResult);

    return methodResult;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

jmethodID BeJNI::GetStaticMethodID (BeJNIThreadEnv &threadEnv, jclass clazz, const char* methodId, const char *methodSignature)
{
    BE_ASSERT (clazz);

    BE_ASSERT (strlen (methodId));

    BE_ASSERT (strlen (methodSignature));

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    jmethodID methodResult = env->GetStaticMethodID (clazz, methodId, methodSignature);

    BeJNI::CheckExceptions (threadEnv);

    BE_ASSERT (methodResult);

    return methodResult;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

jfieldID BeJNI::GetFieldID (BeJNIThreadEnv &threadEnv, jclass clazz, const char* fieldId, const char* fieldSignature)
{
    BE_ASSERT (clazz);

    BE_ASSERT (strlen (fieldId));

    BE_ASSERT (strlen (fieldSignature));

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    jfieldID fieldResult = env->GetFieldID (clazz, fieldId, fieldSignature);

    BeJNI::CheckExceptions (threadEnv);

    BE_ASSERT (fieldResult);

    return fieldResult;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

jfieldID BeJNI::GetStaticFieldID (BeJNIThreadEnv &threadEnv, jclass clazz, const char* fieldId, const char* fieldSignature)
{
    BE_ASSERT (clazz);

    BE_ASSERT (strlen (fieldId));

    BE_ASSERT (strlen (fieldSignature));

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    jfieldID fieldResult = env->GetStaticFieldID (clazz, fieldId, fieldSignature);

    BeJNI::CheckExceptions (threadEnv);

    BE_ASSERT (fieldResult);

    return fieldResult;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

jobject BeJNI::AllocObject (BeJNIThreadEnv &threadEnv, jclass clazz)
{
    BE_ASSERT (clazz);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    jobject allocObject = env->AllocObject (clazz);

    BeJNI::CheckExceptions (threadEnv);

    BE_ASSERT(allocObject);

    ++threadEnv.m_localAllocCount;

    return allocObject;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

jobject BeJNI::NewObject (BeJNIThreadEnv &threadEnv, jclass clazz, jmethodID constructorMethod, ...)
{
    BE_ASSERT (clazz);

    BE_ASSERT (constructorMethod);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    va_list args;

    va_start (args, constructorMethod);

    jobject newObject = env->NewObjectV (clazz, constructorMethod, args);

    va_end (args);

    BeJNI::CheckExceptions (threadEnv);

    BE_ASSERT(newObject);

    ++threadEnv.m_localAllocCount;

    return newObject;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

jstring BeJNI::NewString (BeJNIThreadEnv &threadEnv, const char *source)
{
    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    jstring javaString = env->NewStringUTF (source);

    BeJNI::CheckExceptions (threadEnv);

    BE_ASSERT(javaString);

    ++threadEnv.m_localAllocCount;

    return javaString;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

jstring BeJNI::NewString (BeJNIThreadEnv &threadEnv, const BeString8& source)
{
    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    jstring javaString = env->NewStringUTF (source.ToRaw ());

    BeJNI::CheckExceptions (threadEnv);

    BE_ASSERT(javaString);

    ++threadEnv.m_localAllocCount;

    return javaString;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

jstring BeJNI::NewWString(BeJNIThreadEnv &threadEnv, const BeString16& source)
{
    BeJNI::CheckExceptions(threadEnv);

    JNIEnv *env = threadEnv.GetEnv();

    jchar* temp = new jchar[source.GetSize() + 1];

    for (int i = 0; i < source.GetSize(); ++i)
    {
        temp[i] = ((wchar_t) source.ToRaw()[i]);
    }
    temp[source.GetSize()] = '\0';

    jstring javaString = env->NewString(&temp[0], source.GetSize());

    delete[] temp;

    BeJNI::CheckExceptions(threadEnv);

    BE_ASSERT(javaString);

    ++threadEnv.m_localAllocCount;

    return javaString;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

BeString8 BeJNI::GetString(BeJNIThreadEnv &threadEnv, jstring jniString)
{
    BE_ASSERT (jniString);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    const char *modifiedUtf8String = env->GetStringUTFChars (jniString, NULL);

    BeJNI::CheckExceptions (threadEnv);

    BeString8 nativeString(modifiedUtf8String);

    env->ReleaseStringUTFChars (jniString, modifiedUtf8String);

    BeJNI::CheckExceptions (threadEnv);

    return nativeString;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

BeString16 BeJNI::GetWString(BeJNIThreadEnv &threadEnv, jstring jniString)
{
    BE_ASSERT(jniString);

    BeJNI::CheckExceptions(threadEnv);

    JNIEnv *env = threadEnv.GetEnv();

    const jchar *modifiedString = env->GetStringChars(jniString, NULL);

    const size_t modifiedStringSize = env->GetStringLength(jniString);

    BeString16 nativeString = L"";

    for (int i = 0; i < modifiedStringSize; ++i)
    {
        nativeString.Append((wchar_t)modifiedString[i]);
    }

    env->ReleaseStringChars(jniString, modifiedString);

    BeJNI::CheckExceptions(threadEnv);

    return nativeString;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

jbyteArray BeJNI::NewByteArray(BeJNIThreadEnv &threadEnv, char* source, unsigned int size)
{
    BeJNI::CheckExceptions(threadEnv);

    JNIEnv *env = threadEnv.GetEnv();

    jbyteArray array = env->NewByteArray(size);
    env->SetByteArrayRegion(array, 0, size, reinterpret_cast<jbyte*>(source));

    BeJNI::CheckExceptions(threadEnv);

    ++threadEnv.m_localAllocCount;

    return array;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

char * BeJNI::GetByteArray(BeJNIThreadEnv &threadEnv, jbyteArray source)
{
    BE_ASSERT(source);

    BeJNI::CheckExceptions(threadEnv);

    JNIEnv *env = threadEnv.GetEnv();

    int len = env->GetArrayLength(source);
    char* buf = new char[len];
    env->GetByteArrayRegion(source, 0, len, reinterpret_cast<jbyte*>(buf));

    BeJNI::CheckExceptions(threadEnv);

    return buf;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

jobjectArray BeJNI::NewObjectArray (BeJNIThreadEnv &threadEnv, jsize length, jclass elementClass, jobject initialElement)
{
    BE_ASSERT (length);

    BE_ASSERT (elementClass);

    BE_ASSERT (initialElement);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    jobjectArray newObjectArray = env->NewObjectArray (length, elementClass, initialElement);

    BeJNI::CheckExceptions (threadEnv);

    BE_ASSERT(newObjectArray);

    ++threadEnv.m_localAllocCount;

    return newObjectArray;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

jobject BeJNI::GetObjectArrayElement (BeJNIThreadEnv &threadEnv, jobjectArray array, jsize index)
{
    BE_ASSERT (array);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    jobject arrayElementObj = env->GetObjectArrayElement (array, index);

    if (arrayElementObj)
    {
        ++threadEnv.m_localAllocCount;
    }

    BeJNI::CheckExceptions (threadEnv);

    return arrayElementObj;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BeJNI::SetObjectArrayElement (BeJNIThreadEnv &threadEnv, jobjectArray array, jsize index, jobject value)
{
    BE_ASSERT (array);

    BE_ASSERT (value);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    env->SetObjectArrayElement (array, index, value);

    BeJNI::CheckExceptions (threadEnv);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

jobject BeJNI::NewLocalRef (BeJNIThreadEnv &threadEnv, jobject obj)
{
    BE_ASSERT (obj);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    jobject localObj = env->NewLocalRef (obj);

    if (localObj)
    {
        ++threadEnv.m_localAllocCount;
    }

    BeJNI::CheckExceptions (threadEnv);

    BE_ASSERT(localObj);

    return localObj;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BeJNI::DeleteLocalRef (BeJNIThreadEnv &threadEnv, jobject obj)
{
    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    if (obj)
    {
        env->DeleteLocalRef (obj);

        --threadEnv.m_localAllocCount;
    }

    BeJNI::CheckExceptions (threadEnv);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

jobject BeJNI::NewGlobalRef (BeJNIThreadEnv &threadEnv, jobject obj)
{
    BE_ASSERT (obj);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    jobject globalObj = env->NewGlobalRef (obj);

    BeJNI::CheckExceptions (threadEnv);

    BE_ASSERT(globalObj);

    return globalObj;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BeJNI::DeleteGlobalRef (BeJNIThreadEnv &threadEnv, jobject obj)
{
    BE_ASSERT (obj);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    env->DeleteGlobalRef (obj);

    BeJNI::CheckExceptions (threadEnv);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

jobject BeJNI::NewWeakGlobalRef (BeJNIThreadEnv &threadEnv, jobject obj)
{
    BE_ASSERT (obj);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    jobject weakGlobalObj = env->NewWeakGlobalRef (obj);

    BeJNI::CheckExceptions (threadEnv);

    BE_ASSERT(weakGlobalObj);

    return weakGlobalObj;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BeJNI::DeleteWeakGlobalRef (BeJNIThreadEnv &threadEnv, jobject obj)
{
    BE_ASSERT (obj);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    env->DeleteWeakGlobalRef (obj);

    BeJNI::CheckExceptions (threadEnv);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

jobject BeJNI::LocalToGlobalRef (BeJNIThreadEnv &threadEnv, jobject obj)
{
    BE_ASSERT (obj);

    BeJNI::CheckExceptions (threadEnv);

    jobject globalRefObj = NewGlobalRef (threadEnv, obj);

    DeleteLocalRef (threadEnv, obj);

    BeJNI::CheckExceptions (threadEnv);

    return globalRefObj;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

jobject BeJNI::LocalToWeakGlobalRef (BeJNIThreadEnv &threadEnv, jobject obj)
{
    BE_ASSERT (obj);

    BeJNI::CheckExceptions (threadEnv);

    jobject weakGlobalRefObj = NewWeakGlobalRef (threadEnv, obj);

    DeleteLocalRef (threadEnv, obj);

    BeJNI::CheckExceptions (threadEnv);

    return weakGlobalRefObj;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

jboolean BeJNI::IsSameObject (BeJNIThreadEnv &threadEnv, jobject ref1, jobject ref2)
{
    BE_ASSERT (ref1);

    BE_ASSERT (ref2);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    jboolean isSameObject = env->IsSameObject (ref1, ref2);

    BeJNI::CheckExceptions (threadEnv);

    return isSameObject;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
