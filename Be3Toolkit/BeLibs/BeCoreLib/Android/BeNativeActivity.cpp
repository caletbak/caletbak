/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeNativeActivity.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <BeNativeActivity.h>

#include <BeJNI.h>
//#include <CommonTypes.h>

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const BeChar8* g_BeNativeActivityClassName = "org/bakengine/BeNativeActivity";

jclass g_pBeNativeActivityClass = 0;

jobject g_pBeNativeActivityObj = 0;

jobject g_pBeNativeActivityClassLoader = 0;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

bool BeNativeActivity::s_initialised = false;

bool BeNativeActivity::s_GLContextWasDestroyed = false;

unsigned int BeNativeActivity::s_statusFlags = 0;

bool  BeNativeActivity::s_stopUpdate = false;

unsigned int BeNativeActivity::s_destroyPreviousThread = 0;

struct android_app* BeNativeActivity::s_androidApp = NULL;

BeNativeActivity::RegisteredCallbacks BeNativeActivity::s_registeredCallbacks;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BeNativeActivity_dummy()
{
    // Stub function to prevent code-stripping.
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BeNativeActivity::Initialise(struct android_app *androidApp, const RegisteredCallbacks &registeredCallbacks)
{
    BE_ASSERT_PTR(androidApp);

    s_androidApp = androidApp;

    s_androidApp->onAppCmd = ProcessActivityEvent;

    s_androidApp->onInputEvent = ProcessInputEvent;

    s_registeredCallbacks = registeredCallbacks;

    s_statusFlags = 0;

    s_initialised = true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BeNativeActivity::Deinitialise()
{
    BE_ASSERT(s_initialised);

    s_statusFlags = 0;

    s_androidApp = NULL;

    s_initialised = false;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

unsigned int BeNativeActivity::Update()
{
    BE_ASSERT(s_initialised);

    PollOSLooper((s_statusFlags & NATIVE_ACTIVITY_FLAG_IS_PAUSED) ? 10 : 0);

    if ((s_statusFlags & NATIVE_ACTIVITY_FLAG_HAS_WINDOW) )
    {
        if (s_registeredCallbacks.activityOnUpdate)
        {
            s_registeredCallbacks.activityOnUpdate(s_androidApp);
        }
    }

    return s_statusFlags;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BeNativeActivity::RequestTermination()
{
    // 
    // Flag a requested termination of the Native Activity instance.
    // The principal is that this function should signal a DESTROY event from Update(), and clean remaining events/instances in Deinitialise().
    // 

    BE_ASSERT(s_initialised);

    EnterCriticalSection();

    s_statusFlags |= NATIVE_ACTIVITY_FLAG_DESTROY_REQUESTED;

    s_statusFlags &= ~NATIVE_ACTIVITY_FLAG_IS_STARTED;

    ExitCriticalSection();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BeNativeActivity::EnterCriticalSection()
{
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BeNativeActivity::ExitCriticalSection()
{
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BeNativeActivity::PollOSLooper(const int timeoutMilliseconds)
{
    BE_ASSERT(s_initialised);

    int ident;
    int events;

    struct android_poll_source* source;

    while ((ident = ALooper_pollAll(timeoutMilliseconds, NULL, &events, (void**)&source)) >= 0)
    {
        if (ident == ALOOPER_POLL_TIMEOUT)
        {
            break;
        }

        if (source != NULL)
        {
            source->process(s_androidApp, source);
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BeNativeActivity::ProcessActivityEvent(struct android_app *androidApp, int32_t cmd)
{
    BE_ASSERT(s_initialised);

    BE_ASSERT_PTR(androidApp);

    EnterCriticalSection();

    switch (cmd)
    {
        case APP_CMD_START:
        {
            s_statusFlags |= NATIVE_ACTIVITY_FLAG_IS_STARTED;

            if (s_registeredCallbacks.activityOnStart)
            {
                s_registeredCallbacks.activityOnStart(androidApp);
            }

            break;
        }

        case APP_CMD_RESUME:
        {
            s_statusFlags &= ~NATIVE_ACTIVITY_FLAG_IS_PAUSED;

            if (s_registeredCallbacks.activityOnResume)
            {
                s_registeredCallbacks.activityOnResume(androidApp);
            }
            if (s_statusFlags & NATIVE_ACTIVITY_FLAG_HAS_FOCUS)
            {
                s_stopUpdate = false;
            }
            break;
        }

        case APP_CMD_PAUSE:
        {
            s_statusFlags |= NATIVE_ACTIVITY_FLAG_IS_PAUSED;

            if (s_registeredCallbacks.activityOnPause)
            {
                s_registeredCallbacks.activityOnPause(androidApp);
            }
            s_stopUpdate = true;
            break;
        }

        case APP_CMD_STOP:
        {
            s_statusFlags &= ~NATIVE_ACTIVITY_FLAG_IS_STARTED;

            if (s_registeredCallbacks.activityOnStop)
            {
                s_registeredCallbacks.activityOnStop(androidApp);
            }

            break;
        }

        case APP_CMD_DESTROY:
        {
            BE_ASSERT(!(s_statusFlags & NATIVE_ACTIVITY_FLAG_IS_STARTED));

            s_statusFlags |= NATIVE_ACTIVITY_FLAG_DESTROY_REQUESTED;

            s_destroyPreviousThread = gettid ();

            if (s_registeredCallbacks.activityOnDestroy)
            {
                s_registeredCallbacks.activityOnDestroy(androidApp);
            }

            break;
        }

        case APP_CMD_INIT_WINDOW:
        {
            BE_ASSERT(androidApp->window);

            if (androidApp->window)
            {
                s_statusFlags |= NATIVE_ACTIVITY_FLAG_HAS_WINDOW;

                if (s_registeredCallbacks.activityOnInitView)
                {
                    s_registeredCallbacks.activityOnInitView(androidApp);
                }
            }
            else
            {
                s_statusFlags &= ~NATIVE_ACTIVITY_FLAG_HAS_WINDOW;
            }

            break;
        }

        case APP_CMD_TERM_WINDOW:
        {
            s_statusFlags &= ~NATIVE_ACTIVITY_FLAG_HAS_WINDOW;

            //s_statusFlags &= ~NATIVE_ACTIVITY_FLAG_HAS_FOCUS;

            if (s_registeredCallbacks.activityOnReleaseView)
            {
            s_registeredCallbacks.activityOnReleaseView(androidApp);
            }

            break;
        }

        case APP_CMD_GAINED_FOCUS:
        {
            s_statusFlags |= NATIVE_ACTIVITY_FLAG_HAS_FOCUS;

            if (s_registeredCallbacks.activityOnGainFocus)
            {
                s_registeredCallbacks.activityOnGainFocus(androidApp);
            }
            s_stopUpdate = false;

            break;
        }

        case APP_CMD_LOST_FOCUS:
        {
            s_statusFlags &= ~NATIVE_ACTIVITY_FLAG_HAS_FOCUS;

            if (s_registeredCallbacks.activityOnLoseFocus)
            {
                s_registeredCallbacks.activityOnLoseFocus(androidApp);
            }
            s_stopUpdate = true;

            break;
        }

        case APP_CMD_CONFIG_CHANGED:
        {
            s_statusFlags |= NATIVE_ACTIVITY_FLAG_CONFIG_CHANGED;

            if (s_registeredCallbacks.activityOnConfigChanged)
            {
                s_registeredCallbacks.activityOnConfigChanged(androidApp);
            }

            break;
        }

        case APP_CMD_LOW_MEMORY:
        {
            s_statusFlags |= NATIVE_ACTIVITY_FLAG_LOW_MEMORY;

            if (s_registeredCallbacks.activityOnLowMemoryWarning)
            {
                s_registeredCallbacks.activityOnLowMemoryWarning(androidApp);
            }

            break;
        }
    }

    ExitCriticalSection();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int BeNativeActivity::ProcessInputEvent(struct android_app* pApp, AInputEvent* pInputEv)
{
    BE_ASSERT(s_initialised);

    BE_ASSERT_PTR(pApp);

    BE_ASSERT_PTR(pInputEv);

    if (s_registeredCallbacks.activityOnInputEvent)
    {
        return s_registeredCallbacks.activityOnInputEvent(pApp, pInputEv);
    }

    return 0;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

JNIEXPORT void Java_org_bakengine_BeNativeActivity_onNativeCreate(JNIEnv *env, jobject obj, jclass thisClass, jobject savedInstanceState)
{
    BE_CORE_DEBUG("BeNativeActivity onNativeCreate");

    BE_ASSERT(env);

    BE_ASSERT(BeJNI::GetVM());

    // 
    // Cache global references to this newly created BeAndroidNativeActivity, for if they are ever specifically required.
    // 

    BeJNIThreadEnv jniThread;

    g_pBeNativeActivityObj = BeJNI::NewGlobalRef(jniThread, obj);

    g_pBeNativeActivityClass = thisClass;

    g_pBeNativeActivityClass = (jclass)BeJNI::NewGlobalRef(jniThread, g_pBeNativeActivityClass);

    g_pBeNativeActivityClassLoader = BeJNI::GetObjectClassLoader(jniThread, g_pBeNativeActivityObj);

    g_pBeNativeActivityClassLoader = BeJNI::LocalToGlobalRef(jniThread, g_pBeNativeActivityClassLoader);

    BeJNI::CheckExceptions(jniThread);

    // 
    // Sanity type checking.
    // 

    jclass androidAppActivity = BeJNI::FindClass(jniThread, "android/app/Activity");

    BE_ASSERT(env->IsAssignableFrom(thisClass, androidAppActivity) == JNI_TRUE);

    BE_ASSERT(env->IsAssignableFrom(g_pBeNativeActivityClass, androidAppActivity) == JNI_TRUE);

    BeJNI::DeleteLocalRef (jniThread, androidAppActivity);

    BeJNI::CheckExceptions(jniThread);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

