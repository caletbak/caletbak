/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeOBBManager.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <BeOBBManager.h>

#include <Common/BeAlertDialog.h>
#include <Common/BeDeviceUtils.h>
#include <IO/BeFileTool.h>
#include <IO/BeFileZip.h>

/*
#include <Utils/Localization.h>
*/

#include <BeJNI.h>

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

std::vector<BeOBBManager::BeOBBExpectedFile*> BeOBBManager::s_kExpectedOBBFiles = std::vector<BeOBBManager::BeOBBExpectedFile*>();

std::vector<BeFileZip*> BeOBBManager::s_kOBBs = std::vector<BeFileZip*>();

BeOBBManager::eBeOBBManagerState BeOBBManager::s_eState = BeOBBManager::BE_OBB_MANAGER_STATE_NOT_STARTED;

BeFloat BeOBBManager::s_fDownloadProgress = 0.0f;

BeInt BeOBBManager::s_iTotalToDownload = 0;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

extern jobject g_pBeNativeActivityClassLoader;

extern jobject g_pBeNativeActivityObj;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

static jclass
    g_pBeOBBManagerClass = 0;

static const char
    *g_pBeOBBManagerClassName = "org/bakengine/BeOBBManager";

static jobject
    g_pBeOBBManagerObject = 0;

static jmethodID
    g_pBeOBBManager_Constructor = 0,
    g_pBeOBBManager_Initialise = 0,
    g_pBeOBBManager_Deinitialise = 0,
    g_pBeOBBManager_SetPublicKey = 0,
    g_pBeOBBManager_AddExpansionFile = 0,
    g_pBeOBBManager_CheckForExpansionFiles = 0,
    g_pBeOBBManager_ProceedWithCellularConnection = 0;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

static const BeString8 OBB_CRC_CHECKED_FLAG = "downloadedOBBCRCChecked";

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

JNIEXPORT void Java_org_bakengine_BeOBBManager_onNativeInit(JNIEnv *env, jobject obj, jclass thisClass)
{
    // 
    // Cache class method references.
    // 

    // cpp to Java (declared and defined in Java)
    BeJNIThreadEnv jniThread;

    g_pBeOBBManager_Constructor = BeJNI::GetMethodID(jniThread, thisClass, "<init>", "()V");

    g_pBeOBBManager_Initialise = BeJNI::GetMethodID(jniThread, thisClass, "Initialise", "()V");

    g_pBeOBBManager_Deinitialise = BeJNI::GetMethodID(jniThread, thisClass, "Deinitialise", "()V");

    g_pBeOBBManager_SetPublicKey = BeJNI::GetMethodID(jniThread, thisClass, "SetPublicKey", "(Ljava/lang/String;)V");

    g_pBeOBBManager_AddExpansionFile = BeJNI::GetMethodID(jniThread, thisClass, "AddExpansionFile", "(II)V");

    g_pBeOBBManager_CheckForExpansionFiles = BeJNI::GetMethodID(jniThread, thisClass, "CheckForExpansionFiles", "()V");

    g_pBeOBBManager_ProceedWithCellularConnection = BeJNI::GetMethodID(jniThread, thisClass, "ProceedWithCellularConnection", "()V");

    BeJNI::CheckExceptions(jniThread);

    // 
    // Register native callbacks.
    // 

    // Java to cpp (declared and defined in C++)
    const JNINativeMethod nativeMethodDefs[] =
    {
        { "setDownloadState", "(I)V", (BeVoidP)&setDownloadState },
        { "setDownloadProgress", "(F)V", (BeVoidP)&setDownloadProgress },
        { "showWifiDialog", "()V", (BeVoidP)&showWifiDialog }
    };

    env->RegisterNatives(thisClass, nativeMethodDefs, sizeof(nativeMethodDefs) / sizeof(nativeMethodDefs[0]));

    BeJNI::CheckExceptions(jniThread);
}

/////////////////////////////////////////////////////////////////////////////

JNIEXPORT void JNICALL setDownloadState(JNIEnv *env, jobject obj, BeInt state)
{
    BeOBBManager::s_eState = (BeOBBManager::eBeOBBManagerState) state;

    if (BeOBBManager::s_eState == BeOBBManager::BE_OBB_MANAGER_STATE_DOWNLOADING)
    {
        //BeDeviceUtils::SetDeviceFlag(OBB_CRC_CHECKED_FLAG, "false");
    }
}
/////////////////////////////////////////////////////////////////////////////


JNIEXPORT void JNICALL setDownloadProgress(JNIEnv *env, jobject obj, float progress)
{
    BeOBBManager::s_fDownloadProgress = progress;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class BeWifiAlert : public BeAlertDialog::BeAlertDialogDelegate
{
public:

    virtual void onPressedButton(BeAlertDialog::eBeAlertDialogButton buttonID, const BeString16& textFieldValue)
    {
        if (buttonID == 1)
        {
            BeOBBManager::ProceedWithCellularConnection();
        }
        else
        {
            BeDeviceUtils::ShowWifiSettings();
        }
    }

};

/////////////////////////////////////////////////////////////////////////////

static BeWifiAlert* s_pWifiAlertDialog = NULL;

/////////////////////////////////////////////////////////////////////////////

JNIEXPORT void JNICALL showWifiDialog(JNIEnv *env, jobject obj)
{
    if (!s_pWifiAlertDialog)
    {
        s_pWifiAlertDialog = new BeWifiAlert();
    }

    BeString16 strTitle = L"";

    //BeString16 strMessage = localization::localize("TID_OBB_CONNECTION", BeOBBManager::GetTotalDataToDownload());
    //BeString16 strButtonProceed = localization::localize("TID_OBB_PROCEED");
    //BeString16 strButtonWifi = localization::localize("TID_OBB_WIFI_SETTINGS");

    BeAlertDialog::GetInstance()->SetCurrentDelegate(s_pWifiAlertDialog);

    //BeAlertDialog::GetInstance()->Show(BeAlertDialog::DIALOG_INFO, strTitle, strMessage, strButtonProceed, strButtonWifi, L"", L"", BE_FALSE, BE_TRUE, BE_FALSE, BE_FALSE);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

BeOBBManager::BeOBBExpectedFile::BeOBBExpectedFile(){}
BeOBBManager::BeOBBExpectedFile::~BeOBBExpectedFile(){}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BeOBBManager::Initialise(const BeString8& strAndroidPublicKey, const BeString8& strOBBMainPassword, const BeString8& strOBBPatchPassword)
{
    BE_CORE_DEBUG("BeOBBManager: Initialise");
    BE_CORE_DEBUG("BeOBBManager  PublicKey = %s", strAndroidPublicKey.ToRaw());
    BE_CORE_DEBUG("BeOBBManager  strOBBMainPassword = %s", strOBBMainPassword.ToRaw());
    BE_CORE_DEBUG("BeOBBManager  strOBBPatchPassword = %s", strOBBPatchPassword.ToRaw());

    // 
    // Probe and register required API functions.
    //

    BeJNIThreadEnv jniThread;

    g_pBeOBBManagerClass = BeJNI::FindClass(jniThread, g_pBeNativeActivityClassLoader, g_pBeOBBManagerClassName);

    g_pBeOBBManagerClass = (jclass)BeJNI::LocalToGlobalRef(jniThread, g_pBeOBBManagerClass);

    // 
    // Create a new instance of this class.
    // 

    g_pBeOBBManagerObject = BeJNI::NewObject(jniThread, g_pBeOBBManagerClass, g_pBeOBBManager_Constructor);

    g_pBeOBBManagerObject = BeJNI::LocalToGlobalRef(jniThread, g_pBeOBBManagerObject);

    // 
    // Set publicKey to obb downloader
    // 

    jstring strPublicKey = BeJNI::NewString(jniThread, strAndroidPublicKey.ToRaw());

    BeJNI::CallVoidMethod(jniThread, g_pBeOBBManagerObject, g_pBeOBBManager_SetPublicKey, strPublicKey);

    BeJNI::DeleteLocalRef(jniThread, strPublicKey);

    // 
    // Initialise BeOBBManager
    // 

    BeJNI::CallVoidMethod(jniThread, g_pBeOBBManagerObject, g_pBeOBBManager_Initialise);

    BeJNI::CheckExceptions(jniThread);

    // We load the expansionFiles.dat file for retrieving the OBBs info
     
    BeFileTool kOBBInfoFile;
    kOBBInfoFile.OpenFile("assets/expansionFiles.dat");

    s_iTotalToDownload = 0;

    if (kOBBInfoFile.IsFileReady())
    {
        int iNumberOfOBBs = kOBBInfoFile.ReadInt();

        for (int i = 0; i < iNumberOfOBBs; ++i)
        {
            int iOBBVersion = kOBBInfoFile.ReadInt();

            int iOBBSize = kOBBInfoFile.ReadInt();

            s_iTotalToDownload += iOBBSize;

            BeJNI::CallVoidMethod(jniThread, g_pBeOBBManagerObject, g_pBeOBBManager_AddExpansionFile, iOBBVersion, iOBBSize);

            BeJNI::CheckExceptions(jniThread);

            BeOBBManager::BeOBBExpectedFile* pOBBExpectedFile = new BeOBBManager::BeOBBExpectedFile();

            int iOBBNumberOfFiles = kOBBInfoFile.ReadInt();

            for (int j = 0; j < iOBBNumberOfFiles; ++j)
            {
                BeInt iStringSize = kOBBInfoFile.ReadInt();
                BeString8 strOBBFileName = kOBBInfoFile.ReadString(iStringSize);

                unsigned int iOBBFileCRC = kOBBInfoFile.ReadInt();

                pOBBExpectedFile->m_kFileCRCs[strOBBFileName] = iOBBFileCRC;
            }

            s_kExpectedOBBFiles.push_back (pOBBExpectedFile);

            std::ostringstream convert;   // stream used for the conversion
            convert << iOBBVersion;      // insert the textual representation of 'Number' in the characters in the stream

            /*
            BeString8 strOBBZipFilename = BeDeviceUtils::GetExternalStorageDir(EXTERNAL_OBB_DIRECTORY);
            strOBBZipFilename += "/Android/obb/";
            strOBBZipFilename += BeDeviceUtils::GetPackageName();
            strOBBZipFilename += "/";

            BeString8 strOBBZipPassword;
            if (i == 0)
            {
                strOBBZipFilename += "main.";
                strOBBZipPassword = strOBBMainPassword;
            }
            else
            {
                strOBBZipFilename += "patch.";
                strOBBZipPassword = strOBBPatchPassword;
            }

            strOBBZipFilename.Append(convert.str());
            strOBBZipFilename += ".";
            strOBBZipFilename += BeDeviceUtils::GetPackageName();
            strOBBZipFilename += ".obb";

            BE_CORE_DEBUG("OBB strOBBZipFilename = %s", strOBBZipFilename.ToRaw());

            BeFileZip* pZipFile = new BeFileZip(strOBBZipFilename, strOBBZipPassword);

            s_kOBBs.push_back(pZipFile);
            */
        }
    }
    else
    {
        s_eState = BE_OBB_MANAGER_STATE_READY;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BeOBBManager::Deinitialise()
{
    for (int i = 0; i < s_kExpectedOBBFiles.size(); ++i)
    {
        delete s_kExpectedOBBFiles[i];
    }
    s_kExpectedOBBFiles.clear();

    for (int i = 0; i < s_kOBBs.size(); ++i)
    {
        delete s_kOBBs[i];
    }
    s_kOBBs.clear();

    BeJNIThreadEnv jniThread;

    JNIEnv *env = jniThread.GetEnv();

    BeJNI::CallVoidMethod(jniThread, g_pBeOBBManagerObject, g_pBeOBBManager_Deinitialise);

    BeJNI::DeleteGlobalRef(jniThread, g_pBeOBBManagerObject);

    BeJNI::DeleteGlobalRef(jniThread, g_pBeOBBManagerClass);

    BeJNI::CheckExceptions(jniThread);

    g_pBeOBBManagerObject = 0;

    g_pBeOBBManagerClass = 0;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BeOBBManager::CheckForExpansionFiles()
{
    s_fDownloadProgress = 0.0f;

    s_eState = BE_OBB_MANAGER_STATE_CHECKING;

    BeJNIThreadEnv jniThread;

    BeJNI::CallVoidMethod(jniThread, g_pBeOBBManagerObject, g_pBeOBBManager_CheckForExpansionFiles);

    BeJNI::CheckExceptions(jniThread);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BeOBBManager::VerifyDownloadedOBBs(void)
{
    s_eState = BE_OBB_MANAGER_STATE_VERIFYING;

    bool bVerificationFailed = false;

    // External and expected CRC Verification
    for (int i = 0; i < s_kExpectedOBBFiles.size(); ++i)
    {
        if (!VerifyExpectedCRCs(i))
        {
            bVerificationFailed = true;
            break;
        }
    }

    if (!bVerificationFailed)
    {
        // For the first time after downloaded the OBBs must be CRC check for avoiding downloading errors
        /*
        BeString8 strCheckedFlag = BeDeviceUtils::GetDeviceFlag(OBB_CRC_CHECKED_FLAG);
        if (strCheckedFlag.empty() || strCheckedFlag.compare("false") == 0)
        {
            BE_CORE_DEBUG("OBB Verifying Internal CRCs");

            for (int i = 0; i < s_kOBBs.size(); ++i)
            {
                BeFileZip* pOBBDownloaded = s_kOBBs[i];

                if (pOBBDownloaded->Mount())
                {
                    if (!pOBBDownloaded->VerifyInternalCRCs())
                    {
                        bVerificationFailed = true;
                    }

                    pOBBDownloaded->Unmount();
                }
                else
                {
                    bVerificationFailed = true;
                }

                if (bVerificationFailed)
                {
                    break;
                }
            }
        }
        */
    }

    if (bVerificationFailed)
    {
        //BeDeviceUtils::SetDeviceFlag(OBB_CRC_CHECKED_FLAG, "false");

        for (int i = 0; i < s_kOBBs.size(); ++i)
        {
            BeFileZip* pOBBDownloaded = s_kOBBs[i];

            BeString8 strFileToDelete = pOBBDownloaded->GetZipFileName ();

            BeDeviceUtils::DeleteExternalFile(strFileToDelete);
        }

        s_eState = BE_OBB_MANAGER_STATE_NOT_STARTED;
    }
    else
    {
        for (int i = 0; i < s_kOBBs.size(); ++i)
        {
            s_kOBBs[i]->Mount();
        }

        //BeDeviceUtils::SetDeviceFlag(OBB_CRC_CHECKED_FLAG, "true");

        s_eState = BE_OBB_MANAGER_STATE_READY;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

bool BeOBBManager::VerifyExpectedCRCs (int iOBBIdx)
{
    BeOBBExpectedFile* pExpectedOBB = s_kExpectedOBBFiles[iOBBIdx];

    BeFileZip* pOBBDownloaded = s_kOBBs[iOBBIdx];

    bool bVerificationSuccess = true;

    if (pOBBDownloaded->Mount())
    {
        std::map<BeString8, BeUInt>::iterator it = pExpectedOBB->m_kFileCRCs.begin();
        while (it != pExpectedOBB->m_kFileCRCs.end())
        {
            BeFileZip::BeFileZipEntry* pZipEntry = pOBBDownloaded->GetZipEntry(it->first);

            if (pZipEntry)
            {
                if (it->second != pZipEntry->m_uCRC)
                {
                    bVerificationSuccess = false;
                    break;
                }
            }
            else
            {
                bVerificationSuccess = false;
                break;
            }

            it++;
        }

        pOBBDownloaded->Unmount();
    }
    else
    {
        bVerificationSuccess = false;
    }

    return bVerificationSuccess;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

BeFileInterface* BeOBBManager::OpenFile (const BeString8& strFilename)
{
    if (s_eState == BE_OBB_MANAGER_STATE_READY)
    {
        for (int i = s_kOBBs.size()-1; i >= 0; i--)
        {
            BeFileInterface* fileInZip = s_kOBBs[i]->OpenZipFile(strFilename);

            if (fileInZip)
            {
                return fileInZip;
            }
        }
    }

    return NULL;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

bool BeOBBManager::FileExists(const BeString8& strFilename)
{
    if (s_eState == BE_OBB_MANAGER_STATE_READY)
    {
        for (int i = s_kOBBs.size() - 1; i >= 0; i--)
        {
            BeFileZip::BeFileZipEntry* fileInZip = s_kOBBs[i]->GetZipEntry(strFilename);

            if (fileInZip)
            {
                return fileInZip;
            }
        }
    }

    return false;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

BeOBBManager::eBeOBBManagerState BeOBBManager::GetState(void)
{
    return s_eState;
}

float BeOBBManager::GetDownloadProgress(void)
{
    return s_fDownloadProgress;
}

BeString16 BeOBBManager::GetTotalDataToDownload(void)
{
    int toDownloadMBs = s_iTotalToDownload / 1024 / 1024;

    std::wostringstream os;

    if (toDownloadMBs > 0)
    {
        os << toDownloadMBs;
        os << L"MBs";
    }
    else
    {
        toDownloadMBs = s_iTotalToDownload / 1024;
        os << toDownloadMBs;
        os << L"KBs";
    }

    return os.str().c_str();
}

void BeOBBManager::ProceedWithCellularConnection(void)
{
    BeJNIThreadEnv jniThread;

    BeJNI::CallVoidMethod(jniThread, g_pBeOBBManagerObject, g_pBeOBBManager_ProceedWithCellularConnection);

    BeJNI::CheckExceptions(jniThread);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
