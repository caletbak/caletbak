/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeJNI.inl
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_JNI_INL
#define BE_JNI_INL

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline void BeJNI::CallVoidMethod (BeJNIThreadEnv &threadEnv, jobject obj, jmethodID methodId, ...)
{
    BE_ASSERT (obj);

    BE_ASSERT (methodId);

    BeJNI::CheckExceptions (threadEnv);

    va_list args;

    va_start (args, methodId);

    JNIEnv *env = threadEnv.GetEnv ();

    env->CallVoidMethodV (obj, methodId, args);

    va_end (args);

    BeJNI::CheckExceptions (threadEnv);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline jobject BeJNI::CallObjectMethod(BeJNIThreadEnv &threadEnv, jobject obj, jmethodID methodId, ...)
{
    BE_ASSERT (obj);

    BE_ASSERT (methodId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    va_list args;

    va_start (args, methodId);

    jobject objectMethodRet = env->CallObjectMethodV (obj, methodId, args);

    va_end (args);

    BeJNI::CheckExceptions (threadEnv);

    if (objectMethodRet)
    {
        ++threadEnv.m_localAllocCount;
    }

    return objectMethodRet;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline jboolean BeJNI::CallBooleanMethod (BeJNIThreadEnv &threadEnv, jobject obj, jmethodID methodId, ...)
{
    BE_ASSERT (obj);

    BE_ASSERT (methodId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    va_list args;

    va_start (args, methodId);

    jboolean booleanMethodRet = env->CallBooleanMethodV (obj, methodId, args);

    va_end (args);

    BeJNI::CheckExceptions (threadEnv);

    return booleanMethodRet;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline jbyte BeJNI::CallByteMethod (BeJNIThreadEnv &threadEnv, jobject obj, jmethodID methodId, ...)
{
    BE_ASSERT (obj);

    BE_ASSERT (methodId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    va_list args;

    va_start (args, methodId);

    jbyte byteMethodRet = env->CallByteMethodV (obj, methodId, args);

    va_end (args);

    BeJNI::CheckExceptions (threadEnv);

    return byteMethodRet;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline jchar BeJNI::CallCharMethod (BeJNIThreadEnv &threadEnv, jobject obj, jmethodID methodId, ...)
{
    BE_ASSERT (obj);

    BE_ASSERT (methodId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    va_list args;

    va_start (args, methodId);

    jchar charMethodRet = env->CallCharMethodV (obj, methodId, args);

    va_end (args);

    BeJNI::CheckExceptions (threadEnv);

    return charMethodRet;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline jshort BeJNI::CallShortMethod (BeJNIThreadEnv &threadEnv, jobject obj, jmethodID methodId, ...)
{
    BE_ASSERT (obj);

    BE_ASSERT (methodId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    va_list args;

    va_start (args, methodId);

    jshort shortMethodRet = env->CallShortMethodV (obj, methodId, args);

    va_end (args);

    BeJNI::CheckExceptions (threadEnv);

    return shortMethodRet;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline jint BeJNI::CallIntMethod (BeJNIThreadEnv &threadEnv, jobject obj, jmethodID methodId, ...)
{
    BE_ASSERT (obj);

    BE_ASSERT (methodId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    va_list args;

    va_start (args, methodId);

    jint intMethodRet = env->CallIntMethodV (obj, methodId, args);

    va_end (args);

    BeJNI::CheckExceptions (threadEnv);

    return intMethodRet;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline jlong BeJNI::CallLongMethod (BeJNIThreadEnv &threadEnv, jobject obj, jmethodID methodId, ...)
{
    BE_ASSERT (obj);

    BE_ASSERT (methodId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    va_list args;

    va_start (args, methodId);

    jlong longMethodRet = env->CallLongMethodV (obj, methodId, args);

    va_end (args);

    BeJNI::CheckExceptions (threadEnv);

    return longMethodRet;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline jfloat BeJNI::CallFloatMethod (BeJNIThreadEnv &threadEnv, jobject obj, jmethodID methodId, ...)
{
    BE_ASSERT (obj);

    BE_ASSERT (methodId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    va_list args;

    va_start (args, methodId);

    jfloat floatMethodRet = env->CallFloatMethodV (obj, methodId, args);

    va_end (args);

    BeJNI::CheckExceptions (threadEnv);

    return floatMethodRet;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline jdouble BeJNI::CallDoubleMethod (BeJNIThreadEnv &threadEnv, jobject obj, jmethodID methodId, ...)
{
    BE_ASSERT (obj);

    BE_ASSERT (methodId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    va_list args;

    va_start (args, methodId);

    jdouble doubleMethodRet = env->CallDoubleMethodV (obj, methodId, args);

    va_end (args);

    BeJNI::CheckExceptions (threadEnv);

    return doubleMethodRet;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline void BeJNI::CallStaticVoidMethod (BeJNIThreadEnv &threadEnv, jclass clazz, jmethodID methodId, ...)
{
    BE_ASSERT (clazz);

    BE_ASSERT (methodId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    va_list args;

    va_start (args, methodId);

    env->CallStaticVoidMethodV (clazz, methodId, args);

    va_end (args);

    BeJNI::CheckExceptions (threadEnv);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline jobject BeJNI::CallStaticObjectMethod (BeJNIThreadEnv &threadEnv, jclass clazz, jmethodID methodId, ...)
{
    BE_ASSERT (clazz);

    BE_ASSERT (methodId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    va_list args;

    va_start (args, methodId);

    jobject objectMethodRet = env->CallStaticObjectMethodV (clazz, methodId, args);

    va_end (args);

    BeJNI::CheckExceptions (threadEnv);

    if (objectMethodRet)
    {
        ++threadEnv.m_localAllocCount;
    }

    return objectMethodRet;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline jboolean BeJNI::CallStaticBooleanMethod (BeJNIThreadEnv &threadEnv, jclass clazz, jmethodID methodId, ...)
{
    BE_ASSERT (clazz);

    BE_ASSERT (methodId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    va_list args;

    va_start (args, methodId);

    jboolean booleanMethodRet = env->CallStaticBooleanMethodV (clazz, methodId, args);

    va_end (args);

    BeJNI::CheckExceptions (threadEnv);

    return booleanMethodRet;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline jbyte BeJNI::CallStaticByteMethod (BeJNIThreadEnv &threadEnv, jclass clazz, jmethodID methodId, ...)
{
    BE_ASSERT (clazz);

    BE_ASSERT (methodId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    va_list args;

    va_start (args, methodId);

    jbyte byteMethodRet = env->CallStaticByteMethodV (clazz, methodId, args);

    va_end (args);

    BeJNI::CheckExceptions (threadEnv);

    return byteMethodRet;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline jchar BeJNI::CallStaticCharMethod (BeJNIThreadEnv &threadEnv, jclass clazz, jmethodID methodId, ...)
{
    BE_ASSERT (clazz);

    BE_ASSERT (methodId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    va_list args;

    va_start (args, methodId);

    jchar charMethodRet = env->CallStaticCharMethodV (clazz, methodId, args);

    va_end (args);

    BeJNI::CheckExceptions (threadEnv);

    return charMethodRet;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline jshort BeJNI::CallStaticShortMethod (BeJNIThreadEnv &threadEnv, jclass clazz, jmethodID methodId, ...)
{
    BE_ASSERT (clazz);

    BE_ASSERT (methodId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    va_list args;

    va_start (args, methodId);

    jshort shortMethodRet = env->CallStaticShortMethodV (clazz, methodId, args);

    va_end (args);

    BeJNI::CheckExceptions (threadEnv);

    return shortMethodRet;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline jint BeJNI::CallStaticIntMethod (BeJNIThreadEnv &threadEnv, jclass clazz, jmethodID methodId, ...)
{
    BE_ASSERT (clazz);

    BE_ASSERT (methodId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    va_list args;

    va_start (args, methodId);

    jint intMethodRet = env->CallStaticIntMethodV (clazz, methodId, args);

    va_end (args);

    BeJNI::CheckExceptions (threadEnv);

    return intMethodRet;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline jlong BeJNI::CallStaticLongMethod (BeJNIThreadEnv &threadEnv, jclass clazz, jmethodID methodId, ...)
{
    BE_ASSERT (clazz);

    BE_ASSERT (methodId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    va_list args;

    va_start (args, methodId);

    jlong longMethodRet = env->CallStaticLongMethodV (clazz, methodId, args);

    va_end (args);

    BeJNI::CheckExceptions (threadEnv);

    return longMethodRet;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline jfloat BeJNI::CallStaticFloatMethod (BeJNIThreadEnv &threadEnv, jclass clazz, jmethodID methodId, ...)
{
    BE_ASSERT (clazz);

    BE_ASSERT (methodId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    va_list args;

    va_start (args, methodId);

    jfloat floatMethodRet = env->CallStaticFloatMethodV (clazz, methodId, args);

    va_end (args);

    BeJNI::CheckExceptions (threadEnv);

    return floatMethodRet;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline jdouble BeJNI::CallStaticDoubleMethod (BeJNIThreadEnv &threadEnv, jclass clazz, jmethodID methodId, ...)
{
    BE_ASSERT (clazz);

    BE_ASSERT (methodId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    va_list args;

    va_start (args, methodId);

    jdouble doubleMethodRet = env->CallStaticDoubleMethodV (clazz, methodId, args);

    va_end (args);

    BeJNI::CheckExceptions (threadEnv);

    return doubleMethodRet;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline jobject BeJNI::GetObjectField (BeJNIThreadEnv &threadEnv, jobject obj, jfieldID fieldId)
{
    BE_ASSERT (obj);

    BE_ASSERT (fieldId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    jobject objectField = env->GetObjectField (obj, fieldId);

    BeJNI::CheckExceptions (threadEnv);

    if (objectField)
    {
        ++threadEnv.m_localAllocCount;
    }

    return objectField;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline jboolean BeJNI::GetBooleanField (BeJNIThreadEnv &threadEnv, jobject obj, jfieldID fieldId)
{
    BE_ASSERT (obj);

    BE_ASSERT (fieldId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    jboolean booleanField = env->GetBooleanField (obj, fieldId);

    BeJNI::CheckExceptions (threadEnv);

    return booleanField;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline jbyte BeJNI::GetByteField (BeJNIThreadEnv &threadEnv, jobject obj, jfieldID fieldId)
{
    BE_ASSERT (obj);

    BE_ASSERT (fieldId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    jbyte byteField = env->GetByteField (obj, fieldId);

    BeJNI::CheckExceptions (threadEnv);

    return byteField;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline jchar BeJNI::GetCharField (BeJNIThreadEnv &threadEnv, jobject obj, jfieldID fieldId)
{
    BE_ASSERT (obj);

    BE_ASSERT (fieldId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    jchar charField = env->GetCharField (obj, fieldId);

    BeJNI::CheckExceptions (threadEnv);

    return charField;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline jshort BeJNI::GetShortField (BeJNIThreadEnv &threadEnv, jobject obj, jfieldID fieldId)
{
    BE_ASSERT (obj);

    BE_ASSERT (fieldId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    jshort shortField = env->GetShortField (obj, fieldId);

    BeJNI::CheckExceptions (threadEnv);

    return shortField;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline jint BeJNI::GetIntField (BeJNIThreadEnv &threadEnv, jobject obj, jfieldID fieldId)
{
    BE_ASSERT (obj);

    BE_ASSERT (fieldId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    jint intField = env->GetIntField (obj, fieldId);

    BeJNI::CheckExceptions (threadEnv);

    return intField;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline jlong BeJNI::GetLongField (BeJNIThreadEnv &threadEnv, jobject obj, jfieldID fieldId)
{
    BE_ASSERT (obj);

    BE_ASSERT (fieldId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    jlong longField = env->GetLongField (obj, fieldId);

    BeJNI::CheckExceptions (threadEnv);

    return longField;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline jfloat BeJNI::GetFloatField (BeJNIThreadEnv &threadEnv, jobject obj, jfieldID fieldId)
{
    BE_ASSERT (obj);

    BE_ASSERT (fieldId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    jfloat floatField = env->GetFloatField (obj, fieldId);

    BeJNI::CheckExceptions (threadEnv);

    return floatField;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline jdouble BeJNI::GetDoubleField (BeJNIThreadEnv &threadEnv, jobject obj, jfieldID fieldId)
{
    BE_ASSERT (obj);

    BE_ASSERT (fieldId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    jdouble doubleField = env->GetDoubleField (obj, fieldId);

    BeJNI::CheckExceptions (threadEnv);

    return doubleField;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline jobject BeJNI::GetStaticObjectField (BeJNIThreadEnv &threadEnv, jclass clazz, jfieldID fieldId)
{
    BE_ASSERT (clazz);

    BE_ASSERT (fieldId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    jobject objectField = env->GetStaticObjectField (clazz, fieldId);

    if (objectField)
    {
        ++threadEnv.m_localAllocCount;
    }

    BeJNI::CheckExceptions (threadEnv);

    return objectField;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline jboolean BeJNI::GetStaticBooleanField (BeJNIThreadEnv &threadEnv, jclass clazz, jfieldID fieldId)
{
    BE_ASSERT (clazz);

    BE_ASSERT (fieldId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    jboolean booleanField = env->GetStaticBooleanField (clazz, fieldId);

    BeJNI::CheckExceptions (threadEnv);

    return booleanField;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline jbyte BeJNI::GetStaticByteField (BeJNIThreadEnv &threadEnv, jclass clazz, jfieldID fieldId)
{
    BE_ASSERT (clazz);

    BE_ASSERT (fieldId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    jbyte byteField = env->GetStaticByteField (clazz, fieldId);

    BeJNI::CheckExceptions (threadEnv);

    return byteField;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline jchar BeJNI::GetStaticCharField (BeJNIThreadEnv &threadEnv, jclass clazz, jfieldID fieldId)
{
    BE_ASSERT (clazz);

    BE_ASSERT (fieldId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    jchar charField = env->GetStaticCharField (clazz, fieldId);

    BeJNI::CheckExceptions (threadEnv);

    return charField;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline jshort BeJNI::GetStaticShortField (BeJNIThreadEnv &threadEnv, jclass clazz, jfieldID fieldId)
{
    BE_ASSERT (clazz);

    BE_ASSERT (fieldId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    jshort shortField = env->GetStaticShortField (clazz, fieldId);

    BeJNI::CheckExceptions (threadEnv);

    return shortField;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline jint BeJNI::GetStaticIntField (BeJNIThreadEnv &threadEnv, jclass clazz, jfieldID fieldId)
{
    BE_ASSERT (clazz);

    BE_ASSERT (fieldId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    jint intField = env->GetStaticIntField (clazz, fieldId);

    BeJNI::CheckExceptions (threadEnv);

    return intField;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline jlong BeJNI::GetStaticLongField (BeJNIThreadEnv &threadEnv, jclass clazz, jfieldID fieldId)
{
    BE_ASSERT (clazz);

    BE_ASSERT (fieldId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    jlong longField = env->GetStaticLongField (clazz, fieldId);

    BeJNI::CheckExceptions (threadEnv);

    return longField;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline jfloat BeJNI::GetStaticFloatField (BeJNIThreadEnv &threadEnv, jclass clazz, jfieldID fieldId)
{
    BE_ASSERT (clazz);

    BE_ASSERT (fieldId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    jfloat floatField = env->GetStaticFloatField (clazz, fieldId);

    BeJNI::CheckExceptions (threadEnv);

    return floatField;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline jdouble BeJNI::GetStaticDoubleField (BeJNIThreadEnv &threadEnv, jclass clazz, jfieldID fieldId)
{
    BE_ASSERT (clazz);

    BE_ASSERT (fieldId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    jdouble doubleField = env->GetStaticDoubleField (clazz, fieldId);

    BeJNI::CheckExceptions (threadEnv);

    return doubleField;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline void BeJNI::SetObjectField (BeJNIThreadEnv &threadEnv, jobject obj, jfieldID fieldId, jobject value)
{
    BE_ASSERT (obj);

    BE_ASSERT (fieldId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    env->SetObjectField (obj, fieldId, value);

    BeJNI::CheckExceptions (threadEnv);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline void BeJNI::SetBooleanField (BeJNIThreadEnv &threadEnv, jobject obj, jfieldID fieldId, jboolean value)
{
    BE_ASSERT (obj);

    BE_ASSERT (fieldId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    env->SetBooleanField (obj, fieldId, value);

    BeJNI::CheckExceptions (threadEnv);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline void BeJNI::SetByteField (BeJNIThreadEnv &threadEnv, jobject obj, jfieldID fieldId, jbyte value)
{
    BE_ASSERT (obj);

    BE_ASSERT (fieldId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    env->SetByteField (obj, fieldId, value);

    BeJNI::CheckExceptions (threadEnv);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline void BeJNI::SetCharField (BeJNIThreadEnv &threadEnv, jobject obj, jfieldID fieldId, jchar value)
{
    BE_ASSERT (obj);

    BE_ASSERT (fieldId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    env->SetCharField (obj, fieldId, value);

    BeJNI::CheckExceptions (threadEnv);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline void BeJNI::SetShortField (BeJNIThreadEnv &threadEnv, jobject obj, jfieldID fieldId, jshort value)
{
    BE_ASSERT (obj);

    BE_ASSERT (fieldId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    env->SetShortField (obj, fieldId, value);

    BeJNI::CheckExceptions (threadEnv);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline void BeJNI::SetIntField (BeJNIThreadEnv &threadEnv, jobject obj, jfieldID fieldId, jint value)
{
    BE_ASSERT (obj);

    BE_ASSERT (fieldId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    env->SetIntField (obj, fieldId, value);

    BeJNI::CheckExceptions (threadEnv);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline void BeJNI::SetLongField (BeJNIThreadEnv &threadEnv, jobject obj, jfieldID fieldId, jlong value)
{
    BE_ASSERT (obj);

    BE_ASSERT (fieldId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    env->SetLongField (obj, fieldId, value);

    BeJNI::CheckExceptions (threadEnv);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline void BeJNI::SetFloatField (BeJNIThreadEnv &threadEnv, jobject obj, jfieldID fieldId, jfloat value)
{
    BE_ASSERT (obj);

    BE_ASSERT (fieldId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    env->SetFloatField (obj, fieldId, value);

    BeJNI::CheckExceptions (threadEnv);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline void BeJNI::SetDoubleField (BeJNIThreadEnv &threadEnv, jobject obj, jfieldID fieldId, jdouble value)
{
    BE_ASSERT (obj);

    BE_ASSERT (fieldId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    env->SetDoubleField (obj, fieldId, value);

    BeJNI::CheckExceptions (threadEnv);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline void BeJNI::SetStaticObjectField (BeJNIThreadEnv &threadEnv, jclass clazz, jfieldID fieldId, jobject value)
{
    BE_ASSERT (clazz);

    BE_ASSERT (fieldId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    env->SetStaticObjectField (clazz, fieldId, value);

    BeJNI::CheckExceptions (threadEnv);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline void BeJNI::SetStaticBooleanField (BeJNIThreadEnv &threadEnv, jclass clazz, jfieldID fieldId, jboolean value)
{
    BE_ASSERT (clazz);

    BE_ASSERT (fieldId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    env->SetStaticBooleanField (clazz, fieldId, value);

    BeJNI::CheckExceptions (threadEnv);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline void BeJNI::SetStaticByteField (BeJNIThreadEnv &threadEnv, jclass clazz, jfieldID fieldId, jbyte value)
{
    BE_ASSERT (clazz);

    BE_ASSERT (fieldId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    env->SetStaticByteField (clazz, fieldId, value);

    BeJNI::CheckExceptions (threadEnv);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline void BeJNI::SetStaticCharField (BeJNIThreadEnv &threadEnv, jclass clazz, jfieldID fieldId, jchar value)
{
    BE_ASSERT (clazz);

    BE_ASSERT (fieldId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    env->SetStaticCharField (clazz, fieldId, value);

    BeJNI::CheckExceptions (threadEnv);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline void BeJNI::SetStaticShortField (BeJNIThreadEnv &threadEnv, jclass clazz, jfieldID fieldId, jshort value)
{
    BE_ASSERT (clazz);

    BE_ASSERT (fieldId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    env->SetStaticShortField (clazz, fieldId, value);

    BeJNI::CheckExceptions (threadEnv);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline void BeJNI::SetStaticIntField (BeJNIThreadEnv &threadEnv, jclass clazz, jfieldID fieldId, jint value)
{
    BE_ASSERT (clazz);

    BE_ASSERT (fieldId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    env->SetStaticIntField (clazz, fieldId, value);

    BeJNI::CheckExceptions (threadEnv);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline void BeJNI::SetStaticLongField (BeJNIThreadEnv &threadEnv, jclass clazz, jfieldID fieldId, jlong value)
{
    BE_ASSERT (clazz);

    BE_ASSERT (fieldId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    env->SetStaticLongField (clazz, fieldId, value);

    BeJNI::CheckExceptions (threadEnv);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline void BeJNI::SetStaticFloatField (BeJNIThreadEnv &threadEnv, jclass clazz, jfieldID fieldId, jfloat value)
{
    BE_ASSERT (clazz);

    BE_ASSERT (fieldId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    env->SetStaticFloatField (clazz, fieldId, value);

    BeJNI::CheckExceptions (threadEnv);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

inline void BeJNI::SetStaticDoubleField (BeJNIThreadEnv &threadEnv, jclass clazz, jfieldID fieldId, jdouble value)
{
    BE_ASSERT (clazz);

    BE_ASSERT (fieldId);

    BeJNI::CheckExceptions (threadEnv);

    JNIEnv *env = threadEnv.GetEnv ();

    env->SetStaticDoubleField (clazz, fieldId, value);

    BeJNI::CheckExceptions (threadEnv);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#endif // BE_JNI_INL
