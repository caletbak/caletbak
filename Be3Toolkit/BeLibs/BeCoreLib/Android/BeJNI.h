/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeJNI.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_JNI_H
#define BE_JNI_H

#include <BeCoreLib/Pch/BeCoreLibPredef.h>

#include <STL/BeString.h>

#include <jni.h>

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

extern "C" JNIEXPORT jint JNI_OnLoad (JavaVM* vm, BeVoidP reserved);

extern "C" JNIEXPORT void JNI_OnUnload(JavaVM* vm, BeVoidP reserved);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class BeJNIThreadEnv
{
public:

    BeJNIThreadEnv();

    ~BeJNIThreadEnv();

    JNIEnv* GetEnv () const { return m_threadEnv; }

protected:

    friend class BeJNI;

    void Attach ();

    void Detach ();

    JNIEnv* m_threadEnv;

    BeBoolean m_didAttach;

    unsigned int m_localAllocCount;

};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class BeJNI
{
public:

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // COMMON JNI STUFF
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    static JavaVM *GetVM ();

    static void SetVM (JavaVM *vm);

    static jclass FindClass(BeJNIThreadEnv &threadEnv, const char *classId);

    static jclass FindClass(BeJNIThreadEnv &threadEnv, jobject classLoader, const char *classId);

    static jclass GetObjectClass(BeJNIThreadEnv &threadEnv, jobject obj);

    static jobject GetObjectClassLoader(BeJNIThreadEnv &threadEnv, jobject obj);

    static void CheckExceptions(BeJNIThreadEnv &threadEnv);

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // JNI OBJECTS ALLOCATORS
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    static jobject AllocObject(BeJNIThreadEnv &threadEnv, jclass clazz);

    static jobject NewObject(BeJNIThreadEnv &threadEnv, jclass clazz, jmethodID constructorMethod, ...);

    static jobjectArray NewObjectArray(BeJNIThreadEnv &threadEnv, jsize length, jclass elementClass, jobject initialElement);

    static jobject GetObjectArrayElement(BeJNIThreadEnv &threadEnv, jobjectArray array, jsize index);

    static void SetObjectArrayElement(BeJNIThreadEnv &threadEnv, jobjectArray array, jsize index, jobject value);

    static jstring NewString(BeJNIThreadEnv &threadEnv, const char *source);

    static jstring NewString(BeJNIThreadEnv &threadEnv, const BeString8& source);

    static jstring NewWString(BeJNIThreadEnv &threadEnv, const BeString16& source);

    static BeString8 GetString(BeJNIThreadEnv &threadEnv, jstring jniString);

    static BeString16 GetWString(BeJNIThreadEnv &threadEnv, jstring jniString);

    static jbyteArray NewByteArray(BeJNIThreadEnv &threadEnv, char* source, unsigned int size);

    static char * GetByteArray(BeJNIThreadEnv &threadEnv, jbyteArray source);

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // JNI OBJECTS REFERENCES CREATORS AND DESTROYERS
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    static jobject NewLocalRef(BeJNIThreadEnv &threadEnv, jobject obj);

    static jobject NewGlobalRef(BeJNIThreadEnv &threadEnv, jobject obj);

    static jobject NewWeakGlobalRef(BeJNIThreadEnv &threadEnv, jobject obj);

    static jobject LocalToGlobalRef(BeJNIThreadEnv &threadEnv, jobject obj);

    static jobject LocalToWeakGlobalRef(BeJNIThreadEnv &threadEnv, jobject obj);

    static void DeleteLocalRef(BeJNIThreadEnv &threadEnv, jobject obj);

    static void DeleteGlobalRef(BeJNIThreadEnv &threadEnv, jobject obj);

    static void DeleteWeakGlobalRef(BeJNIThreadEnv &threadEnv, jobject obj);

    static jboolean IsSameObject(BeJNIThreadEnv &threadEnv, jobject ref1, jobject ref2);

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // COMMON JNI METHOD FUNCTIONS
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    static jmethodID GetConstructor(BeJNIThreadEnv &threadEnv, jclass clazz, const char *constructorSignature);

    static jmethodID GetMethodID(BeJNIThreadEnv &threadEnv, jclass clazz, const char* methodId, const char *methodSignature);

    static jmethodID GetStaticMethodID(BeJNIThreadEnv &threadEnv, jclass clazz, const char* methodId, const char *methodSignature);

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // NON-STATIC FUNCTION CALLERS
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    static void CallVoidMethod(BeJNIThreadEnv &threadEnv, jobject obj, jmethodID methodId, ...);

    static jobject CallObjectMethod(BeJNIThreadEnv &threadEnv, jobject obj, jmethodID methodId, ...);

    static jbyte CallByteMethod(BeJNIThreadEnv &threadEnv, jobject obj, jmethodID methodId, ...);

    static jchar CallCharMethod(BeJNIThreadEnv &threadEnv, jobject obj, jmethodID methodId, ...);

    static jshort CallShortMethod(BeJNIThreadEnv &threadEnv, jobject obj, jmethodID methodId, ...);

    static jint CallIntMethod(BeJNIThreadEnv &threadEnv, jobject obj, jmethodID methodId, ...);

    static jlong CallLongMethod(BeJNIThreadEnv &threadEnv, jobject obj, jmethodID methodId, ...);

    static jfloat CallFloatMethod(BeJNIThreadEnv &threadEnv, jobject obj, jmethodID methodId, ...);

    static jdouble CallDoubleMethod(BeJNIThreadEnv &threadEnv, jobject obj, jmethodID methodId, ...);

    static jboolean CallBooleanMethod(BeJNIThreadEnv &threadEnv, jobject obj, jmethodID methodId, ...);

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // STATIC FUNCTIONS CALLERS
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    static void CallStaticVoidMethod(BeJNIThreadEnv &threadEnv, jclass clazz, jmethodID methodId, ...);

    static jobject CallStaticObjectMethod(BeJNIThreadEnv &threadEnv, jclass clazz, jmethodID methodId, ...);

    static jbyte CallStaticByteMethod(BeJNIThreadEnv &threadEnv, jclass clazz, jmethodID methodId, ...);

    static jchar CallStaticCharMethod(BeJNIThreadEnv &threadEnv, jclass clazz, jmethodID methodId, ...);

    static jshort CallStaticShortMethod(BeJNIThreadEnv &threadEnv, jclass clazz, jmethodID methodId, ...);

    static jint CallStaticIntMethod(BeJNIThreadEnv &threadEnv, jclass clazz, jmethodID methodId, ...);

    static jlong CallStaticLongMethod(BeJNIThreadEnv &threadEnv, jclass clazz, jmethodID methodId, ...);

    static jfloat CallStaticFloatMethod(BeJNIThreadEnv &threadEnv, jclass clazz, jmethodID methodId, ...);

    static jdouble CallStaticDoubleMethod(BeJNIThreadEnv &threadEnv,jclass clazz, jmethodID methodId, ...);

    static jboolean CallStaticBooleanMethod(BeJNIThreadEnv &threadEnv, jclass clazz, jmethodID methodId, ...);

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // NON-STATIC GETTERS

    static jfieldID GetFieldID(BeJNIThreadEnv &threadEnv, jclass clazz, const char* fieldId, const char* fieldSignature);

    static jfieldID GetStaticFieldID(BeJNIThreadEnv &threadEnv, jclass clazz, const char* fieldId, const char* fieldSignature);

    static jobject GetObjectField(BeJNIThreadEnv &threadEnv, jobject obj, jfieldID fieldId);

    static jbyte GetByteField(BeJNIThreadEnv &threadEnv, jobject obj, jfieldID fieldId);

    static jchar GetCharField(BeJNIThreadEnv &threadEnv, jobject obj, jfieldID fieldId);

    static jshort GetShortField(BeJNIThreadEnv &threadEnv, jobject obj, jfieldID fieldId);

    static jint GetIntField(BeJNIThreadEnv &threadEnv, jobject obj, jfieldID fieldId);

    static jlong GetLongField(BeJNIThreadEnv &threadEnv, jobject obj, jfieldID fieldId);

    static jfloat GetFloatField(BeJNIThreadEnv &threadEnv, jobject obj, jfieldID fieldId);

    static jdouble GetDoubleField(BeJNIThreadEnv &threadEnv, jobject obj, jfieldID fieldId);

    static jboolean GetBooleanField(BeJNIThreadEnv &threadEnv, jobject obj, jfieldID fieldId);

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // STATIC GETTERS
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    static jobject GetStaticObjectField(BeJNIThreadEnv &threadEnv, jclass clazz, jfieldID fieldId);

    static jbyte GetStaticByteField(BeJNIThreadEnv &threadEnv, jclass clazz, jfieldID fieldId);

    static jchar GetStaticCharField(BeJNIThreadEnv &threadEnv, jclass clazz, jfieldID fieldId);

    static jshort GetStaticShortField(BeJNIThreadEnv &threadEnv, jclass clazz, jfieldID fieldId);

    static jint GetStaticIntField(BeJNIThreadEnv &threadEnv, jclass clazz, jfieldID fieldId);

    static jlong GetStaticLongField(BeJNIThreadEnv &threadEnv, jclass clazz, jfieldID fieldId);

    static jfloat GetStaticFloatField(BeJNIThreadEnv &threadEnv, jclass clazz, jfieldID fieldId);

    static jdouble GetStaticDoubleField(BeJNIThreadEnv &threadEnv, jclass clazz, jfieldID fieldId);

    static jboolean GetStaticBooleanField(BeJNIThreadEnv &threadEnv, jclass clazz, jfieldID fieldId);

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // NON-STATIC SETTERS
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    static void SetObjectField(BeJNIThreadEnv &threadEnv, jobject obj, jfieldID fieldId, jobject value);

    static void SetByteField(BeJNIThreadEnv &threadEnv, jobject obj, jfieldID fieldId, jbyte value);

    static void SetCharField(BeJNIThreadEnv &threadEnv, jobject obj, jfieldID fieldId, jchar value);

    static void SetShortField(BeJNIThreadEnv &threadEnv, jobject obj, jfieldID fieldId, jshort value);

    static void SetIntField(BeJNIThreadEnv &threadEnv, jobject obj, jfieldID fieldId, jint value);

    static void SetLongField(BeJNIThreadEnv &threadEnv, jobject obj, jfieldID fieldId, jlong value);

    static void SetFloatField(BeJNIThreadEnv &threadEnv, jobject obj, jfieldID fieldId, jfloat value);

    static void SetDoubleField(BeJNIThreadEnv &threadEnv, jobject obj, jfieldID fieldId, jdouble value);

    static void SetBooleanField(BeJNIThreadEnv &threadEnv, jobject obj, jfieldID fieldId, jboolean value);

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // STATIC SETTERS
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    static void SetStaticObjectField(BeJNIThreadEnv &threadEnv, jclass clazz, jfieldID fieldId, jobject value);

    static void SetStaticBooleanField(BeJNIThreadEnv &threadEnv, jclass clazz, jfieldID fieldId, jboolean value);

    static void SetStaticByteField(BeJNIThreadEnv &threadEnv, jclass clazz, jfieldID fieldId, jbyte value);

    static void SetStaticCharField(BeJNIThreadEnv &threadEnv, jclass clazz, jfieldID fieldId, jchar value);

    static void SetStaticShortField(BeJNIThreadEnv &threadEnv, jclass clazz, jfieldID fieldId, jshort value);

    static void SetStaticIntField(BeJNIThreadEnv &threadEnv, jclass clazz, jfieldID fieldId, jint value);

    static void SetStaticLongField(BeJNIThreadEnv &threadEnv, jclass clazz, jfieldID fieldId, jlong value);

    static void SetStaticFloatField(BeJNIThreadEnv &threadEnv, jclass clazz, jfieldID fieldId, jfloat value);

    static void SetStaticDoubleField(BeJNIThreadEnv &threadEnv, jclass clazz, jfieldID fieldId, jdouble value);

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

protected:

    static JavaVM *s_javaVM;

public:

    static JavaVMAttachArgs s_kAttachArgs;

};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <BeJNI.inl>

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#endif // BE_JNI_H
