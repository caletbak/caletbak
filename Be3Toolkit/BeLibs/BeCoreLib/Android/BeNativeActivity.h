/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeNativeActivity.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_NATIVEACTIVITY_H
#define BE_NATIVEACTIVITY_H

#include <BeCoreLib/Pch/BeCoreLibPredef.h>

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class BeNativeActivity
{
public:

    enum StatusFlags
    {
        NATIVE_ACTIVITY_FLAG_IS_STARTED = (1 << 0),
        NATIVE_ACTIVITY_FLAG_IS_PAUSED = (1 << 1),
        NATIVE_ACTIVITY_FLAG_HAS_FOCUS = (1 << 2),
        NATIVE_ACTIVITY_FLAG_HAS_WINDOW = (1 << 3),
        NATIVE_ACTIVITY_FLAG_DESTROY_REQUESTED = (1 << 4),
        NATIVE_ACTIVITY_FLAG_CONFIG_CHANGED = (1 << 5),
        NATIVE_ACTIVITY_FLAG_LOW_MEMORY = (1 << 6)
    };

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    class RegisteredCallbacks
    {
    public:
        void(*activityOnStart) (struct android_app *androidApp);
        void(*activityOnStop) (struct android_app *androidApp);
        void(*activityOnPause) (struct android_app *androidApp);
        void(*activityOnResume) (struct android_app *androidApp);
        void(*activityOnUpdate) (struct android_app *androidApp);
        void(*activityOnDestroy) (struct android_app *androidApp);
        void(*activityOnInitView) (struct android_app *androidApp);
        void(*activityOnReleaseView) (struct android_app *androidApp);
        void(*activityOnGainFocus) (struct android_app *androidApp);
        void(*activityOnLoseFocus) (struct android_app *androidApp);
        void(*activityOnConfigChanged) (struct android_app *androidApp);
        void(*activityOnLowMemoryWarning) (struct android_app *androidApp);
        int(*activityOnInputEvent) (struct android_app *androidApp, BeVoidP inputEvent);
    };

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    static void Initialise(struct android_app *androidApp, const RegisteredCallbacks &registeredCallbacks);

    static void Deinitialise();

    static unsigned int Update();

    static void RequestTermination();

    static void EnterCriticalSection();

    static void ExitCriticalSection();

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

protected:

    friend void android_main(struct android_app *androidApp);

    static void PollOSLooper(const int timeoutMilliseconds);

    static void ProcessActivityEvent(struct android_app *androidApp, int32_t cmd);

    static BeInt ProcessInputEvent(struct android_app* pApp, AInputEvent* pInputEv);

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    static bool s_initialised;

    static unsigned int s_statusFlags;

    static RegisteredCallbacks s_registeredCallbacks;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

public:

    static struct android_app *s_androidApp;

    static unsigned int s_destroyPreviousThread;

    static bool s_GLContextWasDestroyed;

    static bool s_stopUpdate;

};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BeNativeActivity_dummy();

extern "C" JNIEXPORT void Java_org_bakengine_BeNativeActivity_onNativeCreate (JNIEnv *env, jobject obj, jclass thisClass, jobject savedInstanceState);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#endif // BE_NATIVEACTIVITY_H
