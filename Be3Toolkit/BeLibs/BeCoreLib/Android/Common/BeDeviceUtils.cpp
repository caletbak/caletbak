/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeDeviceUtils.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <Common/BeDeviceUtils.h>

//#include <Utils/Localization.h>
#include <Common/BeAlertDialog.h>
//#include <Events/CommEvents.h>

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const BeInt BeDeviceUtils::GAME_REQUEST_FINISH_FLAG = 0x01;

BeInt BeDeviceUtils::s_iGameFinishRequested = 0;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

extern jobject g_pBeNativeActivityClassLoader;

extern jobject g_pBeNativeActivityObj;

static jclass
    g_pBeDeviceUtilsClass = 0;

static const char
    *g_pBeDeviceUtilsClassName = "org/bakengine/BeDeviceUtils";

static jobject
    g_pBeDeviceUtilsObject = 0;

static jmethodID
    g_pBeDeviceUtils_Constructor = 0,
    g_pBeDeviceUtils_Initialize = 0,
    g_pBeDeviceUtils_Deinitialize = 0,
    g_pBeDeviceUtils_SendMail = 0,
    g_pBeDeviceUtils_OpenExternalURL = 0,
    g_pBeDeviceUtils_SetDeviceFlag = 0,
    g_pBeDeviceUtils_GetDeviceFlag = 0,
    g_pBeDeviceUtils_GetUniqueID = 0,
    g_pBeDeviceUtils_GetMarketProvider = 0,
    g_pBeDeviceUtils_GetMarketName = 0,
    g_pBeDeviceUtils_GetPackageName = 0,
    g_pBeDeviceUtils_GetDeviceUserLanguage = 0,
    g_pBeDeviceUtils_GetExternalStorageDir = 0,
    g_pBeDeviceUtils_DeleteExternalFile = 0,
    g_pBeDeviceUtils_OpenMarketForRating = 0,
    g_pBeDeviceUtils_HasTelephonyFeatures = 0,
    g_pBeDeviceUtils_ShowWifiSettings = 0,
    g_pBeDeviceUtils_SendFinishSignal = 0;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
*	This function intializes the variables to call the function in java.
*/
JNIEXPORT void Java_org_bakengine_BeDeviceUtils_onNativeInit(JNIEnv *env, jobject obj, jclass thisClass)
{
    // 
    // Cache class method references.
    // 
    BeJNIThreadEnv jniThread;

    g_pBeDeviceUtils_Constructor = BeJNI::GetMethodID(jniThread, thisClass, "<init>", "()V");

    g_pBeDeviceUtils_Initialize = BeJNI::GetMethodID(jniThread, thisClass, "Initialize", "()V");

    g_pBeDeviceUtils_Deinitialize = BeJNI::GetMethodID(jniThread, thisClass, "Deinitialize", "()V");

    g_pBeDeviceUtils_SendMail = BeJNI::GetMethodID(jniThread, thisClass, "SendMail", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V");

    g_pBeDeviceUtils_OpenExternalURL = BeJNI::GetMethodID(jniThread, thisClass, "openExternalURL", "(Ljava/lang/String;)V");

    g_pBeDeviceUtils_SetDeviceFlag = BeJNI::GetMethodID(jniThread, thisClass, "SetDeviceFlag", "(Ljava/lang/String;Ljava/lang/String;)V");

    g_pBeDeviceUtils_GetDeviceFlag = BeJNI::GetMethodID(jniThread, thisClass, "GetDeviceFlag", "(Ljava/lang/String;)Ljava/lang/String;");
    
    g_pBeDeviceUtils_GetUniqueID = BeJNI::GetMethodID(jniThread, thisClass, "GetUniqueID", "()Ljava/lang/String;");

    g_pBeDeviceUtils_GetMarketProvider = BeJNI::GetMethodID(jniThread, thisClass, "GetMarketProvider", "()Ljava/lang/String;");

    g_pBeDeviceUtils_GetMarketName = BeJNI::GetMethodID(jniThread, thisClass, "GetMarketName", "()Ljava/lang/String;");

    g_pBeDeviceUtils_GetPackageName = BeJNI::GetStaticMethodID(jniThread, thisClass, "GetPackageName", "()Ljava/lang/String;");

    g_pBeDeviceUtils_GetExternalStorageDir = BeJNI::GetStaticMethodID(jniThread, thisClass, "GetExternalStorageDir", "()Ljava/lang/String;");

    g_pBeDeviceUtils_GetDeviceUserLanguage = BeJNI::GetStaticMethodID(jniThread, thisClass, "GetDeviceUserLanguage", "()Ljava/lang/String;");

    g_pBeDeviceUtils_DeleteExternalFile = BeJNI::GetStaticMethodID(jniThread, thisClass, "DeleteExternalFile", "(Ljava/lang/String;)V");

    g_pBeDeviceUtils_HasTelephonyFeatures = BeJNI::GetMethodID(jniThread, thisClass, "HasTelephonyFeatures", "()Z");

    g_pBeDeviceUtils_ShowWifiSettings = BeJNI::GetMethodID(jniThread, thisClass, "ShowWifiSettings", "()V");

    g_pBeDeviceUtils_OpenMarketForRating = BeJNI::GetMethodID(jniThread, thisClass, "openMarketForRating", "()V");

    g_pBeDeviceUtils_SendFinishSignal = BeJNI::GetMethodID(jniThread, thisClass, "SendFinishSignal", "()V");

    BeJNI::CheckExceptions(jniThread);
}

void BeDeviceUtils::Initialize()
{
    BE_CORE_DEBUG("Initialize library DeviceUtils");

    BeJNIThreadEnv jniThread;

    g_pBeDeviceUtilsClass = BeJNI::FindClass(jniThread, g_pBeNativeActivityClassLoader, g_pBeDeviceUtilsClassName);

    g_pBeDeviceUtilsClass = (jclass)BeJNI::LocalToGlobalRef(jniThread, g_pBeDeviceUtilsClass);

    // 
    // Create a new instance of this class.
    // 

    g_pBeDeviceUtilsObject = BeJNI::NewObject(jniThread, g_pBeDeviceUtilsClass, g_pBeDeviceUtils_Constructor);

    g_pBeDeviceUtilsObject = BeJNI::LocalToGlobalRef(jniThread, g_pBeDeviceUtilsObject);

    //Initializing

    BeJNI::CallVoidMethod(jniThread, g_pBeDeviceUtilsObject, g_pBeDeviceUtils_Initialize);

    BeJNI::CheckExceptions(jniThread);
}

void BeDeviceUtils::Deinitialise()
{
    BeJNIThreadEnv jniThread;

    BeJNI::CallVoidMethod(jniThread, g_pBeDeviceUtilsObject, g_pBeDeviceUtils_Deinitialize);

    BeJNI::DeleteLocalRef(jniThread, g_pBeDeviceUtilsObject);

    BeJNI::DeleteLocalRef(jniThread, g_pBeDeviceUtilsClass);

    g_pBeDeviceUtilsClass = 0;

    BeJNI::CheckExceptions(jniThread);
}

void BeDeviceUtils::ShowSpinner()
{
    BeAlertDialog::GetInstance()->Show(BeAlertDialog::DIALOG_SPINNER, L"", L"");
}

void BeDeviceUtils::HideSpinner()
{
    BeAlertDialog::GetInstance()->Hide();
}

/*
* This function sends a email to the recipient
*/
/*
void BeDeviceUtils::SendMail(const BeString16& subject, const BeString16& message, const BeString8& recipient, BeEmailAttachment* attachment, bcn::display::DisplayObject *eventReceiver)
{
    BeJNIThreadEnv jniThread;

    jstring str_Subject = BeJNI::NewWString(jniThread, subject);
    jstring	str_Message = BeJNI::NewWString(jniThread, message);
    jstring str_Recipient = BeJNI::NewString(jniThread, recipient);

    BeJNI::CallVoidMethod(jniThread, g_pBeDeviceUtilsObject, g_pBeDeviceUtils_SendMail, str_Subject, str_Message, str_Recipient);

    BeJNI::DeleteLocalRef(jniThread, str_Subject);
    BeJNI::DeleteLocalRef(jniThread, str_Recipient);
    BeJNI::DeleteLocalRef(jniThread, str_Message);
    
    // workaround for android
    // since android doesn't callback when a mail is successfully sent,
    // we will assume that the user will send the email if the UI is
    // successfully shown.
    bcn::events::CustomEvent ev(bcn::events::COMPOSE_MAIL);
    eventReceiver->dispatchCustomEvent(&ev);

    BeJNI::CheckExceptions(jniThread);
}
*/

void BeDeviceUtils::OpenMarketForRating()
{
    BeJNIThreadEnv jniThread;

    BeJNI::CallVoidMethod(jniThread, g_pBeDeviceUtilsObject, g_pBeDeviceUtils_OpenMarketForRating);

    BeJNI::CheckExceptions(jniThread);
}

void BeDeviceUtils::OpenExternalURL(const BeString8& urlString)
{
    BeJNIThreadEnv jniThread;
    
    jstring jURL = BeJNI::NewString(jniThread, urlString);
    
    BeJNI::CallVoidMethod(jniThread, g_pBeDeviceUtilsObject, g_pBeDeviceUtils_OpenExternalURL, jURL);
    
    BeJNI::DeleteLocalRef(jniThread, jURL);
    
    BeJNI::CheckExceptions(jniThread);
}

void BeDeviceUtils::SetDeviceFlag(const BeString8& key, const BeString8& value)
{
    BeJNIThreadEnv jniThread;

    jstring str_Key = BeJNI::NewString(jniThread, key);
    jstring	str_Value = BeJNI::NewString(jniThread, value);

    BeJNI::CallVoidMethod(jniThread, g_pBeDeviceUtilsObject, g_pBeDeviceUtils_SetDeviceFlag, str_Key, str_Value);

    BeJNI::DeleteLocalRef(jniThread, str_Key);
    BeJNI::DeleteLocalRef(jniThread, str_Value);

    BeJNI::CheckExceptions(jniThread);
}

BeString8 BeDeviceUtils::GetDeviceFlag(const BeString8& key)
{
    BeJNIThreadEnv jniThread;

    jstring str_Key = BeJNI::NewString(jniThread, key);
    jstring response = (jstring)BeJNI::CallObjectMethod(jniThread, g_pBeDeviceUtilsObject, g_pBeDeviceUtils_GetDeviceFlag, str_Key);
    BeString8 nativeResponse = BeJNI::GetString(jniThread, response);

    BeJNI::DeleteLocalRef(jniThread, str_Key);
    BeJNI::DeleteLocalRef(jniThread, response);

    BeJNI::CheckExceptions(jniThread);

    return nativeResponse;
}

/*
void BeDeviceUtils::ScheduleNotification(const BeString8& sku, const Json::Value& info, int timeLeft)
{
}

void BeDeviceUtils::ScheduleNotification(const BeString8& sku, const BeString16& body, const BeString16& action, const Json::Value &info, int timeLeft)
{
}
*/

void BeDeviceUtils::CancelNotification(const BeString8& sku)
{
}

BeString8 BeDeviceUtils::GetUniqueID()
{
    BeJNIThreadEnv jniThread;
    
    jstring response = (jstring)BeJNI::CallObjectMethod(jniThread, g_pBeDeviceUtilsObject, g_pBeDeviceUtils_GetUniqueID);

    BeString8 nativeResponse = BeJNI::GetString(jniThread, response);

    BeJNI::DeleteLocalRef(jniThread, response);

    BeJNI::CheckExceptions(jniThread);

    return nativeResponse;
}

BeString8 BeDeviceUtils::GetMarketProvider()
{
    BeJNIThreadEnv jniThread;

    jstring response = (jstring)BeJNI::CallObjectMethod(jniThread, g_pBeDeviceUtilsObject, g_pBeDeviceUtils_GetMarketProvider);

    BeString8 nativeResponse = BeJNI::GetString(jniThread, response);

    BeJNI::DeleteLocalRef(jniThread, response);

    BeJNI::CheckExceptions(jniThread);

    return nativeResponse;
}

BeString8 BeDeviceUtils::GetMarketName()
{
    BeJNIThreadEnv jniThread;

    jstring response = (jstring)BeJNI::CallObjectMethod(jniThread, g_pBeDeviceUtilsObject, g_pBeDeviceUtils_GetMarketName);

    BeString8 nativeResponse = BeJNI::GetString(jniThread, response);

    BeJNI::DeleteLocalRef(jniThread, response);

    BeJNI::CheckExceptions(jniThread);

    return nativeResponse;
}

BeString8 BeDeviceUtils::GetPackageName()
{
    BeJNIThreadEnv jniThread;

    jstring response = (jstring)BeJNI::CallStaticObjectMethod(jniThread, g_pBeDeviceUtilsClass, g_pBeDeviceUtils_GetPackageName);

    BeString8 nativeResponse = BeJNI::GetString(jniThread, response);

    BeJNI::DeleteLocalRef(jniThread, response);

    BeJNI::CheckExceptions(jniThread);

    return nativeResponse;
}

BeString8 BeDeviceUtils::GetExternalStorageDir()
{
    BeJNIThreadEnv jniThread;

    jstring response = (jstring)BeJNI::CallStaticObjectMethod(jniThread, g_pBeDeviceUtilsClass, g_pBeDeviceUtils_GetExternalStorageDir);

    BeString8 nativeResponse = BeJNI::GetString(jniThread, response);

    BeJNI::DeleteLocalRef(jniThread, response);

    BeJNI::CheckExceptions(jniThread);

    return nativeResponse;
}

BeString8 BeDeviceUtils::GetDeviceUserLanguage()
{
    BeJNIThreadEnv jniThread;

    jstring response = (jstring)BeJNI::CallStaticObjectMethod(jniThread, g_pBeDeviceUtilsClass, g_pBeDeviceUtils_GetDeviceUserLanguage);

    BeString8 nativeResponse = BeJNI::GetString(jniThread, response);

    BeJNI::DeleteLocalRef(jniThread, response);

    BeJNI::CheckExceptions(jniThread);

    return nativeResponse;
}

void BeDeviceUtils::DeleteExternalFile(const BeString8& filename)
{
    BeJNIThreadEnv jniThread;

    jstring str_filename = BeJNI::NewString(jniThread, filename);

    BeJNI::CallStaticVoidMethod(jniThread, g_pBeDeviceUtilsClass, g_pBeDeviceUtils_DeleteExternalFile, str_filename);

    BeJNI::DeleteLocalRef(jniThread, str_filename);

    BeJNI::CheckExceptions(jniThread);
}

BeBoolean BeDeviceUtils::HasTelephonyFeatures()
{
    BeJNIThreadEnv jniThread;

    BeBoolean response = BeJNI::CallBooleanMethod(jniThread, g_pBeDeviceUtilsObject, g_pBeDeviceUtils_HasTelephonyFeatures);

    BeJNI::CheckExceptions(jniThread);

    return response;
}

void BeDeviceUtils::ShowWifiSettings()
{
    BeJNIThreadEnv jniThread;

    BeJNI::CallVoidMethod(jniThread, g_pBeDeviceUtilsObject, g_pBeDeviceUtils_ShowWifiSettings);

    BeJNI::CheckExceptions(jniThread);
}

void BeDeviceUtils::SendFinishSignal()
{
    BeJNIThreadEnv jniThread;

    BeJNI::CallVoidMethod(jniThread, g_pBeDeviceUtilsObject, g_pBeDeviceUtils_SendFinishSignal);

    BeJNI::CheckExceptions(jniThread);
}

