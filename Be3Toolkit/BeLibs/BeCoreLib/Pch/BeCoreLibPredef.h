/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeCoreLibPredef.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_CORELIBPREDEF_H
#define BE_CORELIBPREDEF_H

// Global BE3 include
#include <BeCommon/BakEngine3Common.h>

#if TARGET_WINDOWS
#ifdef CORE_IMPORTS
#  define CORE_API __declspec(dllimport)
#  pragma message("automatic link to BeCoreLib.lib")
#  pragma comment(lib, "BeCoreLib.lib")
#else
#  define CORE_API __declspec(dllexport)
#endif
#else
#  define CORE_API
#endif

#ifdef _DEBUG
#  define BE_CORE_DEBUG(x, ...) _OutputDebugString("[  CORE DEBUG]: ", true, x, ##__VA_ARGS__)
#  define BE_CORE_WARNING(x, ...) _OutputDebugString("[! CORE WARNING]: ", true, x, ##__VA_ARGS__)
#  define BE_CORE_ERROR(x, ...) _OutputDebugString("[* CORE ERROR]: ", true, x, ##__VA_ARGS__)
#  define BE_CORE_PRINT(x, ...) _OutputDebugString("", false, x, ##__VA_ARGS__)
#else
#  define BE_CORE_DEBUG(x, ...) x
#  define BE_CORE_WARNING(x, ...) x
#  define BE_CORE_ERROR(x, ...) x
#  define BE_CORE_PRINT(x, ...) x
#endif

#if defined(AS400) || defined(OS400)
  typedef pthread_id_np_t ThreadID;
#elif defined(VMS) 
  typedef pthread_t ThreadID;
#else
# ifdef USE_BEGIN_THREAD
  typedef BeUInt ThreadID;
# else
  typedef BeULong ThreadID;
# endif
#endif

#if TARGET_IOS
#ifdef __OBJC__
#import <UIKit/UIKit.h>
#import <objc/runtime.h>
#import <Foundation/Foundation.h>
#endif
#endif

#if TARGET_ANDROID
#include <jni.h>

#include <android/asset_manager.h>
#include <android/configuration.h>
#include <android/log.h>
#include <android/looper.h>
#include <android/input.h>
#include <android/native_activity.h>
#include <android/native_window.h>
#include <android/sensor.h>
#include <AndroidAppGlueLib/Pch/AndroidAppGlueLib.h>

#include <tr1/memory>
#include <vector>
#include <sstream>
#endif

#endif // #ifndef BE_CORELIBPREDEF_H

