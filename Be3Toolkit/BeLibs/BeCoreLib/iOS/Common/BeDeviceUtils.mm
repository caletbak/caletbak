
#include <Utils/DeviceUtilsInterface.h>
#include <Utils/StringUtils.h>
#include <Utils/DeviceUtils.h>

#include <Events/CommEvents.h>

#import <Foundation/Foundation.h>
#import <GameKit/GameKit.h>
#import <MessageUI/MessageUI.h>
#import <UIKit/UIKit.h>

using namespace bcn;


@interface CommunicationComposeViewDelegate: NSObject<MFMessageComposeViewControllerDelegate,MFMailComposeViewControllerDelegate>
{
@public
    display::DisplayObject *eventReceiver;
}
@end

@implementation CommunicationComposeViewDelegate
- (void) messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
		case MessageComposeResultCancelled:
        CORE_DEBUG("Cancelled");
        break;
        
		case MessageComposeResultFailed:
        /*UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"MyApp" message:@"Unknown Error"
         delegate:self cancelButtonTitle:@”OK” otherButtonTitles: nil];
         [alert show];
         [alert release];*/
        break;
        
		case MessageComposeResultSent: {
            bcn::events::CustomEvent ev(bcn::events::COMPOSE_SMS);
            eventReceiver->dispatchCustomEvent(&ev);
        } break;
        
		default:
        break;
	}
    
    UIViewController *iosViewController = (UIViewController*)DeviceUtilsInterface::viewController;
	[iosViewController dismissModalViewControllerAnimated:YES];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result) {
		case MFMailComposeResultCancelled:
        CORE_DEBUG("Cancelled");
        break;
        
		case MFMailComposeResultFailed:
        /*UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"MyApp" message:@"Unknown Error"
         delegate:self cancelButtonTitle:@”OK” otherButtonTitles: nil];
         [alert show];
         [alert release];*/
        break;
        
		case MFMailComposeResultSent: {
            bcn::events::CustomEvent ev(bcn::events::COMPOSE_MAIL);
            eventReceiver->dispatchCustomEvent(&ev);
        } break;
        
		default:
        break;
	}
    
    UIViewController *iosViewController = (UIViewController*)DeviceUtilsInterface::viewController;
	[iosViewController dismissModalViewControllerAnimated:YES];
}
@end

static CommunicationComposeViewDelegate *communicationDelegate = nil;

static void*
	s_spinner = NULL;

static void*
	s_spinnerBackground = NULL;

void* DeviceUtilsInterface::viewController = nil;

void DeviceUtilsInterface::DeviceUtils_Initialize()
{
    communicationDelegate = [[CommunicationComposeViewDelegate alloc] init];
}

void DeviceUtilsInterface::DeviceUtils_Deinitialise()
{
    [communicationDelegate release];
}

void DeviceUtilsInterface::DeviceUtils_ShowSpinner ()
{
    if (s_spinner == NULL)
    {
        UIActivityIndicatorView* activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        UIViewController *iosViewController = (UIViewController*)viewController;
        iosViewController.modalPresentationStyle = UIModalPresentationCurrentContext;
        activityIndicator.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
        CGPoint center = iosViewController.view.center;
        
        int iCenterX = center.x;
        int iCenterY = center.y;
        if (iCenterY > iCenterX)
        {
            iCenterY = center.x;
            iCenterX = center.y;
        }
        
        activityIndicator.center = CGPointMake(iCenterX, iCenterY);

        UIView* iosSpinnerBackground = (UIView*)s_spinnerBackground;
        if (iosSpinnerBackground)
            [iosSpinnerBackground addSubview:activityIndicator];
        else
            [iosViewController.view addSubview:activityIndicator];
        
        s_spinner = activityIndicator;
        
        [activityIndicator startAnimating];
    }
}

void DeviceUtilsInterface::DeviceUtils_HideSpinner ()
{
    UIActivityIndicatorView* iosSpinner = (UIActivityIndicatorView*)s_spinner;
    if (iosSpinner != nil)
    {
        [iosSpinner stopAnimating];
        [iosSpinner release];
        
        UIView* iosSpinnerBackground = (UIView*)s_spinnerBackground;
        if (iosSpinnerBackground) {
            [iosSpinnerBackground removeFromSuperview];
            [iosSpinnerBackground release];
        }
        
        s_spinnerBackground = nil;
        s_spinner = nil;
    }
}

/*
* This function sends a email to the recipient
*/
void DeviceUtilsInterface::DeviceUtils_SendMail(const std::wstring &subject, const std::wstring &message, const std::string &recipient, mailAttachment * attachment, display::DisplayObject *eventReceiver)
{
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *controller = [[[MFMailComposeViewController alloc] init] autorelease];
        // [ETC] TODO: Everything below this line is untested
        communicationDelegate->eventReceiver = eventReceiver;
        controller.mailComposeDelegate = communicationDelegate;
        
        std::vector<std::string> vRecipients = stringUtils::split(recipient, ",");
        if(vRecipients.size() > 0)
        {
            NSMutableArray *recipients = [NSMutableArray array];
            for (int i = 0; i < vRecipients.size(); i++)
            {
                [recipients addObject:[NSString stringWithUTF8String:vRecipients[i].c_str()]];
            }
            [controller setToRecipients:recipients];
        }
        
        [controller setSubject:[[NSString alloc] initWithBytes:subject.data() length:subject.size() * sizeof(wchar_t) encoding:NSUTF32LittleEndianStringEncoding]];
         
        [controller setMessageBody:[[NSString alloc] initWithBytes:message.data() length:message.size() * sizeof(wchar_t) encoding:NSUTF32LittleEndianStringEncoding] isHTML:true];
         
        if (attachment) {
            NSData* data = [NSData dataWithBytes:attachment->data length:attachment->data_len];
            [controller addAttachmentData:data mimeType:[NSString stringWithUTF8String:attachment->type.c_str()] fileName:[NSString stringWithUTF8String:attachment->filename.c_str()]];
        }
        
        UIViewController *iosViewController = (UIViewController*)viewController;
        [iosViewController presentModalViewController:controller animated:YES];
    }
}

void DeviceUtilsInterface::DeviceUtils_SetDeviceFlag(const std::string& key, const std::string& value)
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    NSString * nskey = [NSString stringWithUTF8String:key.c_str()];
    NSString * nsvalue = [NSString stringWithUTF8String:value.c_str()];
    [defaults setValue:nsvalue forKey:nskey];
    [defaults synchronize];
}

std::string DeviceUtilsInterface::DeviceUtils_GetDeviceFlag(const std::string& key)
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    NSString * value = [defaults valueForKey:[NSString stringWithUTF8String:key.c_str()]];
    if(value == nil){
        return "";
    }
    return value.UTF8String;
}

void DeviceUtilsInterface::DeviceUtils_OpenMarketForRating()
{

}

void DeviceUtilsInterface::DeviceUtils_OpenExternalURL(const std::string& urlString)
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithUTF8String:urlString.c_str()]]];
}

std::string DeviceUtilsInterface::DeviceUtils_GetUniqueID()
{
    // TODO
    return "";
}

std::string DeviceUtilsInterface::DeviceUtils_GetMarketProvider()
{
	return "TID_GEN_SYSTEM_CLOUDNAME_APPLE";
}

std::string DeviceUtilsInterface::DeviceUtils_GetMarketName()
{
	return "TID_GEN_SYSTEM_CLOUDNAME_APPLE";
}

std::string DeviceUtilsInterface::DeviceUtils_GetPackageName()
{
	return "";
}

std::string DeviceUtilsInterface::DeviceUtils_GetDeviceUserLanguage()
{
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    std::string strLocaleLanguage = [language UTF8String];
    
    if (strLocaleLanguage.compare("zh") == 0)
    {
        return "zh";
    }
    else if (strLocaleLanguage.compare("ko") == 0)
    {
        return "ko";
    }
    else if (strLocaleLanguage.compare("ja") == 0)
    {
        return "ja";
    }
    else if (strLocaleLanguage.compare("it") == 0)
    {
        return "it";
    }
    else if (strLocaleLanguage.compare("fr") == 0)
    {
        return "fr";
    }
    else if (strLocaleLanguage.compare("es") == 0 || strLocaleLanguage.compare("ca") == 0 || strLocaleLanguage.compare("eu") == 0)
    {
        return "es";
    }
    else if (strLocaleLanguage.compare("de") == 0)
    {
        return "de";
    }
    
    return "en";
}

std::string DeviceUtilsInterface::DeviceUtils_GetExternalStorageDir()
{
    return "";
}

void DeviceUtilsInterface::DeviceUtils_DeleteExternalFile(const std::string& filename)
{
}

bool DeviceUtilsInterface::HasTelephonyFeatures()
{
    // Not implemented
    return true;
}

void DeviceUtilsInterface::ShowWifiSettings()
{
}

void DeviceUtilsInterface::SendFinishSignal()
{
}

/*
- (NSArray*) allNotifications
{
    UIApplication* app = [UIApplication sharedApplication];
    return [NSArray arrayWithArray:app.scheduledLocalNotifications];
}

- (id) objectForNotification:(UILocalNotification*)localNotif
{
    return [NSDictionary dictionaryWithObjectsAndKeys:
            [localNotif.userInfo valueForKey:@"sku"], @"sku",
            localNotif.alertBody, @"alertBody",
            localNotif.alertAction, @"alertAction",
            localNotif.fireDate, @"fireDate",
            [localNotif.userInfo valueForKey:@"info"], @"info",
            nil];
}

- (void) hideNotifications
{
    if (isLocalNotificationsEnabled) {
        isLocalNotificationsEnabled = false;
        
        UIApplication* app = [UIApplication sharedApplication];
        
        [app cancelAllLocalNotifications];
    }
}

- (void) restoreNotifications
{
    if (!isLocalNotificationsEnabled) {
        isLocalNotificationsEnabled = true;
    }
}
 
*/

