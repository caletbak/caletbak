
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <Utils/UbiAlertDialog.h>
#include <Utils/StringUtils.h>

using namespace bcn;

@interface UbiAlertDialogViewHelper : NSObject<UIAlertViewDelegate>
{
@public
    
    int m_dialogType;
    bcn::UbiAlertDialog* parent;
}

- (void) CreateDialog:(int)dialogType title:(NSString*)title message:(NSString*)message buttonPositive:(NSString*)buttonPositive buttonNegative:(NSString*)buttonNegative buttonNeutral:(NSString*)buttonNeutral;

- (void) alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;

@end

@implementation UbiAlertDialogViewHelper

- (void) CreateDialog:(int)dialogType title:(NSString*)title message:(NSString*)message buttonPositive:(NSString*)buttonPositive buttonNegative:(NSString*)buttonNegative buttonNeutral:(NSString*)buttonNeutral
{
    m_dialogType = dialogType;
    
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:nil otherButtonTitles:nil, nil];
    
    if (dialogType == UbiAlertDialog::DIALOG_TEXT_ENTRY)
    {
        [alert setAlertViewStyle: UIAlertViewStylePlainTextInput];
    }
    
    if ([buttonPositive length] > 0)
    {
        [alert addButtonWithTitle:buttonPositive];
    }
    
    if ([buttonNegative length] > 0)
    {
        [alert addButtonWithTitle:buttonNegative];
    }
    
    if ([buttonNeutral length] > 0)
    {
        [alert addButtonWithTitle:buttonNeutral];
    }
    
    [alert show];
    
    [alert release];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    std::wstring textValue = L"";
    
    if (m_dialogType == UbiAlertDialog::DIALOG_TEXT_ENTRY)
    {
        UITextField* textFieldTemp = [alertView textFieldAtIndex:0];
        if (textFieldTemp && [textFieldTemp.text length] > 0)
        {
            NSString* textTemp = textFieldTemp.text;
            textValue = (wchar_t*) [textTemp cStringUsingEncoding:NSUTF32LittleEndianStringEncoding];
        }
    }
    
    parent->onPressedButton(buttonIndex, textValue);
    
    [self release];
}

@end

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void UbiAlertDialog::Initialise()
{

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void UbiAlertDialog::Deinitialise()
{

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void UbiAlertDialog::Show (const eUbiAlertDialogType dialogType, const std::wstring title, const std::wstring message, const std::wstring buttonPositive, const std::wstring buttonNegative, const std::wstring buttonNeutral, const std::wstring defaultEntryText, const bool buttonPositiveAvoidDismiss, const bool buttonNegativeAvoidDismiss, const bool buttonNeutralAvoidDismiss, const bool dialogDismissible)
{
    UbiAlertDialogViewHelper* alert = [[UbiAlertDialogViewHelper alloc] init];
    
    alert->parent = this;
    
    NSString* nsTitle = [[NSString alloc] initWithBytes:title.data() length:title.size() * sizeof(wchar_t) encoding:NSUTF32LittleEndianStringEncoding];
    NSString* nsMessage = [[NSString alloc] initWithBytes:message.data() length:message.size() * sizeof(wchar_t) encoding:NSUTF32LittleEndianStringEncoding];
    NSString* nsButtonPositive = [[NSString alloc] initWithBytes:buttonPositive.data() length:buttonPositive.size() * sizeof(wchar_t) encoding:NSUTF32LittleEndianStringEncoding];
    NSString* nsButtonNegative = [[NSString alloc] initWithBytes:buttonNegative.data() length:buttonNegative.size() * sizeof(wchar_t) encoding:NSUTF32LittleEndianStringEncoding];
    NSString* nsButtonNeutral = [[NSString alloc] initWithBytes:buttonNeutral.data() length:buttonNeutral.size() * sizeof(wchar_t) encoding:NSUTF32LittleEndianStringEncoding];
    
    [alert CreateDialog:dialogType title:nsTitle message:nsMessage buttonPositive:nsButtonPositive buttonNegative:nsButtonNegative buttonNeutral:nsButtonNeutral];

    s_isAlertDialogBeingShown = true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void UbiAlertDialog::onPressedButton(int buttonID, std::wstring textFieldValue)
{
	m_ePressedButton = (eUbiAlertDialogButton) buttonID;
    
	m_strWrittenText = textFieldValue;

	if (m_pCurrentDelegate)
	{
		m_pCurrentDelegate->onPressedButton (m_ePressedButton, m_strWrittenText);

		m_pCurrentDelegate = NULL;
	}

  s_isAlertDialogBeingShown = false;
}

void UbiAlertDialog::Hide()
{
    //TODO:
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
