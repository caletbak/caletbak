/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeMemoryPool.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <Memory/BeMemoryPool.h>

/////////////////////////////////////////////////////////////////////////////
BeInt BeMemoryPool::m_iBlockSizes[MEM_BLOCK_SIZES] = 
{
  16,    // 0
  32,    // 1
  64,    // 2
  96,    // 3
  128,  // 4
  160,  // 5
  192,  // 6
  224,  // 7
  256,  // 8
  320,  // 9
  384,  // 10
  448,  // 11
  512,  // 12
  640,  // 13
};
/////////////////////////////////////////////////////////////////////////////
BeUInt8 BeMemoryPool::m_uBlockSizeLookup[MEM_MAX_BLOCK_SIZE + 1];
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeMemoryPool::m_bBlockSizeLookupInitialized;
/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
struct BeMemoryChunk
{
  BeInt m_iBlockSize;
  BeMemoryBlock* m_pBlocks;
};
/////////////////////////////////////////////////////////////////////////////
struct BeMemoryBlock
{
  BeMemoryBlock* m_pNext;
};
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeMemoryPool::BeMemoryPool()
{
  BE_ASSERT(MEM_BLOCK_SIZES < UCHAR_MAX);

  m_iChunkSpace = MEM_CHUNK_ARRAY_INC;
  m_iChunkCount = 0;
  m_pMemoryChunks = (BeMemoryChunk*)malloc(m_iChunkSpace * sizeof(BeMemoryChunk));
    
  memset(m_pMemoryChunks, 0, m_iChunkSpace * sizeof(BeMemoryChunk));
  memset(m_pFreeLists, 0, sizeof(m_pFreeLists));

  if ( m_bBlockSizeLookupInitialized == BE_FALSE )
  {
    BeInt j = 0;
    for (BeInt i = 1; i <= MEM_MAX_BLOCK_SIZE; ++i)
    {
      BE_ASSERT(j < MEM_BLOCK_SIZES);
      if (i <= m_iBlockSizes[j])
      {
        m_uBlockSizeLookup[i] = (BeUInt8)j;
      }
      else
      {
        ++j;
        m_uBlockSizeLookup[i] = (BeUInt8)j;
      }
    }

    m_bBlockSizeLookupInitialized = BE_TRUE;
  }
}
/////////////////////////////////////////////////////////////////////////////
BeMemoryPool::~BeMemoryPool()
{
  for (BeInt i = 0; i < m_iChunkCount; ++i)
  {
    free(m_pMemoryChunks[i].m_pBlocks);
  }

  free(m_pMemoryChunks);
}
/////////////////////////////////////////////////////////////////////////////
BeVoidP BeMemoryPool::AllocateMEM(BeInt iSize)
{
  if (iSize == 0)
    return NULL;

  BE_ASSERT(0 < iSize);

  if (iSize > MEM_MAX_BLOCK_SIZE)
  {
    return malloc(iSize);
  }

  BeInt iIndex = m_uBlockSizeLookup[iSize];
  BE_ASSERT(0 <= iIndex && iIndex < MEM_BLOCK_SIZES);

  if (m_pFreeLists[iIndex])
  {
    BeMemoryBlock* pBlock = m_pFreeLists[iIndex];
    m_pFreeLists[iIndex] = pBlock->m_pNext;
    return pBlock;
  }
  else
  {
    if (m_iChunkCount == m_iChunkSpace)
    {
      BeMemoryChunk* pOldMemoryChunks = m_pMemoryChunks;
      m_iChunkSpace += MEM_CHUNK_ARRAY_INC;
      m_pMemoryChunks = (BeMemoryChunk*)malloc(m_iChunkSpace * sizeof(BeMemoryChunk));
      memcpy(m_pMemoryChunks, pOldMemoryChunks, m_iChunkCount * sizeof(BeMemoryChunk));
      memset(m_pMemoryChunks + m_iChunkCount, 0, MEM_CHUNK_ARRAY_INC * sizeof(BeMemoryChunk));
      free(pOldMemoryChunks);
    }

    BeMemoryChunk* pChunk = m_pMemoryChunks + m_iChunkCount;
    pChunk->m_pBlocks = (BeMemoryBlock*)malloc(MEM_CHUNK_SIZE);

#if defined(_DEBUG)
    memset(pChunk->m_pBlocks, 0xcd, MEM_CHUNK_SIZE);
#endif

    BeInt iBlockSize = m_iBlockSizes[iIndex];
    pChunk->m_iBlockSize = iBlockSize;

    BeInt iBlockCount = MEM_CHUNK_SIZE / iBlockSize;
    BE_ASSERT(iBlockCount * iBlockSize <= MEM_CHUNK_SIZE);
    for (BeInt i = 0; i < iBlockCount - 1; ++i)
    {
      BeMemoryBlock* pBlock = (BeMemoryBlock*)((BeInt8*)pChunk->m_pBlocks + iBlockSize * i);
      BeMemoryBlock* pNext = (BeMemoryBlock*)((BeInt8*)pChunk->m_pBlocks + iBlockSize * (i + 1));
      pBlock->m_pNext = pNext;
    }

    BeMemoryBlock* pLast = (BeMemoryBlock*)((BeInt8*)pChunk->m_pBlocks + iBlockSize * (iBlockCount - 1));
    pLast->m_pNext = NULL;

    m_pFreeLists[iIndex] = pChunk->m_pBlocks->m_pNext;
    ++m_iChunkCount;

    return pChunk->m_pBlocks;
  }
}
/////////////////////////////////////////////////////////////////////////////
void BeMemoryPool::FreeMEM(BeVoidP p, BeInt iSize)
{
  if (iSize == 0)
    return;

  BE_ASSERT(0 < iSize);
  if (iSize > MEM_MAX_BLOCK_SIZE)
  {
    free(p);
    return;
  }

  BeInt iIndex = m_uBlockSizeLookup[iSize];
  BE_ASSERT(0 <= iIndex && iIndex < MEM_BLOCK_SIZES);

#ifdef _DEBUG
  // Verify the memory address and size is valid.
  BeInt iBlockSize = m_iBlockSizes[iIndex];
  BeBoolean bFound = BE_FALSE;
  for (BeInt i = 0; i < m_iChunkCount; ++i)
  {
    BeMemoryChunk* pChunk = m_pMemoryChunks + i;
    if (pChunk->m_iBlockSize != iBlockSize)
    {
      BE_ASSERT(  (BeInt8*)p + iBlockSize <= (BeInt8*)pChunk->m_pBlocks ||
            (BeInt8*)pChunk->m_pBlocks + MEM_CHUNK_SIZE <= (BeInt8*)p);
    }
    else
    {
      if ((BeInt8*)pChunk->m_pBlocks <= (BeInt8*)p && (BeInt8*)p + iBlockSize <= (BeInt8*)pChunk->m_pBlocks + MEM_CHUNK_SIZE)
        bFound = true;
    }
  }

  BE_ASSERT(bFound);
  memset(p, 0xfd, iBlockSize);
#endif

  BeMemoryBlock* pBlock = (BeMemoryBlock*)p;
  pBlock->m_pNext = m_pFreeLists[iIndex];
  m_pFreeLists[iIndex] = pBlock;
}
/////////////////////////////////////////////////////////////////////////////
void BeMemoryPool::Clear()
{
  for (BeInt i = 0; i < m_iChunkCount; ++i)
  {
    free(m_pMemoryChunks[i].m_pBlocks);
  }

  m_iChunkCount = 0;
  memset(m_pMemoryChunks, 0, m_iChunkSpace * sizeof(BeMemoryChunk));
  memset(m_pFreeLists, 0, sizeof(m_pFreeLists));
}
/////////////////////////////////////////////////////////////////////////////

