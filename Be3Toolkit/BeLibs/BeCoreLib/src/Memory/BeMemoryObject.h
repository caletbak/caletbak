/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeMemoryObject.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEMEMORYOBJECT_H
#define BE_BEMEMORYOBJECT_H

#include <BeCoreLib/Pch/BeCoreLibPredef.h>

#include <BeObject.h>

/////////////////////////////////////////////////
/// \class BeObject
/// \brief Base class for every class in the engine
/////////////////////////////////////////////////
class CORE_API BeMemoryObject : public BeObject
{

  public:

    /// \brief Constructor
    BeMemoryObject();

    /// \brief Destructor
    virtual ~BeMemoryObject();



    /// \brief Operator overloading new 
    BeVoidP operator new(BeSize_T uSize);

    /// \brief Operator overloading delete
    void operator delete(BeVoidP p);

    /// \brief Operator overloading new[]
    BeVoidP operator new[](BeSize_T uSize);

    /// \brief Operator overloading delete[]
    void operator delete[](BeVoidP p);

};

#endif // #ifndef BE_BEMEMORYOBJECT_H
