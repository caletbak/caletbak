/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeMemoryPool.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_MEMORYPOOL_H
#define BE_MEMORYPOOL_H

#include <BeCoreLib/Pch/BeCoreLibPredef.h>

#include <BeObject.h>

// Forward declaratios
struct BeMemoryBlock;
struct BeMemoryChunk;

/////////////////////////////////////////////////////////////////////////////
/// \class BeMemoryPool
/// \brief Memory pool for improving speed on dynamic object creation
class CORE_API BeMemoryPool : public BeObject
{

  public:

    static const BeInt MEM_CHUNK_SIZE = (16 * 1024);
    static const BeInt MEM_CHUNK_ARRAY_INC = 128;
    static const BeInt MEM_MAX_BLOCK_SIZE = 640;
    static const BeInt MEM_BLOCK_SIZES = 14;

    /// \brief  Constructor
    BeMemoryPool();

    /// \brief  Destructor
    ~BeMemoryPool();


    // Methods
    // {

      /// \brief  Allocates memory
      BeVoidP AllocateMEM(BeInt iSize);

      /// \brief  Frees memory
      void FreeMEM(BeVoidP p, BeInt iSize);

      /// \brief  Clears the memory pool
      void Clear();

    // }

private:

  /// \brief  The mempry chunks
  BeMemoryChunk* m_pMemoryChunks;

  /// \brief  Memory chunks count
  BeInt m_iChunkCount;

  /// \brief  Chunk space
  BeInt m_iChunkSpace;

  /// \brief  List of free blocks
  BeMemoryBlock* m_pFreeLists[MEM_BLOCK_SIZES];



  /// \brief  Block sizes
  static BeInt m_iBlockSizes[MEM_BLOCK_SIZES];

  /// \brief  A lookup table for memory block sizes
  static BeUInt8 m_uBlockSizeLookup[MEM_MAX_BLOCK_SIZE + 1];

  /// \brief  Its TRUE if the lookup table has been initialized
  static BeBoolean m_bBlockSizeLookupInitialized;

};

#endif // #ifndef BE_MEMORYPOOL_H
