/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeString8.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BESTRING8_H
#define BE_BESTRING8_H

#include <BeCoreLib/Pch/BeCoreLibPredef.h>

#include <Memory/BeMemoryObject.h>

/////////////////////////////////////////////////
/// \class BeString8
/// \brief Wrap class for Strings based in char
/////////////////////////////////////////////////
class CORE_API BeString8 : public BeMemoryObject
{

  private:

    /// \brief The string base
    BeStringBase8 m_kBase;

  public:

    /// \brief Constructor
    BeString8();
    /// \brief Constructor copy
    BeString8(const BeString8 &strString);
    /// \brief Constructor from char buffer
    BeString8(const BeChar8* pBuffer);
    /// \brief Destructor
    ~BeString8();

    // \brief Operators
    // {

        /// \brief  Assignment by sum
        BeString8& operator +=(const BeString8& strToAppend);

        /// \brief  Equal
        BeBoolean operator ==(const BeString8& strToCompare) const;

        /// \brief  Not Equal
        BeBoolean operator !=(const BeString8& strToCompare) const;

        /// \brief  Less
        BeBoolean operator <(const BeString8& strToCompare) const;

    // }



    // \brief Getters
    // {

        /// \brief The string base
        BeStringBase8 GetStringBase(void) const;

        /// \brief Gets the string size
        BeUInt GetSize(void) const;

        /// \brief Gets if the string is empty
        BeBoolean IsEmpty(void) const;

    // }



    // \brief String methods
    // {

        /// \brief Appends a string to this
        BeString8& Append(const BeString8& strToAppend);

        /// \brief Appends a char to this
        BeString8& Append(const BeChar8& cToAppend);

        /// \brief Compares a string to this
        BeInt Compare(const BeString8& strToCompare);

        /// \brief Compares a string to this
        BeInt Compare(const BeString8& strToCompare) const;

        /// \brief Returns the substring
        BeString8 SubString(const BeInt iPosition = 0, const BeInt iLength = -1);

        /// \brief Returns the substring
        BeString8 SubString(const BeInt iPosition = 0, const BeInt iLength = -1) const;

        /// \brief Clears the string
        void Clear(void);

        /// \brief Returns the string raw data
        const BeChar8* ToRaw(void) const;

    // }



    /// \brief Generates a empty string
    static BeString8 EMPTY(void);

};

#endif // #ifndef BE_BESTRING8_H
