/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeStringTokenizer.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <STL/BeStringTokenizer.h>

/////////////////////////////////////////////////////////////////////////////
BeStringTokenizer::BeStringTokenizer(const BeString8& strInput, const BeString8& strDelimiters)
{
  ProcessTokens(strInput, strDelimiters);
}
/////////////////////////////////////////////////////////////////////////////
BeStringTokenizer::~BeStringTokenizer()
{
  m_kStringTokens.clear();
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeString8 BeStringTokenizer::GetNextToken(void)
{
  if ( HasMoreTokens() )
    return m_kStringTokens.at(m_uCurrentToken++);
  
  return BeString8::EMPTY();
}
/////////////////////////////////////////////////////////////////////////////
BeString8 BeStringTokenizer::GetPreviousToken(void)
{
  if ( m_uCurrentToken > 0 )
    return m_kStringTokens.at(m_uCurrentToken--);
  
  return BeString8::EMPTY();
}
/////////////////////////////////////////////////////////////////////////////
BeString8 BeStringTokenizer::GetTokenAt(const BeUInt uIndex)
{
  if ( uIndex < GetTokenCount() )
    return m_kStringTokens.at(uIndex);
  
  return BeString8::EMPTY();
}
/////////////////////////////////////////////////////////////////////////////
BeString8 BeStringTokenizer::GetTokenAt(const BeUInt uIndex) const
{
  if ( uIndex < GetTokenCount() )
    return m_kStringTokens.at(uIndex);
  
  return BeString8::EMPTY();
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeStringTokenizer::HasMoreTokens(void) const
{
  return ( m_uCurrentToken < GetTokenCount() );
}
/////////////////////////////////////////////////////////////////////////////
BeUInt BeStringTokenizer::GetTokenCount(void) const
{
  return m_kStringTokens.size();
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeStringTokenizer::ProcessTokens(const BeString8& strInput, const BeString8& strDelimiters)
{
  
  m_kStringTokens.clear();

  BeBoolean bInitToken = BE_FALSE;
  BeString8 strNewToken("");

  for (BeUInt i = 0; i < strInput.GetSize(); ++i)
  {
    
    BeChar8 cCurrentStringChar = strInput.ToRaw()[i];
    BeBoolean bDelimeterFound = BE_FALSE;
    for (BeUInt j = 0; j < strDelimiters.GetSize(); ++j)
    {
      if ( cCurrentStringChar == strDelimiters.ToRaw()[j] )
      {
        bDelimeterFound = BE_TRUE;
        break;
      }
    }

    if ( bDelimeterFound )
    {
      if ( bInitToken )
      {
        bInitToken = BE_FALSE;
        m_kStringTokens.push_back(strNewToken);
        strNewToken = "";
      }
    }
    else
    {
      bInitToken = BE_TRUE;
      strNewToken.Append( cCurrentStringChar );
    }
  
  }
  
  if ( bInitToken )
    m_kStringTokens.push_back(strNewToken);
        
  m_uCurrentToken = 0;
  
}
/////////////////////////////////////////////////////////////////////////////

