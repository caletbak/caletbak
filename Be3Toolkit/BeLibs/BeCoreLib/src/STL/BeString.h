/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeString.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BESTRING_H
#define BE_BESTRING_H

#include <STL/BeString8.h>
#include <STL/BeString16.h>



// Converts a BeString16 to BeString8
BeString8 CORE_API ConvertString16To8(const BeString16& strOrig);

// Converts a BeString8 to BeString16
BeString16 CORE_API ConvertString8To16(const BeString8& strOrig);



#endif // #ifndef BE_BESTRING_H
