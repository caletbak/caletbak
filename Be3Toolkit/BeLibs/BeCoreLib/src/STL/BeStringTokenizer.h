/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeStringTokenizer.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_STRINGTOKENIZER_H
#define BE_STRINGTOKENIZER_H

#include <BeCoreLib/Pch/BeCoreLibPredef.h>

#include <STL/BeString.h>
#include <Memory/BeMemoryObject.h>
#include <vector>

/////////////////////////////////////////////////
/// \class BeStringTokenizer
/// \brief Class for getting string tokens
/////////////////////////////////////////////////
class CORE_API BeStringTokenizer : public BeMemoryObject
{

  public:

    /// \brief Constructor
    BeStringTokenizer(const BeString8& strInput, const BeString8& cDelimiters);
    /// \brief Destructor
    ~BeStringTokenizer();

    // \brief Getters
    // {

      /// \brief  Gets next token
      BeString8 GetNextToken(void);

      /// \brief  Gets previous token
      BeString8 GetPreviousToken(void);

      /// \brief  Gets token at index
      BeString8 GetTokenAt(const BeUInt uIndex) const;

      /// \brief  Gets token at index
      BeString8 GetTokenAt(const BeUInt uIndex);

      /// \brief  Returns TRUE if there is more tokens
      BeBoolean HasMoreTokens(void) const;

      /// \brief  Gets the tokens count
      BeUInt GetTokenCount(void) const;

    // }

  private:

    /// \brief  Process the tokens
    void ProcessTokens(const BeString8& strInput, const BeString8& cDelimiters);

    /// \brief  The processed string tokens array type
    typedef std::vector<BeString8> TokenArray;

    /// \brief  The processed string tokens
    TokenArray m_kStringTokens;

    /// \brief  The current token index
    BeUInt m_uCurrentToken;

};

#endif // #ifndef BE_STRINGTOKENIZER_H

