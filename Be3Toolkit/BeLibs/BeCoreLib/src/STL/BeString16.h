/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeString16.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BESTRING16_H
#define BE_BESTRING16_H

#include <BeCoreLib/Pch/BeCoreLibPredef.h>

#include <Memory/BeMemoryObject.h>

/////////////////////////////////////////////////
/// \class BeString16
/// \brief Wrap class for Strings based in w_char
/////////////////////////////////////////////////
class CORE_API BeString16 : public BeMemoryObject
{

  private:

    /// \brief The string base
    BeStringBase16 m_kBase;

  public:

    /// \brief Constructor
    BeString16();
    /// \brief Constructor copy
    BeString16(const BeString16 &strString);
    /// \brief Constructor from char buffer
    BeString16(const BeChar16* pBuffer);
    /// \brief Destructor
    ~BeString16();

    // \brief Operators
    // {

        /// \brief  Assignment by sum
        BeString16& operator +=(const BeString16& strToAppend);

        /// \brief  Equal
        BeBoolean operator ==(const BeString16& strToCompare) const;

        /// \brief  Not Equal
        BeBoolean operator !=(const BeString16& strToCompare) const;

        /// \brief  Less
        BeBoolean operator <(const BeString16& strToCompare) const;

    // }



    // \brief Getters
    // {

        /// \brief The string base
        BeStringBase16 GetStringBase(void) const;

        /// \brief Gets the string size
        BeUInt GetSize(void) const;

        /// \brief Gets if the string is empty
        BeBoolean IsEmpty(void) const;

    // }



    // \brief String methods
    // {

        /// \brief Appends a string to this
        BeString16& Append(const BeString16& strToAppend);

        /// \brief Appends a char to this
        BeString16& Append(const BeChar16& cToAppend);

        /// \brief Compares a string to this
        BeInt Compare(const BeString16& strToCompare);

        /// \brief Compares a string to this
        BeInt Compare(const BeString16& strToCompare) const;

        /// \brief Returns the substring
        BeString16 SubString(const BeInt iPosition = 0, const BeInt iLength = -1);

        /// \brief Returns the substring
        BeString16 SubString(const BeInt iPosition = 0, const BeInt iLength = -1) const;

        /// \brief Clears the string
        void Clear(void);

        /// \brief Returns the string raw data
        const BeChar16* ToRaw(void) const;

    // }



    /// \brief Generates a empty string
    static BeString16 EMPTY(void);

};

#endif // #ifndef BE_BESTRING16_H
