/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeSmartPointer.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BESMARTPOINTER_H
#define BE_BESMARTPOINTER_H

#include <BeCoreLib/Pch/BeCoreLibPredef.h>

#include <BeObject.h>
#if TARGET_IOS
#include <tr1/memory>
#endif

/////////////////////////////////////////////////
/// \class BeSmartPointer
/// \brief Wraps std::tr1::shared_ptr class
/////////////////////////////////////////////////
template <class T>
class CORE_API BeSmartPointer : public BeObject
{

  private:

    /// \brief The shared_ptr base
    std::tr1::shared_ptr<T> m_kBase;

  public:

    /// \brief Constructor
    BeSmartPointer();
    /// \brief Constructor copy
    BeSmartPointer(T* pValue);
    BeSmartPointer(const BeSmartPointer<T> &spCopy);
    /// \brief Destructor
    ~BeSmartPointer();

    // \brief Operators
    // {

      /// \brief  Assignation
      BeSmartPointer<T>& operator =(T* pValue);
      BeSmartPointer<T>& operator =(BeSmartPointer<T>& spToAssign);

      /// \brief Cast
      operator T*();
      operator T*() const;

      /// \brief  Indirection
      T* operator *();
      T* operator *() const;

      /// \brief  Dereference
      T* operator ->();
      T* operator ->() const;

    // }



    // \brief Getters
    // {

      /// \brief  Returns the raw pointer
      T* GetPointer(void);
      T* GetPointer(void) const;

    // }

};

/// \brief  Inline including
#include <STL/BeSmartPointer.inl>

#endif // #ifndef BE_BESMARTPOINTER_H
