/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeString.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <STL/BeString.h>
#include <vector>

/////////////////////////////////////////////////////////////////////////////
BeString8 ConvertString16To8(const BeString16& strOrig)
{
  std::vector<BeChar8> kBuffer( strOrig.GetSize() );

#if TARGET_WINDOWS
  CPINFO kCPInfo;
  GetCPInfo(CP_ACP, &kCPInfo);

  WideCharToMultiByte(
    CP_ACP, // code page
    WC_NO_BEST_FIT_CHARS | WC_COMPOSITECHECK, // performance and mapping flags
    strOrig.ToRaw(), // wide-character string
    strOrig.GetSize(), // number of chars in string
    &kBuffer[0], // buffer for new string (must use vector here!)
    strOrig.GetSize(), // size of buffer
    (LPCSTR)kCPInfo.DefaultChar, // default for unmappable chars
    NULL // set when default char used
  );
  kBuffer.push_back('\0');
#endif
  
  return BeString8(&kBuffer[0]);
}
/////////////////////////////////////////////////////////////////////////////
BeString16 ConvertString8To16(const BeString8& strOrig)
{
  std::vector<BeChar16> kBuffer( strOrig.GetSize() );

#if TARGET_WINDOWS

  MultiByteToWideChar(
    CP_ACP, // code page
    MB_PRECOMPOSED, // character-type options
    strOrig.ToRaw(), // string to map
    strOrig.GetSize(), // number of bytes in string
    &kBuffer[0], // wide-character buffer (must use vector here!)
    strOrig.GetSize() // size of buffer
  );
  kBuffer.push_back('\0');

#endif
  
  return BeString16(&kBuffer[0]);
}
/////////////////////////////////////////////////////////////////////////////


