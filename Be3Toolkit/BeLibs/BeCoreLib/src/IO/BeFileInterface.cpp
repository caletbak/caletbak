/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeFileInterface.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <IO/BeFileInterface.h>

#include <IO/BeFileTool.h>

#if defined(TARGET_ANDROID)
#include <android/asset_manager.h>
static AAssetManager* s_assetManager = NULL;
#endif

#define FILEINTERFACE_DEBUG 0
#define FILEINTERFACE_STREAM 1

/////////////////////////////////////////////////////////////////////////////
BeFileInterface::BeFileInterface(const char* strPath, bool isCachedStreamBuffer, unsigned char* chachedBuffer, const int fileSize, bool forceDeleteBuffer)
:
    m_strFileName(strPath),
    m_pFileHandle(NULL),
    m_pStreamBuffer(chachedBuffer),
    m_iCurrentTellPosition(0),
    m_iFileSize(fileSize),
    m_isCachedStreamBuffer(false),
    m_bForceDeleteBuffer(forceDeleteBuffer)
{
#ifdef TARGET_ANDROID
    s_assetManager = g_app->activity->assetManager;
#endif

    if (m_pStreamBuffer || isCachedStreamBuffer)
    {
        m_isCachedStreamBuffer = true;
    }
}
/////////////////////////////////////////////////////////////////////////////
BeFileInterface::~BeFileInterface()
{
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeSize_T BeFileInterface::GetSize(void)
{
    return m_iFileSize;
}
/////////////////////////////////////////////////////////////////////////////
BeUChar8* BeFileInterface::GetStreamBuffer(void)
{
    return m_pStreamBuffer;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeBoolean BeFileInterface::Open(const BeChar8* pMode)
{
#ifdef TARGET_ANDROID
    BeString8 strPath = BeFileTool::PathForResource(m_strFileName.ToRaw(), "", BE_FALSE);

#if FILEINTERFACE_DEBUG
    BE_CORE_DEBUG("BeFileInterface::Open: %s", strPath.ToRaw());
#endif

    if (strPath.SubString(0,7) == "assets:")
    {
        BeString8 strFilename = strPath.SubString(7);

        AAsset* fp = AAssetManager_open(s_assetManager, strFilename.ToRaw(), AASSET_MODE_RANDOM);
        if (!fp)
        {
            return false;
        }
        m_pFileHandle = fp;

#if     FILEINTERFACE_STREAM
        Seek(0, SEEK_END);
        m_iFileSize = Tell();
        Seek(0, SEEK_SET);

        m_pStreamBuffer = (unsigned char*)malloc(sizeof(unsigned char)*m_iFileSize);

        AAsset_read(fp, m_pStreamBuffer, m_iFileSize);

        AAsset_close(fp);
        m_pFileHandle = NULL;

#       if FILEINTERFACE_DEBUG
        BE_CORE_DEBUG("BeFileInterface::Open size: %d", m_iFileSize);
#       endif
#       endif

        m_iCurrentTellPosition = 0;

        return true;
    }

    return false;
#else
#if TARGET_IOS
    BeString8 strPath = FileTools::PathForResource(m_strFileName.c_str(), "", false);
    FILE* fp = fopen (path.c_str(), strMode);
#else
    FILE** fp = NULL;
    fopen_s(fp, m_strFileName.ToRaw(), pMode);
#endif
    if (! *fp)
    {
        return false;
    }
    m_pFileHandle = *fp;

#if FILEINTERFACE_STREAM
    Seek(0, SEEK_END);
    m_iFileSize = Tell();
    Seek(0, SEEK_SET);

    m_pStreamBuffer = (unsigned char*)malloc(sizeof(unsigned char)*m_iFileSize);
    fread(m_pStreamBuffer, 1, m_iFileSize, *fp);

    fclose(*fp);
    m_pFileHandle = NULL;
#endif

    m_iCurrentTellPosition = 0;

    return true;
#endif
}
/////////////////////////////////////////////////////////////////////////////
int BeFileInterface::Close()
{
#ifdef TARGET_ANDROID

    int retValue = -1;
    if (!m_pStreamBuffer)
    {
        AAsset* file = (AAsset*)m_pFileHandle;
        AAsset_close(file);
    }
    else
    {
        if (!m_isCachedStreamBuffer || m_bForceDeleteBuffer)
        {
            free (m_pStreamBuffer);
            m_pStreamBuffer = NULL;
        }
    }

    m_iCurrentTellPosition = 0;

#if FILEINTERFACE_DEBUG
    BE_CORE_DEBUG("BeFileInterface::Close");
#endif

    return 0;
#else

    int retValue = -1;
    if (!m_pStreamBuffer)
    {
        FILE* file = (FILE*)m_pFileHandle;
        retValue = fclose(file);

        if (retValue == 0)
        {
            m_pFileHandle = NULL;
        }
    }
    else
    {
        if (!m_isCachedStreamBuffer || m_bForceDeleteBuffer)
        {
            free(m_pStreamBuffer);
            m_pStreamBuffer = NULL;
        }

        retValue = 0;
    }

    m_iCurrentTellPosition = 0;

    return retValue;
#endif
}
/////////////////////////////////////////////////////////////////////////////
size_t BeFileInterface::Read(void* ptr, size_t size, size_t count)
{
#ifdef TARGET_ANDROID
    size_t bytesRead = 0;

    if (!m_pStreamBuffer)
    {
        AAsset* file = (AAsset*)m_pFileHandle;

        bytesRead = AAsset_read(file, ptr, count);
    }
    else
    {
        bytesRead = count;
        if (m_iCurrentTellPosition + bytesRead > m_iFileSize)
        {
            bytesRead = m_iFileSize - m_iCurrentTellPosition;
        }

        memcpy(ptr, &m_pStreamBuffer[m_iCurrentTellPosition], bytesRead);
    }

#if FILEINTERFACE_DEBUG
    BE_CORE_DEBUG("BeFileInterface::Read: %d", bytesRead);
#endif

    m_iCurrentTellPosition += bytesRead;

    return bytesRead;
#else
    if (!m_pStreamBuffer)
    {
        FILE* file = (FILE*)m_pFileHandle;
        size_t bytesRead = 0;

        if (file)
            bytesRead = fread(ptr, size, count, file);
        else
            return 0;

        return bytesRead;
    }
    else
    {
        size_t bytesRead = count;
        if (m_iCurrentTellPosition + bytesRead > m_iFileSize)
        {
            bytesRead = m_iFileSize - m_iCurrentTellPosition;
        }

        memcpy(ptr, &m_pStreamBuffer[m_iCurrentTellPosition], bytesRead);

        m_iCurrentTellPosition += bytesRead;

        return bytesRead;
    }
#endif
}
/////////////////////////////////////////////////////////////////////////////
int BeFileInterface::Seek(long int offset, int origin)
{
#ifdef TARGET_ANDROID
    off_t seekPos = 0;

    if (!m_pStreamBuffer)
    {
        AAsset* file = (AAsset*)m_pFileHandle;

        seekPos = AAsset_seek(file, offset, origin);

        m_iCurrentTellPosition = seekPos;
    }
    else
    {
        if (origin == SEEK_SET)
        {
            m_iCurrentTellPosition = offset;
        }
        else if (origin == SEEK_CUR)
        {
            m_iCurrentTellPosition += offset;
        }
        else if (origin == SEEK_END)
        {
            m_iCurrentTellPosition = m_iFileSize - offset;
        }

        seekPos = m_iCurrentTellPosition;
    }

#if FILEINTERFACE_DEBUG
    BE_CORE_DEBUG("BeFileInterface::Seek %d: %d", origin, seekPos);
#endif

    return seekPos;
#else
    if (!m_pStreamBuffer)
    {
        FILE* file = (FILE*)m_pFileHandle;
        return fseek(file, offset, origin);
    }
    else
    {
        if (origin == SEEK_SET)
        {
            m_iCurrentTellPosition = offset;
        }
        else if (origin == SEEK_CUR)
        {
            m_iCurrentTellPosition += offset;
        }
        else if (origin == SEEK_END)
        {
            m_iCurrentTellPosition = m_iFileSize - offset;
        }

        return 0;
    }
#endif
}
/////////////////////////////////////////////////////////////////////////////
long int BeFileInterface::Tell(void)
{
#ifdef TARGET_ANDROID
#if FILEINTERFACE_DEBUG
    BE_CORE_DEBUG("BeFileInterface::Tell pos: %d", m_iCurrentTellPosition);
#endif

    return m_iCurrentTellPosition;
#else
    if (!m_pStreamBuffer)
    {
        FILE* file = (FILE*)m_pFileHandle;
        return ftell(file);
    }
    else
    {
        return m_iCurrentTellPosition;
    }
#endif
}
/////////////////////////////////////////////////////////////////////////////
