/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeFileTool.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEFILETOOL_H
#define BE_BEFILETOOL_H

#include <BeCoreLib/Pch/BeCoreLibPredef.h>
#include <Memory/BeMemoryObject.h>
#include <STL/BeString.h>

#include <iostream>
#include <fstream>

#define BUFFER_SIZE 512

/////////////////////////////////////////////////
/// \class  BeFileTool
/// \brief  File Tool class for reading and writing files
/////////////////////////////////////////////////
class CORE_API BeFileTool : public BeMemoryObject 
{
public:

    enum EFileToolType
    {
      FILE_READING,
      FILE_WRITING
    };



    /// \brief  Constructor
    BeFileTool();
    /// \brief  Destructor
    ~BeFileTool();



    // \brief  External file functions
    // {

        /// \brief  Copies a file
        static void CopyFileTo(const BeString8& strFileNameOrig, const BeString8& strFileNameDest);

        /// \brief  Erases a file
        static void EraseFile(const BeString8& strFileName);

        /// \brief  Checks if file exists
        static BeBoolean FileExists(const BeString8& strFileName);

    // }



    // \brief  Methods
    // {

        static void InitStaticResources(void);

        static void DestroyStaticResources(void);

        /// \brief  Opens a file for reading or writing
        void OpenFile(const BeString8& strFileName, EFileToolType eFileToolType = FILE_READING, BeInt iMode = std::ios::binary);

        /// \brief  Closes the file
        void CloseFile();

    // }



    // \brief  Getters
    // {

        /// \brief  Returns TRUE if the file is ready to be used
        BeBoolean IsFileReady(void);

        /// \brief  Returns the current file position
        BeLong GetFileCurrentPosition(void);

        /// \brief  Returns TRUE if the Code char is a normal writing character
        BeBoolean IsValidCharacter(BeInt iValue);

        /// \brief  Returns the path with mobile modifications
        static BeString8 PathForResource(const BeString8& strPath, const BeString8& strType = "", BeBoolean bFindInCache = BE_FALSE);

        /// \brief  Returns the path with mobile modifications
        static BeString8 PathForResourceInFolder(const BeString8& strPath, const BeString8& strType, const BeString8& strFolder, BeBoolean bFindInCache = BE_FALSE);

    // }



    // \brief  Setters
    // {

        /// \brief  Sets the current file position
        void SetFileCurrentPosition(BeInt iPosition);

    // }



    // \brief  Reading Methods
    // {

        /// \brief  Reads and returns a char from file
        BeChar8 ReadChar(void);
        /// \brief  Reads and returns a string from file
        BeString8 ReadString(BeInt iNumCharsToRead);
        /// \brief  Reads and returns a line from file
        BeString8 ReadLine(void);
        /// \brief  Reads and returns an integer from file
        BeInt ReadInt(void);
        /// \brief  Reads and returns a long from file
        BeLong ReadLong(void);
        /// \brief  Reads and returns a float from file
        BeFloat ReadFloat(void);
        /// \brief  Reads and returns a boolean from file
        BeBoolean ReadBoolean(void);
        /// \brief  Reads and returns a data array from file
        void ReadArrayData(BeVoidP pData, BeInt iSize);

        /// \brief  Reads and returns a complete file and its size
        static BeVoidP ReadEntireFile(BeString16 strFileName, BeInt* iSizeFile);

    // }



    // \brief  Writing Methods
    // {

        /// \brief  Reads and returns a char from file
        void WriteChar(BeChar8 cValue);
        /// \brief  Reads and returns a string from file
        void WriteString(BeString8 strValue);
        /// \brief  Reads and returns an integer from file
        void WriteInt(BeInt iValue);
        /// \brief  Reads and returns a long from file
        void WriteLong(BeLong lValue);
        /// \brief  Reads and returns a float from file
        void WriteFloat(BeFloat fValue);
        /// \brief  Reads and returns a boolean from file
        void WriteBoolean(BeBoolean bValue);
        /// \brief  Reads and returns a data array from file
        void WriteArrayData(BeVoidP pData, BeInt iSize);

    // }

private:

    BeChar8* getBytes( int sizeOf, bool comprobateLittleEndian );

    // Prepare bytes for saving
    BeChar8* prepareBytes(char* byteData, bool comprobateLittleEndian);

#if TARGET_WINDOWS
    /// \brief  Returns the path with mobile modifications
    static BeString8 GetFullPathToResource(const BeString8& strPath);
#endif

#if TARGET_MOBILE
    static BeString8 PathInCache (const BeString8& strPath, const BeString8& strType, BeBoolean bFindInCache);

    static BeString8 PathForCachedFiles (void);
#endif

    /// \brief  Input file stream
    std::ifstream m_kInputStream;
    /// \brief  Output file stream
    std::ofstream m_kOutputStream;

    /// \brief  TRUE if the file is little endian
    BeBoolean isLittleEndian;

};

#endif // #ifndef BE_BEFILETOOL_H
