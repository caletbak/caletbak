/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeFileInterface.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEFILEINTERFACE_H
#define BE_BEFILEINTERFACE_H

#include <BeCoreLib/Pch/BeCoreLibPredef.h>

#include <STL/BeString.h>

class CORE_API BeFileInterface
{
public:

    /// \brief A file interface cached file
    class BeCachedFileInterface
    {
    public:

        BeCachedFileInterface()
        :
            m_pStreamBuffer(NULL),
            m_iFileSize(0)
        {
        }

        ~BeCachedFileInterface()
        {
            if (m_pStreamBuffer)
            {
                free(m_pStreamBuffer);
                m_pStreamBuffer = NULL;
            }
        }

        BeUChar8* m_pStreamBuffer;

        BeSize_T m_iFileSize;
    };

    /// \brief Constructor
    BeFileInterface(const BeChar8* pPath, BeBoolean bIsCachedStreamBuffer = BE_FALSE, unsigned BeChar8* pChachedBuffer = NULL, const BeInt iFileSize = 0, BeBoolean bForceDeleteBuffer = BE_FALSE);
    /// \brief Destructor
    ~BeFileInterface();



    // \brief Methods
    // {

        /// \brief  Opens the file with mode
        BeBoolean       Open(const BeChar8* pMode);

        /// \brief  Closes the current file
        BeInt           Close (void);

        /// \brief  Read a chunk of data
        BeSize_T        Read(BeVoidP pPtr, BeSize_T iSize, BeSize_T iCount);

        /// \brief  Seeks the position
        BeInt           Seek (BeLong iOffset, BeInt iOrigin);

        /// \brief  Returns the current file position
        BeLong          Tell (void);

    // }



    // \brief Getters
    // {

        /// \brief  Returns the size of the opened file
        BeSize_T        GetSize(void);

        /// \brief  Returns a direct pointer to the opened file stream
        BeUChar8*       GetStreamBuffer (void);

    // }

private:

    /// \brief  File filename
    BeString8 m_strFileName;

    /// \brief  Current file position
    BeLong m_iCurrentTellPosition;

    /// \brief  File size
    BeSize_T m_iFileSize;

    /// \brief  File handle
    BeVoidP m_pFileHandle;

    /// \brief  File stream buffer
    BeUChar8* m_pStreamBuffer;

    /// \brief  TRUE is the stream is cached
    BeBoolean m_isCachedStreamBuffer;

    /// \brief  Force to delete the buffer once the file is closed
    BeBoolean m_bForceDeleteBuffer;

};

#endif // BE_BEFILEINTERFACE_H
