/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeFileTool.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <IO/BeFileTool.h>

#if TARGET_ANDROID

#include <BeOBBManager.h>

/////////////////////////////////////////////////////////////////////////////

static AAssetManager* s_pAssetManager = NULL;

/////////////////////////////////////////////////////////////////////////////

static BeBoolean s_bUseAssetCache = BE_FALSE;

static BeString8 s_strCachePath = "";

/////////////////////////////////////////////////////////////////////////////

#endif



/////////////////////////////////////////////////////////////////////////////
BeFileTool::BeFileTool()
{
    isLittleEndian = BE_TRUE;
}
/////////////////////////////////////////////////////////////////////////////
BeFileTool::~BeFileTool()
{
    CloseFile();
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void InitStaticResources(void)
{
#if TARGET_ANDROID
    s_pAssetManager = g_app->activity->assetManager;

    if (s_pAssetManager == NULL)
    {
        BE_CORE_ERROR("FileUtils::init() AAssetManager_fromJava returned NULL");
    }
#endif
}
/////////////////////////////////////////////////////////////////////////////
void DestroyStaticResources(void)
{
}

/////////////////////////////////////////////////////////////////////////////
void BeFileTool::CopyFileTo(const BeString8& strFileNameOrig, const BeString8& strFileNameDest)
{
#if TARGET_WINDOWS
    CopyFile(ConvertString8To16(strFileNameOrig).ToRaw(), ConvertString8To16(strFileNameDest).ToRaw(), BE_FALSE);
#endif
}
/////////////////////////////////////////////////////////////////////////////
void BeFileTool::EraseFile(const BeString8& strFileName)
{
    remove(strFileName.ToRaw());
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeFileTool::FileExists(const BeString8& strFileName)
{
    FILE* file = NULL;

#if TARGET_WINDOWS
    fopen_s(&file, strFileName.ToRaw(), "r");
#else
    file = fopen(strFileName.ToRaw(), "r");
#endif

    if (file)
    {
        fclose (file);

        return BE_TRUE;
    }

    return BE_FALSE;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeFileTool::OpenFile(const BeString8& strFileName, EFileToolType eFileToolType, BeInt iMode)
{
    if (eFileToolType == FILE_READING)
    {
        m_kInputStream.open( strFileName.ToRaw(), (std::ios_base::openmode) iMode );
    }
    else if (eFileToolType == FILE_WRITING)
    {
        m_kOutputStream.open( strFileName.ToRaw(), (std::ios_base::openmode) iMode );
    }
}
/////////////////////////////////////////////////////////////////////////////
void BeFileTool::CloseFile() 
{
    if (m_kInputStream.is_open())
    {
        m_kInputStream.close();
    }
    if (m_kOutputStream.is_open())
    {
        m_kOutputStream.close();
    }
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeBoolean BeFileTool::IsFileReady(void)
{
    if (m_kInputStream.is_open())
    {
        if (!m_kInputStream.eof()) return BE_TRUE;
        return BE_FALSE;
    }
  
    if (m_kOutputStream.is_open())
    {
        if (!m_kOutputStream.eof()) return BE_TRUE;
        return BE_FALSE;
    }

    return BE_FALSE;
}
/////////////////////////////////////////////////////////////////////////////
BeLong BeFileTool::GetFileCurrentPosition()
{
    if (m_kInputStream.is_open())
    {
        return (BeLong)m_kInputStream.tellg();
    }

    return -1;
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeFileTool::IsValidCharacter(BeInt iValue)
{
    if ((iValue >= 65 && iValue <= 90) ||
       (iValue >= 97 && iValue <= 122) ||
       (iValue >= 48 && iValue <= 57) ||
       iValue == 95 || iValue == 46 || iValue == 32 || iValue == 78 || iValue == 241 )
    {
        return BE_TRUE;
    }

    return BE_FALSE;
}
/////////////////////////////////////////////////////////////////////////////



#if TARGET_WINDOWS

/////////////////////////////////////////////////////////////////////////////
BeString8 BeFileTool::PathForResource(const BeString8& strPath, const BeString8& strType, BeBoolean bFindInCache)
{
    BE_UNUSED(bFindInCache);

    BeString8 strFilename(strPath);
    if (!strType.IsEmpty())
    {
        strFilename.Append(".");
        strFilename.Append(strType);
    }

    BeString8 strResult = GetFullPathToResource(strFilename);
    if (FileExists(strResult))
    {
        return strResult;
    }
    else
    {
        return "";
    }
}
/////////////////////////////////////////////////////////////////////////////
BeString8 BeFileTool::PathForResourceInFolder(const BeString8& strPath, const BeString8& strType, const BeString8& strFolder, BeBoolean bFindInCache)
{
    BeString8 strResult(strFolder);
    if (!strPath.IsEmpty())
    {
        strResult.Append("/");
        strResult.Append(strPath);

        return PathForResource(strResult, strType);
    }
    return "";
}
/////////////////////////////////////////////////////////////////////////////
BeString8 BeFileTool::GetFullPathToResource(const BeString8& strPath)
{
    TCHAR buf[BUFFER_SIZE] = TEXT("");

    TCHAR** lppPart = { NULL };

    BeString16 szBuff = ConvertString8To16(strPath);

    GetFullPathName(szBuff.ToRaw(), BUFFER_SIZE, buf, lppPart);

    BeString16 wresult(buf);

    BeString8 strResult = ConvertString16To8(wresult);

    return strResult;
}
/////////////////////////////////////////////////////////////////////////////

#elif TARGET_ANDROID

/////////////////////////////////////////////////////////////////////////////
BeString8 BeFileTool::PathForResource(const BeString8& strPath, const BeString8& strType, BeBoolean bFindInCache)
{
    if (strPath.SubString(0, 6) == "assets")
    {
        // look first in cache folder
        BeString8 strAssetPath = PathInCache(strPath, strType, bFindInCache);
        if (FileExists(strAssetPath))
        {
            BE_CORE_DEBUG("FileUtils::pathForResource : resource found in cache: %s", strAssetPath.ToRaw());

            return strAssetPath;
        }

        strAssetPath = strPath.SubString(7);
        if (!strType.IsEmpty())
        {
            strAssetPath += ".";
            strAssetPath += strType;
        }

        BeString8 strFilenameZip = BeString8("assets/");
        strFilenameZip += strAssetPath;

        BeBoolean bZipAsset = BeOBBManager::FileExists(strFilenameZip);
        if (bZipAsset)
        {
            BeString8 strAssetPathAssets = "assets:";
            strAssetPathAssets.Append(strAssetPath);

            return strAssetPathAssets;
        }

        AAsset* pAsset = AAssetManager_open(s_pAssetManager, strAssetPath.ToRaw(), AASSET_MODE_RANDOM);
        if (pAsset != NULL)
        {
            AAsset_close(pAsset);

            BeString8 strAssetPathTemp("assets:");
            strAssetPathTemp += strAssetPath;

            // BE_CORE_DEBUG("FileUtils::pathForResource : resource found: %s", strAssetPathTemp.ToRaw());

            return strAssetPathTemp;
        }
    }

    // BE_CORE_WARNING("FileUtils::pathForResource : resource not found: %s", strPath.ToRaw());
    return "";
}
/////////////////////////////////////////////////////////////////////////////
BeString8 BeFileTool::PathForResourceInFolder(const BeString8& strPath, const BeString8& strType, const BeString8& strFolder, BeBoolean bFindInCache)
{
    BeString8 strResult(strFolder);
    if (!strPath.IsEmpty())
    {
        strResult += "/";
        strResult += strPath;

        return PathForResource(strResult, strType);
    }
    return "";
}
/////////////////////////////////////////////////////////////////////////////
BeString8 BeFileTool::PathInCache(const BeString8& strPath, const BeString8& strType, BeBoolean bFindInCache)
{
    BeString8 strPathInCache("");

    // look first in cache folder
    if (bFindInCache && s_bUseAssetCache)
    {
        strPathInCache = PathForCachedFiles();

        strPathInCache += "/";

        strPathInCache += strPath;
        if (!strType.IsEmpty())
        {
            strPathInCache += ".";
            strPathInCache += strType;
        }
    }

    return strPathInCache;
}
/////////////////////////////////////////////////////////////////////////////
BeString8 BeFileTool::PathForCachedFiles()
{
    return s_strCachePath;
}
/////////////////////////////////////////////////////////////////////////////

#endif



/////////////////////////////////////////////////////////////////////////////
void BeFileTool::SetFileCurrentPosition(BeInt iPosition)
{
    if (m_kInputStream.is_open())
    {
        m_kInputStream.seekg(iPosition);
    }
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeChar8 BeFileTool::ReadChar(void)
{
    BeChar8 cValue = '\0';
    m_kInputStream.read((BeChar8*)&cValue, sizeof(BeChar8));
    return cValue;
}
/////////////////////////////////////////////////////////////////////////////
BeString8 BeFileTool::ReadString(BeInt iNumCharsToRead)
{
    BeChar8 kValue[256];
    m_kInputStream.read((BeChar8*)&kValue[0], sizeof(BeChar8) * iNumCharsToRead);
    kValue[iNumCharsToRead] = '\0';
    return BeString8(kValue);
}
/////////////////////////////////////////////////////////////////////////////
BeString8 BeFileTool::ReadLine(void)
{
    BeString8 strValue;

    BeChar8 cCurrentCharValue = ReadChar();
    while (IsFileReady() && cCurrentCharValue != 0x0D && cCurrentCharValue != 0x0A && cCurrentCharValue != '\0')
    {
        if (cCurrentCharValue != 0x0A && cCurrentCharValue != 0x09)
        {
            strValue.Append(cCurrentCharValue);
        }
        cCurrentCharValue = ReadChar();
    }

    return strValue;
}
/////////////////////////////////////////////////////////////////////////////
BeInt BeFileTool::ReadInt(void)
{
    BeInt iValue = 0;
    m_kInputStream.read((BeChar8*)&iValue, sizeof(BeInt));
    return iValue;
}
/////////////////////////////////////////////////////////////////////////////
BeLong BeFileTool::ReadLong(void)
{
    BeLong lValue = 0;
    m_kInputStream.read((BeChar8*)&lValue, sizeof(BeLong));
    return lValue;
}
/////////////////////////////////////////////////////////////////////////////
BeFloat BeFileTool::ReadFloat(void)
{
    BeFloat fValue = 0.0f;
    m_kInputStream.read((BeChar8*)&fValue, sizeof(BeFloat));
    return fValue;
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeFileTool::ReadBoolean(void)
{
    BeBoolean bValue = BE_TRUE;
    m_kInputStream.read((BeChar8*)&bValue, sizeof(BeBoolean));
    return bValue;
}
/////////////////////////////////////////////////////////////////////////////
void BeFileTool::ReadArrayData(BeVoidP pData, BeInt iSize)
{
    m_kInputStream.read((BeChar8*)pData, sizeof(BeChar8) * iSize);
  
    if ((m_kInputStream.rdstate() & std::ifstream::eofbit ) != 0)
    {
        BE_CORE_DEBUG("ReadArrayData ERROR : eofbit");
    }
    else if ((m_kInputStream.rdstate() & std::ifstream::failbit ) != 0)
    {
        BE_CORE_DEBUG("ReadArrayData ERROR : failbit");
    }
    else if ((m_kInputStream.rdstate() & std::ifstream::badbit ) != 0)
    {
        BE_CORE_DEBUG("ReadArrayData ERROR : badbit");
    }
}
/////////////////////////////////////////////////////////////////////////////
BeVoidP BeFileTool::ReadEntireFile(BeString16 strFileName, BeInt* iSizeFile)
{
    BeChar8* pData = NULL;

    FILE* pFile = NULL;
#   if TARGET_WINDOWS
    fopen_s(&pFile, ConvertString16To8(strFileName).ToRaw(), "rb");
#   else
    pFile = fopen(ConvertString16To8(strFileName).ToRaw(), "rb");
#   endif

    if (pFile != NULL)
    {
        fseek(pFile, 0, SEEK_END);
        BeLong lSizeData = ftell(pFile);
        fseek(pFile, 0, SEEK_SET);

        pData = BeNew BeChar8[lSizeData + 1];
        fread(pData, 1, lSizeData, pFile);
        pData[lSizeData] = '\0';

        if (iSizeFile)
        {
            *iSizeFile = lSizeData;
        }

        fclose(pFile);
    }

  return pData;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeFileTool::WriteChar(BeChar8 cValue)
{
    m_kOutputStream.write((BeChar8*)&cValue, sizeof(BeChar8));
}
/////////////////////////////////////////////////////////////////////////////
void BeFileTool::WriteString(BeString8 strValue)
{
    const BeChar8* pRaw = strValue.ToRaw();

    BeInt i = 0;
    while(pRaw[i] != '\0')
    {
        WriteChar(pRaw[i]);
        i++;
    }

    WriteChar('\0');
}
/////////////////////////////////////////////////////////////////////////////
void BeFileTool::WriteInt(BeInt iValue)
{
    m_kOutputStream.write((BeChar8*)&iValue, sizeof(BeInt));
}
/////////////////////////////////////////////////////////////////////////////
void BeFileTool::WriteLong(BeLong lValue)
{
    m_kOutputStream.write((BeChar8*)&lValue, sizeof(BeLong));
}
/////////////////////////////////////////////////////////////////////////////
void BeFileTool::WriteFloat(BeFloat fValue)
{
    m_kOutputStream.write((BeChar8*)&fValue, sizeof(BeFloat));
}
/////////////////////////////////////////////////////////////////////////////
void BeFileTool::WriteBoolean(BeBoolean bValue)
{
    m_kOutputStream.write((BeChar8*)&bValue, sizeof(BeBoolean));
}
/////////////////////////////////////////////////////////////////////////////
void BeFileTool::WriteArrayData(BeVoidP pData, BeInt iSize)
{
    m_kOutputStream.write((BeChar8*)pData, sizeof(BeChar8) * iSize);
}
/////////////////////////////////////////////////////////////////////////////

