/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeAlertDialog.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <Common/BeAlertDialog.h>

/////////////////////////////////////////////////////////////////////////////

BeAlertDialog* BeAlertDialog::s_pInstance = NULL;

BeBoolean BeAlertDialog::s_isAlertDialogBeingShown = BE_FALSE;

#if TARGET_ANDROID

extern jobject g_pBeNativeActivityClassLoader;

extern jobject g_pBeNativeActivityObj;

static jclass
    g_pBeAlertDialogClass = 0;

static const char
    *g_pBeAlertDialogClassName = "org/bakengine/BeAlertDialog";

static jmethodID
    g_pBeAlertDialog_Constructor = 0,
    g_pBeAlertDialog_Initialise = 0,
    g_pBeAlertDialog_Deinitialise = 0,
    g_pBeAlertDialog_Show = 0,
    g_pBeAlertDialog_SetMaxCharacters = 0,
    g_pBeAlertDialog_Hide = 0;

JNIEXPORT void Java_org_bakengine_BeAlertDialog_onNativeInit(JNIEnv *env, jobject obj, jclass thisClass)
{
    // 
    // Cache class method references.
    // 
    BeJNIThreadEnv jniThread;

    g_pBeAlertDialog_Constructor = BeJNI::GetMethodID(jniThread, thisClass, "<init>", "()V");

    g_pBeAlertDialog_Initialise = BeJNI::GetStaticMethodID(jniThread, thisClass, "Initialise", "()V");

    g_pBeAlertDialog_Deinitialise = BeJNI::GetStaticMethodID(jniThread, thisClass, "Deinitialise", "()V");

    g_pBeAlertDialog_Show = BeJNI::GetStaticMethodID(jniThread, thisClass, "Show", "(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZ)V");

    g_pBeAlertDialog_SetMaxCharacters = BeJNI::GetStaticMethodID(jniThread, thisClass, "SetMaxCharacters", "(I)V");

    g_pBeAlertDialog_Hide = BeJNI::GetStaticMethodID(jniThread, thisClass, "Hide", "()V");

    BeJNI::CheckExceptions(jniThread);

    // 
    // Register native callbacks.
    // 

    const JNINativeMethod nativeMethodDefs[] =
    {
        { "onPressedButton", "(IILjava/lang/String;Z)V", (void *)&BeAlertDialog::onPressedButton }
    };

    env->RegisterNatives(thisClass, nativeMethodDefs, sizeof(nativeMethodDefs) / sizeof(nativeMethodDefs[0]));

    BeJNI::CheckExceptions(jniThread);
}
#endif

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

BeAlertDialog::BeAlertDialog()
:
    m_ePressedButton(DIALOG_BUTTON_NONE),
    m_strWrittenText(L""),
    m_pCurrentDelegate(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

BeAlertDialog* BeAlertDialog::GetInstance()
{
    if (!s_pInstance)
    {
        s_pInstance = new BeAlertDialog();
    }

    return s_pInstance;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef TARGET_IPHONE

void BeAlertDialog::Initialise()
{
#if TARGET_ANDROID
    BeJNIThreadEnv jniThread;

    g_pBeAlertDialogClass = BeJNI::FindClass(jniThread, g_pBeNativeActivityClassLoader, g_pBeAlertDialogClassName);

    g_pBeAlertDialogClass = (jclass)BeJNI::LocalToGlobalRef(jniThread, g_pBeAlertDialogClass);

    // Initializing

    BeJNI::CallStaticVoidMethod(jniThread, g_pBeAlertDialogClass, g_pBeAlertDialog_Initialise);

    BeJNI::CheckExceptions(jniThread);
#endif
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BeAlertDialog::Deinitialise()
{
#if TARGET_ANDROID
    BeJNIThreadEnv jniThread;

    BeJNI::CallStaticVoidMethod(jniThread, g_pBeAlertDialogClass, g_pBeAlertDialog_Deinitialise);

    BeJNI::DeleteLocalRef(jniThread, g_pBeAlertDialogClass);

    g_pBeAlertDialogClass = 0;

    BeJNI::CheckExceptions(jniThread);
#endif
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BeAlertDialog::Show(const eBeAlertDialogType iDialogType, const BeString16& strTitle, const BeString16& strMessage, const BeString16& strButtonPositive, const BeString16& strButtonNegative, const BeString16& strButtonNeutral, const BeString16& strDefaultEntryText, const BeBoolean bButtonPositiveAvoidDismiss, const BeBoolean bButtonNegativeAvoidDismiss, const BeBoolean bButtonNeutralAvoidDismiss, const BeBoolean bDialogDismissible)
{
#if TARGET_ANDROID
    BeJNIThreadEnv jniThread;

    jstring strTitleJNI = BeJNI::NewWString(jniThread, strTitle);
    jstring strMessageJNI = BeJNI::NewWString(jniThread, strMessage);
    jstring strButtonPositiveJNI = BeJNI::NewWString(jniThread, strButtonPositive);
    jstring strButtonNegativeJNI = BeJNI::NewWString(jniThread, strButtonNegative);
    jstring strButtonNeutralJNI = BeJNI::NewWString(jniThread, strButtonNeutral);
    jstring strDefaultEntryTextJNI = BeJNI::NewWString(jniThread, strDefaultEntryText);
  
    BeJNI::CallStaticVoidMethod (jniThread, g_pBeAlertDialogClass, g_pBeAlertDialog_Show, (BeInt)this, (BeInt)iDialogType, strTitleJNI, strMessageJNI, strButtonPositiveJNI, strButtonNegativeJNI, strButtonNeutralJNI, strDefaultEntryTextJNI, bButtonPositiveAvoidDismiss, bButtonNegativeAvoidDismiss, bButtonNeutralAvoidDismiss, bDialogDismissible);

    BeJNI::DeleteLocalRef(jniThread, strDefaultEntryTextJNI);
    BeJNI::DeleteLocalRef(jniThread, strButtonNeutralJNI);
    BeJNI::DeleteLocalRef(jniThread, strButtonNegativeJNI);
    BeJNI::DeleteLocalRef(jniThread, strButtonPositiveJNI);
    BeJNI::DeleteLocalRef(jniThread, strMessageJNI);
    BeJNI::DeleteLocalRef(jniThread, strTitleJNI);

    BeJNI::CheckExceptions (jniThread);
#endif

    s_isAlertDialogBeingShown = true;
}

void BeAlertDialog::Hide()
{
#if TARGET_ANDROID
    BeJNIThreadEnv jniThread;

    BeJNI::CallStaticVoidMethod(jniThread, g_pBeAlertDialogClass, g_pBeAlertDialog_Hide);

    BeJNI::CheckExceptions(jniThread);
#endif

    s_isAlertDialogBeingShown = false;
}

#endif // #ifndef TARGET_IPHONE

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BeAlertDialog::SetCurrentDelegate(BeAlertDialogDelegate* currentDelegate)
{
    m_pCurrentDelegate = currentDelegate;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

BeAlertDialog::eBeAlertDialogButton BeAlertDialog::GetPressedButton()
{
    return m_ePressedButton;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

bool BeAlertDialog::IsAlertDialogShown()
{
    return s_isAlertDialogBeingShown;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

BeString16 BeAlertDialog::GetWrittenText()
{
    return m_strWrittenText;
}

void BeAlertDialog::Reset()
{
    m_strWrittenText = L"";
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void BeAlertDialog::SetMaxLength(int value)
{
#if TARGET_ANDROID
    BeJNIThreadEnv jniThread;
    
    BeJNI::CallStaticVoidMethod(jniThread, g_pBeAlertDialogClass, g_pBeAlertDialog_SetMaxCharacters, value);
    
    BeJNI::CheckExceptions(jniThread);
#endif
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#if TARGET_ANDROID
JNIEXPORT void BeAlertDialog::onPressedButton(JNIEnv *env, jobject obj, int dialogID, int buttonID, jstring textFieldValue, jboolean bAvoidDismiss)
{
    BeJNIThreadEnv jniThread;

    BeAlertDialog* dialog = (BeAlertDialog*) dialogID;

    dialog->m_ePressedButton = (eBeAlertDialogButton) buttonID;

    dialog->m_strWrittenText = BeJNI::GetWString(jniThread, textFieldValue);

    BeJNI::CheckExceptions(jniThread);

    if (dialog->m_pCurrentDelegate)
    {
        dialog->m_pCurrentDelegate->onPressedButton (dialog->m_ePressedButton, dialog->m_strWrittenText);

        if (!bAvoidDismiss)
        {
            dialog->m_pCurrentDelegate = NULL;
        }
    }

    if (!bAvoidDismiss)
    {
        s_isAlertDialogBeingShown = false;
    }
}
#endif

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
