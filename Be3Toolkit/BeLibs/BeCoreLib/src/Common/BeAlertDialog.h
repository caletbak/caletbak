/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeAlertDialog.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_ALERTDIALOG_H
#define BE_ALERTDIALOG_H

#include <BeCoreLib/pch/BeCoreLibPredef.h>

#include <STL/BeString.h>

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#if TARGET_ANDROID

#include <BeJNI.h>

#ifdef __cplusplus
extern "C"
{
#endif

    extern void Java_org_bakengine_BeAlertDialog_onNativeInit (JNIEnv *env, jobject obj, jclass thisClass);

#ifdef __cplusplus
}
#endif

#endif // #if TARGET_ANDROID

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class CORE_API BeAlertDialog
{

public:

    /// \brief  Dialog type
    enum eBeAlertDialogType
    {
        DIALOG_INFO = 0,
        DIALOG_TEXT_ENTRY,
        DIALOG_NUMBER_ENTRY,
        DIALOG_SPINNER
    };

    /// \brief  Dialog buttons
    enum eBeAlertDialogButton
    {
        DIALOG_BUTTON_NONE = 0,

        DIALOG_BUTTON_POSITIVE,
        DIALOG_BUTTON_NEGATIVE,
        DIALOG_BUTTON_NEUTRAL
    };

    /// \brief  Dialog delegate type
    class BeAlertDialogDelegate
    {
    public:

        virtual void onPressedButton(eBeAlertDialogButton iButtonID, const BeString16& strTextFieldValue) = 0;
    };

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /// \brief  Constructor
    BeAlertDialog (void);

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // Methods
    // {

        /// \brief  Initialises static resources
        static void Initialise();

        /// \brief  Deinitialises static resources
        static void Deinitialise();

        /// \brief  Get Dialog instance class
        static BeAlertDialog* GetInstance();

        /// \brief  Shows the dialog
        void Show(const eBeAlertDialogType iDialogType, const BeString16& strTitle, const BeString16& strMessage, const BeString16& strButtonPositive = L"", const BeString16& strButtonNegative = L"", const BeString16& strButtonNeutral = L"", const BeString16& strDefaultEntryText = L"", const BeBoolean bButtonPositiveAvoidDismiss = BE_FALSE, const BeBoolean bButtonNegativeAvoidDismiss = BE_FALSE, const BeBoolean bButtonNeutralAvoidDismiss = false, const BeBoolean bDialogDismissible = BE_TRUE);

        /// \brief  Hides the dialog
        void Hide(void);

        /// \brief  Resets the dialog
        void Reset();

    // }



    // Getters
    // {

        /// \brief  Returns TRUE if a dialog is being shown
        static bool IsAlertDialogShown();

        /// \brief  Returns the pressed dialog button
        eBeAlertDialogButton GetPressedButton(void);

        /// \brief  Returns the written text in the dialog if it has
        BeString16 GetWrittenText(void);

    // }



    // Setters
    // {

        /// \brief  Sets the current dialog listener
        void SetCurrentDelegate(BeAlertDialogDelegate* pCurrentDelegate);

        /// \brief  Sets the MAX length for input text
        void SetMaxLength(BeInt value);

    // }

#if TARGET_ANDROID

public:

    /// \brief  Pressed button callback
    static JNIEXPORT void JNICALL onPressedButton (JNIEnv *env, jobject obj, BeInt iDialogID, BeInt iButtonID, jstring textFieldValue, jboolean avoidDismiss);

#else

public:

    /// \brief  Pressed button callback
    void onPressedButton(BeInt iButtonID, const BeString16& strTextFieldValue);

#endif // #if TARGET_ANDROID

private:

    /// \brief  The dialog instance
    static BeAlertDialog* s_pInstance;

    /// \brief  The current dialog listener
    BeAlertDialogDelegate* m_pCurrentDelegate;

    /// \brief  The pressed dialog button
    eBeAlertDialogButton m_ePressedButton;

    /// \brief  The written text in the dialog
    BeString16 m_strWrittenText;

    /// \brief  TRUE if some dialog is being shown
    static BeBoolean s_isAlertDialogBeingShown;

};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#endif // BE_ALERTDIALOG_H
