/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeTimer.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BETIMER_H
#define BE_BETIMER_H

#include <BeCoreLib/Pch/BeCoreLibPredef.h>

#include <Memory/BeMemoryObject.h>
#include <STL/BeSmartPointer.h>

/////////////////////////////////////////////////
/// \class BeTimer
/// \brief Timer class for controlling time
/////////////////////////////////////////////////
class CORE_API BeTimer : public BeMemoryObject
{
  public:

    /// \brief Constructor
    BeTimer();

    /// \brief Destructor
    ~BeTimer();



    // Methods
    // {

      /// \brief Starts the Timer
      void Start (BeLong requestedTime);

      /// \brief Starts the Timer
      void Start (BeLong requestedTime, BeBoolean bLoop);

      /// \brief Starts the Timer
      void Start (BeLong requestedTime, BeLong initTime, BeBoolean _bLoop);

      /// \brief Stops the Timer
      void Stop (void);

    // }



    // Getters
    // {

      /// \brief  Gets Timer Delta
      BeFloat GetDelta (void);         /* 0..1 */

      /// \brief  Gets Timer Delta
      BeFloat GetInverseDelta (void);  /* 1..0 */

      /// \brief  Gets Timer Delta
      BeFloat GetDeltaPI (void);       /* 0..0 */

      /// \brief  Gets Timer Delta
      BeFloat GetDelta2PI (void);      /* 0..0 */

      /// \brief  Gets Timer Delta
      BeFloat GetDeltaPI2 (void);      /* 0..1 */

      /// \brief  Gets Timer Delta
      BeFloat GetDelta4PI (void);      /* 0..0 */

      /// \brief  Gets Timer Delta
      BeFloat GetDelta3PI4 (void);     /* */

      /// \brief  Gets Timer Delta
      BeFloat GetDeltaAceFre (void);   /* 0..1 */

      /// \brief  Gets Timer Elapsed Time
      BeLong GetElapsedTime (void);

      /// \brief  Gets Timer Elapsed Time since finish time
      BeLong GetOverTimeFinished (void);

      /// \brief  Is Timer finished?
      BeBoolean IsFinished (void);

      /// \brief  Is Timer looping?
      BeBoolean GetLoop (void);

    // }

  private:

    /// \brief  Requested Time
    BeLong m_iRequestedTime;

    /// \brief  Init Time in MS
    BeLong m_iInitTime;

    /// \brief  Finish timer time
    BeLong m_iFinishTime;

    /// \brief  Timer is looping?
    BeBoolean m_bLoop;

};

// Smart pointer BeTimer
typedef BeSmartPointer<BeTimer> BeTimerPtr;

#endif // #ifndef BE_BETIMER_H


