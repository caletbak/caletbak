/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeSmartPointer.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <BeMutexManager.h>

BeNoEmptyFile();

/*
#include "common\Headers\CMutexManager.h"

CMutexManager CMutexManager::mutexManager;

CMutexManager::CMutexManager(){

  mutexItemsLogic.clear();
  mutexItemsRender.clear();
  mutexItemsResources.clear();

  for ( int i = 0; i < 256; i ++ ){
    mutexItemsLogic.push_back( BeNew CMutexItem() );
    mutexItemsRender.push_back( BeNew CMutexItem() );
    mutexItemsResources.push_back( BeNew CMutexItem() );
  }

}
CMutexManager::~CMutexManager(){

  for ( int i = 0; i < mutexItemsLogic.size(); i++ ){
    BeDelete mutexItemsLogic[i];
    mutexItemsLogic[i] = NULL;
  }
  for ( int i = 0; i < mutexItemsRender.size(); i++ ){
    BeDelete mutexItemsRender[i];
    mutexItemsRender[i] = NULL;
  }
  for ( int i = 0; i < mutexItemsResources.size(); i++ ){
    BeDelete mutexItemsResources[i];
    mutexItemsResources[i] = NULL;
  }

  mutexItemsLogic.clear();
  mutexItemsRender.clear();
  mutexItemsResources.clear();

}

void CMutexManager::insertFromLogic(char* mutexName){

  #ifdef MUTEX_MANAGER

  synchronized(mutex)
  {

  //mutex.Lock();

  for ( int i = 0; i < mutexItemsLogic.size(); i++ ){
    
    CMutexItem* mutexItem = mutexItemsLogic[i];

    if ( !mutexItem->used ){
      sprintf( mutexItem->desc, "%s\0", mutexName );
      mutexItem->timer.start(3000);
      mutexItem->used = true;
      break;
    }
    
  }

  }

  //mutex.Unlock();

  #endif

}
void CMutexManager::insertFromRender(char* mutexName){
  
  #ifdef MUTEX_MANAGER

  synchronized(mutex)
  {
  //mutex.Lock();

  for ( int i = 0; i < mutexItemsRender.size(); i++ ){
    
    CMutexItem* mutexItem = mutexItemsRender[i];

    if ( !mutexItem->used ){
      sprintf( mutexItem->desc, "%s\0", mutexName );
      mutexItem->timer.start(3000);
      mutexItem->used = true;
      break;
    }
    
  }

  //mutex.Unlock();
  }

  #endif

}
void CMutexManager::insertFromResources(char* mutexName){
  
  #ifdef MUTEX_MANAGER

  //mutex.Lock();
  synchronized(mutex)
  {

  for ( int i = 0; i < mutexItemsResources.size(); i++ ){
    
    CMutexItem* mutexItem = mutexItemsResources[i];

    if ( !mutexItem->used ){
      sprintf( mutexItem->desc, "%s\0", mutexName );
      mutexItem->timer.start(3000);
      mutexItem->used = true;
      break;
    }
    
  }

  //mutex.Unlock();

  }

  #endif

}

void CMutexManager::deleteFromLogic(char* mutexName){

  #ifdef MUTEX_MANAGER

  //mutex.Lock();

  synchronized(mutex)
  {

  for ( int i = 0; i < mutexItemsLogic.size(); i++ ){
    
    CMutexItem* mutexItem = mutexItemsLogic[i];

    if ( mutexItem->used ){
      //if ( mutexItem->desc.compare(mutexName) == 0 ){
      if ( compareCharStrings(mutexItem->desc, mutexName) == 0 ){

        mutexItem->used = false;
        break;

      }
    }
    
  }

  //mutex.Unlock();

  }

  #endif

}
void CMutexManager::deleteFromRender(char* mutexName){

  #ifdef MUTEX_MANAGER

  //mutex.Lock();
  synchronized(mutex)
  {

  for ( int i = 0; i < mutexItemsRender.size(); i++ ){
    
    CMutexItem* mutexItem = mutexItemsRender[i];

    if ( mutexItem->used ){
      //if ( mutexItem->desc.compare(mutexName) == 0 ){
      if ( compareCharStrings(mutexItem->desc, mutexName) == 0 ){

        mutexItem->used = false;
        break;

      }
    }
    
  }

  //mutex.Unlock();

  }

  #endif

}
void CMutexManager::deleteFromResources(char* mutexName){

  #ifdef MUTEX_MANAGER

  synchronized(mutex)
  {

  //mutex.Lock();
  
  for ( int i = 0; i < mutexItemsResources.size(); i++ ){
    
    CMutexItem* mutexItem = mutexItemsResources[i];

    if ( mutexItem->used ){
      //if ( mutexItem->desc.compare(mutexName) == 0 ){
      if ( compareCharStrings(mutexItem->desc, mutexName) == 0 ){

        mutexItem->used = false;
        break;

      }
    }
    
  }

  //mutex.Unlock();
  }

  #endif

}

void CMutexManager::updateFromLogic(){

#ifdef MUTEX_MANAGER

  //mutex.Lock();
  synchronized(mutex)
  {

  for ( int i = 0; i < mutexItemsLogic.size(); i++ ){
    
    CMutexItem* mutexItem = mutexItemsLogic[i];

    if ( mutexItem->used ){
      if ( mutexItem->timer.terminado() ){

        debug << "updateFromLogic LOGIC MUTEX PROBLEM : " << mutexItem->desc << "\n";

        debug << "REVISION : \n";

        for ( int j = 0; j < mutexItemsLogic.size(); j++ ){
    
          CMutexItem* mutexItem2 = mutexItemsLogic[j];
          if ( mutexItem != mutexItem2 && mutexItem2->used ){
            if ( mutexItem2->timer.terminado() ){
              debug << "  REVISION LOGIC MUTEX PROBLEM : " << mutexItem2->desc << "\n";
            }
          }

        }

        for ( int j = 0; j < mutexItemsResources.size(); j++ ){
    
          CMutexItem* mutexItem2 = mutexItemsResources[j];
          if ( mutexItem != mutexItem2 && mutexItem2->used ){
            if ( mutexItem2->timer.terminado() ){
              debug << "  REVISION RESOURCES MUTEX PROBLEM : " << mutexItem2->desc << "\n";
            }
          }

        }

        for ( int j = 0; j < mutexItemsRender.size(); j++ ){
    
          CMutexItem* mutexItem2 = mutexItemsRender[j];
          if ( mutexItem != mutexItem2 && mutexItem2->used ){
            if ( mutexItem2->timer.terminado() ){
              debug << "  REVISION RENDER MUTEX PROBLEM : " << mutexItem2->desc << "\n";
            }
          }

        }

        debug << "\n";
        mutexItem->used = false;

      }
    }
    
  }

  for ( int i = 0; i < mutexItemsRender.size(); i++ ){
    
    CMutexItem* mutexItem = mutexItemsRender[i];

    if ( mutexItem->used ){
      if ( mutexItem->timer.terminado() ){

        debug << "updateFromLogic RENDER MUTEX PROBLEM : " << mutexItem->desc << "\n";

        debug << "REVISION : \n";

        for ( int j = 0; j < mutexItemsLogic.size(); j++ ){
    
          CMutexItem* mutexItem2 = mutexItemsLogic[j];
          if ( mutexItem != mutexItem2 && mutexItem2->used ){
            if ( mutexItem2->timer.terminado() ){
              debug << "  REVISION LOGIC MUTEX PROBLEM : " << mutexItem2->desc << "\n";
            }
          }

        }

        for ( int j = 0; j < mutexItemsResources.size(); j++ ){
    
          CMutexItem* mutexItem2 = mutexItemsResources[j];
          if ( mutexItem != mutexItem2 && mutexItem2->used ){
            if ( mutexItem2->timer.terminado() ){
              debug << "  REVISION RESOURCES MUTEX PROBLEM : " << mutexItem2->desc << "\n";
            }
          }

        }

        for ( int j = 0; j < mutexItemsRender.size(); j++ ){
    
          CMutexItem* mutexItem2 = mutexItemsRender[j];
          if ( mutexItem != mutexItem2 && mutexItem2->used ){
            if ( mutexItem2->timer.terminado() ){
              debug << "  REVISION RENDER MUTEX PROBLEM : " << mutexItem2->desc << "\n";
            }
          }

        }

        debug << "\n";
        mutexItem->used = false;

      }
    }
    
  }

  for ( int i = 0; i < mutexItemsResources.size(); i++ ){
    
    CMutexItem* mutexItem = mutexItemsResources[i];

    if ( mutexItem->used ){
      if ( mutexItem->timer.terminado() ){

        debug << "updateFromLogic RESOURCES MUTEX PROBLEM : " << mutexItem->desc << "\n";

        debug << "REVISION : \n";

        for ( int j = 0; j < mutexItemsLogic.size(); j++ ){
    
          CMutexItem* mutexItem2 = mutexItemsLogic[j];
          if ( mutexItem != mutexItem2 && mutexItem2->used ){
            if ( mutexItem2->timer.terminado() ){
              debug << "  REVISION LOGIC MUTEX PROBLEM : " << mutexItem2->desc << "\n";
            }
          }

        }

        for ( int j = 0; j < mutexItemsResources.size(); j++ ){
    
          CMutexItem* mutexItem2 = mutexItemsResources[j];
          if ( mutexItem != mutexItem2 && mutexItem2->used ){
            if ( mutexItem2->timer.terminado() ){
              debug << "  REVISION RESOURCES MUTEX PROBLEM : " << mutexItem2->desc << "\n";
            }
          }

        }

        for ( int j = 0; j < mutexItemsRender.size(); j++ ){
    
          CMutexItem* mutexItem2 = mutexItemsRender[j];
          if ( mutexItem != mutexItem2 && mutexItem2->used ){
            if ( mutexItem2->timer.terminado() ){
              debug << "  REVISION RENDER MUTEX PROBLEM : " << mutexItem2->desc << "\n";
            }
          }

        }

        debug << "\n";
        mutexItem->used = false;

      }
    }
    
  }

  //mutex.Unlock();
  }

#endif

}
void CMutexManager::updateFromRender(){

#ifdef MUTEX_MANAGER

  //mutex.Lock();
  synchronized(mutex)
  {

  for ( int i = 0; i < mutexItemsLogic.size(); i++ ){
    
    CMutexItem* mutexItem = mutexItemsLogic[i];

    if ( mutexItem->used ){
      if ( mutexItem->timer.terminado() ){

        debug << "updateFromRender LOGIC MUTEX PROBLEM : " << mutexItem->desc << "\n";

        debug << "REVISION : \n";

        for ( int j = 0; j < mutexItemsLogic.size(); j++ ){
    
          CMutexItem* mutexItem2 = mutexItemsLogic[j];
          if ( mutexItem != mutexItem2 && mutexItem2->used ){
            if ( mutexItem2->timer.terminado() ){
              debug << "  REVISION LOGIC MUTEX PROBLEM : " << mutexItem2->desc << "\n";
            }
          }

        }

        for ( int j = 0; j < mutexItemsResources.size(); j++ ){
    
          CMutexItem* mutexItem2 = mutexItemsResources[j];
          if ( mutexItem != mutexItem2 && mutexItem2->used ){
            if ( mutexItem2->timer.terminado() ){
              debug << "  REVISION RESOURCES MUTEX PROBLEM : " << mutexItem2->desc << "\n";
            }
          }

        }

        for ( int j = 0; j < mutexItemsRender.size(); j++ ){
    
          CMutexItem* mutexItem2 = mutexItemsRender[j];
          if ( mutexItem != mutexItem2 && mutexItem2->used ){
            if ( mutexItem2->timer.terminado() ){
              debug << "  REVISION RENDER MUTEX PROBLEM : " << mutexItem2->desc << "\n";
            }
          }

        }

        debug << "\n";
        mutexItem->used = false;

      }
    }
    
  }

  for ( int i = 0; i < mutexItemsRender.size(); i++ ){
    
    CMutexItem* mutexItem = mutexItemsRender[i];

    if ( mutexItem->used ){
      if ( mutexItem->timer.terminado() ){

        debug << "updateFromRender RENDER MUTEX PROBLEM : " << mutexItem->desc << "\n";

        debug << "REVISION : \n";

        for ( int j = 0; j < mutexItemsLogic.size(); j++ ){
    
          CMutexItem* mutexItem2 = mutexItemsLogic[j];
          if ( mutexItem != mutexItem2 && mutexItem2->used ){
            if ( mutexItem2->timer.terminado() ){
              debug << "  REVISION LOGIC MUTEX PROBLEM : " << mutexItem2->desc << "\n";
            }
          }

        }

        for ( int j = 0; j < mutexItemsResources.size(); j++ ){
    
          CMutexItem* mutexItem2 = mutexItemsResources[j];
          if ( mutexItem != mutexItem2 && mutexItem2->used ){
            if ( mutexItem2->timer.terminado() ){
              debug << "  REVISION RESOURCES MUTEX PROBLEM : " << mutexItem2->desc << "\n";
            }
          }

        }

        for ( int j = 0; j < mutexItemsRender.size(); j++ ){
    
          CMutexItem* mutexItem2 = mutexItemsRender[j];
          if ( mutexItem != mutexItem2 && mutexItem2->used ){
            if ( mutexItem2->timer.terminado() ){
              debug << "  REVISION RENDER MUTEX PROBLEM : " << mutexItem2->desc << "\n";
            }
          }

        }

        debug << "\n";
        mutexItem->used = false;

      }
    }
    
  }

  for ( int i = 0; i < mutexItemsResources.size(); i++ ){
    
    CMutexItem* mutexItem = mutexItemsResources[i];

    if ( mutexItem->used ){
      if ( mutexItem->timer.terminado() ){

        debug << "updateFromRender RESOURCES MUTEX PROBLEM : " << mutexItem->desc << "\n";

        debug << "REVISION : \n";

        for ( int j = 0; j < mutexItemsLogic.size(); j++ ){
    
          CMutexItem* mutexItem2 = mutexItemsLogic[j];
          if ( mutexItem != mutexItem2 && mutexItem2->used ){
            if ( mutexItem2->timer.terminado() ){
              debug << "  REVISION LOGIC MUTEX PROBLEM : " << mutexItem2->desc << "\n";
            }
          }

        }

        for ( int j = 0; j < mutexItemsResources.size(); j++ ){
    
          CMutexItem* mutexItem2 = mutexItemsResources[j];
          if ( mutexItem != mutexItem2 && mutexItem2->used ){
            if ( mutexItem2->timer.terminado() ){
              debug << "  REVISION RESOURCES MUTEX PROBLEM : " << mutexItem2->desc << "\n";
            }
          }

        }

        for ( int j = 0; j < mutexItemsRender.size(); j++ ){
    
          CMutexItem* mutexItem2 = mutexItemsRender[j];
          if ( mutexItem != mutexItem2 && mutexItem2->used ){
            if ( mutexItem2->timer.terminado() ){
              debug << "  REVISION RENDER MUTEX PROBLEM : " << mutexItem2->desc << "\n";
            }
          }

        }

        debug << "\n";
        mutexItem->used = false;

      }
    }
    
  }

  //mutex.Unlock();
  }

#endif

}
void CMutexManager::updateFromResources(){

#ifdef MUTEX_MANAGER

  //mutex.Lock();
  synchronized(mutex)
  {

  for ( int i = 0; i < mutexItemsLogic.size(); i++ ){
    
    CMutexItem* mutexItem = mutexItemsLogic[i];

    if ( mutexItem->used ){
      if ( mutexItem->timer.terminado() ){

        debug << "updateFromResources LOGIC MUTEX PROBLEM : " << mutexItem->desc << "\n";

        debug << "REVISION : \n";

        for ( int j = 0; j < mutexItemsLogic.size(); j++ ){
    
          CMutexItem* mutexItem2 = mutexItemsLogic[j];
          if ( mutexItem != mutexItem2 && mutexItem2->used ){
            if ( mutexItem2->timer.terminado() ){
              debug << "  REVISION LOGIC MUTEX PROBLEM : " << mutexItem2->desc << "\n";
            }
          }

        }

        for ( int j = 0; j < mutexItemsResources.size(); j++ ){
    
          CMutexItem* mutexItem2 = mutexItemsResources[j];
          if ( mutexItem != mutexItem2 && mutexItem2->used ){
            if ( mutexItem2->timer.terminado() ){
              debug << "  REVISION RESOURCES MUTEX PROBLEM : " << mutexItem2->desc << "\n";
            }
          }

        }

        for ( int j = 0; j < mutexItemsRender.size(); j++ ){
    
          CMutexItem* mutexItem2 = mutexItemsRender[j];
          if ( mutexItem != mutexItem2 && mutexItem2->used ){
            if ( mutexItem2->timer.terminado() ){
              debug << "  REVISION RENDER MUTEX PROBLEM : " << mutexItem2->desc << "\n";
            }
          }

        }

        debug << "\n";

        mutexItem->used = false;

      }
    }
    
  }

  for ( int i = 0; i < mutexItemsRender.size(); i++ ){
    
    CMutexItem* mutexItem = mutexItemsRender[i];

    if ( mutexItem->used ){
      if ( mutexItem->timer.terminado() ){

        debug << "updateFromResources RENDER MUTEX PROBLEM : " << mutexItem->desc << "\n";

        debug << "REVISION : \n";

        for ( int j = 0; j < mutexItemsLogic.size(); j++ ){
    
          CMutexItem* mutexItem2 = mutexItemsLogic[j];
          if ( mutexItem != mutexItem2 && mutexItem2->used ){
            if ( mutexItem2->timer.terminado() ){
              debug << "  REVISION LOGIC MUTEX PROBLEM : " << mutexItem2->desc << "\n";
            }
          }

        }

        for ( int j = 0; j < mutexItemsResources.size(); j++ ){
    
          CMutexItem* mutexItem2 = mutexItemsResources[j];
          if ( mutexItem != mutexItem2 && mutexItem2->used ){
            if ( mutexItem2->timer.terminado() ){
              debug << "  REVISION RESOURCES MUTEX PROBLEM : " << mutexItem2->desc << "\n";
            }
          }

        }

        for ( int j = 0; j < mutexItemsRender.size(); j++ ){
    
          CMutexItem* mutexItem2 = mutexItemsRender[j];
          if ( mutexItem != mutexItem2 && mutexItem2->used ){
            if ( mutexItem2->timer.terminado() ){
              debug << "  REVISION RENDER MUTEX PROBLEM : " << mutexItem2->desc << "\n";
            }
          }

        }

        debug << "\n";
        mutexItem->used = false;

      }
    }
    
  }

  for ( int i = 0; i < mutexItemsResources.size(); i++ ){
    
    CMutexItem* mutexItem = mutexItemsResources[i];

    if ( mutexItem->used ){
      if ( mutexItem->timer.terminado() ){

        debug << "updateFromResources RESOURCES MUTEX PROBLEM : " << mutexItem->desc << "\n";

        debug << "REVISION : \n";

        for ( int j = 0; j < mutexItemsLogic.size(); j++ ){
    
          CMutexItem* mutexItem2 = mutexItemsLogic[j];
          if ( mutexItem != mutexItem2 && mutexItem2->used ){
            if ( mutexItem2->timer.terminado() ){
              debug << "  REVISION LOGIC MUTEX PROBLEM : " << mutexItem2->desc << "\n";
            }
          }

        }

        for ( int j = 0; j < mutexItemsResources.size(); j++ ){
    
          CMutexItem* mutexItem2 = mutexItemsResources[j];
          if ( mutexItem != mutexItem2 && mutexItem2->used ){
            if ( mutexItem2->timer.terminado() ){
              debug << "  REVISION RESOURCES MUTEX PROBLEM : " << mutexItem2->desc << "\n";
            }
          }

        }

        for ( int j = 0; j < mutexItemsRender.size(); j++ ){
    
          CMutexItem* mutexItem2 = mutexItemsRender[j];
          if ( mutexItem != mutexItem2 && mutexItem2->used ){
            if ( mutexItem2->timer.terminado() ){
              debug << "  REVISION RENDER MUTEX PROBLEM : " << mutexItem2->desc << "\n";
            }
          }

        }

        debug << "\n";
        mutexItem->used = false;

      }
    }
    
  }

  //mutex.Unlock();
  }

#endif

}
*/

