/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeCoreTypes.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <BeCoreTypes.h>

/////////////////////////////////////////////////////////////////////////////
BeString8 BeCoreTypes::GetTypeNameString(BeCoreTypes::ETypeNames eType)
{
    switch (eType)
    {
        case BE_CHAR8:
        {
            return "BE_CHAR8";
        }
        case BE_UNSIGNED_CHAR8:
        {
            return "BE_UNSIGNED_CHAR8";
        }
        case BE_CHAR16:
        {
            return "BE_CHAR16";
        }
        case BE_UNSIGNED_CHAR16:
        {
            return "BE_UNSIGNED_CHAR16";
        }
        case BE_SHORT:
        {
            return "BE_SHORT";
        }
        case BE_UNSIGNED_SHORT:
        {
            return "BE_UNSIGNED_SHORT";
        }
        case BE_INT:
        {
            return "BE_INT";
        }
        case BE_UNSIGNED_INT:
        {
            return "BE_UNSIGNED_INT";
        }
        case BE_FLOAT:
        {
            return "BE_FLOAT";
        }
        case BE_DOUBLE:
        {
            return "BE_DOUBLE";
        }
        case BE_BOOLEAN:
        {
            return "BE_BOOLEAN";
        }
        case BE_VOID:
        {
            return "BE_VOID";
        }
    }

    return "UNKNOWN";
}
/////////////////////////////////////////////////////////////////////////////
