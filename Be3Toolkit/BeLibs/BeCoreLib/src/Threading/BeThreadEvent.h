/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeThreadEvent.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BETHREADEVENT_H
#define BE_BETHREADEVENT_H

#include <BeCoreLib/Pch/BeCoreLibPredef.h>

#include <Memory/BeMemoryObject.h>

class CORE_API BeThreadEvent : public BeMemoryObject
{

  public:

    /// \brief Constructor
    BeThreadEvent(void);
    /// \brief Destructor
    ~BeThreadEvent(void);



    // \brief Setters
    // {

      /// \brief Set an event to signaled
      void Set(void);  

    // }



    // \brief Getters
    // {

      /// \brief Returns TRUE if the event has been created
      BeBoolean IsCreated(void);

    // }



    // \brief Methods
    // {

      /// \brief Wait for an event object to be set to signaled.  must be paired with a call to reset within the same thread.
      BeBoolean Wait(void);

      /// \brief Reset an event flag to unsignaled wait must be paired with reset within the same thread.
      void Reset(void);

    // }



  private:

    /// \brief The owner thread of this event
    ThreadID m_iOwnerThread;
    /// \brief TRUE if has been successfully created
    BeBoolean m_bCreated;

    /// \brief The event handle
#if TARGET_WINDOWS
    BeHandle m_pEvent;
#else
    pthread_cond_t m_ready;
    pthread_mutex_t m_lock;
#endif

};

#endif // #ifndef BE_BETHREADEVENT_H


