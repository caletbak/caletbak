/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeThread.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BETHREAD_H
#define BE_BETHREAD_H

#include <BeCoreLib/Pch/BeCoreLibPredef.h>

#include <Threading/BeThreadEvent.h>
#include <Threading/BeThreadMutex.h>
#include <Memory/BeMemoryObject.h>

// Forward declarations
class BeThreadTask;



#define QUEUE_SIZE 100
#define DEFAULT_STACK_SIZE 0
#if    !TARGET_WINDOWS
  void Sleep(BeUInt uMilliSeconds);
#endif



class CORE_API BeThread : public BeMemoryObject
{

  public:

    static const BeShort NO_ERRORS;
    static const BeShort MUTEX_CREATION;
    static const BeShort EVENT_CREATION;
    static const BeShort THREAD_CREATION;
    static const BeShort UNKNOWN;
    static const BeShort ILLEGAL_USE_OF_EVENT;
    static const BeShort MEMORY_FAULT;
    static const BeShort EVENT_AND_TYPE_DONT_MATCH;
    static const BeShort STACK_OVERFLOW;
    static const BeShort STACK_EMPTY;
    static const BeShort STACK_FULL;
  
    enum eThreadState
    {
      THREAD_STATE_BUSY,               // thread is currently handling a task
      THREAD_STATE_WAITING,            // thread is waiting for something to do
      THREAD_STATE_DOWN,               // thread is not running
      THREAD_STATE_SHUTTING_DOWN,      // thread is in the process of shutting down
      THREAD_STATE_FAULT               // an error has occured and the thread could not be launched
    };

    enum eThreadType
    {
      THREAD_TYPE_HOMOGENEOUS,
      THREAD_TYPE_SPECIALIZED,
      THREAD_TYPE_INTERVAL_DRIVEN,
      THREAD_TYPE_NOT_DEFINED
    };



    /// \brief Constructor
    BeThread(void);
    /// \brief Destructor
    ~BeThread(void);

#if TARGET_WINDOWS
#   if USE_BEGIN_THREAD
      friend unsigned __stdcall _THKERNEL( BeVoidP lpvData );
#   else
      friend BeULong WINAPI _THKERNEL( BeVoidP lpvData );
#   endif
#else
      friend BeVoidP _THKERNEL(BeVoidP lpvData);
#endif



    // \brief Getters
    // {

      /// \brief Returns TRUE if the actual CPU thread running is this
      BeBoolean IsFromSameThread(void);

      /// \brief Returns the thread ID
      void GetId(ThreadID* pId);

      /// \brief Returns the thread state
      eThreadState GetThreadState(void);

      /// \brief Returns the error flags/state of the object
      BeULong GetErrorFlags(void);

      /// \brief Returns the number of events pending
      BeUInt GetEventsPending(void);

    // }



    // \brief Setters
    // {

      /// \brief Sets the thread type
      void SetThreadType( eThreadType eType = THREAD_TYPE_NOT_DEFINED, BeULong uIdle = 100 );

      /// \brief Sets the idle time
      void SetIdle(BeULong uIdle = 100);

      /// \brief Sets the stop timeout
      void SetOnStopTimeout( BeInt seconds );

      /// \brief Sets the queue size
      BeBoolean SetQueueSize( BeInt iSize );

      /// \brief Sets the thread priority
#if TARGET_WINDOWS
      void SetPriority(BeULong uPriority = THREAD_PRIORITY_NORMAL);
#else
      void SetPriority(BeULong uPriority = 0);
#endif

    // }



    // \brief Methods
    // {

      /// \brief Starts the thread
      BeBoolean Start(void);

      /// \brief Stops the thread
      BeBoolean Stop(void);

      /// \brief Blocks the thread until is finished
      void WaitTillExit(void);

      /// \brief Pings the thread
      BeBoolean PingThread( BeULong uTimeout = 0 );

      /// \brief Called when an event occurs
      virtual BeBoolean OnTask(BeVoidP lpvData) = 0;

      /// \brief Called when a time interval has elapsed
      virtual BeBoolean OnTask(void) = 0;

      /// \brief Sets the event
      BeBoolean Event(BeVoidP lpvData = NULL);

      /// \brief Sets the event from a thread task
      //BeBoolean Event(BeThreadTask* pThreadTask);

      /// \brief Kernel process of the thread
      BeBoolean KernelProcess(void);

    // }



    /// \brief  returns the thread ID actually running in CPU
    static ThreadID      ThreadId();

    /// \brief  return TRUE if both thread IDs are equal
    static BeBoolean    ThreadIdsEqual( ThreadID* p1, ThreadID* p2 );

  protected:

    /// \brief  Mutex object
    BeThreadMutex  m_kMutex;         // mutex that protects threads internal data

  private:

    /// \brief  Creates the thread
    void CreateThreadInternal();

    /// \brief  Destroys the thread
    void DestroyThreadInternal();

    /// \brief    id of this thread
    ThreadID      m_iThreadID;
    /// \brief    thread type
    eThreadType   m_eType;
    /// \brief    current state of thread see thread state data structure.
    eThreadState  m_eState;
    /// \brief    set to TRUE if thread is running
    BeBoolean     m_bRunning;

    /// \brief    thread BeHandle
#if TARGET_WINDOWS
    BeHandle      m_thread;
#else
    pthread_t     m_thread;
#endif

    /// \brief    event controller
    BeThreadEvent m_kEvent;
    /// \brief    used for Sleep periods
    BeULong       m_uIdle;
    /// \brief    Specifies a timeout value for stop if a thread fails to stop within m_StopTimeout seconds an exception is thrown
    BeInt         m_iStopTimeout;

    /// \brief    task queue
    BeVoidP*      m_lppvQueue;
    /// \brief    queue depth
    BeInt         m_chQueue;
    /// \brief    current que possition
    BeInt         m_queuePos;
    /// \brief    data which is currently being processed
    BeVoidP       m_lpvProcessor;

    /// \brief    thread stack size
    BeULong       m_uStackSize;
    /// \brief    object condition
    BeULong       m_uObjectCondition;



    // \brief Queue process methods
    // {

      /// \brief  Push to queue
      BeBoolean   Push(BeVoidP lpv);

      /// \brief  Pop from queue
      BeBoolean   Pop(void);

      /// \brief  Returns TRUE if the queue is empty
      BeBoolean   IsEmpty(void);

      /// \brief  Return TRUE if the queue is in the limit
      BeBoolean   AtCapacity(void);

      /// \brief  Returns percent of the queue capacity used
      BeFloat     GetPercentCapacity(void);

    // }

};

#include <Threading/BeThread.inl>

#endif // #ifndef BE_BETHREAD_H

