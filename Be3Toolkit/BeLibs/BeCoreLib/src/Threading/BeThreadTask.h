/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeThreadTask.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BETHREADTASK_H
#define BE_BETHREADTASK_H

#include <BeCoreLib/Pch/BeCoreLibPredef.h>

#include <Threading/BeThreadMutex.h>
#include <Memory/BeMemoryObject.h>

class CORE_API BeThreadTask : public BeMemoryObject
{

  public:

    enum eThreadTaskStatus
    {
      TASK_STATUS_NOT_SUBMITTED,
      TASK_STATUS_WAITING_ON_QUEUE,
      TASK_STATUS_BEING_PROCESSED,
      TASK_STATUS_COMPLETED
    };

    /// \brief Constructor
    BeThreadTask(void);
    /// \brief Destructor
    ~BeThreadTask(void);



    // \brief Setters
    // {

      /// \brief Sets the task status
      void SetTaskStatus(eThreadTaskStatus eState);

      /// \brief Sets the task thread ID
      void SetId(ThreadID* pThreadID);

      /// \brief Copy the thread ID from the param
      void Thread(ThreadID* pThreadID);

    // }



    // \brief Getters
    // {

      /// \brief Returns the task status
      eThreadTaskStatus GetTaskStatus(void);  

    // }



    // \brief Methods
    // {

      /// \brief Makes the task to wait
      BeBoolean Wait(BeInt iTimeoutSeconds);

      /// \brief Overload method
      virtual BeBoolean Task(void) = 0;

    // }



  private:

    /// \brief The task status
    eThreadTaskStatus m_eState;

    /// \brief The task thread ID
    ThreadID m_eThreadID;

    /// \brief Thread mutex object
    BeThreadMutex m_kMutex;

};

#endif // #ifndef BE_BETHREADTASK_H

