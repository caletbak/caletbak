/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeThreadMutex.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <Threading/BeThreadMutex.h>
#include <Threading/BeThread.h>

/////////////////////////////////////////////////////////////////////////////

BeUInt BeThreadMutex::s_uInitialNativeThread = 0;

/////////////////////////////////////////////////////////////////////////////
BeThreadMutex::BeThreadMutex()
:
  m_bCreated(BE_TRUE)
{

#if TARGET_WINDOWS
  m_kMutex = CreateMutex( NULL, BE_FALSE, NULL );
  if( !m_kMutex ) m_bCreated = BE_FALSE;
#else
  pthread_mutexattr_t mattr;
  pthread_mutexattr_init( &mattr );
  pthread_mutex_init(&m_kMutex,&mattr);
#endif

  memset( &m_iOwnerThread, 0, sizeof(ThreadID) );

}
/////////////////////////////////////////////////////////////////////////////
BeThreadMutex::~BeThreadMutex()
{
#if TARGET_WINDOWS
  WaitForSingleObject(m_kMutex,INFINITE);
  CloseHandle(m_kMutex);
#else
  pthread_mutex_lock(&m_kMutex);
  pthread_mutex_unlock(&m_kMutex); 
  pthread_mutex_destroy(&m_kMutex);
#endif
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeBoolean BeThreadMutex::IsCreated(void)
{
  return m_bCreated;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeBoolean BeThreadMutex::Lock( BeBoolean bBlock, BeString8 const & strMutexName )
{
  
  BE_UNUSED(strMutexName);

  ThreadID id = BeThread::ThreadId();

  if( BeThread::ThreadIdsEqual( &m_iOwnerThread, &id ) )
  {
    BE_CORE_ERROR( "\n\tthe same thread can not acquire a mutex twice!" );
  }

  if ( !bBlock && m_iOwnerThread != 0 ) return BE_FALSE;

#if    TARGET_WINDOWS
  WaitForSingleObject(m_kMutex,INFINITE);
#else
  pthread_mutex_lock(&m_kMutex);
#endif

  m_iOwnerThread = BeThread::ThreadId();

  return BE_TRUE;
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeThreadMutex::UnLock( BeString8 const & strMutexName )
{

  BE_UNUSED(strMutexName);
  
  ThreadID id = BeThread::ThreadId();
  
  if( !BeThread::ThreadIdsEqual( &id, &m_iOwnerThread ) )
  {
    BE_CORE_ERROR( "   Fatal exception CMutexClass::Unlock   owner: %d    id: %d", m_iOwnerThread, id );
  }

  memset( &m_iOwnerThread, 0, sizeof(ThreadID) );

#if    TARGET_WINDOWS
  ReleaseMutex( m_kMutex );
#else
  pthread_mutex_unlock( &m_kMutex );
#endif

  return BE_TRUE;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeInt BeThreadMutex::GetCurrentThreadIdentifier()
{
#if defined(TARGET_ANDROID)
    BeInt iCallingThread = pthread_self();
#elif defined(TARGET_IOS)
    BeInt iCallingThread = pthread_mach_thread_np(pthread_self());
#else
    BeInt iCallingThread = GetCurrentThreadId();
#endif

    return iCallingThread;
}
/////////////////////////////////////////////////////////////////////////////

