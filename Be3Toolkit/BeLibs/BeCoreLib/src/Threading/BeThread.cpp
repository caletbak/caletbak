/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeThread.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <Threading/BeThread.h>
#include <Threading/BeThreadTask.h>

/////////////////////////////////////////////////////////////////////////////
const BeShort BeThread::NO_ERRORS            = 0;
const BeShort BeThread::MUTEX_CREATION          = 0x01;
const BeShort BeThread::EVENT_CREATION          = 0x02;
const BeShort BeThread::THREAD_CREATION          = 0x04;
const BeShort BeThread::UNKNOWN              = 0x08;
const BeShort BeThread::ILLEGAL_USE_OF_EVENT      = 0x10;
const BeShort BeThread::MEMORY_FAULT          = 0x20;
const BeShort BeThread::EVENT_AND_TYPE_DONT_MATCH    = 0x40;
const BeShort BeThread::STACK_OVERFLOW          = 0x80;
const BeShort BeThread::STACK_EMPTY            = 0x100;
const BeShort BeThread::STACK_FULL            = 0x200;
/////////////////////////////////////////////////////////////////////////////
ThreadID BeThread::ThreadId()
{
  ThreadID thisThreadsId;
#if defined(AS400) || defined(OS400)
    pthread_t thread;
#endif

#if TARGET_WINDOWS
  thisThreadsId = (ThreadID)GetCurrentThreadId();
#else

#   if defined(AS400) || defined(OS400)
    thread = pthread_self();
    pthread_getunique_np(&thread, &thisThreadsId);
#   elif defined(ALPHA) || defined(DEC) || defined(VMS)
#       ifdef VMS
    thisThreadsId = pthread_self();
#       else
    thisThreadsId = pthread_getsequence_np(pthread_self());
#       endif
#   else
#       if TARGET_IOS
        thisThreadsId = pthread_mach_thread_np(pthread_self());
#       else
    thisThreadsId = pthread_self();
#       endif
#   endif
#endif

  return thisThreadsId;
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeThread::ThreadIdsEqual( ThreadID* p1, ThreadID* p2 )
{
#if defined(AS400)||defined(OS400)
    return( ( memcmp( p1, p2, sizeof(ThreadID) ) == 0 ) ? BE_TRUE : BE_FALSE );
#elif defined(VMS) 
    return ( ( pthread_equal( *p1, *p2 ) ) ? BE_TRUE : BE_FALSE );
#else
    return ( (*p1 == *p2) ? BE_TRUE : BE_FALSE);
#endif
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
#ifdef USE_BEGIN_THREAD
  #include <process.h>
#endif

#if (!TARGET_WINDOWS && !TARGET_ANDROID)
  extern "C"
  {
    int  usleep(useconds_t useconds);
#ifdef NANO_SECOND_SLEEP
      int   nanosleep(const struct timespec *rqtp, struct timespec *rmtp);
#endif
  }

  void Sleep( unsigned int milli )
  {
#ifdef NANO_SECOND_SLEEP
      struct timespec interval, remainder;
      milli = milli * 1000000;
      interval.tv_sec= 0;
      interval.tv_nsec=milli;
      nanosleep(&interval,&remainder);
#else
      usleep(milli*1000);
#endif  
  }
#endif

//#include <iostream>
//using namespace std;


#if TARGET_WINDOWS
  #ifdef USE_BEGIN_THREAD
    unsigned __stdcall
  #else
    DWORD WINAPI
  #endif
#else
  BeVoidP
#endif
_THKERNEL( BeVoidP lpvData )
{

  BeThread* pThread = (BeThread*)lpvData;
  BeThread::eThreadType eLastType;
  
    pThread->m_kMutex.Lock();
    pThread->m_eState = BeThread::THREAD_STATE_WAITING;
    pThread->m_bRunning = BE_TRUE;
#if !TARGET_WINDOWS
    pThread->m_iThreadID = BeThread::ThreadId();
#endif
  pThread->m_kMutex.UnLock();
  
  for( ;; )
  {

    eLastType = pThread->m_eType;

    if( eLastType == BeThread::THREAD_TYPE_HOMOGENEOUS || eLastType == BeThread::THREAD_TYPE_SPECIALIZED || eLastType == BeThread::THREAD_TYPE_NOT_DEFINED )
    {
      if( !pThread->m_kEvent.Wait()  ) break;
      pThread->m_kEvent.Reset();
    }
  
    if( ! pThread->KernelProcess() ) break;
    
    //if( eLastType == BeThread::THREAD_TYPE_HOMOGENEOUS || eLastType == BeThread::THREAD_TYPE_SPECIALIZED || eLastType == BeThread::THREAD_TYPE_NOT_DEFINED )
    //{
    //  pThread->m_kEvent.Reset();
    //}

    if( pThread->m_eType == BeThread::THREAD_TYPE_INTERVAL_DRIVEN )
    {
#if      TARGET_WINDOWS
      Sleep(pThread->m_uIdle);
#else
      sleep(pThread->m_uIdle);
#endif
    }

  }
  
  pThread->m_kMutex.Lock();
    pThread->m_eState = BeThread::THREAD_STATE_DOWN;
    pThread->m_bRunning = BE_FALSE;
  pThread->m_kMutex.UnLock();
  
#if TARGET_WINDOWS
  return 0;
#else
  return (BeVoidP)0;
#endif

}
/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
BeThread::BeThread(void)
:
  m_bRunning(BE_FALSE),
#if TARGET_WINDOWS
  m_thread(NULL),
#endif
  m_iThreadID(0L),
  m_eState(THREAD_STATE_DOWN),
  m_uIdle(100),
  m_lppvQueue(NULL),
  m_lpvProcessor(NULL),
  m_chQueue(QUEUE_SIZE),
  m_eType(THREAD_TYPE_NOT_DEFINED),
  m_uStackSize(DEFAULT_STACK_SIZE),
  m_queuePos(0),
  m_iStopTimeout(1)
{
  CreateThreadInternal();
}
/////////////////////////////////////////////////////////////////////////////
BeThread::~BeThread(void)
{
  DestroyThreadInternal();
}
/////////////////////////////////////////////////////////////////////////////
void BeThread::CreateThreadInternal()
{

  // We set the thread type and interval
  SetThreadType( BeThread::THREAD_TYPE_HOMOGENEOUS, 0 );
  
  m_uObjectCondition = NO_ERRORS;

  m_lppvQueue = BeNew BeVoidP [QUEUE_SIZE];

  if( !m_lppvQueue ) 
  {
    m_uObjectCondition |= MEMORY_FAULT;
    m_eState = THREAD_STATE_FAULT;
    return;
  }
    
  if( !m_kMutex.IsCreated() )
  {
    perror("mutex creation failed");
    m_uObjectCondition |= MUTEX_CREATION;
    m_eState = THREAD_STATE_FAULT;
    return;
  }

  if( !m_kEvent.IsCreated() )
  {
    perror("event creation failed");
    m_uObjectCondition |= EVENT_CREATION;
    m_eState = THREAD_STATE_FAULT;
    return;
  }
  
  Start();
}
/////////////////////////////////////////////////////////////////////////////
void BeThread::DestroyThreadInternal()
{
  if( m_bRunning ) // gracefull termination
  {
    if( !Stop() )
    {
      BE_CORE_ERROR("\n\tthread failed to stop in a timely manner!");
    }
  }

#if TARGET_WINDOWS
  CloseHandle(m_thread);
#endif

  BeDeleteArray m_lppvQueue;
}
/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
BeBoolean BeThread::Start(void)
{
  if( IsFromSameThread() )
  {
    BE_CORE_ERROR("\n\tit is illegal for a thread to attempt to start itself!");
  }


  m_kMutex.Lock();
  if( m_bRunning ) 
  {
    m_kMutex.UnLock();
    return BE_TRUE;
  }
  m_kMutex.UnLock();
  
  if( m_uObjectCondition & THREAD_CREATION )
    m_uObjectCondition = m_uObjectCondition ^ THREAD_CREATION;

#if      TARGET_WINDOWS

    if( m_thread ) CloseHandle(m_thread);
    #ifdef USE_BEGIN_THREAD
      m_thread = (BeHandle )_beginthreadex( NULL, (BeUInt)m_uStackSize, _THKERNEL, (BeVoidP)this, 0, &m_iThreadID);
    #else
      m_thread = CreateThread( NULL, m_uStackSize, _THKERNEL, (BeVoidP)this, 0, &m_iThreadID );
    #endif

    if( !m_thread )
    {
      perror("thread creation failed");
      m_uObjectCondition |= THREAD_CREATION;
      m_eState = THREAD_STATE_FAULT;
      return BE_FALSE;
    }

#else
  
    pthread_attr_t attr;
    pthread_attr_init(&attr);

#if        VMS
      if( m_uStackSize == 0 )
        pthread_attr_setstacksize( &attr, PTHREAD_STACK_MIN*10 );
#endif
      if( m_uStackSize != 0 )
        pthread_attr_setstacksize( &attr, m_uStackSize );

      BeInt error = pthread_create( &m_thread, &attr, _THKERNEL, (BeVoidP)this );

      if( error != 0 )
      {
        m_uObjectCondition |= THREAD_CREATION;
        m_eState = THREAD_STATE_FAULT;

#if          defined(HPUX) || defined(SUNOS) || defined(LINUX)
          switch(error)
          {

            case EINVAL:
              //cerr << "error: attr in an invalid thread attributes object\n";
              break;
            case EAGAIN:
              //cerr << "error: the necessary resources to create a thread are not\n";
              //cerr << "available.\n";
              break;
            case EPERM:
              //cerr << "error: the caller does not have the privileges to create\n";
              //cerr << "the thread with the specified attr object.\n";
              break;

#if                defined(HPUX)
              case ENOSYS:

                //cerr << "error: pthread_create not implemented!\n";
                if( __is_threadlib_linked()==0 )
                {
                  //cerr << "error: threaded library not being used, improper linkage \"-lpthread -lc\"!\n";
                }
                break;
#endif

            default:
              //cerr << "error: an unknown error was encountered attempting to create\n";
              //cerr << "the requested thread.\n";
              break;
          }
#else
          //cerr << "error: could not create thread, pthread_create failed (" << error << ")!\n";
#endif

        return BE_FALSE;  
      }
#endif

  return BE_TRUE;
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeThread::Stop(void)
{

  if( IsFromSameThread() )
  {
    BE_CORE_ERROR("\n\tIt is illegal for a thread to attempt to signal itself to stop!");
  }

  m_kMutex.Lock();
  m_bRunning = BE_FALSE;
  m_kMutex.UnLock();
  m_kEvent.Set();

  BeInt ticks = (m_iStopTimeout * 1000) / 100;

  for( BeInt i = 0; i < ticks; i++ )
  {
#if    TARGET_WINDOWS
    Sleep(100);
#else
    sleep(100);
#endif

    m_kMutex.Lock();
    if( m_eState == THREAD_STATE_DOWN )
    {
      m_kMutex.UnLock();
      return BE_TRUE;
    }
    m_kMutex.UnLock();

  } 

  return BE_FALSE;
}
/////////////////////////////////////////////////////////////////////////////
void BeThread::WaitTillExit(void)
{
  
  if( IsFromSameThread() )
  {
    BE_CORE_ERROR("\n\tthis function can not be called from within the same thread!");
  }
  
  if( !m_bRunning ) return;

#if    TARGET_WINDOWS
  WaitForSingleObject(m_thread,INFINITE);
#else
  BeVoidP lpv;
  pthread_join(m_thread,&lpv);
#endif

}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeThread::PingThread(BeULong uTimeout)
{

    BeULong uTotal = 0;

  BeBoolean bReturn = BE_FALSE;

  for( ;; )
  {

    if( uTotal > uTimeout && uTimeout > 0 )
    {
      bReturn = BE_FALSE;
      break;
    }

    m_kMutex.Lock();
      if( m_bRunning )
      {
        m_kMutex.UnLock();
        bReturn = BE_TRUE;
        m_kMutex.UnLock();
        break;
      }
      uTotal += m_uIdle;
    m_kMutex.UnLock();

#if    TARGET_WINDOWS
    Sleep(m_uIdle);
#else
    sleep(m_uIdle);
#endif

  }

  return bReturn;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeBoolean BeThread::OnTask( BeVoidP lpvData )
{

    BE_ASSERT(lpvData && m_eType == THREAD_TYPE_HOMOGENEOUS);

  if( m_eType != THREAD_TYPE_HOMOGENEOUS )
  {
    //cerr << "Warning CThread::OnTask:\n\tOnTask(BeVoidP) called for a non-homogeneous thread!\n";
    return BE_FALSE;
  }

  ((BeThreadTask *)lpvData)->SetTaskStatus(BeThreadTask::TASK_STATUS_BEING_PROCESSED);
  BeBoolean bReturn = ((BeThreadTask *)lpvData)->Task();
  ((BeThreadTask *)lpvData)->SetTaskStatus(BeThreadTask::TASK_STATUS_COMPLETED);

  return bReturn; 

} 
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeThread::OnTask(void)
{

  BE_ASSERT(m_eType == THREAD_TYPE_INTERVAL_DRIVEN);

  if( m_eType != THREAD_TYPE_INTERVAL_DRIVEN )
  {
    //cerr << "Warning CThread::OnTask:\n\tOnTask() called for a non-event driven thread!\n";
    return BE_FALSE;
  }

  printf("\nthread is alive\n");

  return BE_TRUE;
}
/////////////////////////////////////////////////////////////////////////////
/*
BeBoolean BeThread::Event( BeThreadTask* pThreadTask )
{
  
  m_kMutex.Lock();

  BE_ASSERT( m_eType == THREAD_TYPE_HOMOGENEOUS || m_eType == THREAD_TYPE_NOT_DEFINED );

  try 
  {

    if( IsFromSameThread() )
    {
      BE_CORE_ERROR("\n\tit is illegal for a thread to place an event on its own event stack!");
    }
    
    // make sure that the thread is running 
    if( !m_bRunning && m_uObjectCondition == NO_ERRORS )
    {
      m_kMutex.UnLock();
      PingThread(m_uIdle * 2); // wait two idle cycles for it to start
      m_kMutex.Lock();
    }
    if( !m_bRunning ) // if it is not running return BE_FALSE;
    {
      m_kMutex.UnLock();
      return BE_FALSE;
    }
    
    if( m_uObjectCondition & ILLEGAL_USE_OF_EVENT )
      m_uObjectCondition = m_uObjectCondition ^ ILLEGAL_USE_OF_EVENT;

    if( m_uObjectCondition & EVENT_AND_TYPE_DONT_MATCH)
      m_uObjectCondition = m_uObjectCondition ^ EVENT_AND_TYPE_DONT_MATCH;

    if( m_eType != THREAD_TYPE_HOMOGENEOUS &&  m_eType != THREAD_TYPE_NOT_DEFINED )
    {
      m_kMutex.UnLock();
      m_uObjectCondition |= ILLEGAL_USE_OF_EVENT;
      m_uObjectCondition |= EVENT_AND_TYPE_DONT_MATCH;
      m_eState = THREAD_STATE_FAULT;
      //cerr << "Warning: invalid call to CEvent::Event(CTask *), thread type is not specialized\n";

      return BE_FALSE;
    }

    m_eType = THREAD_TYPE_HOMOGENEOUS;
    m_kMutex.UnLock();

    pThreadTask->SetId( &m_iThreadID );
    if( !Push( (BeVoidP)pThreadTask ) )
      return BE_FALSE;

    pThreadTask->SetTaskStatus(BeThreadTask::TASK_STATUS_WAITING_ON_QUEUE);
    m_kEvent.Set();

  }
  catch (char *psz)
  {
#if    TARGET_WINDOWS
    MessageBoxA(NULL,&psz[2],"Fatal exception CThread::CEvent",MB_ICONHAND);
    exit(-1);
#else
    //cerr << "Fatal exception CThread::CEvent(CTask *pvTask):" << psz;
#endif
  }

  return BE_TRUE;

}
*/
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeThread::Event( BeVoidP lpvData )
{

  m_kMutex.Lock();
  BE_ASSERT( m_eType == THREAD_TYPE_SPECIALIZED ||  m_eType == THREAD_TYPE_NOT_DEFINED );

  if( IsFromSameThread() )
  {
    BE_CORE_ERROR("\n\tit is illegal for a thread to place an event on its own event stack!");
  }

  // make sure that the thread is running 
  if( !m_bRunning && m_uObjectCondition == NO_ERRORS )
  {
    m_kMutex.UnLock();
    PingThread( m_uIdle*2 ); // wait two idle cycles for it to start
    m_kMutex.Lock();
  }
  if( !m_bRunning ) // if it is not running return BE_FALSE;
  {
    m_kMutex.UnLock();
    return BE_FALSE;
  }

  if( m_uObjectCondition & ILLEGAL_USE_OF_EVENT )
    m_uObjectCondition = m_uObjectCondition ^ ILLEGAL_USE_OF_EVENT;

  if( m_uObjectCondition & EVENT_AND_TYPE_DONT_MATCH)
    m_uObjectCondition = m_uObjectCondition ^ EVENT_AND_TYPE_DONT_MATCH;

  if( m_eType != THREAD_TYPE_SPECIALIZED && m_eType != THREAD_TYPE_NOT_DEFINED )
  {
    m_uObjectCondition |= ILLEGAL_USE_OF_EVENT;
    m_uObjectCondition |= EVENT_AND_TYPE_DONT_MATCH;
    //cerr << "Warning: invalid call to CEvent::Event(BeVoidP), thread type is not specialized\n";
    m_kMutex.UnLock();
    return BE_FALSE;
  }
  m_eType = THREAD_TYPE_SPECIALIZED;

  m_kMutex.UnLock();
  if( !Push(lpvData) )
  {
    return BE_FALSE;
  }

  m_kEvent.Set();

  return BE_TRUE;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeBoolean BeThread::IsFromSameThread()
{
  
  ThreadID id = ThreadId();
  
  if( ThreadIdsEqual( &id, &m_iThreadID ) ) return BE_TRUE;

  return BE_FALSE;

}
/////////////////////////////////////////////////////////////////////////////
void BeThread::GetId(ThreadID* pId)
{
  memcpy( pId, &m_iThreadID, sizeof(ThreadID) );
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeThread::IsEmpty()
{
  
  m_kMutex.Lock();
  if( m_queuePos <= 0 )
  {
    m_kMutex.UnLock();
    return BE_TRUE;
  }
  m_kMutex.UnLock();

  return BE_FALSE;

}
/////////////////////////////////////////////////////////////////////////////
BeThread::eThreadState BeThread::GetThreadState(void)
{
  
  eThreadState currentState;

  m_kMutex.Lock();
    currentState = m_eState;
  m_kMutex.UnLock();

  return currentState;
}
/////////////////////////////////////////////////////////////////////////////
BeULong BeThread::GetErrorFlags(void)
{
  return m_uObjectCondition;
}
/////////////////////////////////////////////////////////////////////////////
BeUInt BeThread::GetEventsPending()
{

  BeUInt uEventsWaiting;

  m_kMutex.Lock();
    uEventsWaiting = m_queuePos;
    m_kMutex.UnLock();

  return uEventsWaiting;

}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeThread::SetPriority(BeULong uPriority)
{
#if TARGET_WINDOWS
  SetThreadPriority( m_thread, uPriority );
#endif
}
/////////////////////////////////////////////////////////////////////////////
void BeThread::SetThreadType(eThreadType eType, BeULong uIdle)
{

  if( IsFromSameThread() )
  {
    BE_CORE_ERROR("\n\tit is illegal for a thread to change its own type!");
  }

  m_kMutex.Lock();
  m_uIdle = uIdle;

  if( m_eType == eType )
  {
    m_kMutex.UnLock();
    return;
  }

  if( m_uObjectCondition & ILLEGAL_USE_OF_EVENT )
    m_uObjectCondition = m_uObjectCondition ^ ILLEGAL_USE_OF_EVENT;

  if( m_uObjectCondition & EVENT_AND_TYPE_DONT_MATCH )
    m_uObjectCondition = m_uObjectCondition ^ EVENT_AND_TYPE_DONT_MATCH;

  m_eType = eType;


  m_kMutex.UnLock();
  m_kEvent.Set();

}
/////////////////////////////////////////////////////////////////////////////
void BeThread::SetIdle(BeULong uIdle)
{
  m_kMutex.Lock();
    m_uIdle = uIdle;
  m_kMutex.UnLock();
}
/////////////////////////////////////////////////////////////////////////////
void BeThread::SetOnStopTimeout( BeInt iSeconds )
{
  m_iStopTimeout = iSeconds; 
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeThread::SetQueueSize( BeInt iSize )
{

  BeVoidP * newQueue = NULL;

  m_kMutex.Lock();

  BE_ASSERT(iSize > m_queuePos);

  if( iSize <= m_queuePos )
  {
    //cerr << "Warning CThread::SetQueueSize:\n\tthe new queue size is less than the number of tasks on a non-empty queue! Request ignored.\n";
    m_kMutex.UnLock();
    return BE_FALSE;
  }

  newQueue = BeNew BeVoidP [iSize];
  if( !newQueue )
  {
    //cerr << "Warning CThread::SetQueueSize:\n\ta low memory, could not reallocate queue!\n";
    m_kMutex.UnLock();
    return BE_FALSE;
  }

  for(BeInt i=0;i<m_queuePos; i++)
  {
    newQueue[i] = m_lppvQueue[i];
  }

  BeDeleteArray m_lppvQueue;

  m_chQueue = iSize;
  m_lppvQueue = newQueue;

  m_kMutex.UnLock();

  return BE_TRUE;

}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeBoolean BeThread::Push( BeVoidP lpv )
{

  if( !lpv ) return BE_TRUE;

  m_kMutex.Lock();

  if( m_queuePos+1 >= m_chQueue )
  {
    m_uObjectCondition |= STACK_OVERFLOW;
    m_kMutex.UnLock();
    return BE_FALSE;
  }
  
  if( m_uObjectCondition & STACK_EMPTY    )
    m_uObjectCondition = m_uObjectCondition ^ STACK_EMPTY;

  if( m_uObjectCondition & STACK_OVERFLOW ) 
    m_uObjectCondition = m_uObjectCondition ^ STACK_OVERFLOW;

  m_lppvQueue[m_queuePos++] = lpv;
  if( m_queuePos+1 >= m_chQueue )
    m_uObjectCondition |= STACK_FULL;

  m_kMutex.UnLock();

  return BE_TRUE;

}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeThread::Pop()
{

  m_kMutex.Lock();
  if( m_queuePos-1 < 0 )
  {
    m_queuePos = 0;
    m_uObjectCondition |= STACK_EMPTY;
    m_kMutex.UnLock();
    return BE_FALSE;
  }

  if( m_uObjectCondition & STACK_EMPTY )
    m_uObjectCondition = m_uObjectCondition ^ STACK_EMPTY;

  if( m_uObjectCondition & STACK_OVERFLOW )
    m_uObjectCondition = m_uObjectCondition ^ STACK_OVERFLOW;

  if( m_uObjectCondition & STACK_FULL )
    m_uObjectCondition = m_uObjectCondition ^ STACK_FULL;

  m_queuePos--;
  m_lpvProcessor = m_lppvQueue[m_queuePos];
  m_kMutex.UnLock();

  return BE_TRUE;

}
/////////////////////////////////////////////////////////////////////////////
BeFloat BeThread::GetPercentCapacity()
{
  
  float fValue = 0;

  m_kMutex.Lock();
    fValue = (float)m_queuePos / m_chQueue;
  m_kMutex.UnLock();

  return fValue;
}
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeThread::AtCapacity()
{

  m_kMutex.Lock();

    if( ((m_uObjectCondition & STACK_OVERFLOW || m_uObjectCondition & STACK_FULL ) &&  m_eState == THREAD_STATE_BUSY) || !m_bRunning)
    {
      m_kMutex.UnLock();
      return BE_TRUE;
    }

  m_kMutex.UnLock();

  return BE_FALSE;

}
/////////////////////////////////////////////////////////////////////////////


    
/////////////////////////////////////////////////////////////////////////////
BeBoolean BeThread::KernelProcess()
{

  m_kMutex.Lock();
  m_eState = THREAD_STATE_BUSY;
  if( !m_bRunning )
  {
    m_eState = THREAD_STATE_SHUTTING_DOWN;
    m_kMutex.UnLock();
    return BE_FALSE;
  }
  m_kMutex.UnLock();

  if( !IsEmpty() )
  {
    while( !IsEmpty() )
    {
      Pop();
      if( !OnTask(m_lpvProcessor) )
      {
        m_kMutex.Lock();
        m_lpvProcessor = NULL;
        m_eState = THREAD_STATE_SHUTTING_DOWN;
        m_kMutex.UnLock();
        return BE_FALSE;
      }
    }
    m_kMutex.Lock();
    m_lpvProcessor = NULL;
    m_eState = THREAD_STATE_WAITING;
  }
  else
  {
    if( !OnTask() )
    {
      m_kMutex.Lock();
      m_eState = THREAD_STATE_SHUTTING_DOWN;
      m_kMutex.UnLock();
      return BE_FALSE;
    }
    m_kMutex.Lock();
    m_eState = THREAD_STATE_WAITING;
  }

  m_kMutex.UnLock();

  return BE_TRUE;
}
/////////////////////////////////////////////////////////////////////////////

