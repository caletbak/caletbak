/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeThreadEvent.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <Threading/BeThreadEvent.h>
#include <Threading/BeThread.h>

/////////////////////////////////////////////////////////////////////////////
BeThreadEvent::BeThreadEvent(void)
:
  m_bCreated(BE_TRUE)
{

  memset( &m_iOwnerThread, 0, sizeof(ThreadID) );

#if TARGET_WINDOWS
  m_pEvent = CreateEvent( NULL, BE_FALSE, BE_FALSE, NULL );
  if( !m_pEvent )
  {
    m_bCreated = FALSE;
  }
#else
  pthread_mutexattr_t mattr;

  pthread_mutexattr_init(&mattr);
  pthread_mutex_init(&m_lock,&mattr);
  pthread_cond_init(&m_ready,NULL);
#endif  

}
/////////////////////////////////////////////////////////////////////////////
BeThreadEvent::~BeThreadEvent(void)
{
#if TARGET_WINDOWS
  CloseHandle(m_pEvent);
#else
  pthread_cond_destroy(&m_ready);
  pthread_mutex_destroy(&m_lock);
#endif
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
void BeThreadEvent::Set()
{
#if TARGET_WINDOWS
  SetEvent(m_pEvent);
#else
  pthread_cond_signal(&m_ready);
#endif
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeBoolean BeThreadEvent::IsCreated(void)
{
  return m_bCreated;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeBoolean BeThreadEvent::Wait()
{

  ThreadID id = BeThread::ThreadId();
  if( BeThread::ThreadIdsEqual( &id, &m_iOwnerThread ) )
  {
    BE_CORE_ERROR("\n\tinvalid Wait call, Wait can not be called more than once\n\twithout a corresponding call to Reset!");
  }

  ThreadID zero;
  memset( &zero, 0, sizeof(ThreadID) );
  if( memcmp( &zero, &m_iOwnerThread, sizeof(ThreadID) ) != 0 )
  {
    BE_CORE_ERROR("\n\tanother thread is already waiting on this event!");
  }

  m_iOwnerThread = BeThread::ThreadId();
#if    TARGET_WINDOWS
  if( WaitForSingleObject(m_pEvent,INFINITE) != WAIT_OBJECT_0 )
  {
    return BE_FALSE;
  }
#else
  pthread_mutex_lock(&m_lock);
  pthread_cond_wait(&m_ready,&m_lock);
  return BE_TRUE;
#endif

  return BE_TRUE;

}
/////////////////////////////////////////////////////////////////////////////
void BeThreadEvent::Reset()
{

  ThreadID id = BeThread::ThreadId();
  if( !BeThread::ThreadIdsEqual( &id, &m_iOwnerThread ) )
  {
    BE_CORE_ERROR("\n\tunbalanced call to Reset, Reset must be called from\n\n\tthe same Wait-Reset pair!");
  }

  memset( &m_iOwnerThread, 0, sizeof(ThreadID) );

#if    !TARGET_WINDOWS
  pthread_mutex_unlock(&m_lock);
#endif

}
/////////////////////////////////////////////////////////////////////////////


