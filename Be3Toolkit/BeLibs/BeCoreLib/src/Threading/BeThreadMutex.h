/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeThreadMutex.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BETHREADMUTEX_H
#define BE_BETHREADMUTEX_H

#include <BeCoreLib/Pch/BeCoreLibPredef.h>

#include <STL/BeString.h>
#include <Memory/BeMemoryObject.h>

#if !TARGET_WINDOWS
  #include <pthread.h>
#endif

//#include "BeThread.h"
//#define BE_SYNCHRONIZED(x)  for(Lock x##_Lock = x; x##_Lock; x##_Lock.SetUnlock())

/////////////////////////////////////////////////
/// \class BeMutex
/// \brief This class implements a mutex
/////////////////////////////////////////////////
class CORE_API BeThreadMutex : public BeMemoryObject
{

  public:

    /// \brief Constructor
    BeThreadMutex();
    /// \brief Destructor
    ~BeThreadMutex();

    /// \brief Locks the mutex
    BeBoolean Lock( BeBoolean bBlock = BE_TRUE, BeString8 const & strMutexName = BeString8::EMPTY() );
    /// \brief UnLocks the mutex
    BeBoolean UnLock( BeString8 const & strMutexName = BeString8::EMPTY() );

    /// \brief Return TRUE if the mutex has been created
    BeBoolean IsCreated(void);



    static BeInt GetCurrentThreadIdentifier();

    static BeUInt s_uInitialNativeThread;

  private:

#if TARGET_WINDOWS
    HANDLE m_kMutex;
#else
    pthread_mutex_t m_kMutex;
#endif

    BeBoolean m_bCreated;
    ThreadID m_iOwnerThread;

#if TARGET_WINDOWS
    CRITICAL_SECTION m_criticalSection;
#endif

};


/*
class CORE_API SynchronizedLock
{

  public:

  Lock(CMutexSync &mutex) : m_mutex(mutex), m_locked(true)
  {
    mutex.lock();
  }

  //the destructor

  ~Lock()
  {
    m_mutex.unlock();
  }

  //report the state of locking when used as a boolean

  operator bool () const
  {
    return m_locked;
  }

  //unlock

  void setUnlock()
  {
    m_locked = false;        
  }

private:
  CMutexSync &m_mutex;
  bool m_locked;
};
*/

#endif // #ifndef BE_BETHREADMUTEX_H

