/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeCoreTypes.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BECORETYPES_H
#define BE_BECORETYPES_H

#include <BeCoreLib/Pch/BeCoreLibPredef.h>

#include <Memory/BeMemoryObject.h>
#include <STL/BeString.h>

/////////////////////////////////////////////////
/// \class BeCoreTypes
/// \brief Core project types
/////////////////////////////////////////////////
class CORE_API BeCoreTypes
{

  public:

    /// \brief  Resource events to be done in rendering processes
    enum ELoadResourceEvents
    {
      LOAD_RESOURCE_FILE,
      LOAD_RESOURCE_SCENE_DATA,
      LOAD_RESOURCE_MODEL_DATA,
      LOAD_RESOURCE_TEXTURE_DATA,
      
      LOAD_RESOURCE_TYPE_COUNT
    };

    /// \brief  Resource events to be done in update processes
    enum EResourceFinishEvents
    {
      DATA_TEXTURE_TO_DEVICE,

      RESOURCE_FINISH_EVENTS_COUNT
    };

    /// \brief  Type names
    enum ETypeNames
    {
        BE_CHAR8 = 0,
        BE_UNSIGNED_CHAR8,
        BE_CHAR16,
        BE_UNSIGNED_CHAR16,
        BE_SHORT,
        BE_UNSIGNED_SHORT,
        BE_INT,
        BE_UNSIGNED_INT,
        BE_FLOAT,
        BE_DOUBLE,
        BE_BOOLEAN,
        BE_VOID
    };
    static BeString8 GetTypeNameString(ETypeNames eType);

    /// \brief  Resources interface
    class CORE_API ResourcesInterface : public BeMemoryObject
    {
      public:

        /// \brief  Called when the ResourcesManager returns the loaded data to be processed
        virtual void ProcessLoadedEvent(const BeString16& strFileName, BeVoidP pBufferData, BeInt iBufferSize, int iExtraParam1) = 0;
    };

};

#endif // #ifndef BE_BECORETYPES_H
