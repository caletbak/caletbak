/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BakEngine3Common.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BAKENGINE3COMMON_H
#define BAKENGINE3COMMON_H

#if TARGET_WINDOWS

//#include <targetver.h>
#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers

// Windows Header Files:
#include <windows.h>
#include <tchar.h>

// Visual Leak detector
#ifdef _DEBUG
#  ifndef DYNAMIC_LINK
#    include <BeCommon/vld.h>
#  endif
#endif

#if !WINDOWS
#  if defined(WIN32) || defined(WIN64)
#    define WINDOWS
#  endif
#endif

#endif

#include <memory>

// iOS Prefix Header
#if TARGET_IOS
#ifndef __IPHONE_3_0
#warning "This project uses features only available in iPhone SDK 3.0 and later."
#endif

#ifdef __OBJC__
#import <Availability.h>
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#endif

#define MULTIBYTE_STRINGS
#define ICROSSBOX_NATIVE
#define USE_OGL_ES2
#define BE_IOS
#elif TARGET_ANDROID
#ifndef MULTIBYTE_STRINGS
#define MULTIBYTE_STRINGS
#endif
#elif TARGET_WINDOWS
#ifndef MULTIBYTE_STRINGS
#define MULTIBYTE_STRINGS
#endif
#endif

// C RunTime Header Files
#if TARGET_IOS
#   include <malloc/malloc.h>
#else
#   include <malloc.h>
#endif
#include <stdio.h>

#if TARGET_WINDOWS
#  pragma warning(disable: 4100)
#  pragma warning(disable: 4189)
#  pragma warning(disable: 4201)
#  pragma warning(disable: 4316)
#  pragma warning(disable: 4324)
#  pragma warning(disable: 4389)
#  pragma warning(disable: 4505)

#  include <direct.h>
#  define GetCurrentDir _getcwd
#else
#  include <unistd.h>
#  define GetCurrentDir getcwd
#endif

#define BeChar8               char                // Character or small integer.  1byte  signed: -128 to 127
#define BeChar16              wchar_t             // Character or small integer.  2byte  signed: -32768 to 32767
#define BeUChar8              unsigned char       // Character or small integer.  1byte  unsigned: 0 to 255
#define BeUChar16             unsigned wchar_t    // Character or small integer.  1byte  unsigned: 0 to 65535
#define BeWChar               wchar_t             // Wide character.  2 or 4 bytes  1 wide character

#define BeShort               short               // Short Integer.  2bytes  signed: -32768 to 32767
#define BeUShort              unsigned short      // Short Integer.  2bytes  unsigned: 0 to 65535

#define BeInt                 int                 // Integer.  4bytes  signed: -2147483648 to 2147483647
#define BeUInt                unsigned int        // Integer.  4bytes  unsigned: 0 to 4294967295

#define BeInt8                char
#define BeInt16               short
#define BeInt32               int

#define BeUInt8               unsigned char
#define BeUInt16              unsigned short
#define BeUInt32              unsigned int

#define BeLong                long                // Long integer.  4bytes  signed: -2147483648 to 2147483647
#define BeULong               unsigned long       // Long integer.  4bytes  unsigned: 0 to 4294967295

#define BeFloat               float               // Floating point number.  4bytes  +/- 3.4e +/- 38 (~7 digits)
#define BeDouble              double              // Double precision floating point number.  8bytes  +/- 1.7e +/- 308 (~15 digits)
#define BeLongDouble          long double         // Long double precision floating point number.  8bytes  +/- 1.7e +/- 308 (~15 digits)

#define BeBoolean             bool                // Boolean value. It can take one of two values: true or false.  1byte  true or false

#define BeVoidP               void*
#define BeHandle              void*
#if TARGET_WINDOWS
#define BeInstance            HINSTANCE
#define BeAccTableHandle      HACCEL
#define BeWindowHandle        HWND
#define BeWParam              WPARAM
#define BeLParam              LPARAM
#define BeLResult             LRESULT
#define BeWindowProcess       WNDPROC
#else
#define BeWindowHandle        BeHandle
#define BeInstance            BeHandle
/*
#define BeAccTableHandle      BeHandle
#define BeWParam              BeUInt32*
#define BeLParam              BeLong*
#define BeLResult             BeLong*
#define CALLBACK              __stdcall
typedef BeLResult (CALLBACK* BeWindowProcess)(BeWindowHandle, BeUInt32, BeWParam, BeLParam);
*/
#endif

#include <assert.h>
#define BE_ASSERT             assert
#define BE_ASSERT_PTR(x)      _Assert_PTR(x)

#define BE_TRUE               true
#define BE_FALSE              false

#define BE_FAILED(x)          ((x)<0)

#define BE_UNUSED(x)          x
#define BE_MAX_LENGTH         2048

#define BeSize_T              size_t
#define BeNew                 new
#define BeDelete              delete
#define BeDeleteArray         delete[]
#define BeAllocateMemory      malloc
#define BeFreeMemory          free

#if TARGET_WINDOWS
#  pragma warning(disable: 4251)
#endif

#define MAX_LENGTH              2048

#include <string>

typedef std::string           BeStringBase8;
typedef std::wstring          BeStringBase16;


#include <list>
#include <map>


/////////////////////////////////////////////////////////////////////////////
// DEBUG GLOBAL FUNCTIONS
/////////////////////////////////////////////////////////////////////////////

#ifdef TARGET_WINDOWS
#define VSPRINTF	vsprintf_s

#define VSNPRINTF(buffer, size, format, args)   \
    vsnprintf_s(buffer, size, _TRUNCATE, format, args)
#else
#define VSPRINTF	vsprintf
#define VSNPRINTF   vsnprintf
#endif

/////////////////////////////////////////////////////////////////////////////
// The following macro "NoEmptyFile()" can be put into a file
// in order suppress the MS Visual C++ Linker warning 4221
//
// warning LNK4221: no public symbols found; archive member will be inaccessible
//
// This warning occurs on PC and XBOX when a file compiles out completely
// has no externally visible symbols which may be dependant on configuration
// #defines and options.

#define BeNoEmptyFile()   namespace { char NoEmptyFileDummy##__LINE__; }
/////////////////////////////////////////////////////////////////////////////
#if TARGET_ANDROID
#include <android/log.h>
#elif TARGET_IOS
void __ios_log_print(const char* message);
#endif
inline void _OutputDebugString(const std::string strHeader, bool bAddEndOfLine, const char *str, ...)
{
    char kBuffer[MAX_LENGTH];

    va_list ptr;

    va_start(ptr,str);
    VSNPRINTF(kBuffer, MAX_LENGTH, str, ptr);
    va_end(ptr);

    std::string strResult(strHeader);
    strResult.append(kBuffer);

#if TARGET_WINDOWS
    OutputDebugStringA(strResult.c_str());

    if (bAddEndOfLine)
    {
        int iEndLineChar = strResult.find('\n');
        if (iEndLineChar == std::string::npos)
        {
            OutputDebugStringA("\n");
        }
    }
#elif TARGET_ANDROID
    int logLength = strResult.length();
    int logCurrentPos = 0;

    while (logLength > 0)
    {
        int logSizeToWrite = 1024;
        if (logSizeToWrite > logLength)
        {
            logSizeToWrite = logLength;
        }

        __android_log_print(ANDROID_LOG_INFO, strHeader.c_str(), "%s", strResult.substr(logCurrentPos, logSizeToWrite).c_str());

        logLength -= logSizeToWrite;
        logCurrentPos += logSizeToWrite;
    }
#elif TARGET_IOS
    __ios_log_print(strResult.c_str());
#endif
}
/////////////////////////////////////////////////////////////////////////////
inline void _Assert_PTR(void* pointer)
{
    if (pointer == NULL)
    {
        BE_ASSERT(false);
    }
}
/////////////////////////////////////////////////////////////////////////////
#include <time.h>
inline BeInt GetCurrentTimeMS(void)
{
  return clock();
}
/////////////////////////////////////////////////////////////////////////////

#endif // #ifndef BAKENGINE3COMMON_H

