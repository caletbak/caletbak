/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeMathLibPredef.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_MATHLIBPREDEF_H
#define BE_MATHLIBPREDEF_H

// Global BE3 include
#include <BeCommon/BakEngine3Common.h>

#if TARGET_WINDOWS
#ifdef MATH_IMPORTS
#  define MATH_API __declspec(dllimport)
#  pragma message("automatic link to BeMathLib.lib")
#  pragma comment(lib, "BeMathLib.lib")
#else
#  define MATH_API __declspec(dllexport)
#endif
#else
#  define MATH_API
#endif

#ifdef _DEBUG
#  define BE_MATH_DEBUG(x, ...) _OutputDebugString("[  MATH DEBUG]: ", true, x, ##__VA_ARGS__)
#  define BE_MATH_WARNING(x, ...) _OutputDebugString("[! MATH WARNING]: ", true, x, ##__VA_ARGS__)
#  define BE_MATH_ERROR(x, ...) _OutputDebugString("[* MATH ERROR]: ", true, x, ##__VA_ARGS__)
#  define BE_MATH_PRINT(x, ...) _OutputDebugString("", false, x, ##__VA_ARGS__)
#else
#  define BE_MATH_DEBUG(x, ...) x
#  define BE_MATH_WARNING(x, ...) x
#  define BE_MATH_ERROR(x, ...) x
#  define BE_MATH_PRINT(x, ...) x
#endif

// External includes
#include <BeCoreLib/Pch/BeCoreLib.h>



// Common includes
#include <math.h>

#define BeSin    sinf
#define BeCos    cosf
#define BeASin    asinf
#define BeACos    acosf
#define BeTan    tanf
#define BeAtan    atanf
#define BeAtan2    atan2f

#define BeSqrt    sqrt
#define BeFloor    floorf

#define BeAbs    fabsf

#endif // #ifndef BE_MATHLIBPREDEF_H

