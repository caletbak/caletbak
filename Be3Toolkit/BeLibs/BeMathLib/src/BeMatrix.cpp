/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeMatrix.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <BeMatrix.h>
#include <BeMathUtils.h>
#include <BeVector3D.h>
#include <BeEulerAngles.h>
#include <BeQuaternion.h>
#include <BeRotationMatrix.h>

/////////////////////////////////////////////////////////////////////////////
void BeMatrix::Identity()
{
    m_fM11 = 1.0f; m_fM12 = 0.0f; m_fM13 = 0.0f; m_fM14 = 0.0f;
    m_fM21 = 0.0f; m_fM22 = 1.0f; m_fM23 = 0.0f; m_fM24 = 0.0f;
    m_fM31 = 0.0f; m_fM32 = 0.0f; m_fM33 = 1.0f; m_fM34 = 0.0f;
    m_fTx = 0.0f; m_fTy = 0.0f; m_fTz = 0.0f; m_fTw = 1.0f;
}
/////////////////////////////////////////////////////////////////////////////
void BeMatrix::ZeroTranslation()
{
  m_fTx = m_fTy = m_fTz = 0.0f;
}
/////////////////////////////////////////////////////////////////////////////
void BeMatrix::SetTranslation(const BeVector3D &vTranslation)
{
  m_fTx = vTranslation.m_fX; m_fTy = vTranslation.m_fY; m_fTz = vTranslation.m_fZ;
}
/////////////////////////////////////////////////////////////////////////////
void BeMatrix::SetupTranslation(const BeVector3D &vTranslation)
{
    m_fM11 = 1.0f; m_fM12 = 0.0f; m_fM13 = 0.0f; m_fM14 = 0.0f;
    m_fM21 = 0.0f; m_fM22 = 1.0f; m_fM23 = 0.0f; m_fM24 = 0.0f;
    m_fM31 = 0.0f; m_fM32 = 0.0f; m_fM33 = 1.0f; m_fM34 = 0.0f;

    m_fTx = vTranslation.m_fX; m_fTy = vTranslation.m_fY; m_fTz = vTranslation.m_fZ; m_fTw = 1.0f;
}
/////////////////////////////////////////////////////////////////////////////
void BeMatrix::SetupLocalToParent(const BeVector3D &vPosition, const BeEulerAngles &kOrient)
{

  // Create a rotation matrix.

  BeRotationMatrix mOrientMatrix;
  mOrientMatrix.Setup(kOrient);

  // Setup the 4x3 matrix.  Note: if we were really concerned with
  // speed, we could create the matrix directly BeInto these variables,
  // without using the temporary RotationMatrix object.  This would
  // save us a function call and a few copy operations.

  SetupLocalToParent(vPosition, mOrientMatrix);

}
/////////////////////////////////////////////////////////////////////////////
void BeMatrix::SetupLocalToParent(const BeVector3D &vPosition, const BeRotationMatrix &kOrient)
{

  // Copy the rotation portion of the matrix.  According to
  // the comments in RotationMatrix.cpp, the rotation matrix
  // is "normally" an inertial->object matrix, which is
  // parent->local.  We want a local->parent rotation, so we
  // must transpose while copying

    m_fM11 = kOrient.m_fM11; m_fM12 = kOrient.m_fM21; m_fM13 = kOrient.m_fM31; m_fM14 = 0.0f;
    m_fM21 = kOrient.m_fM12; m_fM22 = kOrient.m_fM22; m_fM23 = kOrient.m_fM32; m_fM24 = 0.0f;
    m_fM31 = kOrient.m_fM13; m_fM32 = kOrient.m_fM23; m_fM33 = kOrient.m_fM33; m_fM34 = 0.0f;

  // Now set the translation portion.  Translation happens "after"
  // the 3x3 portion, so we can simply copy the position
  // field directly

  m_fTx = vPosition.m_fX; m_fTy = vPosition.m_fY; m_fTz = vPosition.m_fZ; m_fTw = 1.0f;
}
/////////////////////////////////////////////////////////////////////////////
void BeMatrix::SetupParentToLocal(const BeVector3D &vPosition, const BeEulerAngles &kOrient)
{

  // Create a rotation matrix.

  BeRotationMatrix orientMatrix;
  orientMatrix.Setup(kOrient);

  // Setup the 4x3 matrix.

  SetupParentToLocal(vPosition, orientMatrix);
}
/////////////////////////////////////////////////////////////////////////////
void BeMatrix::SetupParentToLocal(const BeVector3D &vPosition, const BeRotationMatrix &kOrient)
{

  // Copy the rotation portion of the matrix.  We can copy the
  // elements directly (without transposing) according
  // to the layout as commented in RotationMatrix.cpp

    m_fM11 = kOrient.m_fM11; m_fM12 = kOrient.m_fM12; m_fM13 = kOrient.m_fM13; m_fM14 = 0.0f;
    m_fM21 = kOrient.m_fM21; m_fM22 = kOrient.m_fM22; m_fM23 = kOrient.m_fM23; m_fM24 = 0.0f;
    m_fM31 = kOrient.m_fM31; m_fM32 = kOrient.m_fM32; m_fM33 = kOrient.m_fM33; m_fM34 = 0.0f;

  // Now set the translation portion.  Normally, we would
  // translate by the negative of the position to translate
  // from world to inertial space.  However, we must correct
  // for the fact that the rotation occurs "first."  So we
  // must rotate the translation portion.  This is the same
  // as create a translation matrix T to translate by -pos,
  // and a rotation matrix R, and then creating the matrix
  // as the concatenation of TR

  m_fTx = -(vPosition.m_fX*m_fM11 + vPosition.m_fY*m_fM21 + vPosition.m_fZ*m_fM31);
  m_fTy = -(vPosition.m_fX*m_fM12 + vPosition.m_fY*m_fM22 + vPosition.m_fZ*m_fM32);
  m_fTz = -(vPosition.m_fX*m_fM13 + vPosition.m_fY*m_fM23 + vPosition.m_fZ*m_fM33);
  m_fTw = 1.0f;
}
/////////////////////////////////////////////////////////////////////////////
void BeMatrix::SetupRotate(BeEulerAngles::ECoordAxis eAxis, BeFloat fTheta)
{

  // Get sin and cosine of rotation angle

  BeFloat  s, c;
  BeMathUtils::SinCos(&s, &c, fTheta);

  // Check which axis they are rotating about

  switch (eAxis)
  {

  case BeEulerAngles::AXIS_X: // Rotate about the x-axis

      m_fM11 = 1.0f; m_fM12 = 0.0f; m_fM13 = 0.0f;
      m_fM21 = 0.0f; m_fM22 = c;    m_fM23 = s;
      m_fM31 = 0.0f; m_fM32 = -s;   m_fM33 = c;
      break;

  case BeEulerAngles::AXIS_Y: // Rotate about the y-axis

      m_fM11 = c;    m_fM12 = 0.0f; m_fM13 = -s;
      m_fM21 = 0.0f; m_fM22 = 1.0f; m_fM23 = 0.0f;
      m_fM31 = s;    m_fM32 = 0.0f; m_fM33 = c;
      break;

  case BeEulerAngles::AXIS_Z: // Rotate about the z-axis

      m_fM11 = c;    m_fM12 = s;    m_fM13 = 0.0f;
      m_fM21 = -s;   m_fM22 = c;    m_fM23 = 0.0f;
      m_fM31 = 0.0f; m_fM32 = 0.0f; m_fM33 = 1.0f;
      break;

    default:
      BE_ASSERT(BE_FALSE);
  }

  // Reset the translation portion

  m_fTx = m_fTy = m_fTz = 0.0f;
}
/////////////////////////////////////////////////////////////////////////////
void BeMatrix::SetupRotate(const BeVector3D &vAxis, BeFloat fTheta)
{

  // Quick sanim_fTy check to make sure they passed in a unit vector
  // to specify the axis
  BE_ASSERT(BeAbs(vAxis*vAxis - 1.0f) < .01f);

  // Get sin and cosine of rotation angle

  BeFloat  s, c;
  BeMathUtils::SinCos(&s, &c, fTheta);

  // Compute 1 - cos(fTheta) and some common subexpressions

  BeFloat  a = 1.0f - c;
  BeFloat  ax = a * vAxis.m_fX;
  BeFloat  ay = a * vAxis.m_fY;
  BeFloat  az = a * vAxis.m_fZ;

  // Set the matrix elements.  There is still a little more
  // opportunim_fTy for optimization due to the many common
  // subexpressions.  We'll let the compiler handle that...

  m_fM11 = ax*vAxis.m_fX + c;
  m_fM12 = ax*vAxis.m_fY + vAxis.m_fZ*s;
  m_fM13 = ax*vAxis.m_fZ - vAxis.m_fY*s;

  m_fM21 = ay*vAxis.m_fX - vAxis.m_fZ*s;
  m_fM22 = ay*vAxis.m_fY + c;
  m_fM23 = ay*vAxis.m_fZ + vAxis.m_fX*s;

  m_fM31 = az*vAxis.m_fX + vAxis.m_fY*s;
  m_fM32 = az*vAxis.m_fY - vAxis.m_fX*s;
  m_fM33 = az*vAxis.m_fZ + c;

  // Reset the translation portion

  m_fTx = m_fTy = m_fTz = 0.0f;
}
/////////////////////////////////////////////////////////////////////////////
void BeMatrix::FromQuaternion(const BeQuaternion &kQuaternion)
{

  // Compute a few values to optimize common subexpressions

  BeFloat  ww = 2.0f * kQuaternion.m_fW;
  BeFloat  xx = 2.0f * kQuaternion.m_fX;
  BeFloat  yy = 2.0f * kQuaternion.m_fY;
  BeFloat  zz = 2.0f * kQuaternion.m_fZ;

  // Set the matrix elements.  There is still a little more
  // opportunim_fTy for optimization due to the many common
  // subexpressions.  We'll let the compiler handle that...

  m_fM11 = 1.0f - yy*kQuaternion.m_fY - zz*kQuaternion.m_fZ;
  m_fM12 = xx*kQuaternion.m_fY + ww*kQuaternion.m_fZ;
  m_fM13 = xx*kQuaternion.m_fZ - ww*kQuaternion.m_fX;

  m_fM21 = xx*kQuaternion.m_fY - ww*kQuaternion.m_fZ;
  m_fM22 = 1.0f - xx*kQuaternion.m_fX - zz*kQuaternion.m_fZ;
  m_fM23 = yy*kQuaternion.m_fZ + ww*kQuaternion.m_fX;

  m_fM31 = xx*kQuaternion.m_fZ + ww*kQuaternion.m_fY;
  m_fM32 = yy*kQuaternion.m_fZ - ww*kQuaternion.m_fX;
  m_fM33 = 1.0f - xx*kQuaternion.m_fX - yy*kQuaternion.m_fY;

  // Reset the translation portion

  m_fTx = m_fTy = m_fTz = 0.0f;
}
/////////////////////////////////////////////////////////////////////////////
void BeMatrix::SetupScale(const BeVector3D &vScale)
{

  // Set the matrix elements.  Pretm_fTy straightforward

  m_fM11 = vScale.m_fX;  m_fM12 = 0.0f; m_fM13 = 0.0f;
  m_fM21 = 0.0f; m_fM22 = vScale.m_fY;  m_fM23 = 0.0f;
  m_fM31 = 0.0f; m_fM32 = 0.0f; m_fM33 = vScale.m_fZ;

  // Reset the translation portion

  m_fTx = m_fTy = m_fTz = 0.0f;
}
/////////////////////////////////////////////////////////////////////////////
void BeMatrix::SetupScaleAlongAxis(const BeVector3D &vAxis, BeFloat fK)
{

  // Quick sanim_fTy check to make sure they passed in a unit vector
  // to specify the axis

  BE_ASSERT(BeAbs(vAxis*vAxis - 1.0f) < .01f);

  // Compute k-1 and some common subexpressions

  BeFloat  a = fK - 1.0f;
  BeFloat  ax = a * vAxis.m_fX;
  BeFloat  ay = a * vAxis.m_fY;
  BeFloat  az = a * vAxis.m_fZ;

  // Fill in the matrix elements.  We'll do the common
  // subexpression optimization ourselves here, since diagonally
  // opposite matrix elements are equal

  m_fM11 = ax*vAxis.m_fX + 1.0f;
  m_fM22 = ay*vAxis.m_fY + 1.0f;
  m_fM32 = az*vAxis.m_fZ + 1.0f;

  m_fM12 = m_fM21 = ax*vAxis.m_fY;
  m_fM13 = m_fM31 = ax*vAxis.m_fZ;
  m_fM23 = m_fM32 = ay*vAxis.m_fZ;

  // Reset the translation portion

  m_fTx = m_fTy = m_fTz = 0.0f;
}
/////////////////////////////////////////////////////////////////////////////
void BeMatrix::SetupShear(BeEulerAngles::ECoordAxis eAxis, BeFloat fShear, BeFloat fT)
{

  // Check which m_fType of shear they want

    switch (eAxis)
    {

    case BeEulerAngles::AXIS_X: // Shear y and z using x

      m_fM11 = 1.0f; m_fM12 = fShear;    m_fM13 = fT;
      m_fM21 = 0.0f; m_fM22 = 1.0f; m_fM23 = 0.0f;
      m_fM31 = 0.0f; m_fM32 = 0.0f; m_fM33 = 1.0f;
      break;

    case BeEulerAngles::AXIS_Y: // Shear x and z using y

      m_fM11 = 1.0f; m_fM12 = 0.0f; m_fM13 = 0.0f;
      m_fM21 = fShear;    m_fM22 = 1.0f; m_fM23 = fT;
      m_fM31 = 0.0f; m_fM32 = 0.0f; m_fM33 = 1.0f;
      break;

    case BeEulerAngles::AXIS_Z: // Shear x and y using z

      m_fM11 = 1.0f; m_fM12 = 0.0f; m_fM13 = 0.0f;
      m_fM21 = 0.0f; m_fM22 = 1.0f; m_fM23 = 0.0f;
      m_fM31 = fShear;    m_fM32 = fT;    m_fM33 = 1.0f;
      break;

    default:

      // bogus axis index

      BE_ASSERT(BE_FALSE);
  }

  // Reset the translation portion

  m_fTx = m_fTy = m_fTz = 0.0f;
}
/////////////////////////////////////////////////////////////////////////////
void BeMatrix::SetupProject(const BeVector3D &vVector)
{

  // Quick sanimity check to make sure they passed in a unit vector
  // to specify the axis

  BE_ASSERT(BeAbs(vVector*vVector - 1.0f) < .01f);

  // Fill in the matrix elements.  We'll do the common
  // subexpression optimization ourselves here, since diagonally
  // opposite matrix elements are equal

  m_fM11 = 1.0f - vVector.m_fX*vVector.m_fX;
  m_fM22 = 1.0f - vVector.m_fY*vVector.m_fY;
  m_fM33 = 1.0f - vVector.m_fZ*vVector.m_fZ;

  m_fM12 = m_fM21 = -vVector.m_fX*vVector.m_fY;
  m_fM13 = m_fM31 = -vVector.m_fX*vVector.m_fZ;
  m_fM23 = m_fM32 = -vVector.m_fY*vVector.m_fZ;

  // Reset the translation portion

  m_fTx = m_fTy = m_fTz = 0.0f;
}
/////////////////////////////////////////////////////////////////////////////
void BeMatrix::SetupReflect(BeEulerAngles::ECoordAxis eAxis, BeFloat fK)
{

  // Check which plane they want to reflect about

    switch (eAxis)
    {

    case BeEulerAngles::AXIS_X: // Reflect about the plane x=k

      m_fM11 = -1.0f; m_fM12 =  0.0f; m_fM13 =  0.0f;
      m_fM21 =  0.0f; m_fM22 =  1.0f; m_fM23 =  0.0f;
      m_fM31 =  0.0f; m_fM32 =  0.0f; m_fM33 =  1.0f;

      m_fTx = 2.0f * fK;
      m_fTy = 0.0f;
      m_fTz = 0.0f;

      break;

    case BeEulerAngles::AXIS_Y: // Reflect about the plane y=k

      m_fM11 =  1.0f; m_fM12 =  0.0f; m_fM13 =  0.0f;
      m_fM21 =  0.0f; m_fM22 = -1.0f; m_fM23 =  0.0f;
      m_fM31 =  0.0f; m_fM32 =  0.0f; m_fM33 =  1.0f;

      m_fTx = 0.0f;
      m_fTy = 2.0f * fK;
      m_fTz = 0.0f;

      break;

    case BeEulerAngles::AXIS_Z: // Reflect about the plane z=k

      m_fM11 =  1.0f; m_fM12 =  0.0f; m_fM13 =  0.0f;
      m_fM21 =  0.0f; m_fM22 =  1.0f; m_fM23 =  0.0f;
      m_fM31 =  0.0f; m_fM32 =  0.0f; m_fM33 = -1.0f;

      m_fTx = 0.0f;
      m_fTy = 0.0f;
      m_fTz = 2.0f * fK;

      break;

    default:

      // bogus axis index

      BE_ASSERT(false);
  }

}
/////////////////////////////////////////////////////////////////////////////
void BeMatrix::SetupReflect(const BeVector3D &vVector)
{

  // Quick sanim_fTy check to make sure they passed in a unit vector
  // to specify the axis

  BE_ASSERT(BeAbs(vVector*vVector - 1.0f) < .01f);

  // Compute common subexpressions

  BeFloat  ax = -2.0f * vVector.m_fX;
  BeFloat  ay = -2.0f * vVector.m_fY;
  BeFloat  az = -2.0f * vVector.m_fZ;

  // Fill in the matrix elements.  We'll do the common
  // subexpression optimization ourselves here, since diagonally
  // opposite matrix elements are equal

  m_fM11 = 1.0f + ax*vVector.m_fX;
  m_fM22 = 1.0f + ay*vVector.m_fY;
  m_fM32 = 1.0f + az*vVector.m_fZ;

  m_fM12 = m_fM21 = ax*vVector.m_fY;
  m_fM13 = m_fM31 = ax*vVector.m_fZ;
  m_fM23 = m_fM32 = ay*vVector.m_fZ;

  // Reset the translation portion

  m_fTx = m_fTy = m_fTz = 0.0f;
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeVector3D BeMatrix::operator *(const BeVector3D &p)
{

  // Grind through the linear algebra.

  return BeVector3D(
    p.m_fX*m_fM11 + p.m_fY*m_fM21 + p.m_fZ*m_fM31 + m_fTx,
    p.m_fX*m_fM12 + p.m_fY*m_fM22 + p.m_fZ*m_fM32 + m_fTy,
    p.m_fX*m_fM13 + p.m_fY*m_fM23 + p.m_fZ*m_fM33 + m_fTz
  );

}
/////////////////////////////////////////////////////////////////////////////
BeMatrix BeMatrix::operator *(const BeMatrix &b)
{
  
  BeMatrix r;

  // Compute the upper 3x3 (linear transformation) portion

  r.m_fM11 = m_fM11*b.m_fM11 + m_fM12*b.m_fM21 + m_fM13*b.m_fM31;
  r.m_fM12 = m_fM11*b.m_fM12 + m_fM12*b.m_fM22 + m_fM13*b.m_fM32;
  r.m_fM13 = m_fM11*b.m_fM13 + m_fM12*b.m_fM23 + m_fM13*b.m_fM33;
  //r.m_fM14 = m_fM11*b.m_fM14 + m_fM12*b.m_fM24 + m_fM13*b.m_fM34;
  r.m_fM14 = 0.0f;

  r.m_fM21 = m_fM21*b.m_fM11 + m_fM22*b.m_fM21 + m_fM23*b.m_fM31;
  r.m_fM22 = m_fM21*b.m_fM12 + m_fM22*b.m_fM22 + m_fM23*b.m_fM32;
  r.m_fM23 = m_fM21*b.m_fM13 + m_fM22*b.m_fM23 + m_fM23*b.m_fM33;
  //r.m_fM24 = m_fM21*b.m_fM14 + m_fM22*b.m_fM24 + m_fM23*b.m_fM34;
  r.m_fM24 = 0.0f;

  r.m_fM31 = m_fM31*b.m_fM11 + m_fM32*b.m_fM21 + m_fM33*b.m_fM31;
  r.m_fM32 = m_fM31*b.m_fM12 + m_fM32*b.m_fM22 + m_fM33*b.m_fM32;
  r.m_fM33 = m_fM31*b.m_fM13 + m_fM32*b.m_fM23 + m_fM33*b.m_fM33;
  //r.m_fM34 = m_fM31*b.m_fM14 + m_fM32*b.m_fM24 + m_fM33*b.m_fM34;
  r.m_fM34 = 0.0f;

  // Compute the translation portion

  r.m_fTx = m_fTx*b.m_fM11 + m_fTy*b.m_fM21 + m_fTz*b.m_fM31 + b.m_fTx;
  r.m_fTy = m_fTx*b.m_fM12 + m_fTy*b.m_fM22 + m_fTz*b.m_fM32 + b.m_fTy;
  r.m_fTz = m_fTx*b.m_fM13 + m_fTy*b.m_fM23 + m_fTz*b.m_fM33 + b.m_fTz;
  //r.m_fTw = m_fTx*b.m_fM14 + m_fTy*b.m_fM24 + m_fTz*b.m_fM34 + b.m_fTw;
  r.m_fTw = 1.0f;

  // Return it.  Ouch - involves a copy constructor call.  If speed
  // is critical, we may need a seperate function which places the
  // result where we want it...

  return r;

}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeVector3D BeMatrix::MultiplyRowVector(const BeVector3D &p, const BeMatrix &m)
{

  // Grind through the linear algebra.

  return BeVector3D(
    p.m_fX*m.m_fM11 + p.m_fY*m.m_fM21 + p.m_fZ*m.m_fM31 + m.m_fTx,
    p.m_fX*m.m_fM12 + p.m_fY*m.m_fM22 + p.m_fZ*m.m_fM32 + m.m_fTy,
    p.m_fX*m.m_fM13 + p.m_fY*m.m_fM23 + p.m_fZ*m.m_fM33 + m.m_fTz
  );
}
/////////////////////////////////////////////////////////////////////////////
BeVector3D BeMatrix::MultiplyColVector(const BeVector3D &p, const BeMatrix &m)
{

  // Grind through the linear algebra.

  return BeVector3D(
    p.m_fX*m.m_fM11 + p.m_fY*m.m_fM12 + p.m_fZ*m.m_fM13 + m.m_fTx,
    p.m_fX*m.m_fM21 + p.m_fY*m.m_fM22 + p.m_fZ*m.m_fM23 + m.m_fTy,
    p.m_fX*m.m_fM31 + p.m_fY*m.m_fM32 + p.m_fZ*m.m_fM33 + m.m_fTz
  );
}
/////////////////////////////////////////////////////////////////////////////
/*
BeVector3D  BeMatrix::mulColVectorSpecial( D3DXVECTOR4 &p, TempMatrix3x4 &m )
{

  // Grind through the linear algebra.

  return BeVector3D(
    p.m_fX*m._11 + p.m_fY*m._21 + p.m_fZ*m._31 + p.w*m._41,
    p.m_fX*m._12 + p.m_fY*m._22 + p.m_fZ*m._32 + p.w*m._42,
    p.m_fX*m._13 + p.m_fY*m._23 + p.m_fZ*m._33 + p.w*m._43
  );
}
*/
/////////////////////////////////////////////////////////////////////////////
BeFloat BeMatrix::Determinant(const BeMatrix &m)
{
  return
      m.m_fM11 * (m.m_fM22*m.m_fM33 - m.m_fM23*m.m_fM32)
    + m.m_fM12 * (m.m_fM23*m.m_fM31 - m.m_fM21*m.m_fM33)
    + m.m_fM13 * (m.m_fM21*m.m_fM32 - m.m_fM22*m.m_fM31);
}
/////////////////////////////////////////////////////////////////////////////
BeMatrix BeMatrix::Inverse(const BeMatrix &m)
{

  // Compute the determinant

  BeFloat  det = Determinant(m);

  // If we're singular, then the determinant is zero and there's
  // no inverse

  BE_ASSERT(BeAbs(det) > 0.000001f);

  // Compute one over the determinant, so we divide once and
  // can *multiply* per element

  BeFloat  oneOverDet = 1.0f / det;

  // Compute the 3x3 portion of the inverse, by
  // dividing the adjoBeInt by the determinant

  BeMatrix  r;

  r.m_fM11 = (m.m_fM22*m.m_fM33 - m.m_fM23*m.m_fM32) * oneOverDet;
  r.m_fM12 = (m.m_fM13*m.m_fM32 - m.m_fM12*m.m_fM33) * oneOverDet;
  r.m_fM13 = (m.m_fM12*m.m_fM23 - m.m_fM13*m.m_fM22) * oneOverDet;

  r.m_fM21 = (m.m_fM23*m.m_fM31 - m.m_fM21*m.m_fM33) * oneOverDet;
  r.m_fM22 = (m.m_fM11*m.m_fM33 - m.m_fM13*m.m_fM31) * oneOverDet;
  r.m_fM23 = (m.m_fM13*m.m_fM21 - m.m_fM11*m.m_fM23) * oneOverDet;

  r.m_fM31 = (m.m_fM21*m.m_fM32 - m.m_fM22*m.m_fM31) * oneOverDet;
  r.m_fM32 = (m.m_fM12*m.m_fM31 - m.m_fM11*m.m_fM32) * oneOverDet;
  r.m_fM33 = (m.m_fM11*m.m_fM22 - m.m_fM12*m.m_fM21) * oneOverDet;

  // Compute the translation portion of the inverse

  r.m_fTx = -(m.m_fTx*r.m_fM11 + m.m_fTy*r.m_fM21 + m.m_fTz*r.m_fM31);
  r.m_fTy = -(m.m_fTx*r.m_fM12 + m.m_fTy*r.m_fM22 + m.m_fTz*r.m_fM32);
  r.m_fTz = -(m.m_fTx*r.m_fM13 + m.m_fTy*r.m_fM23 + m.m_fTz*r.m_fM33);

  // Return it.  Ouch - involves a copy constructor call.  If speed
  // is critical, we may need a seperate function which places the
  // result where we want it...

  return r;
}
/////////////////////////////////////////////////////////////////////////////
BeVector3D BeMatrix::GetTranslation(const BeMatrix &m)
{
  return BeVector3D(m.m_fTx, m.m_fTy, m.m_fTz);
}
/////////////////////////////////////////////////////////////////////////////
BeVector3D BeMatrix::GetPositionFromParentToLocalMatrix(const BeMatrix &m)
{

  // Multiply negative translation value by the
  // transpose of the 3x3 portion.  By using the transpose,
  // we assume that the matrix is orthogonal.  (This function
  // doesn't really make sense for non-rigid transformations...)

  return BeVector3D(
    -(m.m_fTx*m.m_fM11 + m.m_fTy*m.m_fM12 + m.m_fTz*m.m_fM13),
    -(m.m_fTx*m.m_fM21 + m.m_fTy*m.m_fM22 + m.m_fTz*m.m_fM23),
    -(m.m_fTx*m.m_fM31 + m.m_fTy*m.m_fM32 + m.m_fTz*m.m_fM33)
  );
}
/////////////////////////////////////////////////////////////////////////////
BeVector3D BeMatrix::GetPositionFromLocalToParentMatrix(const BeMatrix &m)
{

  // Position is simply the translation portion

  return BeVector3D(m.m_fTx, m.m_fTy, m.m_fTz);
}
/////////////////////////////////////////////////////////////////////////////

