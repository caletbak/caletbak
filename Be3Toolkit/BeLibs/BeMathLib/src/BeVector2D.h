/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeVector2D.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEVECTOR2D_H
#define BE_BEVECTOR2D_H

#include <BeMathLib/Pch/BeMathLibPredef.h>

/////////////////////////////////////////////////
/// \class BeVector2D
/// \brief This class represents a 2D Vector
/////////////////////////////////////////////////
class MATH_API BeVector2D
{

  public:

    /// \brief Vector X
    BeFloat m_fX;
    /// \brief Vector Y
    BeFloat m_fY;


    
    /// \brief Constructor
    BeVector2D();
    /// \brief Constructor copy
    BeVector2D(const BeVector2D &vVector);
    /// \brief Constructor by values
    BeVector2D(BeFloat fX, BeFloat fY);

    /// \brief Destructor
    ~BeVector2D();

    /// \brief Copy values
    BeVector2D &operator =(const BeVector2D &vVector);
    /// \brief Are vectors equals?
    BeBoolean operator ==(const BeVector2D &vVector) const;
    /// \brief Are vectors different?
    BeBoolean operator !=(const BeVector2D &vVector) const;
    /// \brief New Vector negative
    BeVector2D operator -() const;
    /// \brief New Vector sum
    BeVector2D operator +(const BeVector2D &vVector) const;
    /// \brief New Vector substract
    BeVector2D operator -(const BeVector2D &vVector) const;
    /// \brief New Vector multiply by scalar
    BeVector2D operator *(BeFloat a) const;
    /// \brief New Vector divide by scalar
    BeVector2D operator /(BeFloat fScalar) const;
    /// \brief This Vector add
    BeVector2D &operator +=(const BeVector2D &vVector);
    /// \brief This Vector substract
    BeVector2D &operator -=(const BeVector2D &vVector);
    /// \brief This Vector multiply by scalar
    BeVector2D &operator *=(BeFloat fScalar);
    /// \brief This Vector divide by scalar
    BeVector2D &operator /=(BeFloat fScalar);

    /// \brief Vector dot product.  We overload the standard multiplication symbol to do this
    BeFloat operator *(const BeVector2D &vVector) const;
    
    /// \brief All values to 0.0f
    void Zero();
    /// \brief Normalize the vector
    void Normalize();
    /// \brief Magnitude of the vector
    BeFloat Magnitude();
    /// \brief Set the vector values
    void SetValues(BeFloat fX, BeFloat fY);
        
    /// \brief Dot product between two BeVector2D
    static BeFloat DotProduct(const BeVector2D &vVectorA, const BeVector2D &vVectorB);
    /// \brief Distance between two BeVector2D
    static BeFloat Distance(const BeVector2D &vVectorA, const BeVector2D &vVectorB);
    /// \brief Distance without square root between two BeVector2D
    static BeFloat DistanceSquared(const BeVector2D &vVectorA, const BeVector2D &vVectorB);
    /// \brief A new BeVector2D initialized to 0.0f
    static BeVector2D ZERO(void);

};

#include <BeVector2D.inl>

#endif // #ifndef BE_BEVECTOR2D_H
