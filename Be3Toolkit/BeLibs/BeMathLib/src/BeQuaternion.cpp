/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeQuaternion.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <BeQuaternion.h>
#include <BeMathUtils.h>
#include <BeVector3D.h>
#include <BeEulerAngles.h>

/////////////////////////////////////////////////////////////////////////////
void BeQuaternion::Identity()
{
  m_fW = 1.0f;
  m_fX = m_fY = m_fZ = 0.0f;
}
/////////////////////////////////////////////////////////////////////////////
void BeQuaternion::SetToRotateAboutX(BeFloat fTheta)
{

  BeFloat fThetaOver2 = fTheta * .5f;
  
  m_fW = BeCos(fThetaOver2);
  m_fX = BeSin(fThetaOver2);
  m_fY = 0.0f;
  m_fZ = 0.0f;

}
/////////////////////////////////////////////////////////////////////////////
void BeQuaternion::SetToRotateAboutY(BeFloat fTheta)
{

  BeFloat fThetaOver2 = fTheta * .5f;

  m_fW = BeCos(fThetaOver2);
  m_fX = 0.0f;
  m_fY = BeSin(fThetaOver2);
  m_fZ = 0.0f;

}
/////////////////////////////////////////////////////////////////////////////
void BeQuaternion::SetToRotateAboutZ(BeFloat fTheta)
{
  
  BeFloat fThetaOver2 = fTheta * .5f;
  
  m_fW = BeCos(fThetaOver2);
  m_fX = 0.0f;
  m_fY = 0.0f;
  m_fZ = BeSin(fThetaOver2);

}
/////////////////////////////////////////////////////////////////////////////
void BeQuaternion::SetToRotateAboutAxis(const BeVector3D &vAxis, BeFloat fTheta)
{

  BeFloat fThetaOver2 = fTheta * .5f;
  BeFloat fSinThetaOver2 = sin(fThetaOver2);
  
  m_fW = BeCos(fThetaOver2);
  m_fX = vAxis.m_fX * fSinThetaOver2;
  m_fY = vAxis.m_fY * fSinThetaOver2;
  m_fZ = vAxis.m_fZ * fSinThetaOver2;

}
/////////////////////////////////////////////////////////////////////////////
void BeQuaternion::SetToRotateObjectToInertial(const BeEulerAngles &kOrientation)
{
  
  BeFloat fSp, fSb, fSh;
  BeFloat fCp, fCb, fCh;
  BeMathUtils::SinCos(&fSp, &fCp, kOrientation.m_fPitch * 0.5f);
  BeMathUtils::SinCos(&fSb, &fCb, kOrientation.m_fBank * 0.5f);
  BeMathUtils::SinCos(&fSh, &fCh, kOrientation.m_fHeading * 0.5f);
  
  m_fW =  fCh*fCp*fCb + fSh*fSp*fSb;
  m_fX =  fCh*fSp*fCb + fSh*fCp*fSb;
  m_fY = -fCh*fSp*fSb + fSh*fCp*fCb;
  m_fZ = -fSh*fSp*fCb + fCh*fCp*fSb;

}
/////////////////////////////////////////////////////////////////////////////
void BeQuaternion::SetToRotateInertialToObject(const BeEulerAngles &kOrientation)
{
  
  BeFloat fSp, fSb, fSh;
  BeFloat fCp, fCb, fCh;
  BeMathUtils::SinCos(&fSp, &fCp, kOrientation.m_fPitch * 0.5f);
  BeMathUtils::SinCos(&fSb, &fCb, kOrientation.m_fBank * 0.5f);
  BeMathUtils::SinCos(&fSh, &fCh, kOrientation.m_fHeading * 0.5f);
  
  m_fW =  fCh*fCp*fCb + fSh*fSp*fSb;
  m_fX = -fCh*fSp*fCb - fSh*fCp*fSb;
  m_fY =  fCh*fSp*fSb - fSh*fCb*fCp;
  m_fZ =  fSh*fSp*fCb - fCh*fCp*fSb;

}
/////////////////////////////////////////////////////////////////////////////
BeQuaternion BeQuaternion::operator *(const BeQuaternion &kQuaternion) const
{

  BeQuaternion kQesult;

  kQesult.m_fW = m_fW*kQuaternion.m_fW - m_fX*kQuaternion.m_fX - m_fY*kQuaternion.m_fY - m_fZ*kQuaternion.m_fZ;
  kQesult.m_fX = m_fW*kQuaternion.m_fX + m_fX*kQuaternion.m_fW + m_fZ*kQuaternion.m_fY - m_fY*kQuaternion.m_fZ;
  kQesult.m_fY = m_fW*kQuaternion.m_fY + m_fY*kQuaternion.m_fW + m_fX*kQuaternion.m_fZ - m_fZ*kQuaternion.m_fX;
  kQesult.m_fZ = m_fW*kQuaternion.m_fZ + m_fZ*kQuaternion.m_fW + m_fY*kQuaternion.m_fX - m_fX*kQuaternion.m_fY;

  return kQesult;

}
/////////////////////////////////////////////////////////////////////////////
BeQuaternion &BeQuaternion::operator *=(const BeQuaternion &kQuaternion)
{
    
  *this = *this * kQuaternion;
  return *this;

}
/////////////////////////////////////////////////////////////////////////////
void BeQuaternion::Normalize()
{
  
  BeFloat fMagnitude = Magnitude();
  
  if (fMagnitude > 0.0f) {

    BeFloat fOneOverMag = 1.0f / fMagnitude;
    m_fW *= fOneOverMag;
    m_fX *= fOneOverMag;
    m_fY *= fOneOverMag;
    m_fZ *= fOneOverMag;

  } else {
    
    BE_ASSERT(BE_FALSE);
    Identity();

  }
}
/////////////////////////////////////////////////////////////////////////////
BeFloat BeQuaternion::Magnitude()
{
  return BeSqrt(m_fW*m_fW + m_fX*m_fX + m_fY*m_fY + m_fZ*m_fZ);
}
/////////////////////////////////////////////////////////////////////////////
BeFloat BeQuaternion::GetRotationAngle() const
{
  
  BeFloat fThetaOver2 = BeMathUtils::SafeAcos(m_fW);
  return fThetaOver2 * 2.0f;

}
/////////////////////////////////////////////////////////////////////////////
BeVector3D BeQuaternion::GetRotationAxis() const
{

  BeFloat fSinThetaOver2Sq = 1.0f - m_fW*m_fW;
  if (fSinThetaOver2Sq <= 0.0f)
  {
    return BeVector3D(1.0f, 0.0f, 0.0f);
  }
  
  BeFloat fOneOverSinThetaOver2 = 1.0f / sqrt(fSinThetaOver2Sq);
  return BeVector3D(
    m_fX * fOneOverSinThetaOver2,
    m_fY * fOneOverSinThetaOver2,
    m_fZ * fOneOverSinThetaOver2
  );
}
/////////////////////////////////////////////////////////////////////////////
BeFloat BeQuaternion::DotProduct(const BeQuaternion &kQuaternionA, const BeQuaternion &kQuaternionB)
{
  return kQuaternionA.m_fW*kQuaternionB.m_fW + kQuaternionA.m_fX*kQuaternionB.m_fX + kQuaternionA.m_fY*kQuaternionB.m_fY + kQuaternionA.m_fZ*kQuaternionB.m_fZ;
}
/////////////////////////////////////////////////////////////////////////////
BeQuaternion BeQuaternion::Slerp(const BeQuaternion &kQuaternionQ0, const BeQuaternion &kQuaternionQ1, BeFloat fT)
{
  
  if (fT <= 0.0f) return kQuaternionQ0;
  if (fT >= 1.0f) return kQuaternionQ1;
  
  BeFloat fCosOmega = BeQuaternion::DotProduct(kQuaternionQ0, kQuaternionQ1);

  BeFloat q1w = kQuaternionQ1.m_fW;
  BeFloat q1x = kQuaternionQ1.m_fX;
  BeFloat q1y = kQuaternionQ1.m_fY;
  BeFloat q1z = kQuaternionQ1.m_fZ;
  if (fCosOmega < 0.0f) {
    q1w = -q1w;
    q1x = -q1x;
    q1y = -q1y;
    q1z = -q1z;
    fCosOmega = -fCosOmega;
  }
  
  BE_ASSERT(fCosOmega < 1.1f);
  
  BeFloat k0, k1;
  if (fCosOmega > 0.9999f) {
    
    k0 = 1.0f-fT;
    k1 = fT;

  } else {
    
    BeFloat fSinOmega = sqrt(1.0f - fCosOmega*fCosOmega);
    BeFloat fOmega = atan2(fSinOmega, fCosOmega);
    BeFloat fOneOverSinOmega = 1.0f / fSinOmega;
    
    k0 = BeSin((1.0f - fT) * fOmega) * fOneOverSinOmega;
    k1 = BeSin(fT * fOmega) * fOneOverSinOmega;
  }

  BeQuaternion kResult;
  kResult.m_fX = k0*kQuaternionQ0.m_fX + k1*q1x;
  kResult.m_fY = k0*kQuaternionQ0.m_fY + k1*q1y;
  kResult.m_fZ = k0*kQuaternionQ0.m_fZ + k1*q1z;
  kResult.m_fW = k0*kQuaternionQ1.m_fW + k1*q1w;
  
  return kResult;

}
/////////////////////////////////////////////////////////////////////////////
BeQuaternion BeQuaternion::Conjugate(const BeQuaternion &kQuaternion)
{
  
  BeQuaternion kResult;
  kResult.m_fW = kQuaternion.m_fW;
  
  kResult.m_fX = -kQuaternion.m_fX;
  kResult.m_fY = -kQuaternion.m_fY;
  kResult.m_fZ = -kQuaternion.m_fZ;
  
  return kResult;

}
/////////////////////////////////////////////////////////////////////////////
BeQuaternion BeQuaternion::Pow(const BeQuaternion &kQuaternion, BeFloat fExponent)
{
    
  if (BeAbs(kQuaternion.m_fW) > .9999f)
  {
    return kQuaternion;
  }
  
  BeFloat fAlpha = BeACos(kQuaternion.m_fW);
  BeFloat fNewAlpha = fAlpha * fExponent;
  
  BeQuaternion kResult;
  kResult.m_fW = BeCos(fNewAlpha);
  
  BeFloat fMult = BeSin(fNewAlpha) / BeSin(fAlpha);
  kResult.m_fX = kQuaternion.m_fX * fMult;
  kResult.m_fY = kQuaternion.m_fY * fMult;
  kResult.m_fZ = kQuaternion.m_fZ * fMult;
  
  return kResult;

}
/////////////////////////////////////////////////////////////////////////////

