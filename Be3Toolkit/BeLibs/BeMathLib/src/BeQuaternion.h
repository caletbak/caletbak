/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeQuaternion.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEQUATERNION_H
#define BE_BEQUATERNION_H

#include <BeMathLib/Pch/BeMathLibPredef.h>

// Forward declarations
class BeVector3D;
class BeEulerAngles;

/////////////////////////////////////////////////
/// \class BeQuaternion
/// \brief Implement a quaternion, for purposes of representing an angular displacement (orientation) in 3D.
class MATH_API BeQuaternion
{

  public:

    BeFloat    m_fW, m_fX, m_fY, m_fZ;



    /// \brief Constructor
    BeQuaternion();
    /// \brief Destructor
    ~BeQuaternion();

    /// \brief Identity
    void Identity();

    /// \brief Setup the quaternion to a specific rotation X
    void SetToRotateAboutX(BeFloat fTheta);
    /// \brief Setup the quaternion to a specific rotation Y
    void SetToRotateAboutY(BeFloat fTheta);
    /// \brief Setup the quaternion to a specific rotation Z
    void SetToRotateAboutZ(BeFloat fTheta);
    /// \brief Setup the quaternion to a specific rotation axis
    void SetToRotateAboutAxis(const BeVector3D &vAxis, BeFloat fTheta);

    /// \brief Setup the quaternion to perform an object->inertial rotation, given the orientation in Euler angle format
    void SetToRotateObjectToInertial(const BeEulerAngles &kOrientation);
    /// \brief Setup the quaternion to perform an inertial->object rotation, given the orientation in Euler angle format
    void SetToRotateInertialToObject(const BeEulerAngles &kOrientation);

    /// \brief Cross product
    BeQuaternion operator *(const BeQuaternion &kQuaternion) const;
    /// \brief Multiplication with assignment, as per C++ convention
    BeQuaternion &operator *=(const BeQuaternion &kQuaternion);

    /// \brief Normalize the quaternion.
    void Normalize();
    /// \brief Magnitude of the quaternion.
    BeFloat Magnitude();

    /// \brief Extract and return the rotation angle.
    BeFloat  GetRotationAngle() const;
    /// \brief Extract and return the rotation axis.
    BeVector3D GetRotationAxis() const;

    /// \brief Perform Dor product between two quaternions
    static BeFloat DotProduct(const BeQuaternion &kQuaternionA, const BeQuaternion &kQuaternionB);
    /// \brief Spherical linear interpolation
    static BeQuaternion Slerp(const BeQuaternion &kQuaternionQ0, const BeQuaternion &kQuaternionQ1, BeFloat fT);
    /// \brief Quaternion conjugation
    static BeQuaternion Conjugate(const BeQuaternion &kQuaternion);
    /// \brief Quaternion exponentiation
    static BeQuaternion Pow(const BeQuaternion &kQuaternion, BeFloat fExponent);

};

#include <BeQuaternion.inl>

#endif // #ifndef BE_BEQUATERNION_H
