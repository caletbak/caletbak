/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeEulerAngles.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <BeEulerAngles.h>
#include <BeMathUtils.h>
#include <BeQuaternion.h>
#include <BeRotationMatrix.h>
#include <BeMatrix.h>



/////////////////////////////////////////////////////////////////////////////
void BeEulerAngles::Canonize()
{
  
  m_fPitch = BeMathUtils::WrapPi(m_fPitch);
  
  if (m_fPitch < -BeMathUtils::K_PI_OVER_2) {
    m_fPitch = -BeMathUtils::K_PI - m_fPitch;
    m_fHeading += BeMathUtils::K_PI;
    m_fBank += BeMathUtils::K_PI;
  } else if (m_fPitch > BeMathUtils::K_PI_OVER_2) {
    m_fPitch = BeMathUtils::K_PI - m_fPitch;
    m_fHeading += BeMathUtils::K_PI;
    m_fBank += BeMathUtils::K_PI;
  }
  
  if (fabs(m_fPitch) > BeMathUtils::K_PI_OVER_2 - 1e-4) {
    
    m_fHeading += m_fBank;
    m_fBank = 0.0f;

  } else {
    
    m_fBank = BeMathUtils::WrapPi(m_fBank);
  }
  
  m_fHeading = BeMathUtils::WrapPi(m_fHeading);

}
/////////////////////////////////////////////////////////////////////////////
void BeEulerAngles::FromObjectToInertialQuaternion(const BeQuaternion &kQuaternion)
{

  BeFloat fSp = -2.0f * (kQuaternion.m_fY*kQuaternion.m_fZ - kQuaternion.m_fW*kQuaternion.m_fX);

  if (BeAbs(fSp) > 0.9999f) {

    m_fPitch = BeMathUtils::K_PI_OVER_2 * fSp;

    m_fHeading = BeAtan2(-kQuaternion.m_fX*kQuaternion.m_fZ + kQuaternion.m_fW*kQuaternion.m_fY, 0.5f - kQuaternion.m_fY*kQuaternion.m_fY - kQuaternion.m_fZ*kQuaternion.m_fZ);
    m_fBank = 0.0f;

  } else {

    m_fPitch  = BeASin(fSp);
    m_fHeading  = BeAtan2(kQuaternion.m_fX*kQuaternion.m_fZ + kQuaternion.m_fW*kQuaternion.m_fY, 0.5f - kQuaternion.m_fX*kQuaternion.m_fX - kQuaternion.m_fY*kQuaternion.m_fY);
    m_fBank  = BeAtan2(kQuaternion.m_fX*kQuaternion.m_fY + kQuaternion.m_fW*kQuaternion.m_fZ, 0.5f - kQuaternion.m_fX*kQuaternion.m_fX - kQuaternion.m_fZ*kQuaternion.m_fZ);
  }

}
/////////////////////////////////////////////////////////////////////////////
void BeEulerAngles::FromInertialToObjectQuaternion(const BeQuaternion &kQuaternion)
{

  BeFloat fSp = -2.0f * (kQuaternion.m_fY*kQuaternion.m_fZ + kQuaternion.m_fW*kQuaternion.m_fX);

  if (fabs(fSp) > 0.9999f)
  {
    
    m_fPitch = BeMathUtils::K_PI_OVER_2 * fSp;
    
    m_fHeading = BeAtan2(-kQuaternion.m_fX*kQuaternion.m_fZ - kQuaternion.m_fW*kQuaternion.m_fY, 0.5f - kQuaternion.m_fY*kQuaternion.m_fY - kQuaternion.m_fZ*kQuaternion.m_fZ);
    m_fBank = 0.0f;

  } else {
    
    m_fPitch  = BeASin(fSp);
    m_fHeading  = BeAtan2(kQuaternion.m_fX*kQuaternion.m_fZ - kQuaternion.m_fW*kQuaternion.m_fY, 0.5f - kQuaternion.m_fX*kQuaternion.m_fX - kQuaternion.m_fY*kQuaternion.m_fY);
    m_fBank  = BeAtan2(kQuaternion.m_fX*kQuaternion.m_fY - kQuaternion.m_fW*kQuaternion.m_fZ, 0.5f - kQuaternion.m_fX*kQuaternion.m_fX - kQuaternion.m_fZ*kQuaternion.m_fZ);

  }

}
/////////////////////////////////////////////////////////////////////////////
void BeEulerAngles::FromObjectToWorldMatrix(const BeMatrix &mWorldMatrix)
{
  
  BeFloat fSp = -mWorldMatrix.m_fM32;
    
  if (fabs(fSp) > 9.99999f) {
    
    m_fPitch = BeMathUtils::K_PI_OVER_2 * fSp;
    
    m_fHeading = BeAtan2(-mWorldMatrix.m_fM23, mWorldMatrix.m_fM11);
    m_fBank = 0.0f;

  } else {
    
    m_fHeading = BeAtan2(mWorldMatrix.m_fM31, mWorldMatrix.m_fM33);
    m_fPitch = BeASin(fSp);
    m_fBank = BeAtan2(mWorldMatrix.m_fM12, mWorldMatrix.m_fM22);

  }

}
/////////////////////////////////////////////////////////////////////////////
void BeEulerAngles::FromWorldToObjectMatrix(const BeMatrix &mWorldMatrix)
{
  
  BeFloat  fSp = -mWorldMatrix.m_fM23;
    
  if (fabs(fSp) > 9.99999f) {
    
    m_fPitch = BeMathUtils::K_PI_OVER_2 * fSp;
    
    m_fHeading = BeAtan2(-mWorldMatrix.m_fM31, mWorldMatrix.m_fM11);
    m_fBank = 0.0f;

  } else {
    
    m_fHeading = BeAtan2(mWorldMatrix.m_fM13, mWorldMatrix.m_fM33);
    m_fPitch = BeASin(fSp);
    m_fBank = BeAtan2(mWorldMatrix.m_fM21, mWorldMatrix.m_fM22);

  }

}
/////////////////////////////////////////////////////////////////////////////
void BeEulerAngles::FromRotationMatrix(const BeRotationMatrix &mRotationMatrix)
{

  BeFloat  sp = -mRotationMatrix.m_fM23;
  
  if (fabs(sp) > 9.99999f) {

    m_fPitch = BeMathUtils::K_PI_OVER_2 * sp;

    m_fHeading = BeAtan2(-mRotationMatrix.m_fM31, mRotationMatrix.m_fM11);
    m_fBank = 0.0f;

  } else {

    m_fHeading = BeAtan2(mRotationMatrix.m_fM13, mRotationMatrix.m_fM33);
    m_fPitch = BeASin(sp);
    m_fBank = BeAtan2(mRotationMatrix.m_fM21, mRotationMatrix.m_fM22);

  }

}
/////////////////////////////////////////////////////////////////////////////
void BeEulerAngles::Zero()
{

  m_fHeading = 0.0f;
  m_fPitch = 0.0f;
  m_fBank = 0.0f;

}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeEulerAngles BeEulerAngles::ZERO()
{

  return BeEulerAngles(
    0.0f,
    0.0f,
    0.0f
  );

}
/////////////////////////////////////////////////////////////////////////////

