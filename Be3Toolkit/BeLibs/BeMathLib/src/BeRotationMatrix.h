/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeRotationMatrix.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEROTATIONMATRIX_H
#define BE_BEROTATIONMATRIX_H

#include <BeMathLib/Pch/BeMathLibPredef.h>

// Forward declarations
class BeVector3D;
class BeEulerAngles;
class BeQuaternion;

/////////////////////////////////////////////////
/// \class BeRotationMatrix
/// \brief This class represents a rotation Matrix
///
/// MATRIX ORGANIZATION
///
/// A user of this class should rarely care how the matrix is organized.
/// However, it is of course important that internally we keep everything
/// straight.
///
/// The matrix is assumed to be a rotation matrix only, and therefore
/// orthoganal.  The "forward" direction of transformation (if that really
/// even applies in this case) will be from inertial to object space.
/// To perform an object->inertial rotation, we will multiply by the
/// transpose.
///
/// In other words:
///
/// Inertial to object:
///
///                  | m11 m12 m13 |
///     [ ix iy iz ] | m21 m22 m23 | = [ ox oy oz ]
///                  | m31 m32 m33 |
///
/// Object to inertial:
///
///                  | m11 m21 m31 |
///     [ ox oy oz ] | m12 m22 m32 | = [ ix iy iz ]
///                  | m13 m23 m33 |
///
/// Or, using column vector notation:
///
/// Inertial to object:
///
///     | m11 m21 m31 | | ix |  | ox |
///     | m12 m22 m32 | | iy | = | oy |
///     | m13 m23 m33 | | iz |  | oz |
///
/// Object to inertial:
///
///     | m11 m12 m13 | | ox |  | ix |
///     | m21 m22 m23 | | oy | = | iy |
///     | m31 m32 m33 | | oz |  | iz |
///
/////////////////////////////////////////////////
/////////////////////////////////////////////////
class MATH_API BeRotationMatrix
{
        
  public:

    BeFloat    m_fM11, m_fM12, m_fM13;
    BeFloat    m_fM21, m_fM22, m_fM23;
    BeFloat    m_fM31, m_fM32, m_fM33;



    /// \brief Constructor
    BeRotationMatrix();
    /// \brief Destructor
    ~BeRotationMatrix();
        
    /// \brief Set to identity
    void Identity();
    /// \brief Setup the matrix with a specified orientation
    void Setup(const BeEulerAngles &kOrientation);
    
    /// \brief Setup the matrix from a quaternion, assuming the quaternion performs the rotation in the specified direction of transformation
    void FromInertialToObjectQuaternion(const BeQuaternion &kQuaternion);
    /// \brief Setup the matrix from a quaternion, assuming the quaternion performs the rotation in the specified direction of transformation
    void FromObjectToInertialQuaternion(const BeQuaternion &kQuaternion);

    /// \brief Perform rotations from inertial to object
    BeVector3D InertialToObject(const BeVector3D &vVector) const;
    /// \brief Perform rotations from object to inertial
    BeVector3D ObjectToInertial(const BeVector3D &vVector) const;

};

#include <BeRotationMatrix.inl>

#endif // #ifndef BE_BEROTATIONMATRIX_H
