/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// MathUtils.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <BeMathUtils.h>



/////////////////////////////////////////////////////////////////////////////
const BeFloat BeMathUtils::K_PI = 3.14159265f;
const BeFloat BeMathUtils::K_2PI = K_PI * 2.0f;
const BeFloat BeMathUtils::K_PI_OVER_2 = K_PI / 2.0f;
const BeFloat BeMathUtils::K_1_OVER_PI = 1.0f / K_PI;
const BeFloat BeMathUtils::K_1_OVER_2PI = 1.0f / K_2PI;
const BeFloat BeMathUtils::K_PI_OVER_180 = K_PI / 180.0f;
const BeFloat BeMathUtils::K_180_OVER_PI = 180.0f / K_PI;
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeFloat BeMathUtils::WrapPi(BeFloat theta)
{
  theta += K_PI;
  theta -= BeFloor(theta * K_1_OVER_2PI) * K_2PI;
  theta -= K_PI;
  return theta;
}
/////////////////////////////////////////////////////////////////////////////
BeFloat BeMathUtils::SafeAcos(BeFloat fX)
{
  if (fX <= -1.0f)
    return K_PI;

  if (fX >= 1.0f)
    return 0.0f;
  
  return BeACos(fX);

}
/////////////////////////////////////////////////////////////////////////////
void BeMathUtils::SinCos(BeFloat *fReturnSin, BeFloat *fReturnCos, BeFloat fTheta)
{
  *fReturnSin = BeSin(fTheta);
  *fReturnCos = BeCos(fTheta);
}
/////////////////////////////////////////////////////////////////////////////
BeFloat BeMathUtils::DegToRad(BeFloat fDeg)
{
  return fDeg * K_PI_OVER_180;
}
/////////////////////////////////////////////////////////////////////////////
BeFloat BeMathUtils::RadToDeg(BeFloat fRad)
{
  return fRad * K_180_OVER_PI;
}
/////////////////////////////////////////////////////////////////////////////
BeFloat BeMathUtils::FovToZoom(BeFloat fFov)
{
  return 1.0f / BeTan(fFov * .5f);
}
/////////////////////////////////////////////////////////////////////////////
BeFloat BeMathUtils::ZoomToFov(BeFloat fZoom)
{
  return 2.0f * BeAtan(1.0f / fZoom);
}
/////////////////////////////////////////////////////////////////////////////



// Calcula el �ngulo entre dos puntos del espacio
/*
BeFloat calculateAngleBetweenPoints(BeVector3* source, BeFloat sourceYaw, BeVector3* target)
{
      
  BeVector3 v( target->x - source->x, target->y - source->y, target->z - source->z );
  v.normalize();

  BeVector3 vYaw( sinf(sourceYaw), 0, cosf(sourceYaw) );
  BeFloat value = ( v.x * vYaw.x ) + ( v.y * vYaw.y ) + ( v.z * vYaw.z );

  BeVector3 normal( sinf(sourceYaw - (kPi/2.0f)), 0, cosf(sourceYaw - (kPi/2.0f)) );
  BeFloat d = -( ( normal.x * source->x ) + ( normal.y * source->y ) + ( normal.z * source->z ) );
  BeFloat ret = ( normal.x * target->x ) + ( normal.y * target->y ) + ( normal.z * target->z ) + d;

  if (ret >= 0) return -acosf(value); 
  return acosf(value);

}

BeFloat calculateAngleBetweenPointsNOY(BeVector3* source, BeFloat sourceYaw, BeVector3* target)
{
      
  BeVector3 v( target->x - source->x, 0.0f, target->z - source->z );
  v.normalize();

  BeVector3 vYaw( sinf(sourceYaw), 0, cosf(sourceYaw) );
  BeFloat value = ( v.x * vYaw.x ) + ( v.z * vYaw.z );

  BeVector3 normal( sinf(sourceYaw - (kPi/2.0f)), 0, cosf(sourceYaw - (kPi/2.0f)) );
  BeFloat d = -( ( normal.x * source->x ) + ( normal.z * source->z ) );
  BeFloat ret = ( normal.x * target->x ) + ( normal.z * target->z ) + d;

  if (ret >= 0) return -acosf(value); 
  return acosf(value);

}
*/

