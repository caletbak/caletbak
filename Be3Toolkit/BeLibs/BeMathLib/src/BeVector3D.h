/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeVector3D.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEVECTOR3D_H
#define BE_BEVECTOR3D_H

#include <BeMathLib/Pch/BeMathLibPredef.h>

/////////////////////////////////////////////////
/// \class BeVector3D
/// \brief This class represents a 3D Vector
/////////////////////////////////////////////////
class MATH_API BeVector3D
{

  // Friendly class definition
  friend class BeRotationMatrix;

  public:
  
    /// \brief Vector X
    BeFloat m_fX;
    /// \brief Vector Y
    BeFloat m_fY;
    /// \brief Vector Z
    BeFloat m_fZ;



    /// \brief Constructor
    BeVector3D();
    /// \brief Constructor copy
    BeVector3D(const BeVector3D &vVector);
    /// \brief Constructor by values
    BeVector3D(BeFloat fX, BeFloat fY, BeFloat fZ);

    /// \brief Destructor
    ~BeVector3D();

    /// \brief Copy values
    BeVector3D &operator =(const BeVector3D &vVector);
    /// \brief Are vectors equals?
    BeBoolean operator ==(const BeVector3D &vVector) const;
    /// \brief Are vectors different?
    BeBoolean operator !=(const BeVector3D &vVector) const;
    /// \brief New Vector negative
    BeVector3D operator -() const;
    /// \brief New Vector sum
    BeVector3D operator +(const BeVector3D &vVector) const;
    /// \brief New Vector substract
    BeVector3D operator -(const BeVector3D &vVector) const;
    /// \brief New Vector multiply by scalar
    BeVector3D operator *(BeFloat a) const;
    /// \brief New Vector divide by scalar
    BeVector3D operator /(BeFloat fScalar) const;
    /// \brief This Vector add
    BeVector3D &operator +=(const BeVector3D &vVector);
    /// \brief This Vector substract
    BeVector3D &operator -=(const BeVector3D &vVector);
    /// \brief This Vector multiply by scalar
    BeVector3D &operator *=(BeFloat fScalar);
    /// \brief This Vector divide by scalar
    BeVector3D &operator /=(BeFloat fScalar);

    /// \brief Vector dot product.  We overload the standard multiplication symbol to do this
    BeFloat operator *(const BeVector3D &vVector) const;
    
    /// \brief All values to 0.0f
    void Zero();
    /// \brief Normalize the vector
    void Normalize();
    /// \brief Magnitude of the vector
    BeFloat Magnitude();
    /// \brief Set the vector values
    void SetValues(BeFloat fX, BeFloat fY, BeFloat fZ);

    /// \brief Cross product between two BeVector3D
    static BeVector3D CrossProduct(const BeVector3D &vVectorA, const BeVector3D &vVectorB);
    /// \brief Dot product between two BeVector3D
    static BeFloat DotProduct(const BeVector3D &vVectorA, const BeVector3D &vVectorB);
    /// \brief Distance between two BeVector3D
    static BeFloat Distance(const BeVector3D &vVectorA, const BeVector3D &vVectorB);
    /// \brief Distance without square root between two BeVector3D
    static BeFloat DistanceSquared(const BeVector3D &vVectorA, const BeVector3D &vVectorB);
    /// \brief A new BeVector3D initialized to 0.0f
    static BeVector3D ZERO(void);

};

#include <BeVector3D.inl>

#endif // #ifndef BE_BEVECTOR3D_H
