/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeVector4D.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEVECTOR4D_H
#define BE_BEVECTOR4D_H

#include <BeMathLib/Pch/BeMathLibPredef.h>

/////////////////////////////////////////////////
/// \class BeVector4D
/// \brief This class represents a 4D Vector
/////////////////////////////////////////////////
class MATH_API BeVector4D
{
      
  public:

    /// \brief Vector X
    BeFloat m_fX;
    /// \brief Vector Y
    BeFloat m_fY;
    /// \brief Vector Z
    BeFloat m_fZ;
    /// \brief Vector W
    BeFloat m_fW;



    /// \brief Constructor
    BeVector4D();
    /// \brief Constructor copy
    BeVector4D(const BeVector4D &vVector);
    /// \brief Constructor by values
    BeVector4D(BeFloat fX, BeFloat fY, BeFloat fZ, BeFloat fW);

    /// \brief Destructor
    ~BeVector4D();

    /// \brief Copy values
    BeVector4D &operator =(const BeVector4D &vVector);
    /// \brief Are vectors equals?
    BeBoolean operator ==(const BeVector4D &vVector) const;
    /// \brief Are vectors different?
    BeBoolean operator !=(const BeVector4D &vVector) const;
    /// \brief New Vector negative
    BeVector4D operator -() const;
    /// \brief New Vector sum
    BeVector4D operator +(const BeVector4D &vVector) const;
    /// \brief New Vector substract
    BeVector4D operator -(const BeVector4D &vVector) const;
    /// \brief New Vector multiply by scalar
    BeVector4D operator *(BeFloat a) const;
    /// \brief New Vector divide by scalar
    BeVector4D operator /(BeFloat fScalar) const;
    /// \brief This Vector add
    BeVector4D &operator +=(const BeVector4D &vVector);
    /// \brief This Vector substract
    BeVector4D &operator -=(const BeVector4D &vVector);
    /// \brief This Vector multiply by scalar
    BeVector4D &operator *=(BeFloat fScalar);
    /// \brief This Vector divide by scalar
    BeVector4D &operator /=(BeFloat fScalar);

    /// \brief Vector dot product.  We overload the standard multiplication symbol to do this
    BeFloat operator *(const BeVector4D &vVector) const;
    
    /// \brief All values to 0.0f
    void Zero();
    /// \brief Normalize the vector
    void Normalize();
    /// \brief Magnitude of the vector
    BeFloat Magnitude();
    /// \brief Set the vector values
    void SetValues(BeFloat fX, BeFloat fY, BeFloat fZ, BeFloat fW);
        
    /// \brief Dot product between two BeVector4D
    static BeFloat DotProduct(const BeVector4D &vVectorA, const BeVector4D &vVectorB);
    /// \brief Distance between two BeVector4D
    static BeFloat Distance(const BeVector4D &vVectorA, const BeVector4D &vVectorB);
    /// \brief Distance without square root between two BeVector4D
    static BeFloat DistanceSquared(const BeVector4D &vVectorA, const BeVector4D &vVectorB);
    /// \brief A new BeVector4D initialized to 0.0f
    static BeVector4D ZERO(void);

};

#include <BeVector4D.inl>

#endif // #ifndef BE_BEVECTOR4D_H
