/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeVector3D.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <BeVector3D.h>



/////////////////////////////////////////////////////////////////////////////
BeVector3D BeVector3D::operator /(BeFloat fScalar) const
{
  BeFloat  fOneOverA = 1.0f / fScalar; // NOTE: no check for divide by zero here
  return BeVector3D(m_fX*fOneOverA, m_fY*fOneOverA, m_fZ*fOneOverA);
}
/////////////////////////////////////////////////////////////////////////////
BeVector3D &BeVector3D::operator +=(const BeVector3D &vVector)
{
  m_fX += vVector.m_fX;
  m_fY += vVector.m_fY;
  m_fZ += vVector.m_fZ;
  return *this;
}
/////////////////////////////////////////////////////////////////////////////
BeVector3D &BeVector3D::operator -=(const BeVector3D &vVector)
{
  m_fX -= vVector.m_fX;
  m_fY -= vVector.m_fY;
  m_fZ -= vVector.m_fZ;
  return *this;
}
/////////////////////////////////////////////////////////////////////////////
BeVector3D &BeVector3D::operator *=(BeFloat fScalar)
{
  m_fX *= fScalar;
  m_fY *= fScalar;
  m_fZ *= fScalar;
  return *this;
}
/////////////////////////////////////////////////////////////////////////////
BeVector3D &BeVector3D::operator /=(BeFloat fScalar)
{
  BeFloat  fOneOverA = 1.0f / fScalar;
  m_fX *= fOneOverA;
  m_fY *= fOneOverA;
  m_fZ *= fOneOverA;
  return *this;
}
/////////////////////////////////////////////////////////////////////////////
void BeVector3D::Normalize()
{

  BeFloat fMagnitude = m_fX*m_fX + m_fY*m_fY + m_fZ*m_fZ;

  if (fMagnitude > 0.0f) // check for divide-by-zero
  { 
    BeFloat fOneOverMag = 1.0f / BeSqrt(fMagnitude);
    m_fX *= fOneOverMag;
    m_fY *= fOneOverMag;
    m_fZ *= fOneOverMag;
  }

}
/////////////////////////////////////////////////////////////////////////////
BeFloat BeVector3D::Magnitude()
{
  return BeSqrt(m_fX*m_fX + m_fY*m_fY + m_fZ*m_fZ);
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeVector3D BeVector3D::CrossProduct(const BeVector3D &vVectorA, const BeVector3D &vVectorB)
{
  return BeVector3D(
    vVectorA.m_fY*vVectorB.m_fZ - vVectorA.m_fZ*vVectorB.m_fY,
    vVectorA.m_fZ*vVectorB.m_fX - vVectorA.m_fX*vVectorB.m_fZ,
    vVectorA.m_fX*vVectorB.m_fY - vVectorA.m_fY*vVectorB.m_fX
  );
}
/////////////////////////////////////////////////////////////////////////////
BeFloat BeVector3D::DotProduct(const BeVector3D &vVectorA, const BeVector3D &vVectorB)
{
  return vVectorA.m_fX*vVectorB.m_fX + vVectorA.m_fY*vVectorB.m_fY + vVectorA.m_fZ*vVectorB.m_fZ;
}
/////////////////////////////////////////////////////////////////////////////
BeFloat BeVector3D::Distance(const BeVector3D &vVectorA, const BeVector3D &vVectorB)
{
  BeFloat fDx = vVectorA.m_fX - vVectorB.m_fX;
  BeFloat fDy = vVectorA.m_fY - vVectorB.m_fY;
  BeFloat fDz = vVectorA.m_fZ - vVectorB.m_fZ;

  return BeSqrt(fDx*fDx + fDy*fDy + fDz*fDz);
}
/////////////////////////////////////////////////////////////////////////////
BeFloat BeVector3D::DistanceSquared(const BeVector3D &vVectorA, const BeVector3D &vVectorB)
{
  BeFloat fDx = vVectorA.m_fX - vVectorB.m_fX;
  BeFloat fDy = vVectorA.m_fY - vVectorB.m_fY;
  BeFloat fDz = vVectorA.m_fZ - vVectorB.m_fZ;

  return fDx*fDx + fDy*fDy + fDz*fDz;
}
/////////////////////////////////////////////////////////////////////////////
BeVector3D BeVector3D::ZERO(void)
{
  return BeVector3D(
    0.0f,
    0.0f,
    0.0f
  );
}
/////////////////////////////////////////////////////////////////////////////

