/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeMathUtils.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEMATHUTILS_H
#define BE_BEMATHUTILS_H

#include <BeMathLib/Pch/BeMathLibPredef.h>

/////////////////////////////////////////////////
/// \class BeMathUtils
/// \brief This class has some common Math methods
class MATH_API BeMathUtils
{
    
  public:

    /// \brief PI
    static const BeFloat K_PI;
    /// \brief PI * 2
    static const BeFloat K_2PI;
    /// \brief PI / 2
    static const BeFloat K_PI_OVER_2;
    /// \brief 1 / PI
    static const BeFloat K_1_OVER_PI;
    /// \brief 1 / 2PI
    static const BeFloat K_1_OVER_2PI;
    /// \brief PI / 180
    static const BeFloat K_PI_OVER_180;
    /// \brief 180 / PI
    static const BeFloat K_180_OVER_PI;

    /// \brief "Wrap" an angle in range -pi...pi by adding the correct multiple
    static BeFloat WrapPi(BeFloat fTheta);
    /// \brief Same as acos(x), but if x is out of range, it is "clamped" to the nearest valid value.  The value returned is in range 0...pi, the same as the standard C acos() function
    static BeFloat SafeAcos(BeFloat fX);
    /// \brief Compute the sin and cosine of an angle.  On some platforms, if we know that we need both values, it can be computed faster than computing the two values seperately.
    static void SinCos(BeFloat *fReturnSin, BeFloat *fReturnCos, BeFloat fTheta);
    
    /// \brief Deg to Rad
    static BeFloat DegToRad(BeFloat fDeg);
    /// \brief Rad to Deg
    static BeFloat RadToDeg(BeFloat fRad);
        
    /// \brief Convert between "field of view" and "zoom"
    /// \param fFov The FOV angle is specified in radians.
    static BeFloat FovToZoom(BeFloat fFov);
    /// \brief Convert between "field of view" and "zoom"
    static BeFloat ZoomToFov(BeFloat fZoom);

    //BeFloat CalculateAngleBetweenPoints(BeVector3* source, BeFloat sourceYaw, BeVector3* target);
    //BeFloat calculateAngleBetweenPointsNOY(BeVector3* source, BeFloat sourceYaw, BeVector3* target);
    
};


#endif // #ifndef BE_BEMATHUTILS_H
