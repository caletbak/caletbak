/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeRotationMatrix.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <BeRotationMatrix.h>
#include <BeMathUtils.h>
#include <BeVector3D.h>
#include <BeEulerAngles.h>
#include <BeQuaternion.h>

/////////////////////////////////////////////////////////////////////////////
void BeRotationMatrix::Identity()
{
  m_fM11 = 1.0f; m_fM12 = 0.0f; m_fM13 = 0.0f;
  m_fM21 = 0.0f; m_fM22 = 1.0f; m_fM23 = 0.0f;
  m_fM31 = 0.0f; m_fM32 = 0.0f; m_fM33 = 1.0f;
}
/////////////////////////////////////////////////////////////////////////////
void BeRotationMatrix::Setup(const BeEulerAngles &kOrientation)
{
  
  BeFloat  fSh, fCh, fSp, fCp, fSb, fCb;
  BeMathUtils::SinCos(&fSh, &fCh, kOrientation.m_fHeading);
  BeMathUtils::SinCos(&fSp, &fCp, kOrientation.m_fPitch);
  BeMathUtils::SinCos(&fSb, &fCb, kOrientation.m_fBank);
  
  m_fM11 = fCh * fCb + fSh * fSp * fSb;
  m_fM12 = -fCh * fSb + fSh * fSp * fCb;
  m_fM13 = fSh * fCp;

  m_fM21 = fSb * fCp;
  m_fM22 = fCb * fCp;
  m_fM23 = -fSp;

  m_fM31 = -fSh * fCb + fCh * fSp * fSb;
  m_fM32 = fSb * fSh + fCh * fSp * fCb;
  m_fM33 = fCh * fCp;

}
/////////////////////////////////////////////////////////////////////////////
void BeRotationMatrix::FromInertialToObjectQuaternion(const BeQuaternion &kQuaternion)
{
  m_fM11 = 1.0f - 2.0f * (kQuaternion.m_fY*kQuaternion.m_fY + kQuaternion.m_fZ*kQuaternion.m_fZ);
  m_fM12 = 2.0f * (kQuaternion.m_fX*kQuaternion.m_fY + kQuaternion.m_fW*kQuaternion.m_fZ);
  m_fM13 = 2.0f * (kQuaternion.m_fX*kQuaternion.m_fZ - kQuaternion.m_fW*kQuaternion.m_fY);

  m_fM21 = 2.0f * (kQuaternion.m_fX*kQuaternion.m_fY - kQuaternion.m_fW*kQuaternion.m_fZ);
  m_fM22 = 1.0f - 2.0f * (kQuaternion.m_fX*kQuaternion.m_fX + kQuaternion.m_fZ*kQuaternion.m_fZ);
  m_fM23 = 2.0f * (kQuaternion.m_fY*kQuaternion.m_fZ + kQuaternion.m_fW*kQuaternion.m_fX);

  m_fM31 = 2.0f * (kQuaternion.m_fX*kQuaternion.m_fZ + kQuaternion.m_fW*kQuaternion.m_fY);
  m_fM32 = 2.0f * (kQuaternion.m_fY*kQuaternion.m_fZ - kQuaternion.m_fW*kQuaternion.m_fX);
  m_fM33 = 1.0f - 2.0f * (kQuaternion.m_fX*kQuaternion.m_fX + kQuaternion.m_fY*kQuaternion.m_fY);

}
/////////////////////////////////////////////////////////////////////////////
void BeRotationMatrix::FromObjectToInertialQuaternion(const BeQuaternion &kQuaternion)
{
  m_fM11 = 1.0f - 2.0f * (kQuaternion.m_fY*kQuaternion.m_fY + kQuaternion.m_fZ*kQuaternion.m_fZ);
  m_fM12 = 2.0f * (kQuaternion.m_fX*kQuaternion.m_fY - kQuaternion.m_fW*kQuaternion.m_fZ);
  m_fM13 = 2.0f * (kQuaternion.m_fX*kQuaternion.m_fZ + kQuaternion.m_fW*kQuaternion.m_fY);

  m_fM21 = 2.0f * (kQuaternion.m_fX*kQuaternion.m_fY + kQuaternion.m_fW*kQuaternion.m_fZ);
  m_fM22 = 1.0f - 2.0f * (kQuaternion.m_fX*kQuaternion.m_fX + kQuaternion.m_fZ*kQuaternion.m_fZ);
  m_fM23 = 2.0f * (kQuaternion.m_fY*kQuaternion.m_fZ - kQuaternion.m_fW*kQuaternion.m_fX);

  m_fM31 = 2.0f * (kQuaternion.m_fX*kQuaternion.m_fZ - kQuaternion.m_fW*kQuaternion.m_fY);
  m_fM32 = 2.0f * (kQuaternion.m_fY*kQuaternion.m_fZ + kQuaternion.m_fW*kQuaternion.m_fX);
  m_fM33 = 1.0f - 2.0f * (kQuaternion.m_fX*kQuaternion.m_fX + kQuaternion.m_fY*kQuaternion.m_fY);
}
/////////////////////////////////////////////////////////////////////////////
BeVector3D BeRotationMatrix::InertialToObject(const BeVector3D &vVector) const
{
  return BeVector3D(
    m_fM11*vVector.m_fX + m_fM21*vVector.m_fY + m_fM31*vVector.m_fZ,
    m_fM12*vVector.m_fX + m_fM22*vVector.m_fY + m_fM32*vVector.m_fZ,
    m_fM13*vVector.m_fX + m_fM23*vVector.m_fY + m_fM33*vVector.m_fZ
  );
}
/////////////////////////////////////////////////////////////////////////////
BeVector3D BeRotationMatrix::ObjectToInertial(const BeVector3D &vVector) const
{
  return BeVector3D(
    m_fM11*vVector.m_fX + m_fM12*vVector.m_fY + m_fM13*vVector.m_fZ,
    m_fM21*vVector.m_fX + m_fM22*vVector.m_fY + m_fM23*vVector.m_fZ,
    m_fM31*vVector.m_fX + m_fM32*vVector.m_fY + m_fM33*vVector.m_fZ
  );
}
/////////////////////////////////////////////////////////////////////////////
