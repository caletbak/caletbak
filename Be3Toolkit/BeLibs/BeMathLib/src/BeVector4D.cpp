/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeVector4D.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <BeVector4D.h>



/////////////////////////////////////////////////////////////////////////////
BeVector4D BeVector4D::operator /(BeFloat fScalar) const
{
  BeFloat  fOneOverA = 1.0f / fScalar; // NOTE: no check for divide by zero here
  return BeVector4D(m_fX*fOneOverA, m_fY*fOneOverA, m_fZ*fOneOverA, m_fW*fOneOverA);
}
/////////////////////////////////////////////////////////////////////////////
BeVector4D &BeVector4D::operator +=(const BeVector4D &vVector)
{
  m_fX += vVector.m_fX;
  m_fY += vVector.m_fY;
  m_fZ += vVector.m_fZ;
  m_fW += vVector.m_fW;
  return *this;
}
/////////////////////////////////////////////////////////////////////////////
BeVector4D &BeVector4D::operator -=(const BeVector4D &vVector)
{
  m_fX -= vVector.m_fX;
  m_fY -= vVector.m_fY;
  m_fZ -= vVector.m_fZ;
  m_fW -= vVector.m_fW;
  return *this;
}
/////////////////////////////////////////////////////////////////////////////
BeVector4D &BeVector4D::operator *=(BeFloat fScalar)
{
  m_fX *= fScalar;
  m_fY *= fScalar;
  m_fZ *= fScalar;
  m_fW *= fScalar;
  return *this;
}
/////////////////////////////////////////////////////////////////////////////
BeVector4D &BeVector4D::operator /=(BeFloat fScalar)
{
  BeFloat  fOneOverA = 1.0f / fScalar;
  m_fX *= fOneOverA;
  m_fY *= fOneOverA;
  m_fZ *= fOneOverA;
  m_fW *= fOneOverA;
  return *this;
}
/////////////////////////////////////////////////////////////////////////////
void BeVector4D::Normalize()
{

  BeFloat fMagnitude = m_fX*m_fX + m_fY*m_fY + m_fZ*m_fZ + m_fW*m_fW;

  if (fMagnitude > 0.0f) // check for divide-by-zero
  { 
    BeFloat fOneOverMag = 1.0f / BeSqrt(fMagnitude);
    m_fX *= fOneOverMag;
    m_fY *= fOneOverMag;
    m_fZ *= fOneOverMag;
    m_fW *= fOneOverMag;
  }

}
/////////////////////////////////////////////////////////////////////////////
BeFloat BeVector4D::Magnitude()
{
  return BeSqrt(m_fX*m_fX + m_fY*m_fY + m_fZ*m_fZ + m_fW*m_fW);
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeFloat BeVector4D::DotProduct(const BeVector4D &vVectorA, const BeVector4D &vVectorB)
{
  return vVectorA.m_fX*vVectorB.m_fX + vVectorA.m_fY*vVectorB.m_fY + vVectorA.m_fZ*vVectorB.m_fZ + vVectorA.m_fW*vVectorB.m_fW;
}
/////////////////////////////////////////////////////////////////////////////
BeFloat BeVector4D::Distance(const BeVector4D &vVectorA, const BeVector4D &vVectorB)
{
  BeFloat fDx = vVectorA.m_fX - vVectorB.m_fX;
  BeFloat fDy = vVectorA.m_fY - vVectorB.m_fY;
  BeFloat fDz = vVectorA.m_fZ - vVectorB.m_fZ;
  BeFloat fDw = vVectorA.m_fW - vVectorB.m_fW;

  return BeSqrt(fDx*fDx + fDy*fDy + fDz*fDz + fDw*fDw);
}
/////////////////////////////////////////////////////////////////////////////
BeFloat BeVector4D::DistanceSquared(const BeVector4D &vVectorA, const BeVector4D &vVectorB)
{
  BeFloat fDx = vVectorA.m_fX - vVectorB.m_fX;
  BeFloat fDy = vVectorA.m_fY - vVectorB.m_fY;
  BeFloat fDz = vVectorA.m_fZ - vVectorB.m_fZ;
  BeFloat fDw = vVectorA.m_fW - vVectorB.m_fW;

  return fDx*fDx + fDy*fDy + fDz*fDz + fDw*fDw;
}
/////////////////////////////////////////////////////////////////////////////
BeVector4D BeVector4D::ZERO(void)
{
  return BeVector4D(
    0.0f,
    0.0f,
    0.0f,
    0.0f
  );
}
/////////////////////////////////////////////////////////////////////////////

