/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeVector2D.cpp
//
/////////////////////////////////////////////////////////////////////////////

#include <BeVector2D.h>



/////////////////////////////////////////////////////////////////////////////
BeVector2D BeVector2D::operator /(BeFloat fScalar) const
{
  BeFloat  fOneOverA = 1.0f / fScalar; // NOTE: no check for divide by zero here
  return BeVector2D(m_fX*fOneOverA, m_fY*fOneOverA);
}
/////////////////////////////////////////////////////////////////////////////
BeVector2D &BeVector2D::operator +=(const BeVector2D &vVector)
{
  m_fX += vVector.m_fX;
  m_fY += vVector.m_fY;
  return *this;
}
/////////////////////////////////////////////////////////////////////////////
BeVector2D &BeVector2D::operator -=(const BeVector2D &vVector)
{
  m_fX -= vVector.m_fX;
  m_fY -= vVector.m_fY;
  return *this;
}
/////////////////////////////////////////////////////////////////////////////
BeVector2D &BeVector2D::operator *=(BeFloat fScalar)
{
  m_fX *= fScalar;
  m_fY *= fScalar;
  return *this;
}
/////////////////////////////////////////////////////////////////////////////
BeVector2D &BeVector2D::operator /=(BeFloat fScalar)
{
  BeFloat  fOneOverA = 1.0f / fScalar;
  m_fX *= fOneOverA;
  m_fY *= fOneOverA;
  return *this;
}
/////////////////////////////////////////////////////////////////////////////
void BeVector2D::Normalize()
{

  BeFloat fMagnitude = Magnitude();

  if (fMagnitude > 0.0f) // check for divide-by-zero
  { 
    BeFloat fOneOverMag = 1.0f / BeSqrt(fMagnitude);
    m_fX *= fOneOverMag;
    m_fY *= fOneOverMag;
  }

}
/////////////////////////////////////////////////////////////////////////////
BeFloat BeVector2D::Magnitude()
{
  return BeSqrt(m_fX*m_fX + m_fY*m_fY);
}
/////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////
BeFloat BeVector2D::DotProduct(const BeVector2D &vVectorA, const BeVector2D &vVectorB)
{
  return vVectorA.m_fX*vVectorB.m_fX + vVectorA.m_fY*vVectorB.m_fY;
}
/////////////////////////////////////////////////////////////////////////////
BeFloat BeVector2D::Distance(const BeVector2D &vVectorA, const BeVector2D &vVectorB)
{
  BeFloat fDx = vVectorA.m_fX - vVectorB.m_fX;
  BeFloat fDy = vVectorA.m_fY - vVectorB.m_fY;

  return BeSqrt(fDx*fDx + fDy*fDy);
}
/////////////////////////////////////////////////////////////////////////////
BeFloat BeVector2D::DistanceSquared(const BeVector2D &vVectorA, const BeVector2D &vVectorB)
{
  BeFloat fDx = vVectorA.m_fX - vVectorB.m_fX;
  BeFloat fDy = vVectorA.m_fY - vVectorB.m_fY;

  return fDx*fDx + fDy*fDy;
}
/////////////////////////////////////////////////////////////////////////////
BeVector2D BeVector2D::ZERO(void)
{
  return BeVector2D(
    0.0f,
    0.0f
  );
}
/////////////////////////////////////////////////////////////////////////////
