/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeMatrix.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEMATRIX_H
#define BE_BEMATRIX_H

#include <BeMathLib/Pch/BeMathLibPredef.h>
#include <BeEulerAngles.h>

// Forward declarations
class BeVector3D;
class BeEulerAngles;
class BeQuaternion;
class BeRotationMatrix;

/*
struct TempMatrix3x4 {
  BeFloat _11, _21, _31, _41;
  BeFloat _12, _22, _32, _42;
  BeFloat _13, _23, _33, _43;
};
*/

/////////////////////////////////////////////////
/// \class BeMatrix
/// \brief Implement a 4x3 transformation matrix.  This class can represent any 3D affine transformation.
///
/// MATRIX ORGANIZATION
///
/// The purpose of this class is so that a user might perform transformations
/// without fiddling with plus or minus signs or transposing the matrix
/// until the output "looks right."  But of course, the specifics of the
//// internal representation is important.  Not only for the implementation
/// in this file to be correct, but occasionally direct access to the
/// matrix variables is necessary, or beneficial for optimization.  Thus,
/// we document our matrix conventions here.
///
/// We use row vectors, so multiplying by our matrix looks like this:
///
///               | m11 m12 m13 |
///     [ x y z ] | m21 m22 m23 | = [ x' y' z' ]
///               | m31 m32 m33 |
///               | tx  ty  tz  |
///
/// Strict adherance to linear algebra rules dictates that this
/// multiplication is actually undefined.  To circumvent this, we can
/// consider the input and output vectors as having an assumed fourth
/// coordinate of 1.  Also, since we cannot technically invert a 4x3 matrix
/// according to linear algebra rules, we will also assume a rightmost
/// column of [ 0 0 0 1 ].  This is shown below:
///
///                 | m11 m12 m13 0 |
///     [ x y z 1 ] | m21 m22 m23 0 | = [ x' y' z' 1 ]
///                 | m31 m32 m33 0 |
///                 | tx  ty  tz  1 |
///
/// In case you have forgotten your linear algebra rules for multiplying
/// matrices (which are described in section 7.1.6 and 7.1.7), see the
/// definition of operator* for the expanded computations.
///
/////////////////////////////////////////////////
class MATH_API BeMatrix
{

  public:

        BeFloat m_fM11, m_fM12, m_fM13, m_fM14;
        BeFloat m_fM21, m_fM22, m_fM23, m_fM24;
        BeFloat m_fM31, m_fM32, m_fM33, m_fM34;
        BeFloat m_fTx, m_fTy, m_fTz, m_fTw;

    /// \brief Constructor
    BeMatrix();
    /// \brief Destructor
    ~BeMatrix();

    /// \brief Setup the matrix as identity matrix
    void Identity();

    /// \brief Resets the translation to 0.0f
    void ZeroTranslation();
    /// \brief Sets the translation to the BeVector3D
    void SetTranslation(const BeVector3D &vTranslation);
    /// \brief Setup the entire Matrix with the translation of the BeVector3D
    void SetupTranslation(const BeVector3D &vTranslation);

    /// \brief Setup the matrix to perform a specific transforms from local <-> parent space, assuming the local space is in the specified position and orientation within the parent space.  The orientation may be specified using Euler angles.
    void SetupLocalToParent(const BeVector3D &vPosition, const BeEulerAngles &kOrient);
    /// \brief Setup the matrix to perform a specific transforms from local <-> parent space, assuming the local space is in the specified position and orientation within the parent space.  The orientation may be specified using a rotation matrix
    void SetupLocalToParent(const BeVector3D &vPosition, const BeRotationMatrix &kOrient);
    /// \brief Setup the matrix to perform a specific transforms from parent <-> local space, assuming the local space is in the specified position and orientation within the parent space.  The orientation may be specified using Euler angles.
    void SetupParentToLocal(const BeVector3D &vPosition, const BeEulerAngles &kOrient);
    /// \brief Setup the matrix to perform a specific transforms from parent <-> local space, assuming the local space is in the specified position and orientation within the parent space.  The orientation may be specified using a rotation matrix
    void SetupParentToLocal(const BeVector3D &vPosition, const BeRotationMatrix &kOrient);

    /// \brief Setup the matrix to perform a rotation about a cardinal axis
    void SetupRotate(BeEulerAngles::ECoordAxis eAxis, BeFloat fTheta);
    /// \brief Setup the matrix to perform a rotation about an arbitrary axis
    void SetupRotate(const BeVector3D &vAxis, BeFloat fTheta);
    /// \brief Setup the matrix to perform a rotation, given the angular displacement in quaternion form
    void FromQuaternion(const BeQuaternion &kQuaternion);
    /// \brief Setup the matrix to perform scale on each axis
    void SetupScale(const BeVector3D &vScale);
    /// \brief Setup the matrix to perform scale along an arbitrary axis
    void SetupScaleAlongAxis(const BeVector3D &vAxis, BeFloat fK);
    /// \brief Setup the matrix to perform a shear
    void SetupShear(BeEulerAngles::ECoordAxis eAxis, BeFloat fShear, BeFloat fT);
    /// \brief Setup the matrix to perform a projection onto a plane passing through the origin
    void SetupProject(const BeVector3D &vVector);
    /// \brief Setup the matrix to perform a reflection about a plane parallel to a cardinal plane
    void SetupReflect(BeEulerAngles::ECoordAxis eAxis, BeFloat fK = 0.0f);
    /// \brief Setup the matrix to perform a reflection about an arbitrary plane through the origin
    void SetupReflect(const BeVector3D &vVector);

    /// \brief Multiply by a vector and return new vector
    BeVector3D operator *(const BeVector3D &p);
    /// \brief Multiply by a matrix and return new matrix
    BeMatrix operator *(const BeMatrix &b);
    
    /// \brief Multiply a row vector by a matrix
    static BeVector3D MultiplyRowVector(const BeVector3D &p, const BeMatrix &m);
    /// \brief Multiply a column vector by a matrix
    static BeVector3D MultiplyColVector(const BeVector3D &p, const BeMatrix &m);
    //static BeVector3 mulColVectorSpecial( D3DXVECTOR4 &p, TempMatrix3x4 &m );

    /// \brief Compute the determinant of a matrix
    static BeFloat Determinant(const BeMatrix &m);
    /// \brief Compute the inverse of a matrix
    static BeMatrix Inverse(const BeMatrix &m);
    /// \brief Extract the translation portion of the matrix
    static BeVector3D GetTranslation(const BeMatrix &m);
    /// \brief Extract the position/orientation from a parent->local matrix
    static BeVector3D GetPositionFromParentToLocalMatrix(const BeMatrix &m);
    /// \brief Extract the position/orientation from a local->parent matrix
    static BeVector3D GetPositionFromLocalToParentMatrix(const BeMatrix &m);
    
};

#include <BeMatrix.inl>

#endif // #ifndef BE_BEMATRIX_H
