/////////////////////////////////////////////////////////////////////////////
//
// BAK ENGINE 3 (2011)
// by Javier Calet Toledo
//
// BeEulerAngles.h
//
/////////////////////////////////////////////////////////////////////////////

#ifndef BE_BEEULERANGLES_H
#define BE_BEEULERANGLES_H

#include <BeMathLib/Pch/BeMathLibPredef.h>

// Forward declarations
class BeQuaternion;
class BeMatrix;
class BeRotationMatrix;

/////////////////////////////////////////////////
/// \class BeEulerAngles
/// \brief This class represents a heading-pitch-bank Euler angle triple.
/////////////////////////////////////////////////
class MATH_API BeEulerAngles
{

  // Friendly class definition
  friend class BeRotationMatrix;
        
  public:

      /// \brief  Coordinate Axis
      enum ECoordAxis
      {
          AXIS_X = 0,
          AXIS_Y,
          AXIS_Z
      };

    /// \brief Heading
    BeFloat m_fHeading;
    /// \brief Pitch
    BeFloat m_fPitch;
    /// \brief Bank
    BeFloat m_fBank;
    


    /// \brief Constructor
    BeEulerAngles();
    /// \brief Constructor by values
    BeEulerAngles(BeFloat fHeading, BeFloat fPitch, BeFloat fBank); 
    /// \brief Destructor
    ~BeEulerAngles();
    
    /// \brief Identity
    void Identity();
    /// \brief Zero values
    void Zero();
    /// \brief Canonize
    void Canonize();
    
    /// \brief Euler Angles are equal?
    BeBoolean operator ==(const BeEulerAngles &a) const;
    /// \brief Euler Angles are different?
    BeBoolean operator !=(const BeEulerAngles &a) const;

    /// \brief Convert the quaternion to Euler angle format.  The input quaternion is assumed to perform the rotation from object-to-inertial or inertial-to-object, as indicated.
    void FromObjectToInertialQuaternion(const BeQuaternion &kQuaternion);
    /// \brief Convert the quaternion to Euler angle format.  The input quaternion is assumed to perform the rotation from object-to-inertial or inertial-to-object, as indicated.
    void FromInertialToObjectQuaternion(const BeQuaternion &kQuaternion);

    /// \brief Convert the transform matrix to Euler angle format.  The input matrix is assumed to perform the transformation from object-to-world, or world-to-object, as indicated.  The translation portion of the matrix is ignored.  The matrix is assumed to be orthogonal.
    void FromObjectToWorldMatrix(const BeMatrix &mWorldMatrix);
    /// \brief Convert the transform matrix to Euler angle format.  The input matrix is assumed to perform the transformation from object-to-world, or world-to-object, as indicated.  The translation portion of the matrix is ignored.  The matrix is assumed to be orthogonal.
    void FromWorldToObjectMatrix(const BeMatrix &mWorldMatrix);

    /// \brief Convert a rotation matrix to Euler Angle form.
    void FromRotationMatrix(const BeRotationMatrix &mRotationMatrix);
  
    /// \brief New BeEulerAngles with values to 0.0f
    static BeEulerAngles ZERO();

};

#include <BeEulerAngles.inl>

#endif // #ifndef BE_BEEULERANGLES_H

