####################################################################
##
## BAK ENGINE 3
## BUILD SYSTEM
## 
## RegenProjectFiles.rb
## This ruby file generates platform project files automatically
##
## Javier Calet Toledo - 2013
##
###################################################################

#!/bin/sh

require_relative 'RegenLibraryFiles'
require_relative 'RegenVSFiles'
require_relative 'RegeniOSFiles'
require_relative 'RegenAndroidFiles'

###################################################################

if not $g_bGeneratingExternalLib
	# We read the BE3 libraries properties
	require_relative "../../../BeLibs/BeLibsConfig"
	
	# We read the 3rdParty libraries properties
	require_relative "../../../BeLibs3rdParty/3rdPartyLibsConfig"
end

# Searches for main project properties
$g_bThereIsMainProject = false
strSearchingPath = Dir.pwd + "/RegenProjectConfig.rb"
if File.exists?(strSearchingPath)
    if File.file?(strSearchingPath)
        require_relative strSearchingPath

		strCurrentDir = Dir.pwd
		iLastSeparatorIndex = (Dir.pwd.rindex '/') + 1
		strMainProjectName = strCurrentDir[iLastSeparatorIndex..strCurrentDir.length]

        # We force the library dependency to BE3 libraries
		if not $g_kProjectPropertiesConfig[$g_kProjectPropertiesConfig.length - 1]["isExternalLibrary"]
			$g_kProjectPropertiesConfig[$g_kProjectPropertiesConfig.length - 1]["projectName"] = strMainProjectName
			$g_kProjectPropertiesConfig[$g_kProjectPropertiesConfig.length - 1]["projectPath"] = Dir.pwd + "/../"
			$g_kProjectPropertiesConfig[$g_kProjectPropertiesConfig.length - 1]["projectDependencies"] = [BE3_MAIN_LIBRARY]
		end
        
        $g_bThereIsMainProject = true
    end
end

# Then we convert the HASH data to ProjectProperties
for i in 0..1

	kCurrentProjectPropertiesConfigArray = nil
	if i == 0
		kCurrentProjectPropertiesConfigArray = $g_kProjectPropertiesConfig
	else
		kCurrentProjectPropertiesConfigArray = $g_k3rdPartyLibsPropertiesConfig
	end

	kCurrentProjectPropertiesConfigArray.each do |kProjectPropertiesConfig|
		kProjectProperties = ProjectProperties.new
		
		# Common project stuff
		if kProjectPropertiesConfig.has_key?("projectName")
			kProjectProperties.m_strProjectName = kProjectPropertiesConfig["projectName"]
		end
		if kProjectPropertiesConfig.has_key?("projectPath")
			kProjectProperties.m_strProjectPath = kProjectPropertiesConfig["projectPath"]
		end
		if kProjectPropertiesConfig.has_key?("isMainProject")
			kProjectProperties.m_bIsMainProject = kProjectPropertiesConfig["isMainProject"]
		end
		if kProjectPropertiesConfig.has_key?("is3rdPartyLib")
			kProjectProperties.m_bIs3rdPartyLib = kProjectPropertiesConfig["is3rdPartyLib"]
		end
		if kProjectPropertiesConfig.has_key?("isLibrary")
			kProjectProperties.m_bIsLibrary = kProjectPropertiesConfig["isLibrary"]
		end
		if kProjectPropertiesConfig.has_key?("isExternalLibrary")
			kProjectProperties.m_bIsExternalLibrary = kProjectPropertiesConfig["isExternalLibrary"]
		end
		if kProjectPropertiesConfig.has_key?("isFinalLibrary")
			kProjectProperties.m_bIsFinalLibrary = kProjectPropertiesConfig["isFinalLibrary"]
			if kProjectProperties.m_bIsFinalLibrary
				$g_kFinalLibraryName = kProjectProperties.m_strProjectName
			end
		end
		if kProjectPropertiesConfig.has_key?("libraryType")
			kProjectProperties.m_LibraryType = kProjectPropertiesConfig["libraryType"]
		end
		if kProjectPropertiesConfig.has_key?("libraryLinkWhole")
			kProjectProperties.m_LibraryLinkWhole = kProjectPropertiesConfig["libraryLinkWhole"]
		end
		if kProjectPropertiesConfig.has_key?("projectPlatformTargets")
			kProjectProperties.m_kProjectPlatformTargets = kProjectPropertiesConfig["projectPlatformTargets"]
		end
		
		# Project compilation related
		if kProjectPropertiesConfig.has_key?("projectDependencies")
			kProjectProperties.setProjectDependencies kProjectPropertiesConfig["projectDependencies"]
		else
			kProjectProperties.setProjectDependencies []
		end
		if kProjectPropertiesConfig.has_key?("project3rdPartyDependencies")
			kProjectProperties.setProject3rdPartyDependencies kProjectPropertiesConfig["project3rdPartyDependencies"]
		else
			kProjectProperties.setProject3rdPartyDependencies []
		end
		if kProjectPropertiesConfig.has_key?("projectExternalDependencies")
			kProjectProperties.setProjectExternalDependencies kProjectPropertiesConfig["projectExternalDependencies"]
		else
			kProjectProperties.setProjectExternalDependencies []
		end
		if kProjectPropertiesConfig.has_key?("projectExternalDependenciesLinkWhole")
			kProjectProperties.setProjectExternalDependenciesLinkWhole kProjectPropertiesConfig["projectExternalDependenciesLinkWhole"]
		else
			kProjectProperties.setProjectExternalDependenciesLinkWhole []
		end
		if kProjectPropertiesConfig.has_key?("projectExports")
			kProjectProperties.m_strProjectExports = kProjectPropertiesConfig["projectExports"]
		end
		if kProjectPropertiesConfig.has_key?("extraIncludeDirs")
			kProjectProperties.m_strExtraIncludeDirs = kProjectPropertiesConfig["extraIncludeDirs"]
		end
		if kProjectPropertiesConfig.has_key?("OnlyCompileCodeFiles")
			kProjectProperties.m_kOnlyCompileCodeFiles = kProjectPropertiesConfig["OnlyCompileCodeFiles"]
		else
			kProjectProperties.m_kOnlyCompileCodeFiles = []
		end
		if kProjectPropertiesConfig.has_key?("AvoidCompileInclusiveCodeFiles")
			kProjectProperties.m_kAvoidCompileInclusiveCodeFiles = kProjectPropertiesConfig["AvoidCompileInclusiveCodeFiles"]
		else
			kProjectProperties.m_kAvoidCompileInclusiveCodeFiles = []
		end
		
		# Project deployment related
		if kProjectPropertiesConfig.has_key?("versionCode")
			kProjectProperties.m_strVersionCode = kProjectPropertiesConfig["versionCode"]
		end
		if kProjectPropertiesConfig.has_key?("versionName")
			kProjectProperties.m_strVersionName = kProjectPropertiesConfig["versionName"]
		end
		if kProjectPropertiesConfig.has_key?("outputFilename")
			kProjectProperties.m_strOutputFileName = kProjectPropertiesConfig["outputFilename"]
		end
		if kProjectPropertiesConfig.has_key?("forcePackageName")
			kProjectProperties.m_strForcedBundlePackage = kProjectPropertiesConfig["forcePackageName"]
		end
		if kProjectPropertiesConfig.has_key?("companyName")
			kProjectProperties.m_strCompanyName = kProjectPropertiesConfig["companyName"]
		end
		if kProjectPropertiesConfig.has_key?("gameName")
			kProjectProperties.m_strGameName = kProjectPropertiesConfig["gameName"]
		end
						
		# VisualStudio related
		if kProjectPropertiesConfig.has_key?("VSResourcesFolder")
			kProjectProperties.m_strVSResourcesFolder = kProjectPropertiesConfig["VSResourcesFolder"]
		end
		if kProjectPropertiesConfig.has_key?("projectVSAvoidLibraries")
			kProjectProperties.setVSAvoidLibraries kProjectPropertiesConfig["projectVSAvoidLibraries"]
		end
		if kProjectPropertiesConfig.has_key?("VSDisableWarnings")
			kProjectProperties.m_strVSDisableWarnings = kProjectPropertiesConfig["VSDisableWarnings"]
		end
		if kProjectPropertiesConfig.has_key?("VSDisableLinkWarnings")
			kProjectProperties.m_kVSDisableLinkWarnings = kProjectPropertiesConfig["VSDisableLinkWarnings"]
		else
			kProjectProperties.m_kVSDisableLinkWarnings = []
		end
		
		if kProjectPropertiesConfig.has_key?("VSAvoidCompile")
			kProjectProperties.m_kVSAvoidCompile = kProjectPropertiesConfig["VSAvoidCompile"]
		else
			kProjectProperties.m_kVSAvoidCompile = []
		end
			
		# iOS related
		if kProjectPropertiesConfig.has_key?("iOSFrameworkDependencies")
			kProjectProperties.setIOSFrameworkDependencies kProjectPropertiesConfig["iOSFrameworkDependencies"]
		else
			kProjectProperties.setIOSFrameworkDependencies []
		end
		if kProjectPropertiesConfig.has_key?("iOSFrameworkResources")
			kProjectProperties.setIOSFrameworkResources kProjectPropertiesConfig["iOSFrameworkResources"]
		else
			kProjectProperties.setIOSFrameworkResources []
		end
		if kProjectPropertiesConfig.has_key?("iOSAppLovinAppID")
			kProjectProperties.m_strIOSAppLovinAppID = kProjectPropertiesConfig["iOSAppLovinAppID"]
		end
		if kProjectPropertiesConfig.has_key?("iOSFaceBookAppID")
			kProjectProperties.m_strIOSFaceBookAppID = kProjectPropertiesConfig["iOSFaceBookAppID"]
		end
		if kProjectPropertiesConfig.has_key?("iOSFaceBookAppName")
			kProjectProperties.m_strIOSFaceBookAppName = kProjectPropertiesConfig["iOSFaceBookAppName"]
		end
		
		# Android related
		if kProjectPropertiesConfig.has_key?("androidJAROnlyClasspath")
			kProjectProperties.m_kAndroidJAROnlyClasspath = kProjectPropertiesConfig["androidJAROnlyClasspath"]
		else
			kProjectProperties.m_kAndroidJAROnlyClasspath = []
		end
		
		iMarket = MARKET_GOOGLE_PLAY
		if ARGV[3] != nil
			if ARGV[3].downcase == "amazon"
				iMarket = MARKET_AMAZON
			end
		end
		
		if kProjectPropertiesConfig.has_key?("androidGPlayABIs") and iMarket == MARKET_GOOGLE_PLAY
			kProjectProperties.m_strAndroidABI = kProjectPropertiesConfig["androidGPlayABIs"]
		end
		if kProjectPropertiesConfig.has_key?("androidAmazonABIs") and iMarket == MARKET_AMAZON
			kProjectProperties.m_strAndroidABI = kProjectPropertiesConfig["androidAmazonABIs"]
		end
		if kProjectPropertiesConfig.has_key?("androidGPlayExpansionFiles") and iMarket == MARKET_GOOGLE_PLAY
			kProjectProperties.m_kAndroidGPlayExpansionFiles = kProjectPropertiesConfig["androidGPlayExpansionFiles"]
		else
			kProjectProperties.m_kAndroidGPlayExpansionFiles = []
		end
		if kProjectPropertiesConfig.has_key?("androidTarget")
			kProjectProperties.m_strAndroidTarget = kProjectPropertiesConfig["androidTarget"]
		end
		if kProjectPropertiesConfig.has_key?("androidMinTarget")
			kProjectProperties.m_strAndroidMinTarget = kProjectPropertiesConfig["androidMinTarget"]
		end
		if kProjectPropertiesConfig.has_key?("androidActivity")
			kProjectProperties.m_strAndroidActivity = kProjectPropertiesConfig["androidActivity"]
		end
		if kProjectPropertiesConfig.has_key?("androidIconName")
			kProjectProperties.m_strAndroidIconName = kProjectPropertiesConfig["androidIconName"]
		end
		if kProjectPropertiesConfig.has_key?("androidDebugKeyStoreFilePath")
			kProjectProperties.m_strAndroidDebugKeyStoreFilePath = kProjectPropertiesConfig["androidDebugKeyStoreFilePath"]
		end
		if kProjectPropertiesConfig.has_key?("androidDebugKeyAlias")
			kProjectProperties.m_strAndroidDebugKeyAlias = kProjectPropertiesConfig["androidDebugKeyAlias"]
		end
		if kProjectPropertiesConfig.has_key?("androidDebugKeyStorePass")
			kProjectProperties.m_strAndroidDebugKeyStorePass = kProjectPropertiesConfig["androidDebugKeyStorePass"]
		end
		if kProjectPropertiesConfig.has_key?("androidReleaseKeyStoreFilePath")
			kProjectProperties.m_strAndroidReleaseKeyStoreFilePath = kProjectPropertiesConfig["androidReleaseKeyStoreFilePath"]
		end
		if kProjectPropertiesConfig.has_key?("androidReleaseKeyAlias")
			kProjectProperties.m_strAndroidReleaseKeyAlias = kProjectPropertiesConfig["androidReleaseKeyAlias"]
		end
		if kProjectPropertiesConfig.has_key?("androidReleaseKeyStorePass")
			kProjectProperties.m_strAndroidReleaseKeyStorePass = kProjectPropertiesConfig["androidReleaseKeyStorePass"]
		end
		
		# Android Manifest related
		if kProjectPropertiesConfig.has_key?("androidScreenOrientation")
			kProjectProperties.m_strAndroidScreenOrientation = kProjectPropertiesConfig["androidScreenOrientation"]
		end
	
		if i == 0
			$g_kProjectProperties.push kProjectProperties
		else
			$g_k3rdPartyLibsProperties.push kProjectProperties
		end
	end
	
end
	
###################################################################

def RegenProject(kProjectProperties, strCleanCommand)
	
	pLibraryFilesBuilder = RegenLibraryFiles.new
	pLibraryFilesBuilder.regenProject kProjectProperties, strCleanCommand
	
	if (::REGENERATE_PLATFORMS & ::PLATFORM_WINDOWS) > 0
		pVSFilesBuilder = RegenVSFiles.new
		pVSFilesBuilder.regenProject kProjectProperties, strCleanCommand
	end
	  
	if (::REGENERATE_PLATFORMS & ::PLATFORM_IOS) > 0
		pXcodeIOSFilesBuilder = RegenXcodeIOSFiles.new
		pXcodeIOSFilesBuilder.regenProject kProjectProperties ,strCleanCommand
	end
	  
	if (::REGENERATE_PLATFORMS & ::PLATFORM_ANDROID) > 0
		pAndroidFilesBuilder = RegenAndroidFiles.new
		pAndroidFilesBuilder.regenProject kProjectProperties, strCleanCommand
	end
	
end

###################################################################

def RegenProjectSolution(kProjectProperties, strCleanCommand)

	if (::REGENERATE_PLATFORMS & ::PLATFORM_WINDOWS) > 0
		pVSFilesBuilder = RegenVSFiles.new
		pVSFilesBuilder.regenSolution kProjectProperties, strCleanCommand
	end
	
	if (::REGENERATE_PLATFORMS & ::PLATFORM_IOS) > 0
		pXcodeIOSFilesBuilder = RegenXcodeIOSFiles.new
		pXcodeIOSFilesBuilder.regenSolution kProjectProperties, strCleanCommand
	end
	
end

###################################################################



###################################################################

def ExecRegenProject(strArg0, strArg1)
	
	kArray3rdPartyDependencyVisited = Array.new
	
	# GENERATES ALL THE PROJECT FILES
	$g_kProjectProperties.each do |kProjectProperties|
	
		# We force external dependencies to main project
		if kProjectProperties.m_bIsMainProject
			kProjectProperties.setProjectExternalDependencies ProjectProperties::s_kProjectExternalDependencies[BE3_MAIN_LIBRARY]
		end
		
		# GENERATES ALL THE PROJECT FILES FOR 3rdPARTY LIBRARIES
		ProjectProperties::s_kProject3rdPartyDependencies.each do |strProjectNameGlobal, kDependenciesArray|
			if (strProjectNameGlobal == kProjectProperties.m_strProjectName)
				if kDependenciesArray.length > 0
					kDependenciesArray.each do |strProjectDependencyName|							
						if not kArray3rdPartyDependencyVisited.include?(strProjectDependencyName)
							$g_k3rdPartyLibsProperties.each do |kProject3rdPartyProperties|
								if (strProjectDependencyName == kProject3rdPartyProperties.m_strProjectName)
									RegenProject kProject3rdPartyProperties, strArg1
								end
							end
							
							kArray3rdPartyDependencyVisited.push(strProjectDependencyName)
						end
					end
				end
			end
		end
		
		RegenProject kProjectProperties, strArg1
	end

	# GENERATES YOUR PROJECT SOLUTION
	if ($g_bThereIsMainProject)
		RegenProjectSolution $g_kProjectProperties[$g_kProjectProperties.length - 1], strArg1
	end

end

if OS::mac?
	ENV["BE3_SDK"] = BE3_SDK_DIR_SCRIPTS[0..(BE3_SDK_DIR_SCRIPTS.length-2)]
	ENV["ANDROID_SDK"] = "../ExternalTools/Android/mac/adt-bundle-mac-x86_64-20140702/sdk"
	ENV["ANDROID_NDK"] = "../ExternalTools/Android/mac/android-ndk-r9c"
else
	ENV["BE3_SDK"] = BE3_SDK_DIR_SCRIPTS[0..(BE3_SDK_DIR_SCRIPTS.length-2)].gsub('/', '\\')
	ENV["ANDROID_SDK"] = "..\\ExternalTools\\Android\\windows\\sdk\\android-sdk-windows"
	ENV["ANDROID_NDK"] = "..\\ExternalTools\\Android\\windows\\ndk\\android-ndk-r10d"
	ENV["JAVA_HOME"] = "..\\ExternalTools\\Java\\windows"
end

def ExecAndroidBuild(strArg0, strArg1, strArg2, strArg3)
	
	# EXECUTES ANDROID BUILD
	ExecuteAndroidCompilation strArg1, strArg2, strArg3

end

###################################################################

