####################################################################
##
## BAK ENGINE 3
## BUILD SYSTEM
## 
## RegenAndroidFiles.rb
## This ruby file generates android project files automatically
##
###################################################################

#!/bin/sh

$g_kLibraryReferenceVisited = Array.new
$g_kExternalPrebuiltLibrariesVisited = Array.new

$g_selectedMarketAPK = MARKET_GOOGLE_PLAY

$g_bToolsWrittenToFile = false

$g_bAndroidSignProxyServer = ""
$g_bAndroidSignProxyPort = ""

# RegenAndroidFiles Class
class RegenAndroidFiles

	@@iDependenciesCount = 0

	attr_accessor :m_outputFile
	attr_accessor :m_strDataToWrite
	attr_accessor :m_strFileHeader
	attr_accessor :m_writingToMemory
	attr_accessor :m_strMemoryData
	attr_accessor :m_bIsFirstMemoryProject

	def initialize()
		self.m_outputFile = nil
		self.m_strDataToWrite = ""
		self.m_strFileHeader ="\#
\# Bak Engine 3 Build System auto-generated file
\#

"
		self.m_writingToMemory = false
		self.m_strMemoryData = ""
		self.m_bIsFirstMemoryProject = false
    end

	# Writes m_strDataToWrite content to m_outputFile and clean it
	private
	def writeDataToFile
		if self.m_writingToMemory == true
			self.m_strMemoryData = self.m_strMemoryData + (self.m_strDataToWrite + "\n").force_encoding('utf-8')
		else
			self.m_outputFile.puts self.m_strDataToWrite.force_encoding('utf-8')
		end
		self.m_strDataToWrite = ""
	end

	public
	def SetWritingToMemory(bValue)
		self.m_writingToMemory = bValue;
		if self.m_writingToMemory == true
			self.m_strMemoryData = ""
		end
	end

	# Generates the AndroidManifest XML file
	public
	def generateAndroidManifestXMLFile(kProjectProperties)

		if kProjectProperties.m_bIsMainProject
			puts "Building #{$g_kPlatformNames[PLATFORM_ANDROID]} " + kProjectProperties.m_strOutputFileName + " AndroidManifest.xml..."
		else
			puts "Building #{$g_kPlatformNames[PLATFORM_ANDROID]} " + kProjectProperties.m_strProjectName + " AndroidManifest.xml..."
		end
		
		strAndroidManifestXMLFile = "AndroidManifest.xml"
		if kProjectProperties.m_bIsMainProject
			strAndroidManifestXMLFilePath = kProjectProperties.m_strProjectPath + "/" + kProjectProperties.m_strProjectName + PROJECT_FILES_PATH + "/" + strAndroidManifestXMLFile
		else
			strAndroidManifestXMLFilePath = BE3_SDK_DIR_SCRIPTS + kProjectProperties.m_strProjectPath + "/" + kProjectProperties.m_strProjectName + PROJECT_FILES_PATH + "/" + strAndroidManifestXMLFile
		end

		self.m_outputFile = File.new(strAndroidManifestXMLFilePath, "w")

		self.m_strDataToWrite = "<?xml version=\"1.0\" encoding=\"utf-8\"?>
<manifest xmlns:android=\"http://schemas.android.com/apk/res/android\""
		writeDataToFile
		
		if $g_selectedMarketAPK == MARKET_AMAZON
			self.m_strDataToWrite = "  xmlns:amazon=\"http://schemas.amazon.com/apk/res/android\""
			writeDataToFile
		end

		if kProjectProperties.m_bIsMainProject == false
			self.m_strDataToWrite = "  package=\"org.bakengine\"
  >"
			writeDataToFile
		else
			strFinalPackageName = ""
			if kProjectProperties.m_strForcedBundlePackage != nil
				strFinalPackageName = kProjectProperties.m_strForcedBundlePackage
			else
				strFinalPackageName = "com.#{kProjectProperties.m_strCompanyName.downcase}.#{kProjectProperties.m_strOutputFileName.downcase}"
			end
		
			self.m_strDataToWrite = "  package=\"#{strFinalPackageName}\"
  android:installLocation=\"auto\"
  android:versionCode=\"#{kProjectProperties.m_strVersionCode}\"
  android:versionName=\"#{kProjectProperties.m_strVersionName}\"
  >"
			writeDataToFile
		end

		writeDataToFile

		if kProjectProperties.m_bIsMainProject == true

			strSDKAndroidVersionNumber = kProjectProperties.m_strAndroidTarget
			strSDKAndroidVersionNumber = strSDKAndroidVersionNumber.gsub("android-", "")
			
			strSDKAndroidMinVersionNumber = kProjectProperties.m_strAndroidMinTarget
			strSDKAndroidMinVersionNumber = strSDKAndroidMinVersionNumber.gsub("android-", "")
			
			self.m_strDataToWrite = "  <uses-sdk 
    android:minSdkVersion=\"#{strSDKAndroidMinVersionNumber}\"
    android:targetSdkVersion=\"#{strSDKAndroidVersionNumber}\"
  />"
			writeDataToFile
			
			self.m_strDataToWrite = "  <uses-feature android:glEsVersion=\"0x00020000\"/>"
			writeDataToFile
			writeDataToFile
			
			self.m_strDataToWrite = "  <application
	android:label=\"@string/app_name\" 
	android:icon=\"@drawable/#{kProjectProperties.m_strAndroidIconName}\""
	
			if $g_selectedMarketAPK == MARKET_GOOGLE_PLAY
				self.m_strDataToWrite += "\n	android:theme=\"@style/MyTheme\""
			elsif $g_selectedMarketAPK == MARKET_AMAZON
				#self.m_strDataToWrite += "\n	android:theme=\"@android:style/Theme.NoTitleBar.Fullscreen\""
				self.m_strDataToWrite += "\n	android:theme=\"@style/MyTheme\""
			end
			
			self.m_strDataToWrite += "\n    android:hasCode=\"true\"
	android:allowBackup=\"false\"
    android:debuggable=\"true\">"
			writeDataToFile
			writeDataToFile

			self.m_strDataToWrite = "    <activity
	  android:name=\"#{kProjectProperties.m_strAndroidActivity}\"
	  android:label=\"@string/app_name\"
      android:configChanges=\"orientation|keyboardHidden\""
			writeDataToFile
	  
			if kProjectProperties.m_strAndroidScreenOrientation
				self.m_strDataToWrite = "	  android:screenOrientation=\"#{kProjectProperties.m_strAndroidScreenOrientation}\""
				writeDataToFile
			end
	  
			self.m_strDataToWrite = "	  android:uiOptions=\"none\"
      android:launchMode=\"singleTask\">"
			writeDataToFile
			writeDataToFile

			self.m_strDataToWrite = "      <!-- Shared library .so name -->
      <meta-data android:name=\"android.app.lib_name\"
                 android:value=\"#{kProjectProperties.m_strOutputFileName}\" />"
			writeDataToFile
			writeDataToFile

			self.m_strDataToWrite = "      <intent-filter>
  	    <action android:name=\"android.intent.action.MAIN\" />
        <category android:name=\"android.intent.category.LAUNCHER\" />
      </intent-filter>"
			writeDataToFile
			writeDataToFile
			
			self.m_strDataToWrite = "    </activity>"
			writeDataToFile
			writeDataToFile

			if $g_bAndroidUsingAppLovin
				self.m_strDataToWrite = "	<meta-data android:name=\"applovin.sdk.key\" 
	   android:value=\"MtArBKFixStQ3-movFuRLmpGdg4i2xiPotE-CTg7wTT-XJnRTeuvOg91LEkOAQoyI0xVUcdPAANhT881o0xAg4\"  />"
				writeDataToFile
				writeDataToFile
			end
			
			self.m_strDataToWrite = "    <activity android:name=\"org.bakengine.BeAlertDialog\"
              android:theme=\"@android:style/Theme.Translucent.NoTitleBar\" android:label=\"@string/app_name\" />"
			writeDataToFile
			writeDataToFile
			
			if $g_bAndroidUsingGameServices
			
				strFinalPackageName = ""
				if kProjectProperties.m_strForcedBundlePackage != nil
					strFinalPackageName = kProjectProperties.m_strForcedBundlePackage
				else
					strFinalPackageName = "com.#{kProjectProperties.m_strCompanyName.downcase}.#{kProjectProperties.m_strOutputFileName.downcase}"
				end
			
				if $g_selectedMarketAPK == MARKET_GOOGLE_PLAY
			
					self.m_strDataToWrite = "    <meta-data android:name=\"com.google.android.gms.games.APP_ID\"
			  android:value=\"@string/app_google_id\" />"
					writeDataToFile
					self.m_strDataToWrite = "    <meta-data android:name=\"com.google.android.gms.appstate.APP_ID\"
			  android:value=\"@string/app_google_id\" />"
					writeDataToFile
					self.m_strDataToWrite = "    <meta-data android:name=\"com.google.android.gms.version\"
			  android:value=\"@integer/google_play_services_version\" />"
					writeDataToFile
					writeDataToFile
				
				elsif $g_selectedMarketAPK == MARKET_AMAZON
				
					self.m_strDataToWrite = "    <activity android:name=\"com.amazon.ags.html5.overlay.GameCircleUserInterface\" android:theme=\"@style/GCOverlay\" />
	
	<activity
		android:name=\"com.amazon.identity.auth.device.authorization.AuthorizationActivity\"
		android:theme=\"@android:style/Theme.NoDisplay\"
		android:allowTaskReparenting=\"true\"
		android:launchMode=\"singleTask\">
		<intent-filter>
			<action android:name=\"android.intent.action.VIEW\" />
				<category android:name=\"android.intent.category.DEFAULT\" />
				<category android:name=\"android.intent.category.BROWSABLE\" />
				<data android:host=\"#{strFinalPackageName}\" android:scheme=\"amzn\" />
		</intent-filter>
	</activity>

	<activity android:name=\"com.amazon.ags.html5.overlay.GameCircleAlertUserInterface\" android:theme=\"@style/GCAlert\" />
		
	<receiver
		android:name=\"com.amazon.identity.auth.device.authorization.PackageIntentReceiver\"
		android:enabled=\"true\">
		<intent-filter>
			<action android:name=\"android.intent.action.PACKAGE_INSTALL\" />
			<action android:name=\"android.intent.action.PACKAGE_ADDED\" />
			<data android:scheme=\"package\" />
		</intent-filter>
	</receiver>"
					writeDataToFile
					writeDataToFile
				end
			end
			
			if $g_bAndroidUsingFacebook
				self.m_strDataToWrite = "    <activity android:name=\"com.facebook.LoginActivity\"
              android:theme=\"@android:style/Theme.Translucent.NoTitleBar\" android:label=\"@string/app_name\" />
			  <meta-data android:name=\"com.facebook.sdk.ApplicationId\" android:value=\"@string/app_id\"/>"
				writeDataToFile
				writeDataToFile
			end
			if $g_bAndroidUsingTwitter
				self.m_strDataToWrite = "    <activity android:name=\"org.bakengine.TwitterWebviewActivity\">
      <intent-filter>
  	    <action android:name=\"android.intent.action.VIEW\" />
        <category android:name=\"android.intent.category.DEFAULT\" />
		<category android:name=\"android.intent.category.BROWSABLE\" />
        <data android:scheme=\"callback\" />
      </intent-filter>
    </activity>"
				writeDataToFile
				writeDataToFile
			end
			if $g_bAndroidUsingSplashScreen
				self.m_strDataToWrite = "    <!-- Splash screen -->
	<activity
		android:name=\"org.bakengine.BeSplashScreenActivity\"
        android:label=\"@string/app_name\"
		android:configChanges=\"orientation|keyboardHidden\"
        android:screenOrientation=\"sensorLandscape\"
        android:theme=\"@android:style/Theme.Black.NoTitleBar\" >
    </activity>"
				writeDataToFile
				writeDataToFile
			end
			if $g_bAndroidUsingAppLovin
				self.m_strDataToWrite = "    <activity android:name=\"com.applovin.adview.AppLovinInterstitialActivity\" />
    <activity android:name=\"com.applovin.adview.AppLovinConfirmationActivity\" />"
				writeDataToFile
			end
			
			if $g_bAndroidUsingChartBoost
				self.m_strDataToWrite = "    <activity android:name=\"com.chartboost.sdk.CBImpressionActivity\" android:theme=\"@android:style/Theme.Translucent.NoTitleBar\" android:excludeFromRecents=\"true\" />"
				writeDataToFile
			end
			
			if $g_bAndroidUsingInAppPurchases
				if $g_selectedMarketAPK == MARKET_AMAZON
					self.m_strDataToWrite = "    <receiver android:name = \"com.amazon.device.iap.ResponseReceiver\" >
		<intent-filter>
			<action android:name = \"com.amazon.inapp.purchasing.NOTIFY\"
					android:permission = \"com.amazon.inapp.purchasing.Permission.NOTIFY\" />
		</intent-filter>
	</receiver>"
					writeDataToFile
				end
				writeDataToFile
			end
			
			if $g_bAndroidUsingPushNotifications or $g_bAndroidUsingLocalNotifications
			
				strFinalPackageName = ""
				if kProjectProperties.m_strForcedBundlePackage != nil
					strFinalPackageName = kProjectProperties.m_strForcedBundlePackage
				else
					strFinalPackageName = "com.#{kProjectProperties.m_strCompanyName.downcase}.#{kProjectProperties.m_strOutputFileName.downcase}"
				end
			
				self.m_strDataToWrite = "    <!-- Push/Local notifications -->
	<receiver android:name=\"org.bakengine.BeNotificationReceiver\" android:enabled=\"true\">
        <intent-filter>"
				writeDataToFile
				
				if $g_bAndroidUsingPushNotifications
					self.m_strDataToWrite = "			<action android:name=\"#{strFinalPackageName}.intent.action.PUSH_NOTIFICATION\" />"
					writeDataToFile
				end
				if $g_bAndroidUsingLocalNotifications
					self.m_strDataToWrite = "			<action android:name=\"#{strFinalPackageName}.intent.action.LOCAL_NOTIFICATION\" />"
					writeDataToFile
				end
				if $g_bAndroidUsingPushNotifications or $g_bAndroidUsingLocalNotifications
					self.m_strDataToWrite = "			<action android:name=\"#{strFinalPackageName}.intent.action.PRESSED_NOTIFICATION\" />"
					writeDataToFile
					self.m_strDataToWrite = "			<action android:name=\"#{strFinalPackageName}.intent.action.DELETE_NOTIFICATION\" />"
					writeDataToFile
				end
				
				self.m_strDataToWrite = "		</intent-filter>
	</receiver>
	<service android:name=\"org.bakengine.BeNotificationReceiverService\" />"
				writeDataToFile
				writeDataToFile
				
			end
			
			if $g_bAndroidUsingPushNotifications
			
				strFinalPackageName = ""
				if kProjectProperties.m_strForcedBundlePackage != nil
					strFinalPackageName = kProjectProperties.m_strForcedBundlePackage
				else
					strFinalPackageName = "com.#{kProjectProperties.m_strCompanyName.downcase}.#{kProjectProperties.m_strOutputFileName.downcase}"
				end
			
				if $g_selectedMarketAPK == MARKET_GOOGLE_PLAY
				
					self.m_strDataToWrite = "	<!-- GCM Push notifications -->
	<receiver android:name=\"org.bakengine.GcmBroadcastReceiver\" android:permission=\"com.google.android.c2dm.permission.SEND\" >
		<intent-filter>
			<action android:name=\"com.google.android.c2dm.intent.RECEIVE\" />
			<action android:name=\"com.google.android.c2dm.intent.REGISTRATION\" />
			<category android:name=\"#{strFinalPackageName}\" />
		</intent-filter>
	</receiver>
	<service android:name=\"org.bakengine.BePNGoogleIntentService\" />"
					writeDataToFile
					writeDataToFile
				
				elsif $g_selectedMarketAPK == MARKET_AMAZON
					
					self.m_strDataToWrite = "	<!-- ADM Push notifications -->
	<amazon:enable-feature android:name=\"com.amazon.device.messaging\" android:required=\"false\" />
	
	<receiver android:name=\"org.bakengine.BePNAmazonIntentService$Be3PNAmazonReceiver\" android:permission=\"com.amazon.device.messaging.permission.SEND\">
		<intent-filter>
			<action android:name=\"com.amazon.device.messaging.intent.REGISTRATION\" />
			<action android:name=\"com.amazon.device.messaging.intent.RECEIVE\" />
			<category android:name=\"#{strFinalPackageName}\"/>
		</intent-filter>
	</receiver>
	
	<service android:name=\"org.bakengine.BePNAmazonIntentService\" android:exported=\"false\" />"
					writeDataToFile
					writeDataToFile		
				end
			
			end
		
			if $g_bAndroidUsingLVL
				if $g_selectedMarketAPK == MARKET_GOOGLE_PLAY
					self.m_strDataToWrite = "    <service android:name=\"org.bakengine.BeOBBDownloaderService\" />"
					writeDataToFile
					self.m_strDataToWrite = "    <receiver android:name=\"org.bakengine.BeOBBAlarmReceiver\" />"
					writeDataToFile
				end
			end
			
			if $g_bAndroidUsingAdColony
				self.m_strDataToWrite = "    <activity android:name=\"com.jirbo.adcolony.AdColonyOverlay\" android:configChanges=\"keyboardHidden|orientation|screenSize\" android:theme=\"@android:style/Theme.Translucent.NoTitleBar.Fullscreen\" />
    <activity android:name=\"com.jirbo.adcolony.AdColonyFullscreen\" android:configChanges=\"keyboardHidden|orientation|screenSize\" android:theme=\"@android:style/Theme.Black.NoTitleBar.Fullscreen\" />
	<activity android:name=\"com.jirbo.adcolony.AdColonyBrowser\" android:configChanges=\"keyboardHidden|orientation|screenSize\" android:theme=\"@android:style/Theme.Black.NoTitleBar.Fullscreen\" />"
				writeDataToFile
			end

			self.m_strDataToWrite = "  </application>"
			writeDataToFile
			writeDataToFile

			if $g_kAndroidCompatibleScreens.size > 0
				self.m_strDataToWrite = "  <compatible-screens>"
				writeDataToFile
				$g_kAndroidCompatibleScreens.each do |strScreenSize, kScreenDensities|
					kScreenDensities.each do |strScreenDensity|
						self.m_strDataToWrite = "    <screen android:screenSize=\"#{strScreenSize}\" android:screenDensity=\"#{strScreenDensity}\" />"
						writeDataToFile
					end
				end
				self.m_strDataToWrite = "  </compatible-screens>"
				writeDataToFile
				writeDataToFile
			end
			
			if $g_kAndroidManifestPermissions.size > 0
				$g_kAndroidManifestPermissions.each do |strPermissionName, kPermissionData|
				
					bAvoidToAddPermission = false
					
					if $g_selectedMarketAPK == MARKET_AMAZON
						if strPermissionName == "READ_PHONE_STATE"
							bAvoidToAddPermission = true
						end
					end
					
					if not bAvoidToAddPermission
						if kPermissionData[0]
							self.m_strDataToWrite = "  <!-- #{kPermissionData[1]} -->"
							writeDataToFile
							self.m_strDataToWrite = "  <uses-permission android:name=\"android.permission.#{strPermissionName}\" />"
							writeDataToFile
						end
					end
					
				end
				writeDataToFile
			end
			if $g_bAndroidUsingInAppPurchases
				if $g_selectedMarketAPK == MARKET_GOOGLE_PLAY
					self.m_strDataToWrite = "  <uses-permission android:name=\"com.android.vending.BILLING\" />"
					writeDataToFile
				end
				writeDataToFile
			end
			if $g_bAndroidUsingLVL
				if $g_selectedMarketAPK == MARKET_GOOGLE_PLAY
					self.m_strDataToWrite = "  <uses-permission android:name=\"com.android.vending.CHECK_LICENSE\" />"
					writeDataToFile
				end
				writeDataToFile
			end
			if $g_bAndroidUsingFacebook
				self.m_strDataToWrite = "  <uses-permission android:name=\"com.facebook.permission.prod.FB_APP_COMMUNICATION\" />"
				writeDataToFile
				writeDataToFile
			end
			if $g_bAndroidUsingPushNotifications
			
				strFinalPackageName = ""
				if kProjectProperties.m_strForcedBundlePackage != nil
					strFinalPackageName = kProjectProperties.m_strForcedBundlePackage
				else
					strFinalPackageName = "com.#{kProjectProperties.m_strCompanyName.downcase}.#{kProjectProperties.m_strOutputFileName.downcase}"
				end
			
				if $g_selectedMarketAPK == MARKET_GOOGLE_PLAY
				
					self.m_strDataToWrite = "  <!-- App receives GCM messages. -->
  <uses-permission android:name=\"com.google.android.c2dm.permission.RECEIVE\" />"
					writeDataToFile
					writeDataToFile
				
					self.m_strDataToWrite = "  <!-- Creates a custom permission so only this app can receive its messages. --> 
  <permission android:name=\"#{strFinalPackageName}.permission.C2D_MESSAGE\" android:protectionLevel=\"signature\" />
  <uses-permission android:name=\"#{strFinalPackageName}.permission.C2D_MESSAGE\" />"
					writeDataToFile
					
				elsif $g_selectedMarketAPK == MARKET_AMAZON
					
					self.m_strDataToWrite = "  <!-- This permission allows your app access to receive push notifications from ADM. -->
  <uses-permission android:name=\"com.amazon.device.messaging.permission.RECEIVE\" />"
					writeDataToFile
					writeDataToFile
				
					self.m_strDataToWrite = "  <!-- This permission ensures that no other application can intercept your ADM messages. It
  should have the form packagename.permission.RECIEVE_ADM_MESSAGE where packagename is the
  name defined in the \"package\" property of the manifest tag. -->
  <permission android:name=\"#{strFinalPackageName}.permission.RECEIVE_ADM_MESSAGE\" android:protectionLevel=\"signature\"/>"
					writeDataToFile
					writeDataToFile
  
					self.m_strDataToWrite = "  <!-- Required permissions -->
  <uses-permission android:name=\"#{strFinalPackageName}.permission.RECEIVE_ADM_MESSAGE\" />"
					writeDataToFile
					
				end
				
				writeDataToFile
			end
		end

		# Write permissions

		self.m_strDataToWrite = "</manifest>"
		writeDataToFile

		self.m_outputFile.close

	end

	# Searches and adds the files for a specific filter
	private
	def searchSourceFilesForAndroidMake(strBasePathToFiles, strIncrementalPathToFiles, strIncludeFolderName, kFiltersArray)
	
		strSearchingPath = strBasePathToFiles + strIncrementalPathToFiles
             
		if Dir.exists?(strSearchingPath)
			Dir.foreach(strSearchingPath) do |strItem|
				next if strItem == '.' or strItem == '..' or strItem == strIncludeFolderName
				
				strItemTmp = "/%1"
				strItemTmp = strItemTmp.gsub("%1", strItem)
				strFinalFolder = strSearchingPath + strItemTmp
				if File.directory?(strFinalFolder)
					searchSourceFilesForAndroidMake strBasePathToFiles, strIncrementalPathToFiles + strItemTmp, strIncludeFolderName, kFiltersArray
				end
			end
		end

		strSearchingPath = strBasePathToFiles + strIncrementalPathToFiles	
		if Dir.exists?(strSearchingPath)
			Dir.foreach(strSearchingPath) do |strItem|
				next if strItem == '.' or strItem == '..' or strItem == strIncludeFolderName
				
				if kFiltersArray.length > 0
					bFilterFound = false
					kFiltersArray.each do |strFilter|
						if strItem.end_with?(strFilter) == true
							bFilterFound = true
							break
						end
					end
					next if bFilterFound == false
				end

				strFinalFolder = strSearchingPath + "/" + strItem
			
				if File.file?(strFinalFolder)
					self.m_strDataToWrite = "  %1/%2 \\"
					self.m_strDataToWrite = self.m_strDataToWrite.gsub("%1", strIncrementalPathToFiles)
					self.m_strDataToWrite = self.m_strDataToWrite.gsub("%2", strItem)
					writeDataToFile
				end
			end
		end

	end

	# Searches and adds the include folders into the make file
	private
	def searchIncludeFoldersForAndroidMake(strBasePathToFiles, strIncrementalPathToFiles)
	
		strSearchingPath = strBasePathToFiles + strIncrementalPathToFiles
             
		if Dir.exists?(strSearchingPath)
			Dir.foreach(strSearchingPath) do |strItem|
				next if strItem == '.' or strItem == '..'
				
				strItemTmp = "/%1"
				strItemTmp = strItemTmp.gsub("%1", strItem)
				strFinalFolder = strSearchingPath + strItemTmp
				
				if not File.file?(strFinalFolder)
					self.m_strDataToWrite = "  $(LOCAL_PATH)/%1/%2 \\"
					self.m_strDataToWrite = self.m_strDataToWrite.gsub("%1", strIncrementalPathToFiles)
					self.m_strDataToWrite = self.m_strDataToWrite.gsub("%2", strItem)
					writeDataToFile
				end
				
				if File.directory?(strFinalFolder)
					searchIncludeFoldersForAndroidMake strBasePathToFiles, strIncrementalPathToFiles + strItemTmp
				end
			end
		end
		
	end
	
	# Generates the Android make file
	private
	def generateAndroidMakeFile(kProjectProperties)

		if kProjectProperties.m_bIsMainProject
			puts "Building #{$g_kPlatformNames[PLATFORM_ANDROID]} " + kProjectProperties.m_strOutputFileName + " Android.mk..."
		else
			puts "Building #{$g_kPlatformNames[PLATFORM_ANDROID]} " + kProjectProperties.m_strProjectName + " Android.mk..."
		end

		strAndroidMakeFile = "Android.mk"
		if kProjectProperties.m_bIsMainProject
			strAndroidMakeFilePath = kProjectProperties.m_strProjectPath + kProjectProperties.m_strProjectName + PROJECT_FILES_PATH + "/" + strAndroidMakeFile
		else
			strAndroidMakeFilePath = BE3_SDK_DIR_SCRIPTS + kProjectProperties.m_strProjectPath + kProjectProperties.m_strProjectName + PROJECT_FILES_PATH + "/" + strAndroidMakeFile
		end

        strFinalProjectFileName = kProjectProperties.m_strProjectName
		if kProjectProperties.m_strOutputFileName != nil
			strFinalProjectFileName = kProjectProperties.m_strOutputFileName
		end

		self.m_outputFile = File.new(strAndroidMakeFilePath, "w")

		self.m_strDataToWrite = self.m_strFileHeader
		writeDataToFile
		
		if kProjectProperties.m_bIsMainProject
			self.m_strDataToWrite = "ifeq ($(findstring #{kProjectProperties.m_strOutputFileName}, $(APP_MODULES)), #{kProjectProperties.m_strOutputFileName})\n\n"
		else
			self.m_strDataToWrite = "ifeq ($(findstring #{kProjectProperties.m_strProjectName}, $(APP_MODULES)), #{kProjectProperties.m_strProjectName})\n\n"
		end
		writeDataToFile
		
		self.m_strDataToWrite = "LOCAL_PATH := $(call my-dir)/..\n\n"
		writeDataToFile
		
		# We add external libs as PREBUILT_STATIC_LIBRARY
		ProjectProperties::s_kProjectExternalDependencies.each do |strProjectNameGlobal, kDependenciesArray|
			if (strProjectNameGlobal == kProjectProperties.m_strProjectName)
				if kDependenciesArray.length > 0
					kDependenciesArray.each do |strProjectDependencyName|
					
						if not $g_kExternalPrebuiltLibrariesVisited.include?(strProjectDependencyName)
							
							bABIFilesExist = true
							kABIs = kProjectProperties.m_strAndroidABI.split
							kABIs.each do |strABI|
							
								strFinalABI = strABI
								if strFinalABI.include? "arm"
									strFinalABI = "arm"
								end
											
								strCheckLibFilePath = "#{BE3_SDK_DIR_SCRIPTS}External/#{strProjectDependencyName}/lib/Android/#{strFinalABI}/lib#{strProjectDependencyName}.a"
								if not File.exists? strCheckLibFilePath
									bABIFilesExist = false
									break
								end
								
							end
							
							if bABIFilesExist
							
								self.m_strDataToWrite = "include $(CLEAR_VARS)\n"
								writeDataToFile
								
								self.m_strDataToWrite = "LOCAL_MODULE          := #{strProjectDependencyName}_base\n"
								writeDataToFile
								self.m_strDataToWrite = "LOCAL_MODULE_FILENAME := #{strProjectDependencyName}_base_static\n"
								writeDataToFile
								if kProjectProperties.m_bIsMainProject
									self.m_strDataToWrite = "LOCAL_SRC_FILES := $(LOCAL_PATH)/#{BE3_SDK_DIR_SCRIPTS}External/#{strProjectDependencyName}/lib/Android/$(TARGET_ARCH)/lib#{strProjectDependencyName}.a\n"
								else
									self.m_strDataToWrite = "LOCAL_SRC_FILES := ../../External/#{strProjectDependencyName}/lib/Android/$(TARGET_ARCH)/lib#{strProjectDependencyName}.a\n"
								end
								writeDataToFile
								
								self.m_strDataToWrite = "include $(PREBUILT_STATIC_LIBRARY)\n\n"
								writeDataToFile
							
							end
							
							$g_kExternalPrebuiltLibrariesVisited.push(strProjectDependencyName)
						end
						
					end
				end
			end
		end
		
		self.m_strDataToWrite = "include $(CLEAR_VARS)\n\n"
		writeDataToFile

		if kProjectProperties.m_bIsMainProject
			self.m_strDataToWrite = "LOCAL_MODULE := #{kProjectProperties.m_strOutputFileName}\n\n"
		else
			self.m_strDataToWrite = "LOCAL_MODULE := #{kProjectProperties.m_strProjectName}\n\n"
		end
		writeDataToFile

		self.m_strDataToWrite = "ifeq ($(APP_OPTIM), debug)\n  LOCAL_MODULE_FILENAME := lib#{strFinalProjectFileName}\nelse\n  LOCAL_MODULE_FILENAME := lib#{strFinalProjectFileName}\nendif\n\n"
		writeDataToFile

		self.m_strDataToWrite = "LOCAL_SRC_FILES := \\"
		writeDataToFile
		if kProjectProperties.m_bIsMainProject
			searchSourceFilesForAndroidMake kProjectProperties.m_strProjectPath + kProjectProperties.m_strProjectName + "/", "Android", kProjectProperties.m_strIncludeFolderName, [".c", ".cpp"]
			searchSourceFilesForAndroidMake kProjectProperties.m_strProjectPath + kProjectProperties.m_strProjectName + "/", "Mobile", kProjectProperties.m_strIncludeFolderName, [".c", ".cpp"]
			searchSourceFilesForAndroidMake kProjectProperties.m_strProjectPath + kProjectProperties.m_strProjectName + "/", "src", kProjectProperties.m_strIncludeFolderName, [".c", ".cpp"]
		else
			searchSourceFilesForAndroidMake BE3_SDK_DIR_SCRIPTS + kProjectProperties.m_strProjectPath + kProjectProperties.m_strProjectName + "/", "Android", kProjectProperties.m_strIncludeFolderName, [".c", ".cpp"]
			searchSourceFilesForAndroidMake BE3_SDK_DIR_SCRIPTS + kProjectProperties.m_strProjectPath + kProjectProperties.m_strProjectName + "/", "Mobile", kProjectProperties.m_strIncludeFolderName, [".c", ".cpp"]
			searchSourceFilesForAndroidMake BE3_SDK_DIR_SCRIPTS + kProjectProperties.m_strProjectPath + kProjectProperties.m_strProjectName + "/", "src", kProjectProperties.m_strIncludeFolderName, [".c", ".cpp"]
		end

		writeDataToFile

		strBe3LibIncludes = ""	
		if kProjectProperties.m_bIsMainProject
			kArrayDependencyVisited = Array.new
			ProjectProperties::s_kProjectDependencies.each do |strProjectNameGlobal, kDependenciesArray|
				if kDependenciesArray.length > 0
					kDependenciesArray.each do |strProjectDependencyName|							
						if not kArrayDependencyVisited.include?(strProjectDependencyName)
						
							dependencyProjectProperties = GetProjectProperties strProjectDependencyName
							if dependencyProjectProperties != nil
								if dependencyProjectProperties.m_kProjectPlatformTargets == nil or (dependencyProjectProperties.m_kProjectPlatformTargets != nil and dependencyProjectProperties.m_kProjectPlatformTargets.include? PLATFORM_ANDROID)
									strBe3LibIncludes += "\n  \$(LOCAL_PATH)/#{BE3_SDK_DIR_SCRIPTS}BeLibs/#{strProjectDependencyName}/src \\\n  \$(LOCAL_PATH)/#{BE3_SDK_DIR_SCRIPTS}BeLibs/#{strProjectDependencyName}/Android \\\n  \$(LOCAL_PATH)/#{BE3_SDK_DIR_SCRIPTS}BeLibs/#{strProjectDependencyName}/Mobile \\"
									kArrayDependencyVisited.push(strProjectDependencyName)
								end
							end
							
						end
					end
				end
			end
			ProjectProperties::s_kProject3rdPartyDependencies.each do |strProjectNameGlobal, kDependenciesArray|
				if kDependenciesArray.length > 0
					kDependenciesArray.each do |strProjectDependencyName|							
						if not kArrayDependencyVisited.include?(strProjectDependencyName)
						
							dependencyProjectProperties = GetProjectProperties strProjectDependencyName
							if dependencyProjectProperties != nil
								if dependencyProjectProperties.m_kProjectPlatformTargets == nil or (dependencyProjectProperties.m_kProjectPlatformTargets != nil and dependencyProjectProperties.m_kProjectPlatformTargets.include? PLATFORM_ANDROID)
									strBe3LibIncludes += "\n  \$(LOCAL_PATH)/#{BE3_SDK_DIR_SCRIPTS}3rdParty/#{strProjectDependencyName}/src \\\n  \$(LOCAL_PATH)/#{BE3_SDK_DIR_SCRIPTS}3rdParty/#{strProjectDependencyName}/Android \\\n  \$(LOCAL_PATH)/#{BE3_SDK_DIR_SCRIPTS}3rdParty/#{strProjectDependencyName}/Mobile \\"
									kArrayDependencyVisited.push(strProjectDependencyName)
								end
							end

						end
					end
				end
			end
		end
		
		strIncludeFolders = "\\\n  \$(LOCAL_PATH)/.. \\\n  \$(LOCAL_PATH)/src \\\n  \$(LOCAL_PATH)/Android \\\n  \$(LOCAL_PATH)/Mobile \\"
		if kProjectProperties.m_bIsMainProject
			strIncludeFolders += "\n  \$(LOCAL_PATH)/#{BE3_SDK_DIR_SCRIPTS}3rdParty \\\n  \$(LOCAL_PATH)/#{BE3_SDK_DIR_SCRIPTS}BeLibs \\"
		else
			if not kProjectProperties.m_bIs3rdPartyLib
				strIncludeFolders += "\n  \$(LOCAL_PATH)/../../3rdParty \\"
			end
		end
		
		strExternalIncludes = ""
		kArrayDependencyVisited = Array.new
		ProjectProperties::s_kProjectExternalDependencies.each do |strProjectNameGlobal, kDependenciesArray|
			if (strProjectNameGlobal == kProjectProperties.m_strProjectName) or kProjectProperties.m_bIsMainProject
				if kDependenciesArray.length > 0
					kDependenciesArray.each do |strProjectDependencyName|
					
						if not kProjectProperties.m_bIsMainProject or (kProjectProperties.m_bIsMainProject and not kArrayDependencyVisited.include?(strProjectDependencyName))
							if kProjectProperties.m_bIsMainProject 
								strExternalIncludes += "\n  $(LOCAL_PATH)/#{BE3_SDK_DIR_SCRIPTS}External/#{strProjectDependencyName}/include \\"
							else
								strExternalIncludes += "\n  $(LOCAL_PATH)/../../External/#{strProjectDependencyName}/include \\"
							end
							
							if kProjectProperties.m_bIsMainProject
								kArrayDependencyVisited.push(strProjectDependencyName)
							end
						end
						
					end
				end
			end
		end
			
		strAdditionalIncludes = ""
		if kProjectProperties.m_strExtraIncludeDirs != nil
			for includeFolder in kProjectProperties.m_strExtraIncludeDirs
				strAdditionalIncludes = strAdditionalIncludes + "\n  $(LOCAL_PATH)/#{includeFolder} \\"
			end
		end
		
		self.m_strDataToWrite = "LOCAL_C_INCLUDES := #{strIncludeFolders}"
		self.m_strDataToWrite = self.m_strDataToWrite + strBe3LibIncludes + strAdditionalIncludes + strExternalIncludes
		#searchIncludeFoldersForAndroidMake kProjectProperties.m_strProjectPath + kProjectProperties.m_strProjectName + "/", "Android"
		#searchIncludeFoldersForAndroidMake kProjectProperties.m_strProjectPath + kProjectProperties.m_strProjectName + "/", "Mobile"
		#searchIncludeFoldersForAndroidMake kProjectProperties.m_strProjectPath + kProjectProperties.m_strProjectName + "/", "src"
		writeDataToFile
		writeDataToFile
		
		self.m_strDataToWrite = "LOCAL_EXPORT_C_INCLUDES := #{strIncludeFolders}"
		self.m_strDataToWrite = self.m_strDataToWrite + strBe3LibIncludes + strAdditionalIncludes + strExternalIncludes
		#searchIncludeFoldersForAndroidMake kProjectProperties.m_strProjectPath + kProjectProperties.m_strProjectName + "/", "Android"
		#searchIncludeFoldersForAndroidMake kProjectProperties.m_strProjectPath + kProjectProperties.m_strProjectName + "/", "Mobile"
		#searchIncludeFoldersForAndroidMake kProjectProperties.m_strProjectPath + kProjectProperties.m_strProjectName + "/", "src"
		writeDataToFile
		writeDataToFile

		self.m_strDataToWrite = "ifeq ($(APP_OPTIM), debug)\n  LOCAL_CPP_FEATURES := rtti\nelse\n  LOCAL_CPP_FEATURES := rtti\nendif\n\n"
		writeDataToFile

		self.m_strDataToWrite = "ifeq ($(APP_OPTIM), debug)\n  LOCAL_CPP_EXTENSION = .cpp\nelse\n  LOCAL_CPP_EXTENSION = .cpp\nendif\n\n"
		writeDataToFile

		strForceDebugMessages = ""
		if $g_bForceDebugInReleaseBuild
			strForceDebugMessages = "-D_FORCE_DEBUGGING "
		end
		
		strGlobalSymbols = ""
		for extraExport in $g_kGlobalSymbols
			strGlobalSymbols = strGlobalSymbols + "-D#{extraExport} "
		end
		
		strAdditionalExports = ""
		if not kProjectProperties.m_bIsMainProject and kProjectProperties.m_strProjectExports != nil
			for extraExport in kProjectProperties.m_strProjectExports
				strAdditionalExports = strAdditionalExports + "-D#{extraExport} "
			end
		end
			
		strWarningsAsFatalErrors = ""
		if not kProjectProperties.m_bIsExternalLibrary
			strWarningsAsFatalErrors = "-Wfatal-errors "	
		end
		
		self.m_strDataToWrite = "ifeq ($(APP_OPTIM), debug)
LOCAL_CFLAGS := -DUSE_OGL_ES2 -DJSON_USE_EXCEPTION=0 -x c++ -DTARGET_ANDROID -DTARGET_MOBILE -D_DEBUG #{strWarningsAsFatalErrors}#{strAdditionalExports} #{strGlobalSymbols}
else
LOCAL_CFLAGS := -DUSE_OGL_ES2 -DJSON_USE_EXCEPTION=0 -x c++ -DTARGET_ANDROID -DTARGET_MOBILE #{strForceDebugMessages}-DRELEASE #{strWarningsAsFatalErrors}#{strAdditionalExports} #{strGlobalSymbols}
endif\n\n"
		writeDataToFile

		#if not kProjectProperties.m_bIsMainProject
			#if kProjectProperties.m_bIsFinalLibrary
			#	self.m_strDataToWrite = "LOCAL_WHOLE_STATIC_LIBRARIES := \\\n  cpufeatures \\"
			#else
			#	self.m_strDataToWrite = "LOCAL_STATIC_LIBRARIES := \\\n  cpufeatures \\"
			#end
			
		if not kProjectProperties.m_bIsExternalLibrary
			self.m_strDataToWrite = "LOCAL_STATIC_LIBRARIES := \\\n  cpufeatures \\"
			writeDataToFile
		else
			self.m_strDataToWrite = "LOCAL_STATIC_LIBRARIES := \\\n"
			writeDataToFile
		end
		if ProjectProperties::s_kProjectDependencies[kProjectProperties.m_strProjectName].length > 0
			ProjectProperties::s_kProjectDependencies[kProjectProperties.m_strProjectName].each do |strDependencyName|
				
				dependencyProjectProperties = GetProjectProperties strDependencyName
				if dependencyProjectProperties != nil
					if dependencyProjectProperties.m_kProjectPlatformTargets == nil or (dependencyProjectProperties.m_kProjectPlatformTargets != nil and dependencyProjectProperties.m_kProjectPlatformTargets.include? PLATFORM_ANDROID)
						if not dependencyProjectProperties.m_LibraryLinkWhole
							self.m_strDataToWrite = "  #{strDependencyName} \\"
							writeDataToFile			
						end
					end
				end
				
			end
		end
		if ProjectProperties::s_kProject3rdPartyDependencies[kProjectProperties.m_strProjectName].length > 0
			ProjectProperties::s_kProject3rdPartyDependencies[kProjectProperties.m_strProjectName].each do |strDependencyName|
			
				dependencyProjectProperties = GetProjectProperties strDependencyName
				if dependencyProjectProperties != nil
					if dependencyProjectProperties.m_kProjectPlatformTargets == nil or (dependencyProjectProperties.m_kProjectPlatformTargets != nil and dependencyProjectProperties.m_kProjectPlatformTargets.include? PLATFORM_ANDROID)
						if not dependencyProjectProperties.m_LibraryLinkWhole
							self.m_strDataToWrite = "  #{strDependencyName} \\"
							writeDataToFile			
						end
					end
				end
				
			end
		end
				
		# Be3Lib static
		if kProjectProperties.m_bIsMainProject
			if ProjectProperties::s_kProjectDependencies[BE3_MAIN_LIBRARY].length > 0
				ProjectProperties::s_kProjectDependencies[BE3_MAIN_LIBRARY].each do |strDependencyName|
				
					dependencyProjectProperties = GetProjectProperties strDependencyName
					if dependencyProjectProperties != nil
						if dependencyProjectProperties.m_kProjectPlatformTargets == nil or (dependencyProjectProperties.m_kProjectPlatformTargets != nil and dependencyProjectProperties.m_kProjectPlatformTargets.include? PLATFORM_ANDROID)
							if not dependencyProjectProperties.m_LibraryLinkWhole
								self.m_strDataToWrite = "  #{strDependencyName} \\"
								writeDataToFile			
							end
						end
					end
					
				end
			end
			if ProjectProperties::s_kProject3rdPartyDependencies[BE3_MAIN_LIBRARY].length > 0
				ProjectProperties::s_kProject3rdPartyDependencies[BE3_MAIN_LIBRARY].each do |strDependencyName|
				
					dependencyProjectProperties = GetProjectProperties strDependencyName
					if dependencyProjectProperties != nil
						if dependencyProjectProperties.m_kProjectPlatformTargets == nil or (dependencyProjectProperties.m_kProjectPlatformTargets != nil and dependencyProjectProperties.m_kProjectPlatformTargets.include? PLATFORM_ANDROID)
							if not dependencyProjectProperties.m_LibraryLinkWhole
								self.m_strDataToWrite = "  #{strDependencyName} \\"
								writeDataToFile			
							end
						end
					end
					
				end
			end
		end
		
		# We add the prebuilt libraries into LOCAL_STATIC_LIBRARIES
		if not kProjectProperties.m_bIsExternalLibrary
			kArrayDependencyVisited = Array.new
			ProjectProperties::s_kProjectExternalDependencies.each do |strProjectNameGlobal, kDependenciesArray|
				if (strProjectNameGlobal == kProjectProperties.m_strProjectName)
				
					kLinkWholeDependencies = []
					if strProjectNameGlobal == BE3_MAIN_LIBRARY
						kLinkWholeDependencies = ProjectProperties::s_kProjectExternalDependenciesLinkWhole[BE3_MAIN_LIBRARY]
					end
				
					if kDependenciesArray.length > 0
						kDependenciesArray.each do |strProjectDependencyName|
						
							if not kArrayDependencyVisited.include?(strProjectDependencyName) and not kLinkWholeDependencies.include?(strProjectDependencyName)
								self.m_strDataToWrite = "  #{strProjectDependencyName}_base \\"
								writeDataToFile	
							end
							
						end
					end
				end
			end
			
			writeDataToFile
		end
		
		self.m_strDataToWrite = "LOCAL_WHOLE_STATIC_LIBRARIES := \\\n"
		writeDataToFile
		if ProjectProperties::s_kProjectDependencies[kProjectProperties.m_strProjectName].length > 0
			ProjectProperties::s_kProjectDependencies[kProjectProperties.m_strProjectName].each do |strDependencyName|
			
				dependencyProjectProperties = GetProjectProperties strDependencyName
				if dependencyProjectProperties != nil
					if dependencyProjectProperties.m_kProjectPlatformTargets == nil or (dependencyProjectProperties.m_kProjectPlatformTargets != nil and dependencyProjectProperties.m_kProjectPlatformTargets.include? PLATFORM_ANDROID)
						if dependencyProjectProperties.m_LibraryLinkWhole
							self.m_strDataToWrite = "  #{strDependencyName} \\"
							writeDataToFile			
						end
					end
				end
				
			end
		end
		if ProjectProperties::s_kProject3rdPartyDependencies[kProjectProperties.m_strProjectName].length > 0
			ProjectProperties::s_kProject3rdPartyDependencies[kProjectProperties.m_strProjectName].each do |strDependencyName|
				
				dependencyProjectProperties = GetProjectProperties strDependencyName
				if dependencyProjectProperties != nil
					if dependencyProjectProperties.m_kProjectPlatformTargets == nil or (dependencyProjectProperties.m_kProjectPlatformTargets != nil and dependencyProjectProperties.m_kProjectPlatformTargets.include? PLATFORM_ANDROID)
						if dependencyProjectProperties.m_LibraryLinkWhole
							self.m_strDataToWrite = "  #{strDependencyName} \\"
							writeDataToFile			
						end
					end
				end
				
			end
		end
		
		# Be3Lib static
		if kProjectProperties.m_bIsMainProject
			if ProjectProperties::s_kProjectDependencies[BE3_MAIN_LIBRARY].length > 0
				ProjectProperties::s_kProjectDependencies[BE3_MAIN_LIBRARY].each do |strDependencyName|
				
					dependencyProjectProperties = GetProjectProperties strDependencyName
					if dependencyProjectProperties != nil
						if dependencyProjectProperties.m_kProjectPlatformTargets == nil or (dependencyProjectProperties.m_kProjectPlatformTargets != nil and dependencyProjectProperties.m_kProjectPlatformTargets.include? PLATFORM_ANDROID)
							if dependencyProjectProperties.m_LibraryLinkWhole
								self.m_strDataToWrite = "  #{strDependencyName} \\"
								writeDataToFile			
							end
						end
					end
					
				end
			end
			if ProjectProperties::s_kProject3rdPartyDependencies[BE3_MAIN_LIBRARY].length > 0
				ProjectProperties::s_kProject3rdPartyDependencies[BE3_MAIN_LIBRARY].each do |strDependencyName|
				
					dependencyProjectProperties = GetProjectProperties strDependencyName
					if dependencyProjectProperties != nil
						if dependencyProjectProperties.m_kProjectPlatformTargets == nil or (dependencyProjectProperties.m_kProjectPlatformTargets != nil and dependencyProjectProperties.m_kProjectPlatformTargets.include? PLATFORM_ANDROID)
							if dependencyProjectProperties.m_LibraryLinkWhole
								self.m_strDataToWrite = "  #{strDependencyName} \\"
								writeDataToFile			
							end
						end
					end
					
				end
			end
		end
		
		# We add the prebuilt libraries into LOCAL_WHOLE_STATIC_LIBRARIES
		if not kProjectProperties.m_bIsExternalLibrary
			kArrayDependencyVisited = Array.new
			ProjectProperties::s_kProjectExternalDependencies.each do |strProjectNameGlobal, kDependenciesArray|
				if (strProjectNameGlobal == kProjectProperties.m_strProjectName)
				
					kLinkWholeDependencies = []
					if strProjectNameGlobal == BE3_MAIN_LIBRARY
						kLinkWholeDependencies = ProjectProperties::s_kProjectExternalDependenciesLinkWhole[BE3_MAIN_LIBRARY]
					end
				
					if kDependenciesArray.length > 0
						kDependenciesArray.each do |strProjectDependencyName|
						
							if not kArrayDependencyVisited.include?(strProjectDependencyName) and kLinkWholeDependencies.include?(strProjectDependencyName)
								self.m_strDataToWrite = "  #{strProjectDependencyName}_base \\"
								writeDataToFile	
							end
							
						end
					end
				end
			end
		end
		
		#else
		#	self.m_strDataToWrite = "LOCAL_SHARED_LIBRARIES := \\\n  Be3Lib \\"
		#	self.m_strDataToWrite = "LOCAL_STATIC_LIBRARIES := \\\n  Be3Lib \\"
		#	writeDataToFile
		#end

		writeDataToFile
		
		#if (not kProjectProperties.m_bIsLibrary) or (kProjectProperties.m_bIsLibrary and kProjectProperties.m_LibraryType == ProjectProperties::LIBRARY_TYPE_SHARED)
		if (not kProjectProperties.m_bIsLibrary and not kProjectProperties.m_bIsExternalLibrary)
			self.m_strDataToWrite = "ifeq ($(APP_OPTIM), debug)
LOCAL_LDLIBS += \\
  -llog \\
  -landroid \\
  -lEGL \\
  -lGLESv1_CM \
  -lGLESv2 \\

else
LOCAL_LDLIBS += \\
  -llog \\
  -landroid \\
  -lEGL \\
  -lGLESv1_CM \
  -lGLESv2 \\

endif\n\n"
			writeDataToFile
		else
			self.m_strDataToWrite = "ifeq ($(APP_OPTIM), debug)\nLOCAL_LDLIBS += \nelse\nLOCAL_LDLIBS += \nendif\n\n"
			writeDataToFile
		end

		# In Windows we need to set the LOCAL_SHORT_COMMANDS because the huge quantity of files and the command length limitation
		#if kProjectProperties.m_bIsMainProject
		#	if not OS::mac?
		#		self.m_strDataToWrite = "LOCAL_SHORT_COMMANDS := true\n\n"
		#		writeDataToFile
		#	end
		#end
		
		#if (not kProjectProperties.m_bIsLibrary) or (kProjectProperties.m_bIsLibrary and kProjectProperties.m_LibraryType == ProjectProperties::LIBRARY_TYPE_SHARED)
		if not kProjectProperties.m_bIsLibrary and not kProjectProperties.m_bIsExternalLibrary
			self.m_strDataToWrite = "include $(BUILD_SHARED_LIBRARY)\n\n"
			writeDataToFile
		else
			self.m_strDataToWrite = "include $(BUILD_STATIC_LIBRARY)\n\n"
			writeDataToFile
		end

		if kProjectProperties.m_bIsMainProject
			ndkModulePath = "$(LOCAL_PATH)/#{BE3_SDK_DIR_SCRIPTS}"
		else
			ndkModulePath = "$(LOCAL_PATH)/../.."
		end
		self.m_strDataToWrite = "$(call import-add-path, #{ndkModulePath})"
		writeDataToFile

		if not kProjectProperties.m_bIsExternalLibrary
			self.m_strDataToWrite = "$(call import-module, android/cpufeatures)"
			writeDataToFile
		end
		if ProjectProperties::s_kProjectDependencies[kProjectProperties.m_strProjectName].length > 0
			ProjectProperties::s_kProjectDependencies[kProjectProperties.m_strProjectName].each do |strDependencyName|
				
				dependencyProjectProperties = GetProjectProperties strDependencyName
				if dependencyProjectProperties != nil
					if dependencyProjectProperties.m_kProjectPlatformTargets == nil or (dependencyProjectProperties.m_kProjectPlatformTargets != nil and dependencyProjectProperties.m_kProjectPlatformTargets.include? PLATFORM_ANDROID)
						self.m_strDataToWrite = "$(call import-module, BeLibs/#{strDependencyName}#{PROJECT_FILES_PATH})"
						writeDataToFile
					end
				end
				
			end
		end
		if ProjectProperties::s_kProject3rdPartyDependencies[kProjectProperties.m_strProjectName].length > 0
			ProjectProperties::s_kProject3rdPartyDependencies[kProjectProperties.m_strProjectName].each do |strDependencyName|
			
				dependencyProjectProperties = GetProjectProperties strDependencyName
				if dependencyProjectProperties != nil
					if dependencyProjectProperties.m_kProjectPlatformTargets == nil or (dependencyProjectProperties.m_kProjectPlatformTargets != nil and dependencyProjectProperties.m_kProjectPlatformTargets.include? PLATFORM_ANDROID)
						self.m_strDataToWrite = "$(call import-module, 3rdParty/#{strDependencyName}#{PROJECT_FILES_PATH})"
						writeDataToFile
					end
				end
				
			end
		end
		# Be3Lib static
		if kProjectProperties.m_bIsMainProject
			if ProjectProperties::s_kProjectDependencies[BE3_MAIN_LIBRARY].length > 0
				ProjectProperties::s_kProjectDependencies[BE3_MAIN_LIBRARY].each do |strDependencyName|
				
					dependencyProjectProperties = GetProjectProperties strDependencyName
					if dependencyProjectProperties != nil
						if dependencyProjectProperties.m_kProjectPlatformTargets == nil or (dependencyProjectProperties.m_kProjectPlatformTargets != nil and dependencyProjectProperties.m_kProjectPlatformTargets.include? PLATFORM_ANDROID)
							self.m_strDataToWrite = "$(call import-module, BeLibs/#{strDependencyName}#{PROJECT_FILES_PATH})"
							writeDataToFile
						end
					end
					
				end
			end
			if ProjectProperties::s_kProject3rdPartyDependencies[BE3_MAIN_LIBRARY].length > 0
				ProjectProperties::s_kProject3rdPartyDependencies[BE3_MAIN_LIBRARY].each do |strDependencyName|
				
					dependencyProjectProperties = GetProjectProperties strDependencyName
					if dependencyProjectProperties != nil
						if dependencyProjectProperties.m_kProjectPlatformTargets == nil or (dependencyProjectProperties.m_kProjectPlatformTargets != nil and dependencyProjectProperties.m_kProjectPlatformTargets.include? PLATFORM_ANDROID)
							self.m_strDataToWrite = "$(call import-module, 3rdParty/#{strDependencyName}#{PROJECT_FILES_PATH})"
							writeDataToFile
						end
					end
					
				end
			end
		end

		writeDataToFile

		self.m_strDataToWrite = "endif"
		writeDataToFile
		
		self.m_outputFile.close

	end

	# Writes APP MODULES to the Application make file
	private
	def writeAppModulesToApplicationMakeFile(kAppModulesVisited, strProjectName, strForceProjectName)

		if strForceProjectName != nil
			self.m_strDataToWrite = "  #{strForceProjectName} \\"
		else
			self.m_strDataToWrite = "  #{strProjectName} \\"
		end
		writeDataToFile

		kAppModulesVisited.push strProjectName
		
		if ProjectProperties::s_kProjectDependencies[strProjectName].length > 0
			ProjectProperties::s_kProjectDependencies[strProjectName].each do |strDependencyName|
				if not kAppModulesVisited.include? strDependencyName
					
					dependencyProjectProperties = GetProjectProperties strDependencyName
					if dependencyProjectProperties != nil
						if dependencyProjectProperties.m_kProjectPlatformTargets == nil or (dependencyProjectProperties.m_kProjectPlatformTargets != nil and dependencyProjectProperties.m_kProjectPlatformTargets.include? PLATFORM_ANDROID)
							writeAppModulesToApplicationMakeFile kAppModulesVisited, strDependencyName, nil
						end
					end
					
				end
			end
		end
		
		if ProjectProperties::s_kProject3rdPartyDependencies[strProjectName].length > 0
			ProjectProperties::s_kProject3rdPartyDependencies[strProjectName].each do |strDependencyName|
				if not kAppModulesVisited.include? strDependencyName
				
					dependencyProjectProperties = GetProjectProperties strDependencyName
					if dependencyProjectProperties != nil
						if dependencyProjectProperties.m_kProjectPlatformTargets == nil or (dependencyProjectProperties.m_kProjectPlatformTargets != nil and dependencyProjectProperties.m_kProjectPlatformTargets.include? PLATFORM_ANDROID)
							writeAppModulesToApplicationMakeFile kAppModulesVisited, strDependencyName, nil
						end
					end
					
				end
			end
		end

	end

	# Generates the Application make file
	public
	def generateApplicationMakeFile(kProjectProperties, strDebugMode)

		if kProjectProperties.m_bIsMainProject
			puts "Building #{$g_kPlatformNames[PLATFORM_ANDROID]} " + kProjectProperties.m_strOutputFileName + " Application.mk..."
		else
			puts "Building #{$g_kPlatformNames[PLATFORM_ANDROID]} " + kProjectProperties.m_strProjectName + " Application.mk..."
		end
		
		strAppMakeFile = "Application.mk"
		
		if not kProjectProperties.m_bIsExternalLibrary
			strAppMakeFilePath = kProjectProperties.m_strProjectPath + "/" + kProjectProperties.m_strProjectName + PROJECT_FILES_PATH + "/" + strAppMakeFile
		else
			strAppMakeFilePath = "." + PROJECT_FILES_PATH + "/" + strAppMakeFile
		end

		if self.m_writingToMemory == true
			self.m_outputFile = nil
		else
			self.m_outputFile = File.new(strAppMakeFilePath, "w")
		end

		self.m_strDataToWrite = self.m_strFileHeader
		writeDataToFile
				
		self.m_strDataToWrite = "NDK_TOOLCHAIN_VERSION := 4.8"
		writeDataToFile
		
		if strDebugMode != nil
			self.m_strDataToWrite = "APP_OPTIM := #{strDebugMode}"
		else
			self.m_strDataToWrite = "APP_OPTIM := debug"
		end
		writeDataToFile
		
		self.m_strDataToWrite = "APP_ABI := #{kProjectProperties.m_strAndroidABI}"
		writeDataToFile

		self.m_strDataToWrite = "APP_PLATFORM := #{kProjectProperties.m_strAndroidMinTarget}"
		writeDataToFile

		self.m_strDataToWrite = "APP_STL := #{kProjectProperties.m_strAndroidSTL}"
		writeDataToFile

		self.m_strDataToWrite = "APP_CPPFLAGS += -std=c++11 -fexceptions"
		writeDataToFile

		self.m_strDataToWrite = "APP_MODULES := \\"
		writeDataToFile

		kAppModulesVisited = Array.new
		writeAppModulesToApplicationMakeFile kAppModulesVisited, kProjectProperties.m_strProjectName, kProjectProperties.m_strOutputFileName
		
		writeDataToFile

		if self.m_writingToMemory == false
			self.m_outputFile.close
		end

	end

	# Generates the Build XML file
	private
	def generateBuildXMLFile(kProjectProperties)

		if kProjectProperties.m_bIsMainProject
			puts "Building #{$g_kPlatformNames[PLATFORM_ANDROID]} " + kProjectProperties.m_strOutputFileName + " build.xml..."
		else
			puts "Building #{$g_kPlatformNames[PLATFORM_ANDROID]} " + kProjectProperties.m_strProjectName + " build.xml..."
		end
		
		strBuildXMLFile = "Build.xml"
		if kProjectProperties.m_bIsMainProject
			strBuildXMLFilePath = kProjectProperties.m_strProjectPath + kProjectProperties.m_strProjectName + PROJECT_FILES_PATH + "/" + strBuildXMLFile
		else
			strBuildXMLFilePath = BE3_SDK_DIR_SCRIPTS + kProjectProperties.m_strProjectPath + kProjectProperties.m_strProjectName + PROJECT_FILES_PATH + "/" + strBuildXMLFile
		end

		self.m_outputFile = File.new(strBuildXMLFilePath, "w")

		kBuildConfigFile = File.open(BE3_BUILD_SYSTEM_DIR + "Scripts/AndroidBuild.cfg", "r")
		self.m_strDataToWrite = kBuildConfigFile.read
		kBuildConfigFile.close

		if kProjectProperties.m_bIsMainProject
			self.m_strDataToWrite = self.m_strDataToWrite.gsub("%projectName", kProjectProperties.m_strOutputFileName)
		else
			self.m_strDataToWrite = self.m_strDataToWrite.gsub("%projectName", kProjectProperties.m_strProjectName)
		end
		writeDataToFile

		self.m_outputFile.close

	end

	# Writes APP MODULES to the Application make file
	private
	def writeLibraryReferencesToProjectPropertiesFile(kLibraryReferenceVisited, strProjectName)

		if not kLibraryReferenceVisited.include? strProjectName
			self.m_strDataToWrite = "android.library.reference." + @@iDependenciesCount.to_s() + "=${env.BE3_SDK}BeLibs/" + "#{strProjectName}"
			writeDataToFile
			
			kLibraryReferenceVisited.push strProjectName
			@@iDependenciesCount += 1
		end

		if ProjectProperties::s_kProjectDependencies[strProjectName].length > 0
			ProjectProperties::s_kProjectDependencies[strProjectName].each do |strDependencyName|
				if not kLibraryReferenceVisited.include? strDependencyName
					writeLibraryReferencesToProjectPropertiesFile kLibraryReferenceVisited, strDependencyName
				end
			end
		end

	end

	# Generates the Project properties file
	private
	def generateProjectPropertiesFile(kProjectProperties)

		if kProjectProperties.m_bIsMainProject
			puts "Building #{$g_kPlatformNames[PLATFORM_ANDROID]} " + kProjectProperties.m_strOutputFileName + " project.properties..."
		else
			puts "Building #{$g_kPlatformNames[PLATFORM_ANDROID]} " + kProjectProperties.m_strProjectName + " project.properties..."
		end
		
		strProjectPropertiesFile = "project.properties"
		if kProjectProperties.m_bIsMainProject
			strProjectPropertiesFilePath = kProjectProperties.m_strProjectPath + kProjectProperties.m_strProjectName + PROJECT_FILES_PATH + "/" + strProjectPropertiesFile
		else
			strProjectPropertiesFilePath = BE3_SDK_DIR_SCRIPTS + kProjectProperties.m_strProjectPath + kProjectProperties.m_strProjectName + PROJECT_FILES_PATH + "/" + strProjectPropertiesFile
		end

		self.m_outputFile = File.new(strProjectPropertiesFilePath, "w")

		self.m_strDataToWrite = self.m_strFileHeader
		writeDataToFile

		self.m_strDataToWrite = "target=" + kProjectProperties.m_strAndroidTarget
		writeDataToFile

		if kProjectProperties.m_bIsLibrary == true
			self.m_strDataToWrite = "android.library=true"
			writeDataToFile
		end
				
		if ProjectProperties::s_kProjectDependencies[kProjectProperties.m_strProjectName].length > 0
			writeDataToFile

			kLibraryReferenceVisited = Array.new
			@@iDependenciesCount = 1

			ProjectProperties::s_kProjectDependencies[kProjectProperties.m_strProjectName].each do |kProjectDependencyName|
				writeLibraryReferencesToProjectPropertiesFile kLibraryReferenceVisited, kProjectDependencyName
			end
		end

		self.m_outputFile.close

	end

	# Generates the Batch files for triggering NDK-BUILD
	private
	def generateBatchFiles(kProjectProperties, strCleanCommand)
		
		if not kProjectProperties.m_bIsExternalLibrary
			strBatchFile = "#{kProjectProperties.m_strOutputFileName}_ANDROID_debug.bat"
			strBatchFilePath = kProjectProperties.m_strProjectPath + "/" + kProjectProperties.m_strProjectName + "/" + strBatchFile
			
			strBatchIOSFile = "#{kProjectProperties.m_strOutputFileName}_ANDROID_debug.sh"
			strBatchIOSFilePath = kProjectProperties.m_strProjectPath + "/" + kProjectProperties.m_strProjectName + "/" + strBatchIOSFile
		else
			strBatchFile = "#{kProjectProperties.m_strProjectName}_ANDROID_debug.bat"
			strBatchFilePath = strBatchFile
			
			strBatchIOSFile = "#{kProjectProperties.m_strProjectName}_ANDROID_debug.sh"
			strBatchIOSFilePath = strBatchIOSFile
		end
		
		if not kProjectProperties.m_bIsExternalLibrary
			strBatchReleaseFile = "#{kProjectProperties.m_strOutputFileName}_ANDROID_release.bat"
			strBatchIOSReleaseFile = "#{kProjectProperties.m_strOutputFileName}_ANDROID_release.sh"
			
			strBatchProductionFile = "#{kProjectProperties.m_strOutputFileName}_ANDROID_production.bat"
			strBatchIOSProductionFile = "#{kProjectProperties.m_strOutputFileName}_ANDROID_production.sh"
		else
			strBatchReleaseFile = "#{kProjectProperties.m_strProjectName}_ANDROID_release.bat"
			strBatchIOSReleaseFile = "#{kProjectProperties.m_strProjectName}_ANDROID_release.sh"
			
			strBatchProductionFile = "#{kProjectProperties.m_strProjectName}_ANDROID_production.bat"
			strBatchIOSProductionFile = "#{kProjectProperties.m_strProjectName}_ANDROID_production.sh"
		end

		if not kProjectProperties.m_bIsExternalLibrary
			strBatchReleaseFilePath = kProjectProperties.m_strProjectPath + "/" + kProjectProperties.m_strProjectName + "/" + strBatchReleaseFile
			strBatchIOSReleaseFilePath = kProjectProperties.m_strProjectPath + "/" + kProjectProperties.m_strProjectName + "/" + strBatchIOSReleaseFile
			
			strBatchProductionFilePath = kProjectProperties.m_strProjectPath + "/" + kProjectProperties.m_strProjectName + "/" + strBatchProductionFile
			strBatchIOSProductionFilePath = kProjectProperties.m_strProjectPath + "/" + kProjectProperties.m_strProjectName + "/" + strBatchIOSProductionFile
		else
			strBatchReleaseFilePath = strBatchReleaseFile
			strBatchIOSReleaseFilePath = strBatchIOSReleaseFile
			
			strBatchProductionFilePath = strBatchProductionFile
			strBatchIOSProductionilePath = strBatchIOSProductionFile
		end
		
		if strCleanCommand == "clean"
		
			if OS.mac?
			
				if not kProjectProperties.m_bIsExternalLibrary
					if File.exists? strBatchIOSFilePath
						puts "Cleaning #{strBatchIOSFile} ..."
					
						File.delete strBatchIOSFilePath
					end
				end
				
				if File.exists? strBatchIOSReleaseFilePath
					puts "Cleaning #{strBatchIOSReleaseFile} ..."
					
					File.delete strBatchIOSReleaseFilePath
				end
				
                if not kProjectProperties.m_bIsExternalLibrary
                    if File.exists? strBatchIOSProductionFilePath
                        puts "Cleaning #{strBatchIOSProductionFile} ..."
					
                        File.delete strBatchIOSProductionFilePath
                    end
                end
		
			else
		
				#if not kProjectProperties.m_bIsExternalLibrary
					if File.exists? strBatchFilePath
						puts "Cleaning #{strBatchFile} ..."
						
						File.delete strBatchFilePath
					end
				#end
				
				if File.exists? strBatchReleaseFilePath
					puts "Cleaning #{strBatchReleaseFile} ..."
					
					File.delete strBatchReleaseFilePath
				end
				
				if File.exists? strBatchProductionFilePath
					puts "Cleaning #{strBatchProductionFile} ..."
					
					File.delete strBatchProductionFilePath
				end
			
			end
			
			return
			
		end
		
		if OS.mac?
		
			if not kProjectProperties.m_bIsExternalLibrary
				self.m_outputFile = File.new(strBatchIOSFilePath, "w")
			
				self.m_strDataToWrite = "echo "
				writeDataToFile
				self.m_strDataToWrite = "echo BE3 MOBILE BARCELONA - Building Android Debug..."
				writeDataToFile
				self.m_strDataToWrite = "echo "
				writeDataToFile
				writeDataToFile
				
				self.m_strDataToWrite = "export NDK_PROJECT_PATH=./projects"
				writeDataToFile
				self.m_strDataToWrite = "export ANDROID_SDK=#{BE3_SDK_DIR_SCRIPTS}#{ENV["ANDROID_SDK"]}"
				writeDataToFile
				self.m_strDataToWrite = "export ANDROID_NDK=#{BE3_SDK_DIR_SCRIPTS}#{ENV["ANDROID_NDK"]}"
				writeDataToFile
				self.m_strDataToWrite = "export PATH=\"$PATH:$ANDROID_SDK/tools:$ANDROID_SDK/platform-tools:$ANDROID_NDK\""
				writeDataToFile
				writeDataToFile

				if ANDROID_IN_MEMORY == true
					self.m_strDataToWrite = "\"ruby\" \"#{BE3_SDK_DIR_SCRIPTS}BeBuildTools/ExecBuildingSystem.rb\" \"BUILD_ANDROID\" \"debug\" \"\" $1"
				else
					self.m_strDataToWrite = "\"ruby\" \"#{BE3_SDK_DIR_SCRIPTS}BeBuildTools/ExecBuildingSystem.rb\" \"BUILD_ANDROID\" \"debug\" ./Application.mk ./Android.mk ./obj/Android %1"
				end
				writeDataToFile

				self.m_outputFile.close
				system("chmod 777 #{strBatchIOSFilePath}")
			end
			
			
			self.m_outputFile = File.new(strBatchIOSReleaseFilePath, "w")
			
			self.m_strDataToWrite = "echo "
			writeDataToFile
			self.m_strDataToWrite = "echo BAK ENGINE 3 - Building Android Release..."
			writeDataToFile
			self.m_strDataToWrite = "echo "
			writeDataToFile
			
			self.m_strDataToWrite = "export NDK_PROJECT_PATH=./projects"
			writeDataToFile

			if ANDROID_IN_MEMORY == true
				self.m_strDataToWrite = "\"ruby\" \"#{BE3_SDK_DIR_SCRIPTS}BeBuildTools/ExecBuildingSystem.rb\" \"BUILD_ANDROID\" \"release\" \"\" $1"
			else
				self.m_strDataToWrite = "\"ruby\" \"#{BE3_SDK_DIR_SCRIPTS}BeBuildTools/ExecBuildingSystem.rb\" \"BUILD_ANDROID\" \"release\" ./Application.mk ./Android.mk ./obj/Android %1"
			end
			writeDataToFile

			self.m_outputFile.close
			system("chmod 777 #{strBatchIOSReleaseFilePath}")
			
			
			if not kProjectProperties.m_bIsExternalLibrary
                self.m_outputFile = File.new(strBatchIOSProductionFilePath, "w")
			
                self.m_strDataToWrite = "echo "
                writeDataToFile
                self.m_strDataToWrite = "echo BAK ENGINE 3 - Building Android Production..."
                writeDataToFile
                self.m_strDataToWrite = "echo "
                writeDataToFile
			
                self.m_strDataToWrite = "export NDK_PROJECT_PATH=./projects"
                writeDataToFile

                if ANDROID_IN_MEMORY == true
                    self.m_strDataToWrite = "\"ruby\" \"#{BE3_SDK_DIR_SCRIPTS}BeBuildTools/ExecBuildingSystem.rb\" \"BUILD_ANDROID\" \"production\" \"\" $1"
                    else
                    self.m_strDataToWrite = "\"ruby\" \"#{BE3_SDK_DIR_SCRIPTS}BeBuildTools/ExecBuildingSystem.rb\" \"BUILD_ANDROID\" \"production\" ./Application.mk ./Android.mk ./obj/Android %1"
                end
                writeDataToFile

                self.m_outputFile.close
                system("chmod 777 #{strBatchIOSProductionFilePath}")
            end
		
		else
				
			if kProjectProperties.m_bIsMainProject
				puts "Building #{$g_kPlatformNames[PLATFORM_ANDROID]} " + kProjectProperties.m_strOutputFileName + " Batch files..."
			else
				puts "Building #{$g_kPlatformNames[PLATFORM_ANDROID]} " + kProjectProperties.m_strProjectName + " Batch files..."
			end

			#if not kProjectProperties.m_bIsExternalLibrary
				self.m_outputFile = File.new(strBatchFilePath, "w")

				self.m_strDataToWrite = "@echo off\nsetlocal\ncd %~dp0\n\ncls"
				writeDataToFile
				writeDataToFile

				self.m_strDataToWrite = "@echo."
				writeDataToFile
				self.m_strDataToWrite = "@echo BAK ENGINE 3 - Building Android Debug..."
				writeDataToFile
				self.m_strDataToWrite = "@echo."
				writeDataToFile
				writeDataToFile
				
				self.m_strDataToWrite = "@set NDK_PROJECT_PATH=./projects"
				writeDataToFile
				writeDataToFile
				
				if ANDROID_IN_MEMORY == true
					self.m_strDataToWrite = "@\"ruby\" \"#{BE3_SDK_DIR_SCRIPTS}BeBuildTools/ExecBuildingSystem.rb\" \"BUILD_ANDROID\" \"debug\" \"\" %1"
				else
					self.m_strDataToWrite = "@\"ruby\" \"#{BE3_SDK_DIR_SCRIPTS}BeBuildTools/ExecBuildingSystem.rb\" \"BUILD_ANDROID\" \"debug\" ./Application.mk ./Android.mk ./obj/Android %1"
				end
				writeDataToFile

				self.m_outputFile.close
			#end
			
			
			self.m_outputFile = File.new(strBatchReleaseFilePath, "w")

			self.m_strDataToWrite = "@echo off\nsetlocal\ncd %~dp0\n\ncls"
			writeDataToFile

			self.m_strDataToWrite = "@echo."
			writeDataToFile
			self.m_strDataToWrite = "@echo BAK ENGINE 3 - Building Android Release..."
			writeDataToFile
			self.m_strDataToWrite = "@echo."
			writeDataToFile
			writeDataToFile
			
			self.m_strDataToWrite = "@set NDK_PROJECT_PATH=./projects"
			writeDataToFile
			writeDataToFile

			if ANDROID_IN_MEMORY == true
				self.m_strDataToWrite = "@\"ruby\" \"#{BE3_SDK_DIR_SCRIPTS}BeBuildTools/ExecBuildingSystem.rb\" \"BUILD_ANDROID\" \"release\" \"\" %1"
			else
				self.m_strDataToWrite = "@\"ruby\" \"#{BE3_SDK_DIR_SCRIPTS}BeBuildTools/ExecBuildingSystem.rb\" \"BUILD_ANDROID\" \"release\" ./Application.mk ./Android.mk ./obj/Android %1"
			end
			writeDataToFile

			self.m_outputFile.close
			
			
			if not kProjectProperties.m_bIsExternalLibrary
                self.m_outputFile = File.new(strBatchProductionFilePath, "w")

                self.m_strDataToWrite = "@echo off\nsetlocal\ncd %~dp0\n\ncls"
                writeDataToFile

                self.m_strDataToWrite = "@echo."
                writeDataToFile
                self.m_strDataToWrite = "@echo BAK ENGINE 3 - Building Android Production..."
                writeDataToFile
                self.m_strDataToWrite = "@echo."
                writeDataToFile
                writeDataToFile
			
                self.m_strDataToWrite = "@set NDK_PROJECT_PATH=./projects"
                writeDataToFile
                writeDataToFile

                if ANDROID_IN_MEMORY == true
                    self.m_strDataToWrite = "@\"ruby\" \"#{BE3_SDK_DIR_SCRIPTS}BeBuildTools/ExecBuildingSystem.rb\" \"BUILD_ANDROID\" \"production\" \"\" %1"
                else
                    self.m_strDataToWrite = "@\"ruby\" \"#{BE3_SDK_DIR_SCRIPTS}BeBuildTools/ExecBuildingSystem.rb\" \"BUILD_ANDROID\" \"production\" ./Application.mk ./Android.mk ./obj/Android %1"
                end
                writeDataToFile

                self.m_outputFile.close
            end
		
		end

		#strRBFile = "AndroidBuild.rb"
		#strRBFilePath = kProjectProperties.m_strProjectPath + "/" + kProjectProperties.m_strProjectName + "/" + strRBFile

		#self.m_outputFile = File.new(strRBFilePath, "w")

		#self.m_strDataToWrite = "\n#!/bin/sh"
		#writeDataToFile
		#writeDataToFile
		#if ANDROID_IN_MEMORY == true
		#	self.m_strDataToWrite = "require_relative '../../BuildTools/Scripts/RegenAndroidFiles'"
		#	writeDataToFile
		#	writeDataToFile
		#	self.m_strDataToWrite = "ExecuteAndroidCompilation ARGV[0], ARGV[1]"
		#	writeDataToFile
		#	writeDataToFile
		#else
		#	strNDKCommand = "ndk-build -j4 NDK_DEBUG=\#{ARGV[0]} NDK_APPLICATION_MK=\#{ARGV[1]} APP_BUILD_SCRIPT=\#{ARGV[2]} NDK_APP_OUT=\#{ARGV[3]} \#{ARGV[4]}"
		#	strJavaCleanCommand = "#{ENV['BE3_SDK']}../BuildTools/Common/make.exe -j4 --directory=#{ENV['BE3_SDK']} --file=#{kProjectProperties.m_strProjectPath}/#{kProjectProperties.m_strProjectName}/AndroidJava.mk clean --quiet (cwd=#{ENV['BE3_SDK']})"
		#	strJavaCommand = "#{ENV['BE3_SDK']}../BuildTools/Common/make.exe -j4 --directory=#{ENV['BE3_SDK']} --file=#{kProjectProperties.m_strProjectPath}/#{kProjectProperties.m_strProjectName}/AndroidJava.mk --quiet deploy-debug (cwd=#{ENV['BE3_SDK']})"
		#	strNDKCommand = strNDKCommand.gsub("\\", "/")
		#	strJavaCleanCommand = strJavaCleanCommand.gsub("\\", "/")
		#	strJavaCommand = strJavaCommand.gsub("\\", "/")
		#	self.m_strDataToWrite = "puts \"Executing NDK Build...\""
		#	writeDataToFile
		#	self.m_strDataToWrite = "puts \"#{strNDKCommand}\""
		#	writeDataToFile
		#	self.m_strDataToWrite = "system(\"#{strNDKCommand}\")"
		#	writeDataToFile
		#	writeDataToFile
		#	self.m_strDataToWrite = "puts \"Executing Java Clean...\""
		#	writeDataToFile
		#	self.m_strDataToWrite = "puts \"#{strJavaCleanCommand}\""
		#	writeDataToFile
		#	self.m_strDataToWrite = "system(\"#{strJavaCleanCommand}\")"
		#	writeDataToFile
		#	writeDataToFile
		#	self.m_strDataToWrite = "puts \"Executing Java Build...\""
		#	writeDataToFile
		#	self.m_strDataToWrite = "puts \"#{strJavaCommand}\""
		#	writeDataToFile
		#	self.m_strDataToWrite = "system(\"#{strJavaCommand}\")"
		#	writeDataToFile
		#end
				
		#self.m_outputFile.close

	end

	# Get dependency modules for Java Make file
	private
	def getDependencyModulesFromThis(kLibraryReferenceVisited, strProjectName)

		if not kLibraryReferenceVisited.include? strProjectName
			kLibraryReferenceVisited.push strProjectName
		end

		if ProjectProperties::s_kProjectDependencies[strProjectName].length > 0
			ProjectProperties::s_kProjectDependencies[strProjectName].each do |strDependencyName|
				if not kLibraryReferenceVisited.include? strDependencyName
					getDependencyModulesFromThis kLibraryReferenceVisited, strDependencyName
				end
			end
		end

	end

	# Write all the files with the extension
	private
	def writeFilesWithExt(kSymbolNames, strBasePathToFiles, strIncrementalPathToFiles, strExtensionFilter, strIncludeFolderName, kAvoidFiles)
	
		strSearchingPath = strBasePathToFiles + strIncrementalPathToFiles
		if Dir.exists?(strSearchingPath)
			Dir.foreach(strSearchingPath) do |strItem|
				next if strItem == '.' or strItem == '..' or strItem == strIncludeFolderName
				
				strItemTmp = "/%1"
				strItemTmp = strItemTmp.gsub("%1", strItem)
				strFinalFolder = strSearchingPath + strItemTmp
				if File.directory?(strFinalFolder)
					writeFilesWithExt kSymbolNames, strBasePathToFiles, strIncrementalPathToFiles + strItemTmp, strExtensionFilter, strIncludeFolderName, kAvoidFiles
				end
			end
		end

		strSearchingPath = strBasePathToFiles + strIncrementalPathToFiles	
		if Dir.exists?(strSearchingPath)
			Dir.foreach(strSearchingPath) do |strItem|
				next if strItem == '.' or strItem == '..' or strItem == strIncludeFolderName
				
				if kAvoidFiles.include? strItem
					next
				end
				
				strFinalFolder = strSearchingPath + "/" + strItem
				
				if File.file?(strFinalFolder) and strFinalFolder.end_with?(strExtensionFilter)
					self.m_strDataToWrite = "	$(#{kSymbolNames['MODULE_PATH']})/../%1/%2 \\"
					self.m_strDataToWrite = self.m_strDataToWrite.gsub("%1", strIncrementalPathToFiles)
					self.m_strDataToWrite = self.m_strDataToWrite.gsub("%2", strItem)
					if not OS::mac?
						self.m_strDataToWrite = self.m_strDataToWrite.gsub("/", "\\")
					end
					writeDataToFile
				end
			end
		end

	end

	# Generates the Java Make File
	public
	def generateAndroidJavaMakeFile(kProjectProperties, bFirstIteration, bOnlyResources, bOnlyExpansionFiles, strMainProjectNameToUse)

		strMarketAPKName = ""
		if $g_selectedMarketAPK == MARKET_AMAZON
			strMarketAPKName = "_amazon"
		end
	
		#if kProjectProperties.m_bIsMainProject
		#	puts "Building #{$g_kPlatformNames[PLATFORM_ANDROID]} " + kProjectProperties.m_strOutputFileName + " AndroidJava.mk..."
		#else
		#	puts "Building #{$g_kPlatformNames[PLATFORM_ANDROID]} " + kProjectProperties.m_strProjectName + " AndroidJava.mk..."
		#end

		strProjectPathNoSlash = kProjectProperties.m_strProjectPath
		strProjectPathNoSlash = strProjectPathNoSlash[0..(strProjectPathNoSlash.length-2)]

		strJavaMakeFile = "AndroidJava.mk"
		strJavaMakeFilePath = kProjectProperties.m_strProjectPath + "/" + kProjectProperties.m_strProjectName + PROJECT_FILES_PATH + "/" + strJavaMakeFile

		# Symbols
		kSymbolNames = Hash.new { |hash, key| }
		
		if bFirstIteration
			if self.m_writingToMemory == true
				self.m_outputFile = nil
			else
				self.m_outputFile = File.new(strJavaMakeFilePath, "w")
			end
		end
			
		# Get all the dependency modules needed for this project
		kLibraryReferenceVisited = Array.new
		ProjectProperties::s_kProjectDependencies[kProjectProperties.m_strProjectName].each do |kProjectDependencyName|
			getDependencyModulesFromThis kLibraryReferenceVisited, kProjectDependencyName
		end

		# Write include project Java makes
		if kProjectProperties.m_bIsMainProject or kProjectProperties.m_bIsFinalLibrary
			if self.m_writingToMemory == true
				self.m_bIsFirstMemoryProject = true
				$g_kLibraryReferenceVisited.clear
				kLibraryReferenceVisited.each do |kProjectDependencyName|
					for i in 0..($g_kProjectProperties.length-1)
						if $g_kProjectProperties[i].m_strProjectName == kProjectDependencyName
							if not $g_kLibraryReferenceVisited.include? kProjectDependencyName
								$g_kLibraryReferenceVisited.push kProjectDependencyName
								generateAndroidJavaMakeFile $g_kProjectProperties[i], false, bOnlyResources, bOnlyExpansionFiles, strMainProjectNameToUse
								break
							end
						end
					end
					self.m_bIsFirstMemoryProject = false
				end
			else
				kLibraryReferenceVisited.reverse.each do |kProjectDependencyName|
					self.m_strDataToWrite = "\include BeLibs\\" + "#{kProjectDependencyName}" + "\\" + "AndroidJava.mk"
					writeDataToFile
				end
				writeDataToFile
			end
		end
				
		kSymbolNames["SDK"] = "ANDROID_TARGET_SDK"
		kSymbolNames["ZIPALIGN"] = "ANDROID_TOOL_ZIPALIGN"
		kSymbolNames["ADB"] = "ANDROID_TOOL_ADB"
		kSymbolNames["AAPT"] = "ANDROID_TOOL_AAPT"
		kSymbolNames["AAPT_PREFIX"] = "ANDROID_TOOL_AAPT_SOURCE_DIR_PREFIX"
		kSymbolNames["AIDL"] = "ANDROID_TOOL_AIDL"
		kSymbolNames["DX"] = "ANDROID_TOOL_DX"
		kSymbolNames["COMPILER"] = "JAVA_TOOL_COMPILER"
		kSymbolNames["ARCHIVER"] = "JAVA_TOOL_ARCHIVER"
		kSymbolNames["JARSIGNER"] = "JAVA_TOOL_JARSIGNER"
		kSymbolNames["7ZIP"] = "SEVEN_ZIP_TOOL"
        kSymbolNames["7ZIPb"] = "SEVEN_ZIP_TOOLb"

		# Tools paths
		if (self.m_writingToMemory == false or (self.m_writingToMemory == true and self.m_bIsFirstMemoryProject == true))
		
			if not $g_bToolsWrittenToFile
				self.m_strDataToWrite = self.m_strFileHeader
				writeDataToFile
				
				if OS::mac?
                    self.m_strDataToWrite = "CUR_DIR := $(shell pwd)"
                    writeDataToFile
					self.m_strDataToWrite = "BE3_SDK := #{ENV["BE3_SDK"]}"
					writeDataToFile
					self.m_strDataToWrite = "ANDROID_SDK := $(BE3_SDK)/#{ENV["ANDROID_SDK"]}"
					writeDataToFile
					writeDataToFile
				else
                    self.m_strDataToWrite = "CUR_DIR := %CD%"
                    writeDataToFile
					self.m_strDataToWrite = "BE3_SDK := #{ENV["BE3_SDK"]}"
					writeDataToFile
					self.m_strDataToWrite = "ANDROID_SDK := $(BE3_SDK)\\#{ENV["ANDROID_SDK"]}"
					writeDataToFile
					self.m_strDataToWrite = "JAVA_HOME := $(BE3_SDK)\\#{ENV["JAVA_HOME"]}"
					writeDataToFile
					writeDataToFile
				end
						
				strSDKAndroidVersionNumber = kProjectProperties.m_strAndroidTarget
				strSDKAndroidVersionNumber = strSDKAndroidVersionNumber.gsub("android-", "")
				self.m_strDataToWrite = kSymbolNames["SDK"] + " := " + strSDKAndroidVersionNumber
				writeDataToFile
			end
			
			strToolchainVersion = "21.1.2"
			
			if OS::mac?
				self.m_strDataToWrite = "#{kSymbolNames['ZIPALIGN']} := $(ANDROID_SDK)/build-tools/#{strToolchainVersion}/zipalign
#{kSymbolNames['ADB']} := $(ANDROID_SDK)/platform-tools/adb
#{kSymbolNames['AAPT']} := $(ANDROID_SDK)/build-tools/#{strToolchainVersion}/aapt
#{kSymbolNames['AAPT_PREFIX']} := -S 
#{kSymbolNames['AIDL']} := $(ANDROID_SDK)/build-tools/#{strToolchainVersion}/aidl
#{kSymbolNames['DX']} := $(ANDROID_SDK)/build-tools/#{strToolchainVersion}/dx
#{kSymbolNames['COMPILER']} := javac
#{kSymbolNames['ARCHIVER']} := jar
#{kSymbolNames['JARSIGNER']} := jarsigner
#{kSymbolNames['7ZIP']} := $(BE3_SDK)/BeBuildTools/Common/7za
#{kSymbolNames['7ZIPb']} := $(CUR_DIR)/$(BE3_SDK)/BeBuildTools/Common/7za"
			else
				self.m_strDataToWrite = "#{kSymbolNames['ZIPALIGN']} := $(ANDROID_SDK)\\build-tools\\#{strToolchainVersion}\\zipalign.exe
#{kSymbolNames['ADB']} := $(ANDROID_SDK)\\platform-tools\\adb.exe
#{kSymbolNames['AAPT']} := $(ANDROID_SDK)\\build-tools\\#{strToolchainVersion}\\aapt.exe
#{kSymbolNames['AAPT_PREFIX']} := -S 
#{kSymbolNames['AIDL']} := $(ANDROID_SDK)\\build-tools\\#{strToolchainVersion}\\aidl.exe
#{kSymbolNames['DX']} := $(ANDROID_SDK)\\build-tools\\#{strToolchainVersion}\\dx.bat
#{kSymbolNames['COMPILER']} := $(JAVA_HOME)\\bin\\javac
#{kSymbolNames['ARCHIVER']} := $(JAVA_HOME)\\bin\\jar
#{kSymbolNames['JARSIGNER']} := $(JAVA_HOME)\\bin\\jarsigner
#{kSymbolNames['7ZIP']} := $(BE3_SDK)\\BeBuildTools\\Common\\7z.exe
#{kSymbolNames['7ZIPb']} := $(CUR_DIR)\\$(BE3_SDK)\\BeBuildTools\\Common\\7z.exe"
			end
			
			if not $g_bToolsWrittenToFile
				writeDataToFile
				writeDataToFile
			end
			
			$g_bToolsWrittenToFile = true
		end

		if OS::mac?
			kSymbolNames["mkdir"] = "-@mkdir -p"
			kSymbolNames["xcopy"] = "-@rsync -au"
			kSymbolNames["xcopy2"] = "-@rsync"
			kSymbolNames["xcopyOptions"] = ""
			kSymbolNames["xcopy_exclude"] = "--exclude-from "
			kSymbolNames["dir"] = "$@/../"
			kSymbolNames["dest_dir"] = "$@/../"
			kSymbolNames["dirFiles"] = "find"
			kSymbolNames["dirFilesOptions"] = ""
			kSymbolNames["excludePattern"] = "*"
			kSymbolNames["rmdir"] = "-@rm -rf"
      		kSymbolNames["rmdirOptions"] = ""
		else
			kSymbolNames["mkdir"] = "-@mkdir"
			kSymbolNames["xcopy"] = "-@xcopy"
			kSymbolNames["xcopy2"] = "-@copy"
			kSymbolNames["xcopyOptions"] = "/q /e /r /y"
			kSymbolNames["xcopy_exclude"] = "/exclude:"
			kSymbolNames["dir"] = "$(dir $@)\\"
			kSymbolNames["dest_dir"] = "$@"
			kSymbolNames["dirFiles"] = "-@dir"
			kSymbolNames["dirFilesOptions"] = "/S /B"
			kSymbolNames["excludePattern"] = ""
			kSymbolNames["rmdir"] = "-@rmdir"
      		kSymbolNames["rmdirOptions"] = "/S /Q %CD%\\"
		end
		
		if kProjectProperties.m_bIsMainProject
			strProjectNameToUse = kProjectProperties.m_strOutputFileName
		else
			strProjectNameToUse = kProjectProperties.m_strProjectName
		end
		
		kSymbolNames["ROOT_DIR"] = strProjectNameToUse.upcase + "_ROOT_DIR"
		
		kSymbolNames["PREREQ"] = strProjectNameToUse.upcase + "_PREREQUISITES"
		kSymbolNames["PREREQ_DEP"] = strProjectNameToUse.upcase + "_PREREQUISITES_DEP"
		kSymbolNames["PREREQ_DEP_EXT"] = strProjectNameToUse.upcase + "_PREREQUISITES_DEP_EXT"
		kSymbolNames["PREREQ_JARS"] = strProjectNameToUse.upcase + "_PREREQUISITES_JARS"
		
		if OS::mac?
			kSymbolNames["MODULE_PATH"] = strProjectNameToUse.upcase + "_MODULE_PATH"
			kSymbolNames["MODULE_PATH_assets"] = "$(#{kSymbolNames['MODULE_PATH']})/../assets"
			kSymbolNames["MODULE_PATH_assetsAndroid"] = "$(#{kSymbolNames['MODULE_PATH']})/../assetsAndroid"
			kProjectProperties.m_kAndroidGPlayExpansionFiles.each do |strExpansion, kExpansionData|
				kSymbolNames["MODULE_PATH_#{strExpansion}"] = "$(#{kSymbolNames['MODULE_PATH']})/../#{strExpansion}"
			end
			kSymbolNames["MODULE_PATH_res"] = "$(#{kSymbolNames['MODULE_PATH']})/../res"
			kSymbolNames["MODULE_PATH_libs"] = "$(#{kSymbolNames['MODULE_PATH']})/libs"
			kSymbolNames["MODULE_PATH_src"] = "$(#{kSymbolNames['MODULE_PATH']})/../src"
			kSymbolNames["MODULE_PATH_manifest"] = "$(#{kSymbolNames['MODULE_PATH']})/AndroidManifest.xml"
			
			kSymbolNames["BUILD_PATH"] = strProjectNameToUse.upcase + "_BUILD_PATH"
			kSymbolNames["BUILD_PATH_bindex"] = "$(#{kSymbolNames['BUILD_PATH']})/bin/dex"
			kSymbolNames["BUILD_PATH_unaligned"] = "$(#{kSymbolNames['BUILD_PATH']})/#{strProjectNameToUse.downcase}.apk.unaligned"
			kSymbolNames["BUILD_PATH_unsigned"] = "$(#{kSymbolNames['BUILD_PATH']})/#{strProjectNameToUse.downcase}-unsigned.apk"
			kSymbolNames["BUILD_PATH_debug"] = "$(#{kSymbolNames['BUILD_PATH']})/#{strProjectNameToUse.downcase}-debug#{strMarketAPKName}.apk"
			kSymbolNames["BUILD_PATH_release"] = "$(#{kSymbolNames['BUILD_PATH']})/#{strProjectNameToUse.downcase}-release#{strMarketAPKName}.apk"
			kSymbolNames["BUILD_PATH_production"] = "$(#{kSymbolNames['BUILD_PATH']})/#{strProjectNameToUse.downcase}_#{kProjectProperties.m_strVersionName}_STAGE_#{kProjectProperties.m_strVersionCode}#{strMarketAPKName}.apk"
			kSymbolNames["BUILD_PATH_classes.dex_dir"] = "$(#{kSymbolNames['BUILD_PATH']})/bin/dex"
			kSymbolNames["BUILD_PATH_classes.dex"] = "#{kSymbolNames['BUILD_PATH_classes.dex_dir']}/classes.dex"
			kSymbolNames["BUILD_PATH_assets"] = "$(#{kSymbolNames['BUILD_PATH']})/assets"
			kProjectProperties.m_kAndroidGPlayExpansionFiles.each do |strExpansion, kExpansionData|
				kSymbolNames["BUILD_PATH_#{strExpansion}"] = "$(#{kSymbolNames['BUILD_PATH']})/#{strExpansion}"
			end
			kSymbolNames["BUILD_PATH_res"] = "$(#{kSymbolNames['BUILD_PATH']})/res"
			kSymbolNames["BUILD_PATH_lib"] = "$(#{kSymbolNames['BUILD_PATH']})/lib"
			kSymbolNames["BUILD_PATH_src"] = "$(#{kSymbolNames['BUILD_PATH']})/src"
			kSymbolNames["BUILD_PATH_classes.jar_dir"] = "$(#{kSymbolNames['BUILD_PATH']})/bin"
			kSymbolNames["BUILD_PATH_classes.jar"] = "#{kSymbolNames['BUILD_PATH_classes.jar_dir']}/classes.jar"
			kSymbolNames["BUILD_PATH_classes.d_dir"] = "$(#{kSymbolNames['BUILD_PATH']})/bin"
			kSymbolNames["BUILD_PATH_classes.d"] = "#{kSymbolNames['BUILD_PATH_classes.d_dir']}/classes.$(#{kSymbolNames['PREREQ_DEP_EXT']})"
			kSymbolNames["BUILD_PATH_classes"] = "$(#{kSymbolNames['BUILD_PATH']})/bin/classes"
			kSymbolNames["BUILD_PATH_gen"] = "$(#{kSymbolNames['BUILD_PATH']})/gen"
			kSymbolNames["BUILD_PATH_aidl.classes.d_dir"] = "$(#{kSymbolNames['BUILD_PATH']})/aidl"
			kSymbolNames["BUILD_PATH_aidl.classes.d"] = "#{kSymbolNames['BUILD_PATH_aidl.classes.d_dir']}/aidl.classes.$(#{kSymbolNames['PREREQ_DEP_EXT']})"
			kSymbolNames["BUILD_PATH_aapt.classes.d"] = "$(#{kSymbolNames['BUILD_PATH']})/gen/aapt.classes.$(#{kSymbolNames['PREREQ_DEP_EXT']})"
			kSymbolNames["BUILD_PATH_main.jar"] = strProjectNameToUse.upcase + "_SELF_MAIN_JAR"
		else
			kSymbolNames["MODULE_PATH"] = strProjectNameToUse.upcase + "_MODULE_PATH"
			kSymbolNames["MODULE_PATH_assets"] = "$(#{kSymbolNames['MODULE_PATH']})\\..\\assets"
			kSymbolNames["MODULE_PATH_assetsAndroid"] = "$(#{kSymbolNames['MODULE_PATH']})\\..\\assetsAndroid"
			kProjectProperties.m_kAndroidGPlayExpansionFiles.each do |strExpansion, kExpansionData|
				kSymbolNames["MODULE_PATH_#{strExpansion}"] = "$(#{kSymbolNames['MODULE_PATH']})\\..\\#{strExpansion}"
			end
			kSymbolNames["MODULE_PATH_res"] = "$(#{kSymbolNames['MODULE_PATH']})\\..\\res"
			kSymbolNames["MODULE_PATH_libs"] = "$(#{kSymbolNames['MODULE_PATH']})\\libs"
			kSymbolNames["MODULE_PATH_src"] = "$(#{kSymbolNames['MODULE_PATH']})\\..\\src"
			kSymbolNames["MODULE_PATH_manifest"] = "$(#{kSymbolNames['MODULE_PATH']})\\AndroidManifest.xml"
			
			kSymbolNames["BUILD_PATH"] = strProjectNameToUse.upcase + "_BUILD_PATH"
			kSymbolNames["BUILD_PATH_bindex"] = "$(#{kSymbolNames['BUILD_PATH']})\\bin\\dex"
			kSymbolNames["BUILD_PATH_unaligned"] = "$(#{kSymbolNames['BUILD_PATH']})\\#{strProjectNameToUse.downcase}.apk.unaligned"
			kSymbolNames["BUILD_PATH_unsigned"] = "$(#{kSymbolNames['BUILD_PATH']})\\#{strProjectNameToUse.downcase}-unsigned.apk"
			kSymbolNames["BUILD_PATH_debug"] = "$(#{kSymbolNames['BUILD_PATH']})\\#{strProjectNameToUse.downcase}-debug#{strMarketAPKName}.apk"
			kSymbolNames["BUILD_PATH_release"] = "$(#{kSymbolNames['BUILD_PATH']})\\#{strProjectNameToUse.downcase}-release#{strMarketAPKName}.apk"
			kSymbolNames["BUILD_PATH_production"] = "$(#{kSymbolNames['BUILD_PATH']})\\#{strProjectNameToUse.downcase}_#{kProjectProperties.m_strVersionName}_STAGE_#{kProjectProperties.m_strVersionCode}#{strMarketAPKName}.apk"
			kSymbolNames["BUILD_PATH_classes.dex_dir"] = "$(#{kSymbolNames['BUILD_PATH']})\\bin\\dex"
			kSymbolNames["BUILD_PATH_classes.dex"] = "#{kSymbolNames['BUILD_PATH_classes.dex_dir']}\\classes.dex"
			kSymbolNames["BUILD_PATH_assets"] = "$(#{kSymbolNames['BUILD_PATH']})\\assets"
			kProjectProperties.m_kAndroidGPlayExpansionFiles.each do |strExpansion, kExpansionData|
				kSymbolNames["BUILD_PATH_#{strExpansion}"] = "$(#{kSymbolNames['BUILD_PATH']})\\#{strExpansion}"
			end
			kSymbolNames["BUILD_PATH_res"] = "$(#{kSymbolNames['BUILD_PATH']})\\res"
			kSymbolNames["BUILD_PATH_lib"] = "$(#{kSymbolNames['BUILD_PATH']})\\lib"
			kSymbolNames["BUILD_PATH_src"] = "$(#{kSymbolNames['BUILD_PATH']})\\src"
			kSymbolNames["BUILD_PATH_classes.jar_dir"] = "$(#{kSymbolNames['BUILD_PATH']})\\bin"
			kSymbolNames["BUILD_PATH_classes.jar"] = "#{kSymbolNames['BUILD_PATH_classes.jar_dir']}\\classes.jar"
			kSymbolNames["BUILD_PATH_classes.d_dir"] = "$(#{kSymbolNames['BUILD_PATH']})\\bin"
			kSymbolNames["BUILD_PATH_classes.d"] = "#{kSymbolNames['BUILD_PATH_classes.d_dir']}\\classes.$(#{kSymbolNames['PREREQ_DEP_EXT']})"
			kSymbolNames["BUILD_PATH_classes"] = "$(#{kSymbolNames['BUILD_PATH']})\\bin\\classes"
			kSymbolNames["BUILD_PATH_gen"] = "$(#{kSymbolNames['BUILD_PATH']})\\gen"
			kSymbolNames["BUILD_PATH_aidl.classes.d_dir"] = "$(#{kSymbolNames['BUILD_PATH']})\\aidl"
			kSymbolNames["BUILD_PATH_aidl.classes.d"] = "#{kSymbolNames['BUILD_PATH_aidl.classes.d_dir']}\\aidl.classes.$(#{kSymbolNames['PREREQ_DEP_EXT']})"
			kSymbolNames["BUILD_PATH_aapt.classes.d"] = "$(#{kSymbolNames['BUILD_PATH']})\\gen\\aapt.classes.$(#{kSymbolNames['PREREQ_DEP_EXT']})"
			kSymbolNames["BUILD_PATH_main.jar"] = strProjectNameToUse.upcase + "_SELF_MAIN_JAR"
		end
		
		
		if bOnlyResources or (bOnlyExpansionFiles and bFirstIteration)
			# Build and project paths
			self.m_strDataToWrite = kSymbolNames["ROOT_DIR"] + " = $(CURDIR)"
			writeDataToFile
			if OS::mac?
				if kProjectProperties.m_bIsMainProject
					self.m_strDataToWrite = kSymbolNames["MODULE_PATH"] + " = " + kProjectProperties.m_strProjectPath + kProjectProperties.m_strProjectName + PROJECT_FILES_PATH
				else
					self.m_strDataToWrite = kSymbolNames["MODULE_PATH"] + " = $(BE3_SDK)/" + kProjectProperties.m_strProjectPath + kProjectProperties.m_strProjectName + PROJECT_FILES_PATH
				end
				self.m_strDataToWrite = self.m_strDataToWrite.gsub("\\", "/")
				writeDataToFile
				
				self.m_strDataToWrite = kSymbolNames["BUILD_PATH"] + " = ./bin/Android/" + strProjectNameToUse
				writeDataToFile
			else
				if kProjectProperties.m_bIsMainProject
					self.m_strDataToWrite = kSymbolNames["MODULE_PATH"] + " = " + kProjectProperties.m_strProjectPath + kProjectProperties.m_strProjectName + PROJECT_FILES_PATH
				else
					self.m_strDataToWrite = kSymbolNames["MODULE_PATH"] + " = $(BE3_SDK)\\" + kProjectProperties.m_strProjectPath + kProjectProperties.m_strProjectName + PROJECT_FILES_PATH
				end
				self.m_strDataToWrite = self.m_strDataToWrite.gsub("/", "\\")
				writeDataToFile
				
				self.m_strDataToWrite = kSymbolNames["BUILD_PATH"] + " = .\\bin\\Android\\" + strProjectNameToUse
				self.m_strDataToWrite = self.m_strDataToWrite.gsub("/", "\\")
				writeDataToFile
			end
			writeDataToFile
		end
					
		kSymbolNames["JAVA_SOURCES"] = strProjectNameToUse.upcase + "_JAVA_SOURCE_FILES"
		kSymbolNames["JAVA_JARS"] = strProjectNameToUse.upcase + "_JAVA_JAR_FILES"
		kSymbolNames["JAVA_AIDL"] = strProjectNameToUse.upcase + "_JAVA_AIDL_SOURCE_FILES"

		if bOnlyResources
			# Project and dependency source files
			if kProjectProperties.m_bIsMainProject 
				self.m_strDataToWrite = kSymbolNames["JAVA_SOURCES"] + " := \\"
				writeDataToFile
				if OS::mac?
					writeFilesWithExt kSymbolNames, kProjectProperties.m_strProjectPath + "/" + kProjectProperties.m_strProjectName + "/", "Android", ".java", kProjectProperties.m_strIncludeFolderName, []
				else
					writeFilesWithExt kSymbolNames, kProjectProperties.m_strProjectPath + "\\" + kProjectProperties.m_strProjectName + "\\", "Android", ".java", kProjectProperties.m_strIncludeFolderName, []
				end
				writeDataToFile
				self.m_strDataToWrite = kSymbolNames["JAVA_JARS"] + " := \\"
				writeDataToFile
				if OS::mac?
					writeFilesWithExt kSymbolNames, kProjectProperties.m_strProjectPath + "/" + kProjectProperties.m_strProjectName + "/", "Android", ".jar", kProjectProperties.m_strIncludeFolderName, kProjectProperties.m_kAndroidJAROnlyClasspath
					writeFilesWithExt kSymbolNames, kProjectProperties.m_strProjectPath + "/" + kProjectProperties.m_strProjectName + "/", "Android", ".jar", kProjectProperties.m_strIncludeFolderName, kProjectProperties.m_kAndroidJAROnlyClasspath
				else
					writeFilesWithExt kSymbolNames, kProjectProperties.m_strProjectPath + "\\" + kProjectProperties.m_strProjectName + "\\", "Android", ".jar", kProjectProperties.m_strIncludeFolderName, kProjectProperties.m_kAndroidJAROnlyClasspath
				end
				writeDataToFile
				self.m_strDataToWrite = kSymbolNames["JAVA_AIDL"] + " := \\"
				writeDataToFile
				if OS::mac?
					writeFilesWithExt kSymbolNames, kProjectProperties.m_strProjectPath + "/" + kProjectProperties.m_strProjectName + "/", "Android", ".aidl", kProjectProperties.m_strIncludeFolderName, []
				else
					writeFilesWithExt kSymbolNames, kProjectProperties.m_strProjectPath + "\\" + kProjectProperties.m_strProjectName + "\\", "Android", ".aidl", kProjectProperties.m_strIncludeFolderName, []
				end
				writeDataToFile
				writeDataToFile
			else
				self.m_strDataToWrite = kSymbolNames["JAVA_SOURCES"] + " := \\"
				writeDataToFile
				if OS::mac?
					writeFilesWithExt kSymbolNames, BE3_SDK_DIR_SCRIPTS + kProjectProperties.m_strProjectPath + "/" + kProjectProperties.m_strProjectName + "/", "Android", ".java", kProjectProperties.m_strIncludeFolderName, []
				else
					writeFilesWithExt kSymbolNames, BE3_SDK_DIR_SCRIPTS + kProjectProperties.m_strProjectPath + "\\" + kProjectProperties.m_strProjectName + "\\", "Android", ".java", kProjectProperties.m_strIncludeFolderName, []
				end
				writeDataToFile
				self.m_strDataToWrite = kSymbolNames["JAVA_JARS"] + " := \\"
				writeDataToFile
				if OS::mac?
					writeFilesWithExt kSymbolNames, BE3_SDK_DIR_SCRIPTS + kProjectProperties.m_strProjectPath + "/" + kProjectProperties.m_strProjectName + "/", "Android", ".jar", kProjectProperties.m_strIncludeFolderName, kProjectProperties.m_kAndroidJAROnlyClasspath
				else
					writeFilesWithExt kSymbolNames, BE3_SDK_DIR_SCRIPTS + kProjectProperties.m_strProjectPath + "\\" + kProjectProperties.m_strProjectName + "\\", "Android", ".jar", kProjectProperties.m_strIncludeFolderName, kProjectProperties.m_kAndroidJAROnlyClasspath
				end
				writeDataToFile
				self.m_strDataToWrite = kSymbolNames["JAVA_AIDL"] + " := \\"
				writeDataToFile
				if OS::mac?
					writeFilesWithExt kSymbolNames, BE3_SDK_DIR_SCRIPTS + kProjectProperties.m_strProjectPath + "/" + kProjectProperties.m_strProjectName + "/", "Android", ".aidl", kProjectProperties.m_strIncludeFolderName, []
				else
					writeFilesWithExt kSymbolNames, BE3_SDK_DIR_SCRIPTS + kProjectProperties.m_strProjectPath + "\\" + kProjectProperties.m_strProjectName + "\\", "Android", ".aidl", kProjectProperties.m_strIncludeFolderName, []
				end
				writeDataToFile
				writeDataToFile
			end

		end
		
		kSymbolNames["JAVA_FLAGS"] = "JAVA_SOURCE_FLAGS"
		kSymbolNames["AAPT_FLAGS"] = "ANDROID_AAPT_FLAGS"
		kSymbolNames["AIDL_FLAGS"] = "ANDROID_AIDL_FLAGS"
				
		if OS::mac?
			kSymbolNames["android.jar"] = "$(ANDROID_SDK)/platforms/android-$(#{kSymbolNames['SDK']})/android.jar"
			kSymbolNames["aapt.java"] = "$@/aapt.java.$(#{kSymbolNames['PREREQ_DEP_EXT']})"
		else
			kSymbolNames["android.jar"] = "$(ANDROID_SDK)\\platforms\\android-$(#{kSymbolNames['SDK']})\\android.jar"
			kSymbolNames["aapt.java"] = "$@\\aapt.java.$(#{kSymbolNames['PREREQ_DEP_EXT']})"
		end

		if bOnlyResources
			# Common flags
			if (self.m_writingToMemory == false or (self.m_writingToMemory == true and self.m_bIsFirstMemoryProject == true))
				self.m_strDataToWrite = kSymbolNames["JAVA_FLAGS"] + " := \\
	-source 1.6 \\
	-target 1.6 \\
	-encoding UTF-8 \\
	-bootclasspath #{kSymbolNames['android.jar']} \\"
				writeDataToFile
				writeDataToFile

				self.m_strDataToWrite = kSymbolNames["AAPT_FLAGS"] + " := \\
	-0 xml \\
	-0 apk \\
	-0 class \\
	-f \\
	-m \\
	--non-constant-id \\
	--generate-dependencies \\
	--auto-add-overlay \\"
				writeDataToFile
				writeDataToFile

				if OS::mac?
					self.m_strDataToWrite = kSymbolNames["AIDL_FLAGS"] + " := \\
	-p\"$(ANDROID_SDK)/platforms/android-$(#{kSymbolNames['SDK']})/framework.aidl\" \\"
				else
					self.m_strDataToWrite = kSymbolNames["AIDL_FLAGS"] + " := \\
	-p\"$(ANDROID_SDK)\\platforms\\android-$(#{kSymbolNames['SDK']})\\framework.aidl\" \\"
				end
				writeDataToFile
				writeDataToFile
			end

		end
		
		if bOnlyResources
			# Target configurations based on expected output
			if kProjectProperties.m_bIsMainProject == true
				self.m_strDataToWrite = ".PHONY : resourcesStep"
				writeDataToFile
				writeDataToFile
				self.m_strDataToWrite = "resourcesStep : #{kSymbolNames['BUILD_PATH_gen']}"
				writeDataToFile
				writeDataToFile
			end
		else
			if bOnlyExpansionFiles
				# Target configurations based on expected output
				if kProjectProperties.m_bIsMainProject == true
				
					strExpansionAssets = ""
					kProjectProperties.m_kAndroidGPlayExpansionFiles.each do |strExpansion, kExpansionData|
						strExpansionAssets = strExpansionAssets + kSymbolNames["BUILD_PATH_#{strExpansion}"] + " "
					end
				
					self.m_strDataToWrite = ".PHONY : generateExpansionFiles"
					writeDataToFile
					writeDataToFile
					self.m_strDataToWrite = "generateExpansionFiles : #{strExpansionAssets}"
					writeDataToFile
					writeDataToFile
					
				end
			else
				# Target configurations based on expected output
				if kProjectProperties.m_bIsMainProject == true
					self.m_strDataToWrite = ".PHONY : debug release deploy-debug deploy-release deploy-production clean"
					writeDataToFile
					writeDataToFile
					self.m_strDataToWrite = "debug : #{kSymbolNames["BUILD_PATH_debug"]}"
					writeDataToFile
					self.m_strDataToWrite = "release : #{kSymbolNames["BUILD_PATH_release"]}"
					writeDataToFile
					writeDataToFile
					self.m_strDataToWrite = "deploy-debug : #{kSymbolNames["BUILD_PATH_debug"]}
		@echo \"  Deploying:  $<\"
		$(#{kSymbolNames["ADB"]}) install -r $<"
					writeDataToFile
					writeDataToFile
					self.m_strDataToWrite = "deploy-release : #{kSymbolNames["BUILD_PATH_release"]}
		@echo \"  Deploying:  $<\"
		$(#{kSymbolNames["ADB"]}) install -r $<"
					writeDataToFile
					writeDataToFile
					self.m_strDataToWrite = "deploy-production : #{kSymbolNames["BUILD_PATH_production"]}
		@echo \"  APK ready for being uploaded to Houston:  $<\""
					writeDataToFile
					writeDataToFile
					self.m_strDataToWrite = "clean :	
		#{kSymbolNames['rmdir']} #{kSymbolNames['rmdirOptions']}$(#{kSymbolNames["BUILD_PATH"]})"
					writeDataToFile
					kLibraryReferenceVisited.reverse.each do |kProjectDependencyName|
						self.m_strDataToWrite = "	#{kSymbolNames['rmdir']} #{kSymbolNames['rmdirOptions']}$(" + kProjectDependencyName.upcase + "_BUILD_PATH)"
						writeDataToFile
					end
					writeDataToFile
				end
			end
		end
		
		kSymbolNames["PREREQ_RES"] = strProjectNameToUse.upcase + "_PREREQUISITES_RES"		

		if not bOnlyResources and not bOnlyExpansionFiles
			# Configure debug and release packages. Sign and zip-align as required.
			if kProjectProperties.m_bIsMainProject == true
			
				strAvoidAddingExpansionToZip = ""
				kProjectProperties.m_kAndroidGPlayExpansionFiles.each do |strExpansion, kExpansionData|
					strAvoidAddingExpansionToZip = strAvoidAddingExpansionToZip + "-xr!#{strExpansion} " 
				end
			
				self.m_strDataToWrite = "#{kSymbolNames['BUILD_PATH_unaligned']} : #{kSymbolNames['BUILD_PATH_classes.dex']} #{kSymbolNames['BUILD_PATH_lib']} #{kSymbolNames['BUILD_PATH_res']} $(#{kSymbolNames['PREREQ_RES']}) #{kSymbolNames['BUILD_PATH_assets']}"
				writeDataToFile
				self.m_strDataToWrite = "	@echo \"  Generating:  $@\""
				writeDataToFile
				self.m_strDataToWrite = "	$(#{kSymbolNames['AAPT']}) p $(#{kSymbolNames['AAPT_FLAGS']}) -M \"#{kSymbolNames['MODULE_PATH_manifest']}\" $(addprefix $(#{kSymbolNames['AAPT_PREFIX']}), $(#{kSymbolNames['PREREQ_RES']})) -I \"#{kSymbolNames['android.jar']}\" -F $@ #{kSymbolNames['BUILD_PATH_bindex']}"
				writeDataToFile
				self.m_strDataToWrite = "	@echo \"  Compressing native libraries:  $@\""
				writeDataToFile
				self.m_strDataToWrite = "	$(#{kSymbolNames['7ZIP']}) a -tzip -mx5 -r $@ #{kSymbolNames['BUILD_PATH_lib']}"
				writeDataToFile
				self.m_strDataToWrite = "	@echo \"  Compressing package assets:  $@\""
				writeDataToFile
				self.m_strDataToWrite = "	$(#{kSymbolNames['7ZIP']}) a -tzip -mx5 -r $@ #{kSymbolNames['BUILD_PATH_assets']} #{strAvoidAddingExpansionToZip}-xr!assetsAndroid -xr!sound"
				writeDataToFile
				self.m_strDataToWrite = "	@echo \"  Copying to package already compressed assets:  $@\""
				writeDataToFile
                if OS::mac?
                    self.m_strDataToWrite = "	cd \"#{kSymbolNames['BUILD_PATH_assets']}/assetsAndroid\" && $(#{kSymbolNames['7ZIPb']}) a -tzip -mx0 -r $(CUR_DIR)/$@ assets/sound && cd $(CUR_DIR)"
                    writeDataToFile
                else
					self.m_strDataToWrite = "	cd \"#{kSymbolNames['BUILD_PATH_assets']}\\assetsAndroid\" && $(#{kSymbolNames['7ZIPb']}) a -tzip -mx0 -r $(CUR_DIR)\\$@ assets\\sound && cd $(CUR_DIR)"
                    writeDataToFile
                end
				writeDataToFile

				self.m_strDataToWrite = "#{kSymbolNames['BUILD_PATH_unsigned']} : #{kSymbolNames['BUILD_PATH_unaligned']}"
				writeDataToFile
				self.m_strDataToWrite = "	@echo \"  Generating:  $@\""
				writeDataToFile
				self.m_strDataToWrite = "	@echo \"  ZipAligning:  $@\""
				writeDataToFile
				self.m_strDataToWrite = "	$(#{kSymbolNames['ZIPALIGN']}) -f 4 $@.unaligned $@"
				writeDataToFile
				writeDataToFile

				strProxySettings = ""
				if $g_bAndroidSignProxyServer.length > 0 and $g_bAndroidSignProxyPort.length > 0
					strProxySettings = "-J-Dhttps.proxyHost=#{$g_bAndroidSignProxyServer} -J-Dhttps.proxyPort=#{$g_bAndroidSignProxyPort}"
				end
				
				self.m_strDataToWrite = "#{kSymbolNames['BUILD_PATH_debug']} : #{kSymbolNames['BUILD_PATH_unaligned']}"
				writeDataToFile
				self.m_strDataToWrite = "	@echo \"  Signing:  $@\""
				writeDataToFile
				self.m_strDataToWrite = "	$(#{kSymbolNames['JARSIGNER']}) -tsa https://timestamp.geotrust.com/tsa #{strProxySettings} -digestalg SHA1 -sigalg MD5withRSA -keystore \"#{kProjectProperties.m_strProjectPath}#{kProjectProperties.m_strProjectName}/#{kProjectProperties.m_strAndroidDebugKeyStoreFilePath}\" -storepass #{kProjectProperties.m_strAndroidDebugKeyStorePass} -signedjar $@.unaligned $< #{kProjectProperties.m_strAndroidDebugKeyAlias}"
				writeDataToFile
				self.m_strDataToWrite = "	@echo \"  ZipAligning:  $@\""
				writeDataToFile
				self.m_strDataToWrite = "	$(#{kSymbolNames['ZIPALIGN']}) -f 4 $@.unaligned $@"
				writeDataToFile
				writeDataToFile

				self.m_strDataToWrite = "#{kSymbolNames['BUILD_PATH_release']} : #{kSymbolNames['BUILD_PATH_unaligned']}"
				writeDataToFile
				self.m_strDataToWrite = "	@echo \"  Signing:  $@\""
				writeDataToFile
				self.m_strDataToWrite = "	$(#{kSymbolNames['JARSIGNER']}) -tsa https://timestamp.geotrust.com/tsa #{strProxySettings} -digestalg SHA1 -sigalg MD5withRSA -keystore \"#{kProjectProperties.m_strProjectPath}#{kProjectProperties.m_strProjectName}/#{kProjectProperties.m_strAndroidReleaseKeyStoreFilePath}\" -storepass #{kProjectProperties.m_strAndroidReleaseKeyStorePass} -signedjar $@.unaligned $< #{kProjectProperties.m_strAndroidReleaseKeyAlias}"
				writeDataToFile
				self.m_strDataToWrite = "	@echo \"  ZipAligning:  $@\""
				writeDataToFile
				self.m_strDataToWrite = "	$(#{kSymbolNames['ZIPALIGN']}) -f 4 $@.unaligned $@"
				writeDataToFile
				writeDataToFile
				
				self.m_strDataToWrite = "#{kSymbolNames['BUILD_PATH_production']} : #{kSymbolNames['BUILD_PATH_unaligned']}"
				writeDataToFile
				self.m_strDataToWrite = "	@echo \"  Copying:  $< to $@.unaligned (avoid signing)\""
				writeDataToFile
				self.m_strDataToWrite = "	#{kSymbolNames['xcopy2']} $< $@.unaligned"
				writeDataToFile
				self.m_strDataToWrite = "	@echo \"  ZipAligning:  $@\""
				writeDataToFile
				self.m_strDataToWrite = "	$(#{kSymbolNames['ZIPALIGN']}) -f 4 $@.unaligned $@"
				writeDataToFile
				writeDataToFile
			end
		end
		
		if bOnlyResources
			# Evaluate prerequisite dependencies for this build module
			self.m_strDataToWrite = kSymbolNames["PREREQ"] + " := \\"
			writeDataToFile
			#if kProjectProperties.m_bIsMainProject == true
			#	self.m_strDataToWrite = "	#{kSymbolNames['BUILD_PATH_classes.jar']} \\"
			#	writeDataToFile
			#end
			kLibraryReferenceVisited.reverse.each do |kProjectDependencyName|
				if OS::mac?
					self.m_strDataToWrite = "	$(" + kProjectDependencyName.upcase + "_BUILD_PATH)/bin/classes.jar \\"
				else
					self.m_strDataToWrite = "	$(" + kProjectDependencyName.upcase + "_BUILD_PATH)\\bin\\classes.jar \\"
				end
				writeDataToFile
			end
			writeDataToFile
			
			if kProjectProperties.m_bIsMainProject == true
				self.m_strDataToWrite = kSymbolNames['BUILD_PATH_main.jar'] + " := \\"
				writeDataToFile
				self.m_strDataToWrite = "	#{kSymbolNames["BUILD_PATH_classes.jar"]} \\"
				writeDataToFile
				writeDataToFile
			end

			self.m_strDataToWrite = kSymbolNames["PREREQ_DEP_EXT"] + " := d"
			writeDataToFile
			writeDataToFile
			
			self.m_strDataToWrite = kSymbolNames["PREREQ_DEP"] + " := \\"
			writeDataToFile
			if kProjectProperties.m_bIsMainProject == true
				#self.m_strDataToWrite = "	$(" + kProjectProperties.m_strProjectName.upcase + "_BUILD_PATH)\\bin\\classes.$(#{kSymbolNames['PREREQ_DEP_EXT']}) \\"
				#writeDataToFile
			end
			kLibraryReferenceVisited.reverse.each do |kProjectDependencyName|
				if OS::mac?
					self.m_strDataToWrite = "	$(" + kProjectDependencyName.upcase + "_BUILD_PATH)/bin/classes.$(#{kSymbolNames['PREREQ_DEP_EXT']}) \\"
				else
					self.m_strDataToWrite = "	$(" + kProjectDependencyName.upcase + "_BUILD_PATH)\\bin\\classes.$(#{kSymbolNames['PREREQ_DEP_EXT']}) \\"
				end
				writeDataToFile
			end
			writeDataToFile

			self.m_strDataToWrite = kSymbolNames["PREREQ_JARS"] + " := \\"
			writeDataToFile
			if kProjectProperties.m_bIsMainProject == true
				self.m_strDataToWrite = "	$(" + kProjectProperties.m_strOutputFileName.upcase + "_JAVA_JAR_FILES) \\"
				writeDataToFile
			end
			kLibraryReferenceVisited.reverse.each do |kProjectDependencyName|
				self.m_strDataToWrite = "	$(" + kProjectDependencyName.upcase + "_JAVA_JAR_FILES) \\"
				writeDataToFile
			end
			writeDataToFile
			
			self.m_strDataToWrite = kSymbolNames["PREREQ_RES"] + " := \\"
			writeDataToFile
			self.m_strDataToWrite = "	#{kSymbolNames['BUILD_PATH_res']} \\"
			writeDataToFile
			kLibraryReferenceVisited.reverse.each do |kProjectDependencyName|
				if OS::mac?
					self.m_strDataToWrite = "	$(" + kProjectDependencyName.upcase + "_BUILD_PATH)/res \\"
				else
					self.m_strDataToWrite = "	$(" + kProjectDependencyName.upcase + "_BUILD_PATH)\\res \\"
				end
				writeDataToFile
			end
			writeDataToFile
		end
		
		if not bOnlyResources and not bOnlyExpansionFiles
			# Generate dex (Dalvik Executable) for the application and dependency classes.
			# - Note: dex tool requires absolute paths
			strSpecificClassDex = ""
			if kProjectProperties.m_bIsMainProject == true
				self.m_strDataToWrite = "#{kSymbolNames['BUILD_PATH_classes.dex']} : $(#{kSymbolNames['PREREQ']}) #{kSymbolNames['BUILD_PATH_classes.jar']}"
				strSpecificClassDex = "$(addprefix $(#{kSymbolNames['ROOT_DIR']})/,$(#{kSymbolNames['BUILD_PATH_main.jar']})) "
			else
				self.m_strDataToWrite = "#{kSymbolNames['BUILD_PATH_classes.dex']} : $(#{kSymbolNames['PREREQ']})"
			end
			writeDataToFile
			self.m_strDataToWrite = "	@echo \"  Generating:  $@\""
			writeDataToFile
			self.m_strDataToWrite = "	#{kSymbolNames["mkdir"]} #{kSymbolNames['BUILD_PATH_classes.dex_dir']}"
			writeDataToFile
			if OS::mac?
				self.m_strDataToWrite = "	$(#{kSymbolNames['DX']}) --dex --verbose --output=\"$(#{kSymbolNames['ROOT_DIR']})/$@\" $(addprefix $(#{kSymbolNames['ROOT_DIR']})/,$(#{kSymbolNames['PREREQ']})) #{strSpecificClassDex}$(addprefix $(#{kSymbolNames['ROOT_DIR']})/,$(#{kSymbolNames['PREREQ_JARS']}))"
			else
				self.m_strDataToWrite = "	$(#{kSymbolNames['DX']}) --dex --verbose --output=\"$(#{kSymbolNames['ROOT_DIR']})\\$@\" $(addprefix $(#{kSymbolNames['ROOT_DIR']})\\,$(#{kSymbolNames['PREREQ']})) #{strSpecificClassDex}$(addprefix $(#{kSymbolNames['ROOT_DIR']})\\,$(#{kSymbolNames['PREREQ_JARS']}))"
			end
			writeDataToFile
			writeDataToFile
		end
				
		kSymbolNames["SPACE_NOOP"] = "CLASSPATH_SPACE_NOOP"
		kSymbolNames["SPACE_PATTERN"] = "CLASSPATH_SPACE_PATTERN"
		kSymbolNames["FILE_PATTERN"] = "ABSOLUTE_SOURCEFILE_PATTERN"

		if not bOnlyResources and not bOnlyExpansionFiles
			# Classpath-ify the word list, strip spaces and append ".jar;"
			if (self.m_writingToMemory == false or (self.m_writingToMemory == true and self.m_bIsFirstMemoryProject == true))
				self.m_strDataToWrite = "#{kSymbolNames['SPACE_NOOP']} :="
				writeDataToFile
				self.m_strDataToWrite = "#{kSymbolNames['SPACE_PATTERN']} := $(#{kSymbolNames['SPACE_NOOP']}) $(#{kSymbolNames['SPACE_NOOP']})"
				writeDataToFile
				self.m_strDataToWrite = "#{kSymbolNames['FILE_PATTERN']} := @"
				writeDataToFile
				writeDataToFile
			end

			self.m_strDataToWrite = "#{kSymbolNames['BUILD_PATH_classes.jar']} : #{kSymbolNames['BUILD_PATH_classes.d']}"
			writeDataToFile
			self.m_strDataToWrite = "	@echo \"  Archiving:  $@\""
			writeDataToFile
			if OS::mac?
				self.m_strDataToWrite = "	$(#{kSymbolNames['ARCHIVER']}) cf0 $@ -C #{kSymbolNames['BUILD_PATH_classes.jar_dir']}/classes ."
			else
				self.m_strDataToWrite = "	$(#{kSymbolNames['ARCHIVER']}) cf0 $@ -C #{kSymbolNames['BUILD_PATH_classes.jar_dir']}\\classes ."
			end
			writeDataToFile
			writeDataToFile
		
		end
		
		kSymbolNames["CLASSPATH"] = strProjectNameToUse.upcase + "_CLASSPATH"
		if OS::mac?
			kSymbolNames["annotations.jar"] = "$(ANDROID_SDK)/tools/support/annotations.jar"
		else
			kSymbolNames["annotations.jar"] = "$(ANDROID_SDK)\\tools\\support\\annotations.jar"
		end

		if not bOnlyResources and not bOnlyExpansionFiles
			# Compile project and dependency Java source files
			self.m_strDataToWrite = "#{kSymbolNames['CLASSPATH']} := \\
	#{kSymbolNames['annotations.jar']} \\
	#{kSymbolNames['BUILD_PATH_classes']} \\
	#{kSymbolNames['BUILD_PATH_gen']} \\
	$(#{kSymbolNames['PREREQ']}) \\
	$(#{kSymbolNames['PREREQ_JARS']}) \\
	$(#{kSymbolNames['JAVA_JARS']}) \\"
			for i in 0..(kProjectProperties.m_kAndroidJAROnlyClasspath.length-1)
				strTemp = "	$(#{kSymbolNames['MODULE_PATH']})/../%1/%2 \\"
				strTemp = strTemp.gsub("%1", "Android")
				strTemp = strTemp.gsub("%2", kProjectProperties.m_kAndroidJAROnlyClasspath[i])
				if not OS::mac?
					strTemp = strTemp.gsub("/", "\\")
				end
			
				self.m_strDataToWrite += "\n#{strTemp}"
			end			
			writeDataToFile
			writeDataToFile
		
			self.m_strDataToWrite = "#{kSymbolNames['BUILD_PATH_classes.d']} : $(#{kSymbolNames['PREREQ_DEP']}) $(#{kSymbolNames['PREREQ']}) #{kSymbolNames['BUILD_PATH_aidl.classes.d']}"
			writeDataToFile
			self.m_strDataToWrite = "	@echo \"  Compiling:  #{strProjectNameToUse}\""
			writeDataToFile
			self.m_strDataToWrite = "ifneq ($(words $(#{kSymbolNames['JAVA_SOURCES']})), 0)"
			writeDataToFile
			if OS::mac?
				self.m_strDataToWrite = "	$(#{kSymbolNames['COMPILER']}) $(#{kSymbolNames['JAVA_FLAGS']}) -d #{kSymbolNames['BUILD_PATH_classes.d_dir']}/classes -classpath \"$(subst $(#{kSymbolNames['SPACE_PATTERN']}),:,$(#{kSymbolNames['CLASSPATH']}))\" $(#{kSymbolNames['JAVA_SOURCES']})"
			else
				self.m_strDataToWrite = "	$(#{kSymbolNames['COMPILER']}) $(#{kSymbolNames['JAVA_FLAGS']}) -d #{kSymbolNames['BUILD_PATH_classes.d_dir']}\\classes -classpath \"$(subst $(#{kSymbolNames['SPACE_PATTERN']}),;,$(#{kSymbolNames['CLASSPATH']}))\" $(#{kSymbolNames['JAVA_SOURCES']})"
			end
			writeDataToFile
			if OS::mac?
				self.m_strDataToWrite = "	#{kSymbolNames['dirFiles']} #{kSymbolNames['BUILD_PATH_classes.d_dir']} -name \"*.class\" > $@"
			else
				self.m_strDataToWrite = "	#{kSymbolNames['dirFiles']} #{kSymbolNames['BUILD_PATH_classes.d_dir']}\\*.class #{kSymbolNames['dirFilesOptions']} > $@"
			end
			writeDataToFile
			self.m_strDataToWrite = "else"
			writeDataToFile
			if OS::mac?
				self.m_strDataToWrite = "	>$@"
			else
				self.m_strDataToWrite = "	@echo. 2>$@"
			end
			writeDataToFile
			self.m_strDataToWrite = "endif"
			writeDataToFile
			writeDataToFile
			
			self.m_strDataToWrite = "#{kSymbolNames['BUILD_PATH_classes']} :"
			writeDataToFile
			self.m_strDataToWrite = "	#{kSymbolNames['mkdir']} $@"
			writeDataToFile
			writeDataToFile

			# Pre-process AIDL interface descriptor files
			self.m_strDataToWrite = "#{kSymbolNames['BUILD_PATH_aidl.classes.d']} : #{kSymbolNames['BUILD_PATH_classes']}"
			writeDataToFile		
			self.m_strDataToWrite = "	#{kSymbolNames['mkdir']} #{kSymbolNames['BUILD_PATH_aidl.classes.d_dir']}"
			writeDataToFile
			self.m_strDataToWrite = "ifneq ($(words $(#{kSymbolNames['JAVA_AIDL']})), 0)"
			writeDataToFile
			self.m_strDataToWrite = "	@echo \"  Processing AIDL interface:  $<\""
			writeDataToFile
			if OS::mac?
				self.m_strDataToWrite = "	$(#{kSymbolNames['AIDL']}) -o$(#{kSymbolNames['BUILD_PATH']})/bin/classes -d$@ $(#{kSymbolNames['AIDL_FLAGS']}) $(#{kSymbolNames['JAVA_AIDL']})"
			else
				self.m_strDataToWrite = "	$(#{kSymbolNames['AIDL']}) -o$(#{kSymbolNames['BUILD_PATH']})\\bin\\classes -d$@ $(#{kSymbolNames['AIDL_FLAGS']}) $(#{kSymbolNames['JAVA_AIDL']})"
			end
			writeDataToFile
			self.m_strDataToWrite = "else"
			writeDataToFile
			if OS::mac?
				self.m_strDataToWrite = "	>$@"
			else
				self.m_strDataToWrite = "	@echo. 2>$@"
			end
			writeDataToFile
			self.m_strDataToWrite = "endif"
			writeDataToFile
			writeDataToFile

		end
		
		kSymbolNames["RES_AAPT_FLAGS"] = strProjectNameToUse.upcase + "_RES_AAPT_FLAGS"

		if bOnlyResources
			# Pre-process resources using AAPT tool
			self.m_strDataToWrite = kSymbolNames['RES_AAPT_FLAGS'] + " := \\"
			writeDataToFile
			self.m_strDataToWrite = "	-I #{kSymbolNames['android.jar']} \\"
			writeDataToFile
			writeDataToFile
		end
	
		if bOnlyResources
			self.m_strDataToWrite = "#{kSymbolNames['BUILD_PATH_gen']} : $(#{kSymbolNames['PREREQ_RES']})"
			writeDataToFile
			self.m_strDataToWrite = "	@echo \"  Pre-processing:  $<\""
			writeDataToFile
			self.m_strDataToWrite = "	#{kSymbolNames['mkdir']} $@"
			writeDataToFile
			self.m_strDataToWrite = "	$(#{kSymbolNames['AAPT']}) p $(#{kSymbolNames['AAPT_FLAGS']}) -M #{kSymbolNames['MODULE_PATH_manifest']} $(#{kSymbolNames['RES_AAPT_FLAGS']}) $(addprefix $(#{kSymbolNames['AAPT_PREFIX']}), $(#{kSymbolNames['PREREQ_RES']})) -J $@"
			writeDataToFile
			self.m_strDataToWrite = "	@echo \"  Exporting resources:  $<\""
			writeDataToFile
			if OS::mac?
				self.m_strDataToWrite = "	#{kSymbolNames['dirFiles']} $@ -name \"*.java\" > #{kSymbolNames['aapt.java']}"
			else
				self.m_strDataToWrite = "	#{kSymbolNames['dirFiles']} $@\\*.java #{kSymbolNames['dirFilesOptions']} > #{kSymbolNames['aapt.java']}"
			end
			writeDataToFile
			self.m_strDataToWrite = "	$(#{kSymbolNames['COMPILER']}) $(#{kSymbolNames['JAVA_FLAGS']}) -d $@ @#{kSymbolNames['aapt.java']}"
			writeDataToFile
			writeDataToFile
		end
		
		kSymbolNames["MODULE_ASSETS"] = strProjectNameToUse.upcase + "_MODULE_ASSETS"
		kSymbolNames["MODULE_RES"] = strProjectNameToUse.upcase + "_MODULE_RES"
		kSymbolNames["MODULE_LIBS"] = strProjectNameToUse.upcase + "_MODULE_LIBS"
		kSymbolNames["MODULE_SRC"] = strProjectNameToUse.upcase + "_MODULE_SRC"

		if not bOnlyResources
		
			if not bOnlyExpansionFiles
				# Create or copy any required data directories
				if kProjectProperties.m_bIsMainProject == true
					self.m_strDataToWrite = "#{kSymbolNames['MODULE_ASSETS']} := #{kSymbolNames['MODULE_PATH_assets']}"
					writeDataToFile
				end
				
				self.m_strDataToWrite = "#{kSymbolNames['MODULE_LIBS']} := #{kSymbolNames['MODULE_PATH_libs']}"
				writeDataToFile
				self.m_strDataToWrite = "#{kSymbolNames['MODULE_SRC']} := #{kSymbolNames['MODULE_PATH_src']}"
				writeDataToFile
				writeDataToFile
			end
			
			if kProjectProperties.m_bIsMainProject == true

				strExtraExcludeFiles = ""
				kProjectProperties.m_kAndroidGPlayExpansionFiles.each do |strExpansion, kExpansionData|
					if OS::mac?
						strExtraExcludeFiles = strExtraExcludeFiles + " #{kSymbolNames['xcopy_exclude']}#{kSymbolNames['dir']}#{strExpansion}.excludes.d"
					else
						strExtraExcludeFiles = strExtraExcludeFiles + "+#{kSymbolNames['dir']}#{strExpansion}.excludes.d"
					end
				end
			
				if bOnlyExpansionFiles
				
					kProjectProperties.m_kAndroidGPlayExpansionFiles.each do |strExpansion, kExpansionData|
						strBuildPathExpansion = kSymbolNames["BUILD_PATH_#{strExpansion}"]
						strModulePathExpansion = kSymbolNames["MODULE_PATH_#{strExpansion}"]
						
						strFinalPackageName = ""
						if kProjectProperties.m_strForcedBundlePackage != nil
							strFinalPackageName = kProjectProperties.m_strForcedBundlePackage
						else
							strFinalPackageName = "com.#{kProjectProperties.m_strCompanyName.downcase}.#{kProjectProperties.m_strOutputFileName.downcase}"
						end
						
						if strExpansion.downcase.include? "main"
							strZipFilename = "main."
						else
							strZipFilename = "patch."
						end
						strZipFilename = strZipFilename + kExpansionData[1] + "." + strFinalPackageName + ".obb"
						
						self.m_strDataToWrite = "#{strBuildPathExpansion} :"
						writeDataToFile
						self.m_strDataToWrite = "	@echo \"  Preparing expansion file $@ ...\""
						writeDataToFile
						self.m_strDataToWrite = "	#{kSymbolNames['mkdir']} $@"
						writeDataToFile
						self.m_strDataToWrite = "	-@echo #{kSymbolNames['excludePattern']}.svn > \"#{kSymbolNames["dir"]}asset.excludes.d\""
						writeDataToFile
						self.m_strDataToWrite = "	-@echo #{kSymbolNames['excludePattern']}.obj >> \"#{kSymbolNames["dir"]}asset.excludes.d\""
						writeDataToFile
						self.m_strDataToWrite = "	-@echo #{kSymbolNames['excludePattern']}.mtl >> \"#{kSymbolNames["dir"]}asset.excludes.d\""
						writeDataToFile
						self.m_strDataToWrite = "	-@echo #{kSymbolNames['excludePattern']}.md5mesh >> \"#{kSymbolNames["dir"]}asset.excludes.d\""
						writeDataToFile
						self.m_strDataToWrite = "	-@echo #{kSymbolNames['excludePattern']}.md5anim >> \"#{kSymbolNames["dir"]}asset.excludes.d\""
						writeDataToFile
						self.m_strDataToWrite = "	-@echo #{kSymbolNames['excludePattern']}.ply >> \"#{kSymbolNames["dir"]}asset.excludes.d\""
						writeDataToFile
						self.m_strDataToWrite = "	-@echo #{kSymbolNames['excludePattern']}.rawmat >> \"#{kSymbolNames["dir"]}asset.excludes.d\""
						writeDataToFile
						self.m_strDataToWrite = "	#{kSymbolNames['xcopy']} #{strModulePathExpansion} $@ #{kSymbolNames['xcopyOptions']} #{kSymbolNames['xcopy_exclude']}#{kSymbolNames['dir']}asset.excludes.d"
						writeDataToFile
						self.m_strDataToWrite = "	@echo \"  Compressing expansion file\""
						writeDataToFile
						if OS::mac?
							self.m_strDataToWrite = "	$(#{kSymbolNames['7ZIP']}) a -p#{kExpansionData[0]} -tzip -mx5 -r #{kSymbolNames["dir"]}#{strZipFilename} $@/assets"
						else
							self.m_strDataToWrite = "	$(#{kSymbolNames['7ZIP']}) a -p#{kExpansionData[0]} -tzip -mx5 -r #{kSymbolNames["dir"]}#{strZipFilename} $@\\assets"
						end
						writeDataToFile
						writeDataToFile
					end
					
				else
			
					self.m_strDataToWrite = "#{kSymbolNames['BUILD_PATH_assets']} :"
					writeDataToFile
					self.m_strDataToWrite = "	@echo \"  Copying $(#{kSymbolNames['MODULE_ASSETS']}) to $@ ...\""
					writeDataToFile
					self.m_strDataToWrite = "	#{kSymbolNames['mkdir']} $@"
					writeDataToFile
					self.m_strDataToWrite = "	-@echo #{kSymbolNames['excludePattern']}.svn > \"#{kSymbolNames["dir"]}asset.excludes.d\""
					writeDataToFile
					self.m_strDataToWrite = "	-@echo #{kSymbolNames['excludePattern']}.obj >> \"#{kSymbolNames["dir"]}asset.excludes.d\""
					writeDataToFile
					self.m_strDataToWrite = "	-@echo #{kSymbolNames['excludePattern']}.mtl >> \"#{kSymbolNames["dir"]}asset.excludes.d\""
					writeDataToFile
					self.m_strDataToWrite = "	-@echo #{kSymbolNames['excludePattern']}.md5mesh >> \"#{kSymbolNames["dir"]}asset.excludes.d\""
					writeDataToFile
					self.m_strDataToWrite = "	-@echo #{kSymbolNames['excludePattern']}.md5anim >> \"#{kSymbolNames["dir"]}asset.excludes.d\""
					writeDataToFile
					self.m_strDataToWrite = "	-@echo #{kSymbolNames['excludePattern']}.ply >> \"#{kSymbolNames["dir"]}asset.excludes.d\""
					writeDataToFile
					self.m_strDataToWrite = "	-@echo #{kSymbolNames['excludePattern']}.rawmat >> \"#{kSymbolNames["dir"]}asset.excludes.d\""
					writeDataToFile
					self.m_strDataToWrite = "	#{kSymbolNames['xcopy']} $(#{kSymbolNames['MODULE_ASSETS']}) #{kSymbolNames['dest_dir']} #{kSymbolNames['xcopyOptions']} #{kSymbolNames['xcopy_exclude']}#{kSymbolNames['dir']}asset.excludes.d#{strExtraExcludeFiles}"
					writeDataToFile
					self.m_strDataToWrite = "	-@echo \"  Copying #{kSymbolNames['MODULE_PATH_assetsAndroid']} to $@ ...\""
					writeDataToFile
					if OS::mac?
						self.m_strDataToWrite = "	#{kSymbolNames['xcopy']} #{kSymbolNames['MODULE_PATH_assetsAndroid']} #{kSymbolNames['dest_dir']}/assets #{kSymbolNames['xcopyOptions']} #{kSymbolNames['xcopy_exclude']}#{kSymbolNames['dir']}asset.excludes.d#{strExtraExcludeFiles}"
					else
						self.m_strDataToWrite = "	#{kSymbolNames['xcopy']} #{kSymbolNames['MODULE_PATH_assetsAndroid']} #{kSymbolNames['dest_dir']}\\assetsAndroid\\ #{kSymbolNames['xcopyOptions']} #{kSymbolNames['xcopy_exclude']}#{kSymbolNames['dir']}asset.excludes.d#{strExtraExcludeFiles}"
					end
					writeDataToFile
					if kProjectProperties.m_kAndroidGPlayExpansionFiles.length > 0
						if OS::mac?
							self.m_strDataToWrite = "	#{kSymbolNames['xcopy2']} \"#{kSymbolNames["dir"]}expansionFiles.dat\" \"#{kSymbolNames["dir"]}assets/expansionFiles.dat\""
						else
							self.m_strDataToWrite = "	#{kSymbolNames['xcopy2']} \"#{kSymbolNames["dir"]}expansionFiles.dat\" \"#{kSymbolNames["dir"]}assets\\expansionFiles.dat\""
						end
						writeDataToFile
					end
					
					writeDataToFile
					
				end
				
			end
		else
			self.m_strDataToWrite = "#{kSymbolNames['MODULE_RES']} := #{kSymbolNames['MODULE_PATH_res']}"
			writeDataToFile
			writeDataToFile
		end
			
		if bOnlyResources
			self.m_strDataToWrite = "#{kSymbolNames['BUILD_PATH_res']} :"
			writeDataToFile
			self.m_strDataToWrite = "	@echo \"  Copying $(#{kSymbolNames['MODULE_RES']}) to $@ ...\""
			writeDataToFile
			self.m_strDataToWrite = "	#{kSymbolNames['mkdir']} $@"
			writeDataToFile
			self.m_strDataToWrite = "	-@echo #{kSymbolNames['excludePattern']}.svn > \"#{kSymbolNames["dir"]}res.excludes.d\""
			writeDataToFile
			self.m_strDataToWrite = "	#{kSymbolNames['xcopy']} $(#{kSymbolNames['MODULE_RES']}) #{kSymbolNames['dest_dir']} #{kSymbolNames['xcopyOptions']} #{kSymbolNames['xcopy_exclude']}#{kSymbolNames['dir']}res.excludes.d"
			writeDataToFile
			writeDataToFile
		end

		if not bOnlyResources and not bOnlyExpansionFiles
			self.m_strDataToWrite = "#{kSymbolNames['BUILD_PATH_lib']} :"
			writeDataToFile
			self.m_strDataToWrite = "	@echo \"  Copying $(#{kSymbolNames['MODULE_LIBS']}) to $@ ...\""
			writeDataToFile
			self.m_strDataToWrite = "	#{kSymbolNames['mkdir']} $@"
			writeDataToFile
			self.m_strDataToWrite = "	-@echo #{kSymbolNames['excludePattern']}.svn > \"#{kSymbolNames["dir"]}lib.excludes.d\""
			writeDataToFile
			if OS::mac?
				kABIs = kProjectProperties.m_strAndroidABI.split
				kABIs.each do |strABI|
					self.m_strDataToWrite = "	#{kSymbolNames['xcopy']} $(#{kSymbolNames['MODULE_LIBS']})/#{strABI} $@ #{kSymbolNames['xcopyOptions']} #{kSymbolNames['xcopy_exclude']}#{kSymbolNames['dir']}lib.excludes.d"
					writeDataToFile
				end
			else
				self.m_strDataToWrite = "	#{kSymbolNames['xcopy']} $(#{kSymbolNames['MODULE_LIBS']}) $@ #{kSymbolNames['xcopyOptions']} #{kSymbolNames['xcopy_exclude']}#{kSymbolNames['dir']}lib.excludes.d"
				writeDataToFile
			end
			writeDataToFile

			self.m_strDataToWrite = "#{kSymbolNames['BUILD_PATH_src']} :"
			writeDataToFile
			self.m_strDataToWrite = "	@echo \"  Copying $(#{kSymbolNames['MODULE_SRC']}) to $@ ...\""
			writeDataToFile
			self.m_strDataToWrite = "	#{kSymbolNames['mkdir']} $@"
			writeDataToFile
			self.m_strDataToWrite = "	-@echo #{kSymbolNames['excludePattern']}.svn > \"#{kSymbolNames["dir"]}src.excludes.d\""
			writeDataToFile
			self.m_strDataToWrite = "	#{kSymbolNames['xcopy']} $(#{kSymbolNames['MODULE_SRC']}) #{kSymbolNames['dest_dir']} #{kSymbolNames['xcopyOptions']} #{kSymbolNames['xcopy_exclude']}#{kSymbolNames['dir']}src.excludes.d"
			writeDataToFile
			writeDataToFile
		end

		if bFirstIteration
			if self.m_writingToMemory == false
				self.m_outputFile.close
			end
		end

	end
	
	# Regenerates all the Android necessary files
	public
	def regenProject(kProjectProperties, strCleanCommand)
		
		if kProjectProperties.m_bIsMainProject
			strProjectsFolderPath = kProjectProperties.m_strProjectPath + kProjectProperties.m_strProjectName + PROJECT_FILES_PATH
			strProjectOBJsFolderPath = kProjectProperties.m_strProjectPath + kProjectProperties.m_strProjectName + "/obj"
		else
			strProjectsFolderPath = BE3_SDK_DIR_SCRIPTS + kProjectProperties.m_strProjectPath + kProjectProperties.m_strProjectName + PROJECT_FILES_PATH
			strProjectOBJsFolderPath = BE3_SDK_DIR_SCRIPTS + kProjectProperties.m_strProjectPath + kProjectProperties.m_strProjectName + "/obj"
		end
		
		if strCleanCommand == "clean"
		
			if Dir.exists? strProjectsFolderPath
				puts "Cleaning #{kProjectProperties.m_strProjectName} \"projects\" directory ..."
				
				require 'fileutils'
				FileUtils.rm_rf strProjectsFolderPath
			end
			
			if Dir.exists? strProjectOBJsFolderPath
				puts "Cleaning #{kProjectProperties.m_strProjectName} \"obj\" directory ..."
				
				require 'fileutils'
				FileUtils.rm_rf strProjectOBJsFolderPath
			end
			
			if Dir.exists? "obj"
				puts "Cleaning \"obj\" directory ..."
				
				require 'fileutils'
				FileUtils.rm_rf 'obj'
			end
			
			if Dir.exists? "bin"
				puts "Cleaning \"bin\" directory ..."
				
				require 'fileutils'
				FileUtils.rm_rf 'bin'
			end
			
			if kProjectProperties.m_bIsExternalLibrary
				if Dir.exists? "lib"
					puts "Cleaning \"lib\" directory ..."
					
					require 'fileutils'
					FileUtils.rm_rf 'lib'
				end
			end
			
			if kProjectProperties.m_bIsMainProject or kProjectProperties.m_bIsExternalLibrary
				generateBatchFiles kProjectProperties, strCleanCommand
			end
			
			return
			
		end
		
		if not Dir.exists? strProjectsFolderPath
			Dir.mkdir strProjectsFolderPath
		end
		
		if not kProjectProperties.m_bIsMainProject 
			generateAndroidManifestXMLFile kProjectProperties
		end
		generateAndroidMakeFile kProjectProperties
		generateBuildXMLFile kProjectProperties
		generateProjectPropertiesFile kProjectProperties

		if kProjectProperties.m_bIsMainProject or kProjectProperties.m_bIsExternalLibrary
			generateBatchFiles kProjectProperties, strCleanCommand
		end

		if ANDROID_IN_MEMORY == false
			generateAndroidJavaMakeFile kProjectProperties, true, true, false, ""
			generateAndroidJavaMakeFile kProjectProperties, false, false, false, ""
		end

	end

end

# We modify files Application.mk and BeCoreLib/BeConstants.java for changing the debug flag
public
def ModifyDebugFlagsIntoFiles(strDebugMode, strOutputFilename)
	
	# Java side needs to set a debug flag _DEBUG into SystemLib/Be3Debug.java
	kBe3LibJavaFilePath = BE3_SDK_DIR_SCRIPTS + "BeLibs/BeCoreLib/Android/org/bakengine/BeConstants.java"
	if File.exists? kBe3LibJavaFilePath

		bModifyFile = false
	
		kBe3LibJavaFile = File.open(kBe3LibJavaFilePath, "r")
		self.m_strDataToWrite = kBe3LibJavaFile.read
		kBe3LibJavaFile.close
	
		if ((strDebugMode == "release" or strDebugMode == "production") and not $g_bForceDebugInReleaseBuild)
			if not self.m_strDataToWrite.include? "_DEBUG = false;"
				bModifyFile = true
				self.m_strDataToWrite = self.m_strDataToWrite.gsub("_DEBUG = true;", "_DEBUG = false;")
			end
		else
			if not self.m_strDataToWrite.include? "_DEBUG = true;"
				bModifyFile = true
				self.m_strDataToWrite = self.m_strDataToWrite.gsub("_DEBUG = false;", "_DEBUG = true;")
			end
		end
		
		if $g_selectedMarketAPK == MARKET_GOOGLE_PLAY
			strMarketFinal = "MarketProvider MARKET_PROVIDER = MarketProvider.MARKET_GOOGLE_PLAY"
		elsif $g_selectedMarketAPK == MARKET_AMAZON
			strMarketFinal = "MarketProvider MARKET_PROVIDER = MarketProvider.MARKET_AMAZON"
		end
				
		if not self.m_strDataToWrite.include? strMarketFinal
			bModifyFile = true
			
			if self.m_strDataToWrite.include? "MarketProvider MARKET_PROVIDER = MarketProvider.MARKET_GOOGLE_PLAY"
				self.m_strDataToWrite = self.m_strDataToWrite.gsub("MarketProvider MARKET_PROVIDER = MarketProvider.MARKET_GOOGLE_PLAY", strMarketFinal)
			elsif self.m_strDataToWrite.include? "MarketProvider MARKET_PROVIDER = MarketProvider.MARKET_AMAZON"
				self.m_strDataToWrite = self.m_strDataToWrite.gsub("MarketProvider MARKET_PROVIDER = MarketProvider.MARKET_AMAZON", strMarketFinal)
			end
		end
		
		if bModifyFile
			self.m_outputFile = File.open(kBe3LibJavaFilePath, "w")
			writeDataToFile
			self.m_outputFile.close
		end

	end
	
	# Application.mk needs to set the a APP_OPTIM flag before compiling native code
	kApplicationMakeFilePath = "." + PROJECT_FILES_PATH + "/Application.mk"
	if File.exists? kApplicationMakeFilePath
	
		bModifyFile = false
	
		kApplicationMakeFile = File.open(kApplicationMakeFilePath, "r")
		self.m_strDataToWrite = kApplicationMakeFile.read
		kApplicationMakeFile.close
	
		if (strDebugMode == "release" or strDebugMode == "production")
			if not self.m_strDataToWrite.include? "APP_OPTIM := release"
				bModifyFile = true
				self.m_strDataToWrite = self.m_strDataToWrite.gsub("APP_OPTIM := debug", "APP_OPTIM := release")
			end
		else
			if not self.m_strDataToWrite.include? "APP_OPTIM := debug"
				bModifyFile = true
				self.m_strDataToWrite = self.m_strDataToWrite.gsub("APP_OPTIM := release", "APP_OPTIM := debug")
			end
		end
		
		if bModifyFile
			self.m_outputFile = File.open(kApplicationMakeFilePath, "w")
			writeDataToFile
			self.m_outputFile.close
		end
		
	end
			
	# AndroidManifest.xml needs to set the library name
	kAndroidManifestMakeFilePath = "." + PROJECT_FILES_PATH + "/AndroidManifest.xml"
	if File.exists? kAndroidManifestMakeFilePath

		bModifyFile = false
	
		kAndroidManifestMakeFile = File.open(kAndroidManifestMakeFilePath, "r")
		self.m_strDataToWrite = kAndroidManifestMakeFile.read
		kAndroidManifestMakeFile.close

		if (strDebugMode == "release" or strDebugMode == "production")
			#strToFind = "android:value=\"#{strOutputFilename}\""
			#strToFindDebug = "android:value=\"#{strOutputFilename}_debug\""
			
			#if not self.m_strDataToWrite.include? strToFind
			#	bModifyFile = true
			#	self.m_strDataToWrite = self.m_strDataToWrite.gsub(strToFindDebug, strToFind)
			#end
			
			strToFind = "android:debuggable=\"false\""
			strToFindDebug = "android:debuggable=\"true\""
			
			if not self.m_strDataToWrite.include? strToFind
				bModifyFile = true
				self.m_strDataToWrite = self.m_strDataToWrite.gsub(strToFindDebug, strToFind)
			end
		else
			#strToFind = "android:value=\"#{strOutputFilename}\""
			#strToFindDebug = "android:value=\"#{strOutputFilename}_debug\""
			
			#if not self.m_strDataToWrite.include? strToFindDebug
			#	bModifyFile = true
			#	self.m_strDataToWrite = self.m_strDataToWrite.gsub(strToFind, strToFindDebug)
			#end
			
			strToFind = "android:debuggable=\"true\""
			strToFindDebug = "android:debuggable=\"false\""
			
			if not self.m_strDataToWrite.include? strToFind
				bModifyFile = true
				self.m_strDataToWrite = self.m_strDataToWrite.gsub(strToFindDebug, strToFind)
			end
		end
		
		if bModifyFile
			self.m_outputFile = File.open(kAndroidManifestMakeFilePath, "w")
			writeDataToFile
			self.m_outputFile.close
		end
		
	end
	
end

private
def GetFilesFromDir(strDirPath, strCurrentReturn, strFirstDir)

	Dir.foreach(strDirPath) do |strItemTmp|
		next if strItemTmp == '.' or strItemTmp == '..' or strItemTmp == '.svn'
		
		strFinalFolder = strDirPath + "/" + strItemTmp
		
		if File.directory?(strFinalFolder)
			strCurrentReturn = GetFilesFromDir strFinalFolder, strCurrentReturn, strFirstDir
		else
			strFinalFolder = strFinalFolder.gsub("#{strFirstDir}/", "")
            if not OS::mac?
                strFinalFolder = strFinalFolder.gsub("/", "\\")
            end
			strCurrentReturn = strCurrentReturn + strFinalFolder + "\n"
		end
	end

	return strCurrentReturn
	
end

# Executes all the Android compilation process
def ExecuteAndroidCompilation(strDebugMode, strCleanCommand, strMarketAPK) 
	
	if strMarketAPK != nil
		if strMarketAPK.downcase == "amazon"
			$g_selectedMarketAPK = MARKET_AMAZON
		end
	end
    
    strMarketAPKName = ""
    if $g_selectedMarketAPK == MARKET_AMAZON
        strMarketAPKName = "_amazon"
    end
	
	if $g_selectedMarketAPK == MARKET_GOOGLE_PLAY
		puts "BUILDING APK FOR MARKET_GOOGLE_PLAY"
	elsif $g_selectedMarketAPK == MARKET_AMAZON
		puts "BUILDING APK FOR MARKET_AMAZON"
	end
	puts ""
	
	if not OS::mac?	
		strTestSDKPath = ENV["BE3_SDK"] + "\\" + ENV["ANDROID_SDK"] + "\\tools"
		strTestNDKPath = ENV["BE3_SDK"] + "\\" +  ENV["ANDROID_NDK"] + "\\toolchains"
	else
		strTestSDKPath = ENV["BE3_SDK"] + "/" + ENV["ANDROID_SDK"] + "/tools"
		strTestNDKPath = ENV["BE3_SDK"] + "/" +  ENV["ANDROID_NDK"] + "/toolchains"
	end
	
	if not Dir.exists? strTestSDKPath
		puts "Android SDK folder not found. Unzip SDK zip file with \"extract here\" option inside Be3Toolkit/BeExternalTools/Android/<Platform> folder."
		return
	end
	if not Dir.exists? strTestNDKPath
		puts "Android NDK folder not found. Unzip NDK zip file with \"extract here\" option inside Be3Toolkit/BeExternalTools/Android/<Platform> folder."
		return
	end

	strObjsOutputFolder = strDebugMode
		
	if strDebugMode == "debug"
		iDebugMode = 1
	else
		iDebugMode = 0
	end
	
	kMainProjectProperties = $g_kProjectProperties[$g_kProjectProperties.length - 1]
	
	
	
	# We generate the exclude files for expansion zips
	bHasExpansionFiles = false
	kMainProjectProperties.m_kAndroidGPlayExpansionFiles.each do |strExpansion, kExpansionData|
		output = File.new("." + PROJECT_FILES_PATH + "/#{strExpansion}.excludes.d", "w")
		strFiles = GetFilesFromDir strExpansion, "", strExpansion
		output << strFiles
		output.close
		
		bHasExpansionFiles = true
	end
	
	
	
	$g_bToolsWrittenToFile = false
	pAndroidFilesBuilder = RegenAndroidFiles.new
	pAndroidFilesBuilder.SetWritingToMemory(true)
	pAndroidFilesBuilder.generateAndroidJavaMakeFile kMainProjectProperties, true, true, false, kMainProjectProperties.m_strOutputFileName
	pAndroidFilesBuilder.SetWritingToMemory(false)

	output = File.new("." + PROJECT_FILES_PATH + "/javaMakeStep1Temp.mk", "w")
	output << pAndroidFilesBuilder.m_strMemoryData
	output.close
	
	$g_bToolsWrittenToFile = false
	pAndroidFilesBuilder.SetWritingToMemory(true)
	pAndroidFilesBuilder.generateAndroidJavaMakeFile kMainProjectProperties, true, true, false, kMainProjectProperties.m_strOutputFileName
	pAndroidFilesBuilder.generateAndroidJavaMakeFile kMainProjectProperties, false, false, false, kMainProjectProperties.m_strOutputFileName
	pAndroidFilesBuilder.SetWritingToMemory(false)

	output = File.new("." + PROJECT_FILES_PATH + "/javaMakeStep2Temp.mk", "w")
	output << pAndroidFilesBuilder.m_strMemoryData
	output.close
	
	if bHasExpansionFiles
		$g_bToolsWrittenToFile = false
		pAndroidFilesBuilder = RegenAndroidFiles.new
		pAndroidFilesBuilder.SetWritingToMemory(true)
		pAndroidFilesBuilder.generateAndroidJavaMakeFile kMainProjectProperties, true, false, true, kMainProjectProperties.m_strOutputFileName
		pAndroidFilesBuilder.SetWritingToMemory(false)

		output = File.new("." + PROJECT_FILES_PATH + "/javaMakeStep3Temp.mk", "w")
		output << pAndroidFilesBuilder.m_strMemoryData
		output.close
	end
	
	pAndroidFilesBuilder.generateAndroidManifestXMLFile kMainProjectProperties
	
	
	strAPIOptim = strDebugMode
    if strAPIOptim == "production"
        strAPIOptim = "release"
    end
	pAndroidFilesBuilder.SetWritingToMemory(true)
	pAndroidFilesBuilder.generateApplicationMakeFile kMainProjectProperties, strAPIOptim
	pAndroidFilesBuilder.SetWritingToMemory(false)
	
	strAppMakeFile = "Application.mk"
	if not kMainProjectProperties.m_bIsExternalLibrary
		strAppMakeFilePath = kMainProjectProperties.m_strProjectPath + "/" + kMainProjectProperties.m_strProjectName + PROJECT_FILES_PATH + "/" + strAppMakeFile
	else
		strAppMakeFilePath = "." + PROJECT_FILES_PATH + "/" + strAppMakeFile
	end
	
	bNeedToOverrideAppMake = false
	if not File.exists? strAppMakeFilePath
		bNeedToOverrideAppMake = true
	else
		appFile = File.open(strAppMakeFilePath, "r")
		strAppFileData = appFile.read
		appFile.close
		
		strCompare1 = strAppFileData
		strCompare2 = pAndroidFilesBuilder.m_strMemoryData
		
		strCompare1 = strCompare1.gsub(/\W+/, '')
		strCompare2 = strCompare2.gsub(/\W+/, '')
		
		if not strCompare1.eql? strCompare2
			bNeedToOverrideAppMake = true
		end
	end
	
	if bNeedToOverrideAppMake
		output = File.new(strAppMakeFilePath, "w")
		output << pAndroidFilesBuilder.m_strMemoryData
		output.close
	end
	
	

	strMainProjectName = kMainProjectProperties.m_strProjectName
	pAndroidFilesBuilder.ModifyDebugFlagsIntoFiles(strObjsOutputFolder, kMainProjectProperties.m_strOutputFileName)
		
	strNDKAppOut = "./obj/Android/#{strObjsOutputFolder}"
	
	strMultithreadCompilation = "-j4"
	
	strNDKCommand = "#{BE3_SDK_DIR_SCRIPTS}#{ENV['ANDROID_NDK']}\\ndk-build #{strMultithreadCompilation} NDK_DEBUG=#{iDebugMode} NDK_APPLICATION_MK=.#{PROJECT_FILES_PATH}/Application.mk APP_BUILD_SCRIPT=.#{PROJECT_FILES_PATH}/Android.mk NDK_APP_OUT=#{strNDKAppOut} #{strCleanCommand}"
	if OS.mac?
		strJavaCleanCommand = "make #{strMultithreadCompilation} --file=.#{PROJECT_FILES_PATH}/javaMakeStep2Temp.mk --directory=./ clean --quiet"
		strJavaCommand1 = "make -j1 --file=.#{PROJECT_FILES_PATH}/javaMakeStep1Temp.mk --directory=./ --quiet resourcesStep"
		strJavaCommand2 = "make #{strMultithreadCompilation} --file=.#{PROJECT_FILES_PATH}/javaMakeStep2Temp.mk --directory=./ --quiet deploy-#{strDebugMode}"
		if bHasExpansionFiles
			strJavaCommand3 = "make #{strMultithreadCompilation} --file=.#{PROJECT_FILES_PATH}/javaMakeStep3Temp.mk --directory=./ --quiet generateExpansionFiles"
		end
	else
		strJavaCleanCommand = "#{BE3_SDK_DIR_SCRIPTS}BeBuildTools/Common/make.exe #{strMultithreadCompilation} --file=.#{PROJECT_FILES_PATH}/javaMakeStep2Temp.mk --directory=./ clean --quiet (cwd=./)"
		strJavaCommand1 = "#{BE3_SDK_DIR_SCRIPTS}BeBuildTools/Common/make.exe -j1 --file=.#{PROJECT_FILES_PATH}/javaMakeStep1Temp.mk --directory=./ --quiet resourcesStep (cwd=./)"
		strJavaCommand2 = "#{BE3_SDK_DIR_SCRIPTS}BeBuildTools/Common/make.exe #{strMultithreadCompilation} --file=.#{PROJECT_FILES_PATH}/javaMakeStep2Temp.mk --directory=./ --quiet deploy-#{strDebugMode} (cwd=./)"
		if bHasExpansionFiles
			strJavaCommand3 = "#{BE3_SDK_DIR_SCRIPTS}BeBuildTools/Common/make.exe #{strMultithreadCompilation} --file=.#{PROJECT_FILES_PATH}/javaMakeStep3Temp.mk --directory=./ --quiet generateExpansionFiles (cwd=./)"
		end
	end
	
	strNDKCommand = strNDKCommand.gsub("\\", "/")
	strJavaCleanCommand = strJavaCleanCommand.gsub("\\", "/")
	strJavaCommand1 = strJavaCommand1.gsub("\\", "/")
	strJavaCommand2 = strJavaCommand2.gsub("\\", "/")
	if bHasExpansionFiles
		strJavaCommand3 = strJavaCommand3.gsub("\\", "/")
	end
	
	puts "Executing NDK Build..."
	puts "#{strNDKCommand}"
	system("#{strNDKCommand}")
	bCommandResult = $?.success?

	if not kMainProjectProperties.m_bIsExternalLibrary
		if bCommandResult == true
			puts "Executing Java Clean..."
			puts "#{strJavaCleanCommand}"
			system("#{strJavaCleanCommand}")
			bCommandResult = $?.success?
		
			if bCommandResult == true
				puts "Executing Java Build 1..."
				puts "#{strJavaCommand1}"
				system("#{strJavaCommand1}")
				bCommandResult = $?.success?
			end
			
			# We copy the R file in every LIB gen folder in order to compile
			strRFileName = "R.java"
			strFinalPackageName = ""
			strFinalPackageNameRFilePath = ""
			if kMainProjectProperties.m_strForcedBundlePackage != nil
				strFinalPackageName = kMainProjectProperties.m_strForcedBundlePackage
				strFinalPackageNameTemp = strFinalPackageName.gsub(".", "/")
				strFinalPackageNameRFilePath = "#{strFinalPackageNameTemp}/"
			else
				strFinalPackageName = "com.#{kMainProjectProperties.m_strCompanyName.downcase}.#{kMainProjectProperties.m_strOutputFileName.downcase}"
				strFinalPackageNameRFilePath = "com/#{kMainProjectProperties.m_strCompanyName.downcase}/#{kMainProjectProperties.m_strOutputFileName.downcase}/"
			end
			strRFilePath = "./bin/Android/#{kMainProjectProperties.m_strOutputFileName}/gen/#{strFinalPackageNameRFilePath}" + strRFileName
			if File.exists? strRFilePath
				kRFile = File.open(strRFilePath, "r")
				strRFileData = kRFile.read
				kRFile.close
				
				strToReplace = "package #{strFinalPackageName};"
				strReplaceFinal = "package org.#{kMainProjectProperties.m_strCompanyName.downcase};"
				strRFileData = strRFileData.gsub(strToReplace, strReplaceFinal)
				
				require 'fileutils'
				for i in 0..($g_kProjectProperties.length-1)
					strPathToCopyDestDir = "./bin/Android/#{$g_kProjectProperties[i].m_strProjectName}/gen/org/#{kMainProjectProperties.m_strCompanyName.downcase}"
					strPathToCopyDestDirFile = "#{strPathToCopyDestDir}/#{strRFileName}"
					FileUtils.mkdir_p(strPathToCopyDestDir)
					
					output = File.open(strPathToCopyDestDirFile, "w")
					output << strRFileData
					output.close
				end			
			end
			
			# We copy the exclude files for expansion zips
			kMainProjectProperties.m_kAndroidGPlayExpansionFiles.each do |strExpansion, kExpansionData|
				strCopyOrig = "." + PROJECT_FILES_PATH + "/#{strExpansion}.excludes.d"
				strCopyDest = "./bin/Android/#{kMainProjectProperties.m_strOutputFileName}/#{strExpansion}.excludes.d"
				
				require 'fileutils'
				FileUtils.copy_file strCopyOrig, strCopyDest
			end
			
			if bHasExpansionFiles
				if bCommandResult == true
					puts "Executing Java Build for Expansion files..."
					puts "#{strJavaCommand3}"
					system("#{strJavaCommand3}")
					bCommandResult = $?.success?
				end
				
				# After generating expansion files we need to generate the expansionFiles.dat into the assets folder
				if bCommandResult == true
					require 'fileutils'
					
					strAssetsFolder = "./bin/Android/#{kMainProjectProperties.m_strOutputFileName}"
					
					kExpansionDatFile = File.open(strAssetsFolder + "/expansionFiles.dat", "wb")
					kExpansionDatFile.write [kMainProjectProperties.m_kAndroidGPlayExpansionFiles.length].pack("I")
					
					kMainProjectProperties.m_kAndroidGPlayExpansionFiles.each do |strExpansion, kExpansionData|
						strFinalPackageName = ""
						if kMainProjectProperties.m_strForcedBundlePackage != nil
							strFinalPackageName = kMainProjectProperties.m_strForcedBundlePackage
						else
							strFinalPackageName = "com.#{kMainProjectProperties.m_strCompanyName.downcase}.#{kMainProjectProperties.m_strOutputFileName.downcase}"
						end
						
						if strExpansion.downcase.include? "main"
							strZipFilename = "main."
						else
							strZipFilename = "patch."
						end
						strZipFilename = strZipFilename + kExpansionData[1] + "." + strFinalPackageName + ".obb"
						
						strExpansionFile = "./bin/Android/#{kMainProjectProperties.m_strOutputFileName}/#{strZipFilename}"
						
						iExpansionVersion = kExpansionData[1].to_i
						kExpansionDatFile.write [iExpansionVersion].pack("I")
						
						iExpansionFileSize = File.size strExpansionFile
						kExpansionDatFile.write [iExpansionFileSize].pack("I")
						
						require 'zip/zip'
						Zip::ZipFile.open(strExpansionFile) { |zip_file|
							iTotalFiles = 0
							zip_file.each { |f|
								if f.ftype == :file
									iTotalFiles = iTotalFiles + 1
								end
							}
							kExpansionDatFile.write [iTotalFiles].pack("I")
							
							zip_file.each { |f|
								if f.ftype == :file
									kExpansionDatFile.write [f.name.length].pack("I")
									kExpansionDatFile.write f.name
									kExpansionDatFile.write [f.crc].pack("I")
								end
							}
						}
					end
					
					kExpansionDatFile.close
				end
			end
			
			if bCommandResult == true
				puts "Executing Java Build 2..."
				puts "#{strJavaCommand2}"
				system("#{strJavaCommand2}")
				bCommandResult = $?.success?
			end
		end
		
		# We process the final APK to our builds repository
		if bCommandResult == true
			if strDebugMode == "release" and BE3_BUILDS_DIR != nil
				strAPKFileName = "#{kMainProjectProperties.m_strOutputFileName.downcase}-release#{strMarketAPKName}.apk"
				
				puts "\nCopying final APK to BUILDS folder..."
				
				strPathToCopyDestPath = BE3_BUILDS_DIR + "Android/#{kMainProjectProperties.m_strOutputFileName}"
				if not Dir.exists? strPathToCopyDestPath
					Dir.mkdir strPathToCopyDestPath
				end
						
				strPathToCopyOrig = "./bin/Android/#{kMainProjectProperties.m_strOutputFileName}/#{strAPKFileName}"
				strPathToCopyDest = strPathToCopyDestPath + "/#{strAPKFileName}"
				
				require 'fileutils'
				FileUtils.copy_file strPathToCopyOrig, strPathToCopyDest
				
				strExtraExcludeFiles = ""
				kMainProjectProperties.m_kAndroidGPlayExpansionFiles.each do |strExpansion, kExpansionData|
					strFinalPackageName = ""
					if kMainProjectProperties.m_strForcedBundlePackage != nil
						strFinalPackageName = kMainProjectProperties.m_strForcedBundlePackage
					else
						strFinalPackageName = "com.#{kMainProjectProperties.m_strCompanyName.downcase}.#{kMainProjectProperties.m_strOutputFileName.downcase}"
					end
					
					if strExpansion.downcase.include? "main"
						strZipFilename = "main."
					else
						strZipFilename = "patch."
					end
					strZipFilename = strZipFilename + kExpansionData[1] + "." + strFinalPackageName + ".obb"
					
					strPathToCopyOrig = "./bin/Android/#{kMainProjectProperties.m_strOutputFileName}/#{strZipFilename}"
					strPathToCopyDest = strPathToCopyDestPath + "/#{strZipFilename}"
					
					FileUtils.copy_file strPathToCopyOrig, strPathToCopyDest
				end
			end
		end
	else
		if bCommandResult == true
			if not Dir.exists? "./lib"
				Dir.mkdir "./lib"
			end
			if not Dir.exists? "./lib/Android"
				Dir.mkdir "./lib/Android"
			end
			
			kABIs = kMainProjectProperties.m_strAndroidABI.split
			
			kABIs.each do |strABI|
				strPathToCopyOrig = strNDKAppOut + "/local/#{strABI}/lib#{strMainProjectName}.a"
				
				strFinalABI = strABI
				if strFinalABI.include? "arm"
					strFinalABI = "arm"
				end
				strPathToCopyDest = "./lib/Android/#{strFinalABI}"
				
				if not Dir.exists? strPathToCopyDest
					Dir.mkdir strPathToCopyDest
				end
				
				require 'fileutils'
				FileUtils.copy_file strPathToCopyOrig, "#{strPathToCopyDest}/lib#{strMainProjectName}.a"
			end
		end
	end
		
end
