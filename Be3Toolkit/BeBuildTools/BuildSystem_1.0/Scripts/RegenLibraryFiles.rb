####################################################################
##
## BAK ENGINE 3
## BUILD SYSTEM
## 
## RegenLibraryFiles.rb
## This ruby file generates library common files
##
## Javier Calet Toledo - 2013
##
###################################################################

#!/bin/sh

# RegenLibraryFiles Class
class RegenLibraryFiles

	attr_accessor :m_outputFile
	attr_accessor :m_strDataToWrite
	attr_accessor :m_strFileHeader
	attr_accessor :m_strMemoryData
	attr_accessor :m_bIsFileCreationIntoMemory
	attr_accessor :m_strFileMemoryBuffer

	def initialize()
		self.m_outputFile = nil
		self.m_strDataToWrite = ""
		self.m_strFileHeader ="\//
\// BakEngine3 Build System auto-generated file
\//

"
		self.m_strMemoryData = ""
		self.m_bIsFileCreationIntoMemory = true
		self.m_strFileMemoryBuffer = ""
    end
	
	# Writes m_strDataToWrite content to m_outputFile and clean it
	private
	def writeDataToFile
		if m_bIsFileCreationIntoMemory
			self.m_strFileMemoryBuffer = self.m_strFileMemoryBuffer + self.m_strDataToWrite + "\n"
		else
			self.m_outputFile.puts self.m_strDataToWrite
		end
		
		self.m_strDataToWrite = ""
	end
	
	# Checks for file differences and override it if they exist
	private
	def checkFileDifferencesAndSave(strPathFile, bForceGeneration)
		
		bNeedToSaveToFile = false
		
		if File.exists? strPathFile
			kPreviousFile = File.open(strPathFile, "r")
			strPreviousFileContent = kPreviousFile.read
			kPreviousFile.close
			
			if self.m_strFileMemoryBuffer.size != strPreviousFileContent.size
				bNeedToSaveToFile = true
			else
				bIsEqual = true
				
				for i in 0..(self.m_strFileMemoryBuffer.size)
					if self.m_strFileMemoryBuffer[i] != strPreviousFileContent[i]
						bIsEqual = false
					end
				end
				
				if not bIsEqual
					bNeedToSaveToFile = true
				end
			end
		else
			bNeedToSaveToFile = true
		end
		
		if bNeedToSaveToFile or bForceGeneration
			self.m_outputFile = File.new(strPathFile, "w")
			self.m_outputFile.write self.m_strFileMemoryBuffer
			self.m_outputFile.close
			
			self.m_strFileMemoryBuffer = ""
		end
		
	end

	# Writes m_strDataToWrite content to m_outputFile and clean it
	private
	def writeDataToFile
		if m_bIsFileCreationIntoMemory
			self.m_strFileMemoryBuffer = self.m_strFileMemoryBuffer + self.m_strDataToWrite + "\n"
		else
			self.m_outputFile.puts self.m_strDataToWrite
		end
		
		self.m_strDataToWrite = ""
	end

	public

	# Searches and adds the files for a specific filter
	private
	def searchHeaderFiles(strBasePathToFiles, strIncrementalPathToFiles, strIncludeFolderName, kFiltersArray, strFirstFilterFolder)
	
		strSearchingPath = strBasePathToFiles + strIncrementalPathToFiles
             
		if Dir.exists?(strSearchingPath)
			Dir.foreach(strSearchingPath) do |strItem|
				next if strItem == '.' or strItem == '..' or strItem == strIncludeFolderName
				
				strItemTmp = "/%1"
				strItemTmp = strItemTmp.gsub("%1", strItem)
				strFinalFolder = strSearchingPath + strItemTmp
				if File.directory?(strFinalFolder)
					searchHeaderFiles strBasePathToFiles, strIncrementalPathToFiles + strItemTmp, strIncludeFolderName, kFiltersArray, strFirstFilterFolder
				end
			end
		end

		strSearchingPath = strBasePathToFiles + strIncrementalPathToFiles	
		if Dir.exists?(strSearchingPath)
			Dir.foreach(strSearchingPath) do |strItem|
				next if strItem == '.' or strItem == '..' or strItem == strIncludeFolderName
				
				if kFiltersArray.length > 0
					bFilterFound = false
					kFiltersArray.each do |strFilter|
						if strItem.end_with?(strFilter) == true
							bFilterFound = true
							break
						end
					end
					next if bFilterFound == false
				end

				strFinalFolder = strSearchingPath + "/" + strItem
                
				if File.file?(strFinalFolder)
					strIncrementalPathToFilesTemp = strIncrementalPathToFiles + "/" + strItem		
					strIncrementalPathToFilesTemp = strIncrementalPathToFilesTemp.slice strFirstFilterFolder.length+1, strIncrementalPathToFilesTemp.length-strFirstFilterFolder.length-1
					self.m_strDataToWrite = "#include <#{strIncrementalPathToFilesTemp}>\n"
					
					writeDataToFile
				end
			end
		end

	end

	# Generates the Android make file
	private
	def generateLibraryCommonHeader(kProjectProperties, strCleanCommand)

		strHeaderFile = kProjectProperties.m_strProjectName + ".h"
		strHeaderFilePathBase = BE3_SDK_DIR_SCRIPTS + kProjectProperties.m_strProjectPath + kProjectProperties.m_strProjectName + "/pch"
		strHeaderFilePath = strHeaderFilePathBase + "/" + strHeaderFile
		
		if strCleanCommand == "clean"
		
			if File.exists? strHeaderFilePath
				puts "Cleaning #{strHeaderFile} ..."
				
				File.delete strHeaderFilePath
			end
			
		else

			if not Dir.exists?(strHeaderFilePathBase)
				Dir.mkdir strHeaderFilePathBase
			end
		
			puts "Building " + kProjectProperties.m_strProjectName + ".h ..."
			
			if m_bIsFileCreationIntoMemory
				self.m_strFileMemoryBuffer = ""
			else
				self.m_outputFile = File.new(strHeaderFilePath, "w")
			end
		
			self.m_strDataToWrite = self.m_strFileHeader
			writeDataToFile

			self.m_strDataToWrite = "#ifndef BE3_#{kProjectProperties.m_strProjectName.upcase}_H\n#define BE3_#{kProjectProperties.m_strProjectName.upcase}_H\n\n"
			writeDataToFile

			searchHeaderFiles BE3_SDK_DIR_SCRIPTS + kProjectProperties.m_strProjectPath + kProjectProperties.m_strProjectName + "/", "src", kProjectProperties.m_strIncludeFolderName, [".h", ".hpp"], "src"
			
			self.m_strDataToWrite = "\n"
			writeDataToFile
			
			# WINDOWS specific headers
			self.m_strDataToWrite = "#if TARGET_WINDOWS\n"
			writeDataToFile
			searchHeaderFiles BE3_SDK_DIR_SCRIPTS + kProjectProperties.m_strProjectPath + kProjectProperties.m_strProjectName + "/", "windows", kProjectProperties.m_strIncludeFolderName, [".h", ".hpp"], "windows"
			self.m_strDataToWrite = "#endif\n\n"
			writeDataToFile
			
			# MAC OS specific headers
			self.m_strDataToWrite = "#if TARGET_MAC\n"
			writeDataToFile
			searchHeaderFiles BE3_SDK_DIR_SCRIPTS + kProjectProperties.m_strProjectPath + kProjectProperties.m_strProjectName + "/", "macos", kProjectProperties.m_strIncludeFolderName, [".h", ".hpp"], "macos"
			self.m_strDataToWrite = "#endif\n\n"
			writeDataToFile
			
			# iOS specific headers
			self.m_strDataToWrite = "#if TARGET_IOS\n"
			writeDataToFile
			searchHeaderFiles BE3_SDK_DIR_SCRIPTS + kProjectProperties.m_strProjectPath + kProjectProperties.m_strProjectName + "/", "iOS", kProjectProperties.m_strIncludeFolderName, [".h", ".hpp"], "iOS"
			self.m_strDataToWrite = "#endif\n\n"
			writeDataToFile
			
			# Android specific headers
			self.m_strDataToWrite = "#if TARGET_ANDROID\n"
			writeDataToFile
			searchHeaderFiles BE3_SDK_DIR_SCRIPTS + kProjectProperties.m_strProjectPath + kProjectProperties.m_strProjectName + "/", "Android", kProjectProperties.m_strIncludeFolderName, [".h", ".hpp"], "Android"
			self.m_strDataToWrite = "#endif\n\n"
			writeDataToFile
		
			self.m_strDataToWrite = "#endif // #ifndef BE3_#{kProjectProperties.m_strProjectName.upcase}_H\n"
			writeDataToFile
			
			if m_bIsFileCreationIntoMemory
				checkFileDifferencesAndSave strHeaderFilePath, false
			else
				self.m_outputFile.close
			end

		end
			
	end

	# Regenerates all the Android necessary files
	public
	def regenProject(kProjectProperties, strCleanCommand)

		if kProjectProperties.m_bIsMainProject == false
			generateLibraryCommonHeader kProjectProperties, strCleanCommand
		end

	end

end
