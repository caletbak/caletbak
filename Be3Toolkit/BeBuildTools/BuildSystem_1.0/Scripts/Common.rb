####################################################################
##
## BAK ENGINE 3
## BUILD SYSTEM
## 
## Common.rb
## This ruby defines common classes and variables
##
## Javier Calet Toledo - 2013
##
###################################################################

#!/bin/sh

# If TRUE everything will be make in memory
ANDROID_IN_MEMORY	= true

# If TRUE an external lib is going to be generated
$g_bGeneratingExternalLib = false

# REGENERATE PLATFORMS
PLATFORM_WINDOWS			= 0x0001
PLATFORM_MAC				= 0x0002
PLATFORM_IOS				= 0x0004
PLATFORM_ANDROID			= 0x0008
PLATFORM_ANDROID_ECLIPSE 	= 0x0010

# ANDROID MARKETS
MARKET_GOOGLE_PLAY			= 0x0001
MARKET_AMAZON				= 0x0002

# Main project folder
MAIN_PROJECT_PATH_IOS = Dir.pwd

# Platform names
$g_kPlatformNames = Hash.new { |hash, key| }
$g_kPlatformNames[PLATFORM_WINDOWS] = "[WINDOWS]"
$g_kPlatformNames[PLATFORM_MAC] = "[MAC OS]"
$g_kPlatformNames[PLATFORM_IOS] = "[iOS]"
$g_kPlatformNames[PLATFORM_ANDROID] = "[ANDROID]"
$g_kPlatformNames[PLATFORM_ANDROID_ECLIPSE] = "[ECLIPSE]"

# If TRUE forces debug messages even in Release
$g_bForceDebugInReleaseBuild = false

# Android related
$g_strAndroidMinTarget = "android-9"
$g_strAndroidTarget = "android-21"
$g_kAndroidManifestPermissions = Hash.new { |hash, key| }
$g_kAndroidCompatibleScreens = Hash.new { |hash, key| }

# ProjectPaths - Stores the project paths
$g_kProjectPaths = Hash.new { |hash, key| }
$g_kProjectPathsFromMainProject = Hash.new { |hash, key| }

# Predefined global symbols - They will be used in libs and main project
$g_kGlobalSymbols = []

# Project properties Class
class ProjectProperties
	
	# CONSTANTS
	LIBRARY_TYPE_STATIC = 0x01
	LIBRARY_TYPE_SHARED = 0x02

	# Common project properties
	
		# Project code version
		attr_accessor :m_strVersionCode
		
		# Project version name
		attr_accessor :m_strVersionName
	
	# Common platform properties

		# Project name (must be the same as project folder)
		attr_accessor :m_strProjectName

		# Project base folder path
		attr_accessor :m_strProjectPath

		# Output library filename
		attr_accessor :m_strOutputFileName
		
		# Forced bundle package
		attr_accessor :m_strForcedBundlePackage

		# Is main project?
		attr_accessor :m_bIsMainProject

		# The final Company name
		attr_accessor :m_strCompanyName
		
		# The final Game name
		attr_accessor :m_strGameName

		# Project dependencies stored statically
		@@s_kProjectDependencies = Hash.new{|h, k| h[k] = []}
		def self.s_kProjectDependencies
			@@s_kProjectDependencies
		end
		
		# Project 3rdParty dependencies stored statically
		@@s_kProject3rdPartyDependencies = Hash.new{|h, k| h[k] = []}
		def self.s_kProject3rdPartyDependencies
			@@s_kProject3rdPartyDependencies
		end
		
		# Project external dependencies stored statically
		@@s_kProjectExternalDependencies = Hash.new{|h, k| h[k] = []}
		def self.s_kProjectExternalDependencies
			@@s_kProjectExternalDependencies
		end
		
		# Project external dependencies stored statically to be whole-linked
		@@s_kProjectExternalDependenciesLinkWhole = Hash.new{|h, k| h[k] = []}
		def self.s_kProjectExternalDependenciesLinkWhole
			@@s_kProjectExternalDependenciesLinkWhole
		end
		
		# Project 3rdParty IOS framework dependencies stored statically
		@@s_kProjectIOSFrameworkDependencies = Hash.new{|h, k| h[k] = []}
		def self.s_kProjectIOSFrameworkDependencies
			@@s_kProjectIOSFrameworkDependencies
		end
		
		# Project 3rdParty IOS framework resources stored statically
		@@s_kProjectIOSFrameworkResources = Hash.new{|h, k| h[k] = []}
		def self.s_kProjectIOSFrameworkResources
			@@s_kProjectIOSFrameworkResources
		end
		
        # Library Exports Define
        attr_accessor :m_strProjectExports
				
		# Default Include folder name
		attr_accessor :m_strIncludeFolderName

		# Is a Library?
		attr_accessor :m_bIsLibrary
		
		# Is a external Library?
		attr_accessor :m_bIsExternalLibrary
		
		# Is the final Library?
		attr_accessor :m_bIsFinalLibrary
				
		# Is a 3rdParty LIB?
		attr_accessor :m_bIs3rdPartyLib

		# Library type
		attr_accessor :m_LibraryType
		
		# Force to link the entire lib objects
		attr_accessor :m_LibraryLinkWhole
		
		# Define platform targets for each project
		attr_accessor :m_kProjectPlatformTargets

		# Extra include folders to be added for compiling
		attr_accessor :m_strExtraIncludeDirs
		
		# Avoid to include Source files in the project
		attr_accessor :m_avoidLibrariesSourceFiles
		
		# Only compiles these code files, ignoring all the rest
		attr_accessor :m_kOnlyCompileCodeFiles
		
		# Avoid to compile code files that are included in other code file compilings
		attr_accessor :m_kAvoidCompileInclusiveCodeFiles

	# VS properties
		
		# VS resources folder (if any)
		attr_accessor :m_strVSResourcesFolder
		
		# VS Avoid libraries for linking
		@@s_kVSAvoidLibraries = Array.new
		def self.s_kVSAvoidLibraries
			@@s_kVSAvoidLibraries
		end
		
		# VS disable warnings
		attr_accessor :m_strVSDisableWarnings
		
		# VS disable link warnings
		attr_accessor :m_kVSDisableLinkWarnings
		
		# VS Avoid to compile files in this platform
		attr_accessor :m_kVSAvoidCompile
				
	# iOS properties
		
		# Array of specific 3rd party libs
		attr_accessor :m_kProjectSpecialLibraries
		
		# Applovin App ID
		attr_accessor :m_strIOSAppLovinAppID
		
		# Facebook App ID
		attr_accessor :m_strIOSFaceBookAppID
		
		# Facebook App Name
		attr_accessor :m_strIOSFaceBookAppName
		
	# Android properties

		# Android target
		attr_accessor :m_strAndroidTarget
		
		# Android min target
		attr_accessor :m_strAndroidMinTarget
		
		# Android only classpath libraries
		attr_accessor :m_kAndroidJAROnlyClasspath
		
		# Android ABI to be used
		attr_accessor :m_strAndroidABI
		
		# Android only classpath libraries
		attr_accessor :m_kAndroidGPlayExpansionFiles

		# Android STL to be used
		attr_accessor :m_strAndroidSTL

		# Android Activity Name to be used
		attr_accessor :m_strAndroidActivity
		
		# Android Icon Name
		attr_accessor :m_strAndroidIconName

		# Debug KeyStore filePath
		attr_accessor :m_strAndroidDebugKeyStoreFilePath
		
		# Debug KeyStore StorePass
		attr_accessor :m_strAndroidDebugKeyStorePass

		# Debug KeyStore KeyAlias
		attr_accessor :m_strAndroidDebugKeyAlias

		# Release KeyStore filePath
		attr_accessor :m_strAndroidReleaseKeyStoreFilePath
		
		# Release KeyStore StorePass
		attr_accessor :m_strAndroidReleaseKeyStorePass

		# Release KeyStore KeyAlias
		attr_accessor :m_strAndroidReleaseKeyAlias
		
	# Android manifest properties
		
		attr_accessor :m_strAndroidScreenOrientation

	# Initialise the object with default values
	def initialize()
	
		# Common platform properties
		self.m_strProjectName = nil
		self.m_strOutputFileName = nil
		self.m_strForcedBundlePackage = nil
		self.m_strProjectPath = nil
		self.m_bIsMainProject = false
		self.m_bIs3rdPartyLib = false
		self.m_strCompanyName = nil
		self.m_strGameName = nil
        self.m_strProjectExports = nil
		self.m_strIncludeFolderName = "Include"
		self.m_bIsLibrary = true
		self.m_bIsFinalLibrary = false
		self.m_LibraryType = LIBRARY_TYPE_STATIC
		self.m_LibraryLinkWhole = false
		self.m_kProjectPlatformTargets = nil
		self.m_strExtraIncludeDirs = nil
		self.m_avoidLibrariesSourceFiles = false
		self.m_kOnlyCompileCodeFiles = []
		self.m_kAvoidCompileInclusiveCodeFiles = []
		
		# iOS properties
		self.m_kProjectSpecialLibraries = []
		self.m_strIOSAppLovinAppID = ""
		self.m_strIOSFaceBookAppID = ""
		self.m_strIOSFaceBookAppName = ""
		
		# VS properties
		self.m_strVSResourcesFolder = nil
		self.m_strVSDisableWarnings = ""
		self.m_kVSDisableLinkWarnings = []
		self.m_kVSAvoidCompile = []

		# Android properties
		self.m_strAndroidTarget = "android-19"
		self.m_strAndroidABI = "armeabi-v7a"
		self.m_strAndroidSTL = "gnustl_static"
		self.m_strAndroidActivity = nil
		self.m_strAndroidDebugKeyStoreFilePath = nil
		self.m_strAndroidDebugKeyStorePass = nil
		self.m_strAndroidDebugKeyAlias = nil
		self.m_strAndroidReleaseKeyStoreFilePath = nil
		self.m_strAndroidReleaseKeyStorePass = nil
		self.m_strAndroidReleaseKeyAlias = nil
		
		# Android manifest properties
		self.m_strAndroidScreenOrientation = nil
		self.m_kAndroidJAROnlyClasspath = []
		self.m_kAndroidGPlayExpansionFiles = []

    end

	#private
	def addRecursiveDependencies(kOrinHash, strDependencyName, kTempArray)
		kOrinHash[strDependencyName].each do |strRecursiveDependencyName|
			addRecursiveDependencies kOrinHash, strRecursiveDependencyName, kTempArray
		end
		
		if not kTempArray.include? strDependencyName
			kTempArray.push(strDependencyName)
		end
	end
	
	# Stores statically the project dependencies for the current project
	def setProjectDependencies(kDependenciesArray)
		if not self.m_bIsMainProject
			kTempProjectDependencies = []
			if kDependenciesArray.size > 0
				kDependenciesArray.each do |strDependencyName|
					addRecursiveDependencies @@s_kProjectDependencies, strDependencyName, kTempProjectDependencies
				end
			end

			@@s_kProjectDependencies[self.m_strProjectName] = kTempProjectDependencies
		else
			@@s_kProjectDependencies[self.m_strProjectName] = kDependenciesArray
		end
	end
	
	# Stores statically the project 3rdParty dependencies for the current project
	def setProject3rdPartyDependencies(kDependenciesArray)
		@@s_kProject3rdPartyDependencies[self.m_strProjectName] = kDependenciesArray
	end
	
	# Stores statically the project External dependencies for the current project
	def setProjectExternalDependencies(kDependenciesArray)
		if not self.m_bIsMainProject
			kTempProjectDependencies = []
		
			@@s_kProjectDependencies[self.m_strProjectName].each do |strRecursiveDependencyName|
				@@s_kProjectExternalDependencies[strRecursiveDependencyName].each do |strRecursiveExternalDependencyName|
					if not kTempProjectDependencies.include? strRecursiveExternalDependencyName
						kTempProjectDependencies.push(strRecursiveExternalDependencyName)
					end
				end
			end

			kDependenciesArray.each do |strExternalDependencyName|
				if not kTempProjectDependencies.include? strExternalDependencyName
					kTempProjectDependencies.push(strExternalDependencyName)
				end
			end
		
			@@s_kProjectExternalDependencies[self.m_strProjectName] = kTempProjectDependencies
		else
			@@s_kProjectExternalDependencies[self.m_strProjectName] = kDependenciesArray
		end
	end
	
	# Stores statically the project External dependencies for the current project to whole link
	def setProjectExternalDependenciesLinkWhole(kDependenciesArray)
		@@s_kProjectExternalDependenciesLinkWhole[self.m_strProjectName] = kDependenciesArray
	end
	
	# Stores statically the project 3rdParty frameworks in IOS
	def setIOSFrameworkDependencies(kDependenciesArray)
		@@s_kProjectIOSFrameworkDependencies[self.m_strProjectName] = kDependenciesArray
	end
	
	# Stores statically the project 3rdParty frameworks resources in IOS
	def setIOSFrameworkResources(kDependenciesArray)
		@@s_kProjectIOSFrameworkResources[self.m_strProjectName] = kDependenciesArray
	end
		
	# Stores statically the project libraries to be avoided when linking
	def setVSAvoidLibraries(kLibrariesArray)
		kLibrariesArray.each do |strLibName|
			if not @@s_kVSAvoidLibraries.include? strLibName
				@@s_kVSAvoidLibraries.push strLibName
			end
		end
	end
	
	# Returns a string with all the libraries to be avoided when linking
	def composeAvoidLibrariesToLink()
		strAvoidLibraries = ""
		
		@@s_kVSAvoidLibraries.each do |strLibName|
			strAvoidLibraries = strAvoidLibraries + strLibName + ";"
		end
		
		return strAvoidLibraries
	end
end

module OS
  def OS.windows?
    (/cygwin|mswin|mingw|bccwin|wince|emx/ =~ RUBY_PLATFORM) != nil
  end

  def OS.mac?
   (/darwin/ =~ RUBY_PLATFORM) != nil
  end

  def OS.unix?
    !OS.windows?
  end

  def OS.linux?
    OS.unix? and not OS.mac?
  end
end

# Prepare libraries and main project properties data array
$g_kProjectPropertiesConfig = Array.new

# Prepare libraries and main project properties array
$g_kProjectProperties = Array.new

# Prepare 3rdParty libraries properties data array
$g_k3rdPartyLibsPropertiesConfig = Array.new

# Prepare 3rdParty libraries properties array
$g_k3rdPartyLibsProperties = Array.new

def GetProjectProperties(strProjectName)

	$g_kProjectProperties.each do |kProjectProperties|
		if kProjectProperties.m_strProjectName == strProjectName
			return kProjectProperties
		end
	end
	
	$g_k3rdPartyLibsProperties.each do |kProjectProperties|
		if kProjectProperties.m_strProjectName == strProjectName
			return kProjectProperties
		end
	end

	return nil
	
end
