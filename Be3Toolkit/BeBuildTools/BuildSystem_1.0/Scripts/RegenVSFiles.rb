####################################################################
##
## BAK ENGINE 3
## BUILD SYSTEM
## 
## RegenVSFiles.rb
## This ruby file generates the VisualStudio project files
##
## Javier Calet Toledo - 2013
##
###################################################################

#!/bin/sh

require_relative 'Common'

# ProjectGUIDs - Stores the auto-generated GUIDs for VS projects
$g_kProjectGUIDs = Hash.new { |hash, key| }

# Project targets for VS projects
$g_kProjectTargets = ["DebugStatic", "ReleaseStatic"]

# If any project file has been changed, the solution must be re-generated
$g_bForceSolutionRegeneration = false

# Recursive found folders
$m_strFindFolders = ""

# RegenVSFiles Class
class RegenVSFiles

	attr_accessor :m_outputFile
	attr_accessor :m_strDataToWrite
	attr_accessor :m_strIncludeFolderName
	attr_accessor :m_kProjectProperties
	attr_accessor :m_bIsFileCreationIntoMemory
	attr_accessor :m_strFileMemoryBuffer

	def initialize()
		self.m_kProjectProperties = nil
		self.m_outputFile = nil
		self.m_strDataToWrite = ""
		self.m_strIncludeFolderName = ""
		self.m_bIsFileCreationIntoMemory = true
		self.m_strFileMemoryBuffer = ""
    end

	# Writes m_strDataToWrite content to m_outputFile and clean it
	private
	def writeDataToFile
		if m_bIsFileCreationIntoMemory
			self.m_strFileMemoryBuffer = self.m_strFileMemoryBuffer + self.m_strDataToWrite + "\n"
		else
			self.m_outputFile.puts self.m_strDataToWrite
		end
		
		self.m_strDataToWrite = ""
	end
	
	# Checks for file differences and override it if they exist
	private
	def checkFileDifferencesAndSave(strPathFile, bForceGeneration)
		
		bNeedToSaveToFile = false
		
		if File.exists? strPathFile
			kPreviousFile = File.open(strPathFile, "r")
			strPreviousFileContent = kPreviousFile.read
			kPreviousFile.close
			
			if self.m_strFileMemoryBuffer.size != strPreviousFileContent.size
				bNeedToSaveToFile = true
			else
				bIsEqual = true
				
				# We avoid to compare first 3 chars due they are utf8 special chars
				for i in 3..(self.m_strFileMemoryBuffer.size)
					if self.m_strFileMemoryBuffer[i] != strPreviousFileContent[i]
						bIsEqual = false
					end
				end
				
				if not bIsEqual
					bNeedToSaveToFile = true
				end
			end
		else
			bNeedToSaveToFile = true
		end
		
		if bNeedToSaveToFile or bForceGeneration
			$g_bForceSolutionRegeneration = true
			
			self.m_outputFile = File.new(strPathFile, "w")
			self.m_outputFile.write self.m_strFileMemoryBuffer
			self.m_outputFile.close
			
			self.m_strFileMemoryBuffer = ""
		end
		
	end

	# Generates the Batch files for opening VS
	private
	def generateBatchFiles(kProjectProperties, strCleanCommand)
		
		if not kProjectProperties.m_bIsExternalLibrary
			strBatchFile = "#{kProjectProperties.m_strOutputFileName}_VS.bat"
			strBatchFilePath = kProjectProperties.m_strProjectPath + "/" + kProjectProperties.m_strProjectName + "/" + strBatchFile
		else
			strBatchFile = "#{kProjectProperties.m_strProjectName}_VS.bat"
			strBatchFilePath = strBatchFile
		end
		
		if strCleanCommand == "clean"
		
			if File.exists? strBatchFilePath
				puts "Cleaning #{strBatchFile} ..."
				
				File.delete strBatchFilePath
			end
		
			return
			
		end
		
		if not kProjectProperties.m_bIsExternalLibrary
			puts "Building #{$g_kPlatformNames[PLATFORM_WINDOWS]} " + kProjectProperties.m_strOutputFileName + " Batch files..."
		else
			puts "Building #{$g_kPlatformNames[PLATFORM_WINDOWS]} " + kProjectProperties.m_strProjectName + " Batch files..."
		end

		if m_bIsFileCreationIntoMemory
			self.m_strFileMemoryBuffer = ""
		else
			self.m_outputFile = File.new(strBatchFilePath, "w")
		end

		self.m_strDataToWrite = "@setlocal
call \"%VS140COMNTOOLS%vsvars32.bat\""
		writeDataToFile
		writeDataToFile
		
		self.m_strDataToWrite = "@SET BE3_SDK=" + BE3_SDK_DIR.gsub('/','\\')
		writeDataToFile
		
		writeDataToFile

		if kProjectProperties.m_bIsExternalLibrary
			self.m_strDataToWrite = "start WDExpress.exe .#{PROJECT_FILES_PATH}/#{kProjectProperties.m_strProjectName}.sln /useenv
@endlocal"
		else
			self.m_strDataToWrite = "start WDExpress.exe .#{PROJECT_FILES_PATH}/#{kProjectProperties.m_strOutputFileName}.sln /useenv
@endlocal"
		end
		writeDataToFile
		writeDataToFile
		
		if m_bIsFileCreationIntoMemory
			checkFileDifferencesAndSave strBatchFilePath, false
		else
			self.m_outputFile.close
		end
		
		if not kProjectProperties.m_bIsExternalLibrary
			strPDBBatchFile = "Batch_Copy_External_PDB_Compile.bat"
			strPDBBatchFilePath = kProjectProperties.m_strProjectPath + kProjectProperties.m_strProjectName + PROJECT_FILES_PATH + "/" + strPDBBatchFile
			
			if m_bIsFileCreationIntoMemory
				self.m_strFileMemoryBuffer = ""
			else
				self.m_outputFile = File.new(strPDBBatchFilePath, "w")
			end
			
			if ProjectProperties::s_kProjectExternalDependencies[BE3_MAIN_LIBRARY].length > 0
				ProjectProperties::s_kProjectExternalDependencies[BE3_MAIN_LIBRARY].each do |strDependencyName|
				
					strDependencyFilename = "#{strDependencyName}.lib"
					strDependencyFilenamePath = BE3_SDK_DIR_SCRIPTS + "BeLibsExternal/" + strDependencyName + "/lib/Windows/DebugStatic/" + strDependencyFilename
											
					if File.exists? strDependencyFilenamePath
						self.m_strDataToWrite = "echo Copying PDB #{strDependencyName.downcase}.pdb to bin folder ...
if exist #{BE3_SDK_DIR.gsub('/','\\')}BeLibsExternal\\#{strDependencyName}\\lib\\Windows\\DebugStatic\\*.pdb (
	copy #{BE3_SDK_DIR.gsub('/','\\')}BeLibsExternal\\#{strDependencyName}\\lib\\Windows\\DebugStatic\\*.pdb %CD%\\..\\bin\\libs\\DebugStatic
)"
						writeDataToFile			
					end
					
				end
			end
		
			if m_bIsFileCreationIntoMemory
				checkFileDifferencesAndSave strPDBBatchFilePath, false
			else
				self.m_outputFile.close
			end
		end
		
	end
	
	# Searches and adds the files for a specific filter
	private
	def projectWriteFilesItemGroup(kProjectProperties, strClFilter, strBasePathToFiles, strIncrementalPathToFiles, kFiltersArray)
	
		strSearchingPath = strBasePathToFiles + strIncrementalPathToFiles
             
		if Dir.exists?(strSearchingPath)
			Dir.foreach(strSearchingPath) do |strItem|
				next if strItem == '.' or strItem == '..'
				
				strItemTmp = "/%1"
				strItemTmp = strItemTmp.gsub("%1", strItem)
				strFinalFolder = strSearchingPath + strItemTmp
				if File.directory?(strFinalFolder)
					projectWriteFilesItemGroup kProjectProperties, strClFilter, strBasePathToFiles, strIncrementalPathToFiles + strItemTmp, kFiltersArray
				end
			end
		end

		strSearchingPath = strBasePathToFiles + strIncrementalPathToFiles	
		if Dir.exists?(strSearchingPath)
			Dir.foreach(strSearchingPath) do |strItem|
				next if strItem == '.' or strItem == '..'
				
				if kFiltersArray.length > 0
					bFilterFound = false
					kFiltersArray.each do |strFilter|
						if strItem.end_with?(strFilter) == true
							bFilterFound = true
							break
						end
					end
					next if bFilterFound == false
				end

				strFinalFolder = strSearchingPath + "/" + strItem
                
				if File.file?(strFinalFolder)
					strFinalClFilter = strClFilter
				
					if kProjectProperties.m_kAvoidCompileInclusiveCodeFiles.length > 0
						kProjectProperties.m_kAvoidCompileInclusiveCodeFiles.each do |strAvoidItem|
							if strItem == strAvoidItem
								strFinalClFilter = "ClInclude"
								break
							end
						end
					end
					if kProjectProperties.m_kVSAvoidCompile.length > 0
						kProjectProperties.m_kVSAvoidCompile.each do |strAvoidItem|
							if strItem == strAvoidItem
								strFinalClFilter = "None"
								break
							end
						end
					end
					if kProjectProperties.m_kOnlyCompileCodeFiles.length > 0
						bIsFound = false
						kProjectProperties.m_kOnlyCompileCodeFiles.each do |strOnlyCompileItem|
							if strItem == strOnlyCompileItem
								bIsFound = true
								break
							end
						end
						if bIsFound == false
							strFinalClFilter = "ClInclude"
						end
					end
				
					self.m_strDataToWrite = "    <#{strFinalClFilter} Include=\"../%1/%2\" />"
					self.m_strDataToWrite = self.m_strDataToWrite.gsub("%1", strIncrementalPathToFiles)
					self.m_strDataToWrite = self.m_strDataToWrite.gsub("%2", strItem)
					writeDataToFile
				end
			end
		end

	end
	
	# Searches and adds the files into filters file
	private
	def projectWriteFilesIntoFilters(kProjectProperties, strClFilter, strBasePathToFiles, strIncrementalPathToFiles, kFiltersArray, bGeneratingFilters, strFilterName, strFirstFilterFolder)
	
		strSearchingPath = strBasePathToFiles + strIncrementalPathToFiles
             
		if Dir.exists?(strSearchingPath)
			Dir.foreach(strSearchingPath) do |strItem|
				next if strItem == '.' or strItem == '..'
				
				strItemTmp = "/%1"
				strItemTmp = strItemTmp.gsub("%1", strItem)
				strFinalFolder = strSearchingPath + strItemTmp
				if File.directory?(strFinalFolder)
					if bGeneratingFilters and strItemTmp != strFirstFilterFolder
						strIncrementalPathToFilesTemp = strIncrementalPathToFiles + strItemTmp		
						strIncrementalPathToFilesTemp = strIncrementalPathToFilesTemp.slice strFirstFilterFolder.length+1, strIncrementalPathToFilesTemp.length-strFirstFilterFolder.length-1

						self.m_strDataToWrite = "    <Filter Include=\"#{strFilterName}\\#{strIncrementalPathToFilesTemp.gsub("/", "\\")}\">
      <UniqueIdentifier>{#{generateGUID(strFilterName, strClFilter, strItemTmp).downcase}}</UniqueIdentifier>
    </Filter>"
						writeDataToFile
					end
				
					projectWriteFilesIntoFilters kProjectProperties, strClFilter, strBasePathToFiles, strIncrementalPathToFiles + strItemTmp, kFiltersArray, bGeneratingFilters, strFilterName, strFirstFilterFolder
				end
			end
		end

		strSearchingPath = strBasePathToFiles + strIncrementalPathToFiles	
		if Dir.exists?(strSearchingPath)
			Dir.foreach(strSearchingPath) do |strItem|
				next if strItem == '.' or strItem == '..'
				
				if kFiltersArray.length > 0
					bFilterFound = false
					kFiltersArray.each do |strFilter|
						if strItem.end_with?(strFilter) == true
							bFilterFound = true
							break
						end
					end
					next if bFilterFound == false
				end

				strFinalFolder = strSearchingPath + "/" + strItem
				if not bGeneratingFilters
					if File.file?(strFinalFolder)
						strIncrementalPathToFilesTemp = strIncrementalPathToFiles		
						strIncrementalPathToFilesTemp = strIncrementalPathToFilesTemp.slice strFirstFilterFolder.length+1, strIncrementalPathToFilesTemp.length-strFirstFilterFolder.length-1
						
						strFinalClFilter = strClFilter
						if kProjectProperties.m_kAvoidCompileInclusiveCodeFiles.length > 0
							kProjectProperties.m_kAvoidCompileInclusiveCodeFiles.each do |strAvoidItem|
								if strItem == strAvoidItem
									strFinalClFilter = "ClInclude"
									break
								end
							end
						end
						if kProjectProperties.m_kVSAvoidCompile.length > 0
							kProjectProperties.m_kVSAvoidCompile.each do |strAvoidItem|
								if strItem == strAvoidItem
									strFinalClFilter = "None"
									break
								end
							end
						end
						if kProjectProperties.m_kOnlyCompileCodeFiles.length > 0
							bIsFound = false
							kProjectProperties.m_kOnlyCompileCodeFiles.each do |strOnlyCompileItem|
								if strItem == strOnlyCompileItem
									bIsFound = true
									break
								end
							end
							if bIsFound == false
								strFinalClFilter = "ClInclude"
							end
						end
						
						self.m_strDataToWrite = "    <#{strFinalClFilter} Include=\"../%1/%2\">
      <Filter>#{strFilterName}"
	  
						if strIncrementalPathToFilesTemp != nil
							strIncrementalPathToFilesTemp = strIncrementalPathToFilesTemp.gsub("/", "\\")
							self.m_strDataToWrite += "\\#{strIncrementalPathToFilesTemp}"
						end
						
						self.m_strDataToWrite += "</Filter>
    </#{strFinalClFilter}>"
	  				
						self.m_strDataToWrite = self.m_strDataToWrite.gsub("%1", strIncrementalPathToFiles)
						self.m_strDataToWrite = self.m_strDataToWrite.gsub("%2", strItem)
						writeDataToFile
					end
				end
			end
		end

	end
	
	# Searches folders recursively
	private
	def findFolders(strBasePath, strIncrementalPathToFiles, strPrefix, strSuffix)
	
		strSearchingPath = strBasePath + strIncrementalPathToFiles
		
		strFoundDirectory = strPrefix + strSearchingPath + strSuffix;
		
		$m_strFindFolders = $m_strFindFolders + strFoundDirectory

		if Dir.exists?(strSearchingPath)
			Dir.foreach(strSearchingPath) do |strItem|
				next if strItem == '.' or strItem == '..'
				
				strItemTmp = "/%1"
				strItemTmp = strItemTmp.gsub("%1", strItem)
				
				strFinalFolder = strSearchingPath + strItemTmp
				if File.directory?(strFinalFolder)
					strIncrementalPathToFilesTmp = strIncrementalPathToFiles + strItemTmp
					
					findFolders strBasePath, strIncrementalPathToFilesTmp, strPrefix, strSuffix
				end
			end
		end

	end

	# Generates the project file
	private
	def projectGenerateFile(strProjectPath, strProjectName, strProjectGUID, strVSResourcesFolder = nil, avoidSourceFiles = false)

		kProjectProperties = GetProjectProperties strProjectName
		if kProjectProperties.m_bIsMainProject
			puts "Building #{$g_kPlatformNames[PLATFORM_WINDOWS]} Visual Studio - " + kProjectProperties.m_strOutputFileName + " project..."
		else
			puts "Building #{$g_kPlatformNames[PLATFORM_WINDOWS]} Visual Studio - " + strProjectName + " project..."
		end
		
		strProjectFile = strProjectName + ".vcxproj"
		if kProjectProperties.m_bIsMainProject
			strProjectPathFile = strProjectPath + strProjectName + PROJECT_FILES_PATH + "/" + kProjectProperties.m_strOutputFileName + ".vcxproj"
		else
			strProjectPathFile = strProjectPath + strProjectName + PROJECT_FILES_PATH + "/" + strProjectName + ".vcxproj"
		end
		
		if m_bIsFileCreationIntoMemory
			self.m_strFileMemoryBuffer = [239, 187, 191].pack('C*')
		else
			self.m_outputFile = File.new(strProjectPathFile, "w")
			
			# utf8 Solution Header;
			self.m_outputFile.write [239, 187, 191].pack('C*')
		end
		
		# Project XML header
		self.m_strDataToWrite = "<?xml version=\"1.0\" encoding=\"utf-8\"?>
<Project DefaultTargets=\"Build\" ToolsVersion=\"12.0\" xmlns=\"http://schemas.microsoft.com/developer/msbuild/2003\">"
		writeDataToFile

		# ProjectConfigurations
		self.m_strDataToWrite = "  <ItemGroup Label=\"ProjectConfigurations\">"
		writeDataToFile
		$g_kProjectTargets.each do |strTarget|
			self.m_strDataToWrite = "    <ProjectConfiguration Include=\"#{strTarget}|Win32\">
      <Configuration>#{strTarget}</Configuration>
      <Platform>Win32</Platform>
    </ProjectConfiguration>"
			writeDataToFile
		end
		self.m_strDataToWrite = "  </ItemGroup>"
		writeDataToFile
					
		# Globals
		self.m_strDataToWrite = "  <PropertyGroup Label=\"Globals\">
    <ProjectGuid>{%guid}</ProjectGuid>
    <RootNamespace>%name</RootNamespace>
    <Keyword>Win32Proj</Keyword>
  </PropertyGroup>"
		self.m_strDataToWrite = self.m_strDataToWrite.gsub("%name", strProjectName)
		self.m_strDataToWrite = self.m_strDataToWrite.gsub("%guid", strProjectGUID)
		writeDataToFile
		
		# Import default props
		self.m_strDataToWrite = "  <Import Project=\"$(VCTargetsPath)\\Microsoft.Cpp.Default.props\" />"
		writeDataToFile
		
		# Configuration
		$g_kProjectTargets.each do |strTarget|
			self.m_strDataToWrite = "  <PropertyGroup Condition=\"'$(Configuration)|$(Platform)'=='#{strTarget}|Win32'\" Label=\"Configuration\">
    <ConfigurationType>%libraryType</ConfigurationType>
    <PlatformToolset>v140</PlatformToolset>
    <CharacterSet>Unicode</CharacterSet>
    <WholeProgramOptimization>%optimised</WholeProgramOptimization>
  </PropertyGroup>"
			
			if self.m_kProjectProperties.m_bIsMainProject
				self.m_strDataToWrite = self.m_strDataToWrite.gsub("%libraryType", "Application")
			else
				#if strTarget.include? "Dynamic"
				#	self.m_strDataToWrite = self.m_strDataToWrite.gsub("%libraryType", "DynamicLibrary")
				#else
				#	if self.m_kProjectProperties.m_bIsFinalLibrary
				#		if self.m_kProjectProperties.m_LibraryType == ProjectProperties::LIBRARY_TYPE_STATIC
				#			self.m_strDataToWrite = self.m_strDataToWrite.gsub("%libraryType", "StaticLibrary")
				#		else
				#			self.m_strDataToWrite = self.m_strDataToWrite.gsub("%libraryType", "DynamicLibrary")
				#		end
				#	end
				#end
				
				if strTarget.include? "Dynamic"
					if self.m_kProjectProperties.m_LibraryType == ProjectProperties::LIBRARY_TYPE_STATIC
						self.m_strDataToWrite = self.m_strDataToWrite.gsub("%libraryType", "StaticLibrary")
					else
						self.m_strDataToWrite = self.m_strDataToWrite.gsub("%libraryType", "DynamicLibrary")
					end
				else
					self.m_strDataToWrite = self.m_strDataToWrite.gsub("%libraryType", "StaticLibrary")
				end
			end
			
			if strTarget.include? "Release"
				#self.m_strDataToWrite = self.m_strDataToWrite.gsub("%optimised", "true")
				self.m_strDataToWrite = self.m_strDataToWrite.gsub("%optimised", "false")
			else
				self.m_strDataToWrite = self.m_strDataToWrite.gsub("%optimised", "false")
			end
			
			writeDataToFile
		end

		# Project props and ExtensionSettings
		self.m_strDataToWrite = "  <Import Project=\"$(VCTargetsPath)\\Microsoft.Cpp.props\" />
  <ImportGroup Label=\"ExtensionSettings\">
  </ImportGroup>"
		writeDataToFile

		# PropertySheets
		$g_kProjectTargets.each do |strTarget|
			self.m_strDataToWrite = "  <ImportGroup Condition=\"'$(Configuration)|$(Platform)'=='#{strTarget}|Win32'\" Label=\"PropertySheets\">
    <Import Project=\"$(UserRootDir)\\Microsoft.Cpp.$(Platform).user.props\" Condition=\"exists('$(UserRootDir)\\Microsoft.Cpp.$(Platform).user.props')\" Label=\"LocalAppDataPlatform\" />
  </ImportGroup>"	
			writeDataToFile
		end
		
		# UserMacros and _ProjectFileVersion
		self.m_strDataToWrite = "  <PropertyGroup Label=\"UserMacros\" />
  <PropertyGroup>
    <_ProjectFileVersion>11.0.61030.0</_ProjectFileVersion>
  </PropertyGroup>"
		writeDataToFile
		
		# Out and internal directories, libraries and link config
		$g_kProjectTargets.each do |strTarget|
			self.m_strDataToWrite = "  <PropertyGroup Condition=\"'$(Configuration)|$(Platform)'=='#{strTarget}|Win32'\">"
			
			if not kProjectProperties.m_bIsExternalLibrary
				self.m_strDataToWrite += "    <OutDir>$(SolutionDir)/../bin/libs/$(Configuration)/</OutDir>"
			else
				self.m_strDataToWrite += "    <OutDir>$(SolutionDir)/../lib/Windows/$(Configuration)/</OutDir>"
			end
	
			self.m_strDataToWrite += "    <IntDir>$(ProjectDir)/../obj/$(Configuration)/</IntDir>
    <EnableManagedIncrementalBuild>%incrementalBuilding</EnableManagedIncrementalBuild>"
	
			if strTarget.include? "Release"
				self.m_strDataToWrite = self.m_strDataToWrite.gsub("%incrementalBuilding", "false")
			else
				self.m_strDataToWrite = self.m_strDataToWrite.gsub("%incrementalBuilding", "true")
			end
			
			writeDataToFile
			
			if self.m_kProjectProperties.m_bIsMainProject
				self.m_strDataToWrite = "    <IgnoreImportLibrary>true</IgnoreImportLibrary>
    <LinkIncremental>false</LinkIncremental>
    <GenerateManifest>true</GenerateManifest>"
				writeDataToFile
			end
			
			self.m_strDataToWrite = "  </PropertyGroup>"
			writeDataToFile
		end
  
		# Configuration details
		$g_kProjectTargets.each do |strTarget|
			self.m_strDataToWrite = "  <ItemDefinitionGroup Condition=\"'$(Configuration)|$(Platform)'=='#{strTarget}|Win32'\">"
			writeDataToFile
		
			if self.m_kProjectProperties.m_bIsMainProject 
				strBE3LibsCommonIncludes = "$(ProjectDir)../.;$(ProjectDir)../src;$(ProjectDir)../Windows;$(ProjectDir)../Desktop;#{BE3_SDK_DIR}BeLibs;"
				strBE3LibsCommonIncludes = strBE3LibsCommonIncludes + BE3_SDK_DIR + "3rdParty;"
			else
				$m_strFindFolders = ""
				findFolders "src", 		"", "$(ProjectDir)../", ";"
				findFolders "Windows", 	"", "$(ProjectDir)../", ";"
				findFolders "Desktop", 	"", "$(ProjectDir)../", ";"
				
				strBE3LibsCommonIncludes = "$(ProjectDir)../../.;#{$m_strFindFolders}"
				
				strBE3LibsCommonIncludes = strBE3LibsCommonIncludes + "$(ProjectDir)../../../3rdParty;"
			end
			strBE3LibIncludes = ""
			
			kArrayDependencyVisited = Array.new
			ProjectProperties::s_kProjectDependencies.each do |strProjectNameGlobal, kDependenciesArray|
				if (strProjectNameGlobal == strProjectName) or self.m_kProjectProperties.m_bIsMainProject
					if kDependenciesArray.length > 0
						kDependenciesArray.each do |strProjectDependencyName|
						
							dependencyProjectProperties = GetProjectProperties strProjectDependencyName
							if dependencyProjectProperties != nil
								if dependencyProjectProperties.m_kProjectPlatformTargets == nil or (dependencyProjectProperties.m_kProjectPlatformTargets != nil and dependencyProjectProperties.m_kProjectPlatformTargets.include? PLATFORM_WINDOWS)
									if not self.m_kProjectProperties.m_bIsMainProject or (self.m_kProjectProperties.m_bIsMainProject and not kArrayDependencyVisited.include?(strProjectDependencyName))
										if self.m_kProjectProperties.m_bIsMainProject 
											strBE3LibIncludes += "#{BE3_SDK_DIR}BeLibs/#{strProjectDependencyName}/src;#{BE3_SDK_DIR}BeLibs/#{strProjectDependencyName}/Windows;#{BE3_SDK_DIR}BeLibs/#{strProjectDependencyName}/Desktop;"
										else
											strBE3LibIncludes += "$(ProjectDir)../../#{strProjectDependencyName}/src;$(ProjectDir)../../#{strProjectDependencyName}/Windows;$(ProjectDir)../../#{strProjectDependencyName}/Desktop;"
										end
										
										if self.m_kProjectProperties.m_bIsMainProject
											kArrayDependencyVisited.push(strProjectDependencyName)
										end
									end
								end
							end
							
						end
					end
				end
			end
			
			kArrayDependencyVisited = Array.new
			ProjectProperties::s_kProject3rdPartyDependencies.each do |strProjectNameGlobal, kDependenciesArray|
				if (strProjectNameGlobal == strProjectName) or self.m_kProjectProperties.m_bIsMainProject
					if kDependenciesArray.length > 0
						kDependenciesArray.each do |strProjectDependencyName|
						
							dependencyProjectProperties = GetProjectProperties strProjectDependencyName
							if dependencyProjectProperties != nil
								if dependencyProjectProperties.m_kProjectPlatformTargets == nil or (dependencyProjectProperties.m_kProjectPlatformTargets != nil and dependencyProjectProperties.m_kProjectPlatformTargets.include? PLATFORM_WINDOWS)
									if not self.m_kProjectProperties.m_bIsMainProject or (self.m_kProjectProperties.m_bIsMainProject and not kArrayDependencyVisited.include?(strProjectDependencyName))
										if self.m_kProjectProperties.m_bIsMainProject 
											strBE3LibIncludes += "#{BE3_SDK_DIR}3rdParty/#{strProjectDependencyName}/src;#{BE3_SDK_DIR}3rdParty/#{strProjectDependencyName}/Windows;#{BE3_SDK_DIR}3rdParty/#{strProjectDependencyName}/Desktop;"
										else
											strBE3LibIncludes += "$(ProjectDir)../../../3rdParty/#{strProjectDependencyName}/src;$(ProjectDir)../../../3rdParty/#{strProjectDependencyName}/Windows;$(ProjectDir)../../../3rdParty/#{strProjectDependencyName}/Desktop;"
										end
										
										if self.m_kProjectProperties.m_bIsMainProject
											kArrayDependencyVisited.push(strProjectDependencyName)
										end
									end
								end
							end
							
						end
					end
				end
			end
			
			kArrayDependencyVisited = Array.new
			ProjectProperties::s_kProjectExternalDependencies.each do |strProjectNameGlobal, kDependenciesArray|
				if (strProjectNameGlobal == strProjectName) or self.m_kProjectProperties.m_bIsMainProject
					if kDependenciesArray.length > 0
						kDependenciesArray.each do |strProjectDependencyName|
						
							if not self.m_kProjectProperties.m_bIsMainProject or (self.m_kProjectProperties.m_bIsMainProject and not kArrayDependencyVisited.include?(strProjectDependencyName))
								if self.m_kProjectProperties.m_bIsMainProject 
									strBE3LibIncludes += "#{BE3_SDK_DIR}BeLibsExternal/#{strProjectDependencyName}/include;"
								else
									strBE3LibIncludes += "$(ProjectDir)../../../BeLibsExternal/#{strProjectDependencyName}/include;"
								end
								
								if self.m_kProjectProperties.m_bIsMainProject
									kArrayDependencyVisited.push(strProjectDependencyName)
								end
							end
							
						end
					end
				end
			end
			
			# ClCompile
			self.m_strDataToWrite = "    <ClCompile>"
			writeDataToFile
			
			if strTarget.include? "Release"
				self.m_strDataToWrite = "      <MultiProcessorCompilation>true</MultiProcessorCompilation>"
				writeDataToFile
			end
		
			self.m_strDataToWrite = "      <Optimization>%optimization</Optimization>
      <AdditionalIncludeDirectories>%libraryAdditionalIncludes%libraryExternalIncludes#{strBE3LibsCommonIncludes}#{strBE3LibIncludes}</AdditionalIncludeDirectories>
      <PreprocessorDefinitions>%libraryExports%dynamicLinkWIN32;%configurationDefs_WINDOWS;_USRDLL;TARGET_WINDOWS;TARGET_DESKTOP;%(PreprocessorDefinitions)</PreprocessorDefinitions>
      <MinimalRebuild>false</MinimalRebuild>
      <ExceptionHandling>Sync</ExceptionHandling>"
			if self.m_kProjectProperties.m_strVSDisableWarnings.length > 0
				self.m_strDataToWrite += "\n      <DisableSpecificWarnings>#{self.m_kProjectProperties.m_strVSDisableWarnings}</DisableSpecificWarnings>"
			end
			if self.m_kProjectProperties.m_strExtraIncludeDirs != nil
				$m_strFindFolders = ""
				for includeFolder in self.m_kProjectProperties.m_strExtraIncludeDirs
					findFolders includeFolder, "", "$(ProjectDir)../", ";"
				end

				self.m_strDataToWrite = self.m_strDataToWrite.gsub("%libraryAdditionalIncludes", "#{$m_strFindFolders}")
			else
				self.m_strDataToWrite = self.m_strDataToWrite.gsub("%libraryAdditionalIncludes", "")
			end
			ProjectProperties::s_kProjectExternalDependencies.each do |strProjectNameGlobal, kDependenciesArray|
				if (strProjectNameGlobal == strProjectName) or self.m_kProjectProperties.m_bIsMainProject
					if kDependenciesArray.length > 0
						strExternalIncludes = ""
						kDependenciesArray.each do |strProjectDependencyName|
							if self.m_kProjectProperties.m_bIsMainProject 
								strExternalIncludes += "#{BE3_SDK_DIR}BeLibsExternal/#{strProjectDependencyName}/include;"
							else
								strExternalIncludes += "$(ProjectDir)../../../BeLibsExternal/#{strProjectDependencyName}/include;"
							end
						end
						self.m_strDataToWrite = self.m_strDataToWrite.gsub("%libraryExternalIncludes", "#{strExternalIncludes}")
					else
						self.m_strDataToWrite = self.m_strDataToWrite.gsub("%libraryExternalIncludes", "")
					end
				end
			end
			
			strGlobalSymbols = ""
			for extraExport in $g_kGlobalSymbols
				strGlobalSymbols = strGlobalSymbols + "#{extraExport};"
			end
			if not self.m_kProjectProperties.m_bIsMainProject and self.m_kProjectProperties.m_strProjectExports != nil
				strAdditionalExports = ""
				for extraExport in self.m_kProjectProperties.m_strProjectExports
					strAdditionalExports = strAdditionalExports + "#{extraExport};"
				end
				self.m_strDataToWrite = self.m_strDataToWrite.gsub("%libraryExports", "#{strGlobalSymbols}#{strAdditionalExports}")
			else
				if self.m_kProjectProperties.m_bIsMainProject
					self.m_strDataToWrite = self.m_strDataToWrite.gsub("%libraryExports", "APP_EXPORTS;#{strGlobalSymbols}")
				else
					self.m_strDataToWrite = self.m_strDataToWrite.gsub("%libraryExports", "#{strGlobalSymbols}")
				end
			end
			
			if strTarget.include? "Dynamic"
				self.m_strDataToWrite = self.m_strDataToWrite.gsub("%dynamicLink", "DYNAMIC_LINK;")
			else
				self.m_strDataToWrite = self.m_strDataToWrite.gsub("%dynamicLink", "")
			end
			
			strSpecialDefines = ""
			if $g_kWindowsSimulator
				strSpecialDefines = strSpecialDefines + "OGL_ES_SIMULATOR;"
			end
			if $g_kUSE_OGL_ES2
				strSpecialDefines = strSpecialDefines + "USE_OGL_ES2;"
			end
			
			if strTarget.include? "Release"
				#self.m_strDataToWrite = self.m_strDataToWrite.gsub("%optimization", "MaxSpeed")
				self.m_strDataToWrite = self.m_strDataToWrite.gsub("%optimization", "Disabled")
				self.m_strDataToWrite = self.m_strDataToWrite.gsub("%configurationDefs", "NDEBUG;#{strSpecialDefines}")
			else
				self.m_strDataToWrite = self.m_strDataToWrite.gsub("%optimization", "Disabled")
				self.m_strDataToWrite = self.m_strDataToWrite.gsub("%configurationDefs", "_DEBUG;DEBUG;PROFILE;#{strSpecialDefines}")
			end
			writeDataToFile
	  
			if strTarget.include? "Debug"
				self.m_strDataToWrite = "      <BasicRuntimeChecks>EnableFastChecks</BasicRuntimeChecks>"
				writeDataToFile
			end
						
			self.m_strDataToWrite = "      <RuntimeLibrary>%MT</RuntimeLibrary>
      <FloatingPointModel>Fast</FloatingPointModel>
      <ForceConformanceInForLoopScope>true</ForceConformanceInForLoopScope>
      <RuntimeTypeInfo>true</RuntimeTypeInfo>
      <PrecompiledHeader />
      <ProgramDataBaseFileName>$(IntDir)$(ProjectName).pdb</ProgramDataBaseFileName>
      <WarningLevel>Level4</WarningLevel>
      <TreatWarningAsError>true</TreatWarningAsError>
      <DebugInformationFormat>ProgramDatabase</DebugInformationFormat>
      <CompileAs>Default</CompileAs>
    </ClCompile>"
			if strTarget.include? "Release"
				self.m_strDataToWrite = self.m_strDataToWrite.gsub("%MT", "MultiThreadedDLL")
			else
				self.m_strDataToWrite = self.m_strDataToWrite.gsub("%MT", "MultiThreadedDebugDLL")
			end
			writeDataToFile
			
			# ProjectReference
			if strTarget.include? "Static"
				if self.m_kProjectProperties.m_bIsFinalLibrary or self.m_kProjectProperties.m_bIsMainProject
					self.m_strDataToWrite = "    <ProjectReference>
      <LinkLibraryDependencies>%linkLibraries</LinkLibraryDependencies>
    </ProjectReference>"
	
					if self.m_kProjectProperties.m_bIsMainProject
						self.m_strDataToWrite = self.m_strDataToWrite.gsub("%linkLibraries", "true")
					else
						#self.m_strDataToWrite = self.m_strDataToWrite.gsub("%linkLibraries", "true")
						self.m_strDataToWrite = self.m_strDataToWrite.gsub("%linkLibraries", "false")
					end
	
					writeDataToFile
				end
			end
		
			# Libraries include folders
			strBE3LibsCommonLibIncludes = "$(SolutionDir)/../bin/libs/$(ConfigurationName);$(SolutionDir)/#{BE3_SDK_DIR}3rdParty;$(SolutionDir)/#{BE3_SDK_DIR}3rdParty/Lib/Windows;"
		
			strExternalCommonLibIncludes = ""
			kArrayDependencyVisited = Array.new
			ProjectProperties::s_kProjectExternalDependencies.each do |strProjectNameGlobal, kDependenciesArray|
				if (strProjectNameGlobal == strProjectName) or self.m_kProjectProperties.m_bIsMainProject
					if kDependenciesArray.length > 0
						kDependenciesArray.each do |strProjectDependencyName|
							if not kArrayDependencyVisited.include?(strProjectDependencyName)
							
								strDependencyFilename = "#{strProjectDependencyName}.lib"
								strDependencyFilenamePath = BE3_SDK_DIR_SCRIPTS + "BeLibsExternal/" + strProjectDependencyName + "/lib/Windows/" + strTarget + "/" + strDependencyFilename
								if File.exists? strDependencyFilenamePath
							
									if self.m_kProjectProperties.m_bIsMainProject 
										strExternalCommonLibIncludes += "#{BE3_SDK_DIR}External/#{strProjectDependencyName}/lib/Windows/#{strTarget};"
									else
										strExternalCommonLibIncludes += "$(ProjectDir)../../../External/#{strProjectDependencyName}/lib/Windows/#{strTarget};"
									end
									
								end
								
								if self.m_kProjectProperties.m_bIsMainProject
									kArrayDependencyVisited.push(strProjectDependencyName)
								end
							end
						end
					end
				end
			end
		
			# Lib
			if not self.m_kProjectProperties.m_bIsMainProject
				if not self.m_kProjectProperties.m_bIsFinalLibrary or (self.m_kProjectProperties.m_bIsFinalLibrary and strTarget.include? "Static")
					self.m_strDataToWrite = "    <Lib>
      <AdditionalLibraryDirectories>#{strBE3LibsCommonLibIncludes}#{strExternalCommonLibIncludes}%(AdditionalLibraryDirectories)</AdditionalLibraryDirectories>"
					writeDataToFile
					if self.m_kProjectProperties.m_bIsFinalLibrary
						strExternalDependencies = ""
						
						kArrayDependencyVisited = Array.new
						ProjectProperties::s_kProjectExternalDependencies.each do |strProjectNameGlobal, kDependenciesArray|
							if (strProjectNameGlobal == strProjectName)
								if kDependenciesArray.length > 0
									kDependenciesArray.each do |strProjectDependencyName|
									
										if not kArrayDependencyVisited.include?(strProjectDependencyName)
											strDependencyFilename = "#{strProjectDependencyName}.lib"
											strDependencyFilenamePath = BE3_SDK_DIR_SCRIPTS + "BeLibsExternal/" + strProjectDependencyName + "/lib/Windows/" + strTarget + "/" + strDependencyFilename
											
											if File.exists? strDependencyFilenamePath
												strExternalDependencies = strExternalDependencies + "#{strDependencyFilename};"
												kArrayDependencyVisited.push(strProjectDependencyName)
											end
										end
										
									end
								end
							end
						end
			
						self.m_strDataToWrite = "      <AdditionalDependencies>#{strExternalDependencies}</AdditionalDependencies>"
						writeDataToFile
					end
					
					if self.m_kProjectProperties.m_bIsFinalLibrary and strTarget.include? "Static"
						self.m_strDataToWrite = "      <OutputFile>$(OutDir)$(TargetName).lib</OutputFile>"
						writeDataToFile
					end
					
					if self.m_kProjectProperties.m_kVSDisableLinkWarnings.length > 0
						strDisableLinkWarnings = ""
						self.m_kProjectProperties.m_kVSDisableLinkWarnings.each do |strItem|
							strDisableLinkWarnings = strDisableLinkWarnings + "/ignore:" + strItem + " "
						end
						strDisableLinkWarnings = strDisableLinkWarnings + "%(AdditionalOptions)"
						
						self.m_strDataToWrite = "      <AdditionalOptions>#{strDisableLinkWarnings}</AdditionalOptions>"
						writeDataToFile
					end
		  
					self.m_strDataToWrite = "    </Lib>"
					writeDataToFile
				end
			end
			
			# Link
			if self.m_kProjectProperties.m_bIsMainProject or (not self.m_kProjectProperties.m_bIsMainProject and self.m_kProjectProperties.m_bIsFinalLibrary and strTarget.include? "Dynamic")
				self.m_strDataToWrite = "    <Link>"
				writeDataToFile
				
				if not self.m_kProjectProperties.m_bIsMainProject
					self.m_strDataToWrite = "      <OutputFile>$(OutDir)$(TargetName).dll</OutputFile>
      <AdditionalLibraryDirectories>#{strBE3LibsCommonLibIncludes}#{strExternalCommonLibIncludes}%(AdditionalLibraryDirectories)</AdditionalLibraryDirectories>
      <GenerateDebugInformation>true</GenerateDebugInformation>
      <ProgramDatabaseFile>$(IntDir)$(TargetName).pdb</ProgramDatabaseFile>"
					writeDataToFile
					
					if ProjectProperties::s_kVSAvoidLibraries.size > 0
						strAvoidLibs = self.m_kProjectProperties.composeAvoidLibrariesToLink
						self.m_strDataToWrite = "      <IgnoreSpecificDefaultLibraries>#{strAvoidLibs}</IgnoreSpecificDefaultLibraries>"
						writeDataToFile
					end
				else
					self.m_strDataToWrite = "      <AdditionalDependencies>#{$g_kFinalLibraryName}.lib;%(AdditionalDependencies)</AdditionalDependencies>
      <AdditionalLibraryDirectories>#{strBE3LibsCommonLibIncludes}#{strExternalCommonLibIncludes}%(AdditionalLibraryDirectories)</AdditionalLibraryDirectories>
      <GenerateDebugInformation>true</GenerateDebugInformation>
      <ProgramDatabaseFile>$(TargetDir)$(TargetName).pdb</ProgramDatabaseFile>
      <SubSystem>Windows</SubSystem>"
					writeDataToFile
					
					if strTarget.include? "Release"
						#self.m_strDataToWrite = "      <OptimizeReferences>true</OptimizeReferences>
						self.m_strDataToWrite = "      <OptimizeReferences>false</OptimizeReferences>
      <EnableCOMDATFolding>true</EnableCOMDATFolding>"
						writeDataToFile
					end
					
					self.m_strDataToWrite = "      <RandomizedBaseAddress>true</RandomizedBaseAddress>
      <DataExecutionPrevention />
      <TargetMachine>MachineX86</TargetMachine>"
					writeDataToFile
				end
				
				self.m_strDataToWrite = "    </Link>"
				writeDataToFile
			end
			
			# Build Events
			if self.m_kProjectProperties.m_bIsMainProject
				if strTarget.include? "Release"
					strFolderName = self.m_kProjectProperties.m_strOutputFileName + "_" + self.m_kProjectProperties.m_strVersionName + "_STAGE_" + self.m_kProjectProperties.m_strVersionCode
					
					self.m_strDataToWrite = "    <PostBuildEvent>
      <Command>#{BE3_SDK_DIR}BeBuildTools/Tools/Batch_Builds_AutoCopy.bat \"$(ConfigurationName)\" \"$(ProjectName).exe\" \"#{strFolderName}\"</Command>
    </PostBuildEvent>"
					writeDataToFile
				else
					self.m_strDataToWrite = "    <PreLinkEvent>
		<Command>$(SolutionDir)/Batch_Copy_External_PDB_Compile.bat</Command>
    </PreLinkEvent>"
					writeDataToFile
					self.m_strDataToWrite = "    <PostBuildEvent>
      <Command>#{BE3_SDK_DIR}BeBuildTools/Tools/Batch_DLLs_Debug_AutoCopy.bat \"$(ConfigurationName)\" \"$(ProjectName).exe\"</Command>
    </PostBuildEvent>"
					writeDataToFile
				end
			end
			if self.m_kProjectProperties.m_bIsExternalLibrary
				strTargetID = 0
				if strTarget.include? "Release" 
					strTargetID = 1
				end
				self.m_strDataToWrite = "    <PostBuildEvent>
      <Command>#{BE3_SDK_DIR}BeBuildTools/Tools/Batch_Copy_External_PDB.bat \"$(ConfigurationName)\" $(ProjectName) #{strTargetID}</Command>
    </PostBuildEvent>"
				writeDataToFile
			end
			
			self.m_strDataToWrite = "  </ItemDefinitionGroup>"
			writeDataToFile
		
		end
	  
		if self.m_kProjectProperties.m_bIsMainProject 
			strPreFolder = ""
		else
			strPreFolder = BE3_SDK_DIR_SCRIPTS
		end
	  
		# Header files
		self.m_strDataToWrite = "  <ItemGroup>"
		writeDataToFile
		projectWriteFilesItemGroup self.m_kProjectProperties, "ClInclude", strPreFolder + self.m_kProjectProperties.m_strProjectPath + self.m_kProjectProperties.m_strProjectName + "/", "src", [".h", ".hpp", ".inl"]
		projectWriteFilesItemGroup self.m_kProjectProperties, "ClInclude", strPreFolder + self.m_kProjectProperties.m_strProjectPath + self.m_kProjectProperties.m_strProjectName + "/", "Windows", [".h", ".hpp", ".inl"]
		projectWriteFilesItemGroup self.m_kProjectProperties, "ClInclude", strPreFolder + self.m_kProjectProperties.m_strProjectPath + self.m_kProjectProperties.m_strProjectName + "/", "Pch", [".h", ".hpp", ".inl"]
		projectWriteFilesItemGroup self.m_kProjectProperties, "None", strPreFolder + self.m_kProjectProperties.m_strProjectPath + self.m_kProjectProperties.m_strProjectName + "/", "MacOS", [".h", ".hpp", ".inl"]
		projectWriteFilesItemGroup self.m_kProjectProperties, "ClInclude", strPreFolder + self.m_kProjectProperties.m_strProjectPath + self.m_kProjectProperties.m_strProjectName + "/", "Desktop", [".h", ".hpp", ".inl"]
		projectWriteFilesItemGroup self.m_kProjectProperties, "None", strPreFolder + self.m_kProjectProperties.m_strProjectPath + self.m_kProjectProperties.m_strProjectName + "/", "Android", [".h", ".hpp", ".inl"]
		projectWriteFilesItemGroup self.m_kProjectProperties, "None", strPreFolder + self.m_kProjectProperties.m_strProjectPath + self.m_kProjectProperties.m_strProjectName + "/", "iOS", [".h", ".hpp", ".inl"]
		projectWriteFilesItemGroup self.m_kProjectProperties, "None", strPreFolder + self.m_kProjectProperties.m_strProjectPath + self.m_kProjectProperties.m_strProjectName + "/", "Mobile", [".h", ".hpp", ".inl"]
		if self.m_kProjectProperties.m_bIsExternalLibrary
			projectWriteFilesItemGroup self.m_kProjectProperties, "None", strPreFolder + self.m_kProjectProperties.m_strProjectPath + self.m_kProjectProperties.m_strProjectName + "/", "include", [".h", ".hpp", ".inl"]
			projectWriteFilesItemGroup self.m_kProjectProperties, "None", strPreFolder + self.m_kProjectProperties.m_strProjectPath + self.m_kProjectProperties.m_strProjectName + "/", "includeCode", [".h", ".hpp", ".inl"]
		end
		
		if not self.m_kProjectProperties.m_bIsMainProject and not self.m_kProjectProperties.m_bIs3rdPartyLib and not self.m_kProjectProperties.m_bIsExternalLibrary
			self.m_strDataToWrite = "    <ClInclude Include=\"../../BeCommon/BakEngine3Common.h\" />"
			writeDataToFile
		end
		self.m_strDataToWrite = "  </ItemGroup>"
		writeDataToFile

		# Source files
		if !avoidSourceFiles
			self.m_strDataToWrite = "  <ItemGroup>"
			writeDataToFile
			projectWriteFilesItemGroup self.m_kProjectProperties, "ClCompile", strPreFolder + self.m_kProjectProperties.m_strProjectPath + self.m_kProjectProperties.m_strProjectName + "/", "src", [".c", ".cpp"]
			projectWriteFilesItemGroup self.m_kProjectProperties, "ClCompile", strPreFolder + self.m_kProjectProperties.m_strProjectPath + self.m_kProjectProperties.m_strProjectName + "/", "Windows", [".c", ".cpp"]
			projectWriteFilesItemGroup self.m_kProjectProperties, "None", strPreFolder + self.m_kProjectProperties.m_strProjectPath + self.m_kProjectProperties.m_strProjectName + "/", "MacOS", [".c", ".cpp", ".m", ".mm", ".a"]
			projectWriteFilesItemGroup self.m_kProjectProperties, "ClCompile", strPreFolder + self.m_kProjectProperties.m_strProjectPath + self.m_kProjectProperties.m_strProjectName + "/", "Desktop", [".c", ".cpp"]
			projectWriteFilesItemGroup self.m_kProjectProperties, "None", strPreFolder + self.m_kProjectProperties.m_strProjectPath + self.m_kProjectProperties.m_strProjectName + "/", "Android", [".c", ".cpp", ".java", ".jar"]
			projectWriteFilesItemGroup self.m_kProjectProperties, "None", strPreFolder + self.m_kProjectProperties.m_strProjectPath + self.m_kProjectProperties.m_strProjectName + "/", "iOS", [".c", ".cpp", ".m", ".mm", ".a"]
			projectWriteFilesItemGroup self.m_kProjectProperties, "None", strPreFolder + self.m_kProjectProperties.m_strProjectPath + self.m_kProjectProperties.m_strProjectName + "/", "Mobile", [".c", ".cpp"]
			projectWriteFilesItemGroup self.m_kProjectProperties, "None", strPreFolder + self.m_kProjectProperties.m_strProjectPath + self.m_kProjectProperties.m_strProjectName + "/", "includeCode", [".c", ".cpp"]
			self.m_strDataToWrite = "  </ItemGroup>"
			writeDataToFile
		end
		
		# ProjectReferences
		for i in 0..1

			kCurrentProjectPropertiesConfigArray = nil
			strFolder3rdParty = ""
			if i == 0
				kCurrentProjectPropertiesArray = ProjectProperties::s_kProjectDependencies
			else
				kCurrentProjectPropertiesArray = ProjectProperties::s_kProject3rdPartyDependencies
				strFolder3rdParty = "../3rdParty/"
			end
		
			kCurrentProjectPropertiesArray.each do |strProjectNameGlobal, kDependenciesArray|
				if strProjectNameGlobal == strProjectName
					if kDependenciesArray.length > 0
						self.m_strDataToWrite = "  <ItemGroup>"
						writeDataToFile
						
						kDependenciesArray.each do |strProjectDependencyName|
						
							dependencyProjectProperties = GetProjectProperties strProjectDependencyName
							if dependencyProjectProperties != nil
								if dependencyProjectProperties.m_kProjectPlatformTargets == nil or (dependencyProjectProperties.m_kProjectPlatformTargets != nil and dependencyProjectProperties.m_kProjectPlatformTargets.include? PLATFORM_WINDOWS)
						
									if not self.m_kProjectProperties.m_bIsMainProject
										if self.m_kProjectProperties.m_bIs3rdPartyLib
											if i == 0
												self.m_strDataToWrite = "    <ProjectReference Include=\"../../../BeLibs/#{strProjectDependencyName}#{PROJECT_FILES_PATH}/#{strProjectDependencyName}.vcxproj\">
      <Project>{#{$g_kProjectGUIDs[strProjectDependencyName].downcase}}</Project>
      <ReferenceOutputAssembly>false</ReferenceOutputAssembly>
    </ProjectReference>"
											else
												self.m_strDataToWrite = "    <ProjectReference Include=\"../../#{strProjectDependencyName}#{PROJECT_FILES_PATH}/#{strProjectDependencyName}.vcxproj\">
      <Project>{#{$g_kProjectGUIDs[strProjectDependencyName].downcase}}</Project>
      <ReferenceOutputAssembly>false</ReferenceOutputAssembly>
    </ProjectReference>"
											end
										else
											self.m_strDataToWrite = "    <ProjectReference Include=\"../../#{strFolder3rdParty}#{strProjectDependencyName}#{PROJECT_FILES_PATH}/#{strProjectDependencyName}.vcxproj\">
      <Project>{#{$g_kProjectGUIDs[strProjectDependencyName].downcase}}</Project>
      <ReferenceOutputAssembly>false</ReferenceOutputAssembly>
    </ProjectReference>"
										end
										writeDataToFile
									else
										self.m_strDataToWrite = "    <ProjectReference Include=\"#{BE3_SDK_DIR}BeLibs/#{strProjectDependencyName}#{PROJECT_FILES_PATH}/#{strProjectDependencyName}.vcxproj\">
      <Project>{#{$g_kProjectGUIDs[strProjectDependencyName].downcase}}</Project>
      <ReferenceOutputAssembly>false</ReferenceOutputAssembly>
    </ProjectReference>"
										writeDataToFile
									end
								end
							end
						end
						
						self.m_strDataToWrite = "  </ItemGroup>"
						writeDataToFile
						
					end
				end
			end
		end
		
		#if self.m_kProjectProperties.m_bIsMainProject
		#	self.m_strDataToWrite = "  <ItemGroup>
#    <ResourceCompile Include=\"../windows/Resources/BakEngine3Demo.rc\" />
#  </ItemGroup>
#  <ItemGroup>
#    <Image Include=\"../windows/Resources/AppIcon.ico\" />
#  </ItemGroup>"
#			writeDataToFile		
#		end
		
		# Ending file
		self.m_strDataToWrite = "  <Import Project=\"$(VCTargetsPath)\\Microsoft.Cpp.targets\" />
  <ImportGroup Label=\"ExtensionTargets\">
  </ImportGroup>
</Project>"
		writeDataToFile

		if m_bIsFileCreationIntoMemory
			checkFileDifferencesAndSave strProjectPathFile, false
		else
			self.m_outputFile.close
		end
		
		
		
		# FILTERS FILE
		
		strProjectFile = strProjectName + ".vcxproj.filters"
		if self.m_kProjectProperties.m_bIsMainProject
			strProjectPathFile = strProjectPath + "/" + strProjectName + PROJECT_FILES_PATH + "/" + self.m_kProjectProperties.m_strOutputFileName + ".vcxproj.filters"
		else
			strProjectPathFile = strProjectPath + "/" + strProjectName + PROJECT_FILES_PATH + "/" + strProjectName + ".vcxproj.filters"
		end

		if m_bIsFileCreationIntoMemory
			self.m_strFileMemoryBuffer = [239, 187, 191].pack('C*')
		else
			self.m_outputFile = File.new(strProjectPathFile, "w")
			
			# utf8 Solution Header;
			self.m_outputFile.write [239, 187, 191].pack('C*')
		end

		# Project XML header
		self.m_strDataToWrite = "<?xml version=\"1.0\" encoding=\"utf-8\"?>
<Project ToolsVersion=\"4.0\" xmlns=\"http://schemas.microsoft.com/developer/msbuild/2003\">"
		writeDataToFile
						
		self.m_strDataToWrite = "  <ItemGroup>"
		writeDataToFile
		
		if self.m_kProjectProperties.m_bIsExternalLibrary
			# Include filter
			self.m_strDataToWrite = "    <Filter Include=\"include\">
      <UniqueIdentifier>{#{generateGUID(strProjectName, "filters", "includeGUID").downcase}}</UniqueIdentifier>
    </Filter>"
			writeDataToFile
			projectWriteFilesIntoFilters self.m_kProjectProperties, "None", strPreFolder + self.m_kProjectProperties.m_strProjectPath + self.m_kProjectProperties.m_strProjectName + "/", "include", [".h", ".hpp", ".inl"], true, "include", "include"
	
			# IncludeCode filter
			self.m_strDataToWrite = "    <Filter Include=\"includeCode\">
      <UniqueIdentifier>{#{generateGUID(strProjectName, "filters", "incCodeGUID").downcase}}</UniqueIdentifier>
    </Filter>"
			writeDataToFile
			projectWriteFilesIntoFilters self.m_kProjectProperties, "None", strPreFolder + self.m_kProjectProperties.m_strProjectPath + self.m_kProjectProperties.m_strProjectName + "/", "includeCode", [".h", ".hpp", ".inl", ".c", ".cpp"], true, "includeCode", "includeCode"
		end
		
		# Pch filter
		self.m_strDataToWrite = "    <Filter Include=\"Pch\">
      <UniqueIdentifier>{#{generateGUID(strProjectName, "filters", "PchGUID").downcase}}</UniqueIdentifier>
    </Filter>"
		writeDataToFile

		# Sources filter
		self.m_strDataToWrite = "    <Filter Include=\"src\">
      <UniqueIdentifier>{#{generateGUID(strProjectName, "filters", "srcGUID").downcase}}</UniqueIdentifier>
    </Filter>"
		writeDataToFile
		
		# Add headers filters (or dirs)
		projectWriteFilesIntoFilters self.m_kProjectProperties, "ClInclude", strPreFolder + self.m_kProjectProperties.m_strProjectPath + self.m_kProjectProperties.m_strProjectName + "/", "src", [".h", ".hpp", ".inl"], true, "src", "src"
		
		# Sources windows filter
		self.m_strDataToWrite = "    <Filter Include=\"windows\">
      <UniqueIdentifier>{#{generateGUID(strProjectName, "filters", "windowsGUID").downcase}}</UniqueIdentifier>
    </Filter>"
		writeDataToFile
		
		# Add headers filters (or dirs)
		projectWriteFilesIntoFilters self.m_kProjectProperties, "ClInclude", strPreFolder + self.m_kProjectProperties.m_strProjectPath + self.m_kProjectProperties.m_strProjectName + "/", "Windows", [".h", ".hpp", ".inl"], true, "Windows", "Windows"
		
		# MacOS filter
		self.m_strDataToWrite = "    <Filter Include=\"MacOS\">
      <UniqueIdentifier>{#{generateGUID(strProjectName, "filters", "MacOSGUID").downcase}}</UniqueIdentifier>
    </Filter>"
		writeDataToFile
		
		# Add headers filters (or dirs)
		projectWriteFilesIntoFilters self.m_kProjectProperties, "None", strPreFolder + self.m_kProjectProperties.m_strProjectPath + self.m_kProjectProperties.m_strProjectName + "/", "MacOS", [".h", ".hpp", ".inl"], true, "MacOS", "MacOS"
		
		# Desktop filter
		self.m_strDataToWrite = "    <Filter Include=\"Desktop\">
      <UniqueIdentifier>{#{generateGUID(strProjectName, "filters", "DesktopGUID").downcase}}</UniqueIdentifier>
    </Filter>"
		writeDataToFile
		
		# Add headers filters (or dirs)
		projectWriteFilesIntoFilters self.m_kProjectProperties, "ClInclude", strPreFolder + self.m_kProjectProperties.m_strProjectPath + self.m_kProjectProperties.m_strProjectName + "/", "Desktop", [".h", ".hpp", ".inl"], true, "Desktop", "Desktop"
		
		# Android filter
		self.m_strDataToWrite = "    <Filter Include=\"Android\">
      <UniqueIdentifier>{#{generateGUID(strProjectName, "filters", "AndroidGUID").downcase}}</UniqueIdentifier>
    </Filter>"
		writeDataToFile
		
		# Add headers filters (or dirs)
		projectWriteFilesIntoFilters self.m_kProjectProperties, "None", strPreFolder + self.m_kProjectProperties.m_strProjectPath + self.m_kProjectProperties.m_strProjectName + "/", "Android", [".h", ".hpp", ".inl"], true, "Android", "Android"
		
		# Mobile filter
		self.m_strDataToWrite = "    <Filter Include=\"Mobile\">
      <UniqueIdentifier>{#{generateGUID(strProjectName, "filters", "MobileGUID").downcase}}</UniqueIdentifier>
    </Filter>"
		writeDataToFile
		
		# Add headers filters (or dirs)
		projectWriteFilesIntoFilters self.m_kProjectProperties, "None", strPreFolder + self.m_kProjectProperties.m_strProjectPath + self.m_kProjectProperties.m_strProjectName + "/", "Mobile", [".h", ".hpp", ".inl"], true, "Mobile", "Mobile"
		
		# iOS filter
		self.m_strDataToWrite = "    <Filter Include=\"iOS\">
      <UniqueIdentifier>{#{generateGUID(strProjectName, "filters", "iOSGUID").downcase}}</UniqueIdentifier>
    </Filter>"
		writeDataToFile
		
		# Add headers filters (or dirs)
		projectWriteFilesIntoFilters self.m_kProjectProperties, "None", strPreFolder + self.m_kProjectProperties.m_strProjectPath + self.m_kProjectProperties.m_strProjectName + "/", "iOS", [".h", ".hpp", ".inl"], true, "iOS", "iOS"

		self.m_strDataToWrite = "  </ItemGroup>"
		writeDataToFile
		
		# PCH FILES INTO FILTERS
		self.m_strDataToWrite = "  <ItemGroup>"
		writeDataToFile
		projectWriteFilesIntoFilters self.m_kProjectProperties, "ClInclude", strPreFolder + self.m_kProjectProperties.m_strProjectPath + self.m_kProjectProperties.m_strProjectName + "/", "Pch", [".h", ".hpp", ".inl"], false, "Pch", "src"
		self.m_strDataToWrite = "  </ItemGroup>"
		writeDataToFile
		
		# HEADER FILES INTO FILTERS
		self.m_strDataToWrite = "  <ItemGroup>"
		writeDataToFile
		projectWriteFilesIntoFilters self.m_kProjectProperties, "ClInclude", strPreFolder + self.m_kProjectProperties.m_strProjectPath + self.m_kProjectProperties.m_strProjectName + "/", "src", [".h", ".hpp", ".inl"], false, "src", "src"
		projectWriteFilesIntoFilters self.m_kProjectProperties, "ClInclude", strPreFolder + self.m_kProjectProperties.m_strProjectPath + self.m_kProjectProperties.m_strProjectName + "/", "Windows", [".h", ".hpp", ".inl"], false, "Windows", "Windows"
		projectWriteFilesIntoFilters self.m_kProjectProperties, "None", strPreFolder + self.m_kProjectProperties.m_strProjectPath + self.m_kProjectProperties.m_strProjectName + "/", "MacOS", [".h", ".hpp", ".inl"], false, "MacOS", "MacOS"
		projectWriteFilesIntoFilters self.m_kProjectProperties, "ClInclude", strPreFolder + self.m_kProjectProperties.m_strProjectPath + self.m_kProjectProperties.m_strProjectName + "/", "Desktop", [".h", ".hpp", ".inl"], false, "Desktop", "Desktop"
		projectWriteFilesIntoFilters self.m_kProjectProperties, "None", strPreFolder + self.m_kProjectProperties.m_strProjectPath + self.m_kProjectProperties.m_strProjectName + "/", "Android", [".h", ".hpp", ".inl"], false, "Android", "Android"
		projectWriteFilesIntoFilters self.m_kProjectProperties, "None", strPreFolder + self.m_kProjectProperties.m_strProjectPath + self.m_kProjectProperties.m_strProjectName + "/", "iOS", [".h", ".hpp", ".inl"], false, "iOS", "iOS"
		projectWriteFilesIntoFilters self.m_kProjectProperties, "None", strPreFolder + self.m_kProjectProperties.m_strProjectPath + self.m_kProjectProperties.m_strProjectName + "/", "Mobile", [".h", ".hpp", ".inl"], false, "Mobile", "Mobile"
		if self.m_kProjectProperties.m_bIsExternalLibrary
			projectWriteFilesIntoFilters self.m_kProjectProperties, "None", strPreFolder + self.m_kProjectProperties.m_strProjectPath + self.m_kProjectProperties.m_strProjectName + "/", "include", [".h", ".hpp", ".inl"], false, "include", "include"
			projectWriteFilesIntoFilters self.m_kProjectProperties, "None", strPreFolder + self.m_kProjectProperties.m_strProjectPath + self.m_kProjectProperties.m_strProjectName + "/", "includeCode", [".h", ".hpp", ".inl"], false, "includeCode", "includeCode"
		end
		self.m_strDataToWrite = "  </ItemGroup>"
		writeDataToFile
		
		# SOURCE FILES INTO FILTERS
		self.m_strDataToWrite = "  <ItemGroup>"
		writeDataToFile
		projectWriteFilesIntoFilters self.m_kProjectProperties, "ClCompile", strPreFolder + self.m_kProjectProperties.m_strProjectPath + self.m_kProjectProperties.m_strProjectName + "/", "src", [".c", ".cpp"], false, "src", "src"
		projectWriteFilesIntoFilters self.m_kProjectProperties, "ClCompile", strPreFolder + self.m_kProjectProperties.m_strProjectPath + self.m_kProjectProperties.m_strProjectName + "/", "Windows", [".c", ".cpp"], false, "Windows", "Windows"
		projectWriteFilesIntoFilters self.m_kProjectProperties, "None", strPreFolder + self.m_kProjectProperties.m_strProjectPath + self.m_kProjectProperties.m_strProjectName + "/", "MacOS", [".c", ".cpp", ".m", ".mm", ".a"], false, "MacOS", "MacOS"
		projectWriteFilesIntoFilters self.m_kProjectProperties, "ClCompile", strPreFolder + self.m_kProjectProperties.m_strProjectPath + self.m_kProjectProperties.m_strProjectName + "/", "Desktop", [".c", ".cpp"], false, "Desktop", "Desktop"
		projectWriteFilesIntoFilters self.m_kProjectProperties, "None", strPreFolder + self.m_kProjectProperties.m_strProjectPath + self.m_kProjectProperties.m_strProjectName + "/", "Android", [".c", ".cpp", ".java", ".jar"], false, "Android", "Android"
		projectWriteFilesIntoFilters self.m_kProjectProperties, "None", strPreFolder + self.m_kProjectProperties.m_strProjectPath + self.m_kProjectProperties.m_strProjectName + "/", "iOS", [".c", ".cpp", ".m", ".mm", ".a"], false, "iOS", "iOS"
		projectWriteFilesIntoFilters self.m_kProjectProperties, "None", strPreFolder + self.m_kProjectProperties.m_strProjectPath + self.m_kProjectProperties.m_strProjectName + "/", "Mobile", [".c", ".cpp"], false, "Mobile", "Mobile"
		if self.m_kProjectProperties.m_bIsExternalLibrary
			projectWriteFilesIntoFilters self.m_kProjectProperties, "None", strPreFolder + self.m_kProjectProperties.m_strProjectPath + self.m_kProjectProperties.m_strProjectName + "/", "includeCode", [".c", ".cpp"], false, "includeCode", "includeCode"
		end
		self.m_strDataToWrite = "  </ItemGroup>"
		writeDataToFile
    		
		self.m_strDataToWrite = "</Project>"
		writeDataToFile
		
		if m_bIsFileCreationIntoMemory
			checkFileDifferencesAndSave strProjectPathFile, false
		else
			self.m_outputFile.close
		end
		
	end

	# Generates an unique GUID
	private
	def generateGUIDRandom
		strNewGUID = ""
		
		bValidGUI = false
		while bValidGUI == false do
			strNewGUID = ""

			kCounters = [8, 4, 4, 4, 12]
			for i in 0..(kCounters.length-1)
				for j in 0..(kCounters[i]-1)
					strNewGUID += rand(16).to_s(16).upcase
				end
				if i != (kCounters.length-1)
					strNewGUID += '-'
				end
			end

			bValidGUI = true
			$g_kProjectGUIDs.each do |strKey|
				if $g_kProjectGUIDs[strKey] == strNewGUID
					bValidGUI = false
					break
				end
			end
		end

		return strNewGUID
	end
	
	# Generates an unique GUID
	private
	def generateGUID(strFilename, strFilenameExt, strSpecificWord)
		strNewGUID = ""
		
		intValidGUIDOffset = 0
		
		bValidGUI = false
		while bValidGUI == false do
			strNewGUID = ""

			kCounters = [8, 4, 4, 4, 12]
			for i in 0..(kCounters.length-1)
				for j in 0..(kCounters[i]-1)
					intTemporaryChar = strFilename[strNewGUID.size % strFilename.size].ord
					intTemporaryChar += strFilenameExt[strNewGUID.size % strFilenameExt.size].ord
					intTemporaryChar += strSpecificWord[strNewGUID.size % strSpecificWord.size].ord
					intTemporaryChar += intValidGUIDOffset

					strNewGUID += (intTemporaryChar % 16).to_s(16).upcase
				end
				if i != (kCounters.length-1)
					strNewGUID += '-'
				end
			end

			bValidGUI = true
			$g_kProjectGUIDs.each do |strKey|
				if $g_kProjectGUIDs[strKey] == strNewGUID
					intValidGUIDOffset = intValidGUIDOffset + 1
					bValidGUI = false
					break
				end
			end
		end

		return strNewGUID
	end

	# Regenerates the VS project file
	public
	def regenProject(kProjectProperties, strCleanCommand)
	
		#if not kProjectProperties.m_bIsExternalLibrary
			$g_kProjectTargets = ["DebugStatic", "ReleaseStatic"]
		#else
			#$g_kProjectTargets = ["ReleaseStatic"]
		#end
	
		if kProjectProperties.m_bIsMainProject
			strProjectsFolderPath = kProjectProperties.m_strProjectPath + kProjectProperties.m_strProjectName + PROJECT_FILES_PATH
			strProjectOBJsFolderPath = kProjectProperties.m_strProjectPath + kProjectProperties.m_strProjectName + "/obj"
		else
			strProjectsFolderPath = BE3_SDK_DIR_SCRIPTS + kProjectProperties.m_strProjectPath + kProjectProperties.m_strProjectName + PROJECT_FILES_PATH
			strProjectOBJsFolderPath = BE3_SDK_DIR_SCRIPTS + kProjectProperties.m_strProjectPath + kProjectProperties.m_strProjectName + "/obj"
		end
		
		if strCleanCommand == "clean"
		
			if Dir.exists? strProjectsFolderPath
				puts "Cleaning #{kProjectProperties.m_strProjectName} \"projects\" directory ..."
				
				require 'fileutils'
				FileUtils.rm_rf strProjectsFolderPath
			end
			
			if Dir.exists? strProjectOBJsFolderPath
				puts "Cleaning #{kProjectProperties.m_strProjectName} \"obj\" directory ..."
				
				require 'fileutils'
				FileUtils.rm_rf strProjectOBJsFolderPath
			end
			
			if Dir.exists? "obj"
				puts "Cleaning \"obj\" directory ..."
				
				require 'fileutils'
				FileUtils.rm_rf 'obj'
			end
			
			if Dir.exists? "bin"
				puts "Cleaning \"bin\" directory ..."
				
				require 'fileutils'
				FileUtils.rm_rf 'bin'
			end
			
			if kProjectProperties.m_bIsExternalLibrary
				if Dir.exists? "lib"
					puts "Cleaning \"lib\" directory ..."
					
					require 'fileutils'
					FileUtils.rm_rf 'lib'
				end
			end
			
			if kProjectProperties.m_bIsMainProject or kProjectProperties.m_bIsExternalLibrary
				generateBatchFiles kProjectProperties, strCleanCommand
			end
			
			return
			
		end

		if not Dir.exists? strProjectsFolderPath
			Dir.mkdir strProjectsFolderPath
		end
	
		self.m_kProjectProperties = kProjectProperties
		if kProjectProperties.m_bIsMainProject
			strProjectPath = kProjectProperties.m_strProjectPath
			strProjectPathFromMainProject = strProjectPath
		else
			strProjectPath = BE3_SDK_DIR_SCRIPTS + kProjectProperties.m_strProjectPath
			strProjectPathFromMainProject = BE3_SDK_DIR + kProjectProperties.m_strProjectPath
		end
		strProjectName = kProjectProperties.m_strProjectName
		strVSResourcesFolder = kProjectProperties.m_strVSResourcesFolder
		avoidSourceFiles = kProjectProperties.m_avoidLibrariesSourceFiles
		self.m_strIncludeFolderName = kProjectProperties.m_strIncludeFolderName

		strProjectGUID = generateGUID strProjectName, "vcxproj", "VsProjectGUID"
		
		$g_kProjectGUIDs[strProjectName] = strProjectGUID
		$g_kProjectPaths[strProjectName] = strProjectPath + strProjectName
		$g_kProjectPathsFromMainProject[strProjectName] = strProjectPathFromMainProject + strProjectName

		projectGenerateFile strProjectPath, strProjectName, strProjectGUID, strVSResourcesFolder, avoidSourceFiles
		
		if kProjectProperties.m_bIsMainProject or kProjectProperties.m_bIsExternalLibrary
			generateBatchFiles kProjectProperties, strCleanCommand
		end

	end

	# Writes the VS project section to solution file
	private
	def writeProjectSection(kDependenciesArray)

		self.m_strDataToWrite = "\tProjectSection(ProjectDependencies) = postProject"
		writeDataToFile
		
		kDependenciesArray.each do |strProjectName|
			self.m_strDataToWrite = "\t\t{#{$g_kProjectGUIDs[strProjectName]}} = {#{$g_kProjectGUIDs[strProjectName]}}"
			writeDataToFile
		end

		self.m_strDataToWrite = "\tEndProjectSection"
		writeDataToFile

	end

	# Writes the VS project to solution file
	private
	def writeProjectToSolution(strProjectName, strCommonProjectGUID, strForceProjectName)
		
		if strForceProjectName != nil
			strProjectNameToUse = strForceProjectName
		else
			strProjectNameToUse = strProjectName
		end
		
		self.m_strDataToWrite = "Project(\"{#{strCommonProjectGUID}}\") = \"#{strProjectNameToUse}\", \"#{$g_kProjectPathsFromMainProject[strProjectName]}#{PROJECT_FILES_PATH}/#{strProjectNameToUse}.vcxproj\", \"{#{$g_kProjectGUIDs[strProjectName]}}\""
		writeDataToFile
		self.m_strDataToWrite = "EndProject"
		writeDataToFile

	end

	# Writes the VS global section of the solution file
	private
	def writeGlobalSectionToSolution
		
		self.m_strDataToWrite = "Global
	GlobalSection(SolutionConfigurationPlatforms) = preSolution"
		writeDataToFile
		$g_kProjectTargets.each do |strTarget|
			self.m_strDataToWrite = "		#{strTarget}|Win32 = #{strTarget}|Win32"
			writeDataToFile
		end
		self.m_strDataToWrite = "	EndGlobalSection
	GlobalSection(ProjectConfigurationPlatforms) = postSolution"
		writeDataToFile

		ProjectProperties::s_kProjectDependencies.each do |strProjectName, kDependenciesArray|
			$g_kProjectTargets.each do |strTarget|
				self.m_strDataToWrite = "\t\t{#{$g_kProjectGUIDs[strProjectName]}}.#{strTarget}|Win32.ActiveCfg = #{strTarget}|Win32"
				writeDataToFile
				self.m_strDataToWrite = "\t\t{#{$g_kProjectGUIDs[strProjectName]}}.#{strTarget}|Win32.Build.0 = #{strTarget}|Win32"
				writeDataToFile
			end
		end

		self.m_strDataToWrite = "\tEndGlobalSection
	GlobalSection(SolutionProperties) = preSolution
		HideSolutionNode = FALSE
	EndGlobalSection
EndGlobal
"
	writeDataToFile

	end

	# Regenerates the VS solution file
	public
	def regenSolution(kProjectProperties, strCleanCommand)
	
		self.m_kProjectProperties = kProjectProperties
		
		if kProjectProperties.m_bIsExternalLibrary
			strProjectsFolderPath = "." + PROJECT_FILES_PATH
		else
			strProjectsFolderPath = kProjectProperties.m_strProjectPath + kProjectProperties.m_strProjectName + PROJECT_FILES_PATH
		end
	
		if strCleanCommand == "clean"
		
			if Dir.exists? strProjectsFolderPath
				puts "Cleaning #{kProjectProperties.m_strProjectName} \"projects\" directory ..."
				
				require 'fileutils'
				FileUtils.rm_rf strProjectsFolderPath
			end
			
			if Dir.exists? "obj"
				puts "Cleaning \"obj\" directory ..."
				
				require 'fileutils'
				FileUtils.rm_rf 'obj'
			end
			
			if Dir.exists? "bin"
				puts "Cleaning \"bin\" directory ..."
				
				require 'fileutils'
				FileUtils.rm_rf 'bin'
			end
			
			return
			
		end
			
		if not Dir.exists? strProjectsFolderPath
			Dir.mkdir strProjectsFolderPath
		end
	
		strProjectPath = kProjectProperties.m_strProjectPath
		strProjectName = kProjectProperties.m_strProjectName
			
		if kProjectProperties.m_bIsMainProject
			puts "Building #{$g_kPlatformNames[PLATFORM_WINDOWS]} Visual Studio - " + kProjectProperties.m_strOutputFileName + " solution..."
		else
			puts "Building #{$g_kPlatformNames[PLATFORM_WINDOWS]} Visual Studio - " + strProjectName + " solution..."
		end

		strProjectGUID = generateGUID strProjectName, "sln", "solutionGUID"

		strSolutionFile = strProjectName + ".sln"
		if kProjectProperties.m_bIsExternalLibrary
			strSolutionPathFile = "." + PROJECT_FILES_PATH + "/" + strProjectName + ".sln"
		else
			kProjectProperties = GetProjectProperties strProjectName
			if kProjectProperties.m_bIsMainProject
				strSolutionPathFile = "." + PROJECT_FILES_PATH + "/" + kProjectProperties.m_strOutputFileName + ".sln"
			end
		end

		if m_bIsFileCreationIntoMemory
			self.m_strFileMemoryBuffer = [239, 187, 191].pack('C*')
		else
			self.m_outputFile = File.new(strSolutionPathFile, "w")
			
			# utf8 Solution Header;
			self.m_outputFile.write [239, 187, 191].pack('C*')
		end

		self.m_strDataToWrite = "Microsoft Visual Studio Solution File, Format Version 12.00
\# Visual Studio"

		writeDataToFile

		$g_kProjectProperties.reverse_each do |kDependencyProperties|
			
			bFound = false
			ProjectProperties::s_kProjectDependencies.each do |strProjectName, kDependenciesArray|
				if kDependenciesArray.include? kDependencyProperties.m_strProjectName
					bFound = true
					break
				end
			end
			
			if bFound or kDependencyProperties.m_bIsMainProject or kProjectProperties.m_bIsExternalLibrary
				if kDependencyProperties.m_bIsMainProject
					writeProjectToSolution kDependencyProperties.m_strProjectName, strProjectGUID, kDependencyProperties.m_strOutputFileName
				else
					writeProjectToSolution kDependencyProperties.m_strProjectName, strProjectGUID, nil
				end
			end			
			
		end
		
		$g_k3rdPartyLibsProperties.reverse_each do |kDependencyProperties|
			
			bFound = false
			ProjectProperties::s_kProject3rdPartyDependencies.each do |strProjectName, kDependenciesArray|
				if kDependenciesArray.include? kDependencyProperties.m_strProjectName
					bFound = true
					break
				end
			end
			
			if bFound
				writeProjectToSolution kDependencyProperties.m_strProjectName, strProjectGUID, nil
			end			
			
		end

		writeGlobalSectionToSolution

		if m_bIsFileCreationIntoMemory
			checkFileDifferencesAndSave strSolutionPathFile, $g_bForceSolutionRegeneration
		else
			self.m_outputFile.close
		end

	end

end
