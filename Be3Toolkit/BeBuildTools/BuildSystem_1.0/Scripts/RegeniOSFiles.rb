####################################################################
##
## BAK ENGINE 3
## BUILD SYSTEM
##
## RegeniOSFiles.rb
## This ruby file generates the Xcode iOS project files
##
###################################################################

#!/bin/sh

ALL_DEPENDENCIES_IN_MAIN_PROJECT = true

# Xcode sufix
$g_strXcodeFileSufix = "_IOS"

# Xcode BakEngine needed frameworks
$g_kBakEngineNeededFrameworks = [
								"Accounts.framework",
								"AdSupport.framework",
								"AudioToolbox.framework",
								"AVFoundation.framework",
								"CFNetwork.framework",
								"CoreGraphics.framework",
                                "CoreMedia.framework",
								"CoreMotion.framework",
								"CoreTelephony.framework",
								"CoreText.framework",
                                "EventKit.framework",
								"Foundation.framework",
								"GameKit.framework",
								"libsqlite3.0.dylib",
								"libz.dylib",
								"MediaPlayer.framework",
								"MessageUI.framework",
								"MobileCoreServices.framework",
								"OpenAL.framework",
								"OpenGLES.framework",
								"QuartzCore.framework",
								"Security.framework",
								"Social.framework",
								"StoreKit.framework",
								"SystemConfiguration.framework",
                                "Twitter.framework",
								"UIKit.framework",
								]
							
# Xcode Project needed frameworks
$g_kProjectNeededFrameworks = []

# GeneratedBluePrints - Stores all the auto-generated blueprints until now
$g_kGeneratedBluePrints = Array.new

# ProjectBluePrints - Stores the auto-generated blueprints for Xcode projects
$g_kProjectBluePrints = Hash.new { |hash, key| }

# FilesData - Stores the auto-generated data for each project file
$g_kFilesData = Hash.new{|h, k| h[k] = []}

# GroupBluePrints - Stores the auto-generated blueprints for Xcode Groups
$g_kGroupBluePrints = Hash.new { |hash, key| }

# PBXContainerItemProxy - Stores the auto-generated data for each container item proxy
$g_kPBXContainerItemProxy = Hash.new{|h, k| h[k] = []}

# PBXNativeTarget - Stores the auto-generated data for each native target
$g_kPBXNativeTarget = Hash.new{|h, k| h[k] = []}

# PBXProjectSectionBluePrints - Stores the auto-generated data for each project section
$g_kPBXProjectSectionBluePrints = Hash.new { |hash, key| }

# BuildConfigurationBluePrints - Stores the auto-generated data for each build configuration
$g_kBuildConfigurationBluePrints = Hash.new { |hash, key| }

# g_kAssetsBluePrints
$g_kAssetsBluePrints = Array.new
$g_kIOSResBluePrint = nil
$g_kIOSVariantBluePrint = nil

# g_kProjectSpecialLibraries - Stores the special libraries
$g_kProjectSpecialLibraries = Hash.new{|h, k| h[k] = []}

# FiltersAndFiles - Stores all the filters and files and the data for each
$g_kFiltersAndFiles = Hash.new{|h, k| h[k] = []}
$g_kFiltersAndFilesResources = Hash.new{|h, k| h[k] = []}
$g_kFiltersAndFilesVariants = Hash.new{|h, k| h[k] = []}
$g_kCodeFolderFilterBluePrint = nil
$g_kIOSFolderFilterBluePrint = nil
$g_kPchFolderFilterBluePrint = nil

MAP_EXT = Hash.new { |hash, key| }
MAP_EXT['h'] = "sourcecode.c.h"
MAP_EXT['hh'] = "sourcecode.cpp.h"
MAP_EXT['inl'] = "sourcecode.cpp.h"
MAP_EXT['hpp'] = "sourcecode.cpp.h"
MAP_EXT['c'] = "sourcecode.c.c"
MAP_EXT['m'] = "sourcecode.c.objc"
MAP_EXT['mm'] = "sourcecode.cpp.objcpp"
MAP_EXT['cc'] = "sourcecode.cpp.cpp"
MAP_EXT['cpp'] = "sourcecode.cpp.cpp"
MAP_EXT['C'] = "sourcecode.cpp.cpp"
MAP_EXT['cxx'] = "sourcecode.cpp.cpp"
MAP_EXT['c++'] = "sourcecode.cpp.cpp"
MAP_EXT['l'] = "sourcecode.lex"
MAP_EXT['ll'] = "sourcecode.lex"
MAP_EXT['y'] = "sourcecode.yacc"
MAP_EXT['yy'] = "sourcecode.yacc"
MAP_EXT['plist'] = "text.plist.xml"
MAP_EXT['nib'] = "wrapper.nib"
MAP_EXT['xib'] = "text.xib"

RES_EXT = Hash.new { |hash, key| }
RES_EXT['png'] = "image.png"
RES_EXT['strings'] = "text.plist.strings"
RES_EXT['xib'] = "file.xib"
RES_EXT['plist'] = "text.plist.xml"
RES_EXT['bundle'] = "wrapper.plug-in"

MAP_EXT_SPECIAL = Hash.new { |hash, key| }
MAP_EXT_SPECIAL['a'] = "archive.ar"

COMPILABLE_EXT = ["c", "m", "mm", "cc", "cpp", "C", "cxx", "c++"]
LINK_EXT = ["a"]

$g_iNextBluePrint = 0
$g_strParallelizeBuilds = "NO"

# RegenXcodeIOSFiles Class
class RegenXcodeIOSFiles

	attr_accessor :m_outputFile
	attr_accessor :m_strDataToWrite
	attr_accessor :m_strIncludeFolderName
	attr_accessor :m_kProjectProperties
	attr_accessor :m_strRootBluePrint
	attr_accessor :m_bIsFileCreationIntoMemory
	attr_accessor :m_strFileMemoryBuffer

	def initialize()
		self.m_outputFile = nil
		self.m_strDataToWrite = ""
		self.m_strIncludeFolderName = ""
		self.m_kProjectProperties = nil
		self.m_strRootBluePrint = ""
		self.m_bIsFileCreationIntoMemory = true
		self.m_strFileMemoryBuffer = ""	
	end
		
	# Writes m_strDataToWrite content to m_outputFile and clean it
	private
	def writeDataToFile
		if m_bIsFileCreationIntoMemory
			self.m_strFileMemoryBuffer = self.m_strFileMemoryBuffer + (self.m_strDataToWrite + "\n").force_encoding('utf-8')
		else
			self.m_outputFile.puts self.m_strDataToWrite.force_encoding('utf-8')
		end
		
		self.m_strDataToWrite = ""
	end
	
	# Checks for file differences and override it if they exist
	private
	def checkFileDifferencesAndSave(strPathFile, strDestroyProjectFolder)
		
		bNeedToSaveToFile = false
		
		if File.exists? strPathFile
			kPreviousFile = File.open(strPathFile, "r")
			strPreviousFileContent = kPreviousFile.read
			kPreviousFile.close
			
			if self.m_strFileMemoryBuffer.size != strPreviousFileContent.size
				bNeedToSaveToFile = true
			else
				bIsEqual = true

				for i in 0..(self.m_strFileMemoryBuffer.size)
					if self.m_strFileMemoryBuffer[i] != strPreviousFileContent[i]
						bIsEqual = false
					end
				end
				
				if not bIsEqual
					bNeedToSaveToFile = true
				end
			end
		else
			bNeedToSaveToFile = true
		end
		
		if bNeedToSaveToFile
			if strDestroyProjectFolder.size > 0
				if File.exists? strDestroyProjectFolder
					require 'fileutils'
					FileUtils.rm_rf strDestroyProjectFolder
				
					Dir.mkdir(strDestroyProjectFolder)
				end
			end

			self.m_outputFile = File.new(strPathFile, "w")
			self.m_outputFile.write self.m_strFileMemoryBuffer
			self.m_outputFile.close
			
			self.m_strFileMemoryBuffer = ""
		end
		
	end
	
	# Erases a line from a String
	private
	def eraseLineFromString(strOriginalString, strToErase)
		iIndex = strOriginalString.index(strToErase)
		if iIndex != nil
			iSpaces = 2
			if OS::mac?
				iSpaces = 2
			end
			iIndex = iIndex - iSpaces
			iIndexFinish = iIndex + iSpaces + strToErase.length + 5
		
			strNewString = strOriginalString[0..iIndex]
			strNewString += "\n\t\t\t\t"
			strNewString += strOriginalString[iIndexFinish..strOriginalString.length]
		
			return strNewString
		end
		return strOriginalString
	end
	
	# Gets the final putput name for the main library
	public
	def GetMinLIBOutputName strProjectName
		strMainLIBOutputFile = nil
		for kProjProp in $g_kProjectProperties
			if kProjProp.m_strProjectName == $g_kFinalLibraryName
				if kProjProp.m_strOutputFileName != nil
					strMainLIBOutputFile = kProjProp.m_strOutputFileName
				else
					strMainLIBOutputFile = kProjProp.m_strProjectName
				end
				break
			end
		end
		return strMainLIBOutputFile
	end
	
	# Generates an unique BluePrint
	private
	def generateBluePrintRandom
		strNewBluePrint = ""
		
		bValidUUI = false
		while bValidUUI == false do
			strNewBluePrint = ""
			for j in 0..23
				strNewBluePrint += rand(16).to_s(16).upcase
			end
			
			if not $g_kGeneratedBluePrints.include? strNewBluePrint
				bValidUUI = true
			end
		end

		$g_kGeneratedBluePrints.push strNewBluePrint

		return strNewBluePrint
	end
	
	# Generates an unique BluePrint
	private
	def generateBluePrint(strFilename, strFilenameExt, strSpecificWord)
		strNewBluePrint = ""
		
		$g_iNextBluePrint = $g_iNextBluePrint + 1
		
		strTemp = $g_iNextBluePrint.to_s(16)
		
		strNewBluePrint = '0' * (24-strTemp.length)
		strNewBluePrint = strNewBluePrint + strTemp
	end
	
	# Generates an unique BluePrint
	private
	def generateBluePrint2(strFilename, strFilenameExt, strSpecificWord)# Generates an unique BluePrint
		strNewBluePrint = ""
		
		# sample: 90B08511 F4F4E96E 505D8B19
		#puts strFilename
		#puts strFilenameExt
		#puts strSpecificWord
		
		intValidGUIDOffset = 0
		
		bValidUUI = false
		while bValidUUI == false do
			strNewBluePrint = ""
			for j in 0..7
				intTemporaryChar = strFilename[strNewBluePrint.size % strFilename.size].ord
				intTemporaryChar += intValidGUIDOffset

				strNewBluePrint += (intTemporaryChar % 16).to_s(16).upcase
			end
			for j in 8..15
				if strFilenameExt != nil
					intTemporaryChar += strFilenameExt[strNewBluePrint.size % strFilenameExt.size].ord
					intTemporaryChar += intValidGUIDOffset
				end

				strNewBluePrint += (intTemporaryChar % 16).to_s(16).upcase
			end
			for j in 16..23
				intTemporaryChar += strSpecificWord[strNewBluePrint.size % strSpecificWord.size].ord
				intTemporaryChar += intValidGUIDOffset

				strNewBluePrint += (intTemporaryChar % 16).to_s(16).upcase
			end
			
			if not $g_kGeneratedBluePrints.include? strNewBluePrint
				bValidUUI = true
			else
				intValidGUIDOffset = intValidGUIDOffset + 1
				
				#puts intValidGUIDOffset
			end
		end

		$g_kGeneratedBluePrints.push strNewBluePrint

		return strNewBluePrint
	end

	# Generates the filters and files for the project code
	private
	def generateFiltersAndFiles(strForceFilename, kProjectProperties, strRootBluePrint, strBasePathToFiles, strIncrementalPathToFiles, strFileName, iTagNum, strExtensionToFind)
		
		strCurrentBluePrint = strRootBluePrint
		if strForceFilename != nil
			$g_kFiltersAndFiles[strCurrentBluePrint][0] = strForceFilename
		else
			$g_kFiltersAndFiles[strCurrentBluePrint][0] = strFileName
		end
		$g_kFiltersAndFiles[strCurrentBluePrint][1] = []
		$g_kFiltersAndFiles[strCurrentBluePrint][2] = strIncrementalPathToFiles

		strSearchingPath = strBasePathToFiles + strIncrementalPathToFiles
		if File.directory?(strSearchingPath)
			$g_kFiltersAndFiles[strCurrentBluePrint][3] = true
			$g_kFiltersAndFiles[strCurrentBluePrint][4] = nil
			Dir.foreach(strSearchingPath) do |strItem|
				next if strItem == '.' or strItem == '..' or strItem == '.svn'

				strFileNameExtension = nil
				if not File.directory?(strSearchingPath + "/" + strItem)
					iIndexExtension = strItem.index('.')
					if iIndexExtension != nil
						iIndexExtension += 1
						strFileNameExtension = strItem[iIndexExtension..strItem.length]
					end
					next if iIndexExtension == nil
					if strExtensionToFind != nil
						next if not MAP_EXT_SPECIAL.has_key?(strFileNameExtension)
						
						$g_kProjectSpecialLibraries[strItem].push kProjectProperties.m_strProjectPath
						$g_kProjectSpecialLibraries[strItem].push kProjectProperties.m_strProjectName
						
						kProjectProperties.m_kProjectSpecialLibraries.push strItem
					else
						next if not MAP_EXT.has_key?(strFileNameExtension)
					end
				end
			
				strNewBluePrint = generateBluePrint strItem + strFileName, strRootBluePrint, "dir"
				$g_kFiltersAndFiles[strCurrentBluePrint][1].push strNewBluePrint
				generateFiltersAndFiles nil, kProjectProperties, strNewBluePrint, strBasePathToFiles, strIncrementalPathToFiles + "/" + strItem, strItem, iTagNum + 1, strExtensionToFind
			end
		else
			$g_kFiltersAndFiles[strCurrentBluePrint][3] = false
			$g_kFiltersAndFiles[strCurrentBluePrint][4] = generateBluePrint strFileName, strRootBluePrint, "file"
		end
	
	end
	
	# Generates the filters and files for the project resources
	private
	def generateFiltersAndFilesResources(strForcePath, kProjectProperties, strRootBluePrint, strBasePathToFiles, strIncrementalPathToFiles, strFileName, iTagNum)
		
		strCurrentBluePrint = strRootBluePrint
		$g_kFiltersAndFilesResources[strCurrentBluePrint][0] = strFileName
		$g_kFiltersAndFilesResources[strCurrentBluePrint][1] = []
		if strForcePath != nil
			if kProjectProperties.m_bIsMainProject
				$g_kFiltersAndFilesResources[strCurrentBluePrint][2] = strForcePath + strIncrementalPathToFiles
			else
				$g_kFiltersAndFilesResources[strCurrentBluePrint][2] = strForcePath + kProjectProperties.m_strProjectPath + kProjectProperties.m_strProjectName + "/" + strIncrementalPathToFiles
			end
		else
			$g_kFiltersAndFilesResources[strCurrentBluePrint][2] = strIncrementalPathToFiles
		end

		strSearchingPath = strBasePathToFiles + strIncrementalPathToFiles
		if File.directory?(strSearchingPath)
			$g_kFiltersAndFilesResources[strCurrentBluePrint][3] = true
			$g_kFiltersAndFilesResources[strCurrentBluePrint][4] = nil
			Dir.foreach(strSearchingPath) do |strItem|
				next if strItem == '.' or strItem == '..' or strItem == '.svn'

				strFileNameExtension = nil
				if not File.directory?(strSearchingPath + "/" + strItem)
					iIndexExtension = strItem.index('.')
					if iIndexExtension != nil
						iIndexExtension += 1
						strFileNameExtension = strItem[iIndexExtension..strItem.length]
					end
					next if iIndexExtension == nil
					next if not RES_EXT.has_key?(strFileNameExtension)
				end

				strNewBluePrint = generateBluePrint strItem + strFileName, strRootBluePrint, "dir"
				$g_kFiltersAndFilesResources[strCurrentBluePrint][1].push strNewBluePrint
				generateFiltersAndFilesResources strForcePath, kProjectProperties, strNewBluePrint, strBasePathToFiles, strIncrementalPathToFiles + "/" + strItem, strItem, iTagNum + 1
			end
		else
			$g_kFiltersAndFilesResources[strCurrentBluePrint][3] = false
			if kProjectProperties != nil
				$g_kFiltersAndFilesResources[strCurrentBluePrint][4] = generateBluePrint kProjectProperties.m_strProjectName, strRootBluePrint, "file"
			else
				$g_kFiltersAndFilesResources[strCurrentBluePrint][4] = generateBluePrint strFileName, strRootBluePrint, "file"
			end
		end
	
	end
	
	# Generates the filters and files for the framework resources
	private
	def generateFiltersAndFilesResourcesFromFrameworks(strRootBluePrint)
		
		require 'find'
		
		kAppModulesVisited = Array.new
		ProjectProperties::s_kProjectIOSFrameworkResources.each do |kProjectName, kFrameworkResourcesHash|
		
			kFrameworkResourcesHash.each do |strFrameworkName, kResourcesArray|
				if not kAppModulesVisited.include? strFrameworkName
					strFrameworkNameFull = strFrameworkName + ".framework"
					strFrameworkResourcePath = BE3_SDK_DIR_SCRIPTS + "3rdParty/" + strFrameworkName + "/" + strFrameworkNameFull + "/"
					
					kResourcesArray.each do |strResourceName|
						Find.find(strFrameworkResourcePath) do |strResourcePath|
							if strResourcePath.include? strResourceName
								strFinalResourcePath = strResourcePath
								strFinalResourcePath = strFinalResourcePath.gsub(BE3_SDK_DIR_SCRIPTS, '')
								strFinalResourcePath = BE3_SDK_DIR + strFinalResourcePath
								
								strNewBluePrint = generateBluePrint strFrameworkNameFull, strRootBluePrint, "bundle"
								$g_kFiltersAndFilesResources[strRootBluePrint][1].push strNewBluePrint
								
								strCurrentBluePrint = strNewBluePrint
								$g_kFiltersAndFilesResources[strCurrentBluePrint][0] = strResourceName
								$g_kFiltersAndFilesResources[strCurrentBluePrint][1] = []
								$g_kFiltersAndFilesResources[strCurrentBluePrint][2] = strFinalResourcePath
								$g_kFiltersAndFilesResources[strCurrentBluePrint][3] = false
								$g_kFiltersAndFilesResources[strCurrentBluePrint][4] = generateBluePrint strFrameworkNameFull, "bundle", "fileRef"
								
								Find.prune
							end
						end
					end
					
					kAppModulesVisited.push strFrameworkName
				end
			end
			
		end
		
	end
		
	# Generates the filters and files for the project variants
	private
	def generateFiltersAndFilesVariants(strForcePath, kProjectProperties, strRootBluePrint, strBasePathToFiles, strIncrementalPathToFiles, strFileName, iTagNum)
	
		strSearchingPath = strBasePathToFiles + strIncrementalPathToFiles + strFileName
		if File.directory?(strSearchingPath)
			Dir.foreach(strSearchingPath) do |strItem|
				next if strItem == '.' or strItem == '..' or strItem == '.svn'

				strFileNameExtension = nil
				if not File.directory?(strSearchingPath + "/" + strItem)
					iIndexExtension = strItem.index('.')
					if iIndexExtension != nil
						iIndexExtension += 1
						strFileNameExtension = strItem[iIndexExtension..strItem.length]
					end
					next if iIndexExtension == nil
					next if not RES_EXT.has_key?(strFileNameExtension)
				end

				if File.directory?(strSearchingPath + "/" + strItem)
					strNewBluePrint = generateBluePrint strItem + strFileName, strRootBluePrint, "dir"
					generateFiltersAndFilesVariants strForcePath, kProjectProperties, strNewBluePrint, strBasePathToFiles, strIncrementalPathToFiles + "/" + strItem, "", iTagNum + 1
				else
					strNewBluePrint = generateBluePrint strItem + strFileName, strRootBluePrint, "file"
					generateFiltersAndFilesVariants strForcePath, kProjectProperties, strNewBluePrint, strBasePathToFiles, strIncrementalPathToFiles, strItem, iTagNum + 1
				end
			end
		else
			if not $g_kFiltersAndFilesVariants.include? strFileName
				$g_kFiltersAndFilesVariants[strFileName][0] = generateBluePrint strFileName, "variant", "base"
				$g_kFiltersAndFilesVariants[strFileName][1] = generateBluePrint strFileName, "variant", "file"
				$g_kFiltersAndFilesVariants[strFileName][2] = []
				$g_kFiltersAndFilesVariants[strFileName][3] = []
			end
			
			if strForcePath != nil
				$g_kFiltersAndFilesVariants[strFileName][2].push strForcePath + strIncrementalPathToFiles
			else
				$g_kFiltersAndFilesVariants[strFileName][2].push strIncrementalPathToFiles
			end
			
			strDirBluePrint = generateBluePrint strFileName, strIncrementalPathToFiles, strFileName + "dir"
			$g_kFiltersAndFilesVariants[strFileName][3].push strDirBluePrint
		end
	
	end
	
	# Writes the dependencies to the PBXBuild
	private
	def writeDependendiesToPBXBuild(kAppModulesVisited, strProjectName, bIsFirst)
		
		if not bIsFirst
			
			$g_kFilesData[self.m_kProjectProperties.m_strProjectName + strProjectName][0] = generateBluePrint self.m_kProjectProperties.m_strProjectName + strProjectName, "dependency0", "writeDependendiesToPBXBuild"
			$g_kFilesData[self.m_kProjectProperties.m_strProjectName + strProjectName][1] = generateBluePrint self.m_kProjectProperties.m_strProjectName + strProjectName, "fileRef1", "writeDependendiesToPBXBuild"
			
			strLibName = nil
			if strProjectName == $g_kFinalLibraryName
				strMainLIBOutputFile = GetMinLIBOutputName strProjectName
				strLibName = "lib" + strMainLIBOutputFile + ".a"
			else
				strLibName = "lib" + strProjectName + ".a"
			end
			
			self.m_strDataToWrite = "\t\t#{$g_kFilesData[self.m_kProjectProperties.m_strProjectName + strProjectName][0]} /* #{strLibName} in Frameworks */ = {isa = PBXBuildFile; fileRef = #{$g_kFilesData[self.m_kProjectProperties.m_strProjectName + strProjectName][1]} /* #{strLibName} */; };"
			writeDataToFile
			
		end
		
		kAppModulesVisited.push strProjectName
		
		if ProjectProperties::s_kProject3rdPartyDependencies[strProjectName].length > 0
			ProjectProperties::s_kProject3rdPartyDependencies[strProjectName].each do |strDependencyName|
				if not kAppModulesVisited.include? strDependencyName
					dependencyProjectProperties = GetProjectProperties strDependencyName
					if dependencyProjectProperties != nil
						if dependencyProjectProperties.m_kProjectPlatformTargets == nil or (dependencyProjectProperties.m_kProjectPlatformTargets != nil and dependencyProjectProperties.m_kProjectPlatformTargets.include? PLATFORM_IOS)
							writeDependendiesToPBXBuild kAppModulesVisited, strDependencyName, false
						end
					end
				end
			end
		end

		if ProjectProperties::s_kProjectDependencies[strProjectName].length > 0
			ProjectProperties::s_kProjectDependencies[strProjectName].each do |strDependencyName|
				if not kAppModulesVisited.include? strDependencyName
					writeDependendiesToPBXBuild kAppModulesVisited, strDependencyName, false
				end
			end
		end
		
	end
	
	# Writes the Xcode PBXBuildFile section to project file
	private
	def generatePBXBuildFileSection
		self.m_strDataToWrite = "/* Begin PBXBuildFile section */"
		writeDataToFile
		
		# Retrieves all the filters and files from Code folder
		$g_kFiltersAndFiles.clear
		if self.m_kProjectProperties.m_bIsMainProject
			$g_kCodeFolderFilterBluePrint = generateBluePrint self.m_kProjectProperties.m_strProjectName, "src", "srcgeneratePBXBuildFileSection"
			generateFiltersAndFiles nil, self.m_kProjectProperties, $g_kCodeFolderFilterBluePrint, self.m_kProjectProperties.m_strProjectPath + "/" + self.m_kProjectProperties.m_strProjectName + "/", "./src", "src", 0, nil
			$g_kIOSFolderFilterBluePrint = generateBluePrint self.m_kProjectProperties.m_strProjectName, "iOS", "iOSgeneratePBXBuildFileSection"
			generateFiltersAndFiles nil, self.m_kProjectProperties, $g_kIOSFolderFilterBluePrint, self.m_kProjectProperties.m_strProjectPath + "/" + self.m_kProjectProperties.m_strProjectName + "/", "./iOS", "iOS", 0, nil
			$g_kPchFolderFilterBluePrint = generateBluePrint self.m_kProjectProperties.m_strProjectName, "Pch", "PchgeneratePBXBuildFileSection"
			generateFiltersAndFiles nil, self.m_kProjectProperties, $g_kPchFolderFilterBluePrint, self.m_kProjectProperties.m_strProjectPath + "/" + self.m_kProjectProperties.m_strProjectName + "/", "./Pch", "Pch", 0, nil
			$g_kLibsFolderFilterBluePrint = generateBluePrint self.m_kProjectProperties.m_strProjectName, "Lib", "iOSLibgeneratePBXBuildFileSection"
			generateFiltersAndFiles "Libs", self.m_kProjectProperties, $g_kLibsFolderFilterBluePrint, self.m_kProjectProperties.m_strProjectPath + "/" + self.m_kProjectProperties.m_strProjectName + "/", "./iOS", "iOS", 0, "a"
		else
			$g_kCodeFolderFilterBluePrint = generateBluePrint self.m_kProjectProperties.m_strProjectName, "src", "srcgeneratePBXBuildFileSection"
			generateFiltersAndFiles nil, self.m_kProjectProperties, $g_kCodeFolderFilterBluePrint, BE3_SDK_DIR_SCRIPTS + self.m_kProjectProperties.m_strProjectPath + "/" + self.m_kProjectProperties.m_strProjectName + "/", "./src", "src", 0, nil
			$g_kIOSFolderFilterBluePrint = generateBluePrint self.m_kProjectProperties.m_strProjectName, "iOS", "iOSgeneratePBXBuildFileSection"
			generateFiltersAndFiles nil, self.m_kProjectProperties, $g_kIOSFolderFilterBluePrint, BE3_SDK_DIR_SCRIPTS + self.m_kProjectProperties.m_strProjectPath + "/" + self.m_kProjectProperties.m_strProjectName + "/", "./iOS", "iOS", 0, nil
			$g_kPchFolderFilterBluePrint = generateBluePrint self.m_kProjectProperties.m_strProjectName, "Pch", "PchgeneratePBXBuildFileSection"
			generateFiltersAndFiles nil, self.m_kProjectProperties, $g_kPchFolderFilterBluePrint, BE3_SDK_DIR_SCRIPTS + self.m_kProjectProperties.m_strProjectPath + "/" + self.m_kProjectProperties.m_strProjectName + "/", "./Pch", "Pch", 0, nil
			$g_kLibsFolderFilterBluePrint = generateBluePrint self.m_kProjectProperties.m_strProjectName, "Lib", "iOSLibgeneratePBXBuildFileSection"
			generateFiltersAndFiles "Libs", self.m_kProjectProperties, $g_kLibsFolderFilterBluePrint, BE3_SDK_DIR_SCRIPTS + self.m_kProjectProperties.m_strProjectPath + "/" + self.m_kProjectProperties.m_strProjectName + "/", "./iOS", "iOS", 0, "a"
		end

		# Add source files to compile
		$g_kFiltersAndFiles.each_key do |strKey|
			if not $g_kFiltersAndFiles[strKey][3]
				strFileName = $g_kFiltersAndFiles[strKey][0]
				iIndexExtension = strFileName.index('.')
      
				if iIndexExtension != nil
             		iIndexExtension += 1
					strFileNameExtension = strFileName[iIndexExtension..strFileName.length]
                    
					if COMPILABLE_EXT.include? strFileNameExtension
						self.m_strDataToWrite = "\t\t#{strKey} /* #{strFileName} in Sources */ = {isa = PBXBuildFile; fileRef = #{$g_kFiltersAndFiles[strKey][4]} /* #{strFileName} */; };"
                		writeDataToFile
					end
				end
			end
		end
		
		# Add special libraries to link
		$g_kFiltersAndFiles.each_key do |strKey|
			if not $g_kFiltersAndFiles[strKey][3]
				strFileName = $g_kFiltersAndFiles[strKey][0]
				iIndexExtension = strFileName.index('.')
      
				if iIndexExtension != nil
             		iIndexExtension += 1
					strFileNameExtension = strFileName[iIndexExtension..strFileName.length]
                    
					if LINK_EXT.include? strFileNameExtension
						self.m_strDataToWrite = "\t\t#{strKey} /* #{strFileName} in Sources */ = {isa = PBXBuildFile; fileRef = #{$g_kFiltersAndFiles[strKey][4]} /* #{strFileName} */; };"
                		writeDataToFile
					end
				end
			end
		end
		
		# Add frameworks and dependency libraries
		#if self.m_kProjectProperties.m_bIsMainProject
			for strFrameWorkName in $g_kProjectNeededFrameworks
				$g_kFilesData[self.m_kProjectProperties.m_strProjectName + strFrameWorkName][0] = generateBluePrint self.m_kProjectProperties.m_strProjectName, strFrameWorkName, "name"
				$g_kFilesData[self.m_kProjectProperties.m_strProjectName + strFrameWorkName][1] = generateBluePrint self.m_kProjectProperties.m_strProjectName, strFrameWorkName, "fileRef"
				self.m_strDataToWrite = "\t\t#{$g_kFilesData[self.m_kProjectProperties.m_strProjectName + strFrameWorkName][0]} /* #{strFrameWorkName} in Frameworks */ = {isa = PBXBuildFile; fileRef = #{$g_kFilesData[self.m_kProjectProperties.m_strProjectName + strFrameWorkName][1]} /* #{strFrameWorkName} */; };"
				writeDataToFile
			end
			for strFrameWorkName in $g_kBakEngineNeededFrameworks
				next if $g_kProjectNeededFrameworks.include?(strFrameWorkName)
				$g_kFilesData[self.m_kProjectProperties.m_strProjectName + strFrameWorkName][0] = generateBluePrint self.m_kProjectProperties.m_strProjectName, strFrameWorkName, "name"
				$g_kFilesData[self.m_kProjectProperties.m_strProjectName + strFrameWorkName][1] = generateBluePrint self.m_kProjectProperties.m_strProjectName, strFrameWorkName, "fileRef"
				self.m_strDataToWrite = "\t\t#{$g_kFilesData[self.m_kProjectProperties.m_strProjectName + strFrameWorkName][0]} /* #{strFrameWorkName} in Frameworks */ = {isa = PBXBuildFile; fileRef = #{$g_kFilesData[self.m_kProjectProperties.m_strProjectName + strFrameWorkName][1]} /* #{strFrameWorkName} */; };"
				writeDataToFile
			end
		
		if self.m_kProjectProperties.m_bIsMainProject
			if not ALL_DEPENDENCIES_IN_MAIN_PROJECT
				for strDependency in ProjectProperties::s_kProjectDependencies[self.m_kProjectProperties.m_strProjectName]
					$g_kFilesData[self.m_kProjectProperties.m_strProjectName + strDependency][0] = generateBluePrint self.m_kProjectProperties.m_strProjectName, strDependency, "dependencies"
					$g_kFilesData[self.m_kProjectProperties.m_strProjectName + strDependency][1] = generateBluePrint self.m_kProjectProperties.m_strProjectName, strDependency, "fileRefdependencies"
					strLibName = nil
					if strDependency == $g_kFinalLibraryName
						strMainLIBOutputFile = GetMinLIBOutputName strDependency
						strLibName = "lib" + strMainLIBOutputFile + ".a"
					else
						strLibName = "lib" + strDependency + ".a"
					end
					self.m_strDataToWrite = "\t\t#{$g_kFilesData[self.m_kProjectProperties.m_strProjectName + strDependency][0]} /* #{strLibName} in Frameworks */ = {isa = PBXBuildFile; fileRef = #{$g_kFilesData[self.m_kProjectProperties.m_strProjectName + strDependency][1]} /* #{strLibName} */; };"
					writeDataToFile
				end
			else
				for strDependency in ProjectProperties::s_kProjectDependencies[self.m_kProjectProperties.m_strProjectName]
					if strDependency == $g_kFinalLibraryName
						kAppModulesVisited = Array.new
						writeDependendiesToPBXBuild kAppModulesVisited, strDependency, false
					else
						$g_kFilesData[self.m_kProjectProperties.m_strProjectName + strDependency][0] = generateBluePrint self.m_kProjectProperties.m_strProjectName, strDependency, "dependencies"
						$g_kFilesData[self.m_kProjectProperties.m_strProjectName + strDependency][1] = generateBluePrint self.m_kProjectProperties.m_strProjectName, strDependency, "fileRefdependencies"
						strLibName = "lib" + strDependency + ".a"
						self.m_strDataToWrite = "\t\t#{$g_kFilesData[self.m_kProjectProperties.m_strProjectName + strDependency][0]} /* #{strLibName} in Frameworks */ = {isa = PBXBuildFile; fileRef = #{$g_kFilesData[self.m_kProjectProperties.m_strProjectName + strDependency][1]} /* #{strLibName} */; };"
						writeDataToFile
					end
				end
			end
		else
			if not ALL_DEPENDENCIES_IN_MAIN_PROJECT and self.m_kProjectProperties.m_strProjectName == $g_kFinalLibraryName
				kAppModulesVisited = Array.new
				writeDependendiesToPBXBuild kAppModulesVisited, self.m_kProjectProperties.m_strProjectName, true
			end
		end
		
		# Add project assets
		if $g_kIOSResBluePrint == nil
			$g_kIOSResBluePrint = generateBluePrint "iOS", "iOSRes", "resFiles"
		end
		if self.m_kProjectProperties.m_bIsMainProject
			$g_kFilesData[self.m_kProjectProperties.m_strProjectName + "assets"][0] = generateBluePrint self.m_kProjectProperties.m_strProjectName, "assets", "name"
			$g_kFilesData[self.m_kProjectProperties.m_strProjectName + "assets"][1] = generateBluePrint self.m_kProjectProperties.m_strProjectName, "assets", "fileRef"
			self.m_strDataToWrite = "\t\t#{$g_kFilesData[self.m_kProjectProperties.m_strProjectName + "assets"][0]} /* assets in Resources */ = {isa = PBXBuildFile; fileRef = #{$g_kFilesData[self.m_kProjectProperties.m_strProjectName + "assets"][1]} /* assets */; };"
			writeDataToFile
			
			$g_kFilesData[self.m_kProjectProperties.m_strProjectName + "iOSVariant"][0] = generateBluePrint self.m_kProjectProperties.m_strProjectName, "iOSVariant", "name"
			$g_kFilesData[self.m_kProjectProperties.m_strProjectName + "iOSVariant"][1] = generateBluePrint self.m_kProjectProperties.m_strProjectName, "iOSVariant", "fileRef"
			self.m_strDataToWrite = "\t\t#{$g_kFilesData[self.m_kProjectProperties.m_strProjectName + "iOSVariant"][0]} /* iOSVariant in Resources */ = {isa = PBXBuildFile; fileRef = #{$g_kFilesData[self.m_kProjectProperties.m_strProjectName + "iOSVariant"][1]} /* iOSVariant */; };"
			writeDataToFile
			
			generateFiltersAndFilesResources ".", self.m_kProjectProperties, $g_kIOSResBluePrint, "./", "./iOSRes", "iOSRes", 0
			generateFiltersAndFilesResourcesFromFrameworks $g_kIOSResBluePrint

			$g_kFiltersAndFilesResources.each_key do |strKey|
				if not $g_kFiltersAndFilesResources[strKey][3]
					strFileName = $g_kFiltersAndFilesResources[strKey][0]

					next if strFileName == $strProjectNameToUse.downcase + "-Info.plist"
					
					self.m_strDataToWrite = "\t\t#{strKey} /* #{strFileName} in Resources */ = {isa = PBXBuildFile; fileRef = #{$g_kFiltersAndFilesResources[strKey][4]} /* #{strFileName} */; };"
					writeDataToFile
				end
			end
			
			$g_kIOSVariantBluePrint = generateBluePrint "iOS", "iOSVariant", "resVariant"
			generateFiltersAndFilesVariants ".", self.m_kProjectProperties, $g_kIOSVariantBluePrint, "./", "./iOSVariant", "", 0
			
			$g_kFiltersAndFilesVariants.each_key do |strKey|
				self.m_strDataToWrite = "\t\t#{$g_kFiltersAndFilesVariants[strKey][0]} /* #{strKey} in Resources */ = {isa = PBXBuildFile; fileRef = #{$g_kFiltersAndFilesVariants[strKey][1]} /* #{strKey} */; };"
				writeDataToFile
			end
		else
			generateFiltersAndFilesResources BE3_SDK_DIR, self.m_kProjectProperties, $g_kIOSResBluePrint, BE3_SDK_DIR_SCRIPTS + self.m_kProjectProperties.m_strProjectPath + "/" + self.m_kProjectProperties.m_strProjectName + "/", "iOSRes", "iOSRes", 0
		end

		self.m_strDataToWrite = "/* End PBXBuildFile section */"
		writeDataToFile
		writeDataToFile
	end
		
	# Writes the containers to item proxy
	private
	def writeContainersToItemProxy(kAppModulesVisited, strProjectName, bIsFirst)
	
		if self.m_kProjectProperties.m_bIsMainProject
			if ProjectProperties::s_kProject3rdPartyDependencies[strProjectName].length > 0
				ProjectProperties::s_kProject3rdPartyDependencies[strProjectName].each do |strDependencyName|

					dependencyProjectProperties = GetProjectProperties strDependencyName
					if dependencyProjectProperties != nil
						if dependencyProjectProperties.m_kProjectPlatformTargets == nil or (dependencyProjectProperties.m_kProjectPlatformTargets != nil and dependencyProjectProperties.m_kProjectPlatformTargets.include? PLATFORM_IOS)
					
							$g_kPBXContainerItemProxy[strDependencyName][0] = generateBluePrint strProjectName, strDependencyName, "proxyType2"
							$g_kPBXContainerItemProxy[strDependencyName][1] = generateBluePrint strProjectName, strDependencyName, "proxyType1"
							$g_kPBXContainerItemProxy[strDependencyName][2] = generateBluePrint strProjectName, strDependencyName, "containerPortal"
							$g_kPBXContainerItemProxy[strDependencyName][3] = generateBluePrint strProjectName, strDependencyName, "proxyType2remoteGlobalIDString"
							$g_kPBXContainerItemProxy[strDependencyName][4] = generateBluePrint strProjectName, strDependencyName, "proxyType1remoteGlobalIDString"

							self.m_strDataToWrite = "\t\t#{$g_kPBXContainerItemProxy[strDependencyName][0]} /* PBXContainerItemProxy */ = {
			isa = PBXContainerItemProxy;
			containerPortal = #{$g_kPBXContainerItemProxy[strDependencyName][2]} /* #{strDependencyName}#{$g_strXcodeFileSufix}.xcodeproj */;
			proxyType = 2;
			remoteGlobalIDString = #{$g_kPBXContainerItemProxy[strDependencyName][3]};
			remoteInfo = #{strDependencyName};
		};"
							writeDataToFile
					
							self.m_strDataToWrite = "\t\t#{$g_kPBXContainerItemProxy[strDependencyName][1]} /* PBXContainerItemProxy */ = {
			isa = PBXContainerItemProxy;
			containerPortal = #{$g_kPBXContainerItemProxy[strDependencyName][2]} /* #{strDependencyName}#{$g_strXcodeFileSufix}.xcodeproj */;
			proxyType = 1;
			remoteGlobalIDString = #{$g_kPBXContainerItemProxy[strDependencyName][4]};
			remoteInfo = #{strDependencyName};
		};"
							writeDataToFile
					
						end
					end
				end
			end
				
			if ProjectProperties::s_kProjectDependencies[strProjectName].length > 0
				ProjectProperties::s_kProjectDependencies[strProjectName].each do |strDependencyName|
				
					if ALL_DEPENDENCIES_IN_MAIN_PROJECT and strDependencyName == $g_kFinalLibraryName
						writeContainersToItemProxy kAppModulesVisited, strDependencyName, false
					end
					
					$g_kPBXContainerItemProxy[strDependencyName][0] = generateBluePrint strProjectName, strDependencyName, "proxyType2"
					$g_kPBXContainerItemProxy[strDependencyName][1] = generateBluePrint strProjectName, strDependencyName, "proxyType1"
					$g_kPBXContainerItemProxy[strDependencyName][2] = generateBluePrint strProjectName, strDependencyName, "containerPortal"
					$g_kPBXContainerItemProxy[strDependencyName][3] = generateBluePrint strProjectName, strDependencyName, "proxyType2remoteGlobalIDString"
					$g_kPBXContainerItemProxy[strDependencyName][4] = generateBluePrint strProjectName, strDependencyName, "proxyType1remoteGlobalIDString"

					self.m_strDataToWrite = "\t\t#{$g_kPBXContainerItemProxy[strDependencyName][0]} /* PBXContainerItemProxy */ = {
			isa = PBXContainerItemProxy;
			containerPortal = #{$g_kPBXContainerItemProxy[strDependencyName][2]} /* #{strDependencyName}#{$g_strXcodeFileSufix}.xcodeproj */;
			proxyType = 2;
			remoteGlobalIDString = #{$g_kPBXContainerItemProxy[strDependencyName][3]};
			remoteInfo = #{strDependencyName};
		};"
					writeDataToFile
					
					self.m_strDataToWrite = "\t\t#{$g_kPBXContainerItemProxy[strDependencyName][1]} /* PBXContainerItemProxy */ = {
			isa = PBXContainerItemProxy;
			containerPortal = #{$g_kPBXContainerItemProxy[strDependencyName][2]} /* #{strDependencyName}#{$g_strXcodeFileSufix}.xcodeproj */;
			proxyType = 1;
			remoteGlobalIDString = #{$g_kPBXContainerItemProxy[strDependencyName][4]};
			remoteInfo = #{strDependencyName};
		};"
					writeDataToFile
					
				end
			end
			return
		else
			if not bIsFirst
					$g_kPBXContainerItemProxy[strProjectName][0] = generateBluePrint strProjectName, strProjectName, "proxyType2"
					$g_kPBXContainerItemProxy[strProjectName][1] = generateBluePrint strProjectName, strProjectName, "proxyType1"
					$g_kPBXContainerItemProxy[strProjectName][2] = generateBluePrint strProjectName, strProjectName, "containerPortal"
					$g_kPBXContainerItemProxy[strProjectName][3] = generateBluePrint strProjectName, strProjectName, "proxyType2remoteGlobalIDString"
					$g_kPBXContainerItemProxy[strProjectName][4] = generateBluePrint strProjectName, strProjectName, "proxyType1remoteGlobalIDString"

				self.m_strDataToWrite = "\t\t#{$g_kPBXContainerItemProxy[strProjectName][0]} /* PBXContainerItemProxy */ = {
			isa = PBXContainerItemProxy;
			containerPortal = #{$g_kPBXContainerItemProxy[strProjectName][2]} /* #{strProjectName}#{$g_strXcodeFileSufix}.xcodeproj */;
			proxyType = 2;
			remoteGlobalIDString = #{$g_kPBXContainerItemProxy[strProjectName][3]};
			remoteInfo = #{strProjectName};
		};"
				writeDataToFile
					
				self.m_strDataToWrite = "\t\t#{$g_kPBXContainerItemProxy[strProjectName][1]} /* PBXContainerItemProxy */ = {
			isa = PBXContainerItemProxy;
			containerPortal = #{$g_kPBXContainerItemProxy[strProjectName][2]} /* #{strProjectName}#{$g_strXcodeFileSufix}.xcodeproj */;
			proxyType = 1;
			remoteGlobalIDString = #{$g_kPBXContainerItemProxy[strProjectName][4]};
			remoteInfo = #{strProjectName};
		};"
				writeDataToFile
			end
		end
		
		kAppModulesVisited.push strProjectName

		if ProjectProperties::s_kProject3rdPartyDependencies[strProjectName].length > 0
			ProjectProperties::s_kProject3rdPartyDependencies[strProjectName].each do |strDependencyName|
				if not kAppModulesVisited.include? strDependencyName
					dependencyProjectProperties = GetProjectProperties strDependencyName
					if dependencyProjectProperties != nil
						if dependencyProjectProperties.m_kProjectPlatformTargets == nil or (dependencyProjectProperties.m_kProjectPlatformTargets != nil and dependencyProjectProperties.m_kProjectPlatformTargets.include? PLATFORM_IOS)
							writeContainersToItemProxy kAppModulesVisited, strDependencyName, false
						end
					end
				end
			end
		end
		
		if ProjectProperties::s_kProjectDependencies[strProjectName].length > 0
			ProjectProperties::s_kProjectDependencies[strProjectName].each do |strDependencyName|
				if not kAppModulesVisited.include? strDependencyName
					writeContainersToItemProxy kAppModulesVisited, strDependencyName, false
				end
			end
		end

	end
	
	# Writes the Xcode PBXContainerItemProxy section to project file
	private
	def generatePBXContainerItemProxySection
		self.m_strDataToWrite = "/* Begin PBXContainerItemProxy section */"
		writeDataToFile

		kAppModulesVisited = Array.new
		writeContainersToItemProxy kAppModulesVisited, self.m_kProjectProperties.m_strProjectName, true

		self.m_strDataToWrite = "/* End PBXContainerItemProxy section */"
		writeDataToFile
		writeDataToFile
	end

	# Writes the Xcode PBXCopyFilesBuildPhase section to project file
	private
	def generatePBXCopyFilesBuildPhaseSection
		self.m_strDataToWrite = "/* Begin PBXCopyFilesBuildPhase section */"
		writeDataToFile

		self.m_strDataToWrite = "/* End PBXCopyFilesBuildPhase section */"
		writeDataToFile
		writeDataToFile
	end

	# Writes dependency projects to File Refs
	private
	def writeDependencyProjectsToFileRefs(kAppModulesVisited, strProjectPath, strProjectName, bIsFirst)

		if not bIsFirst
			self.m_strDataToWrite = "\t\t#{$g_kPBXContainerItemProxy[strProjectName][2]} /* #{strProjectName}#{$g_strXcodeFileSufix}.xcodeproj */ = {isa = PBXFileReference; lastKnownFileType = \"wrapper.pb-project\"; name = #{strProjectName}#{$g_strXcodeFileSufix}.xcodeproj; path = #{BE3_SDK_DIR}#{strProjectPath}#{strProjectName}#{PROJECT_FILES_PATH}/#{strProjectName}#{$g_strXcodeFileSufix}.xcodeproj; sourceTree = \"<group>\"; };"
			writeDataToFile
		end
		
		kAppModulesVisited.push strProjectName
		
		if ProjectProperties::s_kProject3rdPartyDependencies[strProjectName].length > 0
			ProjectProperties::s_kProject3rdPartyDependencies[strProjectName].each do |strDependencyName|
				if not kAppModulesVisited.include? strDependencyName
					dependencyProjectProperties = GetProjectProperties strDependencyName
					if dependencyProjectProperties != nil
						if dependencyProjectProperties.m_kProjectPlatformTargets == nil or (dependencyProjectProperties.m_kProjectPlatformTargets != nil and dependencyProjectProperties.m_kProjectPlatformTargets.include? PLATFORM_IOS)
							writeDependencyProjectsToFileRefs kAppModulesVisited, dependencyProjectProperties.m_strProjectPath, strDependencyName, false
						end
					end
				end
			end
		end

		if ProjectProperties::s_kProjectDependencies[strProjectName].length > 0
			ProjectProperties::s_kProjectDependencies[strProjectName].each do |strDependencyName|
				if not kAppModulesVisited.include? strDependencyName
					dependencyProjectProperties = GetProjectProperties strDependencyName
					if dependencyProjectProperties != nil
						writeDependencyProjectsToFileRefs kAppModulesVisited, dependencyProjectProperties.m_strProjectPath, strDependencyName, false
					end
				end
			end
		end

	end
	
	# Writes the Xcode PBXFileReference section to project file
	private
	def generatePBXFileReferenceSection
		self.m_strDataToWrite = "/* Begin PBXFileReference section */"
		writeDataToFile

		# Add all source files
		$g_kFiltersAndFiles.each_key do |strKey|
			if not $g_kFiltersAndFiles[strKey][3]
				strFileName = $g_kFiltersAndFiles[strKey][0]
				iIndexExtension = strFileName.index('.')
				if iIndexExtension != nil
					iIndexExtension += 1
					strFileNameExtension = strFileName[iIndexExtension..strFileName.length]
					
					if MAP_EXT.include? strFileNameExtension
						self.m_strDataToWrite = "\t\t#{$g_kFiltersAndFiles[strKey][4]} /* #{strFileName} */ = {isa = PBXFileReference; lastKnownFileType = #{MAP_EXT[strFileNameExtension]}; name = \"#{strFileName}\"; path = \".#{$g_kFiltersAndFiles[strKey][2].gsub('\\','/')}\"; sourceTree = \"<group>\"; };"
						writeDataToFile
					end
					if MAP_EXT_SPECIAL.include? strFileNameExtension
						self.m_strDataToWrite = "\t\t#{$g_kFiltersAndFiles[strKey][4]} /* #{strFileName} */ = {isa = PBXFileReference; lastKnownFileType = #{MAP_EXT_SPECIAL[strFileNameExtension]}; name = \"#{strFileName}\"; path = \".#{$g_kFiltersAndFiles[strKey][2].gsub('\\','/')}\"; sourceTree = \"<group>\"; };"
						writeDataToFile
					end
				end
			end
		end
		
		# Add self output file
		strSelfOutputName = nil
		if self.m_kProjectProperties.m_bIsMainProject
			strSelfOutputName = $strProjectNameToUse + ".app"
			$g_kFilesData[strSelfOutputName][0] = generateBluePrint strSelfOutputName, "app", "generatePBXFileReferenceSection"
			self.m_strDataToWrite = "\t\t#{$g_kFilesData[strSelfOutputName][0]} /* #{strSelfOutputName} */ = {isa = PBXFileReference; explicitFileType = wrapper.application; includeInIndex = 0; path = #{strSelfOutputName}; sourceTree = BUILT_PRODUCTS_DIR; };"
			writeDataToFile
		else
			if self.m_kProjectProperties.m_strProjectName == $g_kFinalLibraryName
				if self.m_kProjectProperties.m_strOutputFileName != nil
					strSelfOutputName = "lib" + self.m_kProjectProperties.m_strOutputFileName + ".a"
				else
					strSelfOutputName = "lib" + self.m_kProjectProperties.m_strProjectName + ".a"
				end
			else
				strSelfOutputName = "lib" + self.m_kProjectProperties.m_strProjectName + ".a"
			end
			$g_kFilesData[strSelfOutputName][0] = generateBluePrint strSelfOutputName, "app", "generatePBXFileReferenceSection"
			self.m_strDataToWrite = "\t\t#{$g_kFilesData[strSelfOutputName][0]} /* #{strSelfOutputName} */ = {isa = PBXFileReference; explicitFileType = archive.ar; includeInIndex = 0; path = #{strSelfOutputName}; sourceTree = BUILT_PRODUCTS_DIR; };"
			writeDataToFile
		end
		
		# Add frameworks and dependency libraries
		#if self.m_kProjectProperties.m_bIsMainProject
			for strFrameWorkName in $g_kProjectNeededFrameworks
				strFrameWorkNamePath = strFrameWorkName.gsub(".framework", "")
				if self.m_kProjectProperties.m_bIsMainProject
					self.m_strDataToWrite = "\t\t#{$g_kFilesData[self.m_kProjectProperties.m_strProjectName + strFrameWorkName][1]} /* #{strFrameWorkName} */ = {isa = PBXFileReference; lastKnownFileType = wrapper.framework; name = #{strFrameWorkName}; path = \"#{BE3_SDK_DIR}3rdParty/#{strFrameWorkNamePath}/#{strFrameWorkName}\"; sourceTree = \"<group>\"; };"
				else
					self.m_strDataToWrite = "\t\t#{$g_kFilesData[self.m_kProjectProperties.m_strProjectName + strFrameWorkName][1]} /* #{strFrameWorkName} */ = {isa = PBXFileReference; lastKnownFileType = wrapper.framework; name = #{strFrameWorkName}; path = \"$(SRCROOT)/../../../../3rdParty/#{strFrameWorkNamePath}/#{strFrameWorkName}\"; sourceTree = \"<group>\"; };"
				end
				writeDataToFile
			end
			for strFrameWorkName in $g_kBakEngineNeededFrameworks
				next if $g_kProjectNeededFrameworks.include?(strFrameWorkName)
				if strFrameWorkName == "libz.dylib" or strFrameWorkName == "libsqlite3.0.dylib"
					self.m_strDataToWrite = "\t\t#{$g_kFilesData[self.m_kProjectProperties.m_strProjectName + strFrameWorkName][1]} /* #{strFrameWorkName} */ = {isa = PBXFileReference; lastKnownFileType = \"compiled.mach-o.dylib\"; name = #{strFrameWorkName}; path = usr/lib/#{strFrameWorkName}; sourceTree = SDKROOT; };"
				else
					self.m_strDataToWrite = "\t\t#{$g_kFilesData[self.m_kProjectProperties.m_strProjectName + strFrameWorkName][1]} /* #{strFrameWorkName} */ = {isa = PBXFileReference; lastKnownFileType = wrapper.framework; name = #{strFrameWorkName}; path = System/Library/Frameworks/#{strFrameWorkName}; sourceTree = SDKROOT; };"
				end
				writeDataToFile
			end
		#end
		
		# Add dependency projects
		if self.m_kProjectProperties.m_bIsMainProject or (self.m_kProjectProperties.m_strProjectName == $g_kFinalLibraryName and not ALL_DEPENDENCIES_IN_MAIN_PROJECT)
			if self.m_kProjectProperties.m_bIsMainProject
				if not ALL_DEPENDENCIES_IN_MAIN_PROJECT
					if ProjectProperties::s_kProjectDependencies[self.m_kProjectProperties.m_strProjectName].length > 0
						ProjectProperties::s_kProjectDependencies[self.m_kProjectProperties.m_strProjectName].each do |strDependencyName|
							self.m_strDataToWrite = "\t\t#{$g_kPBXContainerItemProxy[strDependencyName][2]} /* #{strDependencyName}#{$g_strXcodeFileSufix}.xcodeproj */ = {isa = PBXFileReference; lastKnownFileType = \"wrapper.pb-project\"; name = #{strDependencyName}#{$g_strXcodeFileSufix}.xcodeproj; path = #{BE3_SDK_DIR}BeLibs/#{strDependencyName}#{PROJECT_FILES_PATH}/#{strDependencyName}#{$g_strXcodeFileSufix}.xcodeproj; sourceTree = \"<group>\"; };"
							writeDataToFile
						end
					end
				else
					kAppModulesVisited = Array.new
					writeDependencyProjectsToFileRefs kAppModulesVisited, self.m_kProjectProperties.m_strProjectPath, self.m_kProjectProperties.m_strProjectName, true
				end
			else
				if not ALL_DEPENDENCIES_IN_MAIN_PROJECT
					kAppModulesVisited = Array.new
					writeDependencyProjectsToFileRefs kAppModulesVisited, self.m_kProjectProperties.m_strProjectPath, self.m_kProjectProperties.m_strProjectName, true
				end
			end
		end
		
		if self.m_kProjectProperties.m_bIsMainProject
			self.m_strDataToWrite = "\t\t#{$g_kFilesData[self.m_kProjectProperties.m_strProjectName + "assets"][1]} /* assets */ = {isa = PBXFileReference; lastKnownFileType = folder; name = assets; path = ../assets; sourceTree = \"<group>\"; };"
			writeDataToFile
			self.m_strDataToWrite = "\t\t#{$g_kFilesData[self.m_kProjectProperties.m_strProjectName + "iOSVariant"][1]} /* iOSVariant */ = {isa = PBXFileReference; lastKnownFileType = folder; name = iOSVariant; path = ../iOSVariant; sourceTree = \"<group>\"; };"
			writeDataToFile
			
			$g_kFiltersAndFilesResources.each_key do |strKey|
				if not $g_kFiltersAndFilesResources[strKey][3]
					strFileName = $g_kFiltersAndFilesResources[strKey][0]
					strFileNameExtension = nil
					
					iIndexExtension = strFileName.index('.')
					if iIndexExtension != nil
						iIndexExtension += 1
						strFileNameExtension = strFileName[iIndexExtension..strFileName.length]
					else
						if RES_EXT.include? strFileName
							strFileNameExtension = strFileName
						end
					end
						
					if strFileNameExtension != nil
						self.m_strDataToWrite = "\t\t#{$g_kFiltersAndFilesResources[strKey][4]} /* #{strFileName} */ = {isa = PBXFileReference; lastKnownFileType = #{RES_EXT[strFileNameExtension]}; name = \"#{strFileName}\"; path = \"#{$g_kFiltersAndFilesResources[strKey][2].gsub('\\','/')}\"; sourceTree = \"<group>\"; };"
						writeDataToFile
					end
				end
			end
			
			$g_kFiltersAndFilesVariants.each_key do |strKey|
				iIndexExtension = strKey.index('.')
				if iIndexExtension != nil
					iIndexExtension += 1
					strFileNameExtension = strKey[iIndexExtension..strKey.length]
					
					if $g_kFiltersAndFilesVariants[strKey][2].size > 0
						for i in 0..($g_kFiltersAndFilesVariants[strKey][2].size-1)
							strVariantName = $g_kFiltersAndFilesVariants[strKey][2][i]
							strVariantName = strVariantName.gsub('../iOSVariant/', '')
							iIndexVariantExtension = strVariantName.index('.')
							if iIndexVariantExtension != nil
								strVariantName = strVariantName[0..iIndexVariantExtension-1]
							end
							
							self.m_strDataToWrite = "\t\t#{$g_kFiltersAndFilesVariants[strKey][3][i]} /* #{strVariantName} */ = {isa = PBXFileReference; lastKnownFileType = #{RES_EXT[strFileNameExtension]}; name = #{strVariantName}; path = \"#{$g_kFiltersAndFilesVariants[strKey][2][i].gsub('\\','/')}/#{strKey}\"; sourceTree = \"<group>\"; };"
							writeDataToFile
						end
					end
				end
			end
		end
		
		self.m_strDataToWrite = "/* End PBXFileReference section */"
		writeDataToFile
		writeDataToFile
	end

	# Writes dependency projects to File Refs
	private
	def writeDependencyProjectsToFramewotksBuildPhase(kAppModulesVisited, strProjectName, bIsFirst)

		if not bIsFirst
			strLibName = nil
			if strProjectName == $g_kFinalLibraryName
				strMainLIBOutputFile = GetMinLIBOutputName strProjectName
				strLibName = "lib" + strMainLIBOutputFile + ".a"
			else
				strLibName = "lib" + strProjectName + ".a"
			end
			self.m_strDataToWrite = "\t\t\t\t#{$g_kFilesData[self.m_kProjectProperties.m_strProjectName + strProjectName][0]} /* #{strLibName} in Frameworks */,"
			writeDataToFile
		end
		
		kAppModulesVisited.push strProjectName

		if ProjectProperties::s_kProject3rdPartyDependencies[strProjectName].length > 0
			ProjectProperties::s_kProject3rdPartyDependencies[strProjectName].each do |strDependencyName|
				if not kAppModulesVisited.include? strDependencyName
					dependencyProjectProperties = GetProjectProperties strDependencyName
					if dependencyProjectProperties != nil
						if dependencyProjectProperties.m_kProjectPlatformTargets == nil or (dependencyProjectProperties.m_kProjectPlatformTargets != nil and dependencyProjectProperties.m_kProjectPlatformTargets.include? PLATFORM_IOS)
							writeDependencyProjectsToFramewotksBuildPhase kAppModulesVisited, strDependencyName, false
						end
					end
				end
			end
		end
		
		if ProjectProperties::s_kProjectDependencies[strProjectName].length > 0
			ProjectProperties::s_kProjectDependencies[strProjectName].each do |strDependencyName|
				if not kAppModulesVisited.include? strDependencyName
					writeDependencyProjectsToFramewotksBuildPhase kAppModulesVisited, strDependencyName, false
				end
			end
		end

	end
	
	# Writes the Xcode PBXFrameworksBuildPhase section to project file
	private
	def generatePBXFrameworksBuildPhaseSection
		self.m_strDataToWrite = "/* Begin PBXFrameworksBuildPhase section */"
		writeDataToFile

		$g_kGroupBluePrints[self.m_kProjectProperties.m_strProjectName + "FrameworksBuildPhase"] = generateBluePrint self.m_kProjectProperties.m_strProjectName, "FrameworksBuildPhase", "generatePBXFrameworksBuildPhaseSection"
		self.m_strDataToWrite = "\t\t#{$g_kGroupBluePrints[self.m_kProjectProperties.m_strProjectName + "FrameworksBuildPhase"]} /* Frameworks */ = {
			isa = PBXFrameworksBuildPhase;
			buildActionMask = 2147483647;
			files = ("
		writeDataToFile
		
		# Add frameworks and dependency libraries
		#if self.m_kProjectProperties.m_bIsMainProject
			for strFrameWorkName in $g_kProjectNeededFrameworks
				self.m_strDataToWrite = "\t\t\t\t#{$g_kFilesData[self.m_kProjectProperties.m_strProjectName + strFrameWorkName][0]} /* #{strFrameWorkName} in Frameworks */,"
				writeDataToFile
			end
			for strFrameWorkName in $g_kBakEngineNeededFrameworks
				next if $g_kProjectNeededFrameworks.include?(strFrameWorkName)
				self.m_strDataToWrite = "\t\t\t\t#{$g_kFilesData[self.m_kProjectProperties.m_strProjectName + strFrameWorkName][0]} /* #{strFrameWorkName} in Frameworks */,"
				writeDataToFile
			end
			
		if self.m_kProjectProperties.m_bIsMainProject
			if ProjectProperties::s_kProjectDependencies[self.m_kProjectProperties.m_strProjectName].length > 0
				ProjectProperties::s_kProjectDependencies[self.m_kProjectProperties.m_strProjectName].each do |strDependency|
					if not ALL_DEPENDENCIES_IN_MAIN_PROJECT
						strLibName = nil
						if strDependency == $g_kFinalLibraryName
							strMainLIBOutputFile = GetMinLIBOutputName strDependency
							strLibName = "lib" + strMainLIBOutputFile + ".a"
						else
							strLibName = "lib" + strDependency + ".a"
						end
						self.m_strDataToWrite = "\t\t\t\t#{$g_kFilesData[self.m_kProjectProperties.m_strProjectName + strDependency][0]} /* #{strLibName} in Frameworks */,"
						writeDataToFile
					else
						kAppModulesVisited = Array.new
						writeDependencyProjectsToFramewotksBuildPhase kAppModulesVisited, self.m_kProjectProperties.m_strProjectName, true
					end
				end
			end
		else
			if not ALL_DEPENDENCIES_IN_MAIN_PROJECT
				kAppModulesVisited = Array.new
				writeDependencyProjectsToFramewotksBuildPhase kAppModulesVisited, self.m_kProjectProperties.m_strProjectName, true
			end
		end
		
		self.m_strDataToWrite = "\t\t\t);
			runOnlyForDeploymentPostprocessing = 0;
		};"
		writeDataToFile
		
		self.m_strDataToWrite = "/* End PBXFrameworksBuildPhase section */"
		writeDataToFile
		writeDataToFile
	end
	
	# Writes dependency projects to dependencies group
	private
	def writeDependencyProjectsDependencyGroup(kAppModulesVisited, strProjectName, bIsFirst)

		if not bIsFirst
			self.m_strDataToWrite = "\t\t\t\t#{$g_kPBXContainerItemProxy[strProjectName][2]} /* #{strProjectName}#{$g_strXcodeFileSufix}.xcodeproj */,"
			writeDataToFile
		end
		
		kAppModulesVisited.push strProjectName

		if ProjectProperties::s_kProject3rdPartyDependencies[strProjectName].length > 0
			ProjectProperties::s_kProject3rdPartyDependencies[strProjectName].each do |strDependencyName|
				if not kAppModulesVisited.include? strDependencyName
					dependencyProjectProperties = GetProjectProperties strDependencyName
					if dependencyProjectProperties != nil
						if dependencyProjectProperties.m_kProjectPlatformTargets == nil or (dependencyProjectProperties.m_kProjectPlatformTargets != nil and dependencyProjectProperties.m_kProjectPlatformTargets.include? PLATFORM_IOS)
							writeDependencyProjectsDependencyGroup kAppModulesVisited, strDependencyName, false
						end
					end
				end
			end
		end
		
		if ProjectProperties::s_kProjectDependencies[strProjectName].length > 0
			ProjectProperties::s_kProjectDependencies[strProjectName].each do |strDependencyName|
				if not kAppModulesVisited.include? strDependencyName
					writeDependencyProjectsDependencyGroup kAppModulesVisited, strDependencyName, false
				end
			end
		end

	end

	# Writes a filter to the PBXGroup
	private
	def writeProjectFilter(strFilterBluePrint)
		
		self.m_strDataToWrite = "\t\t#{strFilterBluePrint} /* #{$g_kFiltersAndFiles[strFilterBluePrint][0]} */ = {
			isa = PBXGroup;
			children = ("
		writeDataToFile
		
			if $g_kFiltersAndFiles[strFilterBluePrint][1].length > 0
				for strFileBluePrint in $g_kFiltersAndFiles[strFilterBluePrint][1]
					if $g_kFiltersAndFiles[strFileBluePrint][3]
						self.m_strDataToWrite = "\t\t\t\t#{strFileBluePrint} /* #{$g_kFiltersAndFiles[strFileBluePrint][0]} */,"
					else
						self.m_strDataToWrite = "\t\t\t\t#{$g_kFiltersAndFiles[strFileBluePrint][4]} /* #{$g_kFiltersAndFiles[strFileBluePrint][0]} */,"
					end
					writeDataToFile
				end
			end
		
		self.m_strDataToWrite = "\t\t\t);
			name = \"#{$g_kFiltersAndFiles[strFilterBluePrint][0]}\";
			sourceTree = \"<group>\";
		};"
		writeDataToFile
		
		if $g_kFiltersAndFiles[strFilterBluePrint][1].length > 0
			for strFileBluePrint in $g_kFiltersAndFiles[strFilterBluePrint][1]
				if $g_kFiltersAndFiles[strFileBluePrint][3]
					writeProjectFilter strFileBluePrint
				end
			end
		end
		
	end
	
	# Writes a resources filter to the PBXGroup
	private
	def writeProjectResourcesFilter(strFilterBluePrint)

		self.m_strDataToWrite = "\t\t#{strFilterBluePrint} /* #{$g_kFiltersAndFilesResources[strFilterBluePrint][0]} */ = {
			isa = PBXGroup;
			children = ("
		writeDataToFile
		
			if $g_kFiltersAndFilesResources[strFilterBluePrint][1].length > 0
				for strFileBluePrint in $g_kFiltersAndFilesResources[strFilterBluePrint][1]
					if $g_kFiltersAndFilesResources[strFileBluePrint][3]
						self.m_strDataToWrite = "\t\t\t\t#{strFileBluePrint} /* #{$g_kFiltersAndFilesResources[strFileBluePrint][0]} */,"
					else
						self.m_strDataToWrite = "\t\t\t\t#{$g_kFiltersAndFilesResources[strFileBluePrint][4]} /* #{$g_kFiltersAndFilesResources[strFileBluePrint][0]} */,"
					end
					writeDataToFile
				end
			end
		
		self.m_strDataToWrite = "\t\t\t);
			name = \"#{$g_kFiltersAndFilesResources[strFilterBluePrint][0]}\";
			sourceTree = \"<group>\";
		};"
		writeDataToFile
		
		if $g_kFiltersAndFilesResources[strFilterBluePrint][1].length > 0
			for strFileBluePrint in $g_kFiltersAndFilesResources[strFilterBluePrint][1]
				if $g_kFiltersAndFilesResources[strFileBluePrint][3]
					writeProjectResourcesFilter strFileBluePrint
				end
			end
		end
		
	end
	
	# Writes the project products from dependencies
	private
	def writeProjectsProductsFromDependencies(kAppModulesVisited, strProjectName, bIsFirst)
	
		if not bIsFirst
			strLibName = nil
			if strProjectName == $g_kFinalLibraryName
				strMainLIBOutputFile = GetMinLIBOutputName strProjectName
				strLibName = "lib" + strMainLIBOutputFile + ".a"
			else
				strLibName = "lib" + strProjectName + ".a"
			end
			
			$g_kGroupBluePrints[self.m_kProjectProperties.m_strProjectName + "Products" + strProjectName] = generateBluePrint self.m_kProjectProperties.m_strProjectName, strLibName + "Prdts", "wPPFD"
			self.m_strDataToWrite = "\t\t#{$g_kGroupBluePrints[self.m_kProjectProperties.m_strProjectName + "Products" + strProjectName]} /* Products */ = {
			isa = PBXGroup;
			children = ("
			writeDataToFile
			
			self.m_strDataToWrite = "\t\t\t\t#{$g_kFilesData[self.m_kProjectProperties.m_strProjectName + strProjectName][1]} /* #{strLibName} */,"
			writeDataToFile

			self.m_strDataToWrite = "\t\t\t);
			name = Products;
			sourceTree = \"<group>\";
		};"
			writeDataToFile
		end
		
		kAppModulesVisited.push strProjectName

		if ProjectProperties::s_kProject3rdPartyDependencies[strProjectName].length > 0
			ProjectProperties::s_kProject3rdPartyDependencies[strProjectName].each do |strDependencyName|
				if not kAppModulesVisited.include? strDependencyName
					dependencyProjectProperties = GetProjectProperties strDependencyName
					if dependencyProjectProperties != nil
						if dependencyProjectProperties.m_kProjectPlatformTargets == nil or (dependencyProjectProperties.m_kProjectPlatformTargets != nil and dependencyProjectProperties.m_kProjectPlatformTargets.include? PLATFORM_IOS)
							writeProjectsProductsFromDependencies kAppModulesVisited, strDependencyName, false
						end
					end
				end
			end
		end
		
		if ProjectProperties::s_kProjectDependencies[strProjectName].length > 0
			ProjectProperties::s_kProjectDependencies[strProjectName].each do |strDependencyName|
				if not kAppModulesVisited.include? strDependencyName
					writeProjectsProductsFromDependencies kAppModulesVisited, strDependencyName, false
				end
			end
		end
	
	end
	
	# Writes the Xcode PBXGroup section to project file
	private
	def generatePBXGroupSection
		self.m_strDataToWrite = "/* Begin PBXGroup section */"
		writeDataToFile

		# Writing the mainGroup group
		$g_kGroupBluePrints[self.m_kProjectProperties.m_strProjectName + "mainGroup"] = generateBluePrint self.m_kProjectProperties.m_strProjectName + "mainGroup", "mainGroup", "generatePBXGroupSection"
		self.m_strDataToWrite = "\t\t#{$g_kGroupBluePrints[self.m_kProjectProperties.m_strProjectName + "mainGroup"]} = {
			isa = PBXGroup;
			children = ("
		writeDataToFile
			
			if self.m_kProjectProperties.m_bIsMainProject or (self.m_kProjectProperties.m_strProjectName == $g_kFinalLibraryName and not ALL_DEPENDENCIES_IN_MAIN_PROJECT)
				$g_kGroupBluePrints[self.m_kProjectProperties.m_strProjectName + "Dependencies"] = generateBluePrint self.m_kProjectProperties.m_strProjectName + "Dependencies", "Dependencies", "generatePBXGroupSection"
				self.m_strDataToWrite = "\t\t\t\t#{$g_kGroupBluePrints[self.m_kProjectProperties.m_strProjectName + "Dependencies"]} /* Dependencies */,"
				writeDataToFile
			end
			
			$g_kGroupBluePrints[self.m_kProjectProperties.m_strProjectName + "Frameworks"] = generateBluePrint self.m_kProjectProperties.m_strProjectName + "Frameworks", "Frameworks", "generatePBXGroupSection"
			self.m_strDataToWrite = "\t\t\t\t#{$g_kGroupBluePrints[self.m_kProjectProperties.m_strProjectName + "Frameworks"]} /* Frameworks */,"
			writeDataToFile
			
			$g_kGroupBluePrints[self.m_kProjectProperties.m_strProjectName + "Products"] = generateBluePrint self.m_kProjectProperties.m_strProjectName + "Products", "Products", "generatePBXGroupSection"
			self.m_strDataToWrite = "\t\t\t\t#{$g_kGroupBluePrints[self.m_kProjectProperties.m_strProjectName + "Products"]} /* Products */,"
			writeDataToFile
			
			$g_kGroupBluePrints[self.m_kProjectProperties.m_strProjectName] = generateBluePrint self.m_kProjectProperties.m_strProjectName, "self", "generatePBXGroupSection"
			self.m_strDataToWrite = "\t\t\t\t#{$g_kGroupBluePrints[self.m_kProjectProperties.m_strProjectName]} /* #{$strProjectNameToUse} */,"
			writeDataToFile
			
			if self.m_kProjectProperties.m_bIsMainProject
				$g_kGroupBluePrints[self.m_kProjectProperties.m_strProjectName + "Resources"] = generateBluePrint self.m_kProjectProperties.m_strProjectName + "assets", "assets", "generatePBXGroupSection"
				self.m_strDataToWrite = "\t\t\t\t#{$g_kGroupBluePrints[self.m_kProjectProperties.m_strProjectName + "Resources"]} /* Resources */,"
				writeDataToFile
			end
			
		self.m_strDataToWrite = "\t\t\t);
			sourceTree = \"<group>\";
		};"
		writeDataToFile
			
		# Writing the project filters
		self.m_strDataToWrite = "\t\t#{$g_kGroupBluePrints[self.m_kProjectProperties.m_strProjectName]} /* #{$strProjectNameToUse} */ = {
			isa = PBXGroup;
			children = ("
		writeDataToFile
		
			self.m_strDataToWrite = "\t\t\t\t#{$g_kCodeFolderFilterBluePrint} /* src */,"
			writeDataToFile
			self.m_strDataToWrite = "\t\t\t\t#{$g_kIOSFolderFilterBluePrint} /* iOS */,"
			writeDataToFile
			self.m_strDataToWrite = "\t\t\t\t#{$g_kPchFolderFilterBluePrint} /* Pch */,"
			writeDataToFile
			if self.m_kProjectProperties.m_kProjectSpecialLibraries.size > 0
				self.m_strDataToWrite = "\t\t\t\t#{$g_kLibsFolderFilterBluePrint} /* Libs */,"
				writeDataToFile
			end
		
		self.m_strDataToWrite = "\t\t\t);
			name = #{$strProjectNameToUse};
			sourceTree = \"<group>\";
		};"
		writeDataToFile
		
		writeProjectFilter $g_kCodeFolderFilterBluePrint
		writeProjectFilter $g_kIOSFolderFilterBluePrint
		writeProjectFilter $g_kPchFolderFilterBluePrint
		if self.m_kProjectProperties.m_kProjectSpecialLibraries.size > 0
			writeProjectFilter $g_kLibsFolderFilterBluePrint
		end
		
		# Writing the Frameworks group
		self.m_strDataToWrite = "\t\t#{$g_kGroupBluePrints[self.m_kProjectProperties.m_strProjectName + "Frameworks"]} /* Frameworks */ = {
			isa = PBXGroup;
			children = ("
		writeDataToFile
		
			#if self.m_kProjectProperties.m_bIsMainProject
				for strFrameWorkName in $g_kProjectNeededFrameworks
					self.m_strDataToWrite = "\t\t\t\t#{$g_kFilesData[self.m_kProjectProperties.m_strProjectName + strFrameWorkName][1]} /* #{strFrameWorkName} */,"
					writeDataToFile
				end
				for strFrameWorkName in $g_kBakEngineNeededFrameworks
					next if $g_kProjectNeededFrameworks.include?(strFrameWorkName)
					self.m_strDataToWrite = "\t\t\t\t#{$g_kFilesData[self.m_kProjectProperties.m_strProjectName + strFrameWorkName][1]} /* #{strFrameWorkName} */,"
					writeDataToFile
				end
			#end
					
		self.m_strDataToWrite = "\t\t\t);
			name = Frameworks;
			sourceTree = \"<group>\";
		};"
		writeDataToFile
		
		# Writing the project Products group
		self.m_strDataToWrite = "\t\t#{$g_kGroupBluePrints[self.m_kProjectProperties.m_strProjectName + "Products"]} /* Products */ = {
			isa = PBXGroup;
			children = ("
		writeDataToFile
		strSelfOutputName = nil
		if self.m_kProjectProperties.m_bIsMainProject
			strSelfOutputName = $strProjectNameToUse + ".app"
		else
			if self.m_kProjectProperties.m_strProjectName == $g_kFinalLibraryName
				if self.m_kProjectProperties.m_strOutputFileName != nil
					strSelfOutputName = "lib" + self.m_kProjectProperties.m_strOutputFileName + ".a"
				else
					strSelfOutputName = "lib" + self.m_kProjectProperties.m_strProjectName + ".a"
				end
			else
				strSelfOutputName = "lib" + self.m_kProjectProperties.m_strProjectName + ".a"
			end
		end
		self.m_strDataToWrite = "\t\t\t\t#{$g_kFilesData[strSelfOutputName][0]} /* #{strSelfOutputName} */,"
		writeDataToFile
		self.m_strDataToWrite = "\t\t\t);
			name = Products;
			sourceTree = \"<group>\";
		};"
		writeDataToFile
		
		# Writing the project Resources group
		if self.m_kProjectProperties.m_bIsMainProject
			writeProjectResourcesFilter $g_kIOSResBluePrint
		end
		
		# Writing the dependencies group
		if self.m_kProjectProperties.m_bIsMainProject or (self.m_kProjectProperties.m_strProjectName == $g_kFinalLibraryName and not ALL_DEPENDENCIES_IN_MAIN_PROJECT)
			self.m_strDataToWrite = "\t\t#{$g_kGroupBluePrints[self.m_kProjectProperties.m_strProjectName + "Dependencies"]} /* Dependencies */ = {
			isa = PBXGroup;
			children = ("
			writeDataToFile
			
				# Add dependency projects
				if self.m_kProjectProperties.m_bIsMainProject
					if ProjectProperties::s_kProjectDependencies[self.m_kProjectProperties.m_strProjectName].length > 0
						ProjectProperties::s_kProjectDependencies[self.m_kProjectProperties.m_strProjectName].each do |strDependencyName|
							if not ALL_DEPENDENCIES_IN_MAIN_PROJECT
								self.m_strDataToWrite = "\t\t\t\t#{$g_kPBXContainerItemProxy[strDependencyName][2]} /* #{strDependencyName}#{$g_strXcodeFileSufix}.xcodeproj */,"
								writeDataToFile
							else
								kAppModulesVisited = Array.new
								writeDependencyProjectsDependencyGroup kAppModulesVisited, self.m_kProjectProperties.m_strProjectName, true
							end
						end
					end
				else
					if not ALL_DEPENDENCIES_IN_MAIN_PROJECT
						kAppModulesVisited = Array.new
						writeDependencyProjectsDependencyGroup kAppModulesVisited, self.m_kProjectProperties.m_strProjectName, true
					end
				end
				
			self.m_strDataToWrite = "\t\t\t);
			name = Dependencies;
			sourceTree = \"<group>\";
		};"
			writeDataToFile
		end
		
		# Writing the Dependencies Products groups
		if self.m_kProjectProperties.m_bIsMainProject or (self.m_kProjectProperties.m_strProjectName == $g_kFinalLibraryName and not ALL_DEPENDENCIES_IN_MAIN_PROJECT)
		
			if self.m_kProjectProperties.m_bIsMainProject
				if ProjectProperties::s_kProjectDependencies[self.m_kProjectProperties.m_strProjectName].length > 0
					ProjectProperties::s_kProjectDependencies[self.m_kProjectProperties.m_strProjectName].each do |strDependency|
						if not ALL_DEPENDENCIES_IN_MAIN_PROJECT
							$g_kGroupBluePrints[self.m_kProjectProperties.m_strProjectName + "Products" + strDependency] = generateBluePrint self.m_kProjectProperties.m_strProjectName + "Products" + strDependency, "Products" + strDependency, "generatePBXGroupSection"
							self.m_strDataToWrite = "\t\t#{$g_kGroupBluePrints[self.m_kProjectProperties.m_strProjectName + "Products" + strDependency]} /* Products */ = {
			isa = PBXGroup;
			children = ("
							writeDataToFile

							strLibName = nil
							if strDependency == $g_kFinalLibraryName
								strMainLIBOutputFile = GetMinLIBOutputName strDependency
								strLibName = "lib" + strMainLIBOutputFile + ".a"
							else
								strLibName = "lib" + strDependency + ".a"
							end
							self.m_strDataToWrite = "\t\t\t\t#{$g_kFilesData[self.m_kProjectProperties.m_strProjectName + strDependency][1]} /* #{strLibName} */,"
							writeDataToFile

							self.m_strDataToWrite = "\t\t\t);
			name = Products;
			sourceTree = \"<group>\";
		};"
							writeDataToFile
						else
							kAppModulesVisited = Array.new
							writeProjectsProductsFromDependencies kAppModulesVisited, self.m_kProjectProperties.m_strProjectName, true
						end
					end
				end
			else
				if not ALL_DEPENDENCIES_IN_MAIN_PROJECT
					kAppModulesVisited = Array.new
					writeProjectsProductsFromDependencies kAppModulesVisited, self.m_kProjectProperties.m_strProjectName, true
				end
			end

		end
		
		# Writing the project assets groups
		if self.m_kProjectProperties.m_bIsMainProject
			self.m_strDataToWrite = "\t\t#{$g_kGroupBluePrints[self.m_kProjectProperties.m_strProjectName + "Resources"]} /* Resources */ = {
			isa = PBXGroup;
			children = ("
			writeDataToFile

				self.m_strDataToWrite = "\t\t\t\t#{$g_kFilesData[self.m_kProjectProperties.m_strProjectName + "assets"][1]} /* assets */,"
				writeDataToFile
				
				self.m_strDataToWrite = "\t\t\t\t#{$g_kFilesData[self.m_kProjectProperties.m_strProjectName + "iOSVariant"][1]} /* iOSVariant */,"
				writeDataToFile
				
				self.m_strDataToWrite = "\t\t\t\t#{$g_kIOSResBluePrint} /* iOSRes */,"
				writeDataToFile
				
			self.m_strDataToWrite = "\t\t\t);
			name = Resources;
			sourceTree = \"<group>\";
		};"
			writeDataToFile		
		end
		
		self.m_strDataToWrite = "/* End PBXGroup section */"
		writeDataToFile
		writeDataToFile
	end

	# Writes dependencies to PBXNativeTarget
	private
	def writeDependenciesPBXNativeTarget(kAppModulesVisited, strProjectName, bIsFirst)

		if not bIsFirst
			$g_kPBXNativeTarget[self.m_kProjectProperties.m_strProjectName][3].push generateBluePrint strProjectName, "dependencies", "writeDependenciesPBXNativeTarget"
		end

		kAppModulesVisited.push strProjectName

		if ProjectProperties::s_kProject3rdPartyDependencies[strProjectName].length > 0
			ProjectProperties::s_kProject3rdPartyDependencies[strProjectName].each do |strDependencyName|
				if not kAppModulesVisited.include? strDependencyName
					dependencyProjectProperties = GetProjectProperties strDependencyName
					if dependencyProjectProperties != nil
						if dependencyProjectProperties.m_kProjectPlatformTargets == nil or (dependencyProjectProperties.m_kProjectPlatformTargets != nil and dependencyProjectProperties.m_kProjectPlatformTargets.include? PLATFORM_IOS)
							writeDependenciesPBXNativeTarget kAppModulesVisited, strDependencyName, false
						end
					end
				end
			end
		end
		
		if ProjectProperties::s_kProjectDependencies[strProjectName].length > 0
			ProjectProperties::s_kProjectDependencies[strProjectName].each do |strDependencyName|
				if not kAppModulesVisited.include? strDependencyName
					writeDependenciesPBXNativeTarget kAppModulesVisited, strDependencyName, false
				end
			end
		end

	end
	
	# Writes the Xcode PBXNativeTarget section to project file
	private
	def generatePBXNativeTargetSection
		self.m_strDataToWrite = "/* Begin PBXNativeTarget section */"
		writeDataToFile
		
		$g_kPBXNativeTarget[self.m_kProjectProperties.m_strProjectName][0] = generateBluePrint self.m_kProjectProperties.m_strProjectName, "self", "generatePBXNativeTargetSection"
		$g_kPBXNativeTarget[self.m_kProjectProperties.m_strProjectName][1] = generateBluePrint self.m_kProjectProperties.m_strProjectName, "buildConfigurationList", "generatePBXNativeTargetSection"
		
		if self.m_kProjectProperties.m_bIsMainProject
			$g_kPBXNativeTarget[self.m_kProjectProperties.m_strProjectName][2] = [generateBluePrint(self.m_kProjectProperties.m_strProjectName, "Sources", "generatePBXNativeTargetSection"), generateBluePrint(self.m_kProjectProperties.m_strProjectName, "Frameworks", "generatePBXNativeTargetSection"), generateBluePrint(self.m_kProjectProperties.m_strProjectName, "Resources", "generatePBXNativeTargetSection"), generateBluePrint(self.m_kProjectProperties.m_strProjectName, "CopyFiles", "generatePBXNativeTargetSection")]
		else
			if self.m_kProjectProperties.m_strProjectName == $g_kFinalLibraryName
				$g_kPBXNativeTarget[self.m_kProjectProperties.m_strProjectName][2] = [generateBluePrint(self.m_kProjectProperties.m_strProjectName, "Sources", "generatePBXNativeTargetSection"), generateBluePrint(self.m_kProjectProperties.m_strProjectName, "Frameworks", "generatePBXNativeTargetSection")]
			else
				$g_kPBXNativeTarget[self.m_kProjectProperties.m_strProjectName][2] = [generateBluePrint(self.m_kProjectProperties.m_strProjectName, "Sources", "generatePBXNativeTargetSection")]
			end			
		end
		
		self.m_strDataToWrite = "\t\t#{$g_kPBXNativeTarget[self.m_kProjectProperties.m_strProjectName][0]} /* #{$strProjectNameToUse} */ = {
			isa = PBXNativeTarget;
			buildConfigurationList = #{$g_kPBXNativeTarget[self.m_kProjectProperties.m_strProjectName][1]} /* Build configuration list for PBXNativeTarget \"#{$strProjectNameToUse}\" */;"
		writeDataToFile
		
		# buildPhases
		self.m_strDataToWrite = "\t\t\tbuildPhases = ("
		writeDataToFile
		for i in 0..$g_kPBXNativeTarget[self.m_kProjectProperties.m_strProjectName][2].length-1
			strBuildPhaseName = nil
			case i
				when 0
					strBuildPhaseName = "Sources"
				when 1
					strBuildPhaseName = "Frameworks"
				when 2
					strBuildPhaseName = "Resources"
				when 3
					strBuildPhaseName = "CopyFiles"
			end
			self.m_strDataToWrite = "\t\t\t\t#{$g_kPBXNativeTarget[self.m_kProjectProperties.m_strProjectName][2][i]} /* #{strBuildPhaseName} */,"
			writeDataToFile
		end
		self.m_strDataToWrite = "\t\t\t);"
		writeDataToFile
		
		# buildRules
		self.m_strDataToWrite = "\t\t\tbuildRules = (
			);"
		writeDataToFile
		
		# dependencies
		$g_kPBXNativeTarget[self.m_kProjectProperties.m_strProjectName][3] = []
		if self.m_kProjectProperties.m_bIsMainProject
			for strDependency in ProjectProperties::s_kProjectDependencies[self.m_kProjectProperties.m_strProjectName]
				if not ALL_DEPENDENCIES_IN_MAIN_PROJECT
					$g_kPBXNativeTarget[self.m_kProjectProperties.m_strProjectName][3].push generateBluePrint self.m_kProjectProperties.m_strProjectName, "dependencies", "generatePBXNativeTargetSection"
				else
					kAppModulesVisited = Array.new
					writeDependenciesPBXNativeTarget kAppModulesVisited, self.m_kProjectProperties.m_strProjectName, true
				end
			end
		else
			if not ALL_DEPENDENCIES_IN_MAIN_PROJECT
				if self.m_kProjectProperties.m_strProjectName == $g_kFinalLibraryName
					kAppModulesVisited = Array.new
					writeDependenciesPBXNativeTarget kAppModulesVisited, self.m_kProjectProperties.m_strProjectName, true
				end
			end
		end
		self.m_strDataToWrite = "\t\t\tdependencies = ("
		writeDataToFile
		#for strTargetDependencyBluePrint in $g_kPBXNativeTarget[self.m_kProjectProperties.m_strProjectName][3]
		#	self.m_strDataToWrite = "\t\t\t\t#{strTargetDependencyBluePrint} /* PBXTargetDependency */,"
		#	writeDataToFile
		#end
		self.m_strDataToWrite = "\t\t\t);"
		writeDataToFile
		
		self.m_strDataToWrite = "\t\t\tname = #{$strProjectNameToUse};
			productName = #{$strProjectNameToUse};"
		writeDataToFile

		strSelfOutputName = nil
		if self.m_kProjectProperties.m_bIsMainProject
			strSelfOutputName = $strProjectNameToUse + ".app"
		else
			if self.m_kProjectProperties.m_strProjectName == $g_kFinalLibraryName
				if self.m_kProjectProperties.m_strOutputFileName != nil
					strSelfOutputName = "lib" + self.m_kProjectProperties.m_strOutputFileName + ".a"
				else
					strSelfOutputName = "lib" + self.m_kProjectProperties.m_strProjectName + ".a"
				end
			else
				strSelfOutputName = "lib" + self.m_kProjectProperties.m_strProjectName + ".a"
			end			
		end
		self.m_strDataToWrite = "\t\t\tproductReference = #{$g_kFilesData[strSelfOutputName][0]} /* #{strSelfOutputName} */;"
		writeDataToFile
		
		if self.m_kProjectProperties.m_bIsMainProject
			self.m_strDataToWrite = "\t\t\tproductType = \"com.apple.product-type.application\";"
		else
			if self.m_kProjectProperties.m_strProjectName == $g_kFinalLibraryName
				self.m_strDataToWrite = "\t\t\tproductType = \"com.apple.product-type.library.static\";"
			else
				self.m_strDataToWrite = "\t\t\tproductType = \"com.apple.product-type.library.static\";"
			end
		end
		writeDataToFile
		
		self.m_strDataToWrite = "\t\t};"
		writeDataToFile

		self.m_strDataToWrite = "/* End PBXNativeTarget section */"
		writeDataToFile
		writeDataToFile
	end

	# Writes the containers to item proxy
	private
	def writeDependendiesToProjectReferences(kAppModulesVisited, strProjectName, bIsFirst)

		if self.m_kProjectProperties.m_bIsMainProject
			if not ALL_DEPENDENCIES_IN_MAIN_PROJECT
				if ProjectProperties::s_kProjectDependencies[strProjectName].length > 0
					ProjectProperties::s_kProjectDependencies[strProjectName].each do |strDependencyName|
						self.m_strDataToWrite = "\t\t\t\t{
					ProductGroup = #{$g_kGroupBluePrints[self.m_kProjectProperties.m_strProjectName + "Products" + strDependencyName]} /* Products */;
					ProjectRef = #{$g_kPBXContainerItemProxy[strDependencyName][2]} /* #{strDependencyName}#{$g_strXcodeFileSufix}.xcodeproj */;
				},"
						writeDataToFile
					end
				end
				return
			else
				if not bIsFirst
					self.m_strDataToWrite = "\t\t\t\t{
					ProductGroup = #{$g_kGroupBluePrints[self.m_kProjectProperties.m_strProjectName + "Products" + strProjectName]} /* Products */;
					ProjectRef = #{$g_kPBXContainerItemProxy[strProjectName][2]} /* #{strProjectName}#{$g_strXcodeFileSufix}.xcodeproj */;
				},"
					writeDataToFile
				end
			end
		else
			if not ALL_DEPENDENCIES_IN_MAIN_PROJECT
				if not bIsFirst
					self.m_strDataToWrite = "\t\t\t\t{
					ProductGroup = #{$g_kGroupBluePrints[self.m_kProjectProperties.m_strProjectName + "Products" + strProjectName]} /* Products */;
					ProjectRef = #{$g_kPBXContainerItemProxy[strProjectName][2]} /* #{strProjectName}#{$g_strXcodeFileSufix}.xcodeproj */;
				},"
					writeDataToFile
				end
			end
		end
		
		kAppModulesVisited.push strProjectName
		
		if ProjectProperties::s_kProject3rdPartyDependencies[strProjectName].length > 0
			ProjectProperties::s_kProject3rdPartyDependencies[strProjectName].each do |strDependencyName|
				if not kAppModulesVisited.include? strDependencyName
					dependencyProjectProperties = GetProjectProperties strDependencyName
					if dependencyProjectProperties != nil
						if dependencyProjectProperties.m_kProjectPlatformTargets == nil or (dependencyProjectProperties.m_kProjectPlatformTargets != nil and dependencyProjectProperties.m_kProjectPlatformTargets.include? PLATFORM_IOS)
							writeDependendiesToProjectReferences kAppModulesVisited, strDependencyName, false
						end
					end
				end
			end
		end

		if ProjectProperties::s_kProjectDependencies[strProjectName].length > 0
			ProjectProperties::s_kProjectDependencies[strProjectName].each do |strDependencyName|
				if not kAppModulesVisited.include? strDependencyName
					writeDependendiesToProjectReferences kAppModulesVisited, strDependencyName, false
				end
			end
		end

	end
	
	# Writes the Xcode PBXProject section to project file
	private
	def generatePBXProjectSection
		self.m_strDataToWrite = "/* Begin PBXProject section */"
		writeDataToFile
				
		$g_kPBXProjectSectionBluePrints[self.m_kProjectProperties.m_strProjectName] = generateBluePrint self.m_kProjectProperties.m_strProjectName, "buildConfigurationList", "generatePBXProjectSection"
		
		self.m_strDataToWrite = "\t\t#{m_strRootBluePrint} /* Project object */ = {
			isa = PBXProject;
			attributes = {
				LastUpgradeCheck = 0500;
			};
			buildConfigurationList = #{$g_kPBXProjectSectionBluePrints[self.m_kProjectProperties.m_strProjectName]} /* Build configuration list for PBXProject \"#{$strProjectNameToUse}#{$g_strXcodeFileSufix}\" */;
			compatibilityVersion = \"Xcode 3.2\";
			developmentRegion = English;
			hasScannedForEncodings = 0;
			knownRegions = (
				en,
			);
			mainGroup = #{$g_kGroupBluePrints[self.m_kProjectProperties.m_strProjectName + "mainGroup"]};
			productRefGroup = #{$g_kGroupBluePrints[self.m_kProjectProperties.m_strProjectName + "Products"]} /* Products */;
			projectDirPath = \"\";"
		writeDataToFile
		
		if self.m_kProjectProperties.m_bIsMainProject or (self.m_kProjectProperties.m_strProjectName == $g_kFinalLibraryName and not ALL_DEPENDENCIES_IN_MAIN_PROJECT)
			self.m_strDataToWrite = "\t\t\tprojectReferences = ("
			writeDataToFile
			kAppModulesVisited = Array.new
			writeDependendiesToProjectReferences kAppModulesVisited, self.m_kProjectProperties.m_strProjectName, true
			self.m_strDataToWrite = "\t\t\t);"
			writeDataToFile
		end
		
		self.m_strDataToWrite = "\t\t\tprojectRoot = \"\";
			targets = (
				#{$g_kPBXNativeTarget[self.m_kProjectProperties.m_strProjectName][0]} /* #{$strProjectNameToUse} */,
			);
		};"
		writeDataToFile
		
		self.m_strDataToWrite = "/* End PBXProject section */"
		writeDataToFile
		writeDataToFile
	end

	# Writes the Xcode PBXResourcesBuildPhase section to project file
	private
	def generatePBXResourcesBuildPhaseSection(strProjectName)
		self.m_strDataToWrite = "/* Begin PBXResourcesBuildPhase section */"
		writeDataToFile
		
			self.m_strDataToWrite = "		#{$g_kPBXNativeTarget[strProjectName][2][2]} /* Resources */ = {
			isa = PBXResourcesBuildPhase;
			buildActionMask = 2147483647;
			files = ("
			writeDataToFile
		
				self.m_strDataToWrite = "				#{$g_kFilesData[strProjectName + "assets"][0]} /* assets in Resources */,"
				writeDataToFile
				
				$g_kFiltersAndFilesResources.each_key do |strKey|
					next if $g_kFiltersAndFilesResources[strKey][0] == $strProjectNameToUse.downcase + "-Info.plist"
					if not $g_kFiltersAndFilesResources[strKey][3]
						self.m_strDataToWrite = "				#{strKey} /* #{$g_kFiltersAndFilesResources[strKey][0]} in Resources */,"
						writeDataToFile
					end
				end
				
				$g_kFiltersAndFilesVariants.each_key do |strKey|
					self.m_strDataToWrite = "				#{$g_kFiltersAndFilesVariants[strKey][0]} /* #{strKey} in Resources */,"
					writeDataToFile
				end
				
			self.m_strDataToWrite = "			);
			runOnlyForDeploymentPostprocessing = 0;
		};"
			writeDataToFile
		
		self.m_strDataToWrite = "/* End PBXResourcesBuildPhase section */"
		writeDataToFile
		writeDataToFile
	end

	# Writes dependencies to reference proxy section
	private
	def writeDependenciesToReferenceProxy(kAppModulesVisited, strProjectName, bIsFirst)

		if not bIsFirst
			strLibPathName = "lib" + strProjectName + ".a"
			strLibName = nil
			if strProjectName == $g_kFinalLibraryName
				strMainLIBOutputFile = GetMinLIBOutputName strProjectName
				strLibName = "lib" + strMainLIBOutputFile + ".a"
			end
			if strProjectName == $g_kFinalLibraryName
                self.m_strDataToWrite = "\t\t#{$g_kFilesData[self.m_kProjectProperties.m_strProjectName + strProjectName][1]} /* #{strLibName} */ = {
            isa = PBXReferenceProxy;
            fileType = archive.ar;
            "
                self.m_strDataToWrite += "path = #{strLibName};\n"
				self.m_strDataToWrite += "\t\t\tname = #{strLibName};\n"
            else
                self.m_strDataToWrite = "\t\t#{$g_kFilesData[self.m_kProjectProperties.m_strProjectName + strProjectName][1]} /* #{strLibPathName} */ = {
            isa = PBXReferenceProxy;
            fileType = archive.ar;
            "
                self.m_strDataToWrite += "path = #{strLibPathName};\n"
			end
			self.m_strDataToWrite += "\t\t\tremoteRef = #{$g_kPBXContainerItemProxy[strProjectName][0]} /* PBXContainerItemProxy */;
			sourceTree = BUILT_PRODUCTS_DIR;
		};"
			writeDataToFile
		end
		
		kAppModulesVisited.push strProjectName
		
		if ProjectProperties::s_kProject3rdPartyDependencies[strProjectName].length > 0
			ProjectProperties::s_kProject3rdPartyDependencies[strProjectName].each do |strDependencyName|
				if not kAppModulesVisited.include? strDependencyName
					dependencyProjectProperties = GetProjectProperties strDependencyName
					if dependencyProjectProperties != nil
						if dependencyProjectProperties.m_kProjectPlatformTargets == nil or (dependencyProjectProperties.m_kProjectPlatformTargets != nil and dependencyProjectProperties.m_kProjectPlatformTargets.include? PLATFORM_IOS)
							writeDependenciesToReferenceProxy kAppModulesVisited, strDependencyName, false
						end
					end
				end
			end
		end

		if ProjectProperties::s_kProjectDependencies[strProjectName].length > 0
			ProjectProperties::s_kProjectDependencies[strProjectName].each do |strDependencyName|
				if not kAppModulesVisited.include? strDependencyName
					writeDependenciesToReferenceProxy kAppModulesVisited, strDependencyName, false
				end
			end
		end
		
	end
	
	# Writes the Xcode PBXReferenceProxy section to project file
	private
	def generatePBXReferenceProxySection
		self.m_strDataToWrite = "/* Begin PBXReferenceProxy section */"
		writeDataToFile

		kAppModulesVisited = Array.new
		if self.m_kProjectProperties.m_bIsMainProject
		
			if not ALL_DEPENDENCIES_IN_MAIN_PROJECT
				for strDependency in ProjectProperties::s_kProjectDependencies[self.m_kProjectProperties.m_strProjectName]
					strLibName = nil
					if strDependency == $g_kFinalLibraryName
						strMainLIBOutputFile = GetMinLIBOutputName strDependency
						strLibName = "lib" + strMainLIBOutputFile + ".a"
					else
						strLibName = "lib" + strDependency + ".a"
					end
					self.m_strDataToWrite = "\t\t#{$g_kFilesData[self.m_kProjectProperties.m_strProjectName + strDependency][1]} /* #{strLibName} */ = {
			isa = PBXReferenceProxy;
			fileType = archive.ar;
			path = #{strLibName};
			remoteRef = #{$g_kPBXContainerItemProxy[strDependency][0]} /* PBXContainerItemProxy */;
			sourceTree = BUILT_PRODUCTS_DIR;
		};"
					writeDataToFile
				end
			else
				writeDependenciesToReferenceProxy kAppModulesVisited, self.m_kProjectProperties.m_strProjectName, true
			end
			
		else
			if not ALL_DEPENDENCIES_IN_MAIN_PROJECT
				if self.m_kProjectProperties.m_strProjectName == $g_kFinalLibraryName
					writeDependenciesToReferenceProxy kAppModulesVisited, self.m_kProjectProperties.m_strProjectName, true
				end
			end
		end
		
		self.m_strDataToWrite = "/* End PBXReferenceProxy section */"
		writeDataToFile
		writeDataToFile
	end

	# Writes the Xcode PBXSourcesBuildPhase section to project file
	private
	def generatePBXSourcesBuildPhaseSection
		self.m_strDataToWrite = "/* Begin PBXSourcesBuildPhase section */"
		writeDataToFile

		self.m_strDataToWrite = "\t\t#{$g_kPBXNativeTarget[self.m_kProjectProperties.m_strProjectName][2][0]} /* Sources */ = {
			isa = PBXSourcesBuildPhase;
			buildActionMask = 2147483647;
			files = ("
		writeDataToFile
		
		# Add source files to compile
		$g_kFiltersAndFiles.each_key do |strKey|
			if not $g_kFiltersAndFiles[strKey][3]
				strFileName = $g_kFiltersAndFiles[strKey][0]
				iIndexExtension = strFileName.index('.')
				if iIndexExtension != nil
					iIndexExtension += 1
					strFileNameExtension = strFileName[iIndexExtension..strFileName.length]
					if COMPILABLE_EXT.include? strFileNameExtension
						self.m_strDataToWrite = "\t\t\t\t#{strKey} /* #{strFileName} in Sources */,"
						writeDataToFile
					end
				end
			end
		end
		
		self.m_strDataToWrite = "\t\t\t);
			runOnlyForDeploymentPostprocessing = 0;
		};"
		writeDataToFile
		
		self.m_strDataToWrite = "/* End PBXSourcesBuildPhase section */"
		writeDataToFile
		writeDataToFile
	end

	# Writes dependencies to target dependency section
	private
	def writeDependenciesToTargetDependency(kAppModulesVisited, strProjectName, bIsFirst, iIndex)

		if not bIsFirst
			self.m_strDataToWrite = "\t\t#{$g_kPBXNativeTarget[self.m_kProjectProperties.m_strProjectName][3][iIndex]} /* PBXTargetDependency */ = {
			isa = PBXTargetDependency;
			name = #{strProjectName};
			targetProxy = #{$g_kPBXContainerItemProxy[strProjectName][1]} /* PBXContainerItemProxy */;
		};"
			writeDataToFile
			iIndex += 1
		end
		
		kAppModulesVisited.push strProjectName

		if ProjectProperties::s_kProject3rdPartyDependencies[strProjectName].length > 0
			ProjectProperties::s_kProject3rdPartyDependencies[strProjectName].each do |strDependencyName|
				if not kAppModulesVisited.include? strDependencyName
					dependencyProjectProperties = GetProjectProperties strDependencyName
					if dependencyProjectProperties != nil
						if dependencyProjectProperties.m_kProjectPlatformTargets == nil or (dependencyProjectProperties.m_kProjectPlatformTargets != nil and dependencyProjectProperties.m_kProjectPlatformTargets.include? PLATFORM_IOS)
							iIndex = writeDependenciesToTargetDependency kAppModulesVisited, strDependencyName, false, iIndex
						end
					end
				end
			end
		end
		
		if ProjectProperties::s_kProjectDependencies[strProjectName].length > 0
			ProjectProperties::s_kProjectDependencies[strProjectName].each do |strDependencyName|
				if not kAppModulesVisited.include? strDependencyName
					iIndex = writeDependenciesToTargetDependency kAppModulesVisited, strDependencyName, false, iIndex
				end
			end
		end
		
		return iIndex
		
	end
	
	# Writes the Xcode PBXTargetDependency section to project file
	private
	def generatePBXTargetDependencySection
		self.m_strDataToWrite = "/* Begin PBXTargetDependency section */"
		writeDataToFile

		iIndex = 0
		kAppModulesVisited = Array.new

		if self.m_kProjectProperties.m_bIsMainProject
			for strDependency in ProjectProperties::s_kProjectDependencies[self.m_kProjectProperties.m_strProjectName]
				if not ALL_DEPENDENCIES_IN_MAIN_PROJECT
					self.m_strDataToWrite = "\t\t#{$g_kPBXNativeTarget[self.m_kProjectProperties.m_strProjectName][3][iIndex]} /* PBXTargetDependency */ = {
			isa = PBXTargetDependency;
			name = #{strDependency};
			targetProxy = #{$g_kPBXContainerItemProxy[strDependency][1]} /* PBXContainerItemProxy */;
		};"
					writeDataToFile
					iIndex += 1
				else
					#iIndex = writeDependenciesToTargetDependency kAppModulesVisited, self.m_kProjectProperties.m_strProjectName, true, iIndex
				end
			end
		else
			if not ALL_DEPENDENCIES_IN_MAIN_PROJECT
				if self.m_kProjectProperties.m_strProjectName == $g_kFinalLibraryName
					#writeDependenciesToTargetDependency kAppModulesVisited, self.m_kProjectProperties.m_strProjectName, true, iIndex
				end
			end
		end
		
		self.m_strDataToWrite = "/* End PBXTargetDependency section */"
		writeDataToFile
		writeDataToFile
	end

	# Writes dependencies to target dependency section
	private
	def writeDependenciesToOtherLDLibs(kAppModulesVisited, strProjectName, strDependencyLibs, bIsFirst)

		if not bIsFirst
			strLibName = nil
			if strProjectName == $g_kFinalLibraryName
				strMainLIBOutputFile = GetMinLIBOutputName strProjectName
				strLibName = strMainLIBOutputFile
			else
				strLibName = strProjectName
			end
			strDependencyLibs += "\n\t\t\t\t\t-l#{strLibName},"
		end
		
		kAppModulesVisited.push strProjectName

		if ProjectProperties::s_kProject3rdPartyDependencies[strProjectName].length > 0
			ProjectProperties::s_kProject3rdPartyDependencies[strProjectName].each do |strDependencyName|
				if not kAppModulesVisited.include? strDependencyName
					dependencyProjectProperties = GetProjectProperties strDependencyName
					if dependencyProjectProperties != nil
						if dependencyProjectProperties.m_kProjectPlatformTargets == nil or (dependencyProjectProperties.m_kProjectPlatformTargets != nil and dependencyProjectProperties.m_kProjectPlatformTargets.include? PLATFORM_IOS)
							strDependencyLibs = writeDependenciesToOtherLDLibs kAppModulesVisited, strDependencyName, strDependencyLibs, false 
						end
					end
				end
			end
		end
		
		if ProjectProperties::s_kProjectDependencies[strProjectName].length > 0
			ProjectProperties::s_kProjectDependencies[strProjectName].each do |strDependencyName|
				if not kAppModulesVisited.include? strDependencyName
					strDependencyLibs = writeDependenciesToOtherLDLibs kAppModulesVisited, strDependencyName, strDependencyLibs, false 
				end
			end
		end
		
		return strDependencyLibs
		
	end
	
	# Writes the Xcode PBXVariantGroup section to project file
	private
	def generatePBXVariantGroupSection
		self.m_strDataToWrite = "/* Begin PBXVariantGroup section */"
		writeDataToFile
		
			$g_kFiltersAndFilesVariants.each_key do |strKey|
				self.m_strDataToWrite = "		#{$g_kFiltersAndFilesVariants[strKey][1]} /* #{strKey} */ = {
			isa = PBXVariantGroup;
			children = ("
				writeDataToFile
				
				if $g_kFiltersAndFilesVariants[strKey][2].size > 0
					for i in 0..($g_kFiltersAndFilesVariants[strKey][2].size-1)
						strVariantName = $g_kFiltersAndFilesVariants[strKey][2][i]
						strVariantName = strVariantName.gsub('../iOSVariant/', '')
						iIndexVariantExtension = strVariantName.index('.')
						if iIndexVariantExtension != nil
							strVariantName = strVariantName[0..iIndexVariantExtension-1]
						end
							
						self.m_strDataToWrite = "				#{$g_kFiltersAndFilesVariants[strKey][3][i]} /* #{strVariantName} */,"
						writeDataToFile
					end
				end
				
				self.m_strDataToWrite = "			);
			name = #{strKey};
			sourceTree = \"<group>\";
		};"
				writeDataToFile
			end
		
		self.m_strDataToWrite = "/* End PBXVariantGroup section */"
		writeDataToFile
		writeDataToFile
	end
	
	# Writes the Xcode XCBuildConfiguration section to project file
	private
	def writeBuildConfiguration(strBuildConfigurationName)
		
		# WRITE CONFIGURATION NATIVE
		
		$g_kBuildConfigurationBluePrints[self.m_kProjectProperties.m_strProjectName + strBuildConfigurationName + "Native"] = generateBluePrint self.m_kProjectProperties.m_strProjectName, strBuildConfigurationName + "Native", "writeBuildConfiguration"
		
		self.m_strDataToWrite = "\t\t#{$g_kBuildConfigurationBluePrints[self.m_kProjectProperties.m_strProjectName + strBuildConfigurationName + "Native"]} /* #{strBuildConfigurationName} */ = {
			isa = XCBuildConfiguration;
			buildSettings = {"
		writeDataToFile
		
		if self.m_kProjectProperties.m_bIsMainProject
			strBeLibsCommonIncludes = "					../,
					../src,
					../iOS,
					#{BE3_SDK_DIR}BeLibs,
					#{BE3_SDK_DIR}3rdParty,
					/usr/include/libxml2,"
		else
			strBeLibsCommonIncludes = "					../../,
					../src,
					../iOS,
					../../../3rdParty,
					/usr/include/libxml2,"
		end
		strBeLibIncludes = ""
		ProjectProperties::s_kProjectDependencies.each do |strProjectNameGlobal, kDependenciesArray|
			if strProjectNameGlobal == self.m_kProjectProperties.m_strProjectName
				if kDependenciesArray.length > 0
					kDependenciesArray.each do |strProjectDependencyName|
						if self.m_kProjectProperties.m_bIsMainProject
							strBeLibIncludes += "\n					#{BE3_SDK_DIR}BeLibs/#{strProjectDependencyName}/src,"
							strBeLibIncludes += "\n					#{BE3_SDK_DIR}BeLibs/#{strProjectDependencyName}/iOS,"
						else
							strBeLibIncludes += "\n					../../../BeLibs/#{strProjectDependencyName}/src,"
							strBeLibIncludes += "\n					../../../BeLibs/#{strProjectDependencyName}/iOS,"
						end
					end
				end
			end
		end
		if self.m_kProjectProperties.m_bIsMainProject
			if ProjectProperties::s_kProjectDependencies["ApplicationLib"].length > 0
				ProjectProperties::s_kProjectDependencies["ApplicationLib"].each do |strDependencyName|
				
					dependencyProjectProperties = GetProjectProperties strDependencyName
					if dependencyProjectProperties != nil
						if dependencyProjectProperties.m_kProjectPlatformTargets == nil or (dependencyProjectProperties.m_kProjectPlatformTargets != nil and dependencyProjectProperties.m_kProjectPlatformTargets.include? PLATFORM_IOS)
							strBeLibIncludes += "\n					#{BE3_SDK_DIR}BeLibs/#{strDependencyName}/src,"
							strBeLibIncludes += "\n					#{BE3_SDK_DIR}BeLibs/#{strDependencyName}/iOS,"
						end
					end
					
				end
			end
		end
		ProjectProperties::s_kProject3rdPartyDependencies.each do |strProjectNameGlobal, kDependenciesArray|
			if strProjectNameGlobal == self.m_kProjectProperties.m_strProjectName
				if kDependenciesArray.length > 0
					kDependenciesArray.each do |strProjectDependencyName|							
					
						dependencyProjectProperties = GetProjectProperties strProjectDependencyName
						if dependencyProjectProperties != nil
							if dependencyProjectProperties.m_kProjectPlatformTargets == nil or (dependencyProjectProperties.m_kProjectPlatformTargets != nil and dependencyProjectProperties.m_kProjectPlatformTargets.include? PLATFORM_IOS)
								if self.m_kProjectProperties.m_bIsMainProject
									strBeLibIncludes += "\n					#{BE3_SDK_DIR}3rdParty/#{strProjectDependencyName}/src,"
									strBeLibIncludes += "\n					#{BE3_SDK_DIR}3rdParty/#{strProjectDependencyName}/iOS,"
								else
									strBeLibIncludes += "\n					../../../3rdParty/#{strProjectDependencyName}/src,"
									strBeLibIncludes += "\n					../../../3rdParty/#{strProjectDependencyName}/iOS,"
								end
							end
						end
					end
				end
			end
		end
		if self.m_kProjectProperties.m_bIsMainProject
			if ProjectProperties::s_kProject3rdPartyDependencies["ApplicationLib"].length > 0
				bFirst = true
				ProjectProperties::s_kProject3rdPartyDependencies["ApplicationLib"].each do |strDependencyName|
				
					dependencyProjectProperties = GetProjectProperties strDependencyName
					if dependencyProjectProperties != nil
						if dependencyProjectProperties.m_kProjectPlatformTargets == nil or (dependencyProjectProperties.m_kProjectPlatformTargets != nil and dependencyProjectProperties.m_kProjectPlatformTargets.include? PLATFORM_IOS)
							strBeLibIncludes += "\n					#{BE3_SDK_DIR}3rdParty/#{strDependencyName}/src,"
							strBeLibIncludes += "\n					#{BE3_SDK_DIR}3rdParty/#{strDependencyName}/iOS,"
						end
					end
					
				end
			end
		end
		strBeLibFrameworkIncludes = ""
		ProjectProperties::s_kProjectIOSFrameworkDependencies.each do |strProjectNameGlobal, kDependenciesArray|
			if strProjectNameGlobal == self.m_kProjectProperties.m_strProjectName
				if kDependenciesArray.length > 0
					kDependenciesArray.each do |strProjectDependencyName|
						strBeLibFrameworkIncludes += "\n					\"../../../3rdParty/#{strProjectDependencyName}\","
					end
				end
			end
		end
		if self.m_kProjectProperties.m_bIsMainProject
			$g_kProjectNeededFrameworks.each do |strFrameworkDependency|
				strFrameworkDependencyBase = strFrameworkDependency.gsub('.framework', '')
				strBeLibFrameworkIncludes += "\n					\"#{BE3_SDK_DIR}3rdParty/#{strFrameworkDependencyBase}\","
			end
		end
		
		strLibName = nil
		if self.m_kProjectProperties.m_strProjectName == $g_kFinalLibraryName
			strMainLIBOutputFile = GetMinLIBOutputName self.m_kProjectProperties.m_strProjectName
			strLibName = strMainLIBOutputFile
		else
			strLibName = self.m_kProjectProperties.m_strProjectName
		end
		
		strLibsToLink = "\"-ObjC\",		
					\"-lxml2\",
					\"-lsqlite3.0\","
		for strFrameWorkName in $g_kBakEngineNeededFrameworks
			if strFrameWorkName != "libz.dylib" and strFrameWorkName != "libsqlite3.0.dylib"
				strFrameWorkNameOnly = strFrameWorkName.gsub(".framework", "")
				strLibsToLink += "\n					\"-weak_framework\",
					#{strFrameWorkNameOnly},"
			end
		end	
		strLibsToLink += "\n					\"-weak_framework\",
					FacebookSDK,
					\"-weak_framework\",
					AdColony,
					\"-weak_framework\",
					MobileAppTracker,"
					
		strAdditionalIncludes = ""
		if self.m_kProjectProperties.m_strExtraIncludeDirs != nil
			for includeFolder in self.m_kProjectProperties.m_strExtraIncludeDirs
				strAdditionalIncludes = strAdditionalIncludes + "\n					../#{includeFolder},"
			end
		end
		strExternalIncludes = ""
		kArrayDependencyVisited = Array.new
		ProjectProperties::s_kProjectExternalDependencies.each do |strProjectNameGlobal, kDependenciesArray|
			if (strProjectNameGlobal == self.m_kProjectProperties.m_strProjectName) or self.m_kProjectProperties.m_bIsMainProject
				if kDependenciesArray.length > 0
					kDependenciesArray.each do |strProjectDependencyName|
					
						if not self.m_kProjectProperties.m_bIsMainProject or (self.m_kProjectProperties.m_bIsMainProject and not kArrayDependencyVisited.include?(strProjectDependencyName))
							if self.m_kProjectProperties.m_bIsMainProject 
								strExternalIncludes += "\n					#{BE3_SDK_DIR}External/#{strProjectDependencyName}/include,"
							else
								strExternalIncludes += "\n					../../../External/#{strProjectDependencyName}/include,"
							end
							
							if self.m_kProjectProperties.m_bIsMainProject
								kArrayDependencyVisited.push(strProjectDependencyName)
							end
						end
						
					end
				end
			end
		end
		
		if self.m_kProjectProperties.m_bIsMainProject
			strNeLibLibraryIncludes = ""
			$g_kProjectSpecialLibraries.each do |strLibFileName, kProjectDetails|
				strLibFileNameTemp = strLibFileName.gsub('lib', '')
				strLibFileNameTemp = strLibFileNameTemp.gsub('.a', '')
				strLibsToLink += "\n\t\t\t\t\t-l#{strLibFileNameTemp},"
				
				strNewPath = "#{BE3_SDK_DIR}#{kProjectDetails[0]}#{kProjectDetails[1]}/iOS"
				if not strBeLibLibraryIncludes.include? strNewPath
					strBeLibLibraryIncludes += "\n					\"#{strNewPath}\","
				end
			end
			
			strExternalLibraryDirs = ""
			kArrayDependencyVisited = Array.new
			ProjectProperties::s_kProjectExternalDependencies.each do |strProjectNameGlobal, kDependenciesArray|
				if (strProjectNameGlobal == self.m_kProjectProperties.m_strProjectName)
					if kDependenciesArray.length > 0
						kDependenciesArray.each do |strProjectDependencyName|
						
							if not kArrayDependencyVisited.include?(strProjectDependencyName)
								strExternalLibraryDirs += "\n					\"#{BE3_SDK_DIR}External/#{strProjectDependencyName}/lib/iOS\","
								kArrayDependencyVisited.push(strProjectDependencyName)
							end
							
						end
					end
				end
			end
		end
			
		strDependencyLibs = ""
		kAppModulesVisited = Array.new
		if self.m_kProjectProperties.m_bIsMainProject
			if not ALL_DEPENDENCIES_IN_MAIN_PROJECT
				for strDependency in ProjectProperties::s_kProjectDependencies[self.m_kProjectProperties.m_strProjectName]
					strLibName = nil
					if strDependency == $g_kFinalLibraryName
						strMainLIBOutputFile = GetMinLIBOutputName strDependency
						strLibName = strMainLIBOutputFile
					else
						strLibName = strDependency
					end
					strDependencyLibs += "\n\t\t\t\t\t-l#{strLibName},"
				end
			else
				strDependencyLibs = writeDependenciesToOtherLDLibs kAppModulesVisited, self.m_kProjectProperties.m_strProjectName, strDependencyLibs, true
			end
		else
			if not ALL_DEPENDENCIES_IN_MAIN_PROJECT
				if self.m_kProjectProperties.m_strProjectName == $g_kFinalLibraryName
					strDependencyLibs = writeDependenciesToOtherLDLibs kAppModulesVisited, self.m_kProjectProperties.m_strProjectName, strDependencyLibs, true
				end
			end
		end
		if self.m_kProjectProperties.m_bIsMainProject
			kArrayDependencyVisited = Array.new
			ProjectProperties::s_kProjectExternalDependencies.each do |strProjectNameGlobal, kDependenciesArray|
				if (strProjectNameGlobal == self.m_kProjectProperties.m_strProjectName)
					if kDependenciesArray.length > 0
						kDependenciesArray.each do |strProjectDependencyName|
						
							if not kArrayDependencyVisited.include?(strProjectDependencyName)
								strDependencyLibs += "\n\t\t\t\t\t-l#{strProjectDependencyName},"
								kArrayDependencyVisited.push(strProjectDependencyName)
							end
							
						end
					end
				end
			end
		end
		
		if not self.m_kProjectProperties.m_bIsExternalLibrary
			self.m_strDataToWrite = 	"				CONFIGURATION_BUILD_DIR = \"#{MAIN_PROJECT_PATH_IOS}/bin/iOS/$(CONFIGURATION)\";\n"
		else
			self.m_strDataToWrite = 	"				CONFIGURATION_BUILD_DIR = \"#{MAIN_PROJECT_PATH_IOS}/lib/iOS\";\n"
		end
		self.m_strDataToWrite += 	"				CONFIGURATION_TEMP_DIR = \"#{MAIN_PROJECT_PATH_IOS}/obj/iOS/$(CONFIGURATION)\";\n"
		if strBuildConfigurationName == "Release"
			self.m_strDataToWrite += 	"				COPY_PHASE_STRIP = YES;\n"
		end
		if strBuildConfigurationName == "Release"
			self.m_strDataToWrite += 	"				DEBUG_INFORMATION_FORMAT = dwarf;\n"
		else
			self.m_strDataToWrite += 	"				DEBUG_INFORMATION_FORMAT = \"dwarf-with-dsym\";\n"
		end
		if strBuildConfigurationName == "Release"
			self.m_strDataToWrite += 	"				DEPLOYMENT_POSTPROCESSING = YES;\n"
		end
		self.m_strDataToWrite += 	"				GCC_ENABLE_CPP_EXCEPTIONS = YES;\n"
		self.m_strDataToWrite += 	"				GCC_ENABLE_CPP_RTTI = YES;\n"
		if strBuildConfigurationName == "Release"
			self.m_strDataToWrite += 	"				GCC_OPTIMIZATION_LEVEL = 3;\n"
		else
			self.m_strDataToWrite += 	"				GCC_OPTIMIZATION_LEVEL = 0;\n"
		end
		self.m_strDataToWrite += 	"				GCC_PRECOMPILE_PREFIX_HEADER = NO;\n"
		strGlobalSymbols = nil
		if $g_kGlobalSymbols.size > 0
			strGlobalSymbols = ""
			for extraExport in $g_kGlobalSymbols
				strGlobalSymbols = strGlobalSymbols + "\n					#{extraExport},"
			end
		end
		strAdditionalExports = nil
		if not self.m_kProjectProperties.m_bIsMainProject and self.m_kProjectProperties.m_strProjectExports != nil
			strAdditionalExports = ""
			for extraExport in self.m_kProjectProperties.m_strProjectExports
				strAdditionalExports = strAdditionalExports + "\n					#{extraExport},"
			end
		end
		if strBuildConfigurationName == "Release"
			self.m_strDataToWrite += 	"				GCC_PREPROCESSOR_DEFINITIONS = (
					NDEBUG,
					RELEASE,
					TARGET_IOS,
					TARGET_MOBILE,"
			if strGlobalSymbols != nil
				self.m_strDataToWrite += strGlobalSymbols
			end
			if strAdditionalExports != nil
				self.m_strDataToWrite += strAdditionalExports
			end
			self.m_strDataToWrite += "\n				);\n"
		else
			self.m_strDataToWrite += 	"				GCC_PREPROCESSOR_DEFINITIONS = (
					DEBUG,
					_DEBUG,
					TARGET_IOS,
					TARGET_MOBILE,"
			if strGlobalSymbols != nil
				self.m_strDataToWrite += strGlobalSymbols
			end
			if strAdditionalExports != nil
				self.m_strDataToWrite += strAdditionalExports
			end
			self.m_strDataToWrite += "\n				);\n"
		end
		
		strActiveWarnings = "YES"
		
		self.m_strDataToWrite += 	"				GCC_TREAT_IMPLICIT_FUNCTION_DECLARATIONS_AS_ERRORS = YES;\n"
		self.m_strDataToWrite += 	"				GCC_TREAT_INCOMPATIBLE_POINTER_TYPE_WARNINGS_AS_ERRORS = NO;\n"
		self.m_strDataToWrite += 	"				GCC_TREAT_WARNINGS_AS_ERRORS = NO;\n"
		self.m_strDataToWrite += 	"				GCC_WARN_64_TO_32_BIT_CONVERSION = NO;\n"
		self.m_strDataToWrite += 	"				GCC_WARN_ABOUT_DEPRECATED_FUNCTIONS = YES;\n"
		self.m_strDataToWrite += 	"				GCC_WARN_ABOUT_MISSING_FIELD_INITIALIZERS = YES;\n"
		self.m_strDataToWrite += 	"				GCC_WARN_ABOUT_MISSING_NEWLINE = NO;\n"
		self.m_strDataToWrite += 	"				GCC_WARN_ABOUT_MISSING_PROTOTYPES = NO;\n"
		self.m_strDataToWrite += 	"				GCC_WARN_ABOUT_RETURN_TYPE = YES;\n"
		self.m_strDataToWrite += 	"				GCC_WARN_CHECK_SWITCH_STATEMENTS = NO;\n"
		self.m_strDataToWrite += 	"				GCC_WARN_FOUR_CHARACTER_CONSTANTS = YES;\n"
		self.m_strDataToWrite += 	"				GCC_WARN_HIDDEN_VIRTUAL_FUNCTIONS = YES;\n"
		self.m_strDataToWrite += 	"				GCC_WARN_INITIALIZER_NOT_FULLY_BRACKETED = YES;\n"
		self.m_strDataToWrite += 	"				GCC_WARN_MULTIPLE_DEFINITION_TYPES_FOR_SELECTOR = YES;\n"
		self.m_strDataToWrite += 	"				GCC_WARN_NON_VIRTUAL_DESTRUCTOR = NO;\n"
		self.m_strDataToWrite += 	"				GCC_WARN_SHADOW = NO;\n"
		self.m_strDataToWrite += 	"				GCC_WARN_SIGN_COMPARE = NO;\n"
		self.m_strDataToWrite += 	"				GCC_WARN_STRICT_SELECTOR_MATCH = NO;\n"
		self.m_strDataToWrite += 	"				GCC_WARN_UNDECLARED_SELECTOR = NO;\n"
		self.m_strDataToWrite += 	"				GCC_WARN_UNINITIALIZED_AUTOS = YES;\n"
		self.m_strDataToWrite += 	"				GCC_WARN_UNKNOWN_PRAGMAS = NO;\n"
		self.m_strDataToWrite += 	"				GCC_WARN_UNUSED_FUNCTION = NO;\n"
		self.m_strDataToWrite += 	"				GCC_WARN_UNUSED_LABEL = NO;\n"
		self.m_strDataToWrite += 	"				GCC_WARN_UNUSED_PARAMETER = NO;\n"
		self.m_strDataToWrite += 	"				GCC_WARN_UNUSED_VALUE = NO;\n"
		self.m_strDataToWrite += 	"				GCC_WARN_UNUSED_VARIABLE = NO;\n"
 		self.m_strDataToWrite += 	"				HEADER_SEARCH_PATHS = (
#{strBeLibsCommonIncludes}#{strBeLibIncludes}#{strAdditionalIncludes}#{strExternalIncludes}
				);\n"
		self.m_strDataToWrite += 	"				FRAMEWORK_SEARCH_PATHS = (
					\"$(inherited)\",#{strBeLibFrameworkIncludes}
				);\n"
		self.m_strDataToWrite += 	"				LIBRARY_SEARCH_PATHS = (
					\"$(inherited)\",
					\"/usr/lib\",
					\"#{MAIN_PROJECT_PATH_IOS}/bin/iOS/$(CONFIGURATION)\",#{strBeLibLibraryIncludes}#{strExternalLibraryDirs}
				);\n"
		self.m_strDataToWrite += 	"				OTHER_CFLAGS = (
					\"-ffast-math\",
					\"-fvisibility=hidden\",
					\"-Wno-deprecated\",
					\"-Wno-return-type-c-linkage\",
				);\n"
		#self.m_strDataToWrite += 	"				OTHER_LDFLAGS = (
		#			\"-lxml2\",
		#			#{strLibsToLink}\n"
		if self.m_kProjectProperties.m_bIsMainProject
			self.m_strDataToWrite += 	"				OTHER_LDFLAGS = (
					#{strLibsToLink}"
			if strDependencyLibs.length > 0
				self.m_strDataToWrite += 	"					#{strDependencyLibs}\n"
			else
				self.m_strDataToWrite += 	"\n"
			end
			self.m_strDataToWrite += 	"				);\n"
		end
		
		strLibName = nil
		if self.m_kProjectProperties.m_strProjectName == $g_kFinalLibraryName
			if self.m_kProjectProperties.m_strOutputFileName != nil
				strLibName = self.m_kProjectProperties.m_strOutputFileName
			else
				strLibName = self.m_kProjectProperties.m_strProjectName
			end
		else
			strLibName = $strProjectNameToUse
		end
		self.m_strDataToWrite += 	"				PRODUCT_NAME = #{strLibName};\n"
		if self.m_kProjectProperties.m_bIsMainProject
		#	self.m_strDataToWrite += 	"				PROVISIONING_PROFILE = \"400F6976-4E00-4688-9BDA-31D12F090D00\";\n"
		#	if strBuildConfigurationName == "Debug"
		#		self.m_strDataToWrite += 	"				\"PROVISIONING_PROFILE[sdk=*]\" = \"400F6976-4E00-4688-9BDA-31D12F090D00\";\n"
		#	else
		#		self.m_strDataToWrite += 	"				\"PROVISIONING_PROFILE[sdk=iphoneos*]\" = \"400F6976-4E00-4688-9BDA-31D12F090D00\";\n"
		#	end
		
			self.m_strDataToWrite += 	"				PROVISIONING_PROFILE = \"a394328f-a4fc-4560-8444-9ee906cf7abb\";\n"
			self.m_strDataToWrite += 	"				TARGETED_DEVICE_FAMILY = \"1,2\";\n"
			self.m_strDataToWrite += 	"				ARCHS = \"armv7 arm64\";"
			self.m_strDataToWrite += 	"				VALID_ARCHS = \"armv7 arm64\";"
		else
			self.m_strDataToWrite += 	"				ARCHS = \"armv7 arm64\";"
			self.m_strDataToWrite += 	"				VALID_ARCHS = \"armv7 arm64\";"
		end

		writeDataToFile

		self.m_strDataToWrite = "\t\t\t};
			name = #{strBuildConfigurationName};
		};"
		writeDataToFile
		
		
		
		# WRITE CONFIGURATION OBJECTIVE-C
		
		$g_kBuildConfigurationBluePrints[self.m_kProjectProperties.m_strProjectName + strBuildConfigurationName + "Project"] = generateBluePrint self.m_kProjectProperties.m_strProjectName, strBuildConfigurationName + "Project", "writeBuildConfiguration"
		
		self.m_strDataToWrite = "\t\t#{$g_kBuildConfigurationBluePrints[self.m_kProjectProperties.m_strProjectName + strBuildConfigurationName + "Project"]} /* #{strBuildConfigurationName} */ = {
			isa = XCBuildConfiguration;
			buildSettings = {"
		writeDataToFile
		
		self.m_strDataToWrite = 	"				ALWAYS_SEARCH_USER_PATHS = NO;\n"
		self.m_strDataToWrite += 	"				ARCHS = \"$(ARCHS_STANDARD)\";\n"
		self.m_strDataToWrite += 	"				CLANG_WARN_BOOL_CONVERSION = YES;\n"
		self.m_strDataToWrite += 	"				CLANG_WARN_CONSTANT_CONVERSION = YES;\n"
		self.m_strDataToWrite += 	"				CLANG_WARN_EMPTY_BODY = YES;\n"
		self.m_strDataToWrite += 	"				CLANG_WARN_ENUM_CONVERSION = YES;\n"
		self.m_strDataToWrite += 	"				CLANG_WARN_INT_CONVERSION = YES;\n"
		self.m_strDataToWrite += 	"				CLANG_WARN__DUPLICATE_METHOD_MATCH = YES;\n"
		self.m_strDataToWrite += 	"				CONFIGURATION_BUILD_DIR = \"#{MAIN_PROJECT_PATH_IOS}/bin/iOS/$(CONFIGURATION)\";\n"
		self.m_strDataToWrite += 	"				CONFIGURATION_TEMP_DIR = \"#{MAIN_PROJECT_PATH_IOS}/obj/iOS/$(CONFIGURATION)\";\n"
		self.m_strDataToWrite += 	"				COPY_PHASE_STRIP = NO;\n"
		self.m_strDataToWrite += 	"				DEPLOYMENT_POSTPROCESSING = NO;\n"
		if self.m_kProjectProperties.m_bIsMainProject
			self.m_strDataToWrite += 	"				DSTROOT = /tmp/#{self.m_kProjectProperties.m_strOutputFileName}.dst;\n"
		else
			self.m_strDataToWrite += 	"				DSTROOT = /tmp/#{self.m_kProjectProperties.m_strProjectName}.dst;\n"
		end
		self.m_strDataToWrite += 	"				GCC_C_LANGUAGE_STANDARD = gnu99;\n"
		self.m_strDataToWrite += 	"				GCC_INLINES_ARE_PRIVATE_EXTERN = NO;\n"
		self.m_strDataToWrite += 	"				GCC_SYMBOLS_PRIVATE_EXTERN = NO;\n"
		self.m_strDataToWrite += 	"				GCC_VERSION = com.apple.compilers.llvm.clang.1_0;\n"
		self.m_strDataToWrite += 	"				GCC_WARN_64_TO_32_BIT_CONVERSION = YES;\n"
		self.m_strDataToWrite += 	"				GCC_WARN_ABOUT_RETURN_TYPE = YES;\n"
		self.m_strDataToWrite += 	"				GCC_WARN_UNDECLARED_SELECTOR = NO;\n"
		self.m_strDataToWrite += 	"				GCC_WARN_UNINITIALIZED_AUTOS = YES;\n"
		self.m_strDataToWrite += 	"				GCC_WARN_UNUSED_FUNCTION = YES;\n"
		self.m_strDataToWrite += 	"				GCC_WARN_UNUSED_VARIABLE = YES;\n"
		self.m_strDataToWrite += 	"				IPHONEOS_DEPLOYMENT_TARGET = 6.0;\n"
		self.m_strDataToWrite += 	"				SDKROOT = iphoneos;\n"
		if self.m_kProjectProperties.m_bIsMainProject
			self.m_strDataToWrite += 	"				CODE_SIGN_IDENTITY = \"\";\n"
			self.m_strDataToWrite += 	"				INFOPLIST_FILE = \"../iOSRes/#{$strProjectNameToUse.downcase}-Info.plist\";\n"
			#self.m_strDataToWrite += 	"				INFOPLIST_OUTPUT_FORMAT = xml;\n"
			self.m_strDataToWrite += 	"				PROVISIONING_PROFILE = \"53BCC8B3-B235-4803-BAA6-FF9CC01DC417\";\n"
			#self.m_strDataToWrite += 	"				PROVISIONING_PROFILE = \"\";\n"
			#if strBuildConfigurationName == "Debug"
			#	self.m_strDataToWrite += 	"				\"PROVISIONING_PROFILE[sdk=*]\" = \"\";\n"
			#else
			#	self.m_strDataToWrite += 	"				\"PROVISIONING_PROFILE[sdk=iphoneos*]\" = \"\";\n"
			#end
		else
			self.m_strDataToWrite += 	"				SKIP_INSTALL = YES;\n"
		end
		self.m_strDataToWrite += 	"				SYMROOT = \"#{MAIN_PROJECT_PATH_IOS}/bin/iOS/$(CONFIGURATION)\";"
		
		writeDataToFile
	
		self.m_strDataToWrite = "\t\t\t};
			name = #{strBuildConfigurationName};
		};"
		writeDataToFile
		
	end
	
	# Writes the Xcode XCBuildConfiguration section to project file
	private
	def generateXCBuildConfigurationSection
		self.m_strDataToWrite = "/* Begin XCBuildConfiguration section */"
		writeDataToFile

		if not self.m_kProjectProperties.m_bIsExternalLibrary
			writeBuildConfiguration "Debug"
		end
		writeBuildConfiguration "Release"
		#writeBuildConfiguration "Release Archive"
		
		self.m_strDataToWrite = "/* End XCBuildConfiguration section */"
		writeDataToFile
		writeDataToFile
	end

	# Writes the Xcode XCConfigurationList section to project file
	private
	def generateXCConfigurationListSection
		self.m_strDataToWrite = "/* Begin XCConfigurationList section */"
		writeDataToFile
								
		self.m_strDataToWrite = "\t\t#{$g_kPBXNativeTarget[self.m_kProjectProperties.m_strProjectName][1]} /* Build configuration list for PBXNativeTarget \"#{$strProjectNameToUse}\" */ = {
			isa = XCConfigurationList;
			buildConfigurations = (\n"
			if not self.m_kProjectProperties.m_bIsExternalLibrary
				self.m_strDataToWrite += "				#{$g_kBuildConfigurationBluePrints[self.m_kProjectProperties.m_strProjectName + "Debug" + "Native"]} /* Debug */,"
			end
				self.m_strDataToWrite += "				#{$g_kBuildConfigurationBluePrints[self.m_kProjectProperties.m_strProjectName + "Release" + "Native"]} /* Release */,
			);
			defaultConfigurationIsVisible = 0;
			defaultConfigurationName = Release;
		};"
		writeDataToFile
		##{$g_kBuildConfigurationBluePrints[self.m_kProjectProperties.m_strProjectName + "Release Archive" + "Native"]} /* Release Archive */,
		
		self.m_strDataToWrite = "\t\t#{$g_kPBXProjectSectionBluePrints[self.m_kProjectProperties.m_strProjectName]} /* Build configuration list for PBXProject \"#{$strProjectNameToUse}#{$g_strXcodeFileSufix}\" */ = {
			isa = XCConfigurationList;
			buildConfigurations = (\n"
			if not self.m_kProjectProperties.m_bIsExternalLibrary
				self.m_strDataToWrite += "				#{$g_kBuildConfigurationBluePrints[self.m_kProjectProperties.m_strProjectName + "Debug" + "Project"]} /* Debug */,"
			end
				self.m_strDataToWrite += "				#{$g_kBuildConfigurationBluePrints[self.m_kProjectProperties.m_strProjectName + "Release" + "Project"]} /* Release */,
			);
			defaultConfigurationIsVisible = 0;
			defaultConfigurationName = Release;
		};"
		writeDataToFile
		##{$g_kBuildConfigurationBluePrints[self.m_kProjectProperties.m_strProjectName + "Release Archive" + "Project"]} /* Release Archive */,

		self.m_strDataToWrite = "/* End XCConfigurationList section */"
		writeDataToFile
	end

	# Regenerates the Xcode project file
	public
	def regenProject(kProjectProperties, strCleanCommand)
		
		if kProjectProperties.m_bIsMainProject
			$strProjectNameToUse = kProjectProperties.m_strOutputFileName
		else
			$strProjectNameToUse = kProjectProperties.m_strProjectName
		end
		
		if kProjectProperties.m_bIsMainProject
			strProjectsFolderPath = kProjectProperties.m_strProjectPath + kProjectProperties.m_strProjectName + PROJECT_FILES_PATH
			strProjectOBJsFolderPath = kProjectProperties.m_strProjectPath + kProjectProperties.m_strProjectName + "/obj"
			
			strSolutionInfoListFile = $strProjectNameToUse.downcase + "-Info.plist"
			strSolutionPathInfoListFile = kProjectProperties.m_strProjectPath + "/" + kProjectProperties.m_strProjectName + "/iOSRes/" + strSolutionInfoListFile
		else
			strProjectsFolderPath = BE3_SDK_DIR_SCRIPTS + kProjectProperties.m_strProjectPath + kProjectProperties.m_strProjectName + PROJECT_FILES_PATH
			strProjectOBJsFolderPath = BE3_SDK_DIR_SCRIPTS + kProjectProperties.m_strProjectPath + kProjectProperties.m_strProjectName + "/obj"
		end
	
		if strCleanCommand == "clean"
		
			if Dir.exists? strProjectsFolderPath
				puts "Cleaning #{kProjectProperties.m_strProjectName} \"projects\" directory ..."
				
				require 'fileutils'
				FileUtils.rm_rf strProjectsFolderPath
			end
			
			if Dir.exists? strProjectOBJsFolderPath
				puts "Cleaning #{kProjectProperties.m_strProjectName} \"obj\" directory ..."
				
				require 'fileutils'
				FileUtils.rm_rf strProjectOBJsFolderPath
			end
			
			if Dir.exists? "obj"
				puts "Cleaning \"obj\" directory ..."
				
				require 'fileutils'
				FileUtils.rm_rf 'obj'
			end
			
			if Dir.exists? "bin"
				puts "Cleaning \"bin\" directory ..."
				
				require 'fileutils'
				FileUtils.rm_rf 'bin'
			end
			
			if kProjectProperties.m_bIsMainProject
				if File.exists? strSolutionPathInfoListFile
					puts "Cleaning #{strSolutionInfoListFile} ..."
					
					File.delete strSolutionPathInfoListFile
				end
			end
			
			return
			
		end
		
		
		
		if kProjectProperties.m_bIsMainProject
			if m_bIsFileCreationIntoMemory
				self.m_strFileMemoryBuffer = ""
			else
				self.m_outputFile = File.new(strSolutionPathInfoListFile, "w")
			end
			
			kConfigFile = File.open(BE3_BUILD_SYSTEM_DIR + "Scripts/ConfigXcodeInfoList.cfg", "r")
			self.m_strDataToWrite = kConfigFile.read
			kConfigFile.close

			self.m_strDataToWrite = self.m_strDataToWrite.gsub("%gameName", kProjectProperties.m_strGameName)
			self.m_strDataToWrite = self.m_strDataToWrite.gsub("%companyName", kProjectProperties.m_strCompanyName.downcase)
			if kProjectProperties.m_strForcedBundlePackage != nil
				self.m_strDataToWrite = self.m_strDataToWrite.gsub("%bundleIdentifier", kProjectProperties.m_strForcedBundlePackage)
			else
				self.m_strDataToWrite = self.m_strDataToWrite.gsub("%bundleIdentifier", "com.#{kProjectProperties.m_strCompanyName.downcase}.${PRODUCT_NAME}")
			end
			self.m_strDataToWrite = self.m_strDataToWrite.gsub("%versionName", kProjectProperties.m_strVersionName)
			self.m_strDataToWrite = self.m_strDataToWrite.gsub("%versionCode", kProjectProperties.m_strVersionCode)
			self.m_strDataToWrite = self.m_strDataToWrite.gsub("%applovinAppID", kProjectProperties.m_strIOSAppLovinAppID)
			self.m_strDataToWrite = self.m_strDataToWrite.gsub("%fbAppID", kProjectProperties.m_strIOSFaceBookAppID)
			self.m_strDataToWrite = self.m_strDataToWrite.gsub("%fbAppName", kProjectProperties.m_strIOSFaceBookAppName)
			
			writeDataToFile

			if m_bIsFileCreationIntoMemory
				checkFileDifferencesAndSave strSolutionPathInfoListFile, ""
			else
				self.m_outputFile.close
			end
		end
		

		
		# We add needed 3rdParty frameworks
		if ProjectProperties::s_kProjectIOSFrameworkDependencies[kProjectProperties.m_strProjectName].size > 0
			ProjectProperties::s_kProjectIOSFrameworkDependencies[kProjectProperties.m_strProjectName].each do |strFrameworkName|
				strFinalFrameworkName = strFrameworkName + ".framework"
				if not $g_kProjectNeededFrameworks.include? strFinalFrameworkName
					$g_kProjectNeededFrameworks.push strFinalFrameworkName
				end
			end
		end
		
		
		
		if not Dir.exists? strProjectsFolderPath
			Dir.mkdir strProjectsFolderPath
		end
		
		self.m_kProjectProperties = kProjectProperties

		if kProjectProperties.m_bIsMainProject
			strProjectPath = kProjectProperties.m_strProjectPath
		else
			strProjectPath = BE3_SDK_DIR_SCRIPTS + kProjectProperties.m_strProjectPath
		end
		strProjectName = kProjectProperties.m_strProjectName

		if kProjectProperties.m_bIsMainProject
			puts "Building #{$g_kPlatformNames[PLATFORM_IOS]} XCode - " + kProjectProperties.m_strOutputFileName + " project..."
		else
			puts "Building #{$g_kPlatformNames[PLATFORM_IOS]} XCode - " + strProjectName + " project..."
		end

		strProjectIOSPath = strProjectPath + strProjectName + PROJECT_FILES_PATH + "/" + $strProjectNameToUse + "#{$g_strXcodeFileSufix}.xcodeproj"

		if not File.exists? strProjectIOSPath
			Dir.mkdir(strProjectIOSPath)
		end

		strProjectIOSPathFile = strProjectIOSPath + "/project.pbxproj"

		self.m_strRootBluePrint = generateBluePrint strProjectName, "pbxproj", "regenProject"
		$g_kProjectBluePrints[strProjectName] = self.m_strRootBluePrint

		if m_bIsFileCreationIntoMemory
			self.m_strFileMemoryBuffer = ""
		else
			self.m_outputFile = File.new(strProjectIOSPathFile, "w")
		end

		self.m_strDataToWrite = "// !$*UTF8*$!
{
	archiveVersion = 1;
	classes = {
	};
	objectVersion = 46;
	objects = {
"
		writeDataToFile
		writeDataToFile

		generatePBXBuildFileSection

		if kProjectProperties.m_bIsMainProject == true or (self.m_kProjectProperties.m_strProjectName == $g_kFinalLibraryName and not ALL_DEPENDENCIES_IN_MAIN_PROJECT)
			generatePBXContainerItemProxySection
		end
		
		if kProjectProperties.m_bIsMainProject == true
			generatePBXCopyFilesBuildPhaseSection
		end

		generatePBXFileReferenceSection

		#if kProjectProperties.m_bIsMainProject == true
			generatePBXFrameworksBuildPhaseSection
		#end

		generatePBXGroupSection
		generatePBXNativeTargetSection
		generatePBXProjectSection

		if kProjectProperties.m_bIsMainProject == true or (self.m_kProjectProperties.m_strProjectName == $g_kFinalLibraryName and not ALL_DEPENDENCIES_IN_MAIN_PROJECT)
			generatePBXReferenceProxySection
		end
		
		if kProjectProperties.m_bIsMainProject == true
			generatePBXResourcesBuildPhaseSection kProjectProperties.m_strProjectName
		end

		generatePBXSourcesBuildPhaseSection

		if kProjectProperties.m_bIsMainProject == true or (self.m_kProjectProperties.m_strProjectName == $g_kFinalLibraryName and not ALL_DEPENDENCIES_IN_MAIN_PROJECT)
			generatePBXTargetDependencySection
		end

		if kProjectProperties.m_bIsMainProject == true
			generatePBXVariantGroupSection
		end
		
		generateXCBuildConfigurationSection
		generateXCConfigurationListSection

		self.m_strDataToWrite = "\t};\n\trootObject = #{self.m_strRootBluePrint} /* Project object */;
}
"
		writeDataToFile

		if m_bIsFileCreationIntoMemory
			checkFileDifferencesAndSave strProjectIOSPathFile, strProjectIOSPath
		else
			self.m_outputFile.close
		end

		
		strProjectWorkspacePath = strProjectPath + "/" + strProjectName + PROJECT_FILES_PATH + "/" + $strProjectNameToUse + "#{$g_strXcodeFileSufix}.xcodeproj/project.xcworkspace"

		if not File.exists? strProjectWorkspacePath
			Dir.mkdir(strProjectWorkspacePath)
		end

		strProjectWorkspacePathFile = strProjectWorkspacePath + "/contents.xcworkspacedata"

		if m_bIsFileCreationIntoMemory
			self.m_strFileMemoryBuffer = ""
		else
			self.m_outputFile = File.new(strProjectWorkspacePathFile, "w")
		end
		
		self.m_strDataToWrite = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<Workspace
	version = \"1.0\">
	<FileRef
		location = \"self:#{$strProjectNameToUse}#{$g_strXcodeFileSufix}.xcodeproj\">
	</FileRef>
</Workspace>
"
		writeDataToFile
		
		if m_bIsFileCreationIntoMemory
			checkFileDifferencesAndSave strProjectWorkspacePathFile, ""
		else
			self.m_outputFile.close
		end

	end

	
	
	# Writes the project dependencies to workspace
	private
	def writeBuildActionEntryToScheme(kAppModulesVisited, strProjectPath, strProjectName, bIsFirst, bIsMainProject)

		if ProjectProperties::s_kProject3rdPartyDependencies[strProjectName].length > 0
			ProjectProperties::s_kProject3rdPartyDependencies[strProjectName].each do |strDependencyName|
				if not kAppModulesVisited.include? strDependencyName
					dependencyProjectProperties = GetProjectProperties strDependencyName
					if dependencyProjectProperties != nil
						if dependencyProjectProperties.m_kProjectPlatformTargets == nil or (dependencyProjectProperties.m_kProjectPlatformTargets != nil and dependencyProjectProperties.m_kProjectPlatformTargets.include? PLATFORM_IOS)
							writeBuildActionEntryToScheme kAppModulesVisited, dependencyProjectProperties.m_strProjectPath, strDependencyName, false, bIsMainProject
						end
					end
				end
			end
		end

		if ProjectProperties::s_kProjectDependencies[strProjectName].length > 0
			ProjectProperties::s_kProjectDependencies[strProjectName].each do |strDependencyName|
				if not kAppModulesVisited.include? strDependencyName
					dependencyProjectProperties = GetProjectProperties strDependencyName
					if dependencyProjectProperties != nil
						writeBuildActionEntryToScheme kAppModulesVisited, dependencyProjectProperties.m_strProjectPath, strDependencyName, false, bIsMainProject
					end
				end
			end
		end
	
		strBluePrintToUse = $g_kPBXNativeTarget[strProjectName][0]
			
		if not bIsFirst or (bIsFirst and bIsMainProject)
			self.m_strDataToWrite = "			<BuildActionEntry
				buildForTesting = \"YES\"
				buildForRunning = \"YES\"
				buildForProfiling = \"YES\"
				buildForArchiving = \"YES\"
				buildForAnalyzing = \"YES\">
				<BuildableReference
					BuildableIdentifier = \"primary\"
					BlueprintIdentifier = \"#{strBluePrintToUse}\"
					BuildableName = \"%buildName\"
					BlueprintName = \"%bluePrintName\"
					ReferencedContainer = \"container:%container\">
				</BuildableReference>
			</BuildActionEntry>"
		 
			if bIsMainProject
				mainProjectProperties = GetProjectProperties strProjectName
				
				self.m_strDataToWrite = self.m_strDataToWrite.gsub("%buildName", "#{$strProjectNameToUse}.app")
				self.m_strDataToWrite = self.m_strDataToWrite.gsub("%bluePrintName", "#{$strProjectNameToUse}")
				self.m_strDataToWrite = self.m_strDataToWrite.gsub("%container", ".#{PROJECT_FILES_PATH}/#{$strProjectNameToUse}#{$g_strXcodeFileSufix}.xcodeproj")
			else
				if strProjectName == $g_kFinalLibraryName
					strMainLIBOutputFile = GetMinLIBOutputName strProjectName
					
					self.m_strDataToWrite = self.m_strDataToWrite.gsub("%buildName", "lib#{strMainLIBOutputFile}.a")
					self.m_strDataToWrite = self.m_strDataToWrite.gsub("%bluePrintName", "#{strMainLIBOutputFile}")
				else
					self.m_strDataToWrite = self.m_strDataToWrite.gsub("%buildName", "lib#{strProjectName}.a")
					self.m_strDataToWrite = self.m_strDataToWrite.gsub("%bluePrintName", "#{strProjectName}")
				end
				self.m_strDataToWrite = self.m_strDataToWrite.gsub("%container", "#{BE3_SDK_DIR_SCRIPTS}#{strProjectPath}#{strProjectName}#{PROJECT_FILES_PATH}/#{strProjectName}#{$g_strXcodeFileSufix}.xcodeproj")
			end
		 
			writeDataToFile
			
			if bIsMainProject
				return
			end
		end

		kAppModulesVisited.push strProjectName

	end
	
	# Writes the project dependencies to workspace
	private
	def generateXcodeSolutionScheme(strSchemePath, strSchemeName, kProjectProperties)
	
		strXcodeSolutionSchemeFilePath = strSchemePath + "/#{strSchemeName}.xcscheme"
		
		if m_bIsFileCreationIntoMemory
			self.m_strFileMemoryBuffer = ""
		else
			self.m_outputFile = File.new(strXcodeSolutionSchemeFilePath, "w")
		end
	
		self.m_strDataToWrite = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<Scheme
	version = \"1.3\">
	<BuildAction
		parallelizeBuildables = \"#{$g_strParallelizeBuilds}\"
		buildImplicitDependencies = \"NO\">
		<BuildActionEntries>
"
		writeDataToFile
		
		kAppModulesVisited = Array.new
		writeBuildActionEntryToScheme kAppModulesVisited, kProjectProperties.m_strProjectPath, kProjectProperties.m_strProjectName, true, false
		writeBuildActionEntryToScheme kAppModulesVisited, kProjectProperties.m_strProjectPath, kProjectProperties.m_strProjectName, true, true
				
		strCustomSchemeName = strSchemeName.downcase
		strCustomSchemeName = strCustomSchemeName.gsub(' ', '_')
		strCustomArchiveName = $strProjectNameToUse + "_" + strCustomSchemeName
		
		self.m_strDataToWrite = "		</BuildActionEntries>
	</BuildAction>
	<TestAction
		selectedDebuggerIdentifier = \"Xcode.DebuggerFoundation.Debugger.LLDB\"
		selectedLauncherIdentifier = \"Xcode.DebuggerFoundation.Launcher.LLDB\"
		shouldUseLaunchSchemeArgsEnv = \"YES\"
		buildConfiguration = \"#{strSchemeName}\">
		<Testables>
		</Testables>
		<MacroExpansion>
			<BuildableReference
				BuildableIdentifier = \"primary\"
				BlueprintIdentifier = \"#{$g_kPBXNativeTarget[kProjectProperties.m_strProjectName][0]}\"
				BuildableName = \"#{$strProjectNameToUse}.app\"
				BlueprintName = \"#{$strProjectNameToUse}\"
				ReferencedContainer = \"container:.#{PROJECT_FILES_PATH}/#{$strProjectNameToUse}#{$g_strXcodeFileSufix}.xcodeproj\">
			</BuildableReference>
		</MacroExpansion>
	</TestAction>
	<LaunchAction
		selectedDebuggerIdentifier = \"Xcode.DebuggerFoundation.Debugger.LLDB\"
		selectedLauncherIdentifier = \"Xcode.DebuggerFoundation.Launcher.LLDB\"
		launchStyle = \"0\"
		useCustomWorkingDirectory = \"NO\"
		buildConfiguration = \"#{strSchemeName}\"
		ignoresPersistentStateOnLaunch = \"NO\"
		debugDocumentVersioning = \"YES\"
		allowLocationSimulation = \"YES\">
		<BuildableProductRunnable>
			<BuildableReference
				BuildableIdentifier = \"primary\"
				BlueprintIdentifier = \"#{$g_kPBXNativeTarget[kProjectProperties.m_strProjectName][0]}\"
				BuildableName = \"#{$strProjectNameToUse}.app\"
				BlueprintName = \"#{$strProjectNameToUse}\"
				ReferencedContainer = \"container:.#{PROJECT_FILES_PATH}/#{$strProjectNameToUse}#{$g_strXcodeFileSufix}.xcodeproj\">
			</BuildableReference>
		</BuildableProductRunnable>
		<AdditionalOptions>
		</AdditionalOptions>
	</LaunchAction>
	<ProfileAction
		shouldUseLaunchSchemeArgsEnv = \"YES\"
		savedToolIdentifier = \"\"
		useCustomWorkingDirectory = \"NO\""
		writeDataToFile
		
		if strSchemeName == "Debug"
			self.m_strDataToWrite = "		debugDocumentVersioning = \"YES\""
			writeDataToFile
		end
		
		self.m_strDataToWrite = "		buildConfiguration = \"#{strSchemeName}\">
	</ProfileAction>
	<AnalyzeAction
		buildConfiguration = \"#{strSchemeName}\">
	</AnalyzeAction>
	<ArchiveAction
		buildConfiguration = \"#{strSchemeName}\"
		customArchiveName = \"#{strCustomArchiveName}\"
		revealArchiveInOrganizer = \"YES\">
	</ArchiveAction>
</Scheme>
"
		writeDataToFile
	
		if m_bIsFileCreationIntoMemory
			checkFileDifferencesAndSave strXcodeSolutionSchemeFilePath, ""
		else
			self.m_outputFile.close
		end
	
	end
	
	# Writes the project dependencies to workspace
	private
	def writeDependenciesToWorkspace(kAppModulesVisited, strProjectName, bIsFirst)

		if not bIsFirst
			self.m_strDataToWrite = "	<FileRef location = \"group:#{BE3_SDK_DIR}BeLibs/#{strProjectName}#{PROJECT_FILES_PATH}/#{strProjectName}#{$g_strXcodeFileSufix}.xcodeproj\" />"
			writeDataToFile
		end

		kAppModulesVisited.push strProjectName
		
		if ProjectProperties::s_kProject3rdPartyDependencies[strProjectName].length > 0
			ProjectProperties::s_kProject3rdPartyDependencies[strProjectName].each do |strDependencyName|
				if not kAppModulesVisited.include? strDependencyName
					dependencyProjectProperties = GetProjectProperties strDependencyName
					if dependencyProjectProperties != nil
						if dependencyProjectProperties.m_kProjectPlatformTargets == nil or (dependencyProjectProperties.m_kProjectPlatformTargets != nil and dependencyProjectProperties.m_kProjectPlatformTargets.include? PLATFORM_IOS)
							writeDependenciesToWorkspace kAppModulesVisited, strDependencyName, false
						end
					end
				end
			end
		end

		if ProjectProperties::s_kProjectDependencies[strProjectName].length > 0
			ProjectProperties::s_kProjectDependencies[strProjectName].each do |strDependencyName|
				if not kAppModulesVisited.include? strDependencyName
					writeDependenciesToWorkspace kAppModulesVisited, strDependencyName, false
				end
			end
		end

	end

	# Regenerates the Xcode solution workspace files
	private
	def generateSolutionWorkSpaceFiles(strProjectPath, strProjectName, kProjectProperties)
		
		if not kProjectProperties.m_bIsExternalLibrary
			strSolutionWorkspacePath = strProjectPath + "/" + strProjectName + "/" + $strProjectNameToUse + "#{$g_strXcodeFileSufix}.xcworkspace"
		else
			strSolutionWorkspacePath = "./" + $strProjectNameToUse + "#{$g_strXcodeFileSufix}.xcworkspace"
		end
		
		if not File.exists? strSolutionWorkspacePath
			Dir.mkdir(strSolutionWorkspacePath)
		end

		strSolutionWorkspacePathFile = strSolutionWorkspacePath + "/contents.xcworkspacedata"

		if m_bIsFileCreationIntoMemory
			self.m_strFileMemoryBuffer = ""
		else
			self.m_outputFile = File.new(strSolutionWorkspacePathFile, "w")
		end
		
		self.m_strDataToWrite = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<Workspace
	version = \"1.0\">"
		writeDataToFile

		#kAppModulesVisited = Array.new
		#writeDependenciesToWorkspace kAppModulesVisited, kProjectProperties.m_strProjectName, true
		
		self.m_strDataToWrite = "	<FileRef location = \"group:.#{PROJECT_FILES_PATH}/#{$strProjectNameToUse}#{$g_strXcodeFileSufix}.xcodeproj\" />"
		writeDataToFile

		self.m_strDataToWrite = "</Workspace>"
		writeDataToFile

		if m_bIsFileCreationIntoMemory
			checkFileDifferencesAndSave strSolutionWorkspacePathFile, ""
		else
			self.m_outputFile.close
		end

		

		strSolutionWorkspaceSharedDataPath = strSolutionWorkspacePath + "/xcshareddata"
		if not File.exists? strSolutionWorkspaceSharedDataPath
			Dir.mkdir(strSolutionWorkspaceSharedDataPath)
		end

		strSolutionWorkspaceSharedDataSettingsFile = strSolutionWorkspaceSharedDataPath + "/WorkspaceSettings.xcsettings"

		if m_bIsFileCreationIntoMemory
			self.m_strFileMemoryBuffer = ""
		else
			self.m_outputFile = File.new(strSolutionWorkspaceSharedDataSettingsFile, "w")
		end

		self.m_strDataToWrite = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">
<plist version = \"1.0\">
<dict>
	<key>IDEWorkspaceSharedSettings_AutocreateContextsIfNeeded</key>
	<false/>
</dict>
</plist>
</Workspace>
"
		writeDataToFile
		
		if m_bIsFileCreationIntoMemory
			checkFileDifferencesAndSave strSolutionWorkspaceSharedDataSettingsFile, ""
		else
			self.m_outputFile.close
		end
		
		
		
		strSolutionWorkspaceSharedDataSchemesPath = strSolutionWorkspaceSharedDataPath + "/xcschemes"
		if not File.exists? strSolutionWorkspaceSharedDataSchemesPath
			Dir.mkdir(strSolutionWorkspaceSharedDataSchemesPath)
		end
		
		if not kProjectProperties.m_bIsExternalLibrary
			generateXcodeSolutionScheme strSolutionWorkspaceSharedDataSchemesPath, "Debug", kProjectProperties
		end
		#generateXcodeSolutionScheme strSolutionWorkspaceSharedDataSchemesPath, "Release Archive", kProjectProperties
		generateXcodeSolutionScheme strSolutionWorkspaceSharedDataSchemesPath, "Release", kProjectProperties

	end

	# Regenerates the Xcode solution file
	public
	def regenSolution(kProjectProperties, strCleanCommand)

		if kProjectProperties.m_bIsMainProject
			$strProjectNameToUse = kProjectProperties.m_strOutputFileName
		else
			$strProjectNameToUse = kProjectProperties.m_strProjectName
		end
	
		if not kProjectProperties.m_bIsExternalLibrary
			strProjectsFolderPath = kProjectProperties.m_strProjectPath + kProjectProperties.m_strProjectName + PROJECT_FILES_PATH
		else
			strProjectsFolderPath = "." + PROJECT_FILES_PATH
		end
		
		strProjectPath = kProjectProperties.m_strProjectPath
		strProjectName = kProjectProperties.m_strProjectName
			
		if kProjectProperties.m_bIsMainProject			
			strSolutionWorkspaceFile = $strProjectNameToUse + "#{$g_strXcodeFileSufix}.xcworkspace"
		else
			strSolutionWorkspaceFile = strProjectName + "#{$g_strXcodeFileSufix}.xcworkspace"
		end
		if not kProjectProperties.m_bIsExternalLibrary
			strSolutionWorkspacePath = strProjectPath + "/" + strProjectName + "/" + strSolutionWorkspaceFile
		else
			strSolutionWorkspacePath = "./" + strSolutionWorkspaceFile
		end
		
		if strCleanCommand == "clean"
		
			if Dir.exists? strProjectsFolderPath
				puts "Cleaning #{kProjectProperties.m_strProjectName} \"projects\" directory ..."
				
				require 'fileutils'
				FileUtils.rm_rf strProjectsFolderPath
			end
			
			if Dir.exists? "obj"
				puts "Cleaning \"obj\" directory ..."
				
				require 'fileutils'
				FileUtils.rm_rf 'obj'
			end
			
			if Dir.exists? "bin"
				puts "Cleaning \"bin\" directory ..."
				
				require 'fileutils'
				FileUtils.rm_rf 'bin'
			end
			
			if Dir.exists? strSolutionWorkspacePath
				puts "Cleaning #{strSolutionWorkspaceFile} workspace ..."
				
				require 'fileutils'
				FileUtils.rm_rf strSolutionWorkspacePath
			end
			
			return
			
		end
		
		if not Dir.exists? strProjectsFolderPath
			Dir.mkdir strProjectsFolderPath
		end
	
		if kProjectProperties.m_bIsMainProject
			puts "Building #{$g_kPlatformNames[PLATFORM_IOS]} XCode - " + kProjectProperties.m_strOutputFileName + " solution..."
		else
			puts "Building #{$g_kPlatformNames[PLATFORM_IOS]} XCode - " + strProjectName + " solution..."
		end

		generateSolutionWorkSpaceFiles strProjectPath, strProjectName, kProjectProperties

	end

end
