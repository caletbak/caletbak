####################################################################
##
## BAK ENGINE 3
## BUILD SYSTEM
## 
## ExecBuildingSystem.rb
## This ruby file executes the building system
##
## Javier Calet Toledo - 2013
##
###################################################################

#!/bin/sh

BE3_BUILD_SYSTEM_VERSION = "1.0"

require_relative "BuildSystem_" + BE3_BUILD_SYSTEM_VERSION + "/Scripts/Common"

###################################################################

PROJECT_FILES_PATH = "/projects"

# BE3 SDK Directory
BE3_SDK_DIR_PREFIX = "/Be3Toolkit"
def SearchBE3SDKDir(strPrefixToFind, strStartingFolder, strPreFolderToAdd, bAbortIfNotFound)
	iMaxTries = 10
	
	strPathToBE3SDK = ""
	strPathToSearch = strStartingFolder + strPathToBE3SDK + strPrefixToFind
	
	while not File.exists? strPathToSearch do
		strPathToBE3SDK = strPathToBE3SDK + "/.."
		strPathToSearch = strStartingFolder + strPathToBE3SDK + strPrefixToFind
		iMaxTries = iMaxTries - 1
		if iMaxTries == 0
			if bAbortIfNotFound
				abort("No \"#{strPrefixToFind}\" found. Checkout it and be sure the folder is called \"#{strPrefixToFind}\".")
			else
				return nil
			end
		end
	end
	
	return "." + strPreFolderToAdd + strPathToBE3SDK + strPrefixToFind + "/"
end
BE3_SDK_DIR_SCRIPTS = SearchBE3SDKDir BE3_SDK_DIR_PREFIX, Dir.pwd, "", true
BE3_SDK_DIR = SearchBE3SDKDir BE3_SDK_DIR_PREFIX, Dir.pwd, "/..", true
BE3_BUILD_SYSTEM_DIR = BE3_SDK_DIR_SCRIPTS + "BeBuildTools/BuildSystem_" + BE3_BUILD_SYSTEM_VERSION + "/"

# REGENERATE PLATFORMS (Select platforms)
if OS::mac?
    REGENERATE_PLATFORMS = PLATFORM_IOS + PLATFORM_ANDROID + PLATFORM_ANDROID_ECLIPSE
else
	REGENERATE_PLATFORMS = PLATFORM_WINDOWS + PLATFORM_ANDROID + PLATFORM_ANDROID_ECLIPSE
end

###################################################################

if (ARGV[0] == "REGEN_PROJECT_EXTERNAL")
	$g_bGeneratingExternalLib = true
end

require_relative "BuildSystem_" + BE3_BUILD_SYSTEM_VERSION + "/Scripts/RegenProjectFiles"

if (ARGV[0] == "REGEN_PROJECT" or ARGV[0] == "REGEN_PROJECT_EXTERNAL")
	ExecRegenProject ARGV[0], ARGV[1]
end

if (ARGV[0] == "BUILD_ANDROID")
	ExecAndroidBuild ARGV[0], ARGV[1], ARGV[2], ARGV[3]
end

###################################################################

